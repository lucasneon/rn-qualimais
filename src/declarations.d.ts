// Place here global interface and variable that should be available globally
declare var module;

declare module '*.json' {
  const value: any;
  export default value;
}

declare module '*.png' {
  const value: any;
  export default value;
}

// tslint:disable-next-line:no-namespace
declare namespace NodeJS {
  // tslint:disable-next-line:no-empty-interface
  export interface ReadableStream {}
}
