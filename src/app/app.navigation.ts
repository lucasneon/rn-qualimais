import { FinancialNavigation } from './modules/ financial-statement/financial.navigation';
import RNExitApp from 'react-native-exit-app';
import { Container } from 'typedi';
import { EmergencyButton } from '@app/components/atm.emergency-button';
import { FlashMessage } from '@app/components/obj.flash-message';
import { Navigation } from '@app/core/navigation';
import { AccountNavigation, AccountPage } from '@app/modules/account';
import { AuthenticationNavigation } from '@app/modules/authentication';
import {
  CustomerCareNavigation,
  CustomerCarePage,
} from '@app/modules/customer-care';
import { EmergencyNavigation } from '@app/modules/emergency';
import { HabitNavigation } from '@app/modules/habit';
import { VoucherNavigation } from '@app/modules/voucher';
import { HealthNavigation, HealthPage } from '@app/modules/health';
import { HomeNavigation, HomePage } from '@app/modules/home';
import { MockNavigation } from '@app/modules/mock-links';
import { OnboardNavigation } from '@app/modules/onboard';
import { SplashPage } from '@app/modules/splash';
import { Asset, Color } from '@atomic';
import {
  CommonNavigation,
  PlatformAuthenticationService,
  GeneralStrings,
} from './modules/common';
import { PharmacyNavigation } from './modules/pharmacy/pharmacy.navigation';

export const bootstrapNavigation = (): Promise<any> => {
  const platformAuth: PlatformAuthenticationService = Container.get(
    PlatformAuthenticationService,
  );

  AccountNavigation.register();
  AuthenticationNavigation.register();
  CommonNavigation.register();
  CustomerCareNavigation.register();
  HabitNavigation.register();
  VoucherNavigation.register();
  HealthNavigation.register();
  HomeNavigation.register();
  MockNavigation.register();
  EmergencyNavigation.register();
  Navigation.register(EmergencyButton, false);
  OnboardNavigation.register();
  Navigation.register(FlashMessage, false);
  PharmacyNavigation.register();
  FinancialNavigation.register();

  Navigation.setTopBarHeight();

  return new Promise(resolve => {
    Navigation.events().registerAppLaunchedListener(async () => {
      try {
        await platformAuth.checkAuthentication();
      } catch (error) {
        RNExitApp.exitApp();
      }

      Navigation.setDefaultOptions({
        layout: {
          orientation: ['portrait'],
        },
        topBar: {
          transluscent: true,
          transparent: true,
          leftButtonColor: Color.Primary,
          elevation: 0,
          noBorder: true,
          drawBehind: true,
          backButton: {
            icon: Asset.Icon.NavBar.Back,
            showTitle: false,
            color: Color.Primary,
            visible: true,
          },
          background: {
            translucent: true,
            color: 'transparent',
          },
          // rightButtons: [
          //   {
          //     id: 'EmergencyButton',
          //     component: { name: 'EmergencyButton' },
          //   },
          // ],
        },
        bottomTab: {
          selectedIconColor: Color.BlueDark,
          selectedTextColor: Color.BlueDark,
        },
        bottomTabs: {
          titleDisplayMode: 'alwaysShow',
        },
      });

      Navigation.setBottomTabs([
        {
          component: HomePage,
          title: GeneralStrings.BottomTabs.HomePage,
          icon: Asset.Icon.TabBar.Care,
        },
        {
          component: HealthPage,
          title: GeneralStrings.BottomTabs.HealthPage,
          icon: Asset.Icon.TabBar.Health,
        },
        {
          component: CustomerCarePage,
          title: GeneralStrings.BottomTabs.CustomerCarePage,
          icon: Asset.Icon.TabBar.CustomerCare,
        },
        {
          component: AccountPage,
          title: GeneralStrings.BottomTabs.AccountPage,
          icon: Asset.Icon.TabBar.Account,
        },
      ]);

      resolve(null);
    });
  });
};
