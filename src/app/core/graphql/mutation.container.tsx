import { ApolloError } from 'apollo-client';
import { DocumentNode } from 'graphql';
import * as React from 'react';
import { Mutation as ApolloMutation, MutationFn, MutationResult, OperationVariables } from 'react-apollo';
import { Container } from 'typedi';
import { GraphQLDocsService } from './graphql-docs.service';

export interface MutationProps {
  document: string;
  onCompleted?: (data: any) => void;
  onError?: (error: ApolloError) => void;
  children: (mutateWithVariables: OperationVariables, result: MutationResult, mutate: MutationFn) => React.ReactNode;
}

export declare type MutationVariablesFn<TVariables = OperationVariables> = (variables: TVariables) => void;

export class MutationContainer extends React.Component<MutationProps, any> {

  graphqlDocument: DocumentNode = null;
  mutationFn: MutationFn = null;

  constructor(props: MutationProps) {
    super(props);
    this.graphqlDocument = Container.get(GraphQLDocsService).getDocument(
      props.document.endsWith('.mutation') ? props.document : props.document + '.mutation',
    );
  }

  variablesFn: MutationVariablesFn = (variables: OperationVariables) => {
    this.mutationFn({variables});
  }

  render() {
    const { document, ...props } = this.props;
    return (
      <ApolloMutation mutation={this.graphqlDocument} {...props}>
        {(mutationFn: MutationFn, data: MutationResult) => {
          this.mutationFn = mutationFn;
          return this.props.children(this.variablesFn, data, mutationFn);
        }}
      </ApolloMutation>
    );
  }
}
