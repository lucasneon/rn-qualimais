import { DocumentNode } from 'graphql';
import { Service } from 'typedi';

@Service()
export class GraphQLDocsService {

  private graphQLDocs : object;
  private queryPath   : string;
  private mutationPath: string;

  configure(queryPath: string, mutationPath: string, docs: object) {
    this.queryPath    = queryPath;
    this.mutationPath = mutationPath;
    this.graphQLDocs  = docs;
  }

  getDocument(documentName: string): DocumentNode {
    if (documentName.indexOf('.query') !== -1) {
      return this.getQuery(documentName);
    } else if (documentName.indexOf('.mutation') !== -1) {
      return this.getMutation(documentName);
    }

    throw(new Error('GraphQL document not found. Verify document name'));
  }

  private getQuery(queryName: string): DocumentNode {
    const key = `${this.queryPath}/${queryName}.graphql`;
    return this.graphQLDocs[key];
  }

  private getMutation(mutationName: string): DocumentNode {
    const key = `${this.mutationPath}/${mutationName}.graphql`;
    return this.graphQLDocs[key];
  }

}
