import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { ContextSetter, setContext } from 'apollo-link-context';
import { createHttpLink } from 'apollo-link-http';
import { Service } from 'typedi';

@Service()
export class ApolloClientBuilderService {

  build(url: string, middleware?: ContextSetter, errorMiddleware?: ApolloLink): ApolloClient<NormalizedCacheObject> {
    let chainedLink;

    const httpLink = createHttpLink({ uri: url });
    chainedLink = httpLink;

    if (middleware) {
      chainedLink = setContext(middleware).concat(chainedLink);
    }

    if (errorMiddleware) {
      chainedLink = errorMiddleware.concat(chainedLink);
    }

    const client = new ApolloClient({ link: chainedLink, cache: new InMemoryCache() });

    return client;
  }

}
