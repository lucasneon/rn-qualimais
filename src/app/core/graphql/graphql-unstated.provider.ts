import { NormalizedCacheObject } from 'apollo-cache-inmemory';
import { ApolloClient, ApolloQueryResult, FetchPolicy } from 'apollo-client';
import { Container as TypeDIContainer, Service, Token } from 'typedi';
import { Container } from 'unstated';
import {
  ApolloClientKey,
  ApolloClientMockKey,
} from '@app/config/tokens.config';
import { GraphQLDocsService } from '@app/core/graphql';

export interface GraphQLInterface {
  called: boolean;
  loading: boolean;
  res: any;
  error: any;
}

export const GraphQLProviderToken = new Token<GraphQLProvider>();

const DefaultOptions = { fetchPolicy: 'no-cache' as FetchPolicy };

@Service(GraphQLProviderToken)
export class GraphQLProvider extends Container<GraphQLInterface> {
  private readonly documents: GraphQLDocsService =
    TypeDIContainer.get(GraphQLDocsService);
  private readonly client: ApolloClient<NormalizedCacheObject> =
    TypeDIContainer.get(ApolloClientKey);
  private readonly mockClient: ApolloClient<NormalizedCacheObject> =
    TypeDIContainer.get(ApolloClientMockKey);

  constructor() {
    super();
    this.state = { called: false, loading: false, res: null, error: null };
  }

  async makeQuery<R, V = null>(
    doc: string,
    variables?: V,
    options = DefaultOptions,
  ): Promise<R> {
    const query = this.getDocument(doc, '.query');
    const res: ApolloQueryResult<R> = await this.client.query<R, V>({
      query,
      variables,
      ...options,
    });

    return res.data;
  }

  mutate<V>(
    documentName: string,
    variables: V,
    successFn?,
    errorFn?,
    mock?: boolean,
    options = DefaultOptions,
  ) {
    const mutation = this.getDocument(documentName, '.mutation');
    const client = mock ? this.mockClient : this.client;
    this.performOperation(
      client.mutate({ mutation, variables, ...options }),
      successFn,
      errorFn,
    );
  }

  query<V>(
    documentName: string,
    variables: V,
    successFn?: any,
    errorFn?,
    options = DefaultOptions,
  ) {
    const query = this.getDocument(documentName, '.query');
    this.performOperation(
      this.client.query({ query, variables, ...options }),
      successFn,
      errorFn,
    );
  }

  private async performOperation(operation: Promise<any>, successFn, errorFn) {
    try {
      this.setState({ loading: true });
      const res = await operation;
      this.setState({ loading: false, res, error: null });

      if (successFn) {
        successFn(res.data);
      }
    } catch (error) {
      this.setState({ loading: false, error });

      if (errorFn) {
        errorFn(error);
        console.warn(error);
      }
    }
  }

  private getDocument(documentName: string, type: '.query' | '.mutation') {
    const document = documentName.endsWith(type)
      ? documentName
      : documentName + type;
    return this.documents.getDocument(document);
  }
}
