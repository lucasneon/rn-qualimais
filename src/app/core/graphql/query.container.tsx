import { ErrorPolicy, FetchPolicy, NetworkStatus } from 'apollo-client';
import { DocumentNode } from 'graphql';
import * as React from 'react';
import { OperationVariables, Query as ApolloQuery, QueryResult } from 'react-apollo';
import { Container } from 'typedi';
import { ApolloClientMockKey } from '@app/config';
import { GraphQLDocsService } from './graphql-docs.service';

export interface QueryProps {
  document: string;
  variables?: OperationVariables;
  children: (result: QueryResult) => React.ReactNode;
  fetchPolicy?: FetchPolicy;
  errorPolicy?: ErrorPolicy;
  notifyOnNetworkStatusChange?: boolean;
  pollInterval?: number;
  mock?: boolean;
  ssr?: boolean;
  skip?: boolean;
  context?: Record<string, any>;
}

interface QueryContainerState {
  queryResult: QueryResult;
}

export class QueryContainer extends React.Component<QueryProps, QueryContainerState> {

  graphqlDocument: DocumentNode = null;

  constructor(props: QueryProps) {
    super(props);
    this.state = { queryResult: null };
    this.graphqlDocument = Container.get(GraphQLDocsService).getDocument(
      props.document.endsWith('.query') ? props.document : props.document + '.query',
    );
  }

  render() {
    return (
      <ApolloQuery
        query={this.graphqlDocument}
        notifyOnNetworkStatusChange={true}
        fetchPolicy={this.props.fetchPolicy || 'no-cache'}
        client={this.props.mock && Container.get(ApolloClientMockKey)}
        {...this.props}
      >
        {(queryResult: QueryResult) => {
          queryResult.loading = queryResult.loading || queryResult.networkStatus === NetworkStatus.refetch;

          if (queryResult.loading) {
            queryResult.error = null;
            queryResult.data = {};
          }

          return this.props.children(queryResult);
        }}
      </ApolloQuery>
    );
  }
}
