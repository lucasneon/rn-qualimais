import * as React from 'react';
import { BackHandler, Platform } from 'react-native';
import RNExitApp from 'react-native-exit-app';
import { Navigation } from '@app/core/navigation';

export function forbidBackPage(WrappedPage) {
  return class extends React.Component<any, any> {
    static options = WrappedPage.options;

    constructor(props) {
      super(props);
      Navigation.events().bindComponent(this);
    }

    componentDidMount() {
      Navigation.mergeOptions(this.props.componentId, {
        topBar: { leftButtons: [] },
        hardwareBackButton: {
          dismissModalOnPress: false,
        },
      });

      if (Platform.OS === 'android') {
        Navigation.mergeOptions(this.props.componentId, {
          topBar: { backButton: { color: 'transparent' } },
        });
      }
    }

    componentDidAppear() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackTap);
    }

    componentDidDisappear() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackTap);
    }

    render() {
      return <WrappedPage {...this.props} />;
    }

    private handleBackTap = () => {
      RNExitApp.exitApp();
    };
  };
}
