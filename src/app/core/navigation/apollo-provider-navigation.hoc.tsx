import { NormalizedCacheObject } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import * as React from 'react';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { Container as TypeDIContainer } from 'typedi';
import { Provider } from 'unstated';
import { ProvidersConfig } from '@app/config';
import { ApolloClientKey } from '@app/config/tokens.config';
import { NavigationGuard } from './navigation.guard';

export const navigationWithApolloProvider = (Component: any, Guard?: typeof NavigationGuard) => {

  const client: ApolloClient<NormalizedCacheObject> = TypeDIContainer.get(ApolloClientKey);
  const providers = TypeDIContainer.get<ProvidersConfig>(ProvidersConfig).providers;

  return class extends React.Component<any, any> {

    static options = {
      ...Component.options,
    };

    private ref;

    componentDidAppear() {
      if (this.ref && this.ref.componentDidAppear) {
        this.ref.componentDidAppear();
      }
    }

    componentDidDisappear() {
      if (this.ref && this.ref.componentDidDisappear) {
        this.ref.componentDidDisappear();
      }
    }

    onNavigationButtonPressed(buttonId: string) {
      if (this.ref && this.ref.onNavigationButtonPressed) {
        this.ref.onNavigationButtonPressed(buttonId);
      }
    }

    withGuard = () => {
      return Guard ? (
        <Guard>
          {
            (authorized, params) => {
              return <Component ref={ref => this.ref = ref} guard={{ authorized, params }} {...this.props} />;
            }
          }
        </Guard>
      ) : <Component ref={ref => this.ref = ref} {...this.props} />;
    }

    render() {
      return (
        <ApolloProvider client={client}>
          <Provider inject={providers}>
            <this.withGuard />
          </Provider>
        </ApolloProvider>
      );
    }
  };
};
