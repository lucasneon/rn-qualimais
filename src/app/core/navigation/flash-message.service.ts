import { FlashMessage, FlashMessageType } from '@app/components/obj.flash-message';
import { AppError } from '@app/model';
import { Navigation } from './navigation';

const navigationOptions = {
  overlay: { interceptTouchOutside: false },
};

export class FlashMessageService {
  static showSuccess = async (message: string) =>
    await Navigation.showOverlay(FlashMessage, { type: FlashMessageType.Success, message }, navigationOptions)

  static showError = async (appError: AppError) =>
    await Navigation.showOverlay(FlashMessage, { type: FlashMessageType.Error, error: appError }, navigationOptions)

  static showErrorMessage = async (message: string) =>
    await Navigation.showOverlay(FlashMessage, { type: FlashMessageType.Error, message }, navigationOptions)
}
