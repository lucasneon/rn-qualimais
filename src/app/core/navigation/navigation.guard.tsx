import * as React from 'react';

export interface GuardProps<P> {
  guard?: {
    authorized: boolean;
    params: P;
  };
}
interface NavigationGuardProps<P> {
  children: (authorized: boolean, params?: P) => any;
}

export class NavigationGuard<P> extends React.Component<NavigationGuardProps<P>, any> {}
