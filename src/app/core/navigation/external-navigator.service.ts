import { Linking, PermissionsAndroid, Platform } from 'react-native';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import { showLocation } from 'react-native-map-link';
import { Service } from 'typedi';
import { GeoCoordinates } from '@app/model';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { GeneralStrings } from '@app/modules/common';

@Service()
export class ExternalNavigator {
  openAppSettings() {
    Linking.openSettings();
  }

  openEmail(to: string) {
    return this.openUrl(`mailto:${to}`);
  }

  openWebsite(website: string) {
    return this.openUrl(website);
  }

  openMaps(coordinates: GeoCoordinates): Promise<boolean> {
    const baseUrlAndroid = `https://www.google.com/maps/search/?api=1&query=`;

    return (
      Platform.OS === 'android' ? this.openUrl(`${baseUrlAndroid}${coordinates.latitude},${coordinates.longitude}`) :
      showLocation({
        latitude: coordinates.latitude,
        longitude: coordinates.longitude,
      })
    );
  }

  async makeCall(phone: string, immediate: boolean = false) {
    if (Platform.OS === 'ios') {
      RNImmediatePhoneCall.immediatePhoneCall(phone);
      return;
    }

    const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CALL_PHONE);

    if (immediate && granted) {
      RNImmediatePhoneCall.immediatePhoneCall(phone);
    } else {
      await this.openUrl('tel:' + phone);
    }
  }

  private async openUrl(url: string): Promise<boolean> {
    try {
      const supported: boolean = await Linking.canOpenURL(url);

      if (supported) {
        await Linking.openURL(url);
      } else {
        FlashMessageService.showErrorMessage(GeneralStrings.ErrorOpenUrl);
        console.warn(`Link ${url} not supported.`);
      }

      return supported;
    } catch (error) {
      console.error('An error occurred:', error);
      return false;
    }
  }
}
