import { Navigation as ReactNativeNavigation } from 'react-native-navigation';
import { Asset, Color } from '@atomic';
import { navigationWithApolloProvider } from './apollo-provider-navigation.hoc';

export interface BottomTab {
  component: NavigationPage;
  title: string;
  icon: any;
}

export interface NavigationPageProps<T> {
  componentId?: string;
  navigation?: T;
}

export type NavigationPage = React.ComponentType<any> & {
  options: { name: string };
};

export class Navigation {
  static TopBarHeight: number;

  static events() {
    return ReactNativeNavigation.events();
  }

  static mergeOptions(componentId: string, options: any) {
    ReactNativeNavigation.mergeOptions(componentId, options);
  }

  static setDefaultOptions(options: any) {
    ReactNativeNavigation.setDefaultOptions(options);
  }

  static dismissModal(componentId: string): Promise<any> {
    return ReactNativeNavigation.dismissModal(componentId);
  }

  static dismissAllModals(): Promise<any> {
    return ReactNativeNavigation.dismissAllModals();
  }

  static dismissOverlay(componentId: string): Promise<any> {
    return ReactNativeNavigation.dismissOverlay(componentId);
  }

  static push<T>(
    componentId: string,
    component: NavigationPage,
    passProps?: T,
    options?: any,
  ): Promise<any> {
    return ReactNativeNavigation.push(componentId, {
      component: {
        name: component.options.name,
        passProps: { navigation: passProps },
        options,
      },
    });
  }

  static pop(componentId: string, params?: any): Promise<any> {
    return ReactNativeNavigation.pop(componentId, params);
  }

  static popTo(componentId: string): Promise<any> {
    return ReactNativeNavigation.popTo(componentId);
  }

  static popToRoot(componentId: string): Promise<any> {
    return ReactNativeNavigation.popToRoot(componentId);
  }

  static setRoot(layout: any): Promise<any> {
    return ReactNativeNavigation.setRoot(layout);
  }

  static setBottomTabs(bottomTabs: BottomTab[], options: any = {}) {
    const children = bottomTabs.map((tab: BottomTab) => {
      return {
        stack: {
          children: [
            {
              component: {
                name: tab.component.options.name,
                id: tab.component.options.name,
              },
            },
          ],
          options: {
            bottomTab: {
              text: tab.title,
              icon: tab.icon,
            },
          },
        },
      };
    });

    ReactNativeNavigation.setRoot({
      root: { bottomTabs: { children, options } },
    });
  }

  static setCurrentTab(componentId: string, tabIndex: number) {
    this.mergeOptions(componentId, {
      bottomTabs: { currentTabIndex: tabIndex },
    });
  }

  static resetAndSelectTab(id: string, num: number) {
    this.setCurrentTab(id, num);
    this.popToRoot(id);
  }

  static register(
    Component: NavigationPage,
    withApollo: boolean = true,
    guard?: any,
  ) {
    if (!Component.options || (Component.options && !Component.options.name)) {
      throw Error(
        'Registered navigation pages must have the static options = { name: "PageName" } property',
      );
    }

    ReactNativeNavigation.registerComponent(Component.options.name, () =>
      withApollo ? navigationWithApolloProvider(Component, guard) : Component,
    );
  }

  static showModal<T>(
    component: NavigationPage,
    passProps?: T,
    options?: any,
  ): Promise<any> {
    return ReactNativeNavigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: component.options.name,
              passProps: { navigation: passProps },
              options: {
                topBar: {
                  leftButtons: [
                    {
                      id: 'close',
                      icon: Asset.Icon.NavBar.Close,
                      color: Color.Primary,
                    },
                  ],
                  backButton: { visible: false },
                },
                ...options,
              },
            },
          },
        ],
      },
    });
  }

  static showOverlay<T>(
    component: NavigationPage,
    passProps?: T,
    options: any = {},
  ): Promise<any> {
    return ReactNativeNavigation.showOverlay({
      component: {
        name: component.options.name,
        passProps: { navigation: passProps },
        options: {
          layout: {
            componentBackgroundColor: 'transparent',
          },
          overlay: {
            interceptTouchOutside: true,
          },
          ...options,
        },
      },
    });
  }

  static setBadge(componentId: string, text: string) {
    Navigation.mergeOptions(componentId, {
      bottomTab: {
        badge: text,
        badgeColor: Color.Alert,
      },
    });
  }

  static async setTopBarHeight() {
    const navigationConstants: any = await ReactNativeNavigation.constants();
    Navigation.TopBarHeight = navigationConstants.topBarHeight;
  }
}
