import Container, { Service } from 'typedi';
import { FirebaseAnalytics } from './firebase.analytics';

@Service()
export class Analytics {
  private readonly firebase: FirebaseAnalytics = Container.get(FirebaseAnalytics);

  pageView(pageName: string) {
    this.firebase.logPageView(pageName);
  }
}
