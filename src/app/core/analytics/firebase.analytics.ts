import analytics from '@react-native-firebase/analytics';
import { Service } from 'typedi';

// we couldn't find the real max value in their documentation, then we using this value that works
export const MAX_LENGTH_SCREEN_NAME = 70;

@Service()
export class FirebaseAnalytics {
  logPageView(screenName: string, parentScreenName?: string): void {
    screenName = screenName.slice(
      0,
      Math.min(screenName.length, MAX_LENGTH_SCREEN_NAME),
    );
    // analytics().logEvent(screenName, [
    //   {
    //     parentScreenName: parentScreenName,
    //   },
    // ]);
  }

  logEvent(event: string, params?: any): void {
    // analytics().logEvent(event, params);
  }

  setUserProperty(key: string, value: string): void {
    // analytics().setUserProperty(key, value);
  }
}
