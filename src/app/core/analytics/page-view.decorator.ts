import { Container } from 'typedi';
import { Analytics } from './analytics.service';
import { Navigation } from '../navigation';

interface ConstructorType {
  new(...args: any[]);
}
/**
 * # PageView
 *
 * ## Description
 *
 * Decorator used to log pageviews for analytics module
 *
 * Eg:
 *
 * Given the Decorated Page class:
 * @PageView('SomePage')
 * class SomePage {
 *   ...
 * }
 *
 * the SomePage class will send a page view track event on componentDidAppear
 * If there is the componentDidAppear method inside the wrapped class it will be called too
 *
 * Also if the page is not already using navigation events, it will bind the component to navigation events
 *
 */

export function PageView(pageName: string) {
  return <TargetType extends ConstructorType>(targetClass: TargetType) => {
    return class extends targetClass {
      private analytics: Analytics = null;

      constructor(...args: any[]) {
        super(...args);
        this.analytics = Container.get(Analytics);
        if (!(super.componentDidAppear || super.componentDidDisappear || super.navigationButtonPressed)) {
          Navigation.events().bindComponent(this as any);
        }
      }

      componentDidAppear() {
        if (super.componentDidAppear) {
          super.componentDidAppear();
        }
        this.analytics.pageView(pageName);
      }
    };
  };
}
