import { PermissionsAndroid } from 'react-native';
import { Service, Token } from 'typedi';
import { Container } from 'unstated';
import { GeoCoordinates } from '@app/model';
import { Platform } from '@atomic';
import Geolocation from '@react-native-community/geolocation';

export interface LocationError {
  permissionDenied: boolean;
}

export interface LocationState {
  fetching?: boolean;
  location?: GeoCoordinates;
  error?: LocationError;
}

export const LocationProviderToken = new Token<LocationProvider>();

export const DefaultCenter: GeoCoordinates = {
  latitude: -23.5505,
  longitude: -46.6333,
};

@Service(LocationProviderToken)
export class LocationProvider extends Container<LocationState> {
  constructor() {
    super();
    this.state = { fetching: null, location: null, error: null };
  }

  async handlePlatforms(callback?: Function) {
    this.setState({ fetching: true, error: null });
    if (Platform.OS === 'android') {
      await this.handleAndroid(callback);
    } else {
      await this.fetchPosition(callback);
    }
  }

  private async handleAndroid(callback: Function) {
    const granted = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (granted) {
      this.fetchPosition(callback);
      return;
    }

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Permitir Localização',
        message: 'O aplicativo precisa de acesso a sua localização',
        buttonNeutral: 'Pergunte-me depois',
        buttonNegative: 'Cancelar',
        buttonPositive: 'OK',
      },
    );

    if (status === 'denied') {
      this.setState({
        fetching: false,
        location: null,
        error: { permissionDenied: true },
      });
    } else {
      this.fetchPosition(callback);
    }
  }

  private fetchPosition(callback: Function) {
    Geolocation.getCurrentPosition(
      location => {
        this.successHandler(location);
        if (callback) {
          callback(location);
        }
      },
      error => {
        this.errorHandler(error);
        if (callback) {
          callback(null);
        }
      },
    );
  }

  private successHandler = (location: any) => {
    const coords: GeoCoordinates = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    };
    this.setState({ fetching: false, location: coords });
  };

  private errorHandler = (error: any) => {
    const permissionDenied: boolean = error.code === error.PERMISSION_DENIED;
    this.setState({
      fetching: false,
      location: null,
      error: { permissionDenied },
    });
  };
}
