import {
  FamilyHistoryFragment,
  FamilyHistoryQuery,
  InsertFamilyHIstoryMutation,
  InsertFamilyHIstoryMutationVariables,
} from '@app/data/graphql';
import { execFamilyHistoryRequest } from '@app/modules/health/api/familyHistory/familyHistory.service';
import { Service, Token } from 'typedi';
import { MedicalRecordProvider } from './medical-record.provider';

export const FamilyHIstoryToken = new Token<FamilyHistoryProvider>();

@Service(FamilyHIstoryToken)
export class FamilyHistoryProvider extends MedicalRecordProvider<
  FamilyHistoryQuery,
  InsertFamilyHIstoryMutation,
  InsertFamilyHIstoryMutationVariables,
  FamilyHistoryFragment
> {
  constructor() {
    super(
      'familyHistory',
      'insert-family-history',
      'FamilyHistory',
      'InsertFamilyHistory',
    );
  }

  async fetch() {
    this.setState({ loading: true });
    const res = await execFamilyHistoryRequest();
    if (res?.error) {
      return this.onError(res.error);
    }
    this.setSuccess({ FamilyHistory: res.data });
  }
}
