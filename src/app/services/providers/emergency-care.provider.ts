import { fetchEmergencyCareFilterPoints } from '@app/modules/customer-care/api/emergency-care.service';
import { Service, Token } from 'typedi';
import { Container } from 'unstated';

const EmergencyCareProviderToken = new Token<EmergencyCareProvider>();

interface EmergencyCareProviderState {
  data: any;
  loading: boolean;
  error: any;
}

@Service(EmergencyCareProviderToken)
export class EmergencyCareProvider extends Container<EmergencyCareProviderState> {
  initialState: EmergencyCareProviderState = {
    data: null,
    loading: false,
    error: null,
  };
  constructor() {
    super();
    this.state = this.initialState;
  }

  fetch = async (data: any) => {
    this.setState({ loading: true });
    const res = await fetchEmergencyCareFilterPoints(data);
    this.setState({ loading: false, data: { data: { EmergencyCare: res } } });
  };
}
