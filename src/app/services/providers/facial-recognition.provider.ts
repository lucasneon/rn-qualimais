import { Container as TypeDIContainer, Service, Token } from 'typedi';
import { Container } from 'unstated';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { CheckFacialRecognitionRegisterQuery } from '@app/data/graphql';

export interface FacialRecognitionProviderState {
  isRegistered: boolean;
}

export const FacialRecognitionProviderToken = new Token<FacialRecognitionProvider>();

@Service(FacialRecognitionProviderToken)
export class FacialRecognitionProvider extends Container<FacialRecognitionProviderState> {
  private graphQLProvider: GraphQLProvider = TypeDIContainer.get(GraphQLProviderToken);

  constructor() {
    super();
    this.state = { isRegistered: null };
  }

  rehydrate() {
    this.state = { isRegistered: null };
    this.graphQLProvider.query('check-facial-recognition-register', null, this.handleSuccess, this.handleError);
  }

  private handleSuccess = (res: CheckFacialRecognitionRegisterQuery) => {
    this.setState({ isRegistered: res.CheckFacialRecognitionRegister });
  }

  private handleError = () => {
    this.setState({ isRegistered: null });
  }
}
