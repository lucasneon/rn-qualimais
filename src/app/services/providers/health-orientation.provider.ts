import { fetchQuestionaryFeedback } from '@app/modules/health/api/health-orientations/health-orientation.service';
import { Service, Token } from 'typedi';
import { Container } from 'unstated';

export const HealthOrientationToken = new Token<HealthOrientationProvider>();
export interface HealthOrientationProviderState {
  orientations: any[];
  loading: boolean;
  error: any;
}

@Service(HealthOrientationToken)
export class HealthOrientationProvider extends Container<HealthOrientationProviderState> {
  initialState = { orientations: [], loading: false, error: null };
  constructor() {
    super();
    this.state = this.initialState;
  }

  fetchOrientations = async () => {
    this.setState({ loading: true, orientations: null });
    try {
      const orientations = await fetchQuestionaryFeedback();
      console.warn('orientation', orientations);
      this.setState({ orientations, loading: false });
    } catch (error) {
      this.setState({ error, loading: false, orientations: null });
    }
  };

  refetch = () => {
    this.fetchOrientations();
  };
}
