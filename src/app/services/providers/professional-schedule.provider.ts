import { Service, Token } from 'typedi';
import { Container } from 'unstated';
import { fetchBySchedule } from '@app/modules/common/api/professional-schedule/professional-schedule.service';
import { ProfessionalScheduleModel } from '@app/modules/common/api/professional-schedule/professional-schedule.model';
import { ProfessionalScheduleFragment } from '@app/data/graphql';

export interface ProfessionalScheduleProviderState {
  schedule: ProfessionalScheduleFragment;
  loading: boolean;
  error: any;
}

export const ProfessionalScheduleProviderToken =
  new Token<ProfessionalScheduleProvider>();

@Service(ProfessionalScheduleProviderToken)
export class ProfessionalScheduleProvider extends Container<ProfessionalScheduleProviderState> {
  private initialState: ProfessionalScheduleProviderState = {
    schedule: null,
    loading: false,
    error: null,
  };
  constructor() {
    super();
    this.state = this.initialState;
  }

  fetchSchedule = async input => {
    this.setState({ loading: true });
    try {
      const schedule = await fetchBySchedule(input);
      this.setState({ schedule, loading: false });
    } catch (error) {
      this.setState({ error, loading: false });
    }
  };
}
