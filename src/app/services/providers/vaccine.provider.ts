import { Service, Token } from 'typedi';
import {
  InsertVaccineMutation,
  InsertVaccineMutationVariables,
  VaccineFragment,
  VaccinesQuery,
} from '@app/data/graphql';
import { MedicalRecordProvider } from './medical-record.provider';
import {
  addVaccine,
  vaccinesRequest,
} from '@app/modules/health/api/vaccines/vaccines.service';
import { ApolloError } from 'apollo-client';

export const VaccineProviderToken = new Token<VaccineProvider>();

@Service(VaccineProviderToken)
export class VaccineProvider extends MedicalRecordProvider<
  VaccinesQuery,
  InsertVaccineMutation,
  InsertVaccineMutationVariables,
  VaccineFragment
> {
  constructor() {
    super('vaccines', 'insert-vaccine', 'Vaccines', 'InsertVaccine');
  }

  async fetch() {
    this.setState({ loading: true });
    const res = await vaccinesRequest();
    if (res?.error) {
      return this.onError(res.error);
    }
    this.setSuccess({ Vaccines: res.data || [] });
  }

  insert = (
    data: any,
    succesFn: (resp: InsertVaccineMutation) => any,
    errorFn: (resp: ApolloError) => any,
  ) => {
    this.setState({ loading: true });
    addVaccine(data)
      .then(async res => {
        const vaccines = await vaccinesRequest();
        if (res?.error) {
          this.onMutationError(res.error);
          errorFn(res.error);
          this.setState({ loading: false });
        } else {
          this.onMutationSuccess({ InsertVaccine: vaccines });
          succesFn({ InsertVaccine: vaccines });
          this.setState({ loading: false });
        }
      })
      .catch(error => {
        this.onMutationError(error);
        errorFn(error);
        this.setState({ loading: false });
      });
  };
}
