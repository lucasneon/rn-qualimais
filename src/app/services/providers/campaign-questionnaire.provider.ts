import { ApolloError } from 'apollo-client';
import _ from 'lodash';
import moment from 'moment';
import { Container as TypeDIContainer } from 'typedi';
import { Container } from 'unstated';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import {
  AnswerFragment,
  AnswerQuestionnaireMutation,
  CampaignFragment,
  CampaignQuestionnairesInput,
  CampaignQuestionnairesQuery,
  CampaignQuestionnairesQueryVariables,
  QuestionFragment,
  QuestionGroupFragment,
  QuestionGroupInput,
  QuestionInput,
  QuestionnaireFragment,
  QuestionnaireSummaryFragment,
  QuestionType,
} from '@app/data/graphql';
import { ImcQuestionsCodes } from '@app/modules/health/questionnaire/imc-questions.service';
import {
  answerQuestionnaire,
  execGetQuestionnaires,
} from '@app/modules/health/api/campaign-questionnaires/campaign-questionnaires.service';

export interface CampaignQuestionnaireContainerProps {
  campaignData: CampaignFragment;
}

export interface CampaignQuestionnaireContainerState {
  questionnaires: QuestionnaireFragment[];
  loading: boolean;
  error: ApolloError;
}

export class CampaignQuestionnaireProvider extends Container<CampaignQuestionnaireContainerState> {
  private graph: GraphQLProvider = TypeDIContainer.get(GraphQLProviderToken);

  constructor() {
    super();
    this.state = { questionnaires: null, error: null, loading: false };
  }

  async fetchQuestionnaires(campaignData: CampaignFragment) {
    this.setState({ loading: true });

    const mapQuestionnaireInput = (summary: QuestionnaireSummaryFragment) => ({
      questionnaireCode: summary.code,
      responseCode: summary.responseCode,
    });
    const data: CampaignQuestionnairesInput = {
      campaignCode: campaignData.code,
      questionnaires: campaignData.questionnaires.map(mapQuestionnaireInput),
    };
    try {
      const res = await execGetQuestionnaires(data);
      if (res.error) {
        this.onQueryError(res.error);
      }
      this.onQuerySuccess({ CampaignQuestionnaires: res });
    } catch (e) {
      this.onQueryError(e);
    }
    // this.graph.query<CampaignQuestionnairesQueryVariables>(
    //   'campaign-questionnaires',
    //   { data },
    //   this.onQuerySuccess,
    //   this.onQueryError,
    // );
  }

  answerQuestionGroup = async (
    campaignCode: string,
    questionnaireCode: string,
    questionGroup: QuestionGroupFragment,
    successFn: (resp: AnswerQuestionnaireMutation | any) => void,
    errorFn: (error: ApolloError | any) => void,
  ): Promise<void> => {
    const questionnaire = this.getQuestionnaire(questionnaireCode);

    if (questionnaire.imcRequired) {
      const firstQuestion = _.get(questionGroup, 'questions[0]');
      const firstQuestionAnswer = _.get(
        questionGroup,
        'questions[0].chosenAnswers[0].display',
      );

      if (
        firstQuestion &&
        firstQuestion.code === ImcQuestionsCodes.weight &&
        firstQuestionAnswer
      ) {
        questionnaire.weight = Number(firstQuestionAnswer);
        questionnaire.height =
          Number(
            _.get(questionGroup, 'questions[1].chosenAnswers[0].display'),
          ) || null;
      }
    }

    const questionGroupInputs: QuestionGroupInput[] = this.getNewQuestionGroups(
      questionnaire,
      questionGroup,
    ).map<QuestionGroupInput>(group => {
      const questionsWithoutImc = group.questions.filter(
        (question: QuestionFragment) =>
          question.code !== ImcQuestionsCodes.height &&
          question.code !== ImcQuestionsCodes.weight,
      );

      return {
        code: group.code,
        questions: questionsWithoutImc
          .filter(
            question => question.chosenAnswers && question.chosenAnswers.length,
          )
          .map<QuestionInput>(this.questionMap)
          .filter(
            quesiton =>
              quesiton.typedAnswer ||
              (quesiton.chosenAnswers && quesiton.chosenAnswers.length > 0),
          ),
      };
    });
    const data = {
      weight: questionnaire.weight,
      height: questionnaire.height,
      campaignCode,
      questionnaireCode,
      responseCode: questionnaire.responseCode,
      questionGroups: questionGroupInputs,
    };
    answerQuestionnaire(data)
      .then(res => {
        successFn(res);
      })
      .catch(e => {
        errorFn({ error: e.responseCode });
      });
    // this.graph.mutate<AnswerQuestionnaireMutationVariables>(
    //   'answer-questionnaire',
    //   {
    //     data: {
    //       weight: questionnaire.weight,
    //       height: questionnaire.height,
    //       campaignCode,
    //       questionnaireCode,
    //       responseCode: questionnaire.responseCode,
    //       questionGroups: questionGroupInputs,
    //     },
    //   },
    //   (data: AnswerQuestionnaireMutation) => successFn(data),
    //   (error: ApolloError) => errorFn(error),
    // );
  };

  private onQuerySuccess = (resp: CampaignQuestionnairesQuery) => {
    this.setState({
      questionnaires: resp.CampaignQuestionnaires,
      loading: false,
    });
  };

  private onQueryError = (error: ApolloError) => {
    this.setState({ error, loading: false });
  };

  private getQuestionnaire = (questionnaireCode: string) => {
    // object from apollo query is frozen, needs deep copy
    const questionnaires = _.cloneDeep(this.state.questionnaires);
    const questionnaireIndex = questionnaires.findIndex(
      (questionnaire: QuestionnaireFragment) =>
        questionnaire.code === questionnaireCode,
    );
    return questionnaires[questionnaireIndex];
  };

  private getNewQuestionGroups = (
    tempQuestionnaire: QuestionnaireFragment,
    questionGroup: QuestionGroupFragment,
  ) => {
    const questionGroupIndex = tempQuestionnaire.questionGroups.findIndex(
      (group: QuestionGroupFragment) => group.code === questionGroup.code,
    );

    tempQuestionnaire.questionGroups[questionGroupIndex] = questionGroup;

    return tempQuestionnaire.questionGroups.filter(
      (group: QuestionGroupFragment) =>
        group.questions.some(
          it => it.chosenAnswers && it.chosenAnswers.length > 0,
        ),
    );
  };

  private questionMap = (question: QuestionFragment): QuestionInput => {
    const hasCode =
      question.type === QuestionType.OneChoice ||
      question.type === QuestionType.MultipleChoices;
    if (question.type === QuestionType.Date) {
      question.chosenAnswers[0].display = moment(
        new Date(question.chosenAnswers[0].display),
      ).format('YYYY-MM-DD');
    }
    const answers = question.chosenAnswers;
    const answerInput = hasCode
      ? {
          chosenAnswers: answers.map(
            (answer: AnswerFragment): string => answer.code,
          ),
        }
      : { typedAnswer: answers[0].display };
    return {
      code: question.code,
      type: question.type,
      ...answerInput,
    };
  };
}
