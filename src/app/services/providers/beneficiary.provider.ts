import { Container as TypeDIContainer, Service, Token } from 'typedi';
import { Container } from 'unstated';
import { LocalDataSource } from '@app/data/local';
import { Beneficiary } from '@app/model';
import { fetchAuthBeneficiaryInfo } from '@app/modules/authentication/api/login/beneficiary.service';
import { BeneficiaryInfoModel } from '@app/model/api/beneficiary-info.model';
import moment from 'moment';
import { MedicationMapper } from '@app/modules/home/api/medications/medication.mapper';

export interface BeneficiaryProviderState {
  active: Beneficiary;
  beneficiaries: Beneficiary[];
  beneficiaryInfo: BeneficiaryInfoModel;
  error: any;
  loading: boolean;
}
const localDataSource: LocalDataSource = TypeDIContainer.get(LocalDataSource);
export const BeneficiaryProviderToken = new Token<BeneficiaryProvider>();

const LOCAL_STORAGE_BENEFICIARY_DATA = 'sessionStore-beneficiaryData';
const LOCAL_STORAGE_ACTIVE_BENEFICIARY_DATA =
  'sessionStore-activeBeneficiaryData';

@Service(BeneficiaryProviderToken)
export class BeneficiaryProvider extends Container<BeneficiaryProviderState> {
  isLoginActive: boolean;

  private readonly localDataSource: LocalDataSource =
    TypeDIContainer.get(LocalDataSource);
  private readonly initialState = {
    active: null,
    beneficiaries: null,
    beneficiaryInfo: null,
    error: null,
    loading: false,
  };

  private rehydration: Promise<void>;

  constructor() {
    super();
    this.state = this.initialState;
  }

  async rehydrate(): Promise<void> {
    if (!this.rehydration) {
      this.rehydration = this.fetchLocalStorage();
    }

    return this.rehydration;
  }

  async logout(): Promise<void> {
    const keys = [
      LOCAL_STORAGE_BENEFICIARY_DATA,
      LOCAL_STORAGE_ACTIVE_BENEFICIARY_DATA,
    ];
    await this.localDataSource.removeMulti(keys);
    this.setState(this.initialState);
  }

  getBeneficiaryAge() {
    const birthDate = this.state?.active?.birthDate;
    if (birthDate) {
      return moment().diff(birthDate, 'years');
    }
  }

  async setBeneficiaries(active: Beneficiary, beneficiaries: Beneficiary[]) {
    await Promise.all([
      this.localDataSource.set<Beneficiary>(
        LOCAL_STORAGE_ACTIVE_BENEFICIARY_DATA,
        active,
      ),
      this.localDataSource.set<Beneficiary[]>(
        LOCAL_STORAGE_BENEFICIARY_DATA,
        beneficiaries,
      ),
    ]);

    this.setState({ active, beneficiaries });
  }

  async setActive(
    beneficiary: Beneficiary,
    onFinish?: () => void,
  ): Promise<void> {
    await this.localDataSource.set<Beneficiary>(
      LOCAL_STORAGE_ACTIVE_BENEFICIARY_DATA,
      beneficiary,
    );
    this.setState({ active: beneficiary }, onFinish);
  }

  async getActiveBeneficiaryToken(): Promise<string | null> {
    if (!this.state.active) {
      await this.rehydrate();
    }

    return this.state.active && this.state.active.token;
  }

  isLogged(): boolean {
    return !!this.state.active;
  }

  private async fetchLocalStorage(): Promise<void> {
    const [active, beneficiaries] = await Promise.all([
      this.localDataSource.get<Beneficiary>(
        LOCAL_STORAGE_ACTIVE_BENEFICIARY_DATA,
      ),
      this.localDataSource.get<Beneficiary[]>(LOCAL_STORAGE_BENEFICIARY_DATA),
    ]);

    this.setState({ active, beneficiaries });
  }

  async isHolderLogin() {
    const activeBeneficiary = this.state?.active;
    const titularbeneficiaryCode = await localDataSource.get<string>(
      'beneficiaryCode',
    );
    console.log(
      'activeBeneficiaryCode, titularbeneficiaryCode',
      activeBeneficiary.code,
      titularbeneficiaryCode,
    );
    if (activeBeneficiary.code !== titularbeneficiaryCode) {
      return true;
    }
    return false;
  }

  async getBeneficiaryinfo() {
    this.setState({ loading: true });
    const beneficiaryCode: string = await localDataSource.get<string>(
      'beneficiaryCode',
    );
    const { data } = await fetchAuthBeneficiaryInfo(beneficiaryCode);
    const beneficiary: BeneficiaryInfoModel = data;
    this.setState({ beneficiaryInfo: beneficiary, loading: false });
  }
}
