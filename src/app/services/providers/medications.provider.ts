import { ApolloError } from 'apollo-client';
import moment from 'moment';
import { Container as TypeDIContainer, Service, Token } from 'typedi';
import { Container } from 'unstated';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import {
  ListMedicationInput,
  ListMedicationQuery,
  MedicationInput,
  MedicationListItemFragment,
} from '@app/data/graphql';
import {
  CancelMedicationNotificationsUseCase,
  ScheduleMedicationNotificationsUseCase,
} from '@app/domain/local-notification';
import { AppError, Medication } from '@app/model';
import { ErrorMapper } from '@app/utils';
import {
  fetchCreateMedication,
  fetchMedications,
} from '@app/modules/health/api/medication/medication.service';

export const MedicationsProviderToken = new Token<MedicationsProvider>();

export interface MedicationsProviderState {
  loading?: boolean;
  error?: AppError;
  activeMedications?: MedicationListItemFragment[];
  allMedications?: MedicationListItemFragment[];
}

export interface ActivateMedicationParams {
  medicationCode: string;
  medicationTypeCode: string;
  successFn: (message: string) => void;
  errorFn: (error: AppError) => void;
}

@Service(MedicationsProviderToken)
export class MedicationsProvider extends Container<MedicationsProviderState> {
  private readonly initialState = {
    loading: null,
    activeMedications: null,
    allMedications: null,
    error: null,
  };

  private readonly graphQLProvider: GraphQLProvider =
    TypeDIContainer.get(GraphQLProviderToken);
  private readonly scheduleNotificationsUseCase = TypeDIContainer.get(
    ScheduleMedicationNotificationsUseCase,
  );
  private readonly canceNotificationsUseCase = TypeDIContainer.get(
    CancelMedicationNotificationsUseCase,
  );

  constructor() {
    super();
    this.state = this.initialState;
  }

  create = (
    input: MedicationInput,
    filters: ListMedicationInput,
    successFn: () => void,
    errorFn: (error: ApolloError) => void,
  ) => {
    this.setState({ loading: true, error: null });
    fetchCreateMedication(input)
      .then(async res => {
        if (res?.error) {
          this.handleCreateError(res.error, errorFn);
        } else {
          const medications = await fetchMedications({});
          this.handleCreateSuccess(medications.data, successFn);
        }
      })
      .catch(error => {
        console.error(error);
        this.handleCreateError(error, errorFn);
        this.setState({ loading: false, error: error });
      });
    // this.graphQLProvider.mutate<CreateMedicationMutationVariables>(
    //   'create-medication',
    //   { data: input, filters },
    //   (data: CreateMedicationMutation) =>
    //     this.handleCreateSuccess(data.CreateMedication, successFn),
    //   (error: ApolloError) => this.handleCreateError(error, errorFn),
    // );
  };

  rehydrate = async () => {
    this.setState({
      loading: true,
      activeMedications: null,
      allMedications: null,
      error: null,
    });
    const input = { data: {} };
    const res = await fetchMedications(input.data);
    if (res.error) {
      this.handleListError(res.error);
    } else {
      console.warn(res.data);
      this.handleListSuccess({ ListMedication: res.data });
    }
    //TODO GraphQl
    // this.graphQLProvider
    //   .query<ListMedicationQueryVariables>('list-medication', input, this.handleListSuccess, this.handleListError);
  };

  reset() {
    this.state = this.initialState;
  }

  createLocalNotifications(medication: MedicationListItemFragment) {
    if (
      !medication.notify ||
      !medication.dailySchedule ||
      medication.dailySchedule.length === 0
    ) {
      return;
    }

    this.scheduleNotificationsUseCase.execute(
      this.mapToNotificationInput(medication),
    );
  }

  cancelLocalNotifications(medication: MedicationListItemFragment) {
    this.canceNotificationsUseCase.execute(
      this.mapToNotificationInput(medication),
    );
  }

  private handleListSuccess = (data: ListMedicationQuery) => {
    const activeMedications = data.ListMedication.filter(
      medication => medication.active,
    );
    this.setState({
      loading: false,
      allMedications: data.ListMedication,
      activeMedications,
    });
  };

  private handleListError = (error: ApolloError) => {
    this.setState({ loading: false, error: ErrorMapper.map(error) });
  };

  private handleCreateSuccess = (
    medications: MedicationListItemFragment[],
    successFn: () => void,
  ) => {
    const activeMedications = medications.filter(
      medication => medication.active,
    );
    this.setState({
      loading: false,
      allMedications: medications,
      activeMedications,
    });
    successFn();

    if (medications.length > 0) {
      this.createLocalNotifications(medications[0]);
    }
  };

  private handleCreateError = (
    error: ApolloError,
    errorFn: (error: ApolloError) => void,
  ) => {
    this.setState({ loading: false, error: ErrorMapper.map(error) });
    errorFn(error);
  };

  private mapToNotificationInput(
    medication: MedicationListItemFragment,
  ): Medication {
    return {
      medicationId: medication.id,
      medicineName: medication.medicine && medication.medicine.name,
      start: moment(medication.startDate).toDate(),
      end: medication.endDate && moment(medication.endDate).toDate(),
      dailySchedules:
        medication.dailySchedule && medication.dailySchedule.map(it => it.hour),
    };
  }
}
