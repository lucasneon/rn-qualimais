import {
  ConditionFragment,
  ConditionQuery,
  InsertConditionMutation,
  InsertConditionMutationVariables,
} from '@app/data/graphql';
import { conditionRequest } from '@app/modules/health/api/condition/condition.service';
import { Service, Token } from 'typedi';
import { MedicalRecordProvider } from './medical-record.provider';

export const ConditionProviderToken = new Token<ConditionProvider>();

@Service(ConditionProviderToken)
export class ConditionProvider extends MedicalRecordProvider<
  ConditionQuery,
  InsertConditionMutation,
  InsertConditionMutationVariables,
  ConditionFragment
> {
  constructor() {
    super('conditions', 'insert-condition', 'Conditions', 'InsertCondition');
  }

  async fetch() {
    this.setState({ loading: true });
    const res = await conditionRequest();
    if (res?.error) {
      return this.onError(res.error);
    }
    this.setSuccess({ Conditions: res || [] });
  }
}
