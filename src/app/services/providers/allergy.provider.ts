import { Service, Token } from 'typedi';
import {
  AllergiesQuery,
  AllergyFragment,
  InsertAllergyMutation,
  InsertAllergyMutationVariables,
} from '@app/data/graphql';
import { MedicalRecordProvider } from './medical-record.provider';
import {
  addAllergy,
  allergiesRequest,
} from '@app/modules/health/api/allergies/allergies.service';
import { ApolloError } from 'apollo-client';

export const AllergyProviderToken = new Token<AllergyProvider>();

@Service(AllergyProviderToken)
export class AllergyProvider extends MedicalRecordProvider<
  AllergiesQuery,
  InsertAllergyMutation,
  InsertAllergyMutationVariables,
  AllergyFragment
> {
  constructor() {
    super('allergies', 'insert-allergy', 'Allergies', 'InsertAllergy');
  }

  async fetch() {
    this.setState({ loading: true });
    const res = await allergiesRequest();
    if (res?.error) {
      return this.onError(res.error);
    }
    this.setSuccess({ Allergies: res.data || [] });
  }

  /// @Override
  insert = (
    data: any,
    succesFn: (resp: InsertAllergyMutation) => any,
    errorFn: (resp: ApolloError) => any,
  ): void => {
    this.setState({ loading: true });
    addAllergy(data)
      .then(async res => {
        const allergies = await allergiesRequest();

        if (res?.error) {
          this.onMutationError(res.error);
          errorFn(res.error);
          this.setState({ loading: false });
        } else {
          this.onMutationSuccess({ InsertAllergy: allergies.data });
          succesFn({ InsertAllergy: allergies.data });
          this.setState({ loading: false });
        }
      })
      .catch(error => {
        this.onMutationError(error);
        errorFn(error);
        this.setState({ loading: false });
      });
    return;
  };
}
