import {
  InsertProcedureMutation,
  InsertProcedureMutationVariables,
  ProcedureFragment,
  ProceduresQuery,
} from '@app/data/graphql';
import { proceduresRequest } from '@app/modules/health/api/procedures/procedures.service';
import { Service, Token } from 'typedi';
import { MedicalRecordProvider } from './medical-record.provider';

export const ProcedureProviderToken = new Token<ProcedureProvider>();

@Service(ProcedureProviderToken)
export class ProcedureProvider extends MedicalRecordProvider<
  ProceduresQuery,
  InsertProcedureMutation,
  InsertProcedureMutationVariables,
  ProcedureFragment
> {
  constructor() {
    super('procedures', 'insert-procedure', 'Procedures', 'InsertProcedure');
  }

  async fetch() {
    this.setState({ loading: true });
    const res = await proceduresRequest();
    if (res?.error) {
      return this.onError(res.error);
    }
    this.setSuccess({ Procedures: res.data });
  }
}
