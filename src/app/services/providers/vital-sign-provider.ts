import { ApolloError } from 'apollo-client';
import { Container as TypeDIContainer, Service, Token } from 'typedi';
import { Container } from 'unstated';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import {
  RecentVitalSignsQuery,
  VitalSignFragment,
  VitalSignInput,
  VitalSignType,
} from '@app/data/graphql';
import {
  execLastMesure,
  fetchVitalSign,
  insertVitalSigns,
} from '@app/modules/health/api/vital-signs/vital-sign.service';

export const VitalSignProviderToken = new Token<VitalSignProvider>();

export interface VitalSignState {
  recentVitalSigns: VitalSignFragment[] | any;
  loading: boolean;
  error: ApolloError;
  data: any[];
}

@Service(VitalSignProviderToken)
export class VitalSignProvider extends Container<VitalSignState> {
  private graphQLProvider: GraphQLProvider =
    TypeDIContainer.get(GraphQLProviderToken);

  private data = [
    VitalSignType.Weight,
    VitalSignType.Height,
    VitalSignType.Imc,
    VitalSignType.BloodPressure,
    VitalSignType.Glycemia,
  ];

  constructor() {
    super();
    this.state = {
      recentVitalSigns: null,
      error: null,
      loading: null,
      data: null,
    };
  }

  async fetch(vitalSign: VitalSignType, count?: number) {
    this.setState({ loading: true, data: null });
    const res = await fetchVitalSign(vitalSign, count);
    this.setState({ loading: false, data: res });
  }

  async rehydrate() {
    this.setState({ recentVitalSigns: null, error: null, loading: true });
    execLastMesure(this.data)
      .then(res => this.onMutationSuccess({ RecentVitalSigns: res }))
      .catch(err => this.onMutationError(err));

    //TODO GRAPHQL
    // this.graphQLProvider.query<RecentVitalSignsQueryVariables>(
    //   'recent-vital-signs',
    //   { data: this.data },
    //   this.onMutationSuccess,
    //   this.onMutationError,
    // );
  }

  insertVitalSign(vitalSign: VitalSignInput[]) {
    return insertVitalSigns(vitalSign);
  }
  private onMutationSuccess = (resp: RecentVitalSignsQuery | any) => {
    this.setState({ recentVitalSigns: resp.RecentVitalSigns, loading: false });
  };

  private onMutationError = (error: ApolloError) => {
    this.setState({ error, loading: false });
  };
}
