import { ApolloError } from 'apollo-client';
import { Container as TypeDIContainer, Service, Token } from 'typedi';
import { Container } from 'unstated';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { HabitFragment } from '@app/data/graphql';
import { AppError } from '@app/model';
import { ErrorMapper } from '@app/utils';
import { fetchHabits } from '@app/modules/home/api/habits/habits.service';
import { fetchDetailsHabits } from '@app/modules/habit/api/habits/habits-detail.service';
import {
  CarePlanHabitModel,
  HabitDetailsInputModel,
  HabitModel,
} from '@app/modules/home/api/habits/habit.model';
import { fetchAvailableHabits } from '@app/modules/home/api/habits/available-habits.service';

export const HabitsProviderToken = new Token<HabitsProvider>();

export interface HabitsProviderState {
  loading?: boolean;
  error?: AppError;
  activeHabits?: HabitFragment[];
  allHabits?: HabitFragment[];
  habitsDetail?: any;
}

export interface ActivateHabitParams {
  habitCode: string;
  habitTypeCode: string;
  successFn: (message: string) => void;
  errorFn: (error: AppError) => void;
}

export interface DeactivateHabitParams {
  carePlanItemCode: string;
  successFn: (message: string) => void;
  errorFn: (error: AppError) => void;
}

@Service(HabitsProviderToken)
export class HabitsProvider extends Container<HabitsProviderState> {
  carePlanCode: string;

  private readonly initialState = {
    loading: null,
    activeHabits: null,
    allHabits: null,
    error: null,
    habitsDetail: null,
  };
  private readonly graphQLProvider: GraphQLProvider =
    TypeDIContainer.get(GraphQLProviderToken);

  constructor() {
    super();
    this.state = {
      loading: null,
      activeHabits: null,
      allHabits: null,
      error: null,
      habitsDetail: null,
    };
  }

  rehydrate = async () => {
    this.setState({
      loading: true,
      activeHabits: null,
      allHabits: null,
      error: null,
      habitsDetail: null,
    });

    const input = { data: {} };

    const res = await fetchHabits(input);
    if (res?.error) {
      console.error(res.error);
      return this.handleListError(res.error);
    }
    this.handleListSucces({ Habits: res.data });
    //TODO graphQl
    // this.graphQLProvider.query<HabitsQueryVariables>(
    //   'habits',
    //   input,
    //   this.handleListSucces,
    //   this.handleListError,
    // );
  };

  reset() {
    this.state = this.initialState;
  }

  async fetchDetails(input: HabitDetailsInputModel) {
    // fetchDetails(item);

    const [habits, carePlanItem]: [HabitModel[], CarePlanHabitModel] =
      await Promise.all([
        fetchAvailableHabits(),
        input.carePlanItemCode &&
          fetchDetailsHabits(input.carePlanItemCode).catch(() => null),
      ]);

    const habit: HabitModel = habits.find(it => it.id === input.habitCode);

    if (!habit) {
      this.setState({
        habitsDetail: null,
        loading: false,
        error: new Error(`Habit with id ${input.habitCode} not found`),
      });
      throw new Error(`Habit with id ${input.habitCode} not found`);
    }

    if (carePlanItem) {
      habit.carePlanItem = carePlanItem;
    }

    this.setState({ habitsDetail: habit, loading: false });
  }

  private handleListSucces = (data: any) => {
    const activeHabits = data.Habits.filter(habit => !!habit.carePlanItem);
    this.setState({ loading: false, allHabits: data.Habits, activeHabits });
  };

  private handleListError = (error: ApolloError) => {
    this.setState({ loading: false, error: ErrorMapper.map(error) });
  };
}
