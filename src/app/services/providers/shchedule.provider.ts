import { fetchSchedulesForSingleSpecialty } from '@app/modules/common/api/schedules/schedules.service';
import { ApolloError } from 'apollo-client';
import { Service, Token } from 'typedi';
import { Container } from 'unstated';

const ScheduleProviderToken = new Token<ScheduleProvider>();

export interface ScheduleState {
  data: any;
  loading: boolean;
  error: ApolloError;
}

@Service(ScheduleProviderToken)
export class ScheduleProvider extends Container<ScheduleState> {
  initialState: ScheduleState = {
    data: null,
    loading: true,
    error: null,
  };

  constructor() {
    super();
    this.state = this.initialState;
  }
  
  fetchSchedules = async (data: any) => {
    this.setState({ loading: true });
    try {
      const res = await fetchSchedulesForSingleSpecialty(data);
      this.setState({
        data: { data: { SpecialtySchedules: res } },
        loading: false,
      });
    } catch (error) {
      this.setState({
        error,
        loading: false,
      });
    }
  };
}
