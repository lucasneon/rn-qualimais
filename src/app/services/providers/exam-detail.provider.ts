import { ExamDetailsFragment, FulfillEventInput } from '@app/data/graphql';
import { AppError } from '@app/model';
import { ExamDetailPageProps } from '@app/modules/common/events';
import { fetchExamDetails } from '@app/modules/common/events/api/exam-detail.service';
import { ExamDetailsInputModel } from '@app/modules/common/events/api/exam.model';
import { FulfillEventInputModel } from '@app/modules/home/api/care-plan/care-plan.model';
import { ErrorMapper } from '@app/utils';
import { ApolloError } from 'apollo-client';
import { Service, Token } from 'typedi';
import { Container } from 'unstated';
import { LocationError } from '.';
import moment from 'moment';
import { fetchFulfillExam } from '@app/modules/common/events/api/fulfill-exam.service';

export const ExamDetailsProviderToken = new Token<ExamDetailsProvider>();

export interface ExamDetailsProviderState {
  loading: boolean;
  locationError?: LocationError;
  error?: AppError;
  details?: ExamDetailsFragment;
  fulfillRequestLoading?: boolean;
  fulfillFn?: (data: FulfillEventInput) => void;
  errorFn: (error: AppError) => void;
  successFn: (date: string) => Promise<void>;
}

@Service(ExamDetailsProviderToken)
export class ExamDetailsProvider extends Container<ExamDetailsProviderState> {
  constructor() {
    super();
    this.state = {
      loading: false,
      locationError: null,
      error: null,
      details: null,
      fulfillRequestLoading: null,
      errorFn: null,
      successFn: null,
    };
  }

  async rehydrate(props: ExamDetailPageProps) {
    this.setState({
      loading: true,
      locationError: null,
      error: null,
      details: null,
      fulfillRequestLoading: null,
    });
    const input: ExamDetailsInputModel = {
      carePlanScheduleCode: props.carePlanScheduleCode,
      carePlanItemCode: props.carePlanItemCode,
      latitude: props.userLocation.latitude,
      longitude: props.userLocation.longitude,
      procedureCode: props.procedureCode,
    };

    const res = await fetchExamDetails(input);
    console.warn('res ', res.data);

    if (res?.error) {
      return this.handleListError(res?.error);
    }
    return this.handleListSucces(res.data);
  }

  private handleListSucces = (data: any) => {
    console.warn('data >', data);
    this.setState({
      loading: false,
      error: ErrorMapper.map('Erro ao buscar detalhes do exame'),
      details: data,
      fulfillFn: this.handleFufillDialogConfirmation,
      fulfillRequestLoading: this.state.fulfillRequestLoading,
    });
  };

  private handleFufillDialogConfirmation = async (data: FulfillEventInput) => {
    this.setState({ fulfillRequestLoading: true });

    const input: FulfillEventInputModel = {
      id: data.id,
      itemCode: data.itemCode,
    };
    const res = await fetchFulfillExam(input);
    this.setState({ fulfillRequestLoading: false });
    if (res) {
      console.warn('resFulfill >', res);
      return this.handleSucess(moment().format('YYYY-MM-DD'));
    }
    this.handleError(null);

    //TODO GRAPHQL
    // this.graphQLProvider.mutate<FulfillExamMutationVariables>(
    //   'fulfill-exam',
    //   { data: input },
    //   (date: FulfillExamMutation) => this.handleSucess(date.FulfillExam.doneAt),
    //   (error: ApolloError) => this.handleError(error),
    // );
  };

  private handleSucess = (date: string) => {
    console.warn('data >', date);
    this.setState({ fulfillRequestLoading: false });
    this.state.successFn(date);
  };

  private handleError = (error: ApolloError) => {
    this.setState({ fulfillRequestLoading: false });
    const errorMapper = ErrorMapper.map('Erro ao marcar como realizado!');
    this.state.errorFn(errorMapper);
  };

  private handleListError = (error: ApolloError) => {
    this.setState({ loading: false, error: ErrorMapper.map(error) });
  };
}
