import { MedicationDetailsInput } from '@app/data/graphql';
import { fetchMedicationDetails } from '@app/modules/health/api/medication/medication-details.service';
import { ErrorMapper } from '@app/utils';
import { ApolloError } from 'apollo-client';
import { Service, Token } from 'typedi';
import { Container } from 'unstated';

export const MedicationDetailsProviderToken =
  new Token<MedicationDetailsProvider>();

export interface MedicationDetailsProviderState {
  loading?: boolean;
  error?: any;
  medicationDetails?: any;
}

@Service(MedicationDetailsProviderToken)
export class MedicationDetailsProvider extends Container<MedicationDetailsProviderState> {
  private readonly initialState = {
    loading: null,
    medicationDetails: null,
    error: null,
  };

  constructor() {
    super();
    this.state = {
      loading: null,
      medicationDetails: null,
      error: null,
    };
  }

  async rehydrate(medicationId: any) {
    this.setState({
      loading: true,
      medicationDetails: null,
      error: null,
    });
    const inputData: MedicationDetailsInput = {
      medicationId: medicationId,
    };

    const res = await fetchMedicationDetails(inputData);
    if (res?.error) {
      return this.handleListError(res?.error);
    }
    return this.handleListSucces(res.data);
  }

  private handleListSucces = (data: any) => {
    this.setState({ loading: false, medicationDetails: data });
  };

  private handleListError = (error: ApolloError) => {
    this.setState({ loading: false, error: ErrorMapper.map(error) });
  };
}
