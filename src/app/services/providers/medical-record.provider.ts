import { ApolloError } from 'apollo-client';
import { Container as TypeDIContainer } from 'typedi';
import { Container } from 'unstated';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { allergiesRequest } from '@app/modules/health/api/allergies/allergies.service';

export interface MedicalRecordState<FragmentType> {
  data: FragmentType[];
  loading: boolean;
  error: ApolloError;
}

export class MedicalRecordProvider<
  QueryType,
  MutationType,
  MutationVariablesType,
  FragmentType,
> extends Container<MedicalRecordState<FragmentType>> {
  private graphQLProvider: GraphQLProvider =
    TypeDIContainer.get(GraphQLProviderToken);

  constructor(
    private readonly queryDocument: string,
    private readonly mutationDocument: string,
    private readonly queryResultField: string,
    private readonly mutationResultField: string,
  ) {
    super();
    this.state = { data: null, error: null, loading: null };
  }

  fetch() {
    // if (!this.state.data) {
    this.setState({ loading: true });

    this.graphQLProvider.query(
      this.queryDocument,
      null,
      this.onSuccess,
      this.onError,
    );
    // }
  }

  insert = (
    data: MutationVariablesType,
    succesFn: (resp: MutationType) => any,
    errorFn: (resp: ApolloError) => any,
  ) => {
    this.setState({ loading: true });
    this.graphQLProvider.mutate<MutationVariablesType>(
      this.mutationDocument,
      data,
      (resp: MutationType) => {
        this.onMutationSuccess(resp);
        succesFn(resp);
        this.setState({ loading: false });
      },
      (error: ApolloError) => {
        this.onMutationError(error);
        errorFn(error);
        this.setState({ loading: false });
      },
    );
  };

  protected onSuccess = (resp: any) => {
    this.setState({ data: resp[this.queryResultField], loading: false });
  };

  public setSuccess = (resp: any) => {
    this.onSuccess(resp);
  };

  protected onError = (error: ApolloError) => {
    this.setState({ error, loading: false });
  };

  public setError = (error: any) => {
    this.onError(error);
  };

  protected onMutationSuccess = (resp: MutationType) => {
    this.setState({ data: resp[this.mutationResultField] });
  };

  protected onMutationError = (error: ApolloError) => {
    this.setState({ error, loading: false });
  };
}
