import { fetchCarePlanProgress } from './../../modules/home/api/care-plan-progress/care-plan-progress.service';
import { Container as TypeDIContainer, Service, Token } from 'typedi';
import { Container } from 'unstated';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { Navigation } from '@app/core/navigation';
import { AppError, CarePlanStatus, CarePlanSummary } from '@app/model';
import { ErrorMapper } from '@app/utils';
import { HabitsProvider, HabitsProviderToken } from './habits.provider';
import {
  MedicationsProvider,
  MedicationsProviderToken,
} from './medications.provider';
import {
  BeneficiaryProvider,
  BeneficiaryProviderToken,
} from './beneficiary.provider';

import { CampaignModel } from '@app/modules/home/api/campaings';
import { CarePlanProgressModel } from '@app/modules/home/api/care-plan/care-plan.model';
import { fetchCampaings } from '@app/modules/home/api/campaings/campaings.service';

export const CarePlanProviderToken = new Token<CarePlanProvider>();

export interface CarePlanProviderState {
  campaigns?: CampaignModel[];
  carePlan?: CarePlanSummary;
  didAnswerRequiredQuestionnaires?: boolean;
  loading?: boolean;
  error?: AppError;
  needAnswerPrincipal: boolean;
}

@Service(CarePlanProviderToken)
export class CarePlanProvider extends Container<CarePlanProviderState> {
  private readonly graphQLProvider: GraphQLProvider =
    TypeDIContainer.get(GraphQLProviderToken);
  private readonly habitsProvider: HabitsProvider =
    TypeDIContainer.get(HabitsProviderToken);
  private readonly medicationProvider: MedicationsProvider =
    TypeDIContainer.get(MedicationsProviderToken);

  private readonly beneficiaryProvider: BeneficiaryProvider =
    TypeDIContainer.get(BeneficiaryProviderToken);

  constructor() {
    super();
    this.state = {
      loading: null,
      campaigns: null,
      error: null,
      didAnswerRequiredQuestionnaires: null,
      needAnswerPrincipal: false,
    };
  }

  rehydrate = async () => {
    this.habitsProvider.reset();
    this.medicationProvider.reset();
    this.setState({
      loading: true,
      campaigns: null,
      error: null,
      didAnswerRequiredQuestionnaires: null,
      needAnswerPrincipal: false,
    });

    try {
      //TODO GraphQL
      // const campaignsRes: CampaignsQuery =
      //   await this.graphQLProvider.makeQuery<CampaignsQuery>('campaigns');
      const campaignsRes: CampaignModel[] = await fetchCampaings();
      this.processResults(campaignsRes);
    } catch (error) {
      this.setState({ loading: false, error: ErrorMapper.map(error) });
    }
  };

  updateProgress = async (): Promise<CarePlanSummary> => {
    this.setState({ loading: true });
    try {
      //TODO GraphQL
      // const carePlanProgressRes: CarePlanProgressQuery =
      //   await this.graphQLProvider.makeQuery<CarePlanProgressQuery>(
      //     'care-plan-progress',
      //   );
      const carePlanProgressRes: CarePlanProgressModel =
        await fetchCarePlanProgress();
      const carePlanFragment: CarePlanProgressModel = carePlanProgressRes;
      const carePlanCode: string = carePlanFragment && carePlanFragment.code;
      const carePlan: CarePlanSummary = {
        code: carePlanCode,
        progress: carePlanFragment && carePlanFragment.progress,
      };
      this.setState({ loading: false, carePlan });
      return carePlan;
    } catch (error) {
      this.setState({ loading: false, error: ErrorMapper.map(error) });
      return null;
    }
  };

  private processResults = async (campaignsRes: CampaignModel[]) => {
    console.warn(
      'campaignsRes.questionnaires > ',
      campaignsRes[0].questionnaires,
    );
    const carePlan = await this.updateProgress();
    let didAnswerAll: boolean = true;
    if (!carePlan.code) {
      didAnswerAll = this.areRequiredQuestionnairesAnswered(campaignsRes);
      const carePlanStatus: CarePlanStatus = didAnswerAll
        ? CarePlanStatus.NeedsApproval
        : CarePlanStatus.NeedsAnswers;
      carePlan.status = carePlanStatus;
    }
    const isDepedant = await this.beneficiaryProvider.state.active.dependant;
    const isHolderLogin = await this.beneficiaryProvider.isHolderLogin();
    const age = this.beneficiaryProvider.getBeneficiaryAge();
    console.warn(
      'isDepedant, age, didAnswerAll, isHolderLogin',
      isDepedant,
      age,
      didAnswerAll,
      isHolderLogin,
    );
    if (!didAnswerAll && isDepedant && age < 18 && !isHolderLogin) {
      const carePlanStatus: CarePlanStatus =
        CarePlanStatus.NeedsAnswersPrincipal;
      carePlan.status = carePlanStatus;
    }

    this.setState(
      {
        loading: false,
        carePlan,
        campaigns: campaignsRes,
        didAnswerRequiredQuestionnaires: didAnswerAll,
        needAnswerPrincipal:
          carePlan.status === CarePlanStatus.NeedsAnswersPrincipal,
      },
      () => {
        Navigation.setBadge('HealthPage', didAnswerAll ? '' : '!');
      },
    );

    if (carePlan.code) {
      this.habitsProvider.carePlanCode = carePlan.code;
      this.habitsProvider.rehydrate();
      this.medicationProvider.rehydrate();
    }
  };

  private areRequiredQuestionnairesAnswered(
    campaigns: CampaignModel[],
  ): boolean {
    const isRequiredAndNotAnswered = questionnaire =>
      questionnaire.required && !questionnaire.isCompletelyAnswered;
    return !campaigns.some(campaign =>
      campaign.questionnaires.some(isRequiredAndNotAnswered),
    );
  }
}
