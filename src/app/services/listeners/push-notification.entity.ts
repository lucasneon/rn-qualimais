export interface NotificationData {
  redirect: string;
  data: string;
}

export interface ChatNotificationData {
  pushId: string;
  tipo: string;
  conversaId: string;
  mensagemId: string;
}
