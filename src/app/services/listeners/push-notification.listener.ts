import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';
import Container, { Service } from 'typedi';
import { RegisterPushNotificationUseCase } from '@app/domain/push-notification';
import { NavigateToUseCase } from '@app/domain/redirect';
import {
  LocalNotification,
  NavigationDestination,
  NavigationRequest,
} from '@app/model';
import {
  ChatNotificationData,
  NotificationData,
} from './push-notification.entity';

@Service()
export class PushNotificationListener {
  private readonly navigateToUseCase: NavigateToUseCase =
    Container.get(NavigateToUseCase);
  private readonly registerUseCase: RegisterPushNotificationUseCase =
    Container.get(RegisterPushNotificationUseCase);

  async configurePushNotification(): Promise<void> {
    this.configureLocalNotifications();
    await this.configureRemoteNotifications();
    return;
  }

  private async configureLocalNotifications() {
    PushNotification.configure({
      onNotification: notification => {
        if (notification.isLocalNotification) {
          this.processLocalNotification(notification);
        }
      },
    });
  }

  private processLocalNotification(notification: LocalNotification) {
    if (notification.navigationRequest) {
      this.navigateToUseCase.execute(notification.navigationRequest);
    }
  }

  private async configureRemoteNotifications(): Promise<void> {
    const registered: boolean = await this.registerUseCase.execute();
    try {
      await messaging().requestPermission();

      if (registered) {
        this.handleAppOpenByPush();
        this.startListening();
      }
    } catch (err) {
      console.warn(err);
    }

    return;
  }

  private startListening(): void {
    messaging().onNotificationOpenedApp(this.processPushNotification);
  }

  private handleAppOpenByPush = async () => {
    const initialNotification = await messaging().getInitialNotification();
    if (initialNotification) {
      this.processPushNotification(initialNotification);
    }
  };

  private processPushNotification = (notificationOpen: any) => {
    const navigationRequest: NavigationRequest = this.mapPushData(
      notificationOpen.notification.data,
    );
    this.navigateToUseCase.execute(navigationRequest);
  };

  private mapPushData(
    data: NotificationData & ChatNotificationData,
  ): NavigationRequest {
    const isChatPush: boolean = !!data?.tipo;

    function mapRedirection(redirect: string): NavigationDestination {
      return Object.values(NavigationDestination).find(
        value => redirect === value,
      );
    }

    return isChatPush
      ? {
          destination: NavigationDestination.Chat,
          data: data.conversaId,
        }
      : {
          destination: data ? mapRedirection(data.redirect) : null,
          data: data?.data && JSON.parse(data.data),
        };
  }
}
