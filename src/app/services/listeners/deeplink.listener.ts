import { Linking } from 'react-native';
import Container, { Service } from 'typedi';
import { ParseDeeplinkUseCase } from '@app/domain/deep-link';
import { NavigateToUseCase } from '@app/domain/redirect';
import { NavigationRequest } from '@app/model';

@Service()
export class DeeplinkListener {
  private readonly parseUseCase: ParseDeeplinkUseCase = Container.get(ParseDeeplinkUseCase);
  private readonly navigateToUseCase: NavigateToUseCase = Container.get(NavigateToUseCase);

  startListening(): void {
    Linking.getInitialURL().then(this.handleDeeplink);
    Linking.addEventListener('url', ({ url }) => this.handleDeeplink(url));
  }

  private handleDeeplink = (url: string) => {
    const navigationRequest: NavigationRequest = this.parseUseCase.execute(url);

    if (navigationRequest) {
      this.navigateToUseCase.execute(navigationRequest);
    }
  }
}
