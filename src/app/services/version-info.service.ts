import VersionNumber from 'react-native-version-number';
import { Service } from 'typedi';

@Service()
export class VersionInfoService {

  getVersionName(): string {
    return VersionNumber.appVersion;
  }

  getVersionCode(): string {
    return VersionNumber.buildVersion;
  }

  getBundleId(): string {
    return VersionNumber.bundleIdentifier;
  }
}
