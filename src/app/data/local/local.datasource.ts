import { AsyncStorage } from 'react-native';
import { Service } from 'typedi';

@Service()
export class LocalDataSource {
  set<T>(key: string, value: T): Promise<T> {
    return this.safeSaveInAsyncStorage(key, value);
  }

  async get<T>(key: string): Promise<T> {
    const value = await AsyncStorage.getItem(key);
    return JSON.parse(value);
  }

  remove(key: string): Promise<void> {
    return AsyncStorage.removeItem(key);
  }

  removeMulti(keys: string[]): Promise<void> {
    return AsyncStorage.multiRemove(keys);
  }

  private async safeSaveInAsyncStorage<T>(key: string, value: T): Promise<T> {
    if (key === undefined || value === undefined) {
      throw new Error('undefined key or value');
    }

    await AsyncStorage.setItem(key, JSON.stringify(value));

    return value;
  }
}
