import { NavigationRequest } from '@app/model';

export type RepeatTypeEntity = 'week' | 'day' | 'hour' | 'minute' | 'time';

export interface ScheduleLocalNotificationEntity {
  id?: number;
  userInfo?: { id: number };
  title?: string;
  message: string;
  isLocalNotification: boolean;
  date: Date;
  navigationRequest?: NavigationRequest;
  repeatType?: RepeatTypeEntity;
  repeatTime?: number; // Required if repeatType === 'time'. Number of milliseconds between each interval
}
