import PushNotification from 'react-native-push-notification';
import { Service } from 'typedi';
import { LocalNotification } from '@app/model';
import {
  RepeatTypeEntity,
  ScheduleLocalNotificationEntity,
} from './local-notification.entity';

const RepeatMap: { [period: string]: RepeatTypeEntity } = {
  daily: 'day',
  weekly: 'week',
};

@Service()
export class LocalNotificationDatasource {
  schedule(input: LocalNotification, date: Date) {
    const entity: ScheduleLocalNotificationEntity = {
      id: +input.id,
      userInfo: { id: +input.id },
      title: input.title,
      message: input.message,
      isLocalNotification: true,
      date,
      navigationRequest: input.navigationRequest,
      repeatType: RepeatMap[input.repeatType],
    };

    PushNotification.localNotificationSchedule(entity);
  }

  cancelScheduledNotification(id: number) {
    PushNotification.cancelLocalNotifications({ id });
  }
}
