/* tslint:disable */
//  This file was automatically generated and should not be edited.

import { DaySlotModel } from '@app/modules/common/api/professional-schedule/professional-schedule.model';

// Tipo do procedimento do plano de cuidado
export enum ProcedureType {
  Appointment = 'Appointment', // Consulta
  Exam = 'Exam', // Exame
}

// Origem de quem gerou o item no plano de cuidado
export enum CarePlanItemGenerator {
  LinhaCuidado = 'LinhaCuidado', // Eventos gerados pela linha de cuidado (questionários)
  PlanoCuidado = 'PlanoCuidado', // Eventos gerados diretamente por usuários do sistema
}

export enum Sex {
  Both = 'Both', // Ambos
  Female = 'Female', // Feminino
  Male = 'Male', // Masculino
}

// Descreve os tipos de hábitos padronizados para íconografia.
export enum HabitArea {
  Alcoholism = 'Alcoholism', // Alcoolismo
  Eating = 'Eating', // Alimentação
  PhysicalActivity = 'PhysicalActivity', // Atividade física
  Smoking = 'Smoking', // Tabagismo
  Unknown = 'Unknown', // Outro (não identificado previamente)
  WellBeing = 'WellBeing', // Bem-estar
}

// Formatos de resposta: textual, uma escolha, múltiplas escolhas
export enum QuestionType {
  Date = 'Date', // Data (apenas dia, mês e ano)
  MultipleChoices = 'MultipleChoices', // Alternativas - múltiplas escolhas
  Number = 'Number', // Valor numérico
  OneChoice = 'OneChoice', // Alternativas - apenas uma escolha
  Textual = 'Textual', // Resposta apenas textual
}

// Referência de tópico do Questionário de Sáude
export enum QuestionnaireFeedbackTopicReferenceEnum {
  EmotionalHealth = 'EmotionalHealth', // Saúde emocional
  Exercise = 'Exercise', // Atividade física
  GeneralHealth = 'GeneralHealth', // Sua sáude
  Habits = 'Habits', // Hábitos
  Nutrition = 'Nutrition', // Alimentação
  SelfCare = 'SelfCare', // Autocuidado
  Smoking = 'Smoking', // Tabagismo
  Weight = 'Weight', // Peso
}

// Tipos de sinais vitais
export enum VitalSignType {
  BloodPressure = 'BloodPressure', // Pressão arterial
  pressao_arterial = 'pressao_arterial', // Pressão arterial
  DiastolicPressure = 'DiastolicPressure', // Pressão arteriam diastólica (mínima)
  Glycemia = 'Glycemia', // Glicemia
  htg = 'htg', // Glicemia
  hgt = 'hgt', // Glicemia
  Height = 'altura', // Altura
  Imc = 'imc', // IMC
  MeanPressure = 'MeanPressure', // Pressão arteriam média
  SystolicPressure = 'SystolicPressure', // Pressão arteriam sistólica (máxima)
  Weight = 'peso', // Peso
}

export interface AcceptTermOfUseInput {
  // Usuario logado
  use: string;
  // Código do beneficiário
  codeBenefiary: string;
  // Termo de consentimento
  consentTerm: string;
}

export interface HabitActivateInput {
  // Identificador do plano de cuidado
  carePlanCode?: string | null;
  // Identificador do hábito
  habitCode?: string | null;
  // Identificador do tipo do hábito
  habitTypeCode?: string | null;
}

export interface AnswerQuestionnaireInput {
  // Identificador da campanha
  campaignCode: string;
  // Identificador do questionário
  questionnaireCode: string;
  // Identificador do último gabarito (caso não seja uma nova submissão)
  responseCode?: string | null;
  // Grupos de questões (enviar sempre todos os respondidos)
  questionGroups: Array<QuestionGroupInput | null>;
  // Peso kg (caso precise de IMC)
  weight?: number | null;
  // Altura cm (caso precise de IMC)
  height?: number | null;
}

export interface QuestionGroupInput {
  // Identificador
  code: string;
  // Perguntas
  questions: Array<QuestionInput | null>;
}

export interface QuestionInput {
  // Identificador da pergunta
  code: string;
  // Identificador das respostas escolhidas (alternativas)
  chosenAnswers?: Array<string | null> | null;
  // Identificador da respostas dada (textual/numérico/data)
  typedAnswer?: string | null;
  // Enum do tipo de resposta
  type: QuestionType;
}

export interface CancelAppointmentInput {
  // Identificador da Prontlife fo atendimento
  appointmentId: string;
}

export interface BeneficiaryInfoInput {
  // Novo e-mail
  email?: string | null;
  // Telefone (apenas números - inclui código do país). Ex: 5511999999999
  phone?: string | null;
  // Novo endereço (envie apenas o que mudou)
  address?: AddressInput | null;
}

export interface AddressInput {
  // Estado
  state?: string | null;
  // Cidade
  city?: string | null;
  // Bairro
  district?: string | null;
  // CEP
  cep?: string | null;
  // Rua
  street?: string | null;
  // Número
  addressNumber?: string | null;
  // Complemento
  complement?: string | null;
  // Instruções de como chegar
  instructions?: string | null;
}

export interface ChangePasswordInput {
  // Senha atual
  currentPassword: string;
  // Nova senha
  newPassword: string;
}

export interface MedicationInput {
  // Início da medicação
  startDate: string;
  // Final da medicação, se houver
  endDate?: string | null;
  // Indica se é para ser notificado
  notify?: boolean | null;
  // Orientações
  orientation?: string | null;
  // Horários a tomar
  dailySchedule: Array<MedicationScheduleInput | null>;
  // Detalhes do medicamento
  medicine: MedicineInput;
}

export interface MedicationScheduleInput {
  // Horário para tomar o medicamento
  hour: string;
}

export interface MedicineInput {
  // Nome
  name: string;
  // Tipo
  type?: string | null;
  // Nome popular
  commonName?: string | null;
  // Aspecto
  aspect?: string | null;
  // Dosagem
  dosage?: MeasureInput | null;
}

export interface MeasureInput {
  // Quantidade
  amount: number;
  // Unidade de medida
  unit: string;
}

export interface ListMedicationInput {
  // Indica se é para filtrar apenas as medicações ativas
  activeOnly?: boolean | null;
}

export interface HabitDeactivateInput {
  // Código do item
  itemCode?: string | null;
}

export interface FulfillEventInput {
  // Identificador do evento (agendamento/hábito)
  id: string;
  // Identificador do item do plano de cuidado
  itemCode: string;
  // Data de relização/cumprimento
  fulfilledAt?: string | null;
}

export interface AllergyInput {
  // Causador da alergia
  display: string;
  // Data da primeira vez que foi detectada
  assertedDate: string;
}

export interface VaccineInput {
  // Nome da vacina
  display: string;
  // Data em que tomou
  takenAt: string;
}

export interface VitalSignInput {
  // Tipo do sinal vital (Enum VitalSignType)
  type?: VitalSignType | null;
  // Data de realização da medição (sem timezone). Ex: 2018-09-10T09:00:00.000Z
  measuredAt?: string | null;
  // Medida do sinal vital
  measure?: MeasureInput | null;
  // Sub-medidas (usado, por exemplo, na medida de pressão, que tem mais de uma)
  subMeasures?: Array<SingleVitalSignInput | null> | null;
}

export interface SingleVitalSignInput {
  // Tipo do sinal vital (Enum VitalSignType)
  type: VitalSignType;
  // Data da medição
  measuredAt?: string | null;
  // Medida do sinal vital
  measure?: MeasureInput | null;
}

export interface PushRegisterInput {
  email: string;
  // Nome do usuário
  name: string;
  // Token particular do device
  deviceInfo: DeviceInfo;
  // Código da versão - numérico
  appVersionCode: number;
  // Tag da versão. Ex: 0.4.1-beta.1
  appVersion: string;
}

export interface DeviceInfo {
  // Token do Firebase do aparelho
  token?: string | null;
  // Nome do aparelho
  name?: string | null;
  // Sistema operacional (Android/Ios)
  os?: DeviceOS | null;
  // Versão do sistema operacional
  osVersion?: string | null;
}

export enum DeviceOS {
  Android = 'Android', // Plataforma Android
  Ios = 'Ios', // Plataforma iOS
}

export interface RateDoctorInput {
  // Nota da avaliação (1 a 5)
  rating: number;
  // CPF do médico
  doctorCpf: string;
  // Código da especialidade
  specialtyCode: string;
  // Texto de observações
  observation?: string | null;
}

export interface RateExamInput {
  // Nota da avaliação (1 a 5)
  rating: number;
  // Código do procedimento
  procedureCode: string;
  // Observações
  observation?: string | null;
}

export interface RateGpsInput {
  // Nota da avaliação (1 a 5)
  rating: number;
  // Código do Guia Responsável (apenas avaliação de gps)
  gpsCode: string;
  // Texto de observações
  observation?: string | null;
}

export interface FacialRecognitionInput {
  // Imagem codificada em BASE64
  encodedImage: string;
}

export interface AppointmentInput {
  // Início do atendimento (YYYY-MM-DDTHH:MM:SS)
  start: string;
  // Previsão final do atendimento (YYYY-MM-DDTHH:MM:SS)
  end: string;
  // Identificador do profissional
  professionalId: string;
  // Identificador do local
  locationId: string;
  // Códigos para inclusão do agendamento no plano de cuidado, se for o caso
  carePlan?: CarePlanCodes | null;
}

export interface CarePlanCodes {
  // Identificador do plano de cuidado
  carePlanCode?: string | null;
  // Identificador do item
  itemCode?: string | null;
  // Identificador do agendamento
  scheduleCode?: string | null;
}

export interface UnregisterPushInput {
  // Token do device usado para registrar
  deviceToken: string;
}

export interface UpdateCarePointFavoriteInput {
  // Código do prestador
  practionerId: string;
  // Código do ponto de atendimento
  carePointId: string;
  // Flag favorito ( "S" / "N" )
  flagFavorite: string;
}

export interface CampaignQuestionnairesInput {
  // Código da campanha associada
  campaignCode: string;
  // Questionários a consultar
  questionnaires: Array<QuestionnaireInput | null>;
}

export interface QuestionnaireInput {
  // Identificador do questionário
  questionnaireCode: string;
  // Identificador do último gabarito (se houver)
  responseCode?: string | null;
}

export interface CarePlanItemsInput {
  // Tipos a serem exibidos
  types?: Array<ProcedureType | null> | null;
  // Origem dos itens a serem exibidas
  generator?: Array<CarePlanItemGenerator | null> | null;
}

export interface CarePointNetworkCityInput {
  // Sigla do estado (Ex: SP)
  uf: string;
  // Código da rede
  networkCode?: number | null;
  // Código do Livreto
  bookletCode?: number | null;
  // Descrição da Especialidade
  specialityDesc?: string | null;
}

export interface CarePointNetworkDistrictInput {
  // Sigla do estado (Ex: SP)
  uf: string;
  // Código da cidade
  cityCode?: number | null;
  // Código do Grupo do Livreto
  bookletCode?: number | null;
  // Descrição da Cidade
  cityDesc?: string | null;
  // Código da Rede
  networkCode?: number | null;
  // Descrição da Especialidade
  specialityDesc?: string | null;
}

export interface CarePointNetworkPerPlanInput {
  // Sigla do código do plano
  planCode: string;
}

export interface CarePointNetworkSpecialityInput {
  // Sigla do estado (Ex: SP)
  uf: string;
  // Código da cidade
  cityCode: number;
  // Nome do bairro
  districtName?: string | null;
  // Código do tipo de prestador
  typeCode?: number | null;
  // Código do Grupo do Livreto
  bookletCode?: number | null;
  // Código da Rede
  networkCode?: number | null;
}

export interface CarePointNetworkStateInput {
  // Código do Grupo do Livreto
  bookletCode?: number | null;
  // Código da Rede do Plano do Beneficiário
  networkCode?: number | null;
  // Nome da Especialidade
  specialityDesc?: string | null;
}

export interface CarePointNetworkTypeInput {
  // Sigla do estado (Ex: SP)
  uf: string;
  // Código da cidade
  cityCode: number;
  // Nome do bairro
  districtName?: string | null;
  // Código da rede
  networkCode?: string | null;
}

export interface CarePointNetworkFilterInput {
  // Sigla do estado (Ex: SP)
  uf: string;
  // Código da cidade
  cityCode: number;
  // Nome do bairro
  districtName?: string | null;
  // Código do tipo de prestador
  typeCode?: number | null;
  // Código da especialidade
  specialityPublicationCode?: number | null;
  // Nome do prestador
  practitionerName?: string | null;
  // Latidude da localização desejada
  latitude: number;
  // Longitude da localização desejada
  longitude: number;
  // Distância máxima desejada em km
  maxDistance?: number | null;
}

export interface CarePointNetworkFilterGamaInput {
  // Código da Rede do Plano
  networkCode: number;
  // Código do Grupo do Livreto
  bookletCode: number;
  // Descrição da Especialidade
  specialityDesc: string;
  // Código do Beneficiário
  beneficiaryCode?: number | null;
  // Latidude da localização desejada
  latitude: number;
  // Distância máxima desejada em km (padrão: 8km)
  longitude: number;
  // Distância máxima desejada em km (padrão: 100km)
  maxDistance?: number | null;
  // Sigla do estado
  state?: string | null;
  // Nome da cidade
  city?: string | null;
  // Nome do bairro
  district?: string | null;
}

export interface CarePointNetworkInput {
  // Latidude da localização desejada
  latitude: number;
  // Longitude da localização desejada
  longitude: number;
  // Distância máxima desejada em km (padrão: 8km)
  maxDistance?: number | null;
}

export interface ExamDetailsInput {
  // Identificador do agendamento do plano de cuidado
  carePlanScheduleCode: string;
  // Identificador do item do plano de cudiado
  carePlanItemCode: string;
  // Código TUSS do procedimento
  procedureCode: string;
  // Latitude para busca geolocalizada
  latitude: number;
  // Longitude para busca geolocalizada
  longitude: number;
  // Distância máxima pretendida, em km (padrão: 8 km)
  maxDistance?: number | null;
}

export interface HabitDetailsInput {
  // Identificador do hábito
  habitCode: string;
  // Código do item do plano de cudiado (caso esteja ativo)
  carePlanItemCode?: string | null;
}

export interface HabitsInput {
  // Se true, retorna apenas hábitos ativos
  onlyActive?: boolean | null;
}

export interface LoginInput {
  // CPF ou ID de carteirinha
  user: string;
  // Senha
  password: string;
}

export interface MedicationDetailsInput {
  // Identificador da medicação
  medicationId: string;
}

export interface ProcedureCarePointsInput {
  // Código TUSS do procedimento
  procedureCode: string;
  // Latidude da localização desejada
  latitude: number;
  // Longitude da localização desejada
  longitude: number;
  // Distância máxima desejada em km (padrão: 8km)
  maxDistance?: number | null;
}

export interface ProfessionalScheduleInput {
  // Identificador da agenda do profissional de saúde
  scheduleId: string;
  // Data de inicial da pesquisa de agenda (inclusivo)
  from: string;
  // Data de final da pesquisa de agenda (inclusivo)
  until: string;
  // Estado de busca (abreviação)
  state: string;
  // Cidade de busca
  city: string;
}

export interface PublicationsInput {
  // R = Revistas, M = Manuais
  publicationType: string;
}

export interface SchedulesInput {
  // Estado (abreviatura)
  state: string;
  // Cidade
  city: string;
  // Código da especialidade. Obrigatório se não for médico pessoal
  specialtyCode?: string | null;
  // Indica se deve obter as agendas do médico pessoal
  personalDoctor?: boolean | null;
}

export interface AcceptTermOfUseMutationVariables {
  data: AcceptTermOfUseInput;
}

export interface AcceptTermOfUseMutation {
  // Termo de Aceite
  AcceptTermOfUse: string | null;
}

export interface ActivateHabitMutationVariables {
  data: HabitActivateInput;
}

export interface ActivateHabitMutation {
  // Ativar um hábito (adicionar no plano de cuidado)
  ActivateHabit: string | null;
}

export interface AlertEmergencyMutationVariables {
  sentAt: string;
}

export interface AlertEmergencyMutation {
  // Cria um alerta de emergência para um beneficiário
  AlertEmergency: boolean | null;
}

export interface AnswerQuestionnaireMutationVariables {
  data: AnswerQuestionnaireInput;
}

export interface AnswerQuestionnaireMutation {
  // Resposta do questionário em questão
  AnswerQuestionnaire: {
    // Identificador das respostas dadas.
    responseCode: string;
  } | null;
}

export interface CancelScheduledAppointmentMutationVariables {
  data: CancelAppointmentInput;
}

export interface CancelScheduledAppointmentMutation {
  // Desmarcar consulta
  CancelAppointment: string | null;
}

export interface ChangeBeneficiaryInfoMutationVariables {
  data?: BeneficiaryInfoInput | null;
}

export interface ChangeBeneficiaryInfoMutation {
  // Alterar informações do beneficiário (enviar apenas o que vai mudar)
  ChangeBeneficiaryInfo: {
    // ID da carteirinha
    idHealthCareCard: string;
  } | null;
}

export interface ChangePasswordMutationVariables {
  data: ChangePasswordInput;
}

export interface ChangePasswordMutation {
  // Troca de senha
  ChangePassword: string | null;
}

export interface CreateMedicationMutationVariables {
  data?: MedicationInput | null;
  filters?: ListMedicationInput | null;
}

export interface CreateMedicationMutation {
  // Cria uma medicação para o beneficiário
  CreateMedication: Array<{
    // Identificador do item de medicação
    id: string;
    // Data de início da medicação
    startDate: string;
    // Data de final da medicação, se houver
    endDate: string | null;
    // Indica se é para ser notificado
    notify: boolean;
    // Indica se está ativo
    active: boolean;
    // Horários do dia para a medicação
    dailySchedule: Array<{
      // Identificador do horário de medicação
      id: string;
      // Horário para tomar o medicamento
      hour: string;
    } | null>;
    // Frequencia da medicação
    frequency: string | null;
    // Detalhes do medicamento
    medicine: {
      // Nome
      name: string;
    };
  } | null> | null;
}

export interface DeactivateHabitMutationVariables {
  data: HabitDeactivateInput;
}

export interface DeactivateHabitMutation {
  // Desativar hábito (remover do plano de cuidado)
  DeactivateHabit: string | null;
}

export interface ForgotPasswordMutationVariables {
  user: string;
}

export interface ForgotPasswordMutation {
  // Envio de nova senha por e-mail
  ForgotPassword: string | null;
}

export interface FulfillExamMutationVariables {
  data: FulfillEventInput;
}

export interface FulfillExamMutation {
  // Marcar exame como realizado
  FulfillExam: {
    // Identificador único do agendamento
    id: string;
    // Data mínima para realização
    startDate: string | null;
    // Data limite para realização
    limitDate: string | null;
    // Data agendada, caso tenha agendado
    scheduledFor: string | null;
    // Data realizada, caso tenha cumprido
    doneAt: string | null;
    // Data de não-cumprimento, caso exista
    failedAt: string | null;
  } | null;
}

export interface FulfillHabitMutationVariables {
  data: FulfillEventInput;
}

export interface FulfillHabitMutation {
  // Marcar hábito como cumprido
  FulfillHabit: {
    // Identificador único do agendamento
    id: string;
    // Data mínima para realização
    startDate: string | null;
    // Data limite para realização
    limitDate: string | null;
    // Data agendada, caso tenha agendado
    scheduledFor: string | null;
    // Data realizada, caso tenha cumprido
    doneAt: string | null;
    // Data de não-cumprimento, caso exista
    failedAt: string | null;
  } | null;
}

export interface InsertAllergyMutationVariables {
  data: AllergyInput;
}

export interface InsertAllergyMutation {
  // Inserir alergia
  InsertAllergy: Array<{
    // Causador da alergia
    display: string;
    // Data da primeira vez que foi detectada
    assertedDate: string;
  } | null> | null;
}

export interface InsertVaccineMutationVariables {
  data: VaccineInput;
}

export interface InsertVaccineMutation {
  // Inserir vacinas
  InsertVaccine: Array<{
    // Nome da vacina
    display: string;
    // Data em que tomou
    takenAt: string;
  } | null> | null;
}

export interface InsertVitalSignMutationVariables {
  data?: Array<VitalSignInput | null> | null;
}

export interface InsertVitalSignMutation {
  // Inserir uma ou mais medidas realizada
  InsertVitalSign: string | null;
}

export interface RegisterPushChatMutationVariables {
  data: PushRegisterInput;
}

export interface RegisterPushChatMutation {
  // Registra um usuário e seu aparelho para receber push-notification e usar o chat
  RegisterPushChat: boolean | null;
}

export interface RateDoctorMutationVariables {
  data?: RateDoctorInput | null;
}

export interface RateDoctorMutation {
  // Realizar avaliação de um médico
  RateDoctor: string;
}

export interface RateExamMutationVariables {
  data?: RateExamInput | null;
}

export interface RateExamMutation {
  // Realizar avaliação do Guia Responsável / TimeQ
  RateExam: string;
}

export interface RateGpsMutationVariables {
  data?: RateGpsInput | null;
}

export interface RateGpsMutation {
  // Realizar avaliação do Guia Responsável / TimeQ
  RateGps: string;
}

export interface RegisterUserImageMutationVariables {
  data: FacialRecognitionInput;
}

export interface RegisterUserImageMutation {
  // Cria um usuário e cadastra a foto da biometria
  RegisterUserImage: {
    // A foto é válida para cadastro/checagem?
    isImageValid: boolean | null;
    // Usuário foi reconhecido com sucesso?
    wasUserRecognized: boolean | null;
    // Texto explicativo
    statusText: string | null;
  } | null;
}

export interface ScheduleAppointmentMutationVariables {
  data: AppointmentInput;
}

export interface ScheduleAppointmentMutation {
  // Pré-agendar consulta
  ScheduleAppointment: {
    // Médico responsável pela consulta
    doctor: {
      // Identificador
      id: string | null;
      // Nome
      name: string;
      // CPF
      cpf: string | null;
      // Especialidade
      specialty: {
        // Identificador do tipo (geralmente enum)
        id: string | null;
        // Texto do tipo
        description: string | null;
      };
    } | null;
    // Local da consulta
    location: {
      // Id do estabelecimento
      id: string;
      // Nome do estabelecimento
      name: string | null;
      // Contato (Nome/Tel/E-mail)
      contact: {
        phoneNumbers: Array<string | null> | null;
        name: string | null;
        email: string | null;
        website: string | null;
      } | null;
      // Descrição sobre o estabelecimento
      description: string | null;
      // Endereço
      address: {
        // Estado
        latitude: number | null;
        // Estado
        longitude: number | null;
        // Estado
        state: string;
        // Cidade
        city: string;
        // Bairro
        district: string | null;
        // CEP
        cep: string | null;
        // Rua
        street: string;
        // Número
        addressNumber: string | null;
        // Complemento
        complement: string | null;
        // Instruções de como chegar
        instructions: string | null;
      };
    } | null;
    // Item do plano de cuidado correspondente
    carePlanItem: {
      // Identificador do item no plano de cudiado
      itemCode: string;
      // Informações do procedimento do plano de cuidado: consulta ou exame
      procedure: {
        // Código identificador do evento (procedimento)
        id: string;
        // Nome descritivo do evento (procedimento)
        description: string;
        // Tipo do procedimento do plano de cuidado
        type: ProcedureType | null;
      } | null;
      // Especialidade
      specialty: {
        // Identificador do tipo (geralmente enum)
        id: string | null;
        // Texto do tipo
        description: string | null;
      } | null;
      // Agendamento para um item do plano de cuidado
      schedule: {
        // Identificador único do agendamento
        id: string;
        // Data mínima para realização
        startDate: string | null;
        // Data limite para realização
        limitDate: string | null;
        // Data agendada, caso tenha agendado
        scheduledFor: string | null;
        // Data realizada, caso tenha cumprido
        doneAt: string | null;
        // Data de não-cumprimento, caso exista
        failedAt: string | null;
      } | null;
      // Origem de quem gerou o item no plano de cuidado
      generator: CarePlanItemGenerator | null;
    } | null;
    // Identificador da consulta na base da Prontlife
    appointmentId: string | null;
  } | null;
}

export interface UnregisterPushMutationVariables {
  data: UnregisterPushInput;
}

export interface UnregisterPushMutation {
  // Remove o registro de um usuário para um device específico
  UnregisterPush: boolean | null;
}

export interface UpdateCarePointFavoriteMutationVariables {
  data: UpdateCarePointFavoriteInput;
}

export interface UpdateCarePointFavoriteMutation {
  // Atualiza a flag favorito do ponto de atendimento de acordo com o beneficiário
  UpdateCarePointFavorite: {
    // Codigo do prestador
    practionerId: string | null;
    // Codigo do pronto-atendimento
    carePointId: string | null;
    // Flag que indica se o ponto atendimento foi favoritado pelo beneficiario
    isFavorite: boolean | null;
  } | null;
}

export interface AllergiesQuery {
  // Alergias do beneficiário
  Allergies: Array<{
    // Causador da alergia
    display: string;
    // Data da primeira vez que foi detectada
    assertedDate: string;
  } | null> | null;
}

export interface AppointmentDetailsQueryVariables {
  data: CarePlanCodes;
}

export interface AppointmentDetailsQuery {
  // Informações de uma consulta de um Plano de Cuidado
  AppointmentDetails: {
    // Médico responsável pela consulta
    doctor: {
      // Identificador
      id: string | null;
      // Nome
      name: string;
      // CPF
      cpf: string | null;
      // Especialidade
      specialty: {
        // Identificador do tipo (geralmente enum)
        id: string | null;
        // Texto do tipo
        description: string | null;
      };
    } | null;
    // Local da consulta
    location: {
      // Id do estabelecimento
      id: string;
      // Nome do estabelecimento
      name: string | null;
      // Contato (Nome/Tel/E-mail)
      contact: {
        phoneNumbers: Array<string | null> | null;
        name: string | null;
        email: string | null;
        website: string | null;
      } | null;
      // Descrição sobre o estabelecimento
      description: string | null;
      // Endereço
      address: {
        // Estado
        latitude: number | null;
        // Estado
        longitude: number | null;
        // Estado
        state: string;
        // Cidade
        city: string;
        // Bairro
        district: string | null;
        // CEP
        cep: string | null;
        // Rua
        street: string;
        // Número
        addressNumber: string | null;
        // Complemento
        complement: string | null;
        // Instruções de como chegar
        instructions: string | null;
      };
    } | null;
    // Item do plano de cuidado correspondente
    carePlanItem: {
      // Identificador do item no plano de cudiado
      itemCode: string;
      // Informações do procedimento do plano de cuidado: consulta ou exame
      procedure: {
        // Código identificador do evento (procedimento)
        id: string;
        // Nome descritivo do evento (procedimento)
        description: string;
        // Tipo do procedimento do plano de cuidado
        type: ProcedureType | null;
      } | null;
      // Especialidade
      specialty: {
        // Identificador do tipo (geralmente enum)
        id: string | null;
        // Texto do tipo
        description: string | null;
      } | null;
      // Agendamento para um item do plano de cuidado
      schedule: {
        // Identificador único do agendamento
        id: string;
        // Data mínima para realização
        startDate: string | null;
        // Data limite para realização
        limitDate: string | null;
        // Data agendada, caso tenha agendado
        scheduledFor: string | null;
        // Data realizada, caso tenha cumprido
        doneAt: string | null;
        // Data de não-cumprimento, caso exista
        failedAt: string | null;
      } | null;
      // Origem de quem gerou o item no plano de cuidado
      generator: CarePlanItemGenerator | null;
    } | null;
    // Identificador da consulta na base da Prontlife
    appointmentId: string | null;
  } | null;
}

export interface BeneficiaryInfoQuery {
  // Informações de cadastro do beneficiário
  BeneficiaryInfo: {
    name: string;
    // ID da carteirinha
    idHealthCareCard: string;
    cpf: string | null;
    birthDate: string;
    email: string | null;
    // Telefone (apenas números - inclui country code). Ex: 5511999999999
    phone: string | null;
    // Endereço cadastrado
    address: {
      // Estado
      latitude: number | null;
      // Estado
      longitude: number | null;
      // Estado
      state: string;
      // Cidade
      city: string;
      // Bairro
      district: string | null;
      // CEP
      cep: string | null;
      // Rua
      street: string;
      // Número
      addressNumber: string | null;
      // Complemento
      complement: string | null;
      // Instruções de como chegar
      instructions: string | null;
    };
    // Gênero (apenas uma letra)
    gender: string | null;
    // Código do plano)
    codePlan: string | null;
  } | null;
}

export interface CampaignQuestionnairesQueryVariables {
  data?: CampaignQuestionnairesInput | null;
}

export interface CampaignQuestionnairesQuery {
  // Obtém os questionários relativos à campanha passada
  CampaignQuestionnaires: Array<{
    // Identificador
    code: string;
    // Nome
    name: string;
    // Identificador do gabarito (últimas respostas submetidas)
    responseCode: string | null;
    // Indica se é obrigatória informação do IMC
    imcRequired: boolean | null;
    // Peso kg (caso precise de IMC)
    weight: number | null;
    // Altura cm (caso precise de IMC)
    height: number | null;
    // Respondido em (null caso não tenha sido respondido)
    questionGroups: Array<{
      // Identificador
      code: string;
      // Nome
      name: string;
      // Mínimo de questões a serem respondidas
      minimumToAnswer: number | null;
      // Total de questões respondidas
      totalAnswered: number | null;
      // Perguntas do Tipo de Avaliação
      questions: Array<{
        // Código da pergunta
        code: string;
        // Descrição
        description: string;
        // Textos explicativos, se necessário
        descritive: Array<string | null> | null;
        // Determina se a questão é ou não obrigatória
        required: boolean;
        // Tipo da resposta
        type: QuestionType;
        // Valor mínimo (caso seja numérico)
        minValue: number | null;
        // Valor máximo (caso seja numérico
        maxValue: number | null;
        // Alternativas de resposta
        alternatives: Array<{
          // Código do item da resposta
          code: string;
          // Descrição do item da resposta (para tipos 1 e 2)
          display: string | null;
          // Valor máximo da resposta
          conditionToShow: Array<{
            // Identificador da questão
            questionCode: string;
            // Identificador da resposta
            answerCode: string;
          } | null> | null;
        } | null>;
        // Resposta(s) escolhida(s), caso tenha respondido
        chosenAnswers: Array<{
          // Identificador do item da resposta
          code: string | null;
          // Resposta digitada
          display: string | null;
        } | null> | null;
        // Condição para ser mostrada (se houver) - combinação entre pergunta e resposta
        conditionToShow: Array<{
          // Identificador da questão
          questionCode: string;
          // Identificador da resposta
          answerCode: string;
        } | null> | null;
      } | null>;
    } | null> | null;
  } | null> | null;
}

export interface CampaignsQuery {
  // Campanhas de um beneficiário e seus dependentes
  Campaigns: Array<{
    // Código da camapnha
    code: string;
    // Nome da campanha
    name: string;
    // Resumo dos questionários
    questionnaires: Array<{
      // Código do questionário
      code: string;
      // Nome do questionário
      name: string;
      // Identificador de um conjunto de respostas já dadas, se houver
      responseCode: string | null;
      // Indica se está completamente respondido
      isCompletelyAnswered: boolean | null;
      // Indica se deve ser respondido
      required: boolean | null;
    } | null>;
  } | null> | null;
}

export interface CarePlanEventsQueryVariables {
  data: CarePlanItemsInput;
}

export interface CarePlanEventsQuery {
  // Lista os próximos eventos a serem realizados do plano de cuidado
  CarePlanEvents: Array<{
    // Identificador do item no plano de cudiado
    itemCode: string;
    // Informações do procedimento do plano de cuidado: consulta ou exame
    procedure: {
      // Código identificador do evento (procedimento)
      id: string;
      // Nome descritivo do evento (procedimento)
      description: string;
      // Tipo do procedimento do plano de cuidado
      type: ProcedureType | null;
    } | null;
    // Especialidade
    specialty: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    } | null;
    // Agendamento para um item do plano de cuidado
    schedule: {
      // Identificador único do agendamento
      id: string;
      // Data mínima para realização
      startDate: string | null;
      // Data limite para realização
      limitDate: string | null;
      // Data agendada, caso tenha agendado
      scheduledFor: string | null;
      // Data realizada, caso tenha cumprido
      doneAt: string | null;
      // Data de não-cumprimento, caso exista
      failedAt: string | null;
    } | null;
    // Origem de quem gerou o item no plano de cuidado
    generator: CarePlanItemGenerator | null;
  } | null> | null;
}

export interface CarePlanProgressQuery {
  // Cumprimento do Plano de Cuidado (caso o usuário já tenha um aprovado)
  CarePlanProgress: {
    // Identificador do Plano de Cuidado (caso já tenha sido gerado)
    code: string | null;
    // Progresso do plano de cuidado, de 0 a 1
    progress: number | null;
  } | null;
}

export interface CarePointsNetworkCityQueryVariables {
  data?: CarePointNetworkCityInput | null;
}

export interface CarePointsNetworkCityQuery {
  // Obtém lista de cidades que possuem pronto-atendimento
  CarePointsNetworkCity: Array<{
    // Sigla do estado onde se localiza o pronto-atendimento
    uf: string | null;
    // Nome do estado onde se localiza o pronto-atendimento
    ufName: string | null;
    // Código da cidade onde se localiza o pronto-atendimento
    cityCode: number | null;
    // Nome da cidade onde se localiza o pronto-atendimento
    cityName: string | null;
    // Quantidade de cidades disponíveis
    total: number | null;
  } | null> | null;
}

export interface CarePointsNetworkDistrictQueryVariables {
  data?: CarePointNetworkDistrictInput | null;
}

export interface CarePointsNetworkDistrictQuery {
  // Obtém lista de bairros que possuem pronto-atendimento
  CarePointsNetworkDistrict: Array<{
    // Sigla do estado onde se localiza o pronto-atendimento
    uf: string | null;
    // Nome do estado onde se localiza o pronto-atendimento
    ufName: string | null;
    // Código da cidade onde se localiza o pronto-atendimento
    cityCode: number | null;
    // Nome da cidade onde se localiza o pronto-atendimento
    cityName: string | null;
    // Nome do bairro onde se localiza o pronto-atendimento
    districtName: string | null;
    // Quantidade de bairros para a operadora GAMA
    total: number | null;
  } | null> | null;
}

export interface CarePointsNetworkPerPlanQueryVariables {
  data?: CarePointNetworkPerPlanInput | null;
}

export interface CarePointsNetworkPerPlanQuery {
  // Obtém o código da rede
  CarePointsNetworkPerPlan: {
    // Nome comercial da rede de atendimento
    comercialName: string | null;
    // Número de registro da rede de atendimento
    registryNumber: string | null;
    // Código da rede de atendimento
    networkCode: number | null;
  } | null;
}

export interface CarePointsNetworkSpecialityQueryVariables {
  data?: CarePointNetworkSpecialityInput | null;
}

export interface CarePointsNetworkSpecialityQuery {
  // Obtém lista de especialidades por filtro
  CarePointsNetworkSpeciality: Array<{
    // Código da especialidade principal
    specialityMainCode: number | null;
    // Código da especialidade divulgação
    specialityPublicationCode: number | null;
    // Descrição da especialidade
    specialityDesc: string | null;
  } | null> | null;
}

export interface CarePointsNetworkStateQueryVariables {
  data?: CarePointNetworkStateInput | null;
}

export interface CarePointsNetworkStateQuery {
  // Obtém lista de estados que possuem pronto-atendimento
  CarePointsNetworkState: Array<{
    // Sigla do estado onde se localiza o pronto-atendimento
    uf: string | null;
    // Nome do estado onde se localiza o pronto-atendimento
    ufName: string | null;
    // Quantidade do número de atendimento por estado, válido somente para operadora GAMA
    total: number | null;
  } | null> | null;
}

export interface CarePointsNetworkTypeQueryVariables {
  data?: CarePointNetworkTypeInput | null;
}

export interface CarePointsNetworkTypeQuery {
  // Obtém lista de tipos de prestador
  CarePointsNetworkType: Array<{
    // Sigla do estado onde se localiza o pronto-atendimento
    uf: string | null;
    // Nome do estado onde se localiza o pronto-atendimento
    ufName: string | null;
    // Código da cidade onde se localiza o pronto-atendimento
    cityCode: number | null;
    // Nome da cidade onde se localiza o pronto-atendimento
    cityName: string | null;
    // Nome do bairro onde se localiza o pronto-atendimento
    districtName: string | null;
    // Código do tipo de prestador
    typeCode: number | null;
    // Descrição do tipo de prestador
    typeDesc: string | null;
    // Código do livreto
    bookletCode: string | null;
    // Descrição do livreto
    bookletDesc: string | null;
  } | null> | null;
}

export interface ChatTokenQuery {
  // Obter dados para início ou continuação de conversa
  ChatToken: {
    // Identificador da conversa
    chatId: string;
    // Token para autorização do chat
    wsToken: string;
  } | null;
}

export interface CheckFacialRecognitionRegisterQuery {
  // Checa se o usuário já tem cadastro no serviço biométrico
  CheckFacialRecognitionRegister: boolean | null;
}

export interface ConditionQuery {
  // Doenças (Condition) registradas do beneficiário
  Condition: Array<{
    // Descrição da doença
    display: string;
    // Data de cadastro da doença
    assertedDate: string;
  } | null> | null;
}

export interface InsertConditionMutation {
  data: any;
}

export interface InsertConditionMutationVariables {
  data: any;
}

export interface ConditionFragment {
  display: any;
  assertedDate: any;
}

export interface EmergencyCareFilterQueryVariables {
  data: CarePointNetworkFilterInput;
}

export interface EmergencyCareFilterQuery {
  // Obtém lista de pronto-atendimentos baseado em filtros
  EmergencyCareFilter: Array<{
    // Local do pronto-atendimento
    carePoint: {
      // Id do estabelecimento
      id: string;
      // Nome do estabelecimento
      name: string | null;
      // Contato (Nome/Tel/E-mail)
      contact: {
        phoneNumbers: Array<string | null> | null;
        name: string | null;
        email: string | null;
        website: string | null;
      } | null;
      // Descrição sobre o estabelecimento
      description: string | null;
      // Endereço
      address: {
        // Estado
        latitude: number | null;
        // Estado
        longitude: number | null;
        // Estado
        state: string;
        // Cidade
        city: string;
        // Bairro
        district: string | null;
        // CEP
        cep: string | null;
        // Rua
        street: string;
        // Número
        addressNumber: string | null;
        // Complemento
        complement: string | null;
        // Instruções de como chegar
        instructions: string | null;
      };
    } | null;
    // Código do prestador
    practitionerId: string | null;
    // Distância relativa a posição geográfica passada (em km)
    distance: number | null;
    // Flag que indica se o ponto de atendimento foi favoritado pelo beneficiário
    isFavorite: boolean | null;
  } | null> | null;
}

export interface EmergencyCareGamaFilterQueryVariables {
  data: CarePointNetworkFilterGamaInput;
}

export interface EmergencyCareGamaFilterQuery {
  // Obtém lista de pronto-atendimentos da GAMA baseado em filtros
  EmergencyCareGamaFilter: Array<{
    // Local do pronto-atendimento
    carePoint: {
      // Id do estabelecimento
      id: string;
      // Nome do estabelecimento
      name: string | null;
      // Contato (Nome/Tel/E-mail)
      contact: {
        phoneNumbers: Array<string | null> | null;
        name: string | null;
        email: string | null;
        website: string | null;
      } | null;
      // Descrição sobre o estabelecimento
      description: string | null;
      // Endereço
      address: {
        // Estado
        latitude: number | null;
        // Estado
        longitude: number | null;
        // Estado
        state: string;
        // Cidade
        city: string;
        // Bairro
        district: string | null;
        // CEP
        cep: string | null;
        // Rua
        street: string;
        // Número
        addressNumber: string | null;
        // Complemento
        complement: string | null;
        // Instruções de como chegar
        instructions: string | null;
      };
    } | null;
    // Código do prestador
    practitionerId: string | null;
    // Distância relativa a posição geográfica passada (em km)
    distance: number | null;
    // Flag que indica se o ponto de atendimento foi favoritado pelo beneficiário
    isFavorite: boolean | null;
  } | null> | null;
}

export interface EmergencyCareQueryVariables {
  data: CarePointNetworkInput;
}

export interface EmergencyCareQuery {
  // Obtém lista de pronto-atendimentos baseado em localização
  EmergencyCare: Array<{
    // Local do pronto-atendimento
    carePoint: {
      // Id do estabelecimento
      id: string;
      // Nome do estabelecimento
      name: string | null;
      // Contato (Nome/Tel/E-mail)
      contact: {
        phoneNumbers: Array<string | null> | null;
        name: string | null;
        email: string | null;
        website: string | null;
      } | null;
      // Descrição sobre o estabelecimento
      description: string | null;
      // Endereço
      address: {
        // Estado
        latitude: number | null;
        // Estado
        longitude: number | null;
        // Estado
        state: string;
        // Cidade
        city: string;
        // Bairro
        district: string | null;
        // CEP
        cep: string | null;
        // Rua
        street: string;
        // Número
        addressNumber: string | null;
        // Complemento
        complement: string | null;
        // Instruções de como chegar
        instructions: string | null;
      };
    } | null;
    // Código do prestador
    practitionerId: string | null;
    // Distância relativa a posição geográfica passada (em km)
    distance: number | null;
    // Flag que indica se o ponto de atendimento foi favoritado pelo beneficiário
    isFavorite: boolean | null;
  } | null> | null;
}

export interface ExamDetailsQueryVariables {
  data: ExamDetailsInput;
}

export interface ExamDetailsQuery {
  // Obter detalhes de um exame do plano de cuidado
  ExamDetails: {
    // Detalhes do item no plano de cuidado
    carePlanItem: {
      // Identificador do item no plano de cudiado
      itemCode: string;
      // Informações do procedimento do plano de cuidado: consulta ou exame
      procedure: {
        // Código identificador do evento (procedimento)
        id: string;
        // Nome descritivo do evento (procedimento)
        description: string;
        // Tipo do procedimento do plano de cuidado
        type: ProcedureType | null;
      } | null;
      // Especialidade
      specialty: {
        // Identificador do tipo (geralmente enum)
        id: string | null;
        // Texto do tipo
        description: string | null;
      } | null;
      // Agendamento para um item do plano de cuidado
      schedule: {
        // Identificador único do agendamento
        id: string;
        // Data mínima para realização
        startDate: string | null;
        // Data limite para realização
        limitDate: string | null;
        // Data agendada, caso tenha agendado
        scheduledFor: string | null;
        // Data realizada, caso tenha cumprido
        doneAt: string | null;
        // Data de não-cumprimento, caso exista
        failedAt: string | null;
      } | null;
      // Origem de quem gerou o item no plano de cuidado
      generator: CarePlanItemGenerator | null;
    };
    // Laboratórios disponíveis para entrar em contato
    laboratories: Array<{
      // Local do pronto-atendimento
      carePoint: {
        // Id do estabelecimento
        id: string;
        // Nome do estabelecimento
        name: string | null;
        // Contato (Nome/Tel/E-mail)
        contact: {
          phoneNumbers: Array<string | null> | null;
          name: string | null;
          email: string | null;
          website: string | null;
        } | null;
        // Descrição sobre o estabelecimento
        description: string | null;
        // Endereço
        address: {
          // Estado
          latitude: number | null;
          // Estado
          longitude: number | null;
          // Estado
          state: string;
          // Cidade
          city: string;
          // Bairro
          district: string | null;
          // CEP
          cep: string | null;
          // Rua
          street: string;
          // Número
          addressNumber: string | null;
          // Complemento
          complement: string | null;
          // Instruções de como chegar
          instructions: string | null;
        };
      } | null;
      // Código do prestador
      practitionerId: string | null;
      // Distância relativa a posição geográfica passada (em km)
      distance: number | null;
      // Flag que indica se o ponto de atendimento foi favoritado pelo beneficiário
      isFavorite: boolean | null;
    } | null> | null;
  } | null;
}

export interface FamilyHistoryQuery {
  // Histórico familiar (doenças/condições)
  FamilyHistory: Array<{
    // Histórico a mostrar
    display: string;
    // Parente que tem a condição
    relationship: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    } | null;
    // Data de registo do histórico
    registerDate: string | null;
  } | null> | null;
}

export interface InsertFamilyHIstoryMutation {}

export interface InsertFamilyHIstoryMutationVariables {}

export interface FamilyHistoryFragment {
  display: any;
  relationship: { description: any };
}
export interface HabitDetailsQueryVariables {
  data: HabitDetailsInput;
}

export interface HabitDetailsQuery {
  // Obtém os detalhes de um hábito. Se houver código do item do plano de cuidado, retorna histórico.
  HabitDetails: {
    // Código do hábito
    id: string;
    // Tipo do hábito
    type: {
      // Identificador do tipo de hábito
      id: string | null;
      // Descreve os tipos de hábitos padronizados para íconografia.
      area: HabitArea | null;
      // Descritivo do tipo do hábito
      display: string | null;
    };
    // Descrição do hábito
    description: string;
    // Periodicidade
    frequency: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    };
    // Aviso sobre o hábito
    warning: string | null;
    // Observação sobre o hábito
    observation: string | null;
    // Idade mínima
    minimumAge: string | null;
    // Idade máxima
    maximumAge: string | null;
    // Sexo de cumprimento
    sex: Sex;
    // Item do plano de cuidado (apenas se estiver ativo)
    carePlanItem: {
      // Identificador do item no plano de cudiado
      itemCode: string;
      // Identificador do hábito
      habitCode: string;
      // Periodicidade
      frequency: {
        // Identificador do tipo (geralmente enum)
        id: string | null;
        // Texto do tipo
        description: string | null;
      } | null;
      // Origem de quem gerou o item no plano de cuidado
      generator: CarePlanItemGenerator | null;
      // Todas as agendas do hábito (inclusive histórico)
      schedules: Array<{
        // Identificador único do agendamento
        id: string;
        // Data mínima para realização
        startDate: string | null;
        // Data limite para realização
        limitDate: string | null;
        // Data agendada, caso tenha agendado
        scheduledFor: string | null;
        // Data realizada, caso tenha cumprido
        doneAt: string | null;
        // Data de não-cumprimento, caso exista
        failedAt: string | null;
      } | null> | null;
      // Próxima agendaa do hábito a cumprir, agrupados quando for o mesmo dia.
      nextSchedules: Array<{
        // Identificador único do agendamento
        id: string;
        // Data mínima para realização
        startDate: string | null;
        // Data limite para realização
        limitDate: string | null;
        // Data agendada, caso tenha agendado
        scheduledFor: string | null;
        // Data realizada, caso tenha cumprido
        doneAt: string | null;
        // Data de não-cumprimento, caso exista
        failedAt: string | null;
      } | null> | null;
    } | null;
  } | null;
}

export interface HabitsQueryVariables {
  data: HabitsInput;
}

export interface HabitsQuery {
  // Lista hábitos com opção de estarem ou não no plano de cuidado (ativos)
  Habits: Array<{
    // Código do hábito
    id: string;
    // Tipo do hábito
    type: {
      // Identificador do tipo de hábito
      id: string | null;
      // Descreve os tipos de hábitos padronizados para íconografia.
      area: HabitArea | null;
      // Descritivo do tipo do hábito
      display: string | null;
    };
    // Descrição do hábito
    description: string;
    // Periodicidade
    frequency: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    };
    // Aviso sobre o hábito
    warning: string | null;
    // Observação sobre o hábito
    observation: string | null;
    // Idade mínima
    minimumAge: string | null;
    // Idade máxima
    maximumAge: string | null;
    // Sexo de cumprimento
    sex: Sex;
    // Item do plano de cuidado (apenas se estiver ativo)
    carePlanItem: {
      // Identificador do item no plano de cudiado
      itemCode: string;
      // Identificador do hábito
      habitCode: string;
      // Periodicidade
      frequency: {
        // Identificador do tipo (geralmente enum)
        id: string | null;
        // Texto do tipo
        description: string | null;
      } | null;
      // Origem de quem gerou o item no plano de cuidado
      generator: CarePlanItemGenerator | null;
      // Todas as agendas do hábito (inclusive histórico)
      schedules: Array<{
        // Identificador único do agendamento
        id: string;
        // Data mínima para realização
        startDate: string | null;
        // Data limite para realização
        limitDate: string | null;
        // Data agendada, caso tenha agendado
        scheduledFor: string | null;
        // Data realizada, caso tenha cumprido
        doneAt: string | null;
        // Data de não-cumprimento, caso exista
        failedAt: string | null;
      } | null> | null;
      // Próxima agendaa do hábito a cumprir, agrupados quando for o mesmo dia.
      nextSchedules: Array<{
        // Identificador único do agendamento
        id: string;
        // Data mínima para realização
        startDate: string | null;
        // Data limite para realização
        limitDate: string | null;
        // Data agendada, caso tenha agendado
        scheduledFor: string | null;
        // Data realizada, caso tenha cumprido
        doneAt: string | null;
        // Data de não-cumprimento, caso exista
        failedAt: string | null;
      } | null> | null;
    } | null;
  } | null> | null;
}

export interface HealthCareCardQuery {
  // Obter informações da carteira de saúde do usuário
  HealthCareCard: {
    // ID da carteirinha
    idHealthCareCard: string;
    // Nome do beneficiário
    name: string | null;
    // Dados do plano de saúde
    planData: {
      // Tipo de contratação do plano
      type: string | null;
      // Registro do plano na ANS
      ansRegister: string | null;
      // Número da CNS
      cns: string | null;
      // Contratado em
      contractedAt: string | null;
      // Regulação do plano
      regulation: string | null;
      // Abrangência do plano
      comprehensiveness: string | null;
      // Acomodação do plano
      accommodation: string | null;
      // Data de início de vigencia
      startValidity: string | null;
      // Nome do Plano
      namePlan: string | null;
      // Nome da Rede
      nameNetwork: string | null;
      // Nome da Fantasia
      contractor: string | null;
      // Nome do Contratante
      segmentation: string | null;
    };
    // Contatos do QSaúde
    contact: {
      // Celular para WhatsApp
      cellPhone: string;
      // Telefone fixo para contato
      phone: string | null;
      // E-mail
      email: string | null;
    };
  } | null;
}

export interface ListMedicationQueryVariables {
  data: ListMedicationInput;
}

export interface ListMedicationQuery {
  // Obtém as medicações cadastradas para um beneficiários
  ListMedication: Array<{
    // Identificador do item de medicação
    id: string;
    // Data de início da medicação
    startDate: string;
    // Data de final da medicação, se houver
    endDate: string | null;
    // Indica se é para ser notificado
    notify: boolean;
    // Indica se está ativo
    active: boolean;
    // Horários do dia para a medicação
    dailySchedule: Array<{
      // Identificador do horário de medicação
      id: string;
      // Horário para tomar o medicamento
      hour: string;
    } | null>;
    // Frequencia da medicação
    frequency: string | null;
    // Detalhes do medicamento
    medicine: {
      // Nome
      name: string;
    };
  } | null> | null;
}

export interface LoginQueryVariables {
  data: LoginInput;
}

export interface LoginQuery {
  // Autenticação do beneficiário
  Login: {
    // JWT token
    token: string;
    // Cód beneficiário
    beneficiaryCode: string;
    // ID da carteirinha
    idHealthCareCard: string;
    cpf: string | null;
    birthDate: string;
    city: string;
    state: string;
    // Nome
    name: string;
    // E-mail
    email: string | null;
    // Beneficiário já tem biometria registrada? null = não foi possível obter essa informação
    isRegisteredInBiometricService: boolean | null;
    // Indica se é dependente ou titular
    dependant: boolean | null;
    // Indica se o usuário deve trocar a senha após o acesso
    mustChangePassword: boolean | null;
    // Termo de consentimento
    consentTerm: string | null;
    // Indica se possui linha do cuidado
    hasCarePlan: boolean | null;
    // Dependentes, caso seja titular e os tenha
    dependants: Array<{
      // JWT token
      token: string;
      // Cód beneficiário
      beneficiaryCode: string;
      // ID da carteirinha
      idHealthCareCard: string;
      cpf: string | null;
      birthDate: string;
      city: string;
      state: string;
      // Nome
      name: string;
      // E-mail
      email: string | null;
      // Beneficiário já tem biometria registrada? null = não foi possível obter essa informação
      isRegisteredInBiometricService: boolean | null;
      // Indica se é dependente ou titular
      dependant: boolean | null;
      // Indica se o usuário deve trocar a senha após o acesso
      mustChangePassword: boolean | null;
      // Termo de consentimento
      consentTerm: string | null;
      // Indica se possui linha do cuidado
      hasCarePlan: boolean | null;
    } | null> | null;
  } | null;
}

export interface MedicationDetailsQueryVariables {
  data: MedicationDetailsInput;
}

export interface MedicationDetailsQuery {
  // Obtém detalhes de uma medicação
  MedicationDetails: {
    // Identificador do item de medicação
    id: string;
    // Data de início da medicação
    startDate: string;
    // Data de final da medicação, se houver
    endDate: string | null;
    // Indica se é para ser notificado
    notify: boolean;
    // Indica se está ativo
    active: boolean;
    // Orientações para a medicação, se houver
    orientation: string | null;
    // Horários do dia para a medicação
    dailySchedule: Array<{
      // Identificador do horário de medicação
      id: string;
      // Horário para tomar o medicamento
      hour: string;
    } | null>;
    // Frequencia da medicação
    frequency: string | null;
    // Detalhes do medicamento
    medicine: {
      // Nome
      name: string;
      // Nome popular
      commonName: string | null;
      // Aspecto
      aspect: string | null;
      // Dosagem
      dosage: {
        // Quantidade
        amount: number | null;
        // Unidade de medida
        unit: string;
      } | null;
      // Instruções
      instructions: string | null;
    };
  } | null;
}

export interface NewsQuery {
  // Obtém lista de notícias por operadora
  News: {
    // Response do serviço de notícias
    content: Array<{
      // Código da notícia
      id: number | null;
      // Título da notícia
      title: string | null;
      // Url da notícia
      url: string | null;
      // Status da notícia
      active: boolean | null;
      // Data da notícia
      publicationDate: string | null;
      // Texto da notícia
      description: string | null;
      // Imagem da notícia
      image: string | null;
    } | null> | null;
  } | null;
}

export interface PastEventsQueryVariables {
  data: CarePlanItemsInput;
}

export interface PastEventsQuery {
  // Lista os eventos já realizados ou expirados
  PastEvents: Array<{
    // Identificador do item no plano de cudiado
    itemCode: string;
    // Informações do procedimento do plano de cuidado: consulta ou exame
    procedure: {
      // Código identificador do evento (procedimento)
      id: string;
      // Nome descritivo do evento (procedimento)
      description: string;
      // Tipo do procedimento do plano de cuidado
      type: ProcedureType | null;
    } | null;
    // Especialidade
    specialty: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    } | null;
    // Agendamento para um item do plano de cuidado
    schedule: {
      // Identificador único do agendamento
      id: string;
      // Data mínima para realização
      startDate: string | null;
      // Data limite para realização
      limitDate: string | null;
      // Data agendada, caso tenha agendado
      scheduledFor: string | null;
      // Data realizada, caso tenha cumprido
      doneAt: string | null;
      // Data de não-cumprimento, caso exista
      failedAt: string | null;
    } | null;
    // Origem de quem gerou o item no plano de cuidado
    generator: CarePlanItemGenerator | null;
  } | null> | null;
}

export interface ProcedureCarePointsQueryVariables {
  data: ProcedureCarePointsInput;
}

export interface ProcedureCarePointsQuery {
  // Obtém estabelecimentos para um procedimento (geolocalizada)
  ProcedureCarePoints: Array<{
    // Local do pronto-atendimento
    carePoint: {
      // Id do estabelecimento
      id: string;
      // Nome do estabelecimento
      name: string | null;
      // Contato (Nome/Tel/E-mail)
      contact: {
        phoneNumbers: Array<string | null> | null;
        name: string | null;
        email: string | null;
        website: string | null;
      } | null;
      // Descrição sobre o estabelecimento
      description: string | null;
      // Endereço
      address: {
        // Estado
        latitude: number | null;
        // Estado
        longitude: number | null;
        // Estado
        state: string;
        // Cidade
        city: string;
        // Bairro
        district: string | null;
        // CEP
        cep: string | null;
        // Rua
        street: string;
        // Número
        addressNumber: string | null;
        // Complemento
        complement: string | null;
        // Instruções de como chegar
        instructions: string | null;
      };
    } | null;
    // Código do prestador
    practitionerId: string | null;
    // Distância relativa a posição geográfica passada (em km)
    distance: number | null;
    // Flag que indica se o ponto de atendimento foi favoritado pelo beneficiário
    isFavorite: boolean | null;
  } | null> | null;
}

export interface ProceduresQuery {
  Procedures: Array<{
    // Identificador
    id: string | null;
    // Data realizada
    registeredAt: string | null;
    // Nome descritivo
    display: string;
    // Detlahes / Observações
    detail: string | null;
  } | null> | null;
}

export interface InsertProcedureMutation {
  // Inserir procedures
  InsertProcedure: Array<{} | null> | null;
}
export interface InsertProcedureMutationVariables {
  data: any;
}

export interface ProfessionalScheduleQueryVariables {
  data: ProfessionalScheduleInput;
}

export interface ProfessionalScheduleQuery {
  // Obtém dias e horários de disponibilidade de um profissional de saúde específico
  ProfessionalSchedule: {
    // Identificador da agenda de atendimento
    scheduleId: string;
    // Horários disponíveis agrupados por dia
    daySlots: Array<{
      // Dia do atendimento
      day: string;
      // Horários disponíveis para o atendimento
      slots: Array<{
        // Horário de início do atendimento
        start: string;
        // Horário de término do atendimento
        end: string | null;
      } | null>;
    } | null>;
  } | null;
}

export interface PublicationsQueryVariables {
  data: PublicationsInput;
}

export interface PublicationsQuery {
  // Obtém lista de publicações (Revistas ou Manuais)
  Publications: {
    // Response do serviço de publicações
    content: Array<{
      // Código da publicação
      id: number | null;
      // Nome da publicação
      name: string | null;
      // Resumo da publicação
      description: string | null;
      // Tipo da publicação R = Revista, M = Manual
      publicationType: string | null;
      // Data da publicação
      publicationDate: string | null;
      // Imagem thumbnail em base64
      thumbnail: string | null;
      // Arquivo
      file: {
        // Código do arquivo
        id: number | null;
        // Hash do arquivo
        hash: string | null;
        // Url do arquivo
        url: string | null;
      } | null;
    } | null> | null;
  } | null;
}

export interface QuestionnairesFeedbackQuery {
  // Obter devolutiva do questionário de saúde
  QuestionnairesFeedback: Array<{
    // Nome do questionário que gerou a devolutiva
    questionnaireName: string | null;
    // Código identificador da última resposta
    responseCode: string | null;
    // Data da última devolutiva
    lastAnsweredAt: string | null;
    // Itens da devolutiva
    items: Array<{
      // Assunto do item da devolutiva
      topic: {
        // Possível ID para diferenciação entre os tópicos
        id: string;
        // Título do tópico a ser exibido
        title: string | null;
        // Enum único com a referência do tópico
        reference: QuestionnaireFeedbackTopicReferenceEnum | null;
      } | null;
      // Descrição do item da devolutiva
      description: string;
    } | null>;
  } | null> | null;
}

export interface RecentVitalSignsQueryVariables {
  data: Array<VitalSignType | null>;
}

export interface RecentVitalSignsQuery {
  // Obtém as últimas medidas realizadas dos parâmetros enviados
  RecentVitalSigns: Array<{
    // Tipo do sinal vital (Enum VitalSignType)
    type: VitalSignType;
    // Unidade de medida
    measureUnit: string;
    // Data da medição
    measuredAt: string | null;
    // Medida do sinal vital
    measure: {
      // Quantidade
      amount: number | null;
      // Unidade de medida
      unit: string;
    } | null;
    // Sub-medidas (usado, por exemplo, na medida de pressão, que tem mais de uma)
    subMeasures: Array<{
      // Tipo do sinal vital (Enum VitalSignType)
      type: VitalSignType;
      // Data da medição
      measuredAt: string | null;
      // Medida do sinal vital
      measure: {
        // Quantidade
        amount: number | null;
        // Unidade de medida
        unit: string;
      } | null;
    } | null> | null;
  } | null> | null;
}

export interface ResponsibleGuideQuery {
  // Informações do GPS (guia responsável)
  ResponsibleGuide: {
    // Código do GPS
    code: string;
    name: string | null;
    email: string | null;
  } | null;
}

export interface SpecialtiesQuery {
  // Obtém as especialidades dos profissionais de saúde disponíveis para o beneficiário
  Specialties: Array<{
    // Identificador do tipo (geralmente enum)
    id: string | null;
    // Texto do tipo
    description: string | null;
  } | null> | null;
}

export interface SpecialtySchedulesQueryVariables {
  data: SchedulesInput;
}

export interface SpecialtySchedulesQuery {
  // Obter agendas de especialistas ou do médico pessoal
  SpecialtySchedules: {
    // Especialidade em questão
    specialty: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    } | null;
    // Agendas para os especialistas
    schedules: Array<{
      // Identificador da agenda do especialista
      scheduleId: string;
      // Identificador do profissional
      professionalId: string;
      // Nome do profissional
      professionalName: string;
      // Ponto de atendimento
      carePoint: {
        // Id do estabelecimento
        id: string;
        // Nome do estabelecimento
        name: string | null;
        // Contato (Nome/Tel/E-mail)
        contact: {
          phoneNumbers: Array<string | null> | null;
          name: string | null;
          email: string | null;
          website: string | null;
        } | null;
        // Descrição sobre o estabelecimento
        description: string | null;
        // Endereço
        address: {
          // Estado
          latitude: number | null;
          // Estado
          longitude: number | null;
          // Estado
          state: string;
          // Cidade
          city: string;
          // Bairro
          district: string | null;
          // CEP
          cep: string | null;
          // Rua
          street: string;
          // Número
          addressNumber: string | null;
          // Complemento
          complement: string | null;
          // Instruções de como chegar
          instructions: string | null;
        };
      };
    } | null> | null;
  } | null;
}

export interface StatusAttendanceQuery {
  // Obtém lista de senhas solicitadas pelo usuário
  StatusAttendance: Array<{
    // Senha do atendimento
    attendance: string | null;
    // Status do atendimento
    status: string | null;
    // Data da solicitação
    dateSolicitation: string | null;
    // Data da autorização
    dateAuthorization: string | null;
    // Data da validade
    dateValidity: string | null;
    // Nome do prestador
    practitionerName: string | null;
    // Tipo da solicitação
    typeSolicitation: string | null;
  } | null> | null;
}

export interface TermQuery {
  // Html do termo de consentimento
  Term: string | null;
}

export interface VaccinesQuery {
  // Vacinas tomadas pelo beneficiário
  Vaccines: Array<{
    // Nome da vacina
    display: string;
    // Data em que tomou
    takenAt: string;
  } | null> | null;
}

export interface VitalSignQueryVariables {
  vitalSign: VitalSignType;
  count: number;
}

export interface VitalSignQuery {
  // Obtém as últimas medidas realizadas para um sinal vital específico
  VitalSign: Array<{
    // Tipo do sinal vital (Enum VitalSignType)
    type: VitalSignType;
    // Unidade de medida
    measureUnit: string;
    // Data da medição
    measuredAt: string | null;
    // Medida do sinal vital
    measure: {
      // Quantidade
      amount: number | null;
      // Unidade de medida
      unit: string;
    } | null;
    // Sub-medidas (usado, por exemplo, na medida de pressão, que tem mais de uma)
    subMeasures: Array<{
      // Tipo do sinal vital (Enum VitalSignType)
      type: VitalSignType;
      // Data da medição
      measuredAt: string | null;
      // Medida do sinal vital
      measure: {
        // Quantidade
        amount: number | null;
        // Unidade de medida
        unit: string;
      } | null;
    } | null> | null;
  } | null> | null;
}

export interface AllergyFragment {
  // Causador da alergia
  display: string;
  // Data da primeira vez que foi detectada
  assertedDate: string;
}

export interface SlotFragment {
  // Horário de início do atendimento
  start: string;
  // Horário de término do atendimento
  end: string | null;
}

export interface DaySlotFragment {
  // Dia do atendimento
  day: string;
  // Horários disponíveis para o atendimento
  slots: Array<{
    // Horário de início do atendimento
    start: string;
    // Horário de término do atendimento
    end: string | null;
  } | null>;
}

export interface ScheduleFragment {
  // Identificador da agenda do especialista
  scheduleId: string;
  // Identificador do profissional
  professionalId: string;
  // Nome do profissional
  professionalName: string;
  // Ponto de atendimento
  carePoint: {
    // Id do estabelecimento
    id: string;
    // Nome do estabelecimento
    name: string | null;
    // Contato (Nome/Tel/E-mail)
    contact: {
      phoneNumbers: Array<string | null> | null;
      name: string | null;
      email: string | null;
      website: string | null;
    } | null;
    // Descrição sobre o estabelecimento
    description: string | null;
    // Endereço
    address: {
      // Estado
      latitude: number | null;
      // Estado
      longitude: number | null;
      // Estado
      state: string;
      // Cidade
      city: string;
      // Bairro
      district: string | null;
      // CEP
      cep: string | null;
      // Rua
      street: string;
      // Número
      addressNumber: string | null;
      // Complemento
      complement: string | null;
      // Instruções de como chegar
      instructions: string | null;
    };
  };
}

export interface SpecialtySchedulesFragment {
  // Especialidade em questão
  specialty: {
    // Identificador do tipo (geralmente enum)
    id: string | null;
    // Texto do tipo
    description: string | null;
  } | null;
  // Agendas para os especialistas
  schedules: Array<{
    // Identificador da agenda do especialista
    scheduleId: string;
    // Identificador do profissional
    professionalId: string;
    // Nome do profissional
    professionalName: string;
    // Ponto de atendimento
    carePoint: {
      // Id do estabelecimento
      id: string;
      // Nome do estabelecimento
      name: string | null;
      // Contato (Nome/Tel/E-mail)
      contact: {
        phoneNumbers: Array<string | null> | null;
        name: string | null;
        email: string | null;
        website: string | null;
      } | null;
      // Descrição sobre o estabelecimento
      description: string | null;
      // Endereço
      address: {
        // Estado
        latitude: number | null;
        // Estado
        longitude: number | null;
        // Estado
        state: string;
        // Cidade
        city: string;
        // Bairro
        district: string | null;
        // CEP
        cep: string | null;
        // Rua
        street: string;
        // Número
        addressNumber: string | null;
        // Complemento
        complement: string | null;
        // Instruções de como chegar
        instructions: string | null;
      };
    };
  } | null> | null;
}

export interface ProfessionalScheduleFragment {
  // Identificador da agenda de atendimento
  scheduleId: string;
  // Horários disponíveis agrupados por dia
  daySlots: DaySlotModel[] | null;
}

export interface FamilyMemberFragment {
  // JWT token
  token: string;
  // Cód beneficiário
  beneficiaryCode: string;
  // ID da carteirinha
  idHealthCareCard: string;
  cpf: string | null;
  birthDate: string;
  city: string;
  state: string;
  // Nome
  name: string;
  // E-mail
  email: string | null;
  // Beneficiário já tem biometria registrada? null = não foi possível obter essa informação
  isRegisteredInBiometricService: boolean | null;
  // Indica se é dependente ou titular
  dependant: boolean | null;
  // Indica se o usuário deve trocar a senha após o acesso
  mustChangePassword: boolean | null;
  // Termo de consentimento
  consentTerm: string | null;
  // Indica se possui linha do cuidado
  hasCarePlan: boolean | null;
}

export interface AuthenticationFragment {
  // JWT token
  token: string;
  // Cód beneficiário
  beneficiaryCode: string;
  // ID da carteirinha
  idHealthCareCard: string;
  cpf: string | null;
  birthDate: string;
  city: string;
  state: string;
  // Nome
  name: string;
  // E-mail
  email: string | null;
  // Beneficiário já tem biometria registrada? null = não foi possível obter essa informação
  isRegisteredInBiometricService: boolean | null;
  // Indica se é dependente ou titular
  dependant: boolean | null;
  // Indica se o usuário deve trocar a senha após o acesso
  mustChangePassword: boolean | null;
  // Termo de consentimento
  consentTerm: string | null;
  // Indica se possui linha do cuidado
  hasCarePlan: boolean | null;
  // Dependentes, caso seja titular e os tenha
  dependants: Array<{
    // JWT token
    token: string;
    // Cód beneficiário
    beneficiaryCode: string;
    // ID da carteirinha
    idHealthCareCard: string;
    cpf: string | null;
    birthDate: string;
    city: string;
    state: string;
    // Nome
    name: string;
    // E-mail
    email: string | null;
    // Beneficiário já tem biometria registrada? null = não foi possível obter essa informação
    isRegisteredInBiometricService: boolean | null;
    // Indica se é dependente ou titular
    dependant: boolean | null;
    // Indica se o usuário deve trocar a senha após o acesso
    mustChangePassword: boolean | null;
    // Termo de consentimento
    consentTerm: string | null;
    // Indica se possui linha do cuidado
    hasCarePlan: boolean | null;
  } | null> | null;
}

export interface BeneficiaryInfoFragment {
  name: string;
  // ID da carteirinha
  idHealthCareCard: string;
  cpf: string | null;
  birthDate: string;
  email: string | null;
  // Telefone (apenas números - inclui country code). Ex: 5511999999999
  phone: string | null;
  // Endereço cadastrado
  address: {
    // Estado
    latitude: number | null;
    // Estado
    longitude: number | null;
    // Estado
    state: string;
    // Cidade
    city: string;
    // Bairro
    district: string | null;
    // CEP
    cep: string | null;
    // Rua
    street: string;
    // Número
    addressNumber: string | null;
    // Complemento
    complement: string | null;
    // Instruções de como chegar
    instructions: string | null;
  };
  // Gênero (apenas uma letra)
  gender: string | null;
  // Código do plano)
  codePlan: string | null;
  token?: string;
  beneficiaryCode?: string;
}

export interface CampaignFragment {
  // Código da camapnha
  code: string;
  // Nome da campanha
  name: string;
  // Resumo dos questionários
  questionnaires: Array<{
    // Código do questionário
    code: string;
    // Nome do questionário
    name: string;
    // Identificador de um conjunto de respostas já dadas, se houver
    responseCode: string | null;
    // Indica se está completamente respondido
    isCompletelyAnswered: boolean | null;
    // Indica se deve ser respondido
    required: boolean | null;
  } | null>;
}

export interface QuestionnaireSummaryFragment {
  // Código do questionário
  code: string;
  // Nome do questionário
  name: string;
  // Identificador de um conjunto de respostas já dadas, se houver
  responseCode: string | null;
  // Indica se está completamente respondido
  isCompletelyAnswered: boolean | null;
  // Indica se deve ser respondido
  required: boolean | null;
}

export interface CarePlanProcedureFragment {
  // Código identificador do evento (procedimento)
  id: string;
  // Nome descritivo do evento (procedimento)
  description: string;
  // Tipo do procedimento do plano de cuidado
  type: ProcedureType | null;
}

export interface CarePlanProgressFragment {
  // Identificador do Plano de Cuidado (caso já tenha sido gerado)
  code: string | null;
  // Progresso do plano de cuidado, de 0 a 1
  progress: number | null;
}

export interface CarePlanScheduleFragment {
  // Identificador único do agendamento
  id: string;
  // Data mínima para realização
  startDate?: string | null;
  // Data limite para realização
  limitDate?: string | null;
  // Data agendada, caso tenha agendado
  scheduledFor?: string | null;
  // Data realizada, caso tenha cumprido
  doneAt?: string | null;
  // Data de não-cumprimento, caso exista
  failedAt?: string | null;
}

export interface CarePlanItemFragment {
  // Identificador do item no plano de cudiado
  itemCode: string;
  // Informações do procedimento do plano de cuidado: consulta ou exame
  procedure: {
    // Código identificador do evento (procedimento)
    id: string;
    // Nome descritivo do evento (procedimento)
    description: string;
    // Tipo do procedimento do plano de cuidado
    type: ProcedureType | null;
  } | null;
  // Especialidade
  specialty: {
    // Identificador do tipo (geralmente enum)
    id: string | null;
    // Texto do tipo
    description: string | null;
  } | null;
  // Agendamento para um item do plano de cuidado
  schedule: {
    // Identificador único do agendamento
    id: string;
    // Data mínima para realização
    startDate: string | null;
    // Data limite para realização
    limitDate: string | null;
    // Data agendada, caso tenha agendado
    scheduledFor: string | null;
    // Data realizada, caso tenha cumprido
    doneAt: string | null;
    // Data de não-cumprimento, caso exista
    failedAt: string | null;
  } | null;
  // Origem de quem gerou o item no plano de cuidado
  generator: CarePlanItemGenerator | null;
}

export interface CarePlanAppointmentFragment {
  // Médico responsável pela consulta
  doctor: {
    // Identificador
    id: string | null;
    // Nome
    name: string;
    // CPF
    cpf: string | null;
    // Especialidade
    specialty: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    };
  } | null;
  // Local da consulta
  location: {
    // Id do estabelecimento
    id: string;
    // Nome do estabelecimento
    name: string | null;
    // Contato (Nome/Tel/E-mail)
    contact: {
      phoneNumbers: Array<string | null> | null;
      name: string | null;
      email: string | null;
      website: string | null;
    } | null;
    // Descrição sobre o estabelecimento
    description: string | null;
    // Endereço
    address: {
      // Estado
      latitude: number | null;
      // Estado
      longitude: number | null;
      // Estado
      state: string;
      // Cidade
      city: string;
      // Bairro
      district: string | null;
      // CEP
      cep: string | null;
      // Rua
      street: string;
      // Número
      addressNumber: string | null;
      // Complemento
      complement: string | null;
      // Instruções de como chegar
      instructions: string | null;
    };
  } | null;
  // Item do plano de cuidado correspondente
  carePlanItem: {
    // Identificador do item no plano de cudiado
    itemCode: string;
    // Informações do procedimento do plano de cuidado: consulta ou exame
    procedure: {
      // Código identificador do evento (procedimento)
      id: string;
      // Nome descritivo do evento (procedimento)
      description: string;
      // Tipo do procedimento do plano de cuidado
      type: ProcedureType | null;
    } | null;
    // Especialidade
    specialty: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    } | null;
    // Agendamento para um item do plano de cuidado
    schedule: {
      // Identificador único do agendamento
      id: string;
      // Data mínima para realização
      startDate: string | null;
      // Data limite para realização
      limitDate: string | null;
      // Data agendada, caso tenha agendado
      scheduledFor: string | null;
      // Data realizada, caso tenha cumprido
      doneAt: string | null;
      // Data de não-cumprimento, caso exista
      failedAt: string | null;
    } | null;
    // Origem de quem gerou o item no plano de cuidado
    generator: CarePlanItemGenerator | null;
  } | null;
  // Identificador da consulta na base da Prontlife
  appointmentId: string | null;
}

export interface ExamDetailsFragment {
  // Detalhes do item no plano de cuidado
  carePlanItem: {
    // Identificador do item no plano de cudiado
    itemCode: string;
    // Informações do procedimento do plano de cuidado: consulta ou exame
    procedure: {
      // Código identificador do evento (procedimento)
      id: string;
      // Nome descritivo do evento (procedimento)
      description: string;
      // Tipo do procedimento do plano de cuidado
      type: ProcedureType | null;
    } | null;
    // Especialidade
    specialty: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    } | null;
    // Agendamento para um item do plano de cuidado
    schedule: {
      // Identificador único do agendamento
      id: string;
      // Data mínima para realização
      startDate: string | null;
      // Data limite para realização
      limitDate: string | null;
      // Data agendada, caso tenha agendado
      scheduledFor: string | null;
      // Data realizada, caso tenha cumprido
      doneAt: string | null;
      // Data de não-cumprimento, caso exista
      failedAt: string | null;
    } | null;
    // Origem de quem gerou o item no plano de cuidado
    generator: CarePlanItemGenerator | null;
  };
  // Laboratórios disponíveis para entrar em contato
  laboratories: Array<{
    // Local do pronto-atendimento
    carePoint: {
      // Id do estabelecimento
      id: string;
      // Nome do estabelecimento
      name: string | null;
      // Contato (Nome/Tel/E-mail)
      contact: {
        phoneNumbers: Array<string | null> | null;
        name: string | null;
        email: string | null;
        website: string | null;
      } | null;
      // Descrição sobre o estabelecimento
      description: string | null;
      // Endereço
      address: {
        // Estado
        latitude: number | null;
        // Estado
        longitude: number | null;
        // Estado
        state: string;
        // Cidade
        city: string;
        // Bairro
        district: string | null;
        // CEP
        cep: string | null;
        // Rua
        street: string;
        // Número
        addressNumber: string | null;
        // Complemento
        complement: string | null;
        // Instruções de como chegar
        instructions: string | null;
      };
    } | null;
    // Código do prestador
    practitionerId: string | null;
    // Distância relativa a posição geográfica passada (em km)
    distance: number | null;
    // Flag que indica se o ponto de atendimento foi favoritado pelo beneficiário
    isFavorite: boolean | null;
  } | null> | null;
}

export interface CarePointNetworkFragment {
  // Local do pronto-atendimento
  carePoint: {
    // Id do estabelecimento
    id: string;
    // Nome do estabelecimento
    name: string | null;
    // Contato (Nome/Tel/E-mail)
    contact: {
      phoneNumbers: Array<string | null> | null;
      name: string | null;
      email: string | null;
      website: string | null;
    } | null;
    // Descrição sobre o estabelecimento
    description: string | null;
    // Endereço
    address: {
      // Estado
      latitude: number | null;
      // Estado
      longitude: number | null;
      // Estado
      state: string;
      // Cidade
      city: string;
      // Bairro
      district: string | null;
      // CEP
      cep: string | null;
      // Rua
      street: string;
      // Número
      addressNumber: string | null;
      // Complemento
      complement: string | null;
      // Instruções de como chegar
      instructions: string | null;
    };
  } | null;
  // Código do prestador
  practitionerId: string | null;
  // Distância relativa a posição geográfica passada (em km)
  distance: number | null;
  // Flag que indica se o ponto de atendimento foi favoritado pelo beneficiário
  isFavorite: boolean | null;
}

export interface GeneralTypeFragment {
  // Identificador do tipo (geralmente enum)
  id: string | null;
  // Texto do tipo
  description: string | null;
}

export interface MeasureFragment {
  // Quantidade
  amount: number | null;
  // Unidade de medida
  unit: string;
}

export interface AddressFragment {
  // Estado
  latitude: number | null;
  // Estado
  longitude: number | null;
  // Estado
  state: string;
  // Cidade
  city: string;
  // Bairro
  district: string | null;
  // CEP
  cep: string | null;
  // Rua
  street: string;
  // Número
  addressNumber: string | null;
  // Complemento
  complement: string | null;
  // Instruções de como chegar
  instructions: string | null;
}

export interface ContactFragment {
  phoneNumbers: Array<string | null> | null;
  name: string | null;
  email: string | null;
  website: string | null;
}

export interface CarePointFragment {
  // Id do estabelecimento
  id: string;
  // Nome do estabelecimento
  name: string | null;
  // Contato (Nome/Tel/E-mail)
  contact: {
    phoneNumbers: Array<string | null> | null;
    name: string | null;
    email: string | null;
    website: string | null;
  } | null;
  // Descrição sobre o estabelecimento
  description: string | null;
  // Endereço
  address: {
    // Estado
    latitude: number | null;
    // Estado
    longitude: number | null;
    // Estado
    state: string;
    // Cidade
    city: string;
    // Bairro
    district: string | null;
    // CEP
    cep: string | null;
    // Rua
    street: string;
    // Número
    addressNumber: string | null;
    // Complemento
    complement: string | null;
    // Instruções de como chegar
    instructions: string | null;
  };
}

export interface ProfessionalFragment {
  // Identificador
  id: string | null;
  // Nome
  name: string;
  // Email
  email: string | null;
  // CPF
  cpf: string | null;
  // Especialidade
  specialty: {
    // Identificador do tipo (geralmente enum)
    id: string | null;
    // Texto do tipo
    description: string | null;
  };
  // Número do CRM
  crm: string | null;
  // Texto de resumo da biografia
  bioText: string | null;
  // URL da imagem do médico
  imageUrl: string | null;
}

export interface HabitFragment {
  // Código do hábito
  id: string;
  // Tipo do hábito
  type: {
    // Identificador do tipo de hábito
    id: string | null;
    // Descreve os tipos de hábitos padronizados para íconografia.
    area: HabitArea | null;
    // Descritivo do tipo do hábito
    display: string | null;
  };
  // Descrição do hábito
  description: string;
  // Periodicidade
  frequency: {
    // Identificador do tipo (geralmente enum)
    id: string | null;
    // Texto do tipo
    description: string | null;
  };
  // Aviso sobre o hábito
  warning: string | null;
  // Observação sobre o hábito
  observation: string | null;
  // Idade mínima
  minimumAge: string | null;
  // Idade máxima
  maximumAge: string | null;
  // Sexo de cumprimento
  sex: Sex;
  // Item do plano de cuidado (apenas se estiver ativo)
  carePlanItem: {
    // Identificador do item no plano de cudiado
    itemCode: string;
    // Identificador do hábito
    habitCode: string;
    // Periodicidade
    frequency: {
      // Identificador do tipo (geralmente enum)
      id: string | null;
      // Texto do tipo
      description: string | null;
    } | null;
    // Origem de quem gerou o item no plano de cuidado
    generator: CarePlanItemGenerator | null;
    // Todas as agendas do hábito (inclusive histórico)
    schedules: Array<{
      // Identificador único do agendamento
      id: string;
      // Data mínima para realização
      startDate: string | null;
      // Data limite para realização
      limitDate: string | null;
      // Data agendada, caso tenha agendado
      scheduledFor: string | null;
      // Data realizada, caso tenha cumprido
      doneAt: string | null;
      // Data de não-cumprimento, caso exista
      failedAt: string | null;
    } | null> | null;
    // Próxima agendaa do hábito a cumprir, agrupados quando for o mesmo dia.
    nextSchedules: Array<{
      // Identificador único do agendamento
      id: string;
      // Data mínima para realização
      startDate: string | null;
      // Data limite para realização
      limitDate: string | null;
      // Data agendada, caso tenha agendado
      scheduledFor: string | null;
      // Data realizada, caso tenha cumprido
      doneAt: string | null;
      // Data de não-cumprimento, caso exista
      failedAt: string | null;
    } | null> | null;
  } | null;
}

export interface HabitTypeFragment {
  // Identificador do tipo de hábito
  id: string | null;
  // Descreve os tipos de hábitos padronizados para íconografia.
  area: HabitArea | null;
  // Descritivo do tipo do hábito
  display: string | null;
}

export interface CarePlanHabitFragment {
  // Identificador do item no plano de cudiado
  itemCode: string;
  // Identificador do hábito
  habitCode: string;
  // Periodicidade
  frequency: {
    // Identificador do tipo (geralmente enum)
    id: string | null;
    // Texto do tipo
    description: string | null;
  } | null;
  // Origem de quem gerou o item no plano de cuidado
  generator: CarePlanItemGenerator | null;
  // Todas as agendas do hábito (inclusive histórico)
  schedules: Array<{
    // Identificador único do agendamento
    id: string;
    // Data mínima para realização
    startDate: string | null;
    // Data limite para realização
    limitDate: string | null;
    // Data agendada, caso tenha agendado
    scheduledFor: string | null;
    // Data realizada, caso tenha cumprido
    doneAt: string | null;
    // Data de não-cumprimento, caso exista
    failedAt: string | null;
  } | null> | null;
  // Próxima agendaa do hábito a cumprir, agrupados quando for o mesmo dia.
  nextSchedules: Array<{
    // Identificador único do agendamento
    id: string;
    // Data mínima para realização
    startDate: string | null;
    // Data limite para realização
    limitDate: string | null;
    // Data agendada, caso tenha agendado
    scheduledFor: string | null;
    // Data realizada, caso tenha cumprido
    doneAt: string | null;
    // Data de não-cumprimento, caso exista
    failedAt: string | null;
  } | null> | null;
}

export interface PlanDataFragment {
  // Tipo de contratação do plano
  type: string | null;
  // Registro do plano na ANS
  ansRegister: string | null;
  // Número da CNS
  cns: string | null;
  // Contratado em
  contractedAt: string | null;
  // Regulação do plano
  regulation: string | null;
  // Abrangência do plano
  comprehensiveness: string | null;
  // Acomodação do plano
  accommodation: string | null;
  // Data de início de vigencia
  startValidity: string | null;
  // Nome do Plano
  namePlan: string | null;
  // Nome da Rede
  nameNetwork: string | null;
  // Nome da Fantasia
  contractor: string | null;
  // Nome do Contratante
  segmentation: string | null;
}

export interface HealthCareCardContactsFragment {
  // Celular para WhatsApp
  cellPhone: string;
  // Telefone fixo para contato
  phone: string | null;
  // E-mail
  email: string | null;
}

export interface HealthCareCardFragment {
  // ID da carteirinha
  idHealthCareCard: string;
  // Nome do beneficiário
  name: string | null;
  // Dados do plano de saúde
  planData: {
    // Tipo de contratação do plano
    type: string | null;
    // Registro do plano na ANS
    ansRegister: string | null;
    // Número da CNS
    cns: string | null;
    // Contratado em
    contractedAt: string | null;
    // Regulação do plano
    regulation: string | null;
    // Abrangência do plano
    comprehensiveness: string | null;
    // Acomodação do plano
    accommodation: string | null;
    // Data de início de vigencia
    startValidity: string | null;
    // Nome do Plano
    namePlan: string | null;
    // Nome da Rede
    nameNetwork: string | null;
    // Nome da Fantasia
    contractor: string | null;
    // Nome do Contratante
    segmentation: string | null;
  };
  // Contatos do QSaúde
  contact: {
    // Celular para WhatsApp
    cellPhone: string;
    // Telefone fixo para contato
    phone: string | null;
    // E-mail
    email: string | null;
  };
}

export interface MedicationFragment {
  // Identificador do item de medicação
  id: string;
  // Data de início da medicação
  startDate: string;
  // Data de final da medicação, se houver
  endDate: string | null;
  // Indica se é para ser notificado
  notify: boolean;
  // Indica se está ativo
  active: boolean;
  // Orientações para a medicação, se houver
  orientation: string | null;
  // Horários do dia para a medicação
  dailySchedule: Array<{
    // Identificador do horário de medicação
    id: string;
    // Horário para tomar o medicamento
    hour: string;
  } | null>;
  // Frequencia da medicação
  frequency: string | null;
  // Detalhes do medicamento
  medicine: {
    // Nome
    name: string;
    // Nome popular
    commonName: string | null;
    // Aspecto
    aspect: string | null;
    // Dosagem
    dosage: {
      // Quantidade
      amount: number | null;
      // Unidade de medida
      unit: string;
    } | null;
    // Instruções
    instructions: string | null;
  };
}

export interface MedicationListItemFragment {
  // Identificador do item de medicação
  id: string;
  // Data de início da medicação
  startDate: string;
  // Data de final da medicação, se houver
  endDate: string | null;
  // Indica se é para ser notificado
  notify: boolean;
  // Indica se está ativo
  active: boolean;
  // Horários do dia para a medicação
  dailySchedule: Array<{
    // Identificador do horário de medicação
    id: string;
    // Horário para tomar o medicamento
    hour: string;
  } | null>;
  // Frequencia da medicação
  frequency: string | null;
  // Detalhes do medicamento
  medicine: {
    // Nome
    name: string;
  };
}

export interface MedicationScheduleFragment {
  // Identificador do horário de medicação
  id: string;
  // Horário para tomar o medicamento
  hour: string;
}

export interface MedicineFragment {
  // Nome
  name: string;
  // Nome popular
  commonName: string | null;
  // Aspecto
  aspect: string | null;
  // Dosagem
  dosage: {
    // Quantidade
    amount: number | null;
    // Unidade de medida
    unit: string;
  } | null;
  // Instruções
  instructions: string | null;
}

export interface ProcedureFragment {
  // Identificador
  id: string | null;
  // Data realizada
  registeredAt: string | null;
  // Nome descritivo
  display: string;
  // Detlahes / Observações
  detail: string | null;
  loading?: boolean;
  error?: boolean;
}

export interface PublicationsFragment {
  // Response do serviço de publicações
  content: Array<{
    // Código da publicação
    id: number | null;
    // Nome da publicação
    name: string | null;
    // Resumo da publicação
    description: string | null;
    // Tipo da publicação R = Revista, M = Manual
    publicationType: string | null;
    // Data da publicação
    publicationDate: string | null;
    // Imagem thumbnail em base64
    thumbnail: string | null;
    // Arquivo
    file: {
      // Código do arquivo
      id: number | null;
      // Hash do arquivo
      hash: string | null;
      // Url do arquivo
      url: string | null;
    } | null;
  } | null> | null;
}

export interface FileFragment {
  // Código do arquivo
  id: number | null;
  // Hash do arquivo
  hash: string | null;
  // Url do arquivo
  url: string | null;
}

export interface QuestionnaireFragment {
  // Identificador
  code: string;
  // Nome
  name: string;
  // Identificador do gabarito (últimas respostas submetidas)
  responseCode: string | null;
  // Indica se é obrigatória informação do IMC
  imcRequired: boolean | null;
  // Peso kg (caso precise de IMC)
  weight: number | null;
  // Altura cm (caso precise de IMC)
  height: number | null;
  // Respondido em (null caso não tenha sido respondido)
  questionGroups: Array<{
    // Identificador
    code: string;
    // Nome
    name: string;
    // Mínimo de questões a serem respondidas
    minimumToAnswer: number | null;
    // Total de questões respondidas
    totalAnswered: number | null;
    // Perguntas do Tipo de Avaliação
    questions: Array<{
      // Código da pergunta
      code: string;
      // Descrição
      description: string;
      // Textos explicativos, se necessário
      descritive: Array<string | null> | null;
      // Determina se a questão é ou não obrigatória
      required: boolean;
      // Tipo da resposta
      type: QuestionType;
      // Valor mínimo (caso seja numérico)
      minValue: number | null;
      // Valor máximo (caso seja numérico
      maxValue: number | null;
      // Alternativas de resposta
      alternatives: Array<{
        // Código do item da resposta
        code: string;
        // Descrição do item da resposta (para tipos 1 e 2)
        display: string | null;
        // Valor máximo da resposta
        conditionToShow: Array<{
          // Identificador da questão
          questionCode: string;
          // Identificador da resposta
          answerCode: string;
        } | null> | null;
      } | null>;
      // Resposta(s) escolhida(s), caso tenha respondido
      chosenAnswers: Array<{
        // Identificador do item da resposta
        code: string | null;
        // Resposta digitada
        display: string | null;
      } | null> | null;
      // Condição para ser mostrada (se houver) - combinação entre pergunta e resposta
      conditionToShow: Array<{
        // Identificador da questão
        questionCode: string;
        // Identificador da resposta
        answerCode: string;
      } | null> | null;
    } | null>;
  } | null> | null;
}

export interface QuestionGroupFragment {
  // Identificador
  code: string;
  // Nome
  name: string;
  // Mínimo de questões a serem respondidas
  minimumToAnswer: number | null;
  // Total de questões respondidas
  totalAnswered: number | null;
  // Perguntas do Tipo de Avaliação
  questions: Array<{
    // Código da pergunta
    code: string;
    // Descrição
    description: string;
    // Textos explicativos, se necessário
    descritive: Array<string | null> | null;
    // Determina se a questão é ou não obrigatória
    required: boolean;
    // Tipo da resposta
    type: QuestionType;
    // Valor mínimo (caso seja numérico)
    minValue: number | null;
    // Valor máximo (caso seja numérico
    maxValue: number | null;
    // Alternativas de resposta
    alternatives: Array<{
      // Código do item da resposta
      code: string;
      // Descrição do item da resposta (para tipos 1 e 2)
      display: string | null;
      // Valor máximo da resposta
      conditionToShow: Array<{
        // Identificador da questão
        questionCode: string;
        // Identificador da resposta
        answerCode: string;
      } | null> | null;
    } | null>;
    // Resposta(s) escolhida(s), caso tenha respondido
    chosenAnswers: Array<{
      // Identificador do item da resposta
      code: string | null;
      // Resposta digitada
      display: string | null;
    } | null> | null;
    // Condição para ser mostrada (se houver) - combinação entre pergunta e resposta
    conditionToShow: Array<{
      // Identificador da questão
      questionCode: string;
      // Identificador da resposta
      answerCode: string;
    } | null> | null;
  } | null>;
}

export interface QuestionFragment {
  // Código da pergunta
  code: string;
  // Descrição
  description: string;
  // Textos explicativos, se necessário
  descritive: Array<string | null> | null;
  // Determina se a questão é ou não obrigatória
  required: boolean;
  // Tipo da resposta
  type: QuestionType;
  // Valor mínimo (caso seja numérico)
  minValue: number | null;
  // Valor máximo (caso seja numérico
  maxValue: number | null;
  // Alternativas de resposta
  alternatives: Array<{
    // Código do item da resposta
    code: string;
    // Descrição do item da resposta (para tipos 1 e 2)
    display: string | null;
    // Valor máximo da resposta
    conditionToShow: Array<{
      // Identificador da questão
      questionCode: string;
      // Identificador da resposta
      answerCode: string;
    } | null> | null;
  } | null>;
  // Resposta(s) escolhida(s), caso tenha respondido
  chosenAnswers: Array<{
    // Identificador do item da resposta
    code: string | null;
    // Resposta digitada
    display: string | null;
  } | null> | null;
  // Condição para ser mostrada (se houver) - combinação entre pergunta e resposta
  conditionToShow: Array<{
    // Identificador da questão
    questionCode: string;
    // Identificador da resposta
    answerCode: string;
  } | null> | null;
}

export interface AlternativeFragment {
  // Código do item da resposta
  code: string;
  // Descrição do item da resposta (para tipos 1 e 2)
  display: string | null;
  // Valor máximo da resposta
  conditionToShow: Array<{
    // Identificador da questão
    questionCode: string;
    // Identificador da resposta
    answerCode: string;
  } | null> | null;
}

export interface AnswerFragment {
  // Identificador do item da resposta
  code: string | null;
  // Resposta digitada
  display: string | null;
}

export interface QuestionnaireConditionFragment {
  // Identificador da questão
  questionCode: string;
  // Identificador da resposta
  answerCode: string;
}

export interface QuestionnaireFeedbackTopicFragment {
  // Possível ID para diferenciação entre os tópicos
  id: string;
  // Título do tópico a ser exibido
  title: string | null;
  // Enum único com a referência do tópico
  reference: QuestionnaireFeedbackTopicReferenceEnum | null;
}

export interface QuestionnaireFeedbackItemFragment {
  // Assunto do item da devolutiva
  topic: {
    // Possível ID para diferenciação entre os tópicos
    id: string;
    // Título do tópico a ser exibido
    title: string | null;
    // Enum único com a referência do tópico
    reference: QuestionnaireFeedbackTopicReferenceEnum | null;
  } | null;
  // Descrição do item da devolutiva
  description: string;
}

export interface QuestionnaireFeedbackFragment {
  // Nome do questionário que gerou a devolutiva
  questionnaireName: string | null;
  // Código identificador da última resposta
  responseCode: string | null;
  // Data da última devolutiva
  lastAnsweredAt: string | null;
  // Itens da devolutiva
  items: Array<{
    // Assunto do item da devolutiva
    topic: {
      // Possível ID para diferenciação entre os tópicos
      id: string;
      // Título do tópico a ser exibido
      title: string | null;
      // Enum único com a referência do tópico
      reference: QuestionnaireFeedbackTopicReferenceEnum | null;
    } | null;
    // Descrição do item da devolutiva
    description: string;
  } | null>;
}

export interface ResponsibleGuideFragment {
  // Código do GPS
  code: string;
  name: string | null;
  email: string | null;
}

export interface VaccineFragment {
  // Nome da vacina
  display: string;
  // Data em que tomou
  takenAt: string;
}

export interface VitalSignFragment {
  // Tipo do sinal vital (Enum VitalSignType)
  type: VitalSignType;
  // Unidade de medida
  measureUnit: string;
  // Data da medição
  measuredAt: string | null;
  // Medida do sinal vital
  measure: {
    // Quantidade
    amount: number | null;
    // Unidade de medida
    unit: string;
  } | null;
  // Sub-medidas (usado, por exemplo, na medida de pressão, que tem mais de uma)
  subMeasures: Array<{
    // Tipo do sinal vital (Enum VitalSignType)
    type: VitalSignType;
    // Data da medição
    measuredAt: string | null;
    // Medida do sinal vital
    measure: {
      // Quantidade
      amount: number | null;
      // Unidade de medida
      unit: string;
    } | null;
  } | null> | null;
}
