import { HttpInterceptor, HttpRequest } from '@taqtile/common/core';

import { AxiosResponse } from 'axios';
import { Service } from 'typedi';

@Service()
export class AuthTokenInterceptor implements HttpInterceptor {
  async before(request: HttpRequest): Promise<HttpRequest> {
    // inject token here
    return request;
  }
  async afterSuccess(response: AxiosResponse): Promise<any> {
    // save the token here
    return response;
  }
}
