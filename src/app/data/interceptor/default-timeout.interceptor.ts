import { HttpInterceptor, HttpRequest } from '@taqtile/common/core';
import { Service } from 'typedi';
import { isNil } from 'lodash';

/* *
* Enforces a default timeout to the api requests
* Requests were crashing at the Android native layer when the timeout was `null`
*
* Reference: ./docs/TROUBLESHOOTING.md
*/
const DEFAULT_TIMEOUT_MILLIS = 1000 * 30; // 30 seconds

@Service()
export class DefaultTimeoutInterceptor implements HttpInterceptor {
  async before(request: HttpRequest): Promise<HttpRequest> {

    request.timeout = isNil(request.timeout) ? DEFAULT_TIMEOUT_MILLIS : request.timeout;
    console.log(request.timeout);

    return request;
  }
}
