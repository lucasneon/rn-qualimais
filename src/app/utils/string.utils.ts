export function cleanText(text: string) {
  const punctuation = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^_`{|}~\s]/g;
  return text.replace(punctuation, '');
}
