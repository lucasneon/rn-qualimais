export interface ResourceResponse {
  resourceType: string;
  id: string;
}

export interface TypeRemote {
  text: string;
}

export interface NameRemote {
  use: string;
  text: string;
}

export interface IdentifierRemote {
  type?: TypeRemote;
  system: string;
  value: string;
}

export interface ReferenceRemote {
  reference: string;
}

export interface CodingRemote {
  code: string;
  system?: string;
  display?: string;
}

export interface CodeRemote {
  coding: CodingRemote[];
}

export interface MeasureRemote {
  value: number;
  unit: string;
}

export interface AddressResponse {
  type: string;
  line: string[];
  city: string;
  district: string;
  state: string;
  postalCode: string;
  country: string;
}

export interface PositionResponse {
  latitude: number;
  longitude: number;
}

export interface ListResponse<T> extends ResourceResponse {
  type: string;
  total: number;
  entry: Array<EntryResponse<T>>;
}

export interface EntryResponse<T> {
  fullUrl: string;
  resource: T;
}

export interface ProntlifeRequestParams {
  identifier?: string;
  'actor:Location.state'?: string;
  'actor:Location.city'?: string;
  start?: string;
  end?: string;
  'specialty.code'?: string;
  'actor:Practitioner'?: string;
  Schedule?: string;
  'Coverage.identifier'?: string;
  _query?: string; // required when Coverage.identifier is supplied
  include?: boolean; // Used to include more resources on GET lists
}
