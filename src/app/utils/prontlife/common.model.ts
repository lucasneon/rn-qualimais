export interface GeneralTypeModel {
  id: string;
  description: string;
}

export interface MeasureModel {
  amount?: number;
  unit: string;
}

export interface GeneralProgressModel {
  current: number;
  total: number;
}

export enum SexModel {
  Male = 'Male',
  Female = 'Female',
  Both = 'Both',
}
