import { UserData } from '@app/modules/authentication/api/login/beneficiary.service';

import * as Moment from 'moment';
import {
  CodeRemote,
  CodingRemote,
  IdentifierRemote,
  ReferenceRemote,
} from '../utils/prontlife/prontlife.common';
import { GeneralTypeModel } from './prontlife/common.model';

export const ProntlifeUtils = {
  Extractor: {
    identifiertValue(identifiers: IdentifierRemote[], system: string): string {
      const item =
        identifiers &&
        identifiers.find(identifier => identifier.system.includes(system));
      return item ? item.value : null;
    },

    codingValue(code: CodeRemote, system: string): GeneralTypeModel {
      const coding = code.coding.find(item => item.system.includes(system));
      return coding ? { id: coding.code, description: coding.display } : null;
    },

    codeValue(): GeneralTypeModel {
      let coding: CodingRemote;

      // code.forEach(item => {
      //   if (!coding) {
      //     coding = item.coding.find(codingItem => codingItem.system.includes(system));
      //   }
      // });

      return coding ? { id: coding.code, description: coding.display } : null;
    },

    reference(references: ReferenceRemote[], prefix: string): string {
      const ref = references.find(item => item.reference.includes(prefix));
      return ref ? ref.reference.split(prefix)[1] : null;
    },
  },

  FormatterPatient: {
    insurerParam(input: Partial<UserData>): string {
      return `insurer/${input.insurerId}/members|${input.idHealthCareCard}`;
    },

    date(date: string | Date): string {
      return Moment.utc(date).format('YYYY-MM-DD');
    },
  },

  Formatter: {
    insurerParam(input: Partial<UserData>): string {
      return `insurer/${input.insurerId}/members|${input.idHealthCareCard}`;
    },

    date(date: string | Date): string {
      return Moment.utc(date).format('YYYY-MM-DD');
    },
  },
};

export const requestProntlifeHeaders = (prontlifeToken: string | unknown) => {
  const headers = {
    'x-auth-token': prontlifeToken,
  };
  return headers;
};
