import React, { Component } from 'react';
import { Animated, Dimensions, EmitterSubscription, Keyboard, StyleSheet, TextInput, UIManager } from 'react-native';

const { State: TextInputState } = TextInput;
const SendButtonOfsset = - 45;

interface KeyboardShiftProps {
  useButtonOffset?: boolean;
  removeAnimationTime?: boolean;
}

interface KeyboarShiftState {
  shift: Animated.Value;
}

export default class KeyboardShift extends Component<KeyboardShiftProps, KeyboarShiftState> {
  keyboardWillShowSub: EmitterSubscription;
  keyboardWillHideSub: EmitterSubscription;
  constructor(props) {
    super(props);
    this.state = {shift: new Animated.Value(0)};
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  render() {
    const { children: renderProp } = this.props;
    const shift  = this.state.shift;
    return (
      <Animated.View style={[styles.container, { transform: [{translateY: shift}] }]}>
        {renderProp}
      </Animated.View>
    );
  }

  handleKeyboardDidShow = event => {
    const { height: windowHeight } = Dimensions.get('window');
    const keyboardHeight = event.endCoordinates.height;
    // undocumented method, gets the id of the currentlyFocusedField to get measures
    const currentlyFocusedField = TextInputState.currentlyFocusedField();
    if (currentlyFocusedField) {
    UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
      const componentPosition = {
        positionX: originX,
        positionY: originY,
        componentWidth: width,
        fieldHeight: height,
        field: pageX,
        fieldTop: pageY,
      };
      const visibleSpace = (windowHeight - keyboardHeight);
      const movableSpace = (componentPosition.fieldTop + componentPosition.fieldHeight);
      const gap = visibleSpace - movableSpace  + (this.props.useButtonOffset ? (SendButtonOfsset) : 0);
      if (gap >= 0) {
        return;
      }
      Animated.timing(
        this.state.shift,
        {
          toValue: gap,
          duration: this.props.removeAnimationTime ? 0 : 300,
          useNativeDriver: true,
        },
      ).start();
    });
  }
  }

  handleKeyboardDidHide = () => {
    Animated.timing(
      this.state.shift,
      {
        toValue: 0,
        duration: this.props.removeAnimationTime ? 0 : 300,
        useNativeDriver: true,
      },
    ).start();
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    left: 0,
    position: 'absolute',
    top: 0,
    width: '100%',
  },
});
