import moment, { Duration } from 'moment';
import { Age } from '@app/model';

export const DateUtils = {
  Create: {
    // use negative values for past offset
    getCurrent(daysOffset = 0, hoursOffset = 0): Date {
      return moment()
        .add(daysOffset, 'd')
        .add(hoursOffset, 'h')
        .set({ s: 0, ms: 0 })
        .toDate();
    },
  },

  getPastMidnight(): Date {
    const date = new Date();
    date.setHours(0, 0, 0, 0);
    return date;
  },

  Calculate: {
    getAge(birthDate: Date): Age {
      const duration: Duration = moment.duration(moment().diff(birthDate));

      return {
        years: duration.years(),
        months: duration.months(),
      };
    },
    absoluteDaysDifference(a: string | Date, b: string | Date): number {
      return Math.abs(moment(a).diff(b, 'days'));
    },
  },

  Format: {
    presentation(date: string | Date, onlyHour?: boolean): string {
      return onlyHour
        ? moment(date).format('HH:mm')
        : moment(date).format('DD/MM/YYYY');
    },

    datasource(date: string | Date, includeHours: boolean = false): string {
      return !includeHours
        ? moment(date).format('YYYY-MM-DD')
        : moment(date).toISOString();
    },
  },
};
