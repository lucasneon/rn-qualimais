import { GraphQLError } from 'graphql';
import { AppError, AppErrorType, InternalServerError, MaintenanceError, UnknownError } from '@app/model';

const MaintenanceCode = 503;

export const ErrorMapper = {
  map(error: any): AppError {
    if (!error) {
      return null;
    }

    const graphQLError = findGraphQLError(error);
    const isNetworkError = !!error.networkError;

    if (graphQLError) {
      return parseGraphQLError(graphQLError);
    } else if (isNetworkError) {
      return parseNetworkError(error.networkError);
    }

    return UnknownError;
  },
};

function parseGraphQLError(error: GraphQLError): AppError {
  const message = error.message || UnknownError.message;

  return {
    title: '',
    message,
    type: AppErrorType.Unknown,
    action: 'Tentar novamente',
    errorDetails: JSON.stringify(error, null, 2),
  };
}

function parseNetworkError(error: any): AppError {
  if (error.statusCode === MaintenanceCode) {
    return MaintenanceError;
  }

  if (error.statusCode === 500) {
    return InternalServerError;
  }

  return UnknownError;
}

function findGraphQLError(error): GraphQLError | undefined {
  return (error.graphQLErrors && error.graphQLErrors.length > 0 && error.graphQLErrors[0]) ||
    (error.networkError && error.networkError.result && error.networkError.result.errors &&
      error.networkError.result.errors.length > 0 && error.networkError.result.errors[0]);
}
