import { GeoCoordinates } from '@app/model';

export const DistanceCalculator = {
  degrees(coord1: GeoCoordinates, coord2: GeoCoordinates): number {
    return Math.sqrt((coord1.latitude - coord2.latitude) ** 2 + (coord1.longitude - coord2.longitude) ** 2);
  },
};
