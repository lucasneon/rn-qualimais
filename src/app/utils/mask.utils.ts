import { MaskService } from 'react-native-masked-text';

export const MaskUtils = {
  Apply: {
    phone(phone: string): string {
      const withoutCountyCode: string = phone.length > 11 ? phone.slice(2) : phone;
      return MaskService.toMask('cel-phone', withoutCountyCode);
    },
  },
};
