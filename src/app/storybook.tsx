
import { Navigation } from '@app/core/navigation';
import StorybookUI from '../storybook';

import * as React from 'react';

class Storybook extends React.Component<any, any> {
  static options = {name: 'Storybook' };
  render() {
    return <StorybookUI />;
  }
}

export const bootstrapStorybook = () => {
  Navigation.events().registerAppLaunchedListener(() => {
    Navigation.register(Storybook, false);

    Navigation.setRoot({
      root: {
        component: {name: Storybook.options.name},
      },
    });
  });
};
