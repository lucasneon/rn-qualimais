import 'moment/locale/pt-br';
import moment from 'moment';
import { bootstrapNavigation } from '@app';
import { configureApolloClient } from './apollo.config';
import { configureConstants } from './constants.config';
import { configurePermissions } from './permissions.config';
import { configureServices } from './services.config';
import { configureFeatures } from '@app/config/features.config';
import { LogBox } from 'react-native';

LogBox.ignoreLogs([
  'Animated: `useNativeDriver` was not specified. This is a required option and must be explicitly set to `true` or `false`',
  'Setting a timer',
]);

LogBox.ignoreLogs(['VirtualizedLists should never be nested']);

export function configApp(env: any) {
  configureApolloClient(env);
  configurePermissions();
  configureServices();
  configureLocale();
  configureConstants(env);
  configNavigation();
  configureFeatures(env);
}

async function configNavigation() {
  await bootstrapNavigation();
}

function configureLocale() {
  moment.locale('pt-BR');
}
