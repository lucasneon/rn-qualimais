import { ApolloError } from 'apollo-client';
import Container, { Service } from 'typedi';
import { Container as UnstatedContainer } from 'unstated';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { BeneficiaryInfoQuery } from '@app/data/graphql';
import { Beneficiary } from '@app/model';
import {
  AllergyProvider,
  AllergyProviderToken,
  BeneficiaryProvider,
  BeneficiaryProviderToken,
  CarePlanProvider,
  CarePlanProviderToken,
  HabitsProvider,
  HabitsProviderToken,
  LocationProvider,
  LocationProviderToken,
  MedicationsProvider,
  MedicationsProviderToken,
  VaccineProvider,
  VaccineProviderToken,
  VitalSignProvider,
  VitalSignProviderToken,
} from '@app/services/providers';
import { LocalDataSource } from '@app/data';
import { fetchAuthBeneficiaryInfo } from '@app/modules/authentication/api/login/beneficiary.service';

@Service()
export class ProvidersConfig {
  readonly graphQLProvider: GraphQLProvider =
    Container.get(GraphQLProviderToken);
  readonly vitalSignProvider: VitalSignProvider = Container.get(
    VitalSignProviderToken,
  );
  readonly beneficiaryProvider: BeneficiaryProvider = Container.get(
    BeneficiaryProviderToken,
  );
  readonly allergyProvider: AllergyProvider =
    Container.get(AllergyProviderToken);
  readonly vaccineProvider: VaccineProvider =
    Container.get(VaccineProviderToken);
  readonly carePlanProvider: CarePlanProvider = Container.get(
    CarePlanProviderToken,
  );
  readonly locationProvider: LocationProvider = Container.get(
    LocationProviderToken,
  );
  readonly habitsProvider: HabitsProvider = Container.get(HabitsProviderToken);
  readonly medicationsProvider: MedicationsProvider = Container.get(
    MedicationsProviderToken,
  );

  readonly providers: Array<UnstatedContainer<any>> = [
    this.graphQLProvider,
    this.vitalSignProvider,
    this.beneficiaryProvider,
    this.allergyProvider,
    this.vaccineProvider,
    this.carePlanProvider,
    this.locationProvider,
    this.habitsProvider,
    this.medicationsProvider,
  ];

  async handleContextChange() {
    if (!this.beneficiaryProvider.state.active) {
      return;
    }

    this.configureBeneficiaryProvider();
    this.carePlanProvider.rehydrate();
    this.vitalSignProvider.rehydrate();
  }

  async hydrateInitialProviders(): Promise<void> {
    await this.beneficiaryProvider.rehydrate();
    this.handleContextChange();
  }

  async configureBeneficiaryProvider() {
    const localDataSource: LocalDataSource = Container.get(LocalDataSource);
    const beneficiaryCode: string = await localDataSource.get<string>(
      'beneficiaryCode',
    );

    fetchAuthBeneficiaryInfo(beneficiaryCode)
      .then(res => this.updateBeneficiaryInfo({ BeneficiaryInfo: res.data }))
      .catch(err => console.error('error', err));
    // this.graphQLProvider.query<BeneficiaryInfoQuery>(
    //   'beneficiary-info',
    //   null,
    //   (data: BeneficiaryInfoQuery) => this.updateBeneficiaryInfo(data),
    //   (error: ApolloError) => console.warn(error),
    // );
  }

  async updateBeneficiaryInfo(data: any) {
    const activeBeneficiary: Beneficiary =
      this.beneficiaryProvider.state.active;
    console.warn('activeBeneficiary', activeBeneficiary);
    activeBeneficiary.city = data.BeneficiaryInfo.address.city || '';
    activeBeneficiary.state = data.BeneficiaryInfo.address.state || '';
    await this.beneficiaryProvider.setActive(activeBeneficiary);
    console.warn('setActive', activeBeneficiary);
  }
}
