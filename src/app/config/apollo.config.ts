import { NormalizedCacheObject } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { onError } from 'apollo-link-error';
import { Container } from 'typedi';
import { ApolloClientBuilderService, GraphQLDocsService } from '@app/core/graphql';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services/providers';
import { ApolloClientKey, ApolloClientMockKey } from './tokens.config';

const Unauthorized = 401;

export function configureApolloClient(env: any) {
  const mockUrl = env.BASE_URL + '?mock=true';
  const builder: ApolloClientBuilderService = Container.get(ApolloClientBuilderService);
  const middlwares: ApolloMiddlewares = Container.get(ApolloMiddlewares);

  const builderWithMiddlewares = (url: string): ApolloClient<NormalizedCacheObject> => {
    return builder.build(url, middlwares.before, middlwares.error());
  };

  const client = builderWithMiddlewares(env.BASE_URL);
  const mockClient = builderWithMiddlewares(mockUrl);

  Container.get(GraphQLDocsService).configure(
    'src/app/data/graphql/query',
    'src/app/data/graphql/mutation',
    require('../data/graphql/graphql-documents.json'),
  );

  Container.set(ApolloClientKey, client);
  Container.set(ApolloClientMockKey, mockClient);
}

class ApolloMiddlewares {
  private readonly beneficiaryProvider: BeneficiaryProvider = Container.get(BeneficiaryProviderToken);

  before = async () => {
    const headers: { [key: string]: string } = {};

    const token = await this.beneficiaryProvider.getActiveBeneficiaryToken();

    if (token) {
      headers.Authorization = token;
    }

    return { headers };
  }

  error(): ApolloLink {
    return onError(async (error: any) => {
      if (error.networkError && error.networkError.statusCode === Unauthorized) {
        await this.beneficiaryProvider.logout();
      }
    });
  }
}
