export const ApolloClientMockKey = 'ApolloClientMock';
export const ApolloClientKey = 'ApolloClient';
export const ChatClientKey = 'ChatClient';
export const ShouldCheckTouchIdKey = 'ShouldCheckTouchIdKey';
export const ShouldLogErrorDetailsKey = 'ShouldLogErrorDetailsKey';

// custom features
export const EditBenefData = 'EditBenefData';
export const ActivateHabit = 'ActivateHabit';
export const AppointmentGynecology = 'AppointmentGynecology';
export const ActiveNetworkSearch = 'ActiveNetworkSearch';
export const TermAccpetOfUser = 'TermAccpetOfUser';
export const AppointmentSpeciality = 'AppointmentSpeciality';
export const PersonalDoctor = 'PersonalDoctor';

export const PharmacyTokenUrl = 'PharmacyTokenUrl';
export const PharmacyClientId = 'PharmacyClientId';
export const PharmacyUsername = 'PharmacyUsername';
export const PharmacyPassword = 'PharmacyPassword';
export const GAMA_OP_CODE = 'GAMA_OP_CODE';
export const GAMA_SYSTEM_CODE = 'GAMA_SYSTEM_CODE';
export const GAMA_STIMULATING_CODE = 'GAMA_STIMULATING_CODE';
export const GAMA_SYSTEM_ORIGIN = 'GAMA_SYSTEM_ORIGIN';
export const GAMA_NETWORK_CODE = 'GAMA_NETWORK_CODE';
export const PRONTLIFE_BASE_URL = 'PRONTLIFE_BASE_URL';
export const PRONTLIFE_ACCESS_TOKEN = 'PRONTLIFE_ACCESS_TOKEN';

export const LIVE_TOUCH_API_KEY = 'LIVE_TOUCH_API_KEY';
export const LIVE_TOUCH_BASE_URL = 'LIVE_TOUCH_BASE_URL';
export const LIVE_TOUCH_CHAT_USER_PASSWORD = 'LIVE_TOUCH_CHAT_USER_PASSWORD';
export const LIVE_TOUCH_PROJECT_CODE = 'LIVE_TOUCH_PROJECT_CODE';

export const GOOGLE_MAPS_API_KEY = 'GOOGLE_MAPS_API_KEY';

//TELEMEDICINE
export const TELEMEDICINE_BASE_API = 'TELEMEDICINE_BASE_API';
export const TELEMEDICINE_USERNAME = 'TELEMEDICINE_USERNAME';
export const TELEMEDICINE_PASSWORD = 'TELEMEDICINE_PASSWORD';

//REST
export const GAMA_FUSION_GPS = 'GAMA_FUSION_GPS';
export const GAMA_CARE_PLAN_URL = 'GAMA_CARE_PLAN_URL';
export const GAMA_AUTH_URL = 'GAMA_AUTH_URL';
export const GAMA_OAUTH_URL = 'GAMA_OAUTH_URL';

//VOUCHER
export const VOUCHER_TOKEN = 'VOUCHER_TOKEN';
export const VOUCHER_URL = 'VOUCHER_URL';

export const GAMA_USER = 'GAMA_USER';
export const GAMA_PASSWORD = 'GAMA_PASSWORD';
export const GAMA_CLIENT = 'GAMA_CLIENT';
export const GAMA_CLIENT_PASSWORD = 'GAMA_CLIENT_PASSWORD';
export const GAMA_CLIENT_USRNAME = 'GAMA_CLIENT_USRNAME';
