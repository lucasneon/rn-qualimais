import { Container } from 'typedi';
import { DeeplinkListener, PushNotificationListener } from '@app/services/listeners';
import { ProvidersConfig } from './providers.config';

export async function configureServices(): Promise<void> {
  await configureProviders();
  configureListeners();
}

function configureProviders(): Promise<void> {
  const providersConfig: ProvidersConfig = Container.get(ProvidersConfig);
  return providersConfig.hydrateInitialProviders();
}

function configureListeners(): void {
  const pushNotificationListener = Container.get(PushNotificationListener);
  pushNotificationListener.configurePushNotification();

  const deepLinkListener = Container.get(DeeplinkListener);
  deepLinkListener.startListening();
}
