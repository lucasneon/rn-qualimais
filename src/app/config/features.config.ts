import { Container } from 'typedi';
import {
  ActivateHabit,
  ActiveNetworkSearch,
  AppointmentGynecology,
  AppointmentSpeciality,
  EditBenefData,
  PersonalDoctor,
  TermAccpetOfUser,
} from './tokens.config';

export function configureFeatures(env: any) {
  Container.set(EditBenefData, env.EDIT_BENEF_DATA === 'true');
  Container.set(ActivateHabit, env.ACTIVATE_HABIT === 'true');
  Container.set(AppointmentGynecology, env.APPOINTMENT_GYNECOLOGY === 'true');
  Container.set(ActiveNetworkSearch, env.ACTIVE_NETWORK_SEARCH === 'true');
  Container.set(TermAccpetOfUser, env.TERM_ACCEPT_OF_USER === 'true');
  Container.set(AppointmentSpeciality, env.APPOINTMENT_SPECIALITY === 'true');
  Container.set(PersonalDoctor, env.PERSONAL_DOCTOR);
}
