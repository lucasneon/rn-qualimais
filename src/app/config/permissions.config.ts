import { PermissionsAndroid, Platform } from 'react-native';

export async function configurePermissions() {
  await configurePhone();
}

async function configurePhone() {
  if (Platform.OS !== 'android') {
    return;
  }

  try {
    await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CALL_PHONE);
  } catch (err) {
    console.warn(err);
  }
}
