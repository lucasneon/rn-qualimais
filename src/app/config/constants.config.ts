import { Container } from 'typedi';
import {
  ChatClientKey,
  GOOGLE_MAPS_API_KEY,
  PharmacyClientId,
  PharmacyPassword,
  PharmacyTokenUrl,
  PharmacyUsername,
  ShouldCheckTouchIdKey,
  ShouldLogErrorDetailsKey,
  GAMA_SYSTEM_CODE,
  GAMA_OP_CODE,
  GAMA_STIMULATING_CODE,
  GAMA_SYSTEM_ORIGIN,
  GAMA_NETWORK_CODE,
  PRONTLIFE_BASE_URL,
  PRONTLIFE_ACCESS_TOKEN,
  LIVE_TOUCH_API_KEY,
  LIVE_TOUCH_BASE_URL,
  LIVE_TOUCH_CHAT_USER_PASSWORD,
  LIVE_TOUCH_PROJECT_CODE,
  TELEMEDICINE_BASE_API,
  TELEMEDICINE_PASSWORD,
  TELEMEDICINE_USERNAME,
  GAMA_OAUTH_URL,
  GAMA_AUTH_URL,
  GAMA_CARE_PLAN_URL,
  GAMA_FUSION_GPS,
  GAMA_USER,
  GAMA_PASSWORD,
  GAMA_CLIENT,
  GAMA_CLIENT_PASSWORD,
  GAMA_CLIENT_USRNAME,
  VOUCHER_TOKEN,
  VOUCHER_URL,
} from './tokens.config';

export function configureConstants(env: any) {
  Container.set(ChatClientKey, env.CHAT_URL);
  Container.set(ShouldCheckTouchIdKey, env.CHECK_TOUCH_ID === 'true');
  Container.set(ShouldLogErrorDetailsKey, env.LOG_ERROR_DETAILS === 'true');

  // FARMACY
  Container.set(PharmacyTokenUrl, env.PHARMACY_TOKEN_URL);
  Container.set(PharmacyClientId, env.PHARMACY_CLIENT_ID);
  Container.set(PharmacyUsername, env.PHARMACY_USERNAME);
  Container.set(PharmacyPassword, env.PHARMACY_PASSWORD);
  Container.set(GOOGLE_MAPS_API_KEY, env.GOOGLE_MAPS_API_KEY);

  //REST
  Container.set(GAMA_OP_CODE, env.GAMA_OP_CODE);
  Container.set(GAMA_SYSTEM_CODE, env.GAMA_SYSTEM_CODE);
  Container.set(GAMA_STIMULATING_CODE, env.GAMA_STIMULATING_CODE);
  Container.set(GAMA_SYSTEM_ORIGIN, env.GAMA_SYSTEM_ORIGIN);
  Container.set(GAMA_NETWORK_CODE, env.GAMA_NETWORK_CODE);
  Container.set(GAMA_OAUTH_URL, env.GAMA_OAUTH_URL);
  Container.set(GAMA_AUTH_URL, env.GAMA_AUTH_URL);
  Container.set(GAMA_CARE_PLAN_URL, env.GAMA_CARE_PLAN_URL);
  Container.set(GAMA_FUSION_GPS, env.GAMA_FUSION_GPS);

  Container.set(GAMA_USER, env.GAMA_USER);
  Container.set(GAMA_PASSWORD, env.GAMA_PASSWORD);
  Container.set(GAMA_CLIENT, env.GAMA_CLIENT);
  Container.set(GAMA_CLIENT_PASSWORD, env.GAMA_CLIENT_PASSWORD);
  Container.set(GAMA_CLIENT_USRNAME, env.GAMA_CLIENT_USRNAME);

  // Prontlife
  Container.set(PRONTLIFE_BASE_URL, env.PRONTLIFE_BASE_URL);
  Container.set(PRONTLIFE_ACCESS_TOKEN, env.PRONTLIFE_ACCESS_TOKEN);

  //LIVECOM CHAT
  Container.set(LIVE_TOUCH_API_KEY, env.LIVE_TOUCH_API_KEY);
  Container.set(LIVE_TOUCH_BASE_URL, env.LIVE_TOUCH_BASE_URL);
  Container.set(
    LIVE_TOUCH_CHAT_USER_PASSWORD,
    env.LIVE_TOUCH_CHAT_USER_PASSWORD,
  );
  Container.set(LIVE_TOUCH_PROJECT_CODE, env.LIVE_TOUCH_PROJECT_CODE);
  //TELEMEDICINE
  Container.set(TELEMEDICINE_BASE_API, env.TELEMEDICINE_BASE_API);
  Container.set(TELEMEDICINE_USERNAME, env.TELEMEDICINE_USERNAME);
  Container.set(TELEMEDICINE_PASSWORD, env.TELEMEDICINE_PASSWORD);

  //VOUCHER
  Container.set(VOUCHER_TOKEN, env.VOUCHER_TOKEN);
  Container.set(VOUCHER_URL, env.VOUCHER_URL);
}
