import { DeviceOSModel } from './push-notification.entity';

export interface PushRegisterInputModel {
  email: string;
  name: string;
  deviceInfo: DeviceInfoModel;
  appVersionCode: number;
  appVersion: string;
}

export interface UnregisterPushInputModel {
  deviceToken: string;
}

export interface DeviceInfoModel {
  token: string;
  name: string;
  os: DeviceOSModel;
  osVersion: string;
}
