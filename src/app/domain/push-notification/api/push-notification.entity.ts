export interface PushRegisterRequest {
  nome: string;
  login: string;
  email: string;
  senha: string;
  device: DeviceRequest;
}

export interface DeviceRequest {
  projectCode: string;
  registrationId: string;
  userCode: string;
  userEmail: string;
  deviceName: string;
  deviceOs: string;
  deviceOsVersion: string;
  appVersionCode: number;
  appVersion: string;
}

export interface UnregisterPushRequest {
  userCode: string;
  projectCode: string;
  registrationId: string;
}

export interface PushRegisterRootResponse {
  status: string;
  message: string;
  entity: PushRegisterEntityResponse;
}

export interface PushRegisterEntityResponse {
  id: number;
  login: string;
  nome: string;
  email: string;
  empresa: string;
  chatOn: boolean;
  dataOnlineChat: string;
  timestampLogin: number;
  cargo: string;
  urlFotoUsuario: string;
  urlFotoUsuarioThumb: string;
  urlFotoCapa: string;
  tipoAcesso: string;
  fixo: string;
  celular: string;
  status: string;
  origem: string;
  genero: string;
  estado: string;
  dataNasc: string;
  dataCreated: string;
  dataUltimoLogin: string;
  gruposStr: string;
  isBot: boolean;
  permissao: PushRegisterPermissionResponse;
  desc1: string;
  perfilAtivo: boolean;
  preCadastro: boolean;
  push: boolean;
  mostraMural: boolean;
  muralNotifications: boolean;
  chatNotifications: boolean;
}

export interface PushRegisterPermissionResponse {
  id: number;
  nome: string;
  descricao: string;
  publicar: boolean;
  excluirPostagem: boolean;
  editarPostagem: boolean;
  comentar: boolean;
  visualizarComentario: boolean;
  visualizarConteudo: boolean;
  visualizarRelatorio: boolean;
  curtir: boolean;
  enviarMensagem: boolean;
  cadastrarUsuarios: boolean;
  arquivos: boolean;
  padrao: boolean;
  cadastrarTabelas: boolean;
  codigo: boolean;
  permissoes: PushRegisterPermissionsResponse;
}

export interface PushRegisterPermissionsResponse {
  publicar: boolean;
  excluir_empresa: boolean;
  importacao: boolean;
  arquivos: boolean;
  visualizar_conteudo: boolean;
  curtir_comentarios: boolean;
  curtir_publicacao: boolean;
  cadastrar_codigos: boolean;
  excluir_publicacao: boolean;
  comentar: boolean;
  visualizar_comentarios: boolean;
  enviar_mensagem: boolean;
  cadastrar_usuarios: boolean;
  configuracoes_avancadas: boolean;
  enviar_email: boolean;
  exportacao: boolean;
  visualizar_relatorios: boolean;
  cadastrar_grupos: boolean;
  editar_empresa: boolean;
  editar_publicacao: boolean;
  acesso_cadastros: boolean;
}

export interface UnregisterPushResponse {
  status: string;
  message: string;
}

export enum DeviceOSModel {
  Android = 'Android',
  Ios = 'Ios',
}
