import {
  LIVE_TOUCH_API_KEY,
  LIVE_TOUCH_CHAT_USER_PASSWORD,
  LIVE_TOUCH_PROJECT_CODE,
} from '@app/config';
import { LocalDataSource } from '@app/data';
import { InputParams } from '@app/model/api/input.params.model';
import {
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services';
import Container from 'typedi';
import {
  DeviceOSModel,
  PushRegisterRequest,
  UnregisterPushRequest,
} from './push-notification.entity';
import {
  PushRegisterInputModel,
  UnregisterPushInputModel,
} from './push-notification.model';

const apiKey: string = Container.get(LIVE_TOUCH_API_KEY);
const chatPassword: string = Container.get(LIVE_TOUCH_CHAT_USER_PASSWORD);
const projectCode: string = Container.get(LIVE_TOUCH_PROJECT_CODE);

const beneficiaryProvider: BeneficiaryProvider = Container.get(
  BeneficiaryProviderToken,
);
const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const register = async (input: any): Promise<boolean> => {
  const cpf: string = await localDataSource.get<string>('cpf');

  input.cpf = cpf;
  const request: PushRegisterRequest = mapRegister(input);
  const res = await registerRequest(request);
  // console.warn('on register', res);
  if (!res.data || res.data.status !== 'OK') {
    console.error(res.error);
    throw new Error(res.error);
  }

  return true;
};

export const unregisterPushChat = async (input: UnregisterPushInputModel) => {
  const idHealthCareCard: string = await localDataSource.get<string>(
    'idHealthCareCard',
  );
  const request = {
    projectCode: projectCode,
    userCode: idHealthCareCard,
    registrationId: input.deviceToken,
  };
  // TODO verificar se unregister no chat esta ok
  const res = await unregisterRequest(request);

  if (!res.data || res.data.status !== 'OK') {
    throw new Error(res.error);
  }
  return true;
};

const registerRequest = async (data: any) => {
  const url = 'rest/push/registerLivecom';

  const headers = {
    'Content-Type': 'application/json',
    Authorization: apiKey,
  };
  let res;
  try {
    res = await axiosPost(
      {
        url,
        data,
        baseUrl: BaseUrl.LIVE_TOUCH_BASE_URL,
      },
      headers,
    );
  } catch (e) {
    console.error(e.message, url);
    return { data: null, error: e };
  }

  console.warn('registerLivecom res >', res.data);
  return { data: res.data, error: null };
};

const unregisterRequest = async (req: UnregisterPushRequest) => {
  let res;
  try {
    res = await axiosPost(
      {
        url: 'rest/push/unregister',
        data: req,
        baseUrl: BaseUrl.LIVE_TOUCH_BASE_URL,
      },
      { Authorization: apiKey },
    );
  } catch (e) {
    return { data: null, error: e };
  }
  return { data: res.data, error: null };
};

const mapRegister = (
  input: InputParams<PushRegisterInputModel>,
): PushRegisterRequest => {
  const osMap: { [key in DeviceOSModel]: string } = {
    [DeviceOSModel.Android]: 'android',
    [DeviceOSModel.Ios]: 'ios',
  };

  const deviceInfo = input.deviceInfo;

  return {
    nome: input.name,
    login: input.cpf,
    email: input.email,
    senha: chatPassword,
    device: {
      projectCode: projectCode,
      appVersion: input.appVersion,
      appVersionCode: input.appVersionCode,
      deviceName: deviceInfo.name,
      deviceOs: osMap[deviceInfo.os],
      deviceOsVersion: deviceInfo.osVersion,
      registrationId: deviceInfo.token,
      userCode: beneficiaryProvider.state.active.idHealthCareCard,
      userEmail: input.email,
    },
  };
};
