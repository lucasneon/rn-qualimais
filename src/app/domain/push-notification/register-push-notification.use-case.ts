import { Platform } from 'react-native';
import { Container, Service } from 'typedi';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import DeviceInfo from 'react-native-device-info';
import messaging from '@react-native-firebase/messaging';
import {
  DeviceOS,
  PushRegisterInput,
  RegisterPushChatMutationVariables,
} from '@app/data/graphql';
import {
  BeneficiaryProvider,
  BeneficiaryProviderToken,
} from '@app/services/providers';
import { register } from './api/push-register.service';
import { resolvePlugin } from '@babel/core';

@Service()
export class RegisterPushNotificationUseCase {
  private graph: GraphQLProvider = Container.get(GraphQLProviderToken);
  private beneficiaryProvider: BeneficiaryProvider = Container.get(
    BeneficiaryProviderToken,
  );

  execute = async (): Promise<boolean> => {
    const isLogged: boolean = !!this.beneficiaryProvider.state.active;

    if (!isLogged) {
      return false;
    }

    await messaging().registerDeviceForRemoteMessages();
    const [deviceName, appVersion, appVersionCode, token] = await Promise.all([
      DeviceInfo.getDeviceName(),
      DeviceInfo.getBuildNumber(),
      DeviceInfo.getVersion(),
      messaging().getToken(),
    ]);

    const data: PushRegisterInput = {
      email: this.beneficiaryProvider.state.active.email,
      name: this.beneficiaryProvider.state.active.name,
      deviceInfo: {
        token,
        name: deviceName,
        os: Platform.OS === 'ios' ? DeviceOS.Ios : DeviceOS.Android,
        osVersion: Platform.Version.toString(),
      },
      appVersionCode: parseInt(appVersionCode, 10),
      appVersion: appVersion,
    };

    const res = await register(data);
    if (!res) {
      return false;
    }

    return true;

    // return new Promise<boolean>(resolve => {
    //   this.graph.mutate<RegisterPushChatMutationVariables>(
    //     'push-register',
    //     { data },
    //     () => {
    //       resolve(true);
    //     },
    //     err => {
    //       console.warn('Failure to register for push notification: ', err);
    //       resolve(false);
    //     },
    //     false,
    //   );
    // });
  };
}
