import { Service } from 'typedi';
import { NavigationDestination, NavigationRequest } from '@app/model';

interface DeeplinkOption {
  matchRoute: (url: string) => boolean;
  parse: (url?: string) => any;
}

@Service()
export class ParseDeeplinkUseCase {

  private readonly options: DeeplinkOption[] = this.getInternalDeeplinkOptions();

  execute(url: string): NavigationRequest {
    let request: NavigationRequest;
    try {
      const matchedOption = this.options.find(option => option.matchRoute(url));
      request = matchedOption.parse(url);
    } catch (err) {
      request = null;
    }

    return request;
  }

  private getInternalDeeplinkOptions(): DeeplinkOption[] {
    return [
      this.simpleDeeplinkParser(NavigationDestination.AppointmentReview, /\/\/review/),
      this.simpleDeeplinkParser(NavigationDestination.Chat, /\/\/chat/),
    ];
  }

  private simpleDeeplinkParser(destination: NavigationDestination, regex: RegExp, params?: any): DeeplinkOption {
    return {
      matchRoute: (url: string) => {
        return regex.test(url);
      },
      parse: () => ({ destination, params }),
    };
  }

}
