import Container, { Service } from 'typedi';
import { LocalNotificationDatasource } from '@app/data/local';
import { LocalNotification, Medication, NavigationDestination } from '@app/model';
import { MedicationNotificationGenerator } from './medication-notification-generator.utils';

const Title = 'Medicamento';
const Description = (name: string) => `Hora de tomar o medicamento ${name}.`;

@Service()
export class ScheduleMedicationNotificationsUseCase {

  private readonly datasource: LocalNotificationDatasource = Container.get(LocalNotificationDatasource);

  /* tslint:disable:no-empty */
  constructor() { }

  execute(input: Medication) {
    try {
      const generator = new MedicationNotificationGenerator(input);

      const scheduleDates: Date[] = generator.generateDates();
      const ids: string[] = generator.generateIds();

      const baseNotification: LocalNotification = {
        title: Title,
        message: Description(input.medicineName),
        navigationRequest: {
          destination: NavigationDestination.MedicationDetails,
          data: { medicationId: input.medicationId, medicationName: input.medicineName },
        },
        repeatType: input.end ? undefined : 'daily',
      };

      return scheduleDates.map((date, index) => this.datasource.schedule({ ...baseNotification, id: ids[index] }, date));
    } catch (ex) {
      return null;
    }

  }
}
