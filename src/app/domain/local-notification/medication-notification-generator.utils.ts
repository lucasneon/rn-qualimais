import moment, { Moment } from 'moment';
import { Medication } from '@app/model';

export class MedicationNotificationGenerator {
  private readonly medication: Medication;

  private dates?: Date[];

  constructor(medication: Medication) {
    this.medication = medication;
  }

  generateDates(): Date[] {
    const startDates: Moment[] = this.medication.dailySchedules.map(dailyHour => {
      const start: Moment = moment(this.medication.start);
      const time: Moment = moment(dailyHour, 'HH:mm:ss.SSSZ');

      return start.add(time.hour(), 'hours').add(time.minute(), 'minutes');
    });

    if (!this.medication.end) {
      return this.dates = startDates.map(startMoment => startMoment.toDate());
    }

    const scheduleDates: Date[] = [];
    const end = this.medication.end;

    while (startDates[0].isSameOrBefore(end, 'day')) {
      startDates.forEach(startDate => {
        scheduleDates.push(startDate.toDate());
        startDate = startDate.add(1, 'day');
      });
    }

    return this.dates = scheduleDates;
  }

  generateIds(): string[] {
    if (!this.dates) {
      this.generateDates();
    }

    return this.dates.map((_, index) => this.medication.medicationId + index);
  }
}
