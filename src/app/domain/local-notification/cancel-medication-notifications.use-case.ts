import { Service } from 'typedi';
import { LocalNotificationDatasource } from '@app/data/local';
import { Medication } from '@app/model';
import { MedicationNotificationGenerator } from './medication-notification-generator.utils';

@Service()
export class CancelMedicationNotificationsUseCase {
  constructor(private readonly datasource: LocalNotificationDatasource) {}

  execute(input: Medication) {
    const generator = new MedicationNotificationGenerator(input);
    const ids: string[] = generator.generateIds();

    ids.forEach(id => this.datasource.cancelScheduledNotification(+id));
  }
}
