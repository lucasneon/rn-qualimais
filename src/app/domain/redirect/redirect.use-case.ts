import { Service } from 'typedi';
import { Navigation } from '@app/core/navigation';
import {
  AppointmentDetailsPush, AppointmentReviewPush, ChatPush, MedicationDetailsPush, NavigationDestination,
  NavigationRequest, PushNotificationData
} from '@app/model';
import { AppointmentDetailPage } from '@app/modules/common/events';
import { ChatPage, ChatPageProps } from '@app/modules/customer-care';
import { MedicationDetailsPage, MedicationDetailsPageProps } from '@app/modules/habit/medication';

@Service()
export class NavigateToUseCase {

  execute(notificationData: NavigationRequest): void {
    let data: PushNotificationData;

    switch (notificationData.destination) {
      case NavigationDestination.AppointmentDetails:
        data = (notificationData.data as AppointmentDetailsPush);
        Navigation.resetAndSelectTab('HomePage', 0);
        Navigation.push(
          'HomePage',
          AppointmentDetailPage,
          { appointmentDetailsInput: notificationData.data },
        );
        break;

      case NavigationDestination.AppointmentReview:
        data = (notificationData.data as AppointmentReviewPush);
        Navigation.resetAndSelectTab('HomePage', 0);
        Navigation.push(
          'HomePage',
          AppointmentDetailPage,
          { appointmentDetailsInput: notificationData.data, review: true },
        );
        break;

      case NavigationDestination.Chat:
        data = (notificationData.data as ChatPush);
        Navigation.resetAndSelectTab('CustomerCarePage', 2);
        Navigation.showModal<ChatPageProps>(ChatPage, { chatId: data.chatId });
        break;

      case NavigationDestination.MedicationDetails:
        data = (notificationData.data as MedicationDetailsPush);
        Navigation.resetAndSelectTab('HomePage', 0);
        Navigation.push<MedicationDetailsPageProps>(
          'HomePage',
          MedicationDetailsPage,
          { medicationId: data.medicationId, medicationName: data.medicationName },
        );
        break;

      default:
        Navigation.setCurrentTab('HomePage', 0);
    }
  }
}
