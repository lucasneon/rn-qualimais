import * as React from 'react';
import { MagazineCell } from './magazine-cell.component';
import { MagazineListStyled } from './magazine-list.component.style';

export class MagazineList extends React.Component<any, any> {
  static Item = MagazineCell;

  render() {
    const count = React.Children.count(this.props.children);
    const children = React.Children.map(
      this.props.children,
      (child: any, i: number) => {
        const props = {
          index: i,
          total: count,
          style: undefined,
        };

        if (i === 0) {
          props.style = { marginLeft: 16 };
        } else if (i === count - 1) {
          props.style = { marginRight: 16 };
        }

        return React.cloneElement(child, props);
      },
    );

    return <MagazineListStyled>{children}</MagazineListStyled>;
  }
}
