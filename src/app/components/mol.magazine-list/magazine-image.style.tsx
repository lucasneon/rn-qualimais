import styled from 'styled-components/native';

export const MagazineImageStyled = styled.Image`
  width: 100px;
  height: 150px;
  resize-mode: cover;
  shadow-color: #000;
  shadow-offset: 1px 1px;
  shadow-opacity: 0.5;
  shadow-radius: 2px;
`;
