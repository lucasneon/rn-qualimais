import * as React from 'react';
import { VBox, VSeparator } from '@atomic';
import { MagazineCellStyled, MagazineTouchableCellStyled } from './magazine-cell.component.style';
import { MagazineImage } from '@app/components/mol.magazine-list';
import { ListInfo } from '@app/components/mol.list-info';

export interface MagazineCellProps extends Readonly<{ children?: React.ReactNode }> {
  label: string;
  value: string;
  caption: string;
  onTap?: () => any;
  disabled?: boolean;
  index?: number;
  total?: number;
  image?: string;
  date?: string;
  file: FileCellProps;
}

export interface FileCellProps {
  id: number;
  hash: string;
  url: string;
}

export const MagazineCell: React.SFC<MagazineCellProps>  = (props: MagazineCellProps) => {

  const handleTap = () => {
    return props.onTap && props.onTap();
  };

  return (
      <MagazineTouchableCellStyled disabled={props.disabled} onPress={handleTap}>
        <MagazineCellStyled index={props.index} total={props.total} type='secondary'>
          {props.image && <MagazineImage image={props.image} />}
          <VBox noGutter={true} >
            <VSeparator />
            <ListInfo
              title={`${props.label.substring(0, 15)}...`}
              descriptions={[`${props.caption.substring(0, 15)}...`]}
            />
          </VBox>
        </MagazineCellStyled>
      </MagazineTouchableCellStyled>
  );
};
MagazineCell.defaultProps = {
  index: 0,
  total: 1,
};
