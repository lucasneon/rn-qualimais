import styled from 'styled-components/native';
import { CellBackgroundStyled } from '@app/components/mol.cell/cell.component.style';
import { Border, Opacity, Spacing } from '@atomic';

export const MagazineTouchableCellStyled = styled.TouchableOpacity.attrs({
  activeOpacity: Opacity.Active,
})`
  min-width: 150;
`;

export const MagazineCellStyled = styled(CellBackgroundStyled)`
  padding-vertical: ${Spacing.Small};
  padding-horizontal: ${Spacing.Medium};
  border-radius: ${Border.Radius};
  padding-left: ${Spacing.Medium};
  margin-right: ${Spacing.Medium};
  margin-bottom: ${Spacing.Medium};
  `;
  