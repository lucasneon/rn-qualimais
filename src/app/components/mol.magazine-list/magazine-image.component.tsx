import * as React from 'react';
import { MagazineImageStyled } from './magazine-image.style';

export interface MagazineImageProps {
    image: string;
}

export const MagazineImage = (props: MagazineImageProps) => (
  <MagazineImageStyled source={{ uri: `data:image/png;base64,${props.image}` }}/>
);
