import * as React from 'react';
import LogoQualiMais from '@assets/images/logo/logo-quali-mais.png';

import { LogoStyled } from './logo.component.style';

export const Logo = () => <LogoStyled source={LogoQualiMais} />;
