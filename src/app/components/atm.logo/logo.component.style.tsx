import styled from 'styled-components/native';
import { Spacing } from '@atomic';

export const LogoStyled = styled.Image`
  margin-top: ${Spacing.XLarge};
  margin-bottom: ${Spacing.XLarge};
  align-self: center;
  width: 132;
  height: 40;
  resize-mode: contain;
`;
