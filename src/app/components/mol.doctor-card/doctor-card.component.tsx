import * as React from 'react';
import { Avatar } from '@app/components/atm.avatar';
import { Card } from '@app/components/atm.card';
import { HBox } from '@atomic';
import { ImagePropertiesSourceOptions } from 'react-native';

interface DoctorCardProps {
  onTap?: () => void;
  avatar: ImagePropertiesSourceOptions;
  children: any;
}

export const DoctorCard: React.SFC<DoctorCardProps> = (props: DoctorCardProps) => {
  return (
    <Card onTap={props.onTap ? props.onTap : undefined}>
      <HBox>
        <HBox.Item wrap={true}>
          <Avatar source={props.avatar}/>
        </HBox.Item>
        <HBox.Separator />
        <HBox.Item vAlign='center'>
          {props.children}
        </HBox.Item>
      </HBox>
    </Card>
  );
};
