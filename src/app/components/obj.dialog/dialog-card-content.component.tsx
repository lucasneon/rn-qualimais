import moment from 'moment';
import * as React from 'react';
import { EventsStrings } from '@app/modules/common/events/events.strings';
import {
  Body,
  DividerGray,
  FontSize,
  H3,
  PrimaryButton,
  Shimmer,
  ShimmerSeparator,
  VBox,
  VSeparator,
} from '@atomic';
import { LinkButtonTextStyled } from '@atomic/atm.button/link-button.component.style';
import { PickerWrapper } from '@atomic/atm.picker/picker-wrapper.component';
import { CardActionSheet } from '../mol.card-action-sheet';

export interface DateInputDialog {
  description?: string;
  maxDate: Date;
  minDate?: Date;
  loading: boolean;
  visible: boolean;
  submit: (data: any) => void;
  onCancel: () => void;
}

export const DateDialogContentShimmer = () => (
  <VBox hAlign="center">
    <Shimmer height={FontSize.Medium} width="70%" />
    <ShimmerSeparator />
    <Shimmer height={FontSize.Small} width="90%" />
    <VSeparator />
    <ShimmerSeparator />
    <Shimmer height={FontSize.Small} width="35%" />
  </VBox>
);

export class ExamDialog extends React.PureComponent<DateInputDialog> {
  private pickedDate: Date;

  constructor(props) {
    super(props);
    this.state = { content: 'no-rate' };
  }

  render() {
    return (
      <CardActionSheet
        visible={this.props.visible}
        onCancel={this.props.onCancel}>
        <PickerWrapper
          mode="date"
          minDateTime={new Date(this.props.minDate)}
          maxDateTime={this.props.maxDate}>
          {date => {
            this.pickedDate = date;
            return (
              <>
                <H3 style={{ textAlign: 'center' }}>
                  {EventsStrings.Exam.Question}
                </H3>
                <VBox hAlign="center">
                  <Body>{this.props.description}</Body>
                  <VSeparator />
                  <LinkButtonTextStyled>
                    {moment(date).format('DD [de] MMMM [de] YYYY')}
                  </LinkButtonTextStyled>
                </VBox>
                <VSeparator />
                <DividerGray />
                <VSeparator />
                <VBox>
                  <PrimaryButton
                    expanded={true}
                    text={'Salvar'}
                    onTap={this.handleSubmit}
                    loading={this.props.loading}
                  />
                </VBox>
              </>
            );
          }}
        </PickerWrapper>
      </CardActionSheet>
    );
  }

  private handleSubmit = () => {
    this.props.submit(this.pickedDate);
  };
}
