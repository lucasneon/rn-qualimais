import { Badge } from '@app/components/atm.badge';
import { Card } from '@app/components/atm.card';
import { RadioField } from '@app/components/atm.radio/radio-field.component';
import { AutoFormSample, Form, Validators } from '@atomic';
import { H2, H3, InputLabel } from '@atomic/atm.typography';
import { VBox } from '@atomic/obj.grid';
import { storiesOf } from '@storybook/react-native';
import * as React from 'react';
import { Text } from 'react-native';

const stories = storiesOf('Form', module);

stories.add('CustomRadioField', () => {
  let selected = null;
  const handleChange = (value: number) => {
    selected = value;
    console.log(selected);
  };
  return (
    <AutoFormSample>
      <VBox>
        <H2>Custom Radio Fields Sample</H2>
        <InputLabel>Radio group</InputLabel>
        <Form.Field
          onValueChange={handleChange}
          value={null}
          name='radio'
          validators={[Validators.Required('Required')]}
        >
          <RadioField id={null}>
            <Card><Text>Selecione</Text></Card>
          </RadioField>
          <RadioField id={1}>
            <Badge type={selected === 1 ? 'primary' : 'neutral'} text='1' />
          </RadioField>
          <RadioField id={2}>
            <Text>
              <H3>2</H3>
            </Text>
          </RadioField>
        </Form.Field>
      </VBox>
    </AutoFormSample>
  );
});
