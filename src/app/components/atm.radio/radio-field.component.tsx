import * as React from 'react';
import { FormFieldContexState, FormFieldContext } from '@atomic/obj.form/form-field.component';

import { RadioFieldStyled } from './radio-field.component.style';

export interface RadioFieldProps {
  id: any;
  children?: any;
  onValueChange?: (id: any) => void;
}

export const RadioField = (props: RadioFieldProps) => {
  let formFieldConsumer: FormFieldContexState;

  const radioFieldContent = React.Children.map(props.children, child => {
    return React.cloneElement(child as React.ReactElement<any>);
  });

  const handlePress = () => {
    if (props.onValueChange) {
      props.onValueChange(props.id);
    }
    if (formFieldConsumer) {
      formFieldConsumer.onValueChange(props.id, null);
    }
  };

  return (
    <FormFieldContext.Consumer>
      {(formField: FormFieldContexState) => {
        formFieldConsumer = formField;
        if (!formField) {
          throw new Error('<RadioField /> must be wrapped with a <Form.Field> tag');
        }
        return (
          <RadioFieldStyled onPress={handlePress}>
            {radioFieldContent}
          </RadioFieldStyled>
        );
      }}
    </FormFieldContext.Consumer>
  );
};
