import { Spacing } from '@atomic/obj.constants';
import styled from 'styled-components/native';

export const RadioFieldStyled = styled.TouchableOpacity`
  flex-wrap: wrap;
  flex-direction: row;
  align-items: center;
  margin-bottom: ${Spacing.Small};
`;
