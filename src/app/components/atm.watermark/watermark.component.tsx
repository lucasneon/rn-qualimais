import * as React from 'react';
import { Asset } from '@atomic';
import {
  WatermarkImageStyled, WatermarkImageWrapperStyled, WatermarkStyled, WatermarkStyledProps
} from './watermark.component.style';

export enum WatermarkType {
  Consultation = Asset.Background.Consultation,
  Drinking = Asset.Background.Drinking,
  Exam = Asset.Background.Exam,
  Exercise = Asset.Background.Exercise,
  Food = Asset.Background.Food,
  Medication = Asset.Background.Medication,
  Smoking = Asset.Background.Smoking,
  LogoLines = Asset.Background.LogoLines,
  Health = Asset.Background.HealthBackground,
  Body = Asset.Background.Body,
  Weight = Asset.Background.WeightBackground,
}

export interface WatermarkProps extends WatermarkStyledProps {
  type: WatermarkType;
}

export const Watermark: React.SFC<WatermarkProps> = (props: WatermarkProps) => {

  return (
    <>
      <WatermarkStyled header={props.header}>
        <WatermarkImageWrapperStyled vAlign={props.vAlign}>
          <WatermarkImageStyled light={props.light} big={props.big} source={props.type} />
        </WatermarkImageWrapperStyled>
      </WatermarkStyled>
    </>
  );
};

Watermark.defaultProps = {
  big: false,
  light: false,
};
