import styled from 'styled-components/native';
import { Spacing } from '@atomic';

export interface WatermarkStyledProps {
  big?: boolean;
  vAlign?: string;
  light?: boolean;
  header?: boolean;
}

export const WatermarkStyled = styled.View`
  position: absolute;
  right: -${props => props.header ? Spacing.Large + Spacing.Small : Spacing.Small};
  bottom: ${props => !props.header ? Spacing.XSmall : '0'};
  left: 0;
  top: 0;
  align-items: flex-end;
`;

export const WatermarkImageWrapperStyled = styled.View`
  justify-content: ${(props: WatermarkStyledProps) => props.vAlign ? props.vAlign : 'flex-end'};
  height: 100%;
  overflow: hidden;
`;
export const WatermarkImageStyled = styled.Image`
  height: ${(props: WatermarkStyledProps) => props.big ? 120 : 48};
  width: ${(props: WatermarkStyledProps) => props.big ? 120 : 48};
  resize-mode: contain;
  opacity: ${(props: WatermarkStyledProps) => props.light ? 0.6 : 1};
`;
