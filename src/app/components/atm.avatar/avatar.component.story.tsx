import * as React from 'react';
import AvatarImage from '@assets/images/samples/avatar.png';
import { Avatar } from './avatar.component';
import { Scroll } from '@atomic/obj.scroll';
import { storiesOf } from '@storybook/react-native';
import { VBox, VSeparator } from '@atomic';

const stories = storiesOf('Atoms', module);

stories.add('Avatar', () => (
  <Scroll>
    <VBox>
      <Avatar source={AvatarImage}  />
    </VBox>
    <VSeparator />

  </Scroll>
));
