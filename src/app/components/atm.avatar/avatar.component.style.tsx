import { AvatarProps } from '@app/components/atm.avatar/avatar.component';
import { AvatarSize, Border, Color } from '@atomic';
import styled from 'styled-components/native';

export const AvatarStyled = styled.Image`
  width: ${(props: AvatarProps) => props.big ? AvatarSize.Big : AvatarSize.Small};
  height: ${(props: AvatarProps) => props.big ? AvatarSize.Big : AvatarSize.Small};
  border-radius: ${(props: AvatarProps) => props.big ? AvatarSize.Big / 2 : AvatarSize.Small / 2};
  background-color: ${Color.GrayLight};
  ${(props: AvatarProps) => props.withBorder ? `
  border-color: ${Color.White};
  border-width: ${Border.Width};` : ''}
`;
