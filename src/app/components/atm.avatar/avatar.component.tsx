import * as React from 'react';
import { AvatarStyled } from './avatar.component.style';
import { ImagePropertiesSourceOptions } from 'react-native';

export interface AvatarProps {
  source?: ImagePropertiesSourceOptions;
  big?: boolean;
  withBorder?: boolean;
}

export const Avatar: React.SFC<AvatarProps>  = (props: AvatarProps) => {
  return <AvatarStyled source={props.source} big={props.big} withBorder={props.withBorder} />;
};
