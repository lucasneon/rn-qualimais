import * as React from 'react';
import { NewsImageStyled } from './news-image.style';

export interface NewsImageProps {
    image: string;
}

export const NewsImage = (props: NewsImageProps) => (
  <NewsImageStyled source={{ uri: `data:image/png;base64,${props.image}` }}/>
);
