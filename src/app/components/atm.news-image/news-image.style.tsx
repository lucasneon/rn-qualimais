import styled from 'styled-components/native';
import { Spacing } from '@atomic';
import { Dimensions } from 'react-native';

export const NewsImageStyled = styled.Image`
  margin-bottom: ${Spacing.XLarge};
  width: ${Dimensions.get('window').width};
  height: ${Dimensions.get('window').width * 3 / 4};
  resize-mode: contain;
`;
