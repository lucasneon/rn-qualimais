import styled from 'styled-components/native';
import { TopAreaViewProps } from './top-area-view.component';

export const TopAreaViewStyled = styled.View`
  height: ${(props: TopAreaViewProps) => props.height || '0'};
`;
