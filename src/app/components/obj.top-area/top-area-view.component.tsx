import * as React from 'react';
import { Platform, SafeAreaView } from 'react-native';
import { TopAreaViewStyled } from './top-area-view.component.style';

export interface TopAreaViewProps {
  height?: number;
}

export const TopAreaView: React.SFC<TopAreaViewProps> = props => {
  return Platform.OS === 'android' ? (
    <TopAreaViewStyled height={props.height} />
  ) : (
    <SafeAreaView />
  );
};
