import { TqSafeAreaView } from '@atomic';
import * as React from 'react';
import { SmallHeaderStyled, SmallHeaderTitleStyled } from './small-header.component.style';

export interface SmallHeaderProps {
  title: string;
}

export const SmallHeader: React.SFC<SmallHeaderProps> = (props: SmallHeaderProps) => {

  return (
    <SmallHeaderStyled {...props}>
      <TqSafeAreaView />
      <SmallHeaderTitleStyled {...props}>{props.title}</SmallHeaderTitleStyled>
    </SmallHeaderStyled>
  );
};
