import { H1Large, TqSafeAreaView, VBox, HBox } from '@atomic';
import * as React from 'react';
import { LargeHeaderStyled } from './large-header.component.style';

interface LargeHeaderProps {
  title: string;
  buttons?: any[];
}

export const LargeHeader: React.SFC<LargeHeaderProps> = (props: LargeHeaderProps) => {

  return (
    <LargeHeaderStyled >
      <TqSafeAreaView />
      <VBox>
        <HBox>
          <H1Large>{props.title}</H1Large>
          <HBox.Separator />
          {/* <HBox.Item vAlign='flex-start' hAlign={'flex-end'}> */}
            {props.buttons}
          {/* </HBox.Item> */}
        </HBox>
      </VBox>
    </LargeHeaderStyled>
  );
};
