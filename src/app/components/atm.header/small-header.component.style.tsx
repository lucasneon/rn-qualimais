import styled from 'styled-components/native';
import { Color, FontSize } from '@atomic/obj.constants';
import { H1 } from '@atomic';

const SMALL_HEADER_HEIGHT = 32;

export const SmallHeaderStyled = styled.View`
  background-color: ${Color.White};
`;

export const SmallHeaderTitleStyled = styled(H1)`
  height: ${SMALL_HEADER_HEIGHT};
  text-align: center;
  font-size: ${FontSize.Medium};
`;
