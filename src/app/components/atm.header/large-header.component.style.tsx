import styled from 'styled-components/native';
import { Color } from '@atomic/obj.constants';
import { Platform } from '@atomic';

const StatusBarHeight = Platform.OS === 'ios' ? Platform.getStatusBarHeight() : 0;

export const LargeHeaderHeight = 96 + StatusBarHeight;

export const LargeHeaderStyled = styled.View`
  height: ${LargeHeaderHeight};
  background-color: ${Color.White};
  justify-content: flex-end;
`;

export const LargeHeaderBottomStyled = styled.View`
  height: 20;
  border-top-left-radius: 20;
  border-top-right-radius: 20;
  background-color: ${Color.White};
  align-self: stretch;
`;
