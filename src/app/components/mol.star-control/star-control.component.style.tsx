import styled from 'styled-components/native';
import { Asset, Color } from '@atomic';

interface StarImageProps {
  highlighted: boolean;
}

export const StarImageStyled = styled.Image.attrs({
  source: Asset.Image.Star,
})`
  ${(props: StarImageProps) => props.highlighted ? `tint-color: ${Color.StarHighlightedColor}` : ''}
`;
