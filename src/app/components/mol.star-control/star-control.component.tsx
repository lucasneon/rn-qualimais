import * as React from 'react';
import { Platform, TouchableNativeFeedback, TouchableOpacity } from 'react-native';
import { HBox } from '@atomic';
import { StarImageStyled } from './star-control.component.style';

const Touchable: any = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

interface StarControlProps {
  onStarTap?: (index: number) => void;
}

interface StarControlState {
  selected: number;
}

export const NoneSelected = -1;

export class StarControl extends React.PureComponent<StarControlProps, StarControlState> {

  constructor(props) {
    super(props);
    this.state = { selected: NoneSelected };
  }

  render() {
    return (
      <>
        <HBox hAlign='center'>
          {new Array(5).fill(null).map((_, i) =>
            <HBox.Item hAlign='center' key={i}>
              <Touchable onPress={this.handleStarTap(i)}>
                <StarImageStyled highlighted={i <= this.state.selected} />
              </Touchable>
            </HBox.Item>,
          )}
        </HBox>
      </>
    );
  }

  private handleStarTap = (i: number) => {
    return () => {
      const shouldDeselect: boolean = this.state.selected === i;
      const selected = shouldDeselect ? NoneSelected : i;
      this.setState({ selected });

      if (this.props.onStarTap) {
        this.props.onStarTap(selected);
      }
    };
  }
}
