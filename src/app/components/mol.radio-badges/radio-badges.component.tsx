import moment, { Moment } from 'moment';
import * as React from 'react';
import { Badge, BadgeShimmer } from '@app/components/atm.badge';
import { RadioField } from '@app/components/atm.radio';
import { DaySlotFragment, SlotFragment } from '@app/data/graphql';
import { Body, HBox } from '@atomic';
import { DaySlotModel } from '@app/modules/common/api/professional-schedule/professional-schedule.model';

export interface RadioBadgesProps {
  daySlot: DaySlotModel;
  selectedHour: Moment;
}

const EmptyState = <Body>Não há horários disponíveis para este dia</Body>;

export const RadioBadgesShimmer: React.SFC<{ count: number }> = props => (
  <HBox wrap={true}>
    {new Array(props.count || 3).fill(null).map((_, i) => <BadgeShimmer key={i} /> )}
  </HBox>
);

export const RadioBadges: React.SFC<RadioBadgesProps> = props => {
  const slots = props.daySlot && props.daySlot.slots ? props.daySlot.slots : [];

  const mapTags = (slot: SlotFragment) => {
    const startDate = moment(slot.start);
    const isSelected = () => props.selectedHour ? !props.selectedHour.diff(startDate) : false;
    return (
      <RadioField
        key={startDate.format('HH:mm')}
        id={{start: slot.start, end: slot.end}}
      >
        <Badge text={startDate.format('HH:mm')} type={isSelected() ? 'primary' : 'neutral'} />
      </RadioField>
    );
  };

  return (
    <HBox wrap={true}>
      {slots.length > 0 ? slots.map(mapTags) : EmptyState}
    </HBox>
  );
};
