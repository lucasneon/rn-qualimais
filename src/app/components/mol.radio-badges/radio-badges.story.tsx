import moment from 'moment';
import * as React from 'react';
import { DaySlotFragment, SlotFragment } from '@app/data/graphql';
import { Form, H2 } from '@atomic';
import { storiesOf } from '@storybook/react-native';
import { RadioBadges, RadioBadgesShimmer } from './radio-badges.component';

const stories = storiesOf('Molecules', module);

const daySlots: DaySlotFragment = {
  day: '2019-01-22',
  slots: [
    { start: '2019-01-22T08:00:00Z', end: '2019-01-22T08:30:00Z' },
    { start: '2019-01-22T08:30:00Z', end: '2019-01-22T09:00:00Z' },
    { start: '2019-01-22T09:00:00Z', end: '2019-01-22T09:30:00Z' },
  ],
};

const handleTagSelection = (selected: SlotFragment) => {
  console.warn(`Selected ${selected} tag`);
};

stories.add('Radio badges', () => (
  <>
    <H2>Radios badges</H2>
    <Form.Field onValueChange={handleTagSelection} name='hours'>
      <RadioBadges daySlot={daySlots} selectedHour={moment('2019-01-22T08:30:00Z')} />
    </Form.Field>
    <H2>Radios badges - shimmer</H2>
    <RadioBadgesShimmer count={10} />
  </>
));
