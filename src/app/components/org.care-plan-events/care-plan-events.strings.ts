
export const CarePlanEventsStrings = {
  Empty: {
    Title: 'Sem eventos',
    Message: 'Você ainda não possui eventos.',
  },
  Fulfill: {
    Done: (date: Date | string) => `realizado em: ${date}`,
    Scheduled: (date: Date | string) => `agendado para: ${date}`,
    Until: (date: Date | string) => `realizar até: ${date}`,
    Expired: (date: Date | string) => `vencido em: ${date}`,

    Appointment: {
      Done: (date: string) => `consulta realizada em: ${date}`,
      Scheduled: (date: string) => `consulta agendada para ${date}`,
      Until: (date: string) => `realizar consulta até ${date}`,
      Expired: (date: string) => `consulta vencida em ${date}`,
    },
    Exam: {
      Done: (date: string) => `exame realizado em: ${date}`,
      Scheduled: (date: string) => `exame agendado para ${date}`,
      Until: (date: string) => `realizar exame até ${date}`,
      Expired: (date: string) => `exame vencido em ${date}`,
    },
  },

};
