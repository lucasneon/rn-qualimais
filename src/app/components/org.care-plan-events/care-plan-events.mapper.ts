import { WatermarkType } from '@app/components/atm.watermark';
import { EventCellProps } from '@app/components/mol.cell';
import {
  CarePlanItemFragment, CarePlanProcedureFragment, CarePlanScheduleFragment, ProcedureType
} from '@app/data/graphql';
import { DateUtils } from '@app/utils';
import { CarePlanEventsStrings } from './care-plan-events.strings';

const Strings = CarePlanEventsStrings;

export const CarePlanEventsMapper = {
  map(carePlanItem: CarePlanItemFragment): EventCellProps {
    const procedure: CarePlanProcedureFragment = carePlanItem.procedure;
    const procedureType: ProcedureType = procedure.type;
    const specialty: string = (carePlanItem.specialty && carePlanItem.specialty.description) ||
      (procedure && procedure.description);

    const procedureTypeMap: { [type in ProcedureType]: { tag: any, type: any } } = {
      [ProcedureType.Appointment]: { tag: Strings.Fulfill.Appointment, type: WatermarkType.Consultation },
      [ProcedureType.Exam]: { tag: Strings.Fulfill.Exam, type: WatermarkType.Exam },
    };

    return {
      title: procedureType === ProcedureType.Appointment ? specialty : procedure && procedure.description,
      auxiliar: this.mapEventDateInfo(carePlanItem.schedule,
      procedureTypeMap[procedureType] &&  procedureTypeMap[procedureType].tag),
      ...procedureTypeMap[procedureType],
    };
  },

  mapEventDateInfo(schedule: CarePlanScheduleFragment, phrase: any): string {
    const isExpired: boolean = new Date() > new Date(schedule.limitDate);
    const present = (date: Date | string) => DateUtils.Format.presentation(date);

    return schedule.doneAt ? phrase.Done(present(schedule.doneAt)) :
      isExpired ? phrase.Expired(present(schedule.limitDate)) :
      schedule.scheduledFor ? phrase.Scheduled(present(schedule.scheduledFor)) :
      phrase && phrase.Until(present(schedule.limitDate));
  },
};
