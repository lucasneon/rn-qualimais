import * as React from 'react';
import { FlatList, ListRenderItemInfo, StyleSheet, Text } from 'react-native';
import {
  EventCell,
  EventCellProps,
  EventCellShimmer,
} from '@app/components/mol.cell';
import {
  Placeholder,
  PlaceholderSelector,
} from '@app/components/mol.placeholder';
import { CarePlanItemFragment } from '@app/data/graphql';
import { AppError } from '@app/model';
import { Asset, LoadingState } from '@atomic';
import { CarePlanEventsMapper } from './care-plan-events.mapper';
import { CarePlanEventsStrings } from './care-plan-events.strings';

export interface CarePlanEventsProps {
  loading: boolean;
  error: AppError;
  events: CarePlanItemFragment[];
  onEventTap?: (event: CarePlanItemFragment) => void;
}

const EmptyPlaceholder = () => (
  <Placeholder
    // image={Asset.Icon.Placeholder.Failure}
    title={CarePlanEventsStrings.Empty.Title}
    message={CarePlanEventsStrings.Empty.Message}
  />
);

export class CarePlanEvents extends React.PureComponent<CarePlanEventsProps> {
  render() {
    const items: CarePlanItemFragment[] = this.props.events;
    const loading: boolean = this.props.loading;

    return (
      <LoadingState
        loading={loading}
        data={!loading && !!items}
        error={!!this.props.error}>
        <LoadingState.Shimmer>
          <EventCellShimmer count={5} />
        </LoadingState.Shimmer>
        <LoadingState.ErrorPlaceholder>
          <PlaceholderSelector error={this.props.error} />
        </LoadingState.ErrorPlaceholder>
        {items && (
          <>
            <Text style={styles.title}>Eventos</Text>
            <FlatList
              ListEmptyComponent={EmptyPlaceholder}
              data={this.filterEvents(items)}
              extraData={this.props}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
            />
          </>
        )}
      </LoadingState>
    );
  }

  private filterEvents = (events: CarePlanItemFragment[]) => {
    console.warn(events);
    if (events) {
      return events.filter(it => it.procedure.type !== 'Appointment');
    }
  };

  private renderItem = (
    itemInfo: ListRenderItemInfo<CarePlanItemFragment>,
  ): React.ReactElement<EventCellProps> => {
    const item = itemInfo.item;
    const handleTap =
      this.props.onEventTap && (() => this.props.onEventTap(item));

    return (
      <EventCell
        {...CarePlanEventsMapper.map(item)}
        disabled={!this.props.onEventTap}
        onTap={handleTap}
      />
    );
  };

  private keyExtractor = (item: CarePlanItemFragment) => {
    return item.schedule.id;
  };
}

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    lineHeight: 18,
    fontSize: 16,
    marginLeft: 36,
    marginBottom: 10,
  },
});
