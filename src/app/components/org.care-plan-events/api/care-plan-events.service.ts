import { LocalDataSource } from '@app/data';
import { InputParams } from '@app/model/api/input.params.model';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { getCodesPath } from '@app/modules/home/api/care-plan-progress/care-plan-progress.service';
import {
  CarePlanEventsParams,
  CarePlanItemResponse,
} from '@app/modules/home/api/care-plan/care-plan.entity';
import { CarePlanMapper } from '@app/modules/home/api/care-plan/care-plan.mapper';
import {
  CarePlanItemModel,
  CarePlanItemsInputModel,
} from '@app/modules/home/api/care-plan/care-plan.model';
import Container from 'typedi';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchCarePlanEvents = async (input: any) => {
  const res = await fetchItens(input);
  if (res?.error) {
    console.error(res.error);
    return { data: null, error: res.error };
  }

  const events: CarePlanItemModel[] = res.data;

  const filterNotDone = (event: CarePlanItemModel) => !event.schedule.doneAt;
  const filterNotExpired = (event: CarePlanItemModel) =>
    new Date() < event.schedule.limitDate;

  const filterEvents = events.filter(filterNotDone).filter(filterNotExpired);
  return {
    data: filterEvents,
    error: false,
  };
};

export const fetchItens = async (
  input: InputParams<CarePlanItemsInputModel>,
) => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const params: CarePlanEventsParams =
    CarePlanMapper.mapItemParams(beneficiaryCode);

  const res = await itemsRequest(params);

  if (res?.error) {
    console.error(res.error);
    return { data: null, error: res.error };
  }

  const data: CarePlanItemResponse[] =
    res.data && res.data.planoCuidadoItemAgendamento;
  const itemsWithoutHabits: CarePlanItemResponse[] =
    data && data.filter(item => !item.habito);

  if (!itemsWithoutHabits || itemsWithoutHabits.length === 0) {
    return { data: [], error: false };
  }

  let items: CarePlanItemModel[] = itemsWithoutHabits.map(
    CarePlanMapper.mapItem,
  );

  const typeFilter = (item: CarePlanItemModel): boolean =>
    input.types.some(type => type === item.procedure.type);
  const generatorFilter = (item: CarePlanItemModel): boolean =>
    input.generator.some(gen => gen === item.generator);

  if (input.types && input.types.length) {
    items = items.filter(typeFilter);
  }

  if (input.generator && input.generator.length) {
    items = items.filter(generatorFilter);
  }

  return { data: items, error: false };
};

const itemsRequest = async (params: CarePlanEventsParams) => {
  const filters: string = `${params.from}/${params.until}/${params.eventyType}/${params.generatorType}`;
  const path: string = `${params.beneficiaryCode}/${filters}`;
  let res;
  try {
    res = await axiosGet({
      url: `/fusion-gestao-cuidado/api/plano-cuidado/agendamentos/${getCodesPath()}/${path}`,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    console.error(e);
    return { data: null, error: e };
  }

  return { data: res.data, error: false };
};
