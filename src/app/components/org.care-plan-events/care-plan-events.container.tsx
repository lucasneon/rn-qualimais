import { ApolloError } from 'apollo-client';
import _ from 'lodash';
import * as React from 'react';
import { Container } from 'typedi';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { CarePlanItemFragment } from '@app/data/graphql';
import { AppError } from '@app/model';
import {
  PastEventsProvider,
  PastEventsProviderKey,
} from '@app/modules/health/past-events.provider';
import { ErrorMapper } from '@app/utils';
import { fetchCarePlanEvents } from './api/care-plan-events.service';

export interface CarePlanEventsContainerProps {
  fetch: boolean;
  updateProgress: () => void;
  children: (res: CarePlanEventsContainerChildren) => React.ReactNode;
}

interface State {
  loading: boolean;
  error?: AppError;
  events?: CarePlanItemFragment[];
}

export interface CarePlanEventsContainerChildren extends State {
  onEventUpdate: () => void;
}

export class CarePlanEventsContainer extends React.Component<
  CarePlanEventsContainerProps,
  State
> {
  private readonly graphQLProvider: GraphQLProvider =
    Container.get(GraphQLProviderToken);
  private readonly pastEventsProvider: PastEventsProvider = Container.get(
    PastEventsProviderKey,
  );

  constructor(props) {
    super(props);
    this.state = { loading: null, error: null, events: null };
  }

  componentDidUpdate(prevProps: CarePlanEventsContainerProps) {
    if (!prevProps.fetch && this.props.fetch) {
      this.fetchEvents();
    }
  }

  render() {
    return this.props.children({
      loading: !this.props.fetch || this.state.loading,
      error: this.state.error,
      events: this.state.events,
      onEventUpdate: this.handleEventUpdate,
    });
  }

  private handleEventUpdate = () => {
    this.pastEventsProvider.rehydrate();
    this.fetchEvents();
    this.props.updateProgress();
  };

  private async fetchEvents() {
    this.setState({ loading: true });

    const input = { data: {} };

    const res = await fetchCarePlanEvents(input);
    if (res?.error) {
      console.error(res.error);
      return this.handleListError(res.error);
    }
    this.handleSuccess(res.data);
    //TODO GraphQL
    // this.graphQLProvider.query<CarePlanEventsQueryVariables>(
    //   'care-plan-events',
    //   { data: {} },
    //   (data: CarePlanEventsQuery) =>
    //     this.setState({
    //       loading: false,
    //       events: _.cloneDeep(data.CarePlanEvents),
    //     }),
    //   (error: ApolloError) =>
    //     this.setState({ loading: false, error: ErrorMapper.map(error) }),
    // );
  }

  private handleSuccess(data: any) {
    this.setState({
      loading: false,
      events: _.cloneDeep(data),
    });
  }

  private handleListError(error: ApolloError) {
    this.setState({ loading: false, error: ErrorMapper.map(error) });
  }
}
