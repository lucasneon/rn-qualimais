import * as React from 'react';
import { FlatList, ListRenderItemInfo } from 'react-native';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import {TicketListItemFragment } from '@app/data/graphql';
import { AppError } from '@app/model';
import { LoadingState } from '@atomic';
import { CellShimmer, TicketCellProps } from './voucher-Cell.component';
import { TicketStrings } from './voucher.strings';

export interface TicketsProps {
  loading: boolean;
  error: AppError;
  activeTickets: any[];
  onTicketTap: (ticket: TicketListItemFragment) => void;
}


export class Tickets extends React.PureComponent<TicketsProps> {

  render() {
    const { activeTickets, error, loading } = this.props;

    return (
      <LoadingState loading={loading} data={!!activeTickets} error={!!error}>
        <LoadingState.Shimmer>
          <TicketCellShimmer count={1} />
        </LoadingState.Shimmer>
        <LoadingState.ErrorPlaceholder>
          <PlaceholderSelector small={true} error={error} />
        </LoadingState.ErrorPlaceholder>
        {!loading && activeTickets && activeTickets.length > 0 && (
          <>
            <FlatList
              data={activeTickets}
              renderItem={this.renderItem}
              // keyExtractor={this.keyExtractor}
            />
          </>
        )}
      </LoadingState>
    );
  }

  private renderItem = (itemInfo: ListRenderItemInfo<any>): React.ReactElement<TicketCellProps> => {
    const item: any = itemInfo.item;
    const handleTap = () => this.props.onTicketTap(item);
    
    return (
      // <TicketCell
      //   title='CONSULTA MÉDICA'
      //   id={1}
      //   type={VoucherType.consulta}
      //   tag={TicketStrings.Tag.toUpperCase()}
      //   serieNumber='123456'
      //   validity='Válido até 31/12/2021'
      //   onTap={handleTap}
      //   hideCheck={true}
      //   />
    );
  }

  // private keyExtractor = (item: MedicationFragment) => item.id;

}
