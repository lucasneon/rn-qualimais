import styled from 'styled-components/native';
import { Border, Opacity, Shimmer, Spacing } from '@atomic';
import { GradientCell } from '@app/components/mol.cell/gradient-cell.component.style';

interface TicketCellProps {
  disabled?: boolean;
  inactive?: boolean;
}

export const TicketCellStyled2 = styled.View`
  margin-top: ${Spacing.Small};
  margin-left: ${Spacing.Large};
  margin-right: ${Spacing.Large};
`;

export const TicketCellStyled = styled.View`
  margin-top: 10;
  flex-direction: row;
  margin-left: 20;
  margin-right: 20;
  border-radius: 0;
`;

export const TicketCellCheckAreaStyled = styled.View`
  top: 22;
  position: absolute;
`;

export const TicketGradientCellStyled = styled(GradientCell)`
  padding-vertical: ${Spacing.Large};
  padding-horizontal: ${Spacing.Large};
  border-radius: ${Border.RadiusLarge};
  min-height: 80;
  opacity: ${ (props: TicketCellProps) => props.disabled ? Opacity.Disabled : 1};
`;

export const TicketCellShimmerStyled = styled(Shimmer)`
  border-radius: ${Border.RadiusLarge};
`;
