import * as React from 'react';
import {
  TouchableOpacity,
  ImageBackground,
  Text,
  View,
  StyleSheet,
} from 'react-native';
import { TicketCellStyled } from './voucher-cell.component.style';

import voucher from '../../../assets/images/voucher/bg-voucher-consultas.png';
import voucherExam from '../../../assets/images/voucher/bg-voucher-exame.png';
import indConsulta from '../../../assets/images/bkg_voucher_ind_consulta.png';
import indExame from '../../../assets/images/bkg_voucher_ind_exame.png';
import utConsulta from '../../../assets/images/voucher/bg-voucher-consultas-utilizado.png';
import utExame from '../../../assets/images/voucher/bg-voucher-exame-utilizado.png';
import { ShareReplayConfig } from 'rxjs/internal/operators/shareReplay';

export interface TicketCellProps
  extends Readonly<{ children?: React.ReactNode }> {
  nomeProcedimento?: string;
  nomePrestador?: string;
  voucher?: any;
  numero?: string;
  status: 'DISPONIVEL' | 'INDISPONIVEL' | 'UTILIZADO';
  nomeTipoServico: 'CONSULTA' | 'EXAME';
  especialidadeCBOS?: string;
  dataFimValidade?: string;
  dataInicioValidade?: string;
  dataUtilizacao?: string;
  tag: string;
  motivoStatus?: string;
  hideCheck?: boolean;
  onTap?: (id: string) => any;
  id: any;
  disabled?: boolean;
}

export const TicketCell: React.SFC<TicketCellProps> = (
  props: TicketCellProps,
) => {
  const handleTap = () => {
    if (props.onTap) {
      props.onTap(props.id);
    }
  };

  let voucherImage;
  let vencimento;
  var indisponivel = false;

  switch (props.nomeTipoServico.concat(props.status)) {
    case 'CONSULTADISPONIVEL':
      voucherImage = voucher;
      vencimento = 'Válido até ' + props.dataFimValidade;
      break;
    case 'EXAMEDISPONIVEL':
      voucherImage = voucherExam;
      vencimento = 'Válido até ' + props.dataFimValidade;
      break;
    case 'CONSULTAUTILIZADO':
      voucherImage = utConsulta;
      vencimento = 'Utilizado em ' + props.voucher.dataUtilizacao;
      break;
    case 'EXAMEUTILIZADO':
      voucherImage = utExame;

      vencimento = 'Utilizado em ' + props.voucher.dataUtilizacao;
      break;
    case 'CONSULTAINDISPONIVEL':
      voucherImage = indConsulta;
      indisponivel = true;
      vencimento = 'Válido a partir de ' + props.dataInicioValidade + '*';
      break;
    case 'EXAMEINDISPONIVEL':
      voucherImage = indExame;
      indisponivel = true;
      vencimento = 'Válido a partir de ' + props.dataInicioValidade + '*';
      break;
    default:
      voucherImage = voucherExam;
      indisponivel = false;
      vencimento = 'Válido até ' + props.dataFimValidade;
      break;
  }

  return (
    <TouchableOpacity onPress={handleTap} disabled={props.disabled}>
      <TicketCellStyled>
        <ImageBackground
          resizeMode="stretch"
          source={voucherImage}
          style={styles.imageBackground}>
          <View style={{ paddingHorizontal: 20, paddingRight: 50 }}>
            <View style={{ maxHeight: 40, minHeight: 40 }}>
              <Text style={{ color: '#fff', fontSize: 10 }}>{props.tag}</Text>
              <Text
                adjustsFontSizeToFit
                style={{ color: '#fff', fontSize: 18, paddingRight: 30 }}>
                {props.nomeProcedimento}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingTop: !indisponivel ? 50 : 30,
              }}>
              {!indisponivel ? (
                <View>
                  <Text style={{ color: '#fff', fontSize: 10 }}>NÚMERO</Text>
                  <Text style={{ color: '#fff', fontSize: 12 }}>
                    {props.numero}
                  </Text>
                </View>
              ) : (
                <View>
                  <Text
                    style={{ color: '#fff', fontSize: 12, paddingBottom: 10 }}>
                    {vencimento}
                  </Text>
                  <Text style={{ color: '#fff', fontSize: 12 }}>
                    {'*' + props.motivoStatus}
                  </Text>
                </View>
              )}
              {!indisponivel ? (
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 13,
                    alignContent: 'center',
                  }}>
                  {vencimento}
                </Text>
              ) : (
                <View></View>
              )}
            </View>
          </View>
        </ImageBackground>
      </TicketCellStyled>
    </TouchableOpacity>
  );
};

export enum VoucherType {
  consulta,
  exame,
  usadoConsulta,
  usadoExame,
  indispConsulta,
  indispExame,
}

const styles = StyleSheet.create({
  imageBackground: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover',
    justifyContent: 'space-between',
    paddingVertical: 16,
  },
});
