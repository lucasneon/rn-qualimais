import * as React from 'react';
import { Body, Caption, H4, HBox, LinkButton, LinkButtonProps } from '@atomic';

export interface ListInfoProps {
  title: string;
  side?: string;
  descriptions?: string[];
  links?: LinkButtonProps[];
  date?: string;
}

export const ListInfo: React.SFC<ListInfoProps> = props => (
  <>
    {props.side && <Caption inList={false}>{props.side}</Caption>}
    <HBox wrap={false} vAlign="center" hAlign="flex-start" >
      <HBox.Item grow={3}>
        <H4 adjustsFontSizeToFit>{props.title}</H4>
      </HBox.Item>
      <HBox.Item grow={1} />
    </HBox>
    {props.descriptions && props.descriptions.map(desc => desc && <Body key={desc}>{desc}</Body>)}
    {props.date && <Body>{props.date}</Body>}
    {props.links &&
      props.links.length > 0 &&
      props.links.map(link => (
        <LinkButton align="flex-start" key={link.text} {...link} />
      ))}
  </>
);
