import * as React from 'react';
import { FlatList, ListRenderItemInfo } from 'react-native';
import { ListInfoCell, ListInfoCellProps } from '@app/components/mol.cell';

export interface CarePointListItem extends ListInfoCellProps {
  itemKey: string;
}

export interface CarePointListProps {
  items: CarePointListItem[];
}

export class CarePointList extends React.PureComponent<CarePointListProps> {

  render() {
    return (
      <FlatList
        keyExtractor={this.keyExtractor}
        data={this.props.items}
        renderItem={this.renderCells}
        extraData={this.state}
      />
    );
  }

  private keyExtractor = (item: CarePointListItem) => item.itemKey;

  private renderCells = (itemInfo: ListRenderItemInfo<CarePointListItem>) => {
    return <ListInfoCell {...itemInfo.item} />;
  }
}
