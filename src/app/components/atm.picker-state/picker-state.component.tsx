import * as React from 'react';
import { PickerField } from '@atomic';

interface PickerStateFieldProps {
  disabled?: boolean;
  onValueChange?: (value: any) => void;
  onBlur?: () => void;
  placeholder?: string;
}

const StateItems =  [
  {value: 'AC', label: 'AC'},
  {value: 'AL', label: 'AL'},
  {value: 'AM', label: 'AM'},
  {value: 'AP', label: 'AP'},
  {value: 'BA', label: 'BA'},
  {value: 'CE', label: 'CE'},
  {value: 'DF', label: 'DF'},
  {value: 'ES', label: 'ES'},
  {value: 'GO', label: 'GO'},
  {value: 'MA', label: 'MA'},
  {value: 'MG', label: 'MG'},
  {value: 'MS', label: 'MS'},
  {value: 'MT', label: 'MT'},
  {value: 'PA', label: 'PA'},
  {value: 'PB', label: 'PB'},
  {value: 'PE', label: 'PE'},
  {value: 'PI', label: 'PI'},
  {value: 'PR', label: 'PR'},
  {value: 'RJ', label: 'RJ'},
  {value: 'RN', label: 'RN'},
  {value: 'RO', label: 'RO'},
  {value: 'RR', label: 'RR'},
  {value: 'RS', label: 'RS'},
  {value: 'SC', label: 'SC'},
  {value: 'SE', label: 'SE'},
  {value: 'SP', label: 'SP'},
  {value: 'TO', label: 'TO'},
];

export class PickerStateField extends React.Component<PickerStateFieldProps, undefined> {

  render() {

    return (
      <PickerField
        items={StateItems}
        disabled={this.props.disabled}
        onValueChange={this.props.onValueChange}
        onBlur={this.props.onBlur}
        placeholder={this.props.placeholder}
      />
    );
  }
}
