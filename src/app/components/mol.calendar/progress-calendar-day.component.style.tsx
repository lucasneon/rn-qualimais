import { Animated } from 'react-native';
import styled from 'styled-components/native';
import { Color, FontFamily, FontSize, Opacity, Spacing } from '@atomic';
import { SpinnerStyled } from '@atomic/atm.loader/circle-loader.component';

const DayComponentSize = 36;

// used to avoid pushing the selected day component up
const SelectionBarSize = 4;

export const DayComponentView = styled.View`
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  width: ${DayComponentSize};
  padding-bottom: ${2 * SelectionBarSize};
  height: ${DayComponentSize + 2 * SelectionBarSize};
  justify-content: center;
  align-items: center;
  opacity: ${props => props.disabled ? Opacity.Disabled : 1};
`;

export const DayComponentBackgroundWrapperStyled = styled.View`
  background-color: ${props => props.alert ? Color.Alert : Color.GrayLight};
  width: ${DayComponentSize};
  height: ${DayComponentSize};
  border-radius: ${DayComponentSize / 2};
`;

export const DayComponentBackgroundClipStyled = styled.View`
  width: ${DayComponentSize};
  height: ${DayComponentSize};
  border-radius: ${DayComponentSize / 2};
  border-color: ${Color.Success};
  overflow: hidden;
  justify-content: flex-end;
`;

export const DayComponentBackgroundStyled = styled(Animated.View)`
  position: absolute;
  width: ${DayComponentSize};
  background-color: ${Color.Success};
  opacity: 0.7;
`;

export const DayTextStyled = styled.Text`
  color: ${props => props.alert ? Color.White : props.disabled ? Color.Gray :
    props.progress > 0.5 ? Color.White : Color.GrayDark};
  font-weight: ${props => props.bold ? 'bold' : 'normal'};
  font-size: ${props => props.disabled ? FontSize.XSmall : FontSize.Large};
  font-family: ${FontFamily.Primary.Regular};
  line-height: ${Spacing.XLarge};
`;

export const SelectionBar = styled.View`
  position: absolute;
  bottom: 0;
  width: ${DayComponentSize};
  height: ${SelectionBarSize};
  background-color: ${props => props.alert ? Color.Alert : Color.Success};
  border-radius: 2;
`;

export const DayComponentContentViewStyled = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  width: ${DayComponentSize};
  height: ${DayComponentSize};
  justify-content: center;
  align-items: center;
`;

export const CalendarDaySpinner: any = styled(SpinnerStyled).attrs({
  size: 16,
})`
  color: ${Color.Gray};
`;
