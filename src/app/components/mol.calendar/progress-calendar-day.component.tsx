import * as React from 'react';
import { Animated, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { DateObject } from 'react-native-calendars';
import {
  BigCheckIconStyled,
  BigCheckStyled,
} from '@app/components/atm.big-check/big-check.component.style';
import { Asset, Shimmer } from '@atomic';
import {
  CalendarDaySpinner,
  DayComponentBackgroundClipStyled,
  DayComponentBackgroundStyled,
  DayComponentBackgroundWrapperStyled,
  DayComponentView,
  DayTextStyled,
  SelectionBar,
} from './progress-calendar-day.component.style';

export const ProgressCalendarShimmer: React.SFC = () => (
  <Shimmer rounded={true} height={Dimensions.get('window').height / 2} />
);

export interface CalendarDayProps {
  progress?: number;
  text: string | number;
  selected?: boolean;
  disabled?: boolean;
  marked?: boolean;
  alert?: boolean;
  bold?: boolean;
  date?: DateObject;
  onPress?: (date: DateObject) => void;
  onLongPress?: (date: DateObject) => void;
  loading?: boolean;
}

interface CalendarDayState {
  animatedHeight: Animated.Value;
}

export class ProgressCalendarDay extends React.Component<
  CalendarDayProps,
  CalendarDayState
> {
  static defaultProps = {
    allowUncheck: true,
    progress: 0,
  };

  constructor(props) {
    super(props);

    this.state = {
      animatedHeight: new Animated.Value(props.progress ? 1 : 0),
    };
  }

  componentDidUpdate(prevProps: CalendarDayProps) {
    if (prevProps.progress !== this.props.progress) {
      Animated.timing(this.state.animatedHeight, {
        toValue: this.props.progress,
        duration: 300,
      }).start();
    }
  }

  render() {
    const height = this.state.animatedHeight.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 36],
    });

    const { bold, progress, text, marked, selected, disabled, alert } =
      this.props;

    const onPress = () => {
      this.props.onPress(this.props.date);
      this.props.onLongPress(this.props.date);
    };

    // console.warn('Props Calendar > ', this.props);

    return (
      <>
        {this.props.loading ? (
          <CalendarDaySpinner />
        ) : (
          <TouchableWithoutFeedback onPress={onPress}>
            <DayComponentView disabled={disabled}>
              {marked ? (
                <DayComponentBackgroundWrapperStyled alert={alert}>
                  <DayComponentBackgroundClipStyled>
                    <DayComponentBackgroundStyled style={{ height }} />
                  </DayComponentBackgroundClipStyled>
                  <BigCheckStyled>
                    {this.props.progress === 1 ? (
                      <BigCheckIconStyled
                        source={Asset.Icon.Atomic.Check}
                        progress={this.props.progress}
                      />
                    ) : (
                      <DayTextStyled
                        alert={this.props.alert}
                        disabled={disabled}
                        bold={bold}
                        progress={progress}>
                        {text}
                      </DayTextStyled>
                    )}
                  </BigCheckStyled>
                </DayComponentBackgroundWrapperStyled>
              ) : (
                <DayTextStyled
                  alert={this.props.alert}
                  bold={bold}
                  disabled={disabled}>
                  {this.props.text}
                </DayTextStyled>
              )}
              {selected && <SelectionBar alert={alert} />}
            </DayComponentView>
          </TouchableWithoutFeedback>
        )}
      </>
    );
  }
}
