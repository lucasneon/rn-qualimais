import _ from 'lodash';
import moment from 'moment';
import * as React from 'react';
import {
  Calendar,
  DayComponentProps,
  LocaleConfig,
} from 'react-native-calendars';
import { CalendarString } from '@atomic/mol.calendar/calendar.strings';
import { ProgressCalendarDay } from './progress-calendar-day.component';

const locale = 'br';

LocaleConfig.locales[locale] = {
  monthNames: CalendarString.Months,
  monthNamesShort: CalendarString.Months_Short,
  dayNamesShort: CalendarString.Weekdays,
};

LocaleConfig.defaultLocale = locale;

interface CalendarProps {
  markedDates: CustomCalendarMarking;
  loading?: boolean;
  selected?: string;
  onTap: (dateString: string) => void;
}

export interface CustomCalendarMarking {
  [dateString: string]: { progress: number; alert?: boolean };
}

export class ProgressCalendar extends React.Component<CalendarProps> {
  render() {
    const { loading, markedDates } = this.props;

    return (
      <Calendar
        displayLoadingIndicator={loading}
        markedDates={{ ...markedDates }}
        onDayPress={this.props.onTap}
        dayComponent={this.renderDayComponent}
      />
    );
  }

  private renderDayComponent = (props: DayComponentProps) => {
    const { date, state, marking } = props;

    // console.warn('Props 1 > ', props);

    const selected = this.props.selected === date.dateString;
    const loading = selected && this.props.loading;
    const disabled = state === 'disabled';
    const marked = !_.isEmpty(marking);
    const today = moment().isSame(moment(date.dateString), 'day');
    const progress = marked && marking.progress;
    const alert = marked && marking.alert;

    return (
      <ProgressCalendarDay
        selected={selected}
        loading={loading}
        disabled={disabled}
        text={date.day}
        marked={marked}
        progress={progress}
        onPress={props.onPress}
        onLongPress={props.onLongPress}
        date={date}
        bold={today}
        alert={alert}
      />
    );
  };
}
