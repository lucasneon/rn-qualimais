import * as React from 'react';
import { Navigation } from '@app/core/navigation';
import { EmergencyPage } from '@app/modules/emergency';
import { Asset } from '@atomic';
import {
  ButtonContentStyled, ButtonStyled, ButtonTouchableOpacityStyled
} from '@atomic/atm.button/button.component.style';
import {
  EmergencyButtonTextStyled, EmergencyButtonThumbStyled, EmergencyButtonWrapperStyled
} from './emergency-button.component.style';
import { EmergencyButtonStrings } from './emergency-button.strings';

export class EmergencyButton extends React.PureComponent {
  static options = { name: 'EmergencyButton' };

  render() {
    return (
      <ButtonStyled>
        <ButtonTouchableOpacityStyled onPress={this.handleTap}>
          <EmergencyButtonWrapperStyled>
            <ButtonContentStyled {...this.props}>
              <EmergencyButtonThumbStyled source={Asset.Icon.Custom.Phone} />
              <EmergencyButtonTextStyled >{EmergencyButtonStrings.Text}</EmergencyButtonTextStyled>
            </ButtonContentStyled>
          </EmergencyButtonWrapperStyled>
        </ButtonTouchableOpacityStyled>
      </ButtonStyled >
    );
  }

  private handleTap = () => {
    Navigation.showModal(EmergencyPage);
  }
}
