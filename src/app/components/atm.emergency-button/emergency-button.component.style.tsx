import styled from 'styled-components/native';
import { ButtonTextStyled, ButtonThumbStyled, ButtonWrapperStyled } from '@atomic/atm.button/button.component.style';
import { Color, Spacing } from '@atomic/obj.constants';
import { Platform } from '@atomic/obj.platform';

export const BTN_HEIGHT = 48;
export const BTN_SHORT_HEIGHT = 30;
export const BTN_ICON_SIZE = Spacing.Large;

const EmergencyButtonContentColor = Color.Accessory;

export const EmergencyButtonThumbStyled = styled(ButtonThumbStyled)`
  tintColor: ${EmergencyButtonContentColor};
`;

export const EmergencyButtonWrapperStyled = styled(ButtonWrapperStyled).attrs({
  short: true,
})`
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  ${Platform.OS === 'ios' ? `
    margin-right: -${Spacing.Large};
    shadow-color: ${Color.Black};
    shadow-opacity: 0.2;
    shadow-radius: 3;
    shadow-offset: 0 2px;
  ` : `
    elevation: 5;
    border: ${Color.GrayLight};
    border-width: 2px;
    margin-left: 20px;
    margin-top: 20px;
    margin-bottom: 20px;
  `
  }
`;

export const EmergencyButtonTextStyled = styled(ButtonTextStyled)`
  color: ${EmergencyButtonContentColor};
`;
