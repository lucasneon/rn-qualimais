import * as React from 'react';
import { storiesOf } from '@storybook/react-native';
import { SegmentedControl } from './segmented-control.component';

storiesOf('Molecules', module).add('Segmented Control', () => {
  const firstItem = { text: 'First item', onTap: () => console.warn('1st Item selected!') };
  const secondItem = { text: 'Second item', onTap: () => console.warn('2nd Item selected') };

  return (
    <SegmentedControl left={firstItem} right={secondItem} />
  );
});
