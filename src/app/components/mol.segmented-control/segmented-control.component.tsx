import * as React from 'react';
import { HBox, VSeparator } from '@atomic';
import { LeftSegment, RightSegment, SegmentedControlBackground } from './segmented-control.style';

export interface SegmentedControlItemProps {
  text: string;
  onTap: () => void;
}

export interface SegmentedControlProps {
  left: SegmentedControlItemProps;
  right: SegmentedControlItemProps;
}

type SegmentSelection = 'left' | 'right';

interface SegmentedControlState {
  selected: SegmentSelection;
}

export class SegmentedControl extends React.PureComponent<SegmentedControlProps, SegmentedControlState> {
  constructor(props) {
    super(props);
    this.state = { selected: 'left' };
  }

  render() {
    const left = this.props.left;
    const right = this.props.right;

    return (
      <SegmentedControlBackground>
        <HBox hAlign='center'>
          <HBox.Item hAlign='flex-end' key={left.text}>
            <LeftSegment
              selected={this.state.selected !== 'left'}
              text={left.text}
              onTap={this.handleItemTap(left, 'left')}
            />
          </HBox.Item>
          <HBox.Item hAlign='flex-start' key={right.text}>
            <RightSegment
              selected={this.state.selected !== 'right'}
              text={right.text}
              onTap={this.handleItemTap(right, 'right')}
            />
          </HBox.Item>
        </HBox>
        <VSeparator />
      </SegmentedControlBackground>
    );
  }

  private handleItemTap = (itemProps: SegmentedControlItemProps, index: SegmentSelection) => {
    return () => {
      itemProps.onTap();
      this.setState({ selected: index });
    };
  }
}
