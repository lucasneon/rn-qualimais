import { View } from 'react-native';
import styled from 'styled-components/native';
import { Color } from '@atomic';
import { Button } from '@atomic/atm.button/button.component';

interface SegmentProps {
  selected: boolean;
}

const SegmentAttributes = {
  short: true,
  color: 'success',
};

export const LeftSegment = styled(Button).attrs((props: SegmentProps) => ({
  outlined: props.selected,
  ...SegmentAttributes,
}))`
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
`;

export const RightSegment = styled(Button).attrs((props: SegmentProps) => ({
  outlined: props.selected,
  ...SegmentAttributes,
}))`
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
`;

export const SegmentedControlBackground = styled(View)`
  background-color: ${Color.White};
`;
