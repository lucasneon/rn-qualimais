import * as React from 'react';
import { Watermark, WatermarkType } from '@app/components/atm.watermark';
import {
  BodyGradient,
  CaptionGradient,
  HDisplayGradient,
} from '@app/components/mol.cell/gradient-cell.component.style';
import { HBox, VBox, VSeparator } from '@atomic';
import {
  MeasureCellStyled,
  MeasureGradientCellStyled,
  MeasureTouchableCellStyled,
} from './measure-cell.component.style';

export interface MeasureCellProps
  extends Readonly<{ children?: React.ReactNode }> {
  label: string;
  value: string;
  unit?: string;
  type: WatermarkType;
  caption: string;
  onTap?: () => any;
  style?: any;
  disabled?: boolean;
  index?: number;
  total?: number;
}

export const MeasureCell: React.SFC<MeasureCellProps> = (
  props: MeasureCellProps,
) => {
  const handleTap = () => {
    return props.onTap && props.onTap();
  };

  return (
    <MeasureCellStyled style={props.style}>
      <MeasureTouchableCellStyled disabled={props.disabled} onPress={handleTap}>
        <MeasureGradientCellStyled
          index={props.index}
          total={props.total}
          firstColor="#f5836d"
          secondColor="#ffa18f">
          <VBox noGutter={true}>
            <BodyGradient>{props.label}</BodyGradient>
            <HBox>
              <HBox.Item wrap={true}>
                <HDisplayGradient>{props.value}</HDisplayGradient>
              </HBox.Item>
              <HBox.Item wrap={true} vAlign="center">
                <BodyGradient>{props.unit}</BodyGradient>
              </HBox.Item>
            </HBox>
          </VBox>
          <VSeparator />
          <VBox noGutter={true} hAlign="flex-start" vAlign="flex-end">
            <CaptionGradient>{props.caption}</CaptionGradient>
          </VBox>
        </MeasureGradientCellStyled>
      </MeasureTouchableCellStyled>
    </MeasureCellStyled>
  );
};
MeasureCell.defaultProps = {
  index: 0,
  total: 1,
};
