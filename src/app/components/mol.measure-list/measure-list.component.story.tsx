import * as React from 'react';

import { MeasureList } from './measure-list.component';
import { storiesOf } from '@storybook/react-native';
import { WatermarkType } from '@app/components/atm.watermark';

const stories = storiesOf('Molecules', module);

stories.add('Measure List', () => (
  <MeasureList>
    <MeasureList.Item
      label='Peso'
      value='12'
      unit='kg'
      type={WatermarkType.Consultation}
      caption='há 3 meses'
    />
    <MeasureList.Item
      label='Peso'
      value='12'
      unit='kg'
      type={WatermarkType.Consultation}
      caption='há 3 meses'
    />
    <MeasureList.Item
      label='Peso'
      value='12'
      unit='kg'
      type={WatermarkType.Consultation}
      caption='há 3 meses'
    />
    <MeasureList.Item
      label='Peso'
      value='12'
      unit='kg'
      type={WatermarkType.Consultation}
      caption='há 3 meses'
    />
  </MeasureList>
));
