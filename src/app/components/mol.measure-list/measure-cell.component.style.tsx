import styled from 'styled-components/native';
import { GradientCell } from '@app/components/mol.cell/gradient-cell.component.style';
import { Border, Opacity, Spacing } from '@atomic';

export const MeasureCellStyled = styled.View`
  margin-right: ${Spacing.Small};
`;

export const MeasureTouchableCellStyled = styled.TouchableOpacity.attrs({
  activeOpacity: Opacity.Active,
})`
  min-width: 150;
`;

export const MeasureGradientCellStyled = styled(GradientCell)`
  padding-vertical: ${Spacing.Large};
  padding-horizontal: ${Spacing.Large};
  border-radius: ${Border.RadiusLarge};
  padding-left: ${Spacing.Large};
`;
