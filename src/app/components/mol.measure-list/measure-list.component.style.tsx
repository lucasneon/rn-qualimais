import styled from 'styled-components/native';
import { Scroll } from '@atomic';

export const MeasureListStyled = styled(Scroll).attrs({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
})``;
