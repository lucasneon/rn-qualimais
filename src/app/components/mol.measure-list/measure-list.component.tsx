import * as React from 'react';
import { MeasureCell } from './measure-cell.component';
import { MeasureListStyled } from './measure-list.component.style';

export class MeasureList extends React.Component<any, any> {
  static Item = MeasureCell;

  render() {
    const count = React.Children.count(this.props.children);
    const children = React.Children.map(
      this.props.children,
      (child: any, i: number) => {
        const props = {
          index: i,
          total: count,
          style: undefined,
        };

        if (i === 0) {
          props.style = { marginLeft: 16 };
        } else if (i === count - 1) {
          props.style = { marginRight: 16 };
        }

        return React.cloneElement(child, props);
      },
    );

    return <MeasureListStyled>{children}</MeasureListStyled>;
  }
}
