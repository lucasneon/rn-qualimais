import * as React from 'react';
import { Marker } from 'react-native-maps';
import { ListInfo } from '@app/components/mol.list-info';
import { CarePointListItem } from '@app/components/org.care-point-list';
import { GeoCoordinates } from '@app/model';
import { CalloutStyled, MarkerStyled } from './care-point-map.style';

export interface CarePointMarkerProps extends CarePointListItem {
  coords: GeoCoordinates;
  show?: boolean;
}

export class CarePointMarker extends React.PureComponent<CarePointMarkerProps> {
  private marker: Marker;

  componentDidMount() {
    if (this.props.show) {
      // Workaround to iOS: https://github.com/react-native-community/react-native-maps/issues/1872
      setTimeout(() => this.marker && this.marker.showCallout(), 200);
    }
  }

  render() {
    return (
      <MarkerStyled ref={ref => { this.marker = ref; }} coordinate={this.props.coords}>
        <CalloutStyled onPress={this.props.onTap}>
          <ListInfo
            title={this.props.title}
            side={this.props.side}
            descriptions={this.props.descriptions}
            links={this.props.links}
          />
        </CalloutStyled>
      </MarkerStyled>
    );
  }
}
