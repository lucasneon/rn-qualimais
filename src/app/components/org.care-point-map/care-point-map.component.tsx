import * as React from 'react';
import { GeoCoordinates } from '@app/model';
import { DefaultCenter } from '@app/services';
import { DistanceCalculator } from '@app/utils';
import { Shimmer } from '@atomic';
import { MapHeight, MapViewStyled } from './care-point-map.style';
import { CarePointMarker, CarePointMarkerProps } from './care-point-marker.component';

export interface CarePointMapProps {
  userCoord: GeoCoordinates;
  markers?: CarePointMarkerProps[];
  centerUser?: boolean;
}

const MaxInitialRegionDelta = 0.16; // degrees (~15 km)
const AverageMarkersInsideInitialRegion = 8;

export const CarePointMapShimmer: React.SFC = () => <Shimmer height={MapHeight} />;

export class CarePointMap extends React.PureComponent<CarePointMapProps> {

  render() {
    const hasMarkers: boolean = this.props.markers && this.props.markers.length > 0;
    const center: GeoCoordinates = this.props.centerUser ? (this.props.userCoord || DefaultCenter) :
      hasMarkers ? this.props.markers[0].coords : DefaultCenter;

    const initialRegionDelta: number = this.calculateInitialRegion();

    const initialShownRegion = {
      ...center,
      latitudeDelta: initialRegionDelta,
      longitudeDelta: initialRegionDelta,
    };

    return (
      <MapViewStyled initialRegion={initialShownRegion} showsUserLocation={true}>
        {hasMarkers && this.props.markers.map(this.renderMarker)}
      </MapViewStyled >
    );
  }

  private calculateInitialRegion(): number {
    const hasMarkers = this.props.markers && this.props.markers.length > 0;

    if (hasMarkers && this.props.centerUser) {
      const lastMarkerIndex = Math.min(this.props.markers.length - 1, AverageMarkersInsideInitialRegion);
      const lastMarker = this.props.markers[lastMarkerIndex];

      const distance = DistanceCalculator.degrees(this.props.userCoord || DefaultCenter, lastMarker.coords);
      return Math.min(MaxInitialRegionDelta, 2 * distance);
    } else if (hasMarkers && !this.props.centerUser && this.props.userCoord) {
      const distance = DistanceCalculator.degrees(this.props.userCoord, this.props.markers[0].coords);
      return Math.min(MaxInitialRegionDelta, 2 * distance);
    } else {
      return MaxInitialRegionDelta;
    }

  }

  private renderMarker = (marker: CarePointMarkerProps): React.ReactNode => {
    return <CarePointMarker key={marker.itemKey} coords={marker.coords} {...marker} />;
  }

}
