import { Dimensions } from 'react-native';
import MapView, { Callout, Marker } from 'react-native-maps';
import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource';
import styled from 'styled-components/native';
import { Asset, Color, Spacing } from '@atomic';
import { TotalButtonShortHeight } from '@atomic/atm.button/button.component.style';
import { LargeHeaderHeight } from '../atm.header/large-header.component.style';

// tslint:disable-next-line:max-line-length
// https://github.com/wix/react-native-navigation/blob/b0c8113243de2043b98442dd485c04b0422aa213/lib/android/app/src/main/java/com/reactnativenavigation/react/Constants.java
const BottomTabsHeight = 56; // from the react native navigation source code

const UsedSpace = BottomTabsHeight + LargeHeaderHeight + TotalButtonShortHeight + Spacing.Large;

const CalloutWidth = 0.8 * Dimensions.get('window').width;

export const MapHeight = Dimensions.get('window').height - UsedSpace;

const PinImage = Asset.Image.Pin;

const getCenterOffset = () => {
  return { x: 0, y: -0.5 * resolveAssetSource(PinImage).height };
};

export const CalloutStyled = styled(Callout)`
  width: ${CalloutWidth};
`;

export const MapViewStyled = styled(MapView)`
  height: ${MapHeight};
`;

export const MarkerStyled = styled(Marker).attrs({
  centerOffset: getCenterOffset(),
  image: PinImage,
  pinColor: Color.Primary,
})``;
