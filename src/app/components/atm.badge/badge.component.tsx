import * as React from 'react';
import { Shimmer } from '@atomic';
import { BADGE_HEIGHT, BadgeStyled, BadgeTextStyled } from './badge.component.style';

export interface BadgeProps {
  type: 'primary' | 'secondary' | 'neutral' | 'success';
  text: string;
  id?: string;
  height?: number;
  width?: number;
  outlined?: boolean;
}

export const BadgeShimmer: React.SFC<{ width?: number }> = props =>
  <Shimmer rounded={true} height={BADGE_HEIGHT} width={props.width || BADGE_HEIGHT * 2} />;

export const Badge = (props: BadgeProps) => {
  return (
    <BadgeStyled {...props}>
      <BadgeTextStyled {...props}>{props.text}</BadgeTextStyled>
    </BadgeStyled>
  );
};
