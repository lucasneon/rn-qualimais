import styled from 'styled-components/native';
import { Border, Color, FontFamily, FontSize, Spacing } from '@atomic/obj.constants';
import { BadgeProps } from './badge.component';

export const BADGE_HEIGHT = 32;

const badgeColors = {
  primary: Color.BlueDark,
  secondary: Color.Secondary,
  success: Color.Success,
  neutral: Color.GrayXLight,
};

export const BadgeStyled = styled.View`
  flex-direction: row;
  padding-horizontal: ${Spacing.Medium};
  border-radius: ${Border.RadiusLarge};
  ${(props: BadgeProps) => props.outlined ? `border-width: ${Border.Width};
  border-color: ${badgeColors[props.type] || Color.Primary};` : ''}
  background-color: ${(props: BadgeProps) => props.outlined ?
    Color.White : props.type ? badgeColors[props.type] : Color.Primary};
  align-self: flex-start;
  align-items: center;
  justify-content: center;
  margin-bottom: ${Spacing.XSmall};
  margin-right: ${Spacing.XSmall};
  height: ${(props: BadgeProps) => props.height ? props.height : BADGE_HEIGHT};
  flex-grow: 0;
  flex-shrink: 0;
  flex-basis: ${(props: BadgeProps) => props.width ? props.width : BADGE_HEIGHT * 2};
`;

export const BadgeTextStyled = styled.Text`
  font-size: ${FontSize.Small};
  ${(props: BadgeProps) => props.outlined ? `font-family: ${FontFamily.Primary.Medium}` : ''}
  color: ${(props: BadgeProps) => props.outlined && props.type ? badgeColors[props.type] :
    (props.type && props.type === 'neutral') ? Color.GrayXDark : Color.White};
`;

export const BadgeCloseStyled = styled.Image`
  height: ${ (props: BadgeProps) => props.height ? props.height / 2 : BADGE_HEIGHT / 2};
  width: ${(props: BadgeProps) => props.height ? props.height / 2 : BADGE_HEIGHT / 2};
  tint-color: ${(props: BadgeProps) =>
    (props.type && props.type === 'neutral') ? Color.GrayXDark : Color.White};
`;

export const BadgeCloseButtonStyled = styled.TouchableOpacity`
  justify-content: center;
  align-items: flex-end;
  width: ${(props: BadgeProps) => props.height ? props.height * 0.75 : BADGE_HEIGHT * 0.75};
  height: ${(props: BadgeProps) => props.height ? props.height : BADGE_HEIGHT};
`;
