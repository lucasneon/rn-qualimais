import * as React from 'react';
import { FlatList, ListRenderItemInfo } from 'react-native';
import { ListInfoCell, ListInfoCellProps } from '@app/components/mol.cell';

export interface AttendanceItem extends ListInfoCellProps {
  itemKey: string;  
}

export interface AttendanceListProps {
  items: AttendanceItem[];
}

export class AttendanceList extends React.PureComponent<AttendanceListProps> {

  render() {
    return (
      <FlatList
        keyExtractor={this.keyExtractor}
        data={this.props.items}
        renderItem={this.renderCells}
      />
    );
  }

  private keyExtractor = (item: AttendanceItem) => item.itemKey;

  private renderCells = (itemInfo: ListRenderItemInfo<AttendanceItem>) => {
    return <ListInfoCell {...itemInfo.item} />;
  }
}
