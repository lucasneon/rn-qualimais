import * as React from 'react';
import { Container } from 'typedi';
import { ExternalNavigator } from '@app/core/navigation';
import { Asset, PrimaryButton, VBox } from '@atomic';
import { Placeholder } from './placeholder.component';

const Strings = {
  Title: denied => denied ? 'Permissão não concedida' : 'Problema na localização',
  Description: denied => denied ?
    'Precisamos que você habilite a localização do seu aparelho nas configurações do aplicativo.' :
    'Tivemos um problema para detectar sua localização. Por favor, verifique seu sinal de GPS.',
  ActionLabel: 'Habilitar localização',
};

export interface LocationPlaceholderProps {
  permissionDenied: boolean;
}

export class LocationPlaceholder extends React.PureComponent<LocationPlaceholderProps> {
  private readonly externalNavigator: ExternalNavigator = Container.get(ExternalNavigator);

  render() {
    return (
      <VBox>
        <Placeholder
          image={Asset.Icon.Placeholder.Failure}
          title={Strings.Title(this.props.permissionDenied)}
          message={Strings.Description(this.props.permissionDenied)}
        />
        {this.props.permissionDenied &&
          <PrimaryButton onTap={this.handleButtonTap} text={Strings.ActionLabel} expanded={true} />
        }
      </VBox>
    );
  }

  private handleButtonTap = () => {
    this.externalNavigator.openAppSettings();
  }
}
