import * as React from 'react';
import { Asset, PrimaryButton, VBox } from '@atomic';
import { Placeholder } from './placeholder.component';

export interface NoSchedulePlaceholderProps {
  title: string;
  message: string;
  buttonText: string;
  onButtonTap: () => void;
}

export class NoSchedulePlaceholder extends React.PureComponent<NoSchedulePlaceholderProps> {
  render() {
    return (
      <VBox>
        <Placeholder
          image={Asset.Onboard.OnboardSearch}
          title={this.props.title}
          message={this.props.message}
        />
        <PrimaryButton onTap={this.props.onButtonTap} text={this.props.buttonText} expanded={true} />
      </VBox>
    );
  }
}
