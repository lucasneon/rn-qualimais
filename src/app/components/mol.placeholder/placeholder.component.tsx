import * as React from 'react';
import { Image } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Body, H2, VBox, VSeparator } from '@atomic';
import { PlaceholderContentBlockStyled, PlaceholderRoundedStyled } from './placeholder.component.style';

export interface PlaceholderProps {
  image?: any;
  title?: string;
  message?: string;
  children?: any;
  small?: boolean;
}

export const Placeholder: React.SFC<PlaceholderProps> = (props: PlaceholderProps) => {

  const img = () => {
    if (props.image != null) {
      return <Animatable.View animation='bounceIn' useNativeDriver={true}>
        <PlaceholderRoundedStyled small={props.small}>
          <Image source={props.image} />
        </PlaceholderRoundedStyled>
      </Animatable.View>
    } else {
      return <VSeparator />
    }
  }

  return (
    <VBox hAlign='center'>
      <VSeparator />
      {
        img()
      }
      <PlaceholderContentBlockStyled>
        {props.title ? <H2>{props.title}</H2> : null}
        {props.message ? <Body align='center'>{props.message}</Body> : null}
        <VSeparator />
        {props.children}
      </PlaceholderContentBlockStyled>
    </VBox>
  );
};


