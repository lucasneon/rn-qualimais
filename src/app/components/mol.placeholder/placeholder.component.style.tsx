import * as Animatable from 'react-native-animatable';
import styled from 'styled-components/native';
import { Color, Spacing } from '@atomic';
import { PlaceholderProps } from './placeholder.component';

const PLACEHOLDER_SIZE = 160;

export const PlaceholderRoundedStyled = styled.View`
  margin-vertical: ${(props: PlaceholderProps) => props.small ? Spacing.Small : Spacing.XLarge};
  height: ${(props: PlaceholderProps) => props.small ? 0.5 * PLACEHOLDER_SIZE : PLACEHOLDER_SIZE};
  width: ${(props: PlaceholderProps) => props.small ? 0.5 * PLACEHOLDER_SIZE : PLACEHOLDER_SIZE};
  border-radius: ${(props: PlaceholderProps) => props.small ? PLACEHOLDER_SIZE / 4 : PLACEHOLDER_SIZE / 2};
  background-color: ${Color.GrayXLight};
  justify-content: center;
  align-items: center;
`;

export const PlaceholderContentBlockStyled = styled(Animatable.View).attrs({
  animation: 'flipInX',
  duration: 500,
  useNativeDriver: true,
})`
  align-self: stretch;
  align-items: center;
`;
