import * as React from 'react';
import { Container } from 'typedi';
import { AlertBuilder } from '@app/components/atm.alert-builder';
import { ShouldLogErrorDetailsKey } from '@app/config';
import { AppError, AppErrorType } from '@app/model';
import { Asset, LinkButton, PrimaryButton } from '@atomic';
import { Placeholder } from './placeholder.component';

export interface PlaceholderSelectorProps {
  error: AppError;
  small?: boolean;
  onTap?: () => void;
}

const DefaultImage = Asset.Icon.Placeholder.Failure;

const ImagesMap: { [type in AppErrorType]: any } = {
  [AppErrorType.Camera]: Asset.Icon.Placeholder.Camera,
  [AppErrorType.NoConnection]: Asset.Icon.Placeholder.NoConnection,
  [AppErrorType.Maintenance]: Asset.Icon.Placeholder.Failure,
  [AppErrorType.Unknown]: Asset.Icon.Placeholder.Failure,
  [AppErrorType.Internal]: Asset.Icon.Placeholder.Failure,
};

export class PlaceholderSelector extends React.PureComponent<PlaceholderSelectorProps> {
  private shouldLogErrorDetails: boolean = Container.get(ShouldLogErrorDetailsKey);

  render() {
    const { error, onTap } = this.props;
    const image = ImagesMap[error.type] || DefaultImage;

    return (
      <Placeholder small={this.props.small} image={image} title={error.title} message={error.message}>
        {error.action && onTap && <PrimaryButton expanded={true} text={error.action} onTap={onTap} />}
        {this.shouldLogErrorDetails && error.errorDetails &&
          <LinkButton text='Ver detalhes' onTap={this.handleDetailsTap} />
        }
      </Placeholder>
    );
  }

  private handleDetailsTap = () => {
    if (!this.shouldLogErrorDetails) {
      return;
    }

    new AlertBuilder()
      .withMessage(this.props.error.errorDetails)
      .withButton({ text: 'Cancelar' })
      .show();
  }
}
