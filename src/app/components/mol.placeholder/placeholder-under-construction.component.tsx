import * as React from 'react';
import { Asset } from '@atomic';
import { Placeholder } from './placeholder.component';

export const PlaceholderUnderConstruction: React.SFC = () => (
  <Placeholder
    image={Asset.Icon.Placeholder.NotImplemented}
    title='Em construção'
    message='Estamos trabalhando nesta funcionalidade.'
  />
);
