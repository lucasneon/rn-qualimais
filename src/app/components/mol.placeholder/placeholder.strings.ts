export const PlaceholderStrings = {
  UnderConstruction: {
    Title: 'Em construção',
    Message: 'Estamos trabalhando nesta funcionalidade.',
  },
};
