import { Alert, AlertButton } from 'react-native';

export class AlertBuilder {
  private buttons: AlertButton[] = [];
  private title: string = null;
  private message: string = null;
  private onDismiss: () => void = null;
  private cancelable: boolean = true;

  withTitle(title: string) {
    this.title = title;
    return this;
  }

  withMessage(message: string) {
    this.message = message;
    return this;
  }

  withButton(button: AlertButton) {
    this.buttons.push(button);
    return this;
  }

  withOnDismiss(onDismiss: () => void) {
    this.onDismiss = onDismiss;
    return this;
  }

  isCancelable(cancelable: boolean) {
    this.cancelable = cancelable;
    return this;
  }

  show() {
    return Alert.alert(
      this.title,
      this.message,
      this.buttons,
      {
        onDismiss: this.onDismiss,
        cancelable: this.cancelable,
      },
    );
  }
}
