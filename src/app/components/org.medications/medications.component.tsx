import moment from 'moment';
import * as React from 'react';
import { FlatList, ListRenderItemInfo, StyleSheet, Text } from 'react-native';
import { WatermarkType } from '@app/components/atm.watermark';
import {
  HabitCell,
  HabitCellProps,
  HabitCellShimmer,
} from '@app/components/mol.cell';
import {
  Placeholder,
  PlaceholderSelector,
} from '@app/components/mol.placeholder';
import {
  MedicationFragment,
  MedicationListItemFragment,
} from '@app/data/graphql';
import { AppError } from '@app/model';
import { Asset, LinkButton, LoadingState, VSeparator } from '@atomic';
import { MedicationsStrings } from './medications.strings';

export interface MedicationsProps {
  loading: boolean;
  error: AppError;
  activeMedications: MedicationListItemFragment[];
  onMedicationTap: (medication: MedicationListItemFragment) => void;
  onActivateNewMedicationsTap: () => void;
}

const EmptyPlaceholder = () => (
  <Placeholder
    image={Asset.Icon.Custom.Habit}
    message={MedicationsStrings.Empty.Description}
    small={true}
  />
);

export class Medications extends React.PureComponent<MedicationsProps> {
  render() {
    const { activeMedications, error, loading } = this.props;

    return (
      <LoadingState
        loading={loading}
        data={!!activeMedications}
        error={!!error}>
        <LoadingState.Shimmer>
          <HabitCellShimmer count={1} />
        </LoadingState.Shimmer>
        <LoadingState.ErrorPlaceholder>
          <PlaceholderSelector small={true} error={error} />
        </LoadingState.ErrorPlaceholder>
        {!loading && activeMedications && activeMedications.length > 0 && (
          <>
            <Text style={styles.title}>Medicamentos</Text>
            <FlatList
              data={activeMedications}
              ListEmptyComponent={EmptyPlaceholder}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
            />
            <VSeparator />
            <LinkButton
              text={MedicationsStrings.Add}
              image={Asset.Icon.Atomic.Plus}
              onTap={this.props.onActivateNewMedicationsTap}
            />
          </>
        )}
      </LoadingState>
    );
  }

  private renderItem = (
    itemInfo: ListRenderItemInfo<MedicationFragment>,
  ): React.ReactElement<HabitCellProps> => {
    const item: MedicationFragment = itemInfo.item;
    const handleTap = () => this.props.onMedicationTap(item);
    return (
      <HabitCell
        title={item.medicine.name}
        id={item.id}
        type={WatermarkType.Medication}
        tag={MedicationsStrings.Tag}
        times={item.dailySchedule.map(schedule =>
          moment(schedule.hour, 'HH:mm:ss.sssZ').format('HH:mm'),
        )}
        onTap={handleTap}
        hideCheck={true}
      />
    );
  };

  private keyExtractor = (item: MedicationFragment) => item.id;
}

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    lineHeight: 18,
    fontSize: 16,
    marginLeft: 36,
    marginBottom: 10,
  },
});
