export const MedicationsStrings = {
  Empty: {
    Description: 'Você não tem medicamentos ativos',
  },
  Tag: 'Medicamento',
  Add: 'Adicionar novo medicamento',
};
