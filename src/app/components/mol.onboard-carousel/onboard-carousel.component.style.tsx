import { Dimensions } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import styled from 'styled-components/native';
import { Body, Color } from '@atomic';

const ImageContainerDiameter = 160;
const itemWidth = 0.8 * Dimensions.get('window').width;

export const ImageContainer = styled.View`
  align-self: center;
  justify-content: center;
  align-items: center;
  width: ${ImageContainerDiameter};
  height: ${ImageContainerDiameter};
  border-radius: ${ImageContainerDiameter};
  background-color: ${Color.GrayLight};
`;

export const CarouselStyled = styled(Carousel).attrs({
  sliderWidth: itemWidth,
  itemWidth,
  windowSize: 1,
})``;

export const PaginationStyled = styled(Pagination).attrs({
  containerStyle: { alignSelf: 'center' },
  dotColor: Color.Primary,
  inactiveDotColor: Color.Gray,
  inactiveDotOpacity: 0.4,
  inactiveDotScale: 0.6,
})``;

export const CarouselItemBodyStyled = styled(Body).attrs({
  align: 'center',
})``;
