import * as React from 'react';
import { Image } from 'react-native';
import { H3, LinkButton, VBox, VSeparator } from '@atomic';
import {
  CarouselItemBodyStyled, CarouselStyled, ImageContainer, PaginationStyled
} from './onboard-carousel.component.style';
import { OnboardCarouselStrings } from './onboard-carousel.strings';

interface OnboardCarouselState {
  index: number;
}

export interface OnboardCarouselItem {
  title: string;
  description: string;
  image: string;
}

interface OnboardCarouselProps {
  data: OnboardCarouselItem[];
  onFinish: () => void;
}

export class OnboardCarousel extends React.PureComponent<OnboardCarouselProps, OnboardCarouselState> {
  private carouselRef;

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
    };
  }

  render() {
    return (
      <VBox noGutter={true} vAlign='center' hAlign='center'>
        <CarouselStyled
          ref={c => this.carouselRef = c}
          data={this.props.data}
          renderItem={this.renderCarouselItem}
          onSnapToItem={this.handleSnapToItem}
        />
        <PaginationStyled
          carouselRef={this.carouselRef}
          dotsLength={this.props.data.length}
          activeDotIndex={this.state.index}
          tappableDots={!!this.carouselRef}
        />
        <LinkButton
          text={this.state.index !== this.props.data.length - 1 ?
            OnboardCarouselStrings.Next : OnboardCarouselStrings.Finish}
          onTap={this.handleLinkButtonTap}
        />
      </VBox>
    );
  }

  private handleSnapToItem = index => this.setState({ index });

  private handleLinkButtonTap = () => {
    const isLastItem = this.state.index === this.props.data.length - 1;

    if (isLastItem) {
      this.props.onFinish();
    } else {
      this.carouselRef.snapToNext();
    }
  }

  private renderCarouselItem = ({ item }) => {
    return (
      <VBox noGutter={true} hAlign='center' vAlign='center'>
        <VSeparator />
        <ImageContainer>
          <Image source={item.image} />
        </ImageContainer>
        <VSeparator />
        <H3>{item.title}</H3>
        <VSeparator />
        <CarouselItemBodyStyled>{item.description}</CarouselItemBodyStyled>
      </VBox>
    );
  }
}
