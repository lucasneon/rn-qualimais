import * as React from 'react';
import { Card } from '@app/components/atm.card';
import { ActionSheet, ActionSheetProps, VBox, VSeparator } from '@atomic';

export interface CardActionSheetProps extends ActionSheetProps {
  hasInput?: boolean;
}

export const CardActionSheet = (props: CardActionSheetProps) => {

  return (
    <ActionSheet
      onCancel={props.onCancel}
      onDismiss={props.onDismiss}
      visible={props.visible}
      justifyContent={props.hasInput ? 'flex-start' : 'center'}
    >
      <VBox vAlign={props.hasInput ? 'flex-start' : null} >
        {props.hasInput && <>
          <VSeparator />
          <VSeparator />
        </>}
        <Card>
          {props.children}
        </Card>
      </VBox>
    </ActionSheet >
  );
};
