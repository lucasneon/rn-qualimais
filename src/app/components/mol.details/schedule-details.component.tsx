import * as React from 'react';
import { EventsStrings } from '@app/modules/common/events/events.strings';
import { Asset, Body, DividerGray, H3, H4, VBox, VSeparator } from '@atomic';
import { Watermark, WatermarkType } from '../atm.watermark';
import { ButtonFooter } from '../mol.button-footer';
import { ListInfoCellProps } from '../mol.cell';
import { ListInfo } from '../mol.list-info';
import { Placeholder } from '../mol.placeholder';
import { CarePlanEventsStrings } from '../org.care-plan-events/care-plan-events.strings';

export interface ScheduleDetailsProps {
  dueDate: string;
  appointmentDate?: string;
  observation?: string;
  doctorInfo?: ListInfoCellProps;
  onTap: () => void;
}

export class ScheduleDetails extends React.PureComponent<ScheduleDetailsProps> {
  render() {
    return (
      <>
        <Watermark
          header={true}
          type={WatermarkType.Consultation}
          big={true}
          light={false}
          vAlign='flex-start'
        />
        <VBox>
          <H3>{this.props.appointmentDate ?
          CarePlanEventsStrings.Fulfill.Appointment.Scheduled(this.props.appointmentDate)
          : CarePlanEventsStrings.Fulfill.Appointment.Until(this.props.dueDate) }</H3>
          {this.props.appointmentDate &&
            <Body>{CarePlanEventsStrings.Fulfill.Appointment.Until(this.props.dueDate)}</Body>
          }
          <VSeparator/>
        </VBox>
        {this.props.doctorInfo &&
          <>
            <DividerGray/>
            <VSeparator/>
            <VBox>
              <ListInfo
                title={EventsStrings.Appointment.Details.Doctor(this.props.doctorInfo.title)}
                side={this.props.doctorInfo.side}
                descriptions={this.props.doctorInfo.descriptions}
                links={this.props.doctorInfo.links}
              />
            </VBox>
            <VSeparator/>
          </>
        }
        <DividerGray/>
        {!this.props.appointmentDate && !this.props.doctorInfo &&
          <>
            <Placeholder
              image={Asset.Onboard.OnboardCalendar}
              title={EventsStrings.Appointment.Title}
              message={EventsStrings.Appointment.Details.Warning(this.props.dueDate)}
            />
            {/* <ButtonFooter
              primary={ { text: EventsStrings.Schedule  ,
              onTap: this.props.onTap}}
            /> */}
          </>
        }
        {this.props.observation &&
          <VBox>
            <VSeparator/>
            <H4>{EventsStrings.Appointment.Details.Observation}</H4>
            <Body>{this.props.observation}</Body>
            <VSeparator/>
          </VBox>
        }
        {!this.props.doctorInfo && this.props.appointmentDate &&
          <Placeholder
            image={Asset.Icon.Placeholder.Failure}
            title={EventsStrings.Appointment.Details.NotFound.Title}
            message={EventsStrings.Appointment.Details.NotFound.Error}
          />
        }
      </>
    );
  }
}
