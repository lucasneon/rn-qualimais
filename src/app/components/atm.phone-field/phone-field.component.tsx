import * as React from 'react';
import { Platform } from 'react-native';
import { Form, TextField, Validators } from '@atomic';

const AndroidPhysicalPhoneMaxSize = 17;
const IosPhysicalPhoneMaxSize = 18;
const RawPhysicalPhoneLength = 12;
const RawCellphoneLength = 13;
const PhonePlaceholder = '+55 (99) 99999-9999';
const PhysicalPhoneMask = '+55 ([00]) [0000]-[00009]';
const CellphoneMask = '+55 ([00]) [00000]-[0009]';

interface PhoneFieldProps {
  name: string;
  label: string;
  initialValue: string;
  required: boolean;
  editable?: boolean;
}

interface PhoneFieldState {
  phoneMask: string;
  oneshotMask: string;
}

export class PhoneField extends React.Component<PhoneFieldProps, PhoneFieldState> {
  private validators = [Validators.IsPhone('Deve ser um telefone válido')];

  constructor(props) {
    super(props);

    if (this.props.required) {
      this.validators.push(Validators.Required('Campo obrigatório'));
    }

    // really ugly solution for 1st input android behavior, but it was what worked given the restrictions of the library
    // android always applies the last mask, so the 1st input does not have a previous mask to apply
    // this caused a bug when we had an initial phone value of a physical phone and added one more digit
    // the ugly solution wast to use a oneshotMask to set the first mask
    // and the actual phone mask has the mask that works as previous mask for the 1st input
    switch (this.props.initialValue.length) {
      case RawCellphoneLength:
        this.state = { phoneMask: CellphoneMask, oneshotMask: null };
        break;

      case RawPhysicalPhoneLength:
        this.state = { phoneMask: CellphoneMask, oneshotMask: PhysicalPhoneMask };
        break;

      default:
        this.state = { phoneMask: PhysicalPhoneMask, oneshotMask: null };
        break;
    }
  }

  componentDidMount() {
    // after being used once, oneshotMask is disabled
    this.setState({oneshotMask: null});
  }

  render() {
    return(
      <Form.Field
        label={this.props.label}
        validators={this.validators}
        name={this.props.name}
        onValueChange={this.handlePhoneChange}
        value={this.props.initialValue}
      >
        <TextField
          keyboardType='numeric'
          mask={this.state.oneshotMask || this.state.phoneMask} // part of the ugly solution
          placeholder={PhonePlaceholder}
          onKeyPress={this.handlePhoneKeyPress}
          editable={this.props.editable}
        />
      </Form.Field>
    );
  }

  private handlePhoneKeyPress = ( event: any ) => {
    if ( (Platform.OS === 'android') && (event.nativeEvent.key === 'Backspace') ) {  // Fix for android delayed behavior
      this.setState({phoneMask: PhysicalPhoneMask});
    }
  }

  private handlePhoneChange = (phoneNumber: string) => { // Platform specific because of different mask behaviors
    const isAndroid = Platform.OS === 'android';
    const lengthToCellphone = isAndroid ? AndroidPhysicalPhoneMaxSize : IosPhysicalPhoneMaxSize;

    this.setState({phoneMask: (phoneNumber.length > lengthToCellphone) ? CellphoneMask : PhysicalPhoneMask});
  }
}
