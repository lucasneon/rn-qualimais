import * as React from 'react';
import { Animated, FlatList, RefreshControl, TouchableOpacity, Image } from 'react-native';
import { LargeHeader, SmallHeader } from '@app/components/atm.header';
import { CollapsibleHeader as Collapsible, Color, Scroll, LinkButtonProps, HBox } from '@atomic';

interface CollapsibleHeaderProps {
  title?: string;
  flatList?: boolean;
  children: any;
  topBar?: boolean;
  dock?: any;
  keyboardShouldPersistTaps?: string;
  refresh?: () => void;
  loading?: boolean;
  buttons?: LinkButtonProps[];
}

const AnimatedScroll = Animated.createAnimatedComponent(Scroll);
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const androidLoaderTopDist = 75;

export class CollapsibleHeader extends React.Component<CollapsibleHeaderProps, undefined> {
  static defaultProps = { topBar: true };

  render() {
    const { ...props } = this.props;
    const AnimatedComponent = props.flatList ? AnimatedFlatList : AnimatedScroll;

    return (
      <Collapsible topBar={this.props.topBar}>
        <Collapsible.LargeHeader>
          <LargeHeader title={props.title} buttons={props.buttons && props.buttons.map(this.mapButtons)} />
        </Collapsible.LargeHeader>
        <Collapsible.SmallHeader>
          <SmallHeader title={props.title} />
        </Collapsible.SmallHeader>
        <AnimatedComponent
          keyboardShouldPersistTaps={this.props.keyboardShouldPersistTaps}
          refreshControl={this.props.refresh &&
            <RefreshControl
              onRefresh={this.props.refresh}
              refreshing={this.props.loading}
              progressViewOffset={androidLoaderTopDist}
              tintColor={Color.Primary}
              progressBackgroundColor={Color.White}
            />
            }
        >
          {this.props.dock &&
            <Collapsible.Dock>
              {this.props.dock}
            </Collapsible.Dock>
          }
          {props.children}
        </AnimatedComponent>
      </Collapsible>
    );
  }

  private mapButtons = (button: LinkButtonProps): any => {
    return (
      <HBox>
        <TouchableOpacity onPress={button.onTap}>
          <Image
            source={button.image}
          />
        </TouchableOpacity>
        <HBox.Separator />
      </HBox>
    );
  }

}
