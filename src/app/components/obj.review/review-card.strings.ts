export const ReviewCardStrings = {
  EvaluateLater: 'Avaliar mais tarde',
  Comment: 'Adicionar comentário',
  CommentLabel: 'Comentário',
  CommentLater: 'Comentar mais tarde',
  Send: 'Enviar',
};
