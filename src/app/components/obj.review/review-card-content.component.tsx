import * as React from 'react';
import { StarControl } from '@app/components/mol.star-control';
import { GeneralStrings } from '@app/modules/common';
import AvatarImage from '@assets/images/samples/avatar.png';
import {
  AvatarSize, Body, FontSize, Form, H3, LinkButton, Shimmer, ShimmerSeparator, TextField, VBox, VSeparator
} from '@atomic';
import { ReviewCardStrings } from './review-card.strings';
import { ReviewAvatar, ReviewSpinner } from './review.component.style';

export interface ReviewCardProps {
  title: string;
  description?: string;
  onCancel: () => void;
  onSend: (evaluatedValue: number, comment?: string) => void;
  loading: boolean;
}

interface ReviewCardContentState {
  content: 'no-rate' | 'rated' | 'commenting';
}

export const ReviewCardContentShimmer = () => (
  <VBox hAlign='center'>
    <Shimmer height={AvatarSize.Big} width={AvatarSize.Big} rounded={true} />
    <VSeparator />
    <Shimmer height={FontSize.Medium} width='70%' />
    <ShimmerSeparator />
    <Shimmer height={FontSize.Small} width='90%' />
    <VSeparator />
    <StarControl />
    <VSeparator />
    <ShimmerSeparator />
    <Shimmer height={FontSize.Small} width='35%' />
  </VBox>
);

export class ReviewCardContent extends React.PureComponent<ReviewCardProps, ReviewCardContentState> {
  private evaluationValue;
  private comment;

  constructor(props) {
    super(props);
    this.state = { content: 'no-rate' };
  }

  render() {
    return (
      <VBox>
        <VBox hAlign='center' noGutter={true}>
          <ReviewAvatar source={AvatarImage} />
          <VSeparator />

          <H3>{this.props.title}</H3>
          <Body>{this.props.description}</Body>
          <VSeparator />

          <StarControl onStarTap={this.handleStarTap} />
          <VSeparator />
        </VBox>

        {this.state.content === 'no-rate' ? (
          <LinkButton onTap={this.props.onCancel} text={GeneralStrings.Cancel} />
        ) : this.state.content === 'rated' ? (
          <>
            <LinkButton onTap={this.handleCommentTap} text={ReviewCardStrings.Comment} disabled={this.props.loading} />

            {this.props.loading ? (
              <VBox hAlign='center' noGutter={true}><ReviewSpinner /></VBox>
            ) : (
              <LinkButton onTap={this.handleSendTap} text={ReviewCardStrings.CommentLater} />
            )}
          </>
        ) : (
          <>
            <Form.Field label={ReviewCardStrings.CommentLabel} name='comment'>
              <TextField multiline={true} onChangeText={this.handleChangeText} editable={!this.props.loading} />
            </Form.Field>
            <VSeparator />

            {this.props.loading ? (
              <VBox hAlign='center' noGutter={true}><ReviewSpinner /></VBox>
            ) : (
              <LinkButton onTap={this.handleSendTap} text={ReviewCardStrings.Send} />
            )}
          </>
        )}
      </VBox>
    );
  }

  private handleStarTap = (index: number) => {
    this.evaluationValue = index + 1; // will be zero if none selected
    this.setState({ content: 'rated' });
  }

  private handleCommentTap = () => {
    this.setState({ content: 'commenting' });
  }

  private handleSendTap = () => {
    if (this.props.onSend) {
      this.props.onSend(this.evaluationValue, this.comment);
    }
  }

  private handleChangeText = (textInput: string) => {
    this.comment = textInput;
  }
}
