import styled from 'styled-components/native';
import { Avatar } from '@app/components/atm.avatar';
import { AvatarSize, Color, Spacing } from '@atomic';
import { ButtonIconSize } from '@atomic/atm.button/button.component.style';
import { SpinnerStyled } from '@atomic/atm.loader/circle-loader.component';

export const ReviewAvatar = styled(Avatar).attrs({
  withBorder: true,
  big: true,
})`
  margin-top: ${ -(AvatarSize.Big + Spacing.Large) / 2};
`;

export const ReviewSpinner: any = styled(SpinnerStyled).attrs({
  size: ButtonIconSize * 1.5 ,
  color: Color.Gray,
})`
  color: ${Color.Gray};
`;
