import * as React from 'react';
import { H2, PrimaryButton, Root, VBox } from '@atomic';
import { storiesOf } from '@storybook/react-native';
import { CardActionSheet } from '../mol.card-action-sheet/card-action-sheet.component';
import { ReviewCardContent } from './review-card-content.component';

const stories = storiesOf('Objects', module);

stories.add('Review', () => (
  <ReviewSample />
));

class ReviewSample extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = { visible: false, loading: null };
  }

  render() {
    return (
      <Root>
        <H2>Review dialog</H2>
        <VBox>
          <PrimaryButton
            text='Open review dialog'
            onTap={this.openReviewDialog}
            expanded={true}
          />
        </VBox>
        {/* Same as the Review component but without using a graphql provider */}
        <CardActionSheet onCancel={this.handleCancel} visible={this.state.visible}>
          <ReviewCardContent
            title='Avalie o atendimento do Dr Marcelo'
            description='Quarta-feira - 15h'
            onSend={this.handleSend}
            onCancel={this.handleCancel}
            loading={this.state.loading}
          />
        </CardActionSheet>
      </Root >
    );
  }

  private handleSend = (evaluationValue: number, comment?: string) => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ visible: false, loading: false });
      // tslint:disable-next-line:max-line-length
      console.warn(`Rated!`, `You rated it ${evaluationValue} stars and with ${comment ? 'a comment.' : 'no comments'}!`);
    }, 2500);
  }

  private handleCancel = () => {
    this.setState({ visible: false, loading: false });
  }

  private openReviewDialog = () => {
    this.setState({ visible: true, loading: false });
  }
}
