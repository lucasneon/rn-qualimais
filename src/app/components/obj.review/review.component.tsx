import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { Container } from 'typedi';
import { CardActionSheet } from '@app/components/mol.card-action-sheet';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { ReviewCardContent } from './review-card-content.component';

interface ReviewProps {
  title: string;
  description?: string;
  visible: boolean;
  onSuccess: (response: any) => void;
  onError: (error: ApolloError) => void;
  onCancel: () => void;
  data: any;
}

interface ReviewState {
  loading: boolean;
}

export class Review extends React.PureComponent<ReviewProps, ReviewState> {
  private graph: GraphQLProvider = Container.get(GraphQLProviderToken);

  constructor(props) {
    super(props);
    this.state = { loading: false };
  }

  render() {
    return (
      <CardActionSheet {...this.props}>
        <ReviewCardContent
          onSend={this.onSend}
          {...this.props}
          loading={this.state.loading}
        />
      </CardActionSheet>
    );
  }

  private onSend = (rating: number) => {
    this.setState({ loading: true });
    const input: any = { data: { ...this.props.data, rating } };
    //TODO FAZER A REQUISIÇÃO
    this.graph.mutate(
      'rate-service',
      input,
      this.handleSuccess,
      this.handleError,
      undefined,
    );
  };

  private handleSuccess = (response: any) => {
    this.setState({ loading: false });
    this.props.onSuccess(response);
  };
  private handleError = (error: ApolloError) => {
    this.setState({ loading: false });
    this.props.onError(error);
  };
}
