import * as React from 'react';
import { FlatList, ListRenderItemInfo } from 'react-native';
import { ListInfoCell, ListInfoCellProps } from '@app/components/mol.cell';

export interface NewsListItem extends ListInfoCellProps {
  itemKey: string;
  image?: string;
  url?: string;
}

export interface NewsListProps {
  items: NewsListItem[];
}

export class NewsList extends React.PureComponent<NewsListProps> {

  render() {
    return (
      <FlatList
        keyExtractor={this.keyExtractor}
        data={this.props.items}
        renderItem={this.renderCells}
      />
    );
  }

  private keyExtractor = (item: NewsListItem) => item.itemKey;

  private renderCells = (itemInfo: ListRenderItemInfo<NewsListItem>) => {
    return <ListInfoCell {...itemInfo.item} />;
  }
}
