import * as React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Container } from 'typedi';
import { AlertBuilder } from '@app/components/atm.alert-builder';
import { ShouldLogErrorDetailsKey } from '@app/config/tokens.config';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { AppError } from '@app/model';
import { Asset, Color, Spacing } from '@atomic';
import { CancelImageStyled, DropdownAlertStyled, MessageStyled, WarningImageStyled } from './flash-message.style';

export enum FlashMessageType {
  Success = 'success',
  Error = 'error',
}

interface FlashMessageState {
  color?: string;
}

export interface FlashMessageNavigationProps {
  type: FlashMessageType;
  message?: string;
  error?: AppError;
}

interface FlashMessageProps extends NavigationPageProps<FlashMessageNavigationProps> { }

// https://www.npmjs.com/package/react-native-dropdownalert
export class FlashMessage extends React.Component<FlashMessageProps, FlashMessageState> {
  static options = { name: 'FlashMessage' };

  private shouldLogErrorDetails: boolean = Container.get(ShouldLogErrorDetailsKey);
  private dropdown;

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);

    this.state = {
      color: Color.Success,
    };

  }

  componentDidAppear() {
    const props = this.props.navigation;
    const message = props.message || props.error.message;
    const color = props.type === FlashMessageType.Success ? Color.Success : Color.Alert;

    this.setState({ color }, () => {

      return this.dropdown.alertWithType(props.type, '', message)
    });
  }

  render() {
    return (
      <View>
        <DropdownAlertStyled
          color={this.state.color}
          renderTitle={this.renderTitle}
          renderMessage={this.renderMessage}
          renderCancel={this.renderCancel}
          renderImage={this.renderImage}
          ref={this.getRef}
          onClose={this.onClose}
        />
      </View>
    );
  }

  private renderTitle = () => {
    return null;
  }

  private renderMessage = () => {
    return (
      <MessageStyled color={this.state.color}>{this.dropdown.alertData.message}</MessageStyled>
    );
  }

  private renderCancel = () => {
    return (
      <View style={{ padding: Spacing.Small }}>
        <TouchableOpacity onPress={this.handleCloseButtonTap}>
          <CancelImageStyled source={Asset.Icon.Atomic.Close} color={this.state.color} />
        </TouchableOpacity>
      </View>
    );
  }

  private renderImage = () => {
    return (
      <View style={{ padding: Spacing.Small }}>
        <WarningImageStyled source={Asset.Icon.Custom.Warning} color={this.state.color} />
      </View>
    );
  }

  private handleCloseButtonTap = () => {
    this.dropdown.close('cancel');
  }

  private onClose = data => {
    const didCancelWithPanGesture = data.action === 'pan';
    const errorDetails: string = this.props.navigation.error && this.props.navigation.error.errorDetails;

    if (didCancelWithPanGesture && this.shouldLogErrorDetails && errorDetails) {
      this.showErrorDetail(errorDetails);
    }

    Navigation.dismissOverlay(this.props.componentId);
  }

  private showErrorDetail(errorDetails: string) {
    new AlertBuilder()
      .withMessage(errorDetails)
      .withButton({ text: 'Cancelar' })
      .show();
  }

  private getRef = (ref: any) => {
    this.dropdown = ref;
  }
}
