import { Image, Text } from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import styled from 'styled-components/native';
import { Border, Color, FontFamily, FontSize, Spacing } from '@atomic';

interface FlashMessageProps {
  color: string;
  renderTitle?: any;
  renderImage?: any;
  renderCancel?: any;
}

export const MessageStyled = styled(Text)`
  flex-grow: 1;
  font-size: ${FontSize.Small};
  font-family: ${FontFamily.Primary.Regular};
  color: ${(props: FlashMessageProps) => props.color};
  line-height: 24;
`;

export const WarningImageStyled = styled(Image)`
  tint-color: ${(props: FlashMessageProps) => props.color};
`;

export const CancelImageStyled = styled(Image)`
  tint-color: ${(props: FlashMessageProps) => props.color};
  width: 16;
  height: 16;
  padding: ${Spacing.Small + 'px'};
`;

export const DropdownAlertStyled = styled(DropdownAlert).attrs((props: FlashMessageProps) => ({
  defaultContainer: ({
    display: 'flex',
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: Border.Width,
    borderColor: props.color,
    borderRadius: 8,
    padding: Spacing.Small,
    alignItems: 'center',
  }),
  updateStatusBar: false,
  errorColor: Color.White,
  successColor: Color.White,
  titleNumOfLines: 0,
  messageNumOfLines: 2,
  renderTitle: props.renderTitle,
  renderImage: props.renderImage,
  renderCancel: props.renderCancel,
  tapToCloseEnabled: false,
  showCancel: true,
}))``;
