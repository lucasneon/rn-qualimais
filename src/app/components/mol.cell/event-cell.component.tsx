import * as React from 'react';
import { Watermark, WatermarkType } from '@app/components/atm.watermark';
import { Caption, Cell, DividerGray, H3, HBox, Root, Shimmer } from '@atomic';
import { FontSize } from '@atomic/obj.constants';
import { CellShimmerProps } from './cell-shimmer.model';
import { CellStyled, EventCellBackgroundStyled } from './cell.component.style';
import { StyleSheet, Text } from 'react-native';

export interface EventCellProps {
  title: string;
  auxiliar?: string;
  tag?: string;
  type?: WatermarkType;
  children?: any;
  onTap?: (id: string) => void;
  item?: any;
  disabled?: boolean;
}

const SingleCellShimmer: React.SFC = () => (
  <Cell>
    <Shimmer height={FontSize.Large} width="80%" />
    <Shimmer height={FontSize.Large} width="25%" />
    <Shimmer height={FontSize.XSmall} width="40%" />
  </Cell>
);

export const EventCellShimmer: React.SFC<CellShimmerProps> = props => (
  <Root>
    {new Array(props.count || 1).fill(null).map((_, i) => (
      <SingleCellShimmer key={i} />
    ))}
  </Root>
);

export const EventCell: React.SFC<EventCellProps> = (props: EventCellProps) => {
  const handleItemTap = (id: string) => {
    return () => props.onTap(id);
  };

  return (
    <>
      <CellStyled
        disabled={props.disabled}
        onPress={props.onTap && handleItemTap(props.item)}>
        <EventCellBackgroundStyled>
          <HBox>
            <HBox.Separator />
            <HBox.Item>
              <Text style={styles.title}>{props.title}</Text>
              {props.auxiliar && (
                <Text style={styles.subsTitle}>{props.auxiliar}</Text>
              )}
            </HBox.Item>
            <HBox.Separator />
            <HBox.Separator />
          </HBox>
        </EventCellBackgroundStyled>
      </CellStyled>
      <DividerGray />
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    color: '#ffff',
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 16,
    marginBottom: 10,
  },
  subsTitle: {
    fontSize: 14,
    color: '#ffff',
    lineHeight: 14,
  },
});
