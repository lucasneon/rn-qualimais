import * as React from 'react';
import { PersonCell } from '@app/components/mol.cell';
import AvatarImage from '@assets/images/samples/avatar.png';
import { H3 } from '@atomic';

interface BeneficiaryCellProps {
  name: string;
  onTap?: () => any;
}
export const BeneficiaryCell = (props: BeneficiaryCellProps) => {
  const handleTap = () => {
    if (props.onTap) {
      props.onTap();
    }
  };

  return (
    <>
      <PersonCell avatar={AvatarImage} onTap={handleTap}>
        <H3>{props.name}</H3>
      </PersonCell>
    </>
  );
};
