import * as React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';
import { Body, Caption, Color, H4, HDisplay, Spacing } from '@atomic';
import { GradientColorMapper } from './gradient-cell.mapper';

const Gradient = styled(LinearGradient).attrs({
  start: { x: 0, y: 1 },
  end: { x: 0, y: 0 },
})`
  overflow: hidden;
`;

interface GradientCellProps {
  type?: 'primary' | 'secondary';
  index?: number;
  total?: number;
  inactive?: boolean;
  children: any;
  firstColor: string;
  secondColor: string;
}

export const GradientCell: React.SFC<GradientCellProps> = (
  props: GradientCellProps,
) => {
  const gradient = props.inactive
    ? [Color.GrayDark, Color.GrayDark]
    : GradientColorMapper.getGradient(
        props.firstColor ? props.firstColor : '#171a87',
        props.secondColor ? props.secondColor : '#494dda',
        props.index,
        props.total,
      );

  return (
    <Gradient {...props} colors={gradient}>
      {props.children}
    </Gradient>
  );
};

GradientCell.defaultProps = {
  type: 'primary',
};

export const HDisplayGradient = styled(HDisplay)`
  color: ${Color.White};
  margin-right: ${Spacing.XSmall};
`;

export const H4Gradient = styled(H4)`
  color: ${Color.White};
`;

export const BodyGradient = styled(Body)`
  margin-top: ${Spacing.Small};
  color: ${Color.White};
`;

export const CaptionGradient = styled(Caption)`
  color: ${Color.White};
`;
