import * as React from 'react';
import { ListInfo, ListInfoProps } from '@app/components/mol.list-info';
import { Cell, CellProps, DividerGray, HBox, Shimmer, ShimmerSeparator, VBox, VSeparator } from '@atomic';
import { CellShimmerProps } from './cell-shimmer.model';

export type ListInfoCellProps = CellProps & ListInfoProps;

const CellShimmer: React.SFC = () => (
  <VBox hAlign='flex-start' vAlign='center'>
    <VSeparator />
    <HBox vAlign='center' hAlign='flex-start'>
      <HBox.Item grow={9} ><Shimmer /></HBox.Item>
      <HBox.Separator />
      <HBox.Item grow={2}><Shimmer /></HBox.Item>
    </HBox>
    <ShimmerSeparator />
    <Shimmer width='30%' />
    <ShimmerSeparator />
    <Shimmer width='70%' />
    <ShimmerSeparator />
    <Shimmer width='50%' />
    <VSeparator />
    <DividerGray />
  </VBox>
);

export const ListInfoCellShimmer: React.SFC<CellShimmerProps> = props => (
  <>
    {new Array(props.count || 1).fill(null).map((_, i) => <CellShimmer key={i} />)}
  </>
);

export const ListInfoCell: React.SFC<ListInfoCellProps> = props => (
  <Cell {...props}>
    <ListInfo {...props} />
  </Cell>
);
