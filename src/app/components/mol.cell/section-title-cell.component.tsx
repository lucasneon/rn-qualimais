import * as React from 'react';
import { LinkButton } from '@atomic/atm.button';
import { H2 } from '@atomic/atm.typography';
import { HBox, VBox } from '@atomic/obj.grid';

export interface SectionTitleCellProps {
  text: string;
  linkText?: string;
  onTap?: any;
}

export const SectionTitleCell = (props: SectionTitleCellProps) => (
  <VBox>
    <HBox>
      <HBox.Item>
        <H2>{props.text}</H2>
      </HBox.Item>
      {props.linkText && props.onTap &&
        <>
          <HBox.Separator />
          <HBox.Item wrap={true} vAlign={'center'}>
            <LinkButton text={props.linkText} onTap={props.onTap} />
          </HBox.Item>
        </>
      }
    </HBox>
  </VBox>
);
