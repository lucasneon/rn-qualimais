import * as React from 'react';
import { WatermarkType } from '@app/components/atm.watermark';
import { DefinitionListCell } from '@app/components/mol.cell/definition-list-cell.component';
import { HabitCell } from '@app/components/mol.cell/habit-cell.component';
import { ListCell } from '@app/components/mol.cell/list-cell.component';
import { ListInfoCell } from '@app/components/mol.cell/list-info-cell.component';
import AvatarImage from '@assets/images/samples/avatar.png';
import { Asset, Cell, H2, H3, Root, VSeparator } from '@atomic';
import { Scroll } from '@atomic/obj.scroll';
import { storiesOf } from '@storybook/react-native';
import { EventCell, PersonCell, SectionTitleCell } from './';
import { SwitchCell } from './switch-cell.component';

const stories = storiesOf('Molecules', module);

function action(text) { return () => console.log(text); }
const handleTap = item => console.log(item);

const Section = props => {
  return (
    <Root>
      <H2>{props.children.type.name}</H2>
      {props.children}
      <VSeparator />
    </Root>
  );
};

stories.add('Cells - Custom', () => (
  <Scroll>
    <Section>
      <HabitCell id='1' title='Title' type={WatermarkType.Consultation} tag='tag' times={['11:00']} />
    </Section>

    <Section>
      <Cell onTap={action('Cell click')}>
        <H3>This is the default list cell</H3>
      </Cell>
    </Section>

    <Section>
      <SectionTitleCell
        text='This is a section title with link button (default is H2 title)'
        linkText='Link button'
        onTap={action}
      />
    </Section>

    <Section>
      <SectionTitleCell text='This is a section title' />
    </Section>

    <Section>
      <ListCell
        text='Change Password'
        leftThumb={Asset.Icon.Custom.MedicalRecord}
        rightThumb={Asset.Icon.Atomic.ChevronRight}
        onTap={action('Cell Clicked!')}
      />
    </Section>

    <Section>
      <PersonCell avatar={AvatarImage} item={1} onTap={handleTap}>
        <H3>Person cell component</H3>
      </PersonCell>
    </Section>

    <Section>
      <EventCell
        title={'Consulta com cardiologista'}
        auxiliar={'Realizar até 12/12/2019'}
        tag='consulta'
        type={WatermarkType.Consultation}
        item={1}
        onTap={handleTap}
      />
    </Section>

    <Section>
      <DefinitionListCell term='Para' definition='Célio Cordeira de Lima' />
    </Section>

    <Section>
      <ListInfoCell
        title='Delboni Unidade Paulista'
        side='1,5km'
        descriptions={['Avenida Paulista, 1998', 'Consolação - São Paulo, SP', '09029-221']}
        links={[{ image: Asset.Icon.Custom.Phone, text: '(11) 99999-9999', onTap: action('Link clicked!') }]}
      />
    </Section>

    <Section>
      <SwitchCell text='Cell content' onChange={handleTap} />
    </Section>
  </Scroll>
));
