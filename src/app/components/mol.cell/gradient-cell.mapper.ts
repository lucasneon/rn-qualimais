
export class GradientColorMapper {
  static getGradient(startColor: string, endColor: string, index: number = 0, total: number = 1) {
    if (index >= total) {
      throw new Error('Index must be smaller than total');
    }
    const gradientStart = (index > 0) ?
      GradientColorMapper.getColor(startColor, endColor, index / total) : startColor;
    const gradientEnd = (index < total) ?
      GradientColorMapper.getColor(startColor, endColor, (index + 1) / total) : endColor;

    return [gradientStart, gradientEnd];
  }

  static getColor(startColor: string, endColor: string, percent: number) {
    // strip the leading # if it's there
    startColor = startColor.replace(/^\s*#|\s*$/g, '');
    endColor = endColor.replace(/^\s*#|\s*$/g, '');

    // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
    if (startColor.length === 3) {
      startColor = startColor.replace(/(.)/g, '$1$1');
    }

    if (endColor.length === 3) {
      endColor = endColor.replace(/(.)/g, '$1$1');
    }

    // get colors
    const startRed = parseInt(startColor.substr(0, 2), 16);
    const startGreen = parseInt(startColor.substr(2, 2), 16);
    const startBlue = parseInt(startColor.substr(4, 2), 16);

    const endRed = parseInt(endColor.substr(0, 2), 16);
    const endGreen = parseInt(endColor.substr(2, 2), 16);
    const endBlue = parseInt(endColor.substr(4, 2), 16);

    // calculate new color
    const diffRed = endRed - startRed;
    const diffGreen = endGreen - startGreen;
    const diffBlue = endBlue - startBlue;

    let diffRedString = ( (diffRed * percent) + startRed ).toString(16).split('.')[0];
    let diffGreenString = ( (diffGreen * percent) + startGreen ).toString(16).split('.')[0];
    let diffBlueString = ( (diffBlue * percent) + startBlue ).toString(16).split('.')[0];

    // ensure 2 digits by color
    if (diffRedString.length === 1 ) {
      diffRedString = '0' + diffRedString;
    }
    if (diffGreenString.length === 1 ) {
      diffGreenString = '0' + diffGreenString;
    }
    if (diffBlueString.length === 1 ) {
      diffBlueString = '0' + diffBlueString;
    }

    return '#' + diffRedString + diffGreenString + diffBlueString;
  }

}
