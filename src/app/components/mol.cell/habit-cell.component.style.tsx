import styled from 'styled-components/native';
import { Border, Opacity, Shimmer, Spacing } from '@atomic';
import { GradientCell } from './gradient-cell.component.style';
import { Color } from '@atomic';

interface HabitCellProps {
  disabled?: boolean;
  inactive?: boolean;
}

export const HabitCellStyled = styled.View`
  margin-top: ${Spacing.Small};
  margin-left: ${Spacing.Large};
  margin-right: ${Spacing.Large};
`;

export const HabitCellCheckAreaStyled = styled.View`
  top: 22;
  position: absolute;
`;

export const HabitGradientCellStyled = styled(GradientCell)`
  padding-vertical: ${Spacing.Large};
  padding-horizontal: ${Spacing.Large};
  border-radius: ${Border.RadiusLarge};
  min-height: 80;
  opacity: ${(props: HabitCellProps) =>
    props.disabled ? Opacity.Disabled : 1};
  background: #171a88;
`;

export const HabitCellShimmerStyled = styled(Shimmer)`
  border-radius: ${Border.RadiusLarge};
`;
