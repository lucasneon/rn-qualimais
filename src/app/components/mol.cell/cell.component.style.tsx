import { Platform } from 'react-native';
import styled from 'styled-components/native';
import { Color, Opacity } from '@atomic';
import { GUTTER } from '@atomic/obj.grid';
import { GradientCell } from './gradient-cell.component.style';

const cellStyled: any =
  Platform.OS === 'android'
    ? styled.TouchableNativeFeedback
    : styled.TouchableOpacity;

export const CellStyled = cellStyled.attrs({
  activeOpacity: Opacity.Active,
})``;

export const CellBackgroundStyled = styled.View`
  padding-vertical: ${GUTTER};
  background-color: ${Color.White};
`;

export const EventCellBackgroundStyled = styled(GradientCell)`
  padding-vertical: ${GUTTER};
  min-height: 80;
  background-color: ${Color.BlueDark};
  margin-left: 20;
  margin-right: 20;
  border-radius: 10;
  margin-bottom: 10;
`;
