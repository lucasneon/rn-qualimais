import * as React from 'react';
import { Cell, DD, DT, HBox } from '@atomic';

interface DefinitionListProps {
  term: string;
  definition: string;
  definitionGrowth?: number;
}

export const DefinitionListCell: React.SFC<DefinitionListProps> = props => {
  return (
    <Cell>
      <HBox vAlign='center' hAlign='center'>
        <HBox.Item grow={1}><DT>{props.term}</DT></HBox.Item>
        <HBox.Item grow={props.definitionGrowth ? props.definitionGrowth : 1}><DD>{props.definition}</DD></HBox.Item>
      </HBox>
    </Cell>
  );
};
