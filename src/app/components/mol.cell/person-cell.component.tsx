import * as React from 'react';
import { Image, ImagePropertiesSourceOptions } from 'react-native';
import { Avatar } from '@app/components/atm.avatar';
import { Asset, DividerGray, HBox } from '@atomic';
import { CellBackgroundStyled, CellStyled } from './cell.component.style';

interface PersonCellProps {
  avatar: ImagePropertiesSourceOptions;
  children?: any;
  onTap?: (item: string) => void;
  item?: any;
  chevron?: boolean;
}

export const PersonCell: React.SFC<PersonCellProps> = (props: PersonCellProps) => {

  const handleItemTap = (item: any) => {
    if (props.onTap) {
      props.onTap(item);
    }
  };

  return (
    <>
      <CellStyled onPress={handleItemTap.bind(undefined, props.item)}>
        <CellBackgroundStyled>
          <HBox>
            <HBox.Separator />
            <HBox.Item wrap={true}>
              <Avatar source={props.avatar}/>
            </HBox.Item>
            <HBox.Separator />
            <HBox.Item vAlign='center'>
              {props.children}
            </HBox.Item>
            <HBox.Separator />
            {props.chevron && <HBox.Item  vAlign='center' wrap={true}>
              <Image source={Asset.Icon.Atomic.ChevronRight} />
            </HBox.Item>}
            <HBox.Separator />
          </HBox>
        </CellBackgroundStyled>
      </CellStyled>
      <DividerGray />
    </>
  );
};

PersonCell.defaultProps = {
  chevron: true,
};
