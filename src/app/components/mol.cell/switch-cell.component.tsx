import * as React from 'react';
import { Platform, StyleSheet, Switch, Text } from 'react-native';
import { Cell, Color, H3, HBox } from '@atomic';

export interface SwitchCellProps {
  active?: boolean;
  text: string;
  onChange: (active: boolean) => void;
}

interface SwitchCellState {
  active: boolean;
}

const isAndroid: boolean = Platform.OS === 'android';
const ActiveBackground = Color.BlueDark;

export class SwitchCell extends React.PureComponent<
  SwitchCellProps,
  SwitchCellState
> {
  constructor(props) {
    super(props);
    this.state = { active: props.active || false };
  }

  render() {
    const thumbTintColor = isAndroid
      ? this.state.active
        ? Color.Primary
        : Color.Gray
      : null;

    return (
      <Cell leftGutter={false} rightGutter={false}>
        <HBox>
          <HBox.Item>
            <Text style={styles.title}>{this.props.text}</Text>
          </HBox.Item>
          <HBox.Item hAlign="flex-end">
            <Switch
              onValueChange={this.handleChange}
              value={this.state.active}
              onTintColor={ActiveBackground}
              thumbTintColor={thumbTintColor}
              tintColor={Color.GrayLight}
              thumbColor={this.state.active ? ActiveBackground : '#f4f3f4'}
            />
          </HBox.Item>
        </HBox>
      </Cell>
    );
  }

  private handleChange = (value: boolean) => {
    this.props.onChange(value);
    this.setState({ active: value });
  };
}

const styles = StyleSheet.create({
  title: {
    fontSize: 16,
    color: '#404040',
    lineHeight: 16,
    fontWeight: 'bold',
  },
});
