import * as React from 'react';
import { H3, Root, Shimmer } from '@atomic';
import { Cell } from '@atomic/mol.cell';
import { CellShimmerProps } from './cell-shimmer.model';

export interface ListCellProps {
  text: string;
  leftThumb?: any;
  isAlert?: boolean;
  iconLarge?: boolean;
  rightThumb?: any;
  onTap?: () => any;
}

const CellShimmer = () => (
  <Cell>
    <Shimmer height={20} />
  </Cell>
);

export const ListCellShimmer: React.SFC<CellShimmerProps> = props => (
  <Root>
    {new Array(props.count || 1).fill(null).map((_, i) => <CellShimmer key={i} />)}
  </Root>
);

export const ListCell: React.SFC<ListCellProps> = props => (
  <Cell
    onTap={props.onTap}
    isAlert={props.isAlert}
    iconLarge={props.iconLarge}
    rightThumb={props.rightThumb}
    leftThumb={props.leftThumb}
  >
    <H3>{props.text}</H3>
  </Cell>
);
