import * as React from 'react';
import { ImagePropertiesSourceOptions, TouchableOpacity } from 'react-native';
import { Watermark, WatermarkType } from '@app/components/atm.watermark';
import { HBox } from '@atomic';
import { CellShimmerProps } from './cell-shimmer.model';
import { BodyGradient, CaptionGradient, H4Gradient } from './gradient-cell.component.style';
import { HabitCellShimmerStyled, HabitCellStyled, HabitGradientCellStyled } from './habit-cell.component.style';

export interface HabitCellProps extends Readonly<{ children?: React.ReactNode }> {
  title: string;
  type: WatermarkType;
  tag: string;
  times?: string[];
  hideCheck?: boolean;
  leftThumb?: ImagePropertiesSourceOptions;
  onTap?: (id: string) => any;
  inactive?: boolean;
  disabled?: boolean;
  status?: string;
  id: any;
}

const HabitCellSingleShimmer = () => (
  <HabitCellStyled>
    <HabitCellShimmerStyled height={80} />
  </HabitCellStyled>
);

export const HabitCellShimmer = (props: CellShimmerProps) => (
  <>
    {new Array(props.count || 1).fill(null).map((_, i) => <HabitCellSingleShimmer key={i} />)}
  </>
);

export const HabitCell: React.SFC<HabitCellProps> = (props: HabitCellProps) => {
  const handleTap = () => {
    if (props.onTap) {
      props.onTap(props.id);
    }
  };

  return (
    <TouchableOpacity onPress={handleTap} disabled={props.disabled} activeOpacity={0.7}>
      <HabitCellStyled>
        <HabitGradientCellStyled {...props} type={props.type === WatermarkType.Medication ? 'primary' : 'secondary'}>
          <HBox>
            <HBox.Item>
              <H4Gradient numberOfLines={2} ellipsizeMode='tail'>{props.title}</H4Gradient>
              <HBox>
                {props.times && props.times.map(time => (
                  <React.Fragment key={time}>
                    <HBox.Item wrap={true}>
                      <BodyGradient>{time}</BodyGradient>
                    </HBox.Item>
                    <HBox.Separator />
                  </React.Fragment>
                ))}
              </HBox>
              {props.status && (
                <HBox>
                  <HBox.Item wrap={true}>
                    <BodyGradient>{props.status}</BodyGradient>
                  </HBox.Item>
                  <HBox.Separator />
                </HBox>
              )}
            </HBox.Item>
            <HBox.Separator />
          </HBox>
        </HabitGradientCellStyled>
        {/* TODO: Activate this area if habits can be marked from here */}
        {/* <HabitCellCheckAreaStyled>
          {!props.hideCheck && <BigCheckGroup>
            {props.times && props.times.map(time => <BigCheckGroup.Item key={time} value={time} />)}
          </BigCheckGroup>}
        </HabitCellCheckAreaStyled> */}
      </HabitCellStyled>
    </TouchableOpacity>
  );
};
