import * as React from 'react';
import { Image } from 'react-native';
import { BigCheck } from '@app/components/atm.big-check/big-check.component';
import { QuestionGroupFragment } from '@app/data/graphql';
import { Asset, Caption, H3, HBox, Shimmer, VBox } from '@atomic';
import { CellBackgroundStyled } from '@atomic/mol.cell/cell.component.style';
import { CellShimmerProps } from './cell-shimmer.model';
import { CellStyled } from './cell.component.style';

interface QuestionGroupCellProps {
  text: string;
  onTap?: () => void;
  item?: QuestionGroupFragment;
  minimum?: number;
  answered?: number;
}

const SingleQuestionnaireCellShimmer = () => (
  <>
    <CellStyled>
      <CellBackgroundStyled>
        <HBox>
          <HBox.Separator />
          <HBox.Item wrap={true}>
            <BigCheck type='primary' progress={0} />
          </HBox.Item>
          <HBox.Separator />
          <HBox.Item>
            <Shimmer height={16} width='60%' />
            <HBox>
              <HBox.Item wrap={true}>
                <Shimmer height={12} width={56} />
              </HBox.Item>
              <HBox.Separator />
              <HBox.Item wrap={true}>
                <Shimmer height={12} width={64} />
              </HBox.Item>
            </HBox>
          </HBox.Item>
          <HBox.Separator />
        </HBox>
      </CellBackgroundStyled>
    </CellStyled>
  </>
);

export const QuestionnaireCellShimmer: React.SFC<CellShimmerProps> = props => (
  <>
    {new Array(props.count || 1).fill(null).map((_, i) => <SingleQuestionnaireCellShimmer key={i} />)}
  </>
);

export const QuestionGroupCell: React.SFC<QuestionGroupCellProps> = props => {

  const handleItemTap = () => {
    if (props.onTap) {
      props.onTap();
    }
  };

  const progress: number =
    props.minimum && props.answered ? props.answered / props.minimum : 0;

  return (
    <>
      <CellStyled onPress={handleItemTap}>
        <CellBackgroundStyled>
          <HBox>
            <HBox.Separator />
            <HBox.Item wrap={true}>
              <BigCheck type="primary" progress={progress} />
            </HBox.Item>
            <HBox.Separator />
            <HBox.Item vAlign='center'>
              <VBox noGutter={true}>
                <H3>{props.text}</H3>
                {props.minimum && (
                  <HBox>
                    <HBox.Item vAlign='flex-end' hAlign='center' wrap={true}>
                      <Caption>Mínimo: {props.minimum || '--'}</Caption>
                    </HBox.Item>
                    <HBox.Separator />
                    <HBox.Item vAlign='flex-end' hAlign='center' wrap={true}>
                      <Caption>Respondidas: {props.answered || '--'}</Caption>
                    </HBox.Item>
                  </HBox>
                )}
              </VBox>
            </HBox.Item>
            <HBox.Separator />
            <HBox.Item  vAlign='center' wrap={true}>
              <Image source={Asset.Icon.Atomic.ChevronRight} />
            </HBox.Item>
            <HBox.Separator />
          </HBox>
        </CellBackgroundStyled>
      </CellStyled>
    </>
  );
};
