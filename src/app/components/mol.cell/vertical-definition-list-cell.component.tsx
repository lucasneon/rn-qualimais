import { Caption, H3, HBox, VBox } from '@atomic';
import * as React from 'react';

interface VerticalDefinitionListCellProps {
  topic: string;
  definition: string;
}

export const VerticalDefinitionListCell: React.SFC<VerticalDefinitionListCellProps> =
  (props: VerticalDefinitionListCellProps) => (
    <HBox.Item hAlign='center'>
      <VBox hAlign='center' noGutter={true}>
        <Caption>
          {props.topic}
        </Caption>
        <H3>
          {props.definition}
        </H3>
      </VBox>
    </HBox.Item>
  );
