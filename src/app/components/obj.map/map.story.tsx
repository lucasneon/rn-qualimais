import * as React from 'react';
import { storiesOf } from '@storybook/react-native';
import MapView from 'react-native-maps';

const stories = storiesOf('Objects', module);

stories.add('Map', () => (
  <MapView style={{ height: 300 }} />
));
