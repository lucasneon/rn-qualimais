import React from 'react';
import { ButtonProps, DividerGray, PrimaryButton, SecondaryButton, VBox, VSeparator } from '@atomic';

export interface ButtonFooterProps {
  primary?: ButtonProps;
  secondary?: ButtonProps;
}

export const ButtonFooter: React.SFC<ButtonFooterProps> = props => (
  <>
    <DividerGray />
    <VSeparator />
    <VBox>
      {props.primary && <PrimaryButton expanded={true} {...props.primary} />}
      {props.secondary && <VSeparator />}
      {props.secondary && <SecondaryButton expanded={true} {...props.secondary} />}
    </VBox>
    <VSeparator />
  </>
);
