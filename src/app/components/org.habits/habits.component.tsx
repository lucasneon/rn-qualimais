import * as React from 'react';
import { FlatList, ListRenderItemInfo } from 'react-native';
import {
  HabitCell,
  HabitCellProps,
  HabitCellShimmer,
} from '@app/components/mol.cell';
import {
  Placeholder,
  PlaceholderSelector,
} from '@app/components/mol.placeholder';
import { CarePlanScheduleFragment, HabitFragment } from '@app/data/graphql';
import { AppError } from '@app/model';
import { HabitsWatermarkMap } from '@app/modules/habit';
import { HabitStrings } from '@app/modules/habit/habit.strings';
import { DateUtils } from '@app/utils';
import { Asset, LoadingState, VSeparator } from '@atomic';
import { HabitsStrings } from './habits.strings';
import { ActivateHabit } from '@app/config/tokens.config';
import { Container } from 'typedi';

export interface HabitsProps {
  loading: boolean;
  error: AppError;
  activeHabits: HabitFragment[];
  onHabitTap: (habit: HabitFragment) => void;
  onActivateNewHabitsTap: () => void;
}

const EmptyPlaceholder = () => (
  <Placeholder
    image={Asset.Icon.Custom.Habit}
    message={HabitsStrings.Empty.Description}
    small={true}
  />
);

export class Habits extends React.PureComponent<HabitsProps> {
  private activateHabit: boolean = Container.get(ActivateHabit);

  render() {
    const habits = this.props.activeHabits;
    const error = this.props.error;
    const loading: boolean = this.props.loading;

    return (
      <LoadingState
        loading={loading}
        data={!loading && !!habits}
        error={!!error}>
        <LoadingState.Shimmer>
          <HabitCellShimmer count={1} />
        </LoadingState.Shimmer>
        <LoadingState.ErrorPlaceholder>
          <PlaceholderSelector small={true} error={error} />
        </LoadingState.ErrorPlaceholder>
        {habits && (
          <>
            <FlatList
              ListEmptyComponent={EmptyPlaceholder}
              data={habits}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
            />
            {habits.length > 0 && <VSeparator />}
          </>
        )}
      </LoadingState>
    );
  }

  private renderItem = (
    itemInfo: ListRenderItemInfo<HabitFragment>,
  ): React.ReactElement<HabitCellProps> => {
    const item: HabitFragment = itemInfo.item;
    const handleTap = () => this.props.onHabitTap(item);

    const nextSchedules: CarePlanScheduleFragment[] =
      item.carePlanItem.nextSchedules;
    const next: Date =
      nextSchedules && nextSchedules.length > 0
        ? new Date(nextSchedules[0].limitDate)
        : null;
    const isSameDay: boolean = next && next.getDay() === new Date().getDay();

    return (
      <HabitCell
        title={item.description}
        id={item.id}
        type={HabitsWatermarkMap[item.type.area]}
        times={next && [DateUtils.Format.presentation(next, isSameDay)]}
        tag={HabitStrings.Habit}
        onTap={handleTap}
      />
    );
  };

  private keyExtractor = (item: HabitFragment) => item.id;
}
