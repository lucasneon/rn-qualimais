import * as React from 'react';
import { Platform, TouchableNativeFeedback, TouchableOpacity } from 'react-native';
import { CardStyled } from './card.component.style';

interface CardProps {
  children: any;
  onTap?: () => void;
}

export const Card: React.SFC<CardProps> = props => {

  const Touchable: any =
    Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

  return (
    <Touchable onPress={props.onTap} disabled={!props.onTap}>
      <CardStyled>
        {props.children}
      </CardStyled>
    </Touchable>
  );
};
