
import styled from 'styled-components/native';
import { Border, Color, Spacing } from '@atomic';

export const CardStyled = styled.View`
  border-radius: ${Border.RadiusLarge};
  shadow-color: ${Color.Black};
  shadow-opacity: 0.2;
  shadow-radius: 3;
  shadow-offset: 0 2px;
  elevation: 4;
  background-color: white;
  padding-vertical: ${Spacing.Large};
  padding-horizontal: ${Spacing.Large};
`;
