import * as React from 'react';
import { Platform, TouchableNativeFeedback, TouchableOpacity } from 'react-native';
import { BigCheck } from '@app/components/atm.big-check';
import { HBox } from '@atomic';
import { FormFieldContexState, FormFieldContext } from '@atomic/obj.form';
import { RadioListItemStyled } from './radio-list.component.style';

interface RadioListItemProps {
  source?: any;
  value: any;
  children?: any;
  disable?: boolean;
}

export const RadioListItem: React.SFC<RadioListItemProps> = (props: RadioListItemProps) => {
  const Touchable: any = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

  return (
    <FormFieldContext.Consumer>
      {(consumer: FormFieldContexState) => {
        const handleTouch = () => props.disable ? null : consumer.onValueChange(props.value, undefined);

        return (
          <Touchable onPress={handleTouch}>
            <HBox>
              <HBox.Separator />
              <HBox.Item wrap={true} vAlign='center'>
                { props.source ?
                  <BigCheck
                    type='primary'
                    avatar={props.source}
                    value={props.value}
                    progress={(consumer.value === props.value) ? 1 : 0}
                    onValueChange={props.disable ? null : consumer.onValueChange}
                  />
                :
                  <RadioListItemStyled id={props.value} />
                }
              </HBox.Item>
              <HBox.Separator />
              <HBox.Item vAlign='center'>
              {props.children}
              </HBox.Item>
            </HBox>
          </Touchable>
        );
      }}
    </FormFieldContext.Consumer>
  );
};
