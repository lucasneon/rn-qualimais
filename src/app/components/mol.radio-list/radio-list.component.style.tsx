import styled from 'styled-components/native';

import { RadioField, Spacing } from '@atomic';

export const RadioListItemStyled = styled(RadioField)`
  margin-top: ${Spacing.Small};
`;
