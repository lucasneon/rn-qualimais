import styled from 'styled-components/native';

export const WaveImageStyled = styled.Image`
  width: 100%;
  resize-mode: stretch;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;
