import * as React from 'react';
import { Asset } from '@atomic';
import { EmitterSubscription, Keyboard } from 'react-native';
import { WaveImageStyled } from './wave.component.style';

interface WaveState {
  keyboardOpen: boolean;
}
export class Wave extends React.Component<any, WaveState> {

  private keyboardDidShowListener: EmitterSubscription;
  private keyboardDidHideListener: EmitterSubscription;

  constructor(props) {
    super(props);

    this.state = {
      keyboardOpen: false,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.handleKeyboardShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.handleKeyboardHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  render() {
    return (
      <WaveImageStyled source={Asset.Background.Footer} style={{opacity: this.state.keyboardOpen ? 0 : 1}}/>
    );
  }

  handleKeyboardShow = () => {
    this.setState({keyboardOpen: true});
  }

  handleKeyboardHide = () => {
    this.setState({keyboardOpen: false});
  }
}
