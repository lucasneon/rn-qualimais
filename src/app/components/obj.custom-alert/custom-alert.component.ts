import { AlertBuilder } from '@app/components/atm.alert-builder';

export const CustomAlerts = {
  underConstruction(callback?: () => void) {
    const title = 'Em construção';
    const message = 'Estamos trabalhando nesta funcionalidade.';

    new AlertBuilder()
      .withTitle(title)
      .withMessage(message)
      .withButton({ text: 'Ok', onPress: callback })
      .show();
  },

  additionConfirmation(handler: () => void) {
    const title = 'Aviso';
    const message = 'Após o envio os dados não poderão ser editados';

    new AlertBuilder()
      .withTitle(title)
      .withMessage(message)
      .withButton({ text: 'Revisar', style: 'cancel' })
      .withButton({ text: 'Enviar', onPress: handler })
      .show();
  },
};
