import * as React from 'react';
import * as Animatable from 'react-native-animatable';
import { BigCheck } from '@app/components/atm.big-check/big-check.component';
import { HBox, VBox } from '@atomic';
import { BigCheckItemBody } from './big-check-group.component.style';

interface BigCheckItemProps {
  showLabel?: boolean;
  value: string;
  children?: any;
}
interface BigCheckGroupContexState {
  values: { [value: string]: number };
  onValueChange: (value: string, progress: number) => void;
}

const BigCheckGroupContext = React.createContext<BigCheckGroupContexState>(null);

const BigCheckItem: React.SFC<BigCheckItemProps> = (props: BigCheckItemProps) => {
  return (
    <BigCheckGroupContext.Consumer>
      {(consumer: BigCheckGroupState) => {
        return (
          <VBox noGutter={true} hAlign='center' >
            <BigCheck
              value={props.value}
              type='secondary'
              progress={consumer.values[props.value]}
              onValueChange={consumer.onValueChange}
            />
            {props.showLabel && <BigCheckItemBody>{props.value}</BigCheckItemBody>}
          </VBox>
        );
      }}
    </BigCheckGroupContext.Consumer>
  );
};

BigCheckItem.defaultProps = {
  showLabel: true,
};

interface BigCheckGroupProps {
  children?: any;
}
interface BigCheckGroupState extends BigCheckGroupContexState {
  animation: 'fadeInLeft' | 'fadeOut' | 'bounceOut' | '';
  generalProgress?: number;
  display?: boolean;
}

const BaseAnimationDuration = 300;
const AnimationDelay = 100;

export class BigCheckGroup extends React.Component<BigCheckGroupProps, BigCheckGroupState> {

  static readonly Item = BigCheckItem;

  constructor(props) {
    super(props);

    this.state = {
      values: {},
      onValueChange: this.handleValueChange,
      animation: '',
      generalProgress: 0,
      display: true,
    };
  }

  handleValueChange = (value: string, progress: number) => {
    if (progress > 0) {
      progress = 0;
    } else {
      progress = 1;
    }
    const values = { ...this.state.values };
    values[value] = progress;

    // Compute general progress
    const generalProgress = this.computeGeneralProgress(values);
    this.setState({
      values,
      generalProgress,
      animation: this.state.animation === 'fadeInLeft' ? 'fadeOut' : 'fadeInLeft',
    });

    this.toggleDisplay();
  }

  toggleChecks = () => {
    this.setState({
      animation: this.state.animation === 'fadeInLeft' ? 'fadeOut' : 'fadeInLeft',
    });
    this.toggleDisplay();
  }

  computeGeneralProgress(values: { [x: string]: number }) {
    return Object.keys(values).filter((key: string) => {
      return values[key];
    }).length / React.Children.count(this.props.children);
  }

  mapChildren() {
    const childrenCount = React.Children.count(this.props.children);
    if (childrenCount === 1) {
      return React.cloneElement(this.props.children[0], { showLabel: false });
    }

    return React.Children.map(this.props.children, (child: any, index: number) => {

      let animation = this.state.animation;

      if (this.state.animation === 'fadeOut' && this.state.values[child.props.value]) {
        animation = 'bounceOut';
      }

      return (
        <>
          <HBox.Separator />
          <HBox.Item wrap={true}>
            <Animatable.View
              animation={animation}
              duration={BaseAnimationDuration + (index * AnimationDelay)}
              style={{ opacity: 0 }}
            >
              {child}
            </Animatable.View>
          </HBox.Item>
        </>
      );
    });
  }

  componentDidMount() {
    this.setState({ display: false });
  }

  toggleDisplay() {
    if (this.state.display === true) {
      setTimeout(() => this.setState({
        display: !this.state.display,
      }), BaseAnimationDuration + AnimationDelay * this.props.children.length);
    } else {
      this.setState({
        display: !this.state.display,
      });
    }
  }

  render() {
    const children = this.mapChildren();

    const childrenCount = React.Children.count(this.props.children);
    return (
      <BigCheckGroupContext.Provider value={this.state}>
        <HBox>
          {childrenCount > 1 && <HBox.Item wrap={true}>
            <BigCheck
              type='secondary'
              progress={this.state.generalProgress}
              onValueChange={this.toggleChecks}
            />
          </HBox.Item>}
          {this.state.display && children}
          <HBox.Item />
        </HBox>
      </BigCheckGroupContext.Provider>
    );
  }
}
