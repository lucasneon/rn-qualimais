import * as React from 'react';
import { Animated, ImagePropertiesSourceOptions, TouchableWithoutFeedback } from 'react-native';
import { Avatar } from '@app/components/atm.avatar';
import { Asset } from '@atomic';
import {
  BigCheckBackgroundClipStyled, BigCheckBackgroundStyled, BigCheckBackgroundWrapperStyled, BigCheckIconStyled,
  BigCheckStyled
} from './big-check.component.style';

export interface BigCheckProps  {
  type: 'primary' | 'secondary';
  progress?: number;
  value?: any;
  avatar?: ImagePropertiesSourceOptions;

  onValueChange?: (value: any, progress: number) => void;
}

interface BigCheckState {
  animatedHeight: Animated.Value;
}

export class BigCheck extends React.Component<BigCheckProps, BigCheckState> {
  static defaultProps = {
    allowUncheck: true,
    progress: 0,
  };

  constructor(props) {
    super(props);

    this.state = {
      animatedHeight: new Animated.Value(props.progress),
    };
  }

  componentDidUpdate(prevProps: BigCheckProps) {
    if (prevProps.progress !== this.props.progress) {
      Animated.timing(this.state.animatedHeight, {
        toValue: this.props.progress,
        duration: 300,
      }).start();
    }
  }

  handleCheckPress = () => {
    if (this.props.onValueChange) {
      this.props.onValueChange(this.props.value, this.props.progress);
    }
  }

  render() {
    const height = this.state.animatedHeight.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 36],
    });
    return (
      <TouchableWithoutFeedback onPress={this.handleCheckPress}>
        <BigCheckBackgroundWrapperStyled type={this.props.type}>
          <BigCheckBackgroundClipStyled>
            {this.props.avatar && <Avatar source={this.props.avatar} />}
            <BigCheckBackgroundStyled type={this.props.type} style={{height}}/>
          </BigCheckBackgroundClipStyled>
          <BigCheckStyled>
            <BigCheckIconStyled
              type={this.props.type}
              source={Asset.Icon.Atomic.Check}
              progress={this.props.progress}
            />
          </BigCheckStyled>
        </BigCheckBackgroundWrapperStyled>
      </TouchableWithoutFeedback>
    );
  }
}
