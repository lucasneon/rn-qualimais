import styled from 'styled-components/native';
import { Body, Color, Spacing } from '@atomic';

export const BigCheckItemBody = styled(Body)`
  color: ${Color.White};
  margin-top: ${Spacing.XSmall};
  text-shadow-color: ${Color.GrayXDark};
  text-shadow-radius: 3;
  text-shadow-offset: 0 4px;
`;
