import { Animated } from 'react-native';
import styled from 'styled-components/native';
import { Color } from '@atomic';
import { BigCheckProps } from './big-check.component';

const BIG_CHECK_SIZE = 36;

const ShadowProperties = `
  shadow-color: ${Color.Black};
  shadow-opacity: 0.3;
  shadow-radius: 1;
  shadow-offset: 0 2px;
  elevation: 7;
`;

export const BigCheckBackgroundWrapperStyled = styled.View`
  background-color: ${Color.GrayLight};
  width: ${BIG_CHECK_SIZE};
  height: ${BIG_CHECK_SIZE};
  border-radius: ${BIG_CHECK_SIZE / 2};
  ${(props: BigCheckProps) =>  props.type === 'secondary' && ShadowProperties}
`;

export const BigCheckBackgroundClipStyled = styled.View`
  width: ${BIG_CHECK_SIZE};
  height: ${BIG_CHECK_SIZE};
  border-radius: ${BIG_CHECK_SIZE / 2};
  overflow: hidden;
  justify-content: flex-end;
`;

export const BigCheckBackgroundStyled = styled(Animated.View)`
  position: absolute;
  width: ${BIG_CHECK_SIZE};
  background-color: ${(props: BigCheckProps) => props.type === 'primary' ? Color.Primary : Color.Success };
  opacity: ${(props: BigCheckProps) => props.type === 'primary' ? 0.7 : 1};
`;

export const BigCheckStyled = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  width: ${BIG_CHECK_SIZE};
  height: ${BIG_CHECK_SIZE};
  justify-content: center;
  align-items: center;
`;

export const BigCheckIconStyled = styled.Image`
  tint-color: ${Color.White};
`;
