export interface Address {
  streetAndNumber: string;
  area: string; // district city and state
  zipCode: string;
}
