export interface CarePlanAppointmentParams {
  carePlanCode: string;
  itemCode: string;
  shceduleCode: string;
}

export enum CarePlanStatus {
  NeedsAnswers = 'NeedsAnswers',
  NeedsAnswersPrincipal = 'NeedsAnswersPrincipal',
  NeedsApproval = 'NeedsApproval',
}

export interface CarePlanSummary {
  code: string;
  progress: number;
  status?: CarePlanStatus;
}
