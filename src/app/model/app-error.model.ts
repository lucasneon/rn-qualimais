export enum AppErrorType {
  Camera = 'Camera',
  Maintenance = 'Maintenance',
  NoConnection = 'NoConnection',
  Unknown = 'Unknown',
  Internal = 'Internal',
}

export interface AppError {
  title?: string;
  message: string;
  type?: AppErrorType;
  action?: string;
  errorDetails?: string;
}

export const UnknownError: AppError = {
  title: 'Erro',
  message: 'Tivemos um erro inesperado. Por favor, tente novamente!',
  type: AppErrorType.Unknown,
  action: 'Tentar novamente',
};

export const NoConnectionError: AppError = {
  title: 'Sem conexão',
  message: 'Você está sem conexão. Por favor, verifique o sinal de sua internet e tente novamente.',
  type: AppErrorType.NoConnection,
  action: 'Tentar novamente',
};

export const MaintenanceError: AppError = {
  title: 'Em manutenção',
  message: 'Estamos em manutenção temporariamente. Por favor, tente novamente em alguns minutos.',
  type: AppErrorType.Maintenance,
};

export const InternalServerError: AppError = {
  title: 'Erro',
  message: 'Erro interno do servidor',
  type: AppErrorType.Internal,
};
