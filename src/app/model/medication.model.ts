export interface Medication {
  medicationId: string;
  medicineName: string;
  dailySchedules: string[]; // format: HH:mm:ss.sssZ
  start: Date;
  end?: Date;
}
