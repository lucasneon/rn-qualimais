
export enum NavigationDestination {
  AppointmentDetails = 'AppointmentDetails',
  AppointmentReview = 'AppointmentReview',
  Chat = 'Chat',
  MedicationDetails = 'MedicationDetails',
}

export interface NavigationRequest {
  destination: NavigationDestination;
  data?: PushNotificationData;
}

export interface AppointmentDetailsPush {
  scheduleId: string;
  carePlanItemCode: string;
  carePlanCode: string;
}

export interface AppointmentReviewPush {
  AppointmentData: AppointmentDetailsPush;
  doctorId: string;
  specialtyCode: string;
}

export interface ChatPush {
  chatId: string;
}

export interface MedicationDetailsPush {
  medicationId: string;
  medicationName: string;
}

export type PushNotificationData =  AppointmentDetailsPush | AppointmentReviewPush | ChatPush | MedicationDetailsPush;
