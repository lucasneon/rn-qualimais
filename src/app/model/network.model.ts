export class NetworkParam {
    uf?: string;
    cityCode?: number;
    districtName?: string;
    typeCode?: number;
    specialityPublicationCode?: number;
    practitionerName?: string;
    latitude?: number;
    longitude?: number;
    maxDistance?: number;
}

export class NetworkGamaParam {
    networkCode?: number;
    bookletCode?: number;
    specialityDesc?: string;
    latitude?: number;
    longitude?: number;
    maxDistance?: number;
    uf?: string;
    city?: string;
    district?: string;
}