import moment from 'moment';
import { BaseUserRequest } from './common.entity';
import {
  BeneficiaryInfoResponse,
  ChangeBeneficiaryInfoRequest,
} from './beneficiary-info.entity';

const BrazilianCountryCode = '55';

export const BeneficiaryInfoMapper = {
  // TODO ajustar aqui para deixar dinamico
  mapResponse(beneficiaryRes: BeneficiaryInfoResponse): BeneficiaryInfoModel {
    const beneficiaryInfo: BeneficiaryInfoModel = {
      name: beneficiaryRes.nomeBeneficiario,
      idHealthCareCard: beneficiaryRes.carteirinha,
      insurerId: '55879', //beneficiaryRes.idPlano,
      beneficiaryCode: beneficiaryRes.codigoBeneficiario,
      cpf: beneficiaryRes.cpf,
      birthDate: moment(beneficiaryRes.dataNascimento, 'DD/MM/YYYY').format(
        'YYYY-MM-DD',
      ),
      email: beneficiaryRes.email,
      phone: mountPhone(beneficiaryRes),
      maritalStatus: beneficiaryRes.estadoCivil,
      gender: beneficiaryRes.sexo,
      address: {
        state: beneficiaryRes.estado,
        city: beneficiaryRes.cidade,
        district: beneficiaryRes.bairro,
        cep: beneficiaryRes.cep ? beneficiaryRes.cep.toString() : null,
        street: beneficiaryRes.logradouro,
        addressNumber: beneficiaryRes.numeroLogradouro,
        complement: beneficiaryRes.complemento,
      },
      planData: {
        type: beneficiaryRes.tipoContratacao,
        ansRegister: beneficiaryRes.numeroRegistroAns,
        cns: beneficiaryRes.cns && beneficiaryRes.cns.toString(),
        contractedAt: new Date(beneficiaryRes.dataContratacao),
        regulation: beneficiaryRes.regulacaoPlano,
        comprehensiveness: beneficiaryRes.abrangencia,
        accommodation: beneficiaryRes.acomodacao,
        startValidity: moment(
          beneficiaryRes.dataVigenciaPlano,
          'DD/MM/YYYY',
        ).toDate(),
        namePlan: beneficiaryRes.nomePlano,
        nameNetwork: beneficiaryRes.nomeRede,
        contractor: beneficiaryRes.nomeContratante,
        segmentation: beneficiaryRes.segmentacao,
      },
      codePlan: beneficiaryRes.idPlano,
      operatorCode: beneficiaryRes.codigoOperadora,
      stipulatingCode: beneficiaryRes.codigoEstipulante,
    };

    return beneficiaryInfo;
  },

  mapRequest(
    beneficiaryInfo: BeneficiaryInfoModel,
    codes: BaseUserRequest,
  ): ChangeBeneficiaryInfoRequest {
    return {
      codigoBeneficiario: beneficiaryInfo.beneficiaryCode,
      email: beneficiaryInfo.email,
      codigoOperadora: codes.codigoOperadora,
      codigoSistema: codes.codigoSistema,
      cns: beneficiaryInfo.planData.cns,
      estadoCivil: beneficiaryInfo.maritalStatus,
      cep: beneficiaryInfo.address.cep,
      logradouro: beneficiaryInfo.address.street,
      numeroLogradouro: beneficiaryInfo.address.addressNumber,
      bairro: beneficiaryInfo.address.district,
      cidade: beneficiaryInfo.address.city,
      estado: beneficiaryInfo.address.state,
      sexo: beneficiaryInfo.gender,
      complemento: beneficiaryInfo.address.complement,
      ...parsePhone(beneficiaryInfo.phone),
    };
  },
};

function parsePhone(phone: string) {
  return {
    ddd: phone ? +phone.substr(2, 2) : null,
    telefone: phone ? phone.substr(4) : null,
  };
}

function mountPhone(beneficiaryRes: BeneficiaryInfoResponse) {
  return beneficiaryRes.ddd && beneficiaryRes.telefone
    ? BrazilianCountryCode +
        beneficiaryRes.ddd.toString() +
        beneficiaryRes.telefone.toString()
    : null;
}

export interface BeneficiaryInfoModel {
  name: string;
  idHealthCareCard: string;
  beneficiaryCode?: string;
  insurerId?: string;
  cpf?: string;
  birthDate: string;
  email?: string;
  phone?: string;
  maritalStatus?: string;
  gender?: string;
  address?: AddressModel;
  planData?: PlanDataModel;
  codePlan?: string;
  operatorCode?: string;
  stipulatingCode?: string;
  token?: string;
  city?: string;
  state?: string;
  dependant?: any;
}

export interface AddressModel {
  latitude?: number;
  longitude?: number;
  state: string;
  city: string;
  district?: string;
  cep?: string;
  street?: string;
  addressNumber?: string;
  complement?: string;
  instructions?: string;
}

export interface PlanDataModel {
  type?: string;
  ansRegister?: string;
  cns?: string;
  contractedAt?: Date;
  regulation?: string;
  comprehensiveness?: string;
  accommodation?: string;
  startValidity?: Date;
  namePlan?: string;
  nameNetwork?: string;
  contractor?: string;
  segmentation?: string;
}
