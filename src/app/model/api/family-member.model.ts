export interface FamilyMemberResponse {
  carteirinha: number;
  codigoBeneficiario: number;
  nomeBeneficiario: string;
  vinculo: 'DEPENDENTE' | 'TITULAR';
}
