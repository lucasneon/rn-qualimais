export interface LoginInputModel {
  user: string;
  password: string;
}

export interface AuthenticationModel extends FamilyMemberModel {
  token: string;
  idHealthCareCard: string;
  cpf?: string;
  email?: string;
  birthDate: Date;
  city: string;
  state: string;
  isRegisteredInBiometricService?: boolean;
  mustChangePassword?: boolean;
  dependants?: AuthenticationModel[];
  consentTerm?: string;
  hasCarePlan?: boolean;
  stipulating?: number;
}

export interface FamilyMemberModel {
  beneficiaryCode: string;
  name: string;
  dependant: boolean;
}

export interface AcceptTermOfUseInputModel {
  codeBenefiary?: string;
  use: string;
  consentTerm?: string;
}
