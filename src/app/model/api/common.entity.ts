export interface GeneralTypeRemote {
  codigo?: number;
  chave: string;
  descricao?: string;
}

export interface MessageResponse {
  codigo: string;
  mensagemUsuario: string;
  mensagemDesenvolvedor: string;
}

export interface BaseUserRequest {
  codigoOperadora: string;
  codigoSistema: string;
  codigoRede?: string;
}

export interface BaseCarePlanRequest {
  codigoOperadora: string;
  codigoEstipulante: string;
  codigoBeneficiario: string;
}
