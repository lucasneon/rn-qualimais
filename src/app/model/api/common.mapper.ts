import moment from 'moment';
import { GeneralTypeModel, SexModel } from '../../utils/prontlife/common.model';
import { GeneralTypeRemote } from './common.entity';

export const GeneralTypeMapper = {
  map(remote: GeneralTypeRemote): GeneralTypeModel {
    return {
      id: remote.codigo && remote.codigo.toString(),
      description: remote.descricao,
    };
  },
};

export const GamaSexMap: { [key in string]: SexModel } = {
  MASCULINO: SexModel.Male,
  FEMININO: SexModel.Female,
  AMBOS: SexModel.Both,
};

export interface DateFormatOptions {
  includeHours?: boolean;
  considerOnlyDay?: boolean;
  dayHourSeparator?: string;
}

export const GamaDateFormatter = {
  format(date: Date | string, options?: DateFormatOptions) {
    if (!options) {
      return moment(date).format('YYYY-MM-DDTHH:mm:ss');
    }

    const separator: string = options.dayHourSeparator || '';
    const format: string =
      'YYYY-MM-DD' + (options.includeHours ? `${separator}HH:mm` : '');

    return options.considerOnlyDay
      ? moment.utc(date).format(format)
      : moment(date).local().format(format);
  },
};
