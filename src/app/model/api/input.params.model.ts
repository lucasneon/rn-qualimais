import { UserData } from '@app/modules/authentication/api/login/beneficiary.service';

export type InputParams<T> = UserData & T;
