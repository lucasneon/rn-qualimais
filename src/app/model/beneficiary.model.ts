export interface Beneficiary {
  token?: string;
  code?: string;
  birthDate?: Date;
  city?: string;
  state?: string;
  name: string;
  isRegisteredInBiometricService?: boolean;
  dependant?: boolean;
  email?: string;
  phone?: string;
  idHealthCareCard?: string;
  gender?: string;
  hasCarePlan?: boolean;
}
