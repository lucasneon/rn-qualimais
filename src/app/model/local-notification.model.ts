import { NavigationRequest } from './navigation-destination.model';

export enum NotificationRepeatFrequency {
  Daily,
}

export interface LocalNotification {
  id?: string;
  title?: string;
  message: string;
  navigationRequest?: NavigationRequest;
  repeatType?: 'daily' | 'weekly';
}

export interface ScheduleNotificationsInput extends LocalNotification {
  dates: Date[];
}
