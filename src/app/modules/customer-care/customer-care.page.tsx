import * as React from 'react';
import { Container } from 'typedi';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import {
  GuardProps,
  Navigation,
  NavigationPageProps,
} from '@app/core/navigation';
import { GuardParams } from '@app/modules/authentication';
import {
  SchedulesPage,
  SchedulesPageProps,
} from '@app/modules/common/appointment';
import { OnboardService, OnboardServiceKey } from '@app/modules/onboard';
import { Root, VBox } from '@atomic';
import {
  AppointmentSheet,
  AppointmentSheetAction,
} from './appointment-sheet.component';
import { ChatPage } from './chat.page';
import { CustomerCareStrings } from './customer-care.strings';

import { GpsReview } from './gps-review.component';
import { ActiveNetworkSearch } from '@app/config';
import { EmergencyCarePage, EmergencyCarePageProps } from './emergency-care';
import {
  BeneficiaryProvider,
  BeneficiaryProviderToken,
  CarePlanProvider,
  CarePlanProviderToken,
  LocationProvider,
} from '@app/services';
import { VoucherPage } from '../voucher/voucher.page';
import { WebViewPage } from '@app/modules/common/webview.page';
import {
  Alert,
  FlatList,
  Image,
  Linking,
  PermissionsAndroid,
  ScrollViewComponent,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import { Platform } from '@atomic/obj.platform';

import IconSaude from '@assets/icons/atendimento/grid_icon_saude.png';
import IconAgendamento from '@assets/icons/atendimento/agendamento-icon.png';
import IconChat from '@assets/icons/atendimento/chat-icon.png';
import IconFarmacia from '@assets/icons/atendimento/famarcia-icon.png';
import IconTelemedicina from '@assets/icons/atendimento/telemedicina-icon.png';
import IconVoucher from '@assets/icons/atendimento/voucher-icon.png';
import { fetchInvitePatient } from './api-Telemedicine/telemedicine.service';
import { Subscribe } from 'unstated';
import { GeoCoordinates } from '@app/model';

export interface CustomerCarePageProps extends GuardProps<GuardParams> {
  componentId: string;
  review?: boolean;
}

interface CustomerCarePageState {
  appointmentSheetVisible: boolean;
  reviewVisible: boolean;
  urlTelemedicine: string;
  shouldAnswerQuestionnaire: boolean;
  needAnswerPrincipal: boolean;
  granted: boolean;
  location: GeoCoordinates;
}

const Strings = CustomerCareStrings;
type NavProps = NavigationPageProps<CustomerCarePageProps>;

@PageView('CustomerCare')
export class CustomerCarePage extends React.Component<
  NavProps,
  CustomerCarePageState
> {
  static options = {
    name: 'CustomerCarePage',
    topBar: {
      backButton: {
        visible: false,
      },
      rightButtons: [],
    },
  };
  private location: GeoCoordinates;

  private readonly initialState = {
    appointmentSheetVisible: false,
    reviewVisible: false,
    shouldAnswerQuestionnaire: false,
    needAnswerPrincipal: false,
    granted: null,
    location: null,
  };

  private readonly campaignProvider: CarePlanProvider = Container.get(
    CarePlanProviderToken,
  );
  // private benefHasCarePlan: boolean = this.beneficiaryProvider.state.active.hasCarePlan;
  private benefHasCarePlan: boolean = true;
  private locationProvider: LocationProvider;

  constructor(props) {
    super(props);
    this.state = this.initialState;
    Navigation.events().bindComponent(this);
  }

  async componentDidMount() {
    await this.fetchUrlTelemedicine();
    this.locationProvider.handlePlatforms().then(() => {});
  }

  fetchUrlTelemedicine = async () => {
    const res = await fetchInvitePatient();
    if (!res) {
      return;
    }
    console.warn('url telemedicine', res);
    this.setState({
      urlTelemedicine: res.link_external_login,
    });
  };

  async componentDidAppear() {
    if (this.benefHasCarePlan) {
      Container.get<OnboardService>(OnboardServiceKey).check(
        this.props.componentId,
      );
    }
    const granted = await this.isGpsGranted();
    this.setState({
      reviewVisible: this.props.navigation
        ? this.props.navigation.review
        : false,
      granted: granted,
    });
    console.warn('granted: ', granted);
    this.setState({
      granted: granted,
      shouldAnswerQuestionnaire: this.requiredAnswerQuestionnaire(),
      needAnswerPrincipal: this.campaignProvider.state.needAnswerPrincipal,
    });
  }

  async isGpsGranted() {
    return await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
  }

  showAlert() {
    Alert.alert(
      'Aviso',
      this.campaignProvider.state.needAnswerPrincipal
        ? 'O titular deve responder o questionário de saúde para liberar essa função!'
        : 'Você deve responder o questionário de saúde para liberar essa função!',
    );
  }

  requiredAnswerQuestionnaire() {
    return (
      this.campaignProvider.state.didAnswerRequiredQuestionnaires === false
    );
  }

  render() {
    return (
      <Subscribe to={[LocationProvider]}>
        {(locationProvider: LocationProvider) => {
          console.warn('location', locationProvider.state.location);
          console.warn('locationProvider', locationProvider);
          this.location = locationProvider.state.location;
          const gridMenu = [
            {
              id: '1',
              icon: IconSaude,
              label: 'Buscar Serviço de Saúde',
              onPress: this.state.shouldAnswerQuestionnaire
                ? () => this.showAlert()
                : this.handleAppointmentTap.bind(this),
              disabled: this.state.shouldAnswerQuestionnaire,
            },
            {
              id: '2',
              icon: IconAgendamento,
              label: 'Pré-agendar',
              onPress: this.state.shouldAnswerQuestionnaire
                ? () => this.showAlert()
                : this.handlePersonalDoctor.bind(this),
              disabled: this.state.shouldAnswerQuestionnaire,
            },
            {
              id: '3',
              icon: IconChat,
              label: 'Chat',
              onPress: this.state.needAnswerPrincipal
                ? () => this.showAlert()
                : this.handleTimeQTap.bind(this),
              disabled: this.state.needAnswerPrincipal,
            },
            {
              id: '4',
              icon: IconVoucher,
              label: 'Vouchers',
              onPress: this.state.shouldAnswerQuestionnaire
                ? () => this.showAlert()
                : this.handleVoucherTap.bind(this),
              disabled: this.state.shouldAnswerQuestionnaire,
            },
            {
              id: '5',
              icon: IconFarmacia,
              label: 'Remédios',
              onPress: this.state.shouldAnswerQuestionnaire
                ? () => this.showAlert()
                : this.handlePharmacyTap.bind(this),
              disabled: this.state.shouldAnswerQuestionnaire,
            },
            {
              id: '6',
              icon: IconTelemedicina,
              label: 'Telemedicina',
              onPress: this.state.shouldAnswerQuestionnaire
                ? () => this.showAlert()
                : this.handleTelemedicineTap.bind(this),
              disabled: this.state.shouldAnswerQuestionnaire,
            },
          ];

          return (
            <Root>
              <CollapsibleHeader title={Strings.Title}>
                <VBox>
                  <FlatList
                    data={gridMenu}
                    keyExtractor={item => item.id}
                    numColumns={2}
                    renderItem={({ item }) => {
                      return (
                        <TouchableOpacity
                          onPress={item.onPress}
                          style={[
                            styles.gridItem,
                            {
                              backgroundColor: item.disabled
                                ? '#7e8689'
                                : '#f58c78',
                            },
                          ]}>
                          <Image
                            source={item.icon}
                            resizeMode="contain"
                            style={styles.gridItemIcon}
                          />
                          <Text style={styles.gridItemTitle}>{item.label}</Text>
                        </TouchableOpacity>
                      );
                    }}
                  />
                </VBox>
              </CollapsibleHeader>
              <AppointmentSheet
                visible={this.state.appointmentSheetVisible}
                onActionTaken={this.handleAppointmentSheetAction}
                onDismiss={this.handleAppointmentSheetDismiss}
              />
              <GpsReview
                visible={this.state.reviewVisible}
                onFinish={this.handleReviewFinish}
              />
            </Root>
          );
        }}
      </Subscribe>
    );
  }

  private handleVoucherTap = () => {
    Navigation.push(this.props.componentId, VoucherPage);
  };

  private handlePharmacyTap = () => {
    Linking.canOpenURL('https://consultaremedios.com.br/').then(supported => {
      if (supported) {
        Linking.openURL('https://consultaremedios.com.br/');
      }
    });
    // Navigation.push(this.props.componentId, PharmacyMenuPage);
  };

  private handleAppointmentTap = () => {
    this.setState({ appointmentSheetVisible: true });
  };

  private handleAppointmentSheetAction = () => {
    this.setState({ appointmentSheetVisible: false });
  };

  private handleAppointmentSheetDismiss = (action?: AppointmentSheetAction) => {
    if (action === 'consulta') {
      Navigation.push<EmergencyCarePageProps>(
        this.props.componentId,
        EmergencyCarePage,
        {
          codigoGrupoLivreto: '1',
          granted: this.state.granted,
          location: this.location,
        },
      );
    } else if (action === 'exame') {
      Navigation.push<EmergencyCarePageProps>(
        this.props.componentId,
        EmergencyCarePage,
        {
          codigoGrupoLivreto: '2',
          granted: this.state.granted,
        },
      );
    }
  };

  private handlePersonalDoctor() {
    Navigation.showModal<SchedulesPageProps>(SchedulesPage, {
      personalDoctor: true,
    });
  }

  private handleTimeQTap = async () => {
    Navigation.showModal(ChatPage, { onClose: this.handleChatClose });
  };

  private handleTelemedicineTap = async () => {
    // // Alert.alert(
    // //   'Aviso',
    // //   'Temporariamente Indisponível. Em breve, você poderá realizar teleconsultas diretamente na tela do seu celular!',
    // // );
    // const res = await fetchInvitePatient();
    // if (!res) {
    //   return;
    // }
    // const url = res.link_external_login;
    // console.warn('url >', url);
    // Platform.OS === 'android'
    //   ? Navigation.showModal(WebViewPage, {
    //       url: `${url}`,
    //     })
    //   : this.handleClick(`${url}`);
    const url = this.state.urlTelemedicine;
    this.handleClick(`${url}`);
  };

  handleClick = (url: string) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.warn("Don't know how to open URI: " + url);
      }
    });
  };

  private handleReviewFinish = () => {
    this.setState({ reviewVisible: false });
  };

  private handleChatClose = () => {
    // this.setState({ reviewVisible: true });
  };
}

const styles = StyleSheet.create({
  gridItem: {
    alignItems: 'center',
    backgroundColor: '#f58c78',
    height: 130,
    flexGrow: 1,
    margin: 4,
    paddingTop: 30,
    paddingBottom: 10,
    paddingLeft: 17,
    paddingRight: 17,
    flexBasis: 0,
    borderRadius: 10,
  },
  gridItemTitle: {
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 16,
    lineHeight: 20,
    fontWeight: 'bold',
    fontFamily: 'Helvetica',
  },
  gridItemIcon: {
    width: 40,
    height: 40,
    marginBottom: 10,
  },
});
