export const EmergencyNetworkInfoString = {
    Title: 'Busca de rede',
    Labels: {
        State: 'Estado',
        City: 'Município',
        District: 'Bairro',
        Type: 'Tipo',
        Speciality: 'Especialidade',
        Name: 'Nome do credenciado',

        // PROPRIEDADES LABEL GAMA
        NetworkPlan: 'Plano',
        TypeService: 'Tipo de Serviço',
        CityGama: 'Cidade',
    },
    Button: 'Buscar Rede',
};
