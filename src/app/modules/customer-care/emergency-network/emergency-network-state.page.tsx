import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { EmergencyNetworkInfoString } from './emergency-network-info.string';
import {
  GuardProps,
  Navigation,
  FlashMessageService,
} from '@app/core/navigation';
import { GuardParams } from '@app/modules/authentication';
import { Container } from 'typedi';
import {
  Form,
  FormSubmitData,
  PickerField,
  PickerFieldItem,
  Platform,
  PrimaryButton,
  TextField,
  VBox,
  VSeparator,
} from '@atomic';
import * as React from 'react';
import { PageView } from '@app/core/analytics';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import {
  CarePointFragment,
  CarePointNetworkCityInput,
  CarePointNetworkDistrictInput,
  CarePointNetworkFilterInput,
  CarePointNetworkSpecialityInput,
  CarePointNetworkTypeInput,
  CarePointsNetworkCityQuery,
  CarePointsNetworkCityQueryVariables,
  CarePointsNetworkDistrictQuery,
  CarePointsNetworkDistrictQueryVariables,
  CarePointsNetworkSpecialityQuery,
  CarePointsNetworkSpecialityQueryVariables,
  CarePointsNetworkStateQuery,
  CarePointsNetworkTypeQuery,
  CarePointsNetworkTypeQueryVariables,
  EmergencyCareFilterQuery,
  EmergencyCareFilterQueryVariables,
} from '@app/data/graphql';
import { ApolloError } from 'apollo-client';
// tslint:disable-next-line: max-line-length
import {
  ActivityIndicator,
  PermissionsAndroid,
  StyleSheet,
} from 'react-native';
import { Color } from '@atomic/obj.constants/constants.gama';
import { GeoCoordinates } from '@app/model';
import {
  EmergencyNetworkPage,
  EmergencyNetworkPageProps,
} from './emergency-network.page';
import { ErrorMapper } from '@app/utils/error.mapper';
import { NetworkParam } from '@app/model/network.model';
import { LocalDataSource } from '@app/data/local';
import Geolocation from '@react-native-community/geolocation';

interface EmergencyNetworkStateProps extends GuardProps<GuardParams> {
  componentId?: string;
  networks: ListMapItem[];
}

export interface LocationError {
  permissionDenied: boolean;
}

export interface ListMapItem {
  title: string;
  key: string;
  distance?: number;
  carePoint: CarePointFragment;
  rightThumb?: string;
  userLocation?: string;
  isFavorite?: boolean;
}

interface EmergencyNetworkStateState {
  loading: boolean;
  states: PickerFieldItem[];
  selectedState: string;
  cities: PickerFieldItem[];
  selectedCity: string;
  districts: PickerFieldItem[];
  selectedDistrict: any;
  types: PickerFieldItem[];
  selectedType: string;
  specialities: PickerFieldItem[];
  selectedSpeciality: string;
  name: string;
  location?: GeoCoordinates;
  maxDistance: number;
  error?: LocationError;
  networks: any;
}

const Fields = {
  state: 'state',
  city: 'city',
  district: 'district',
  type: 'type',
  speciality: 'speciality',
  name: 'name',
};

const styles = StyleSheet.create({
  fieldWhite: {
    color: Color.White,
  },
});

export const DefaultCenter: GeoCoordinates = {
  latitude: -23.5505,
  longitude: -46.6333,
};

const LOCAL_STORAGE_SEARCH_NETWORK_DATA = 'sessionStore-activeNetworkData';

@PageView('EmergencyNetworkStatePage')
export class EmergencyNetworkStatePage extends React.Component<
  EmergencyNetworkStateProps,
  EmergencyNetworkStateState
> {
  static options = { name: 'EmergencyNetworkStatePage' };
  public stateSelected: string = '';
  public citySelected: string = '';
  public districtSelected: string = '';
  public typeSelected: string = '';
  public specialitySelected: string = '';

  public controlType: boolean = false;
  public controlSpeciality: boolean = false;
  public controlLoading: boolean = true;

  private readonly graphQLProvider: GraphQLProvider =
    Container.get(GraphQLProviderToken);

  private readonly localDataSource: LocalDataSource =
    Container.get(LocalDataSource);

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      states: [],
      selectedState: '',
      cities: [],
      selectedCity: '',
      districts: [],
      selectedDistrict: '',
      types: [],
      selectedType: '',
      specialities: [],
      selectedSpeciality: '',
      name: '',
      location: DefaultCenter,
      maxDistance: 50,
      error: null,
      networks: null,
    };
  }

  render() {
    return (
      <CollapsibleHeader title={EmergencyNetworkInfoString.Title}>
        <Form onSubmit={this.handleSubmit}>
          <Form.Scroll>
            <VBox>
              <VSeparator />
              <Form.Field
                label={EmergencyNetworkInfoString.Labels.State}
                name={Fields.state}
                value={this.stateSelected}>
                <PickerField
                  items={this.state.states}
                  disabled={false}
                  onValueChange={this.handleChangeState}
                />
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.City}
                name={Fields.city}
                value={this.citySelected}>
                {this.stateSelected !== '' ? (
                  <PickerField
                    items={this.state.cities}
                    disabled={false}
                    onValueChange={this.handleChangeCity}
                  />
                ) : (
                  <TextField editable={false} />
                )}
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.District}
                name={Fields.district}
                value={this.districtSelected}>
                {this.citySelected !== '' ? (
                  <PickerField
                    items={this.state.districts}
                    disabled={false}
                    onValueChange={this.handleChangeDistrict}
                  />
                ) : (
                  <TextField editable={false} style={styles.fieldWhite} />
                )}
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.Type}
                name={Fields.type}
                value={this.typeSelected}>
                {this.citySelected !== '' ? (
                  <PickerField
                    items={this.state.types}
                    disabled={false}
                    onValueChange={this.handleChangeType}
                  />
                ) : (
                  <TextField editable={false} />
                )}
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.Speciality}
                name={Fields.speciality}
                value={this.specialitySelected}>
                {this.citySelected !== '' ? (
                  <PickerField
                    items={this.state.specialities}
                    disabled={false}
                    onValueChange={this.handleChangeSpeciality}
                  />
                ) : (
                  <TextField editable={false} />
                )}
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.Name}
                name={Fields.name}
                value="">
                <TextField editable={true} type={TextField.Type.Normal} />
              </Form.Field>
              <VSeparator />
              <Form.Submit>
                <PrimaryButton
                  expanded={true}
                  disabled={
                    this.controlType === false &&
                    this.controlSpeciality === false
                  }
                  text={EmergencyNetworkInfoString.Button}
                  submit={true}
                />
                <VSeparator />
              </Form.Submit>
            </VBox>
          </Form.Scroll>
        </Form>
        {this.state.loading && this.controlLoading && (
          <ActivityIndicator
            size="large"
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: 0,
              top: 0,
            }}
          />
        )}
      </CollapsibleHeader>
    );
  }

  componentDidMount() {
    this.actionState();
    this.handlePlatforms();
  }

  handlePlatforms() {
    this.setState({ error: null });
    if (Platform.OS === 'android') {
      this.handleAndroid();
    } else {
      this.fetchPosition();
    }
  }

  private async handleAndroid() {
    const granted = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (granted) {
      this.fetchPosition();
      return;
    }

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === 'denied' || status === 'never_ask_again') {
      this.setState({ location: null, error: { permissionDenied: true } });
    } else {
      this.fetchPosition();
    }
  }

  private fetchPosition() {
    Geolocation.getCurrentPosition(
      this.successLocationHandler,
      this.errorLocationHandler,
    );
  }

  private successLocationHandler = (location: any) => {
    const coords: GeoCoordinates = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    };
    this.setState({ location: coords });
    // console.warn('Latitude: ' + location.coords.latitude);
    // console.warn('Longitude: ' + location.coords.longitude);
  };

  private errorLocationHandler = (error: any) => {
    const permissionDenied: boolean = error.code === error.PERMISSION_DENIED;
    this.setState({ location: null, error: { permissionDenied } });
  };

  private handleError = (error: ApolloError) => {
    this.setState({ loading: false });
    this.controlLoading = false;
    FlashMessageService.showError(ErrorMapper.map(error));
  };

  private handleStateSuccess = (res: CarePointsNetworkStateQuery) => {
    const carePointsNetworkStates: PickerFieldItem[] =
      res.CarePointsNetworkState.map(this.mapToListState);
    carePointsNetworkStates.unshift({ value: '', label: 'Selecione' });
    // tslint:disable-next-line: max-line-length
    this.setState({
      loading: false,
      states: carePointsNetworkStates,
      selectedState: carePointsNetworkStates[0].value.toString(),
    });
    this.controlLoading = true;
  };

  private handleCitySuccess = (res: CarePointsNetworkCityQuery) => {
    const carePointsNetworkCity: PickerFieldItem[] =
      res.CarePointsNetworkCity.map(this.mapToListCity);
    carePointsNetworkCity.unshift({ value: '', label: 'Selecione' });
    // tslint:disable-next-line: max-line-length
    this.setState({
      loading: false,
      cities: carePointsNetworkCity,
      selectedCity: carePointsNetworkCity[0].value.toString(),
      districts: [],
    });
    this.controlLoading = true;
  };

  private handleDistrictSuccess = (res: CarePointsNetworkDistrictQuery) => {
    const carePointsNetworkDistrict: PickerFieldItem[] =
      res.CarePointsNetworkDistrict.map(this.mapToListDistrict);
    carePointsNetworkDistrict.unshift({ value: null, label: 'Todos' });
    // tslint:disable-next-line: max-line-length
    this.setState({
      loading: false,
      districts: carePointsNetworkDistrict,
      selectedDistrict: carePointsNetworkDistrict[0].value,
    });
    this.controlLoading = false;
  };

  private handleTypeSuccess = (res: CarePointsNetworkTypeQuery) => {
    const carePointsNetworkType: PickerFieldItem[] =
      res.CarePointsNetworkType.map(this.mapToListType);
    carePointsNetworkType.unshift({ value: '', label: 'Todos' });
    // tslint:disable-next-line: max-line-length
    this.setState({
      loading: false,
      types: carePointsNetworkType,
      selectedType: carePointsNetworkType[0].value.toString(),
    });
    this.controlType = true;
    this.controlLoading = true;
  };

  private handleSpecialitySuccess = (res: CarePointsNetworkSpecialityQuery) => {
    // tslint:disable-next-line: max-line-length
    const carePointsNetworkSpeciality: PickerFieldItem[] =
      res.CarePointsNetworkSpeciality.map(this.mapToListSpeciality);
    carePointsNetworkSpeciality.unshift({ value: '', label: 'Todos' });
    // tslint:disable-next-line: max-line-length
    this.setState({
      loading: false,
      specialities: carePointsNetworkSpeciality,
      selectedSpeciality: carePointsNetworkSpeciality[0].value.toString(),
    });
    this.controlSpeciality = true;
    this.controlLoading = true;
  };

  private handleSearchSuccess = (res: EmergencyCareFilterQuery) => {
    this.setState({ loading: false, networks: res });

    Navigation.push<EmergencyNetworkPageProps>(
      this.props.componentId,
      EmergencyNetworkPage,
      {
        networks: this.state.networks,
        location: this.state.location,
      },
    );
  };

  private handleChangeState = (valueState: string) => {
    this.stateSelected = valueState;
    this.citySelected = '';
    this.districtSelected = '';
    this.setState({
      states: this.state.states,
      selectedState: valueState,
      cities: [],
      selectedCity: '',
      districts: [],
      selectedDistrict: '',
      types: [],
      selectedType: '',
      specialities: [],
      selectedSpeciality: '',
      name: '',
    });
    this.actionCity();
    this.controlType = false;
    this.controlSpeciality = false;
  };

  private handleChangeCity = (valueCity: string) => {
    this.citySelected = valueCity;
    this.districtSelected = '';
    this.setState({
      states: this.state.states,
      selectedState: this.state.selectedState,
      cities: this.state.cities,
      selectedCity: valueCity,
      districts: [],
      selectedDistrict: '',
      types: [],
      selectedType: '',
      specialities: [],
      selectedSpeciality: '',
      name: '',
    });
    this.controlType = false;
    this.controlSpeciality = false;
    this.actionDistrict();
    if (Platform.OS === 'ios') {
      this.actionType();
      this.actionSpeciality();
    }
  };

  private handleChangeDistrict = (valueDistrict: string) => {
    this.districtSelected = valueDistrict;
    this.typeSelected = '';
    this.setState({
      states: this.state.states,
      selectedState: this.state.selectedState,
      cities: this.state.cities,
      selectedCity: this.state.selectedCity,
      districts: this.state.districts,
      selectedDistrict: valueDistrict,
      types: [],
      selectedType: '',
      specialities: [],
      selectedSpeciality: '',
      name: '',
    });
    this.actionType();
    if (Platform.OS === 'ios') {
      this.actionSpeciality();
    }
  };
  private handleChangeType = (valueType: string) => {
    this.typeSelected = valueType;
    this.specialitySelected = '';
    this.setState({
      states: this.state.states,
      selectedState: this.state.selectedState,
      cities: this.state.cities,
      selectedCity: this.state.selectedCity,
      districts: this.state.districts,
      selectedDistrict: this.state.selectedDistrict,
      types: this.state.types,
      selectedType: valueType,
      specialities: [],
      selectedSpeciality: '',
      name: '',
    });
    this.actionSpeciality();
  };

  private handleChangeSpeciality = (valueSpeciality: string) => {
    this.specialitySelected = valueSpeciality;
    this.setState({
      states: this.state.states,
      selectedState: this.state.selectedState,
      cities: this.state.cities,
      selectedCity: this.state.selectedCity,
      districts: this.state.districts,
      selectedDistrict: this.state.selectedDistrict,
      types: this.state.types,
      selectedType: this.state.selectedType,
      specialities: this.state.specialities,
      selectedSpeciality: valueSpeciality,
      name: '',
    });
  };

  private actionState() {
    this.setState({ loading: true });
    this.graphQLProvider.query(
      'care-point-network-state',
      null,
      this.handleStateSuccess,
      this.handleError,
    );
  }

  private actionCity() {
    if (this.stateSelected !== '') {
      this.setState({ loading: true });
      const variables: CarePointNetworkCityInput = {
        uf: this.stateSelected,
      };

      this.graphQLProvider.query<CarePointsNetworkCityQueryVariables>(
        'care-point-network-city',
        { data: variables },
        this.handleCitySuccess,
        this.handleError,
      );
    } else {
      this.districtSelected = '';
      this.setState({ districts: [] });
    }
  }

  private actionDistrict() {
    this.setState({ types: [] });
    if (this.citySelected !== '') {
      this.setState({ loading: true });
      const variables: CarePointNetworkDistrictInput = {
        uf: this.stateSelected,
        cityCode: Number(this.citySelected),
      };

      this.graphQLProvider.query<CarePointsNetworkDistrictQueryVariables>(
        'care-point-network-district',
        { data: variables },
        this.handleDistrictSuccess,
        this.handleError,
      );
    }
  }

  private actionType() {
    this.setState({ loading: true });
    const variables: CarePointNetworkTypeInput = {
      uf: this.stateSelected,
      cityCode: Number(this.citySelected),
      districtName: this.districtSelected,
    };
    this.graphQLProvider.query<CarePointsNetworkTypeQueryVariables>(
      'care-point-network-type',
      { data: variables },
      this.handleTypeSuccess,
      this.handleError,
    );
  }

  private actionSpeciality() {
    this.setState({ loading: true });
    let typeN = null;
    if (this.typeSelected !== '') {
      typeN = Number(this.typeSelected);
    }
    const variables: CarePointNetworkSpecialityInput = {
      uf: this.stateSelected,
      cityCode: Number(this.citySelected),
      districtName: this.districtSelected,
      typeCode: typeN,
    };

    this.graphQLProvider.query<CarePointsNetworkSpecialityQueryVariables>(
      'care-point-network-speciality',
      { data: variables },
      this.handleSpecialitySuccess,
      this.handleError,
    );
  }

  private actionSearch() {
    this.localDataSource.removeMulti([LOCAL_STORAGE_SEARCH_NETWORK_DATA]);

    this.setState({ loading: true });
    let typeN = null;
    let specialityN = null;
    if (this.typeSelected !== '') {
      typeN = Number(this.typeSelected);
    }
    if (this.specialitySelected !== '') {
      specialityN = Number(this.specialitySelected);
    }
    const variables: CarePointNetworkFilterInput = {
      uf: this.stateSelected,
      cityCode: Number(this.citySelected),
      districtName: this.state.selectedDistrict,
      typeCode: typeN,
      specialityPublicationCode: specialityN,
      practitionerName: this.state.name,
      latitude: this.state.location.latitude,
      longitude: this.state.location.longitude,
      maxDistance: this.state.maxDistance,
    };

    this.graphQLProvider.query<EmergencyCareFilterQueryVariables>(
      'emergency-care-filter',
      { data: variables },
      this.handleSearchSuccess,
      this.handleError,
    );

    let network: NetworkParam = new NetworkParam();

    network.uf = variables.uf;
    network.cityCode = variables.cityCode;
    network.districtName = variables.districtName;
    network.typeCode = variables.typeCode;
    network.specialityPublicationCode = variables.specialityPublicationCode;
    network.practitionerName = variables.practitionerName;
    network.latitude = variables.latitude;
    network.longitude = variables.longitude;
    network.maxDistance = variables.maxDistance;

    this.setNetwork(network);
  }

  async setNetwork(network: NetworkParam): Promise<void> {
    await Promise.all([
      this.localDataSource.set<NetworkParam>(
        LOCAL_STORAGE_SEARCH_NETWORK_DATA,
        network,
      ),
    ]);
  }

  private mapToListState = (res: any): PickerFieldItem => {
    return {
      value: res.uf,
      label: res.ufName,
    };
  };

  private mapToListCity = (res: any): PickerFieldItem => {
    return {
      value: res.cityCode,
      label: res.cityName,
    };
  };

  private mapToListDistrict = (res: any): PickerFieldItem => {
    return {
      value: res.districtName,
      label: res.districtName,
    };
  };

  private mapToListType = (res: any): PickerFieldItem => {
    return {
      value: res.typeCode,
      label: res.typeDesc,
    };
  };

  private mapToListSpeciality = (res: any): PickerFieldItem => {
    return {
      value: res.specialityPublicationCode,
      label: res.specialityDesc,
    };
  };

  // private handleServiceSearchNetwork = (formData: FormSubmitData) => {
  //   this.setState({ name: formData.data.name });
  //   this.actionSearch();
  // }

  private handleSubmit = (formData: FormSubmitData) => {
    this.setState({
      name: formData.data.name,
      selectedDistrict: formData.data.district,
    });
    this.actionSearch();
  };
}
