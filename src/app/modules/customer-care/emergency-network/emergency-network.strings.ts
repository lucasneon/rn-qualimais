// tslint:disable:max-line-length
export const EmergencyNetworkStrings = {
  Title: 'Serviços de saúde',
  CheckRoute: 'Ver como chegar',
  Refund:
    'Você não terá reembolso se for diretamente ao hospital sem avisar sua Guia. Clique para falar com ela.',
  Map: 'Mapa',
  List: 'Lista',
  NotFound: {
    Title: 'Serviço de Saúde não encontrado',
    Message:
      'Não foram encontrados pronto-atendimentos próximos a você. Por favor, entre em contato com o Guia.',
  },
};
