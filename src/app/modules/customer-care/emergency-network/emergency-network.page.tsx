import * as React from 'react';
import { PageView } from '@app/core/analytics';
import { ListMap } from '@app/modules/common';

import { Scroll } from '@atomic';
import { NavigationPageProps } from '@app/core/navigation';
import { GeoCoordinates } from '@app/model';
import { CarePointNetworkFragment, EmergencyCareFilterQuery } from '@app/data/graphql';
import { ListMapItem } from '@app/modules/customer-care/emergency-network/emergency-network-state.page';
import { EmergencyNetworkStrings } from './emergency-network.strings';

export interface EmergencyNetworkPageProps {
    networks: EmergencyCareFilterQuery;
    location?: GeoCoordinates;
}

@PageView('EmergencyNetwork')
export class EmergencyNetworkPage extends React.Component<NavigationPageProps<EmergencyNetworkPageProps>> {
  static options = { name: 'EmergencyNetworkPage' };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const emergencyNetworkPoints = this.props.navigation.networks;
    const userLocation = this.props.navigation.location;
    // tslint:disable-next-line: max-line-length
    const items: ListMapItem[] = emergencyNetworkPoints.EmergencyCareFilter && emergencyNetworkPoints.EmergencyCareFilter.map(this.mapToListItem);

    return (
        <Scroll>
            <ListMap
                loading={false}
                userLocation={userLocation}
                items={items}
                title={EmergencyNetworkStrings.Title}
                notFoundTitle={EmergencyNetworkStrings.NotFound.Title}
                notFoundMessage={EmergencyNetworkStrings.NotFound.Message}
            />
        </Scroll>
      );
  }

  private mapToListItem = (emergencyCare: CarePointNetworkFragment): ListMapItem => {

    emergencyCare.distance = (Math.round((emergencyCare.distance / 1000) * 100) / 100);

    return {
      title: emergencyCare.carePoint.name,
      key: emergencyCare.practitionerId,
      distance: emergencyCare.distance,
      carePoint: emergencyCare.carePoint,
      isFavorite: emergencyCare.isFavorite,
    };
  }
}
