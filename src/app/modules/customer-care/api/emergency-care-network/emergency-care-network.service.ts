import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import { LocalDataSource } from '@app/data';
import { CarePointNetworkFilterGamaInput } from '@app/data/graphql';
import {
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { EmergencyCareGamaFilterRequest } from '@app/modules/common/events/api/care-point-network.entity';
import { CarePointNetworkMapper } from '@app/modules/common/events/api/care-point-network.mapper';
import { CarePointNetworkModel } from '@app/modules/common/events/api/care-point-network.model';

import Container from 'typedi';

const opCode: string = Container.get(GAMA_OP_CODE);
const stimulatingCode: string = Container.get(GAMA_STIMULATING_CODE);
const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchEmergencyCareGamaFilterPoints = async (
  input: CarePointNetworkFilterGamaInput,
): Promise<{ data: CarePointNetworkModel[]; error: any }> => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );

  const req: EmergencyCareGamaFilterRequest = {
    codigoSistema: stimulatingCode,
    codigoOperadora: opCode,
    codigoRede: input.networkCode.toString(),
    codigoGrupoLivreto: input.bookletCode,
    latitude: input.latitude.toString().replace('.', ','),
    longitude: input.longitude.toString().replace('.', ','),
    distancia: input.maxDistance.toString(),
    descricaoEspecialidade: input.specialityDesc,
    codigoBeneficiario: beneficiaryCode,
    uf: input.state,
    cidade: input.city,
    bairro: input.district,
  };

  let res;
  try {
    res = await carePointNetworkGamaFilterRequest(req);
  } catch (e) {
    return { data: null, error: e };
  }
  if (res.error) {
    return { data: null, error: res.error };
  }
  const response =
    res.data && res.data.map(CarePointNetworkMapper.mapGamaResponse);
  return { data: response, error: null };
};

const carePointNetworkGamaFilterRequest = async (
  req: EmergencyCareGamaFilterRequest,
) => {
  let res;
  try {
    res = await axiosPost({
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
      url: '/rede-credenciada/lista-rede-credenciada-p2',
      data: req,
    });
  } catch (e) {
    return { data: null, error: e };
  }
  return res;
};
