import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import { InputParams } from '@app/model/api/input.params.model';
import {
  CarePointNetworkRequest,
  CarePointNetworkResponse,
} from '@app/modules/common/events/api/care-point-network.entity';
import {
  CarePointNetworkInputModel,
  CarePointNetworkModel,
} from '@app/modules/common/events/api/care-point-network.model';
import Container from 'typedi';

const opCode: string = Container.get(GAMA_OP_CODE);
const stimulantingCode: string = Container.get(GAMA_STIMULATING_CODE);

export const mapInputCarePoint = (
  input: InputParams<CarePointNetworkInputModel>,
): CarePointNetworkRequest => {
  return {
    codigoOperadora: opCode,
    codigoEstipulante: stimulantingCode,
    codigoBeneficiario: input.beneficiaryCode,
    latitude: input.latitude.toString().replace('.', ','),
    longitude: input.longitude.toString().replace('.', ','),
    distancia: input.maxDistance.toString(),
  };
};

export const mapResponseCarePoint = (
  res: CarePointNetworkResponse,
): CarePointNetworkModel => {
  const [district, cityState] = res.regiao && res.regiao.split(', ');
  const [city, state] = cityState ? cityState.split('-') : ['', ''];
  const [addressType, addressName, addressNumber] =
    res.localizacao && res.localizacao.split(', ');
  const street =
    addressType && addressName ? `${addressType}. ${addressName}` : '';
  return {
    distance: +res.distance,
    practitionerId: res.codigoPrestador,
    isFavorite: res.favorito && res.favorito === 'S',
    carePoint: {
      id: res.codigoPontoAtendimento,
      name: res.prestador,
      contact: {
        phoneNumbers: mapPhones(res),
        website: res.site,
      },
      address: {
        latitude: parseCoordinate(res.latitude),
        longitude: parseCoordinate(res.longitude),
        state,
        city,
        district,
        street,
        addressNumber: addressNumber || '',
      },
    },
  };
};

const mapPhones = (res: CarePointNetworkResponse) => {
  const phones: string[] = [];

  if (res.telefone1) {
    phones.push(res.telefone1);
  }

  if (res.telefone2) {
    phones.push(res.telefone2);
  }

  return phones;
};

function parseCoordinate(coord: string): number {
  try {
    if (coord) {
      return +coord.replace(/,/, '.');
    } else {
      return null;
    }
  } catch (e) {
    throw new Error('Could not parse latitude and longitude.');
  }
}
