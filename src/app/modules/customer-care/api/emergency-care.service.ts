import {
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { EmergencyCareRequest } from '@app/modules/common/events/api/care-point-network.entity';
import {
  mapInputCarePoint,
  mapResponseCarePoint,
} from './emregency-care.mapper';
import Container from 'typedi';
import { LocalDataSource } from '@app/data';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchEmergencyCareFilterPoints = async (input: any) => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const req: EmergencyCareRequest = {
    ...mapInputCarePoint({ ...input, beneficiaryCode }),
    codigoGrupoLivreto: input.codigoGrupoLivreto.toString(),
  };
  const res = await carePointNetworkRequest(req);

  if (res.error) {
    throw new Error(res.error);
  }
  return res.data && res.data.map(mapResponseCarePoint);
};

const carePointNetworkRequest = async (req: EmergencyCareRequest) => {
  let res;
  try {
    res = await axiosPost({
      url: '/rede-credenciada/busca-prestador-geolocalizacao',
      data: req,
      baseUrl: BaseUrl.GAMA_AUTH_URL,
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};
