export interface ChatRequest {
  login: string;
  senha: string;
  codigoOperadora: string;
  codigoBeneficiario: string;
}
