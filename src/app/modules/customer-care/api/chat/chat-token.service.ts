import {
  GAMA_OP_CODE,
  LIVE_TOUCH_API_KEY,
  LIVE_TOUCH_CHAT_USER_PASSWORD,
} from '@app/config';
import { LocalDataSource } from '@app/data';
import { InputParams } from '@app/model/api/input.params.model';
import {
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services';
import Container from 'typedi';
import { ChatRequest } from './chat-token.model';

const beneficiaryProvider: BeneficiaryProvider = Container.get(
  BeneficiaryProviderToken,
);

const apiKey: string = Container.get(LIVE_TOUCH_API_KEY);
const chatPassword: string = Container.get(LIVE_TOUCH_CHAT_USER_PASSWORD);
const opCode: string = Container.get(GAMA_OP_CODE);

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const execChatToken = async (
  input: InputParams<ChatInputModel>,
): Promise<ChatModel> => {
  let res;
  try {
    res = await fetchChatToken(input);
  } catch (err) {
    throw new Error(err);
  }
  return res;
};

const fetchChatToken = async (
  input: InputParams<ChatInputModel>,
): Promise<ChatModel> => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const res = await chatTokenRequest({
    login: '000010973568000', //beneficiaryProvider.state.active.idHealthCareCard,
    senha: chatPassword,
    codigoOperadora: opCode,
    codigoBeneficiario: beneficiaryCode,
  });

  if (res.error) {
    throw new Error(res.error);
  }

  const isValidResponse =
    res.data &&
    res.data.atendimento &&
    res.data.atendimento.wstoken &&
    res.data.atendimento.conversa.conversaId;
  if (!isValidResponse) {
    throw new Error('Invalid response from chat token request');
  }

  return {
    wsToken: res.data.atendimento.wstoken,
    chatId: res.data.atendimento.conversa.conversaId.toString(),
  };
};

const chatTokenRequest = async (request: ChatRequest) => {
  let res;
  try {
    res = await axiosPost({
      url: 'rest/gama/chat/loginOperadora',
      baseUrl: BaseUrl.LIVE_TOUCH_BASE_URL,
      data: request,
    });
  } catch (err) {
    return { data: null, error: err };
  }
  return res;
};

export interface ChatInputModel {
  idHealhCareCard: string;
  beneficiaryCode: string;
}

export interface ChatModel {
  wsToken: string;
  chatId: string;
}
