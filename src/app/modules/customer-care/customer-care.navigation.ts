import { Navigation } from '@app/core/navigation';
import { LoginGuard } from '@app/modules/authentication';
import { VoucherPage } from '../voucher/index.page';
import { ChatPage } from './chat.page';
import { CustomerCarePage } from './customer-care.page';
import { EmergencyCarePage, EmergencyNetworkGamaPage, EmergencyGamaNetworkPage } from './emergency-care';
import { EmergencyNetworkStatePage } from './emergency-network';
import { EmergencyNetworkPage } from './emergency-network/emergency-network.page';

export const CustomerCareNavigation = {
  register: () => {
    Navigation.register(CustomerCarePage, true, LoginGuard());
    Navigation.register(EmergencyCarePage);
    Navigation.register(ChatPage);
    Navigation.register(EmergencyNetworkStatePage);
    Navigation.register(EmergencyNetworkGamaPage);
    Navigation.register(EmergencyNetworkPage);
    Navigation.register(EmergencyGamaNetworkPage);
    // Navigation.register(VoucherPage);
    // Navigation.register(VoucherDetailsPage);
  },
};
