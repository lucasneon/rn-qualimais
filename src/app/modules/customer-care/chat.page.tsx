import { ApolloError } from 'apollo-client';
import * as React from 'react';
import {
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  View,
} from 'react-native';
import { WebView } from 'react-native-webview';
import { Container } from 'typedi';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { TopAreaView } from '@app/components/obj.top-area';
import { ChatClientKey } from '@app/config';
import { PageView } from '@app/core/analytics';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { ChatTokenQuery } from '@app/data/graphql';
import { AppError } from '@app/model';
import { ErrorMapper } from '@app/utils';
import KeyboardShift from '@app/utils/keyboard-shift.utils';
import { Color } from '@atomic';
import {
  ChatTextInput,
  ChatView,
} from '@atomic/atm.text-field/text-field.component.style';
import { ChatButton } from '@atomic/atm.button/chat-button.component';
import { ChatModel, execChatToken } from './api/chat/chat-token.service';

export interface ChatPageProps {
  onClose?: () => void;
  chatId?: string;
}

interface ChatData {
  wsToken: string;
  chatId: string;
}

interface ChatPageState {
  loading: boolean;
  error: AppError;
  data: ChatData;
  text: any;
}

const ChatPageLoad: React.SFC<{}> = () => (
  <ActivityIndicator
    size="large"
    style={{ flex: 1, justifyContent: 'center' }}
  />
);

@PageView('Chat')
export class ChatPage extends React.Component<
  NavigationPageProps<ChatPageProps>,
  ChatPageState
> {
  static options = {
    name: 'ChatPage',
    topBar: { background: { color: Color.White }, rightButtons: [] },
  };

  private readonly chatUrl: string = Container.get(ChatClientKey);
  private readonly graphQLProvider: GraphQLProvider =
    Container.get(GraphQLProviderToken);
  private webview: WebView;

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      loading: true,
      error: null,
      data: null,
      text: '',
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    execChatToken(null)
      .then(res => this.handleSuccess(res))
      .catch(this.handleError);
    // this.graphQLProvider.query(
    //   'chat-token',
    //   null,
    //   this.handleSuccess,
    //   this.handleError,
    // );
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  hideSpinner() {
    this.setState({ loading: false });
  }

  componentWillUnmount() {
    if (this.props.navigation.onClose && this.state.data) {
      this.props.navigation.onClose();
    }
  }

  render() {
    console.warn('loading: ', this.state.loading);
    return this.state.error ? (
      <PlaceholderSelector error={ErrorMapper.map(this.state.error)} />
    ) : this.state.data ? (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 0}>
        <KeyboardShift removeAnimationTime={true}>
          <TopAreaView height={Navigation.TopBarHeight} />
          <WebView
            ref={ref => (this.webview = ref)}
            source={{ uri: this.replaceUrlParams(this.state.data) }}
            onLoad={() => this.hideSpinner()}
            startInLoadingState={true}
            // renderLoading={this.renderLoad}
            javaScriptEnabled={true}
            cacheEnabled={true}
          />
          <ChatView>
            <ChatTextInput
              value={this.state.text}
              onChangeText={this.setText}
              placeholder={'Digite aqui...'}
            />
            <ChatButton
              onTap={this.sendMessage}
              disabled={this.state.loading}
            />
          </ChatView>
        </KeyboardShift>
      </KeyboardAvoidingView>
    ) : (
      <ActivityIndicator color="#171a88" size="large" style={styles.loading} />
    );
  }

  private cleanText = () => {
    this.setState({ text: '' });
  };

  private setText = message => {
    this.setState({ text: message });
  };

  private sendMessage = () => {
    this.webview.injectJavaScript(
      `window.sendMessageMobile('${this.state.text}')`,
    );
    this.cleanText();
  };

  private handleSuccess = (res: ChatModel) => {
    console.warn('link', res);
    this.setState({ data: res });
  };

  private handleError = (error: ApolloError) => {
    this.setState({ error });
  };

  private replaceUrlParams(chatData: ChatData): string {
    const chatId =
      this.props.navigation.chatId || (chatData && chatData.chatId);
    const url = this.chatUrl
      .replace('{wstoken}', chatData.wsToken)
      .replace('{chatId}', chatId);

    console.info('chatUrl >', url);
    return chatData
      ? this.chatUrl
          .replace('{wstoken}', chatData.wsToken)
          .replace('{chatId}', chatId)
      : '';
  }

  private renderLoad = () => {
    return <ChatPageLoad />;
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    justifyContent: 'center',
    paddingTop: 50,
  },
  sizedBox: {
    paddingTop: 40,
  },
  activityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
  },
});
