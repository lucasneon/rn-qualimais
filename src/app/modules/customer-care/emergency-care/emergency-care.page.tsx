import * as React from 'react';
import { PageView } from '@app/core/analytics';
import { CarePointNetworkFragment } from '@app/data/graphql';
import { GeneralStrings, ListMap, ListMapItem } from '@app/modules/common';
import {
  EmergencyCareContainer,
  EmergencyCareRes,
} from './emergency-care.container';
import { EmergencyCareStrings } from './emergency-care.strings';
import { FlashMessageService, NavigationPageProps } from '@app/core/navigation';
import { ActivityIndicator, AppRegistry, StyleSheet, View } from 'react-native';
import { GeoCoordinates } from '@app/model';

export interface EmergencyCarePageProps {
  codigoGrupoLivreto: string;
  granted: boolean;
  location?: GeoCoordinates;
}
interface EmergencyCarePageState {
  granted: boolean;
  location: GeoCoordinates;
}

@PageView('EmergencyCare')
export class EmergencyCarePage extends React.PureComponent<
  NavigationPageProps<EmergencyCarePageProps>,
  EmergencyCarePageState
> {
  static options = { name: 'EmergencyCarePage' };

  constructor(props) {
    super(props);
    this.state = {
      granted: this.props.navigation.granted,
      location: this.props.navigation.location,
    };
  }

  render() {
    var called = false;
    const props = this.props.navigation;
    return (
      <EmergencyCareContainer codigoGrupoLivreto={props.codigoGrupoLivreto}>
        {(res: EmergencyCareRes) => {
          const emergencyCarePoints =
            res.queryRes &&
            res.queryRes.data &&
            res.queryRes.data.EmergencyCare;
          const items: ListMapItem[] =
            (emergencyCarePoints &&
              emergencyCarePoints.map(this.mapToListItem)) ||
            [];

          if (res.loading) {
            return (
              <View style={styles.center}>
                <ActivityIndicator size="large" color="#171a88" />
              </View>
            );
          }

          if (!res.userLocation) {
            if (!called) {
              FlashMessageService.showErrorMessage(
                GeneralStrings.ErrorLocation,
              );
              called = true;
            }
          }

          return (
            <ListMap
              loading={res.loading}
              userLocation={res.userLocation}
              locationError={res.locationError}
              items={items}
              error={res.queryRes && res.queryRes.error}
              refetch={res.queryRes && res.queryRes.refetch}
              title={EmergencyCareStrings.Title}
              notFoundTitle={
                res.userLocation
                  ? EmergencyCareStrings.NotFound.Title
                  : EmergencyCareStrings.NotFound.TitleLocation
              }
              notFoundMessage={
                res.userLocation
                  ? EmergencyCareStrings.NotFound.Message
                  : EmergencyCareStrings.NotFound.MessageLocation
              }
            />
          );
        }}
      </EmergencyCareContainer>
    );
  }

  private mapToListItem = (
    emergencyCare: CarePointNetworkFragment,
  ): ListMapItem => {
    return {
      title: emergencyCare.carePoint.name,
      key: emergencyCare.practitionerId,
      distance: Math.round((emergencyCare.distance / 1000) * 100) / 100,
      carePoint: emergencyCare.carePoint,
      isFavorite: emergencyCare.isFavorite,
    };
  };
}

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
  },
});
