import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import {
  GuardProps,
  FlashMessageService,
  Navigation,
} from '@app/core/navigation';
import { GuardParams } from '@app/modules/authentication';
import {
  Form,
  PickerField,
  PickerFieldItem,
  PrimaryButton,
  FormSubmitData,
  TextField,
  VBox,
  VSeparator,
  Platform,
} from '@atomic';
import * as React from 'react';
import { PageView } from '@app/core/analytics';
import { Color } from '@atomic/obj.constants/constants.gama';
import { GeoCoordinates } from '@app/model';
import {
  CarePointFragment,
  CarePointsNetworkPerPlanQuery,
  CarePointNetworkPerPlanInput,
  CarePointsNetworkPerPlanQueryVariables,
  CarePointNetworkTypeInput,
  CarePointsNetworkTypeQueryVariables,
  CarePointsNetworkTypeQuery,
  CarePointNetworkSpecialityInput,
  CarePointsNetworkSpecialityQueryVariables,
  CarePointsNetworkSpecialityQuery,
  CarePointsNetworkStateQuery,
  CarePointNetworkStateInput,
  CarePointsNetworkStateQueryVariables,
  CarePointNetworkCityInput,
  CarePointsNetworkCityQueryVariables,
  CarePointsNetworkCityQuery,
  CarePointNetworkDistrictInput,
  CarePointsNetworkDistrictQueryVariables,
  CarePointsNetworkDistrictQuery,
  CarePointNetworkFilterGamaInput,
  EmergencyCareGamaFilterQueryVariables,
  EmergencyCareGamaFilterQuery,
  BeneficiaryInfoQuery,
} from '@app/data/graphql';
import {
  StyleSheet,
  PermissionsAndroid,
  ActivityIndicator,
} from 'react-native';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { Container } from 'typedi';
import { LocalDataSource } from '@app/data/local';
import { ApolloError } from 'apollo-client';
import { ErrorMapper } from '@app/utils';
// import { EmergencyNetworkPageProps, EmergencyNetworkPage } from '@app/modules/customer-care/emergency-network';
import { NetworkParam, NetworkGamaParam } from '@app/model/network.model';
import {
  EmergencyGamaNetworkPageProps,
  EmergencyGamaNetworkPage,
} from '@app/modules/customer-care/emergency-care/emergency-gama-network.page';
import { EmergencyNetworkInfoString } from '@app/modules/customer-care/emergency-network/emergency-network-info.string';
import Geolocation from '@react-native-community/geolocation';
import { fetchEmergencyCareGamaFilterPoints } from '../api/emergency-care-network/emergency-care-network.service';

interface EmergencyNetworkGamaProps extends GuardProps<GuardParams> {
  componentId?: string;
  networks: ListMapItem[];
}

export interface LocationError {
  permissionDenied: boolean;
}

export interface ListMapItem {
  title: string;
  key: string;
  distance?: number;
  carePoint: CarePointFragment;
  rightThumb?: string;
  userLocation?: string;
  isFavorite?: boolean;
}

interface EmergencyNetworkGamaGama {
  loading: boolean;
  states: PickerFieldItem[];
  selectedState: string;
  cities: PickerFieldItem[];
  selectedCity: string;
  districts: PickerFieldItem[];
  selectedDistrict: any;
  types: PickerFieldItem[];
  selectedType: string;
  specialities: PickerFieldItem[];
  selectedSpeciality: string;
  networkPlan: number;
  networkName: string;
  location?: GeoCoordinates;
  maxDistance: number;
  error?: LocationError;
  networks: any;
  selectedBookletCode: any;
}

const Fields = {
  state: 'state',
  city: 'city',
  district: 'district',
  type: 'type',
  speciality: 'speciality',
  networkPlan: 'networkPlan',
};

const styles = StyleSheet.create({
  fieldWhite: {
    color: Color.White,
  },
});

export const DefaultCenter: GeoCoordinates = {
  latitude: -23.51056,
  longitude: -46.87611,
};

const LOCAL_STORAGE_SEARCH_NETWORK_DATA = 'sessionStore-activeNetworkData';

@PageView('EmergencyNetworkGamaPage')
export class EmergencyNetworkGamaPage extends React.Component<
  EmergencyNetworkGamaProps,
  EmergencyNetworkGamaGama
> {
  static options = { name: 'EmergencyNetworkGamaPage' };
  public stateSelected: string = '';
  public citySelected: string = '';
  public districtSelected: string = 'Todos';
  public typeSelected: string = '';
  public specialitySelected: string = '';
  public bookletCodeSelected: number = null;

  public codePlan: string = '';

  // public controlType: boolean = false;
  // public controlSpeciality: boolean = false;
  // public controlLoading: boolean = true;

  public controlSearch: boolean = false;

  private readonly graphQLProvider: GraphQLProvider =
    Container.get(GraphQLProviderToken);

  private readonly localDataSource: LocalDataSource =
    Container.get(LocalDataSource);

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      states: [],
      selectedState: '',
      cities: [],
      selectedCity: '',
      districts: [],
      selectedDistrict: 'Todos',
      types: [],
      selectedType: '',
      specialities: [],
      selectedSpeciality: '',
      networkPlan: null,
      networkName: '',
      location: DefaultCenter,
      maxDistance: 50,
      error: null,
      networks: null,
      selectedBookletCode: '',
    };
  }

  render() {
    return (
      <CollapsibleHeader title={EmergencyNetworkInfoString.Title}>
        <Form onSubmit={this.handleSubmit}>
          <Form.Scroll>
            <VBox>
              <VSeparator />
              <Form.Field
                label={EmergencyNetworkInfoString.Labels.NetworkPlan}
                name={Fields.networkPlan}
                value="">
                <TextField
                  editable={false}
                  defaultValue={this.state.networkName}
                />
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.TypeService}
                name={Fields.type}
                value={this.typeSelected}>
                {this.state.networkName !== '' ? (
                  <PickerField
                    items={this.state.types}
                    disabled={false}
                    onValueChange={this.handleChangeType}
                  />
                ) : (
                  <TextField editable={false} style={styles.fieldWhite} />
                )}
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.Speciality}
                name={Fields.speciality}
                value={this.specialitySelected}>
                {this.typeSelected !== '' ? (
                  <PickerField
                    items={this.state.specialities}
                    disabled={false}
                    onValueChange={this.handleChangeSpeciality}
                  />
                ) : (
                  <TextField editable={false} style={styles.fieldWhite} />
                )}
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.State}
                name={Fields.state}
                value={this.stateSelected}>
                {this.specialitySelected !== '' ? (
                  <PickerField
                    items={this.state.states}
                    disabled={false}
                    onValueChange={this.handleChangeState}
                  />
                ) : (
                  <TextField editable={false} style={styles.fieldWhite} />
                )}
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.CityGama}
                name={Fields.city}
                value={this.citySelected}>
                {this.stateSelected !== '' ? (
                  <PickerField
                    items={this.state.cities}
                    disabled={false}
                    onValueChange={this.handleChangeCity}
                  />
                ) : (
                  <TextField editable={false} style={styles.fieldWhite} />
                )}
              </Form.Field>
              <VSeparator />

              <Form.Field
                label={EmergencyNetworkInfoString.Labels.District}
                name={Fields.district}
                value={this.districtSelected}>
                {this.citySelected !== '' ? (
                  <PickerField
                    items={this.state.districts}
                    disabled={false}
                    onValueChange={this.handleChangeDistrict}
                  />
                ) : (
                  <TextField editable={false} style={styles.fieldWhite} />
                )}
              </Form.Field>
              <VSeparator />

              <Form.Submit>
                <PrimaryButton
                  expanded={true}
                  disabled={!this.controlSearch}
                  text={EmergencyNetworkInfoString.Button}
                  submit={true}
                />
                <VSeparator />
              </Form.Submit>
            </VBox>
          </Form.Scroll>
        </Form>
        {this.state.loading && (
          <ActivityIndicator
            size="large"
            color={Color.Primary}
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: 0,
              top: 0,
            }}
          />
        )}
      </CollapsibleHeader>
    );
  }

  componentDidMount() {
    // this.actionPlanNetwork();
    this.actionPlanBeneficiary();
    this.handlePlatforms();
  }

  handlePlatforms() {
    this.setState({ error: null });
    if (Platform.OS === 'android') {
      this.handleAndroid();
    } else {
      this.fetchPosition();
    }
  }

  private async handleAndroid() {
    const granted = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (granted) {
      this.fetchPosition();
      return;
    }

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === 'denied' || status === 'never_ask_again') {
      this.setState({ location: null, error: { permissionDenied: true } });
    } else {
      this.fetchPosition();
    }
  }

  private fetchPosition() {
    Geolocation.getCurrentPosition(
      this.successLocationHandler,
      this.errorLocationHandler,
    );
  }

  private successLocationHandler = (location: any) => {
    const coordsPricipal = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    };
    this.setState({ location: coordsPricipal });

    // console.warn(coordsMock);
  };

  private errorLocationHandler = (error: any) => {
    const permissionDenied: boolean = error.code === error.PERMISSION_DENIED;
    this.setState({ location: null, error: { permissionDenied } });
  };

  private handleError = (error: ApolloError) => {
    this.setState({ loading: false });
    FlashMessageService.showError(ErrorMapper.map(error));
  };

  private actionPlanBeneficiary() {
    this.setState({ loading: true });
    this.graphQLProvider.query(
      'beneficiary-info.query',
      null,
      this.handlePlanBeneficiaryInfoSuccess,
      this.handleError,
    );
  }

  private actionPlanNetwork() {
    this.setState({ loading: true });
    const variables: CarePointNetworkPerPlanInput = {
      planCode: this.codePlan,
    };

    this.graphQLProvider.query<CarePointsNetworkPerPlanQueryVariables>(
      'care-point-network-per-plan',
      { data: variables },
      this.handlePlanNetworkSuccess,
      this.handleError,
    );
  }

  private handleChangeType = (valueType: string) => {
    this.typeSelected = valueType;
    this.setState({
      types: this.state.types,
      selectedType: valueType,
      states: [],
      selectedState: '',
      cities: [],
      selectedCity: '',
      districts: [],
      selectedDistrict: 'Todos',
      specialities: [],
      selectedSpeciality: '',
    });
    this.specialitySelected = '';
    this.stateSelected = '';
    this.citySelected = '';
    this.districtSelected = '';

    this.citySelected = '';
    this.controlSearch = false;
    this.actionSpeciality();
    if (Platform.OS === 'ios') {
      // this.actionSpeciality();
    }
  };

  private handleChangeSpeciality = (valueSpeciality: string) => {
    this.specialitySelected = valueSpeciality;
    this.setState({
      types: this.state.types,
      selectedType: this.state.selectedType,
      specialities: this.state.specialities,
      selectedSpeciality: this.state.selectedSpeciality,

      states: [],
      selectedState: '',
      cities: [],
      selectedCity: '',
      districts: [],
      selectedDistrict: 'Todos',
    });

    this.stateSelected = '';
    this.citySelected = '';
    this.districtSelected = '';

    this.actionState();
    this.controlSearch = false;
    if (Platform.OS === 'ios') {
      // this.actionSpeciality();
    }
  };

  private handleChangeState = (valueState: string) => {
    this.stateSelected = valueState;
    this.citySelected = '';
    this.districtSelected = '';
    this.setState({
      states: this.state.states,
      selectedState: valueState,
      cities: [],
      selectedCity: '',
      districts: [],
      selectedDistrict: 'Todos',
      types: this.state.types,
      selectedType: this.state.selectedType,
      specialities: this.state.specialities,
      selectedSpeciality: this.state.selectedSpeciality,
    });

    this.citySelected = '';
    this.districtSelected = '';

    this.actionCity();
    this.controlSearch = false;
    // this.controlType = false;
    // this.controlSpeciality = false;
  };

  private handleChangeCity = (valueCity: string) => {
    this.citySelected = valueCity;
    this.districtSelected = 'Todos';
    this.setState({
      states: this.state.states,
      selectedState: this.state.selectedState,
      cities: this.state.cities,
      selectedCity: valueCity,
      types: this.state.types,
      selectedType: this.state.selectedType,
      specialities: this.state.specialities,
      selectedSpeciality: this.state.selectedSpeciality,
      districts: [],
      selectedDistrict: 'Todos',
    });
    this.districtSelected = '';

    this.actionDistrict();
    this.controlSearch = false;
    if (valueCity !== '') {
      this.controlSearch = true;
    }
    if (Platform.OS === 'ios') {
      // this.actionType();
      // this.actionSpeciality();
    }
  };

  private actionType() {
    this.setState({ loading: true });
    const variables: CarePointNetworkTypeInput = {
      uf: '',
      cityCode: 0,
      networkCode: this.state.networkPlan.toString(),
    };
    this.graphQLProvider.query<CarePointsNetworkTypeQueryVariables>(
      'care-point-network-type',
      { data: variables },
      this.handleTypeSuccess,
      this.handleError,
    );
  }

  private handlePlanBeneficiaryInfoSuccess = (res: BeneficiaryInfoQuery) => {
    const benef = res.BeneficiaryInfo;
    this.codePlan = benef.codePlan;
    this.actionPlanNetwork();
  };

  private handlePlanNetworkSuccess = (res: CarePointsNetworkPerPlanQuery) => {
    const carePointsNetworkPerPlan = res.CarePointsNetworkPerPlan;

    this.setState({
      networkPlan: carePointsNetworkPerPlan.networkCode,
      networkName: carePointsNetworkPerPlan.comercialName,
    });

    this.actionType();
  };

  private handleTypeSuccess = (res: CarePointsNetworkTypeQuery) => {
    const carePointsNetworkType: PickerFieldItem[] =
      res.CarePointsNetworkType.map(this.mapToListType);

    carePointsNetworkType.unshift({
      value: '',
      label: 'Selecione um Tipo de Serviço',
    });

    this.setState({
      loading: false,
      types: carePointsNetworkType,
      selectedType: carePointsNetworkType[0].value.toString(),
    });
  };

  private actionSpeciality() {
    if (this.typeSelected != '') {
      this.setState({ loading: true });
      const variables: CarePointNetworkSpecialityInput = {
        uf: '',
        cityCode: 0,
        bookletCode: Number(this.typeSelected),
        networkCode: this.state.networkPlan,
      };

      this.graphQLProvider.query<CarePointsNetworkSpecialityQueryVariables>(
        'care-point-network-speciality',
        { data: variables },
        this.handleSpecialitySuccess,
        this.handleError,
      );
    }
  }

  private handleSpecialitySuccess = (res: CarePointsNetworkSpecialityQuery) => {
    const carePointsNetworkSpeciality: PickerFieldItem[] =
      res.CarePointsNetworkSpeciality.map(this.mapToListSpeciality);
    carePointsNetworkSpeciality.unshift({
      value: '',
      label: 'Selecione uma Especialidade',
    });
    this.setState({
      loading: false,
      specialities: carePointsNetworkSpeciality,
    });
  };

  private actionState() {
    if (this.specialitySelected != '') {
      this.setState({ loading: true });
      const variables: CarePointNetworkStateInput = {
        bookletCode: Number(this.typeSelected),
        networkCode: this.state.networkPlan,
        specialityDesc: this.specialitySelected,
      };
      this.graphQLProvider.query<CarePointsNetworkStateQueryVariables>(
        'care-point-network-state',
        { data: variables },
        this.handleStateSuccess,
        this.handleError,
      );
    }
  }

  private handleStateSuccess = (res: CarePointsNetworkStateQuery) => {
    const carePointsNetworkStates: PickerFieldItem[] =
      res.CarePointsNetworkState.map(this.mapToListState);
    carePointsNetworkStates.unshift({
      value: '',
      label: 'Selecione um Estado',
    });
    // tslint:disable-next-line: max-line-length
    this.setState({
      loading: false,
      states: carePointsNetworkStates,
      selectedState: carePointsNetworkStates[0].value.toString(),
    });
  };

  private actionCity() {
    if (this.stateSelected !== '') {
      this.setState({ loading: true });
      const variables: CarePointNetworkCityInput = {
        uf: this.stateSelected,
        bookletCode: Number(this.typeSelected),
        networkCode: this.state.networkPlan,
        specialityDesc: this.specialitySelected,
      };

      this.graphQLProvider.query<CarePointsNetworkCityQueryVariables>(
        'care-point-network-city',
        { data: variables },
        this.handleCitySuccess,
        this.handleError,
      );
    } else {
      this.districtSelected = '';
      // this.setState({ districts: [] });
    }
  }

  private handleCitySuccess = (res: CarePointsNetworkCityQuery) => {
    const carePointsNetworkCity: PickerFieldItem[] =
      res.CarePointsNetworkCity.map(this.mapToListCity);
    carePointsNetworkCity.unshift({ value: '', label: 'Selecione uma Cidade' });
    // tslint:disable-next-line: max-line-length
    this.setState({
      loading: false,
      cities: carePointsNetworkCity,
      selectedCity: carePointsNetworkCity[0].value.toString(),
    });
  };

  private actionDistrict() {
    if (this.citySelected !== '') {
      this.setState({ loading: true });
      const variables: CarePointNetworkDistrictInput = {
        uf: this.stateSelected,
        bookletCode: Number(this.typeSelected),
        networkCode: this.state.networkPlan,
        cityDesc: this.citySelected,
        specialityDesc: this.specialitySelected,
      };

      this.graphQLProvider.query<CarePointsNetworkDistrictQueryVariables>(
        'care-point-network-district',
        { data: variables },
        this.handleDistrictSuccess,
        this.handleError,
      );
    }
  }

  private handleDistrictSuccess = (res: CarePointsNetworkDistrictQuery) => {
    const carePointsNetworkDistrict: PickerFieldItem[] =
      res.CarePointsNetworkDistrict.map(this.mapToListDistrict);
    carePointsNetworkDistrict.unshift({ value: 'Todos', label: 'Todos' });

    this.setState({
      loading: false,
      districts: carePointsNetworkDistrict,
      selectedDistrict: carePointsNetworkDistrict[0].value.toString(),
    });
  };

  private handleChangeDistrict = (valueDistrict: string) => {
    this.districtSelected = valueDistrict;
    this.setState({
      districts: this.state.districts,
      selectedDistrict: valueDistrict,
      states: this.state.states,
      selectedState: this.state.selectedState,
      cities: this.state.cities,
      selectedCity: this.state.selectedCity,
      types: this.state.types,
      selectedType: this.state.selectedType,
      specialities: this.state.specialities,
      selectedSpeciality: this.state.selectedSpeciality,
    });
    if (Platform.OS === 'ios') {
      this.actionSpeciality();
    }
  };

  private async actionSearch() {
    // this.localDataSource.removeMulti([LOCAL_STORAGE_SEARCH_NETWORK_DATA]);

    this.setState({ loading: true });

    const variables: CarePointNetworkFilterGamaInput = {
      networkCode: this.state.networkPlan,
      bookletCode: Number(this.typeSelected),
      specialityDesc: this.specialitySelected,
      latitude: this.state.location.latitude,
      longitude: this.state.location.longitude,
      maxDistance: this.state.maxDistance,
      state: this.stateSelected,
      city: this.citySelected,
      district: this.districtSelected,
    };
    // TODO refact to REST
    let res = await fetchEmergencyCareGamaFilterPoints(variables);
    if (res.error) {
      this.handleError(res.error);
    } else {
      this.handleSearchSuccess({ EmergencyCareGamaFilter: res.data });
    }
    // this.graphQLProvider.query<EmergencyCareGamaFilterQueryVariables>(
    //   'emergency-care-gama-filter',
    //   { data: variables },
    //   this.handleSearchSuccess,
    //   this.handleError,
    // );

    let network: NetworkGamaParam = new NetworkGamaParam();

    network.networkCode = variables.networkCode;
    network.bookletCode = variables.bookletCode;
    network.specialityDesc = variables.specialityDesc;
    network.latitude = variables.latitude;
    network.longitude = variables.longitude;
    network.maxDistance = variables.maxDistance;
    network.uf = variables.state;
    network.city = variables.city;
    network.district = variables.district;

    this.setNetwork(network);
  }

  async setNetwork(network: NetworkParam): Promise<void> {
    await Promise.all([
      this.localDataSource.set<NetworkParam>(
        LOCAL_STORAGE_SEARCH_NETWORK_DATA,
        network,
      ),
    ]);
  }

  private handleSearchSuccess = (res: EmergencyCareGamaFilterQuery | any) => {
    this.setState({ loading: false, networks: res });

    Navigation.push<EmergencyGamaNetworkPageProps>(
      this.props.componentId,
      EmergencyGamaNetworkPage,
      {
        networks: this.state.networks,
        location: this.state.location,
      },
    );
  };

  private handleSubmit = (formData: FormSubmitData) => {
    this.setState({ selectedDistrict: formData.data.district });
    this.actionSearch();
  };

  private mapToListType = (res: any): PickerFieldItem => {
    return {
      value: res.bookletCode,
      label: res.bookletDesc,
    };
  };

  private mapToListSpeciality = (res: any): PickerFieldItem => {
    return {
      value: res.specialityDesc,
      label: res.specialityDesc,
    };
  };

  private mapToListState = (res: any): PickerFieldItem => {
    return {
      value: res.uf,
      label: res.ufName + ' (' + res.total + ')',
    };
  };

  private mapToListCity = (res: any): PickerFieldItem => {
    return {
      value: res.cityName,
      label: res.cityName,
    };
  };

  private mapToListDistrict = (res: any): PickerFieldItem => {
    return {
      value: res.districtName,
      label: res.districtName + ' (' + res.total + ')',
    };
  };
}
