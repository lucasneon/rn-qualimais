// tslint:disable:max-line-length
export const EmergencyCareStrings = {
  Title: 'Serviços de saúde',
  CheckRoute: 'Ver como chegar',
  Refund:
    'Você não terá reembolso se for diretamente ao hospital sem avisar sua Guia. Clique para falar com ela.',
  Map: 'Mapa',
  List: 'Lista',
  NotFound: {
    Title: 'Serviço de Saúde não encontrado',
    TitleLocation: 'Precisamos da sua localização',
    Message:
      'Não foram encontrados pronto-atendimentos próximos a você. Por favor, entre em contato com o seu representante disponível através do chat.',
    MessageLocation:
      'Por favor, Habilite o GPS para encontrar pronto-atendimentos próximos a você.',
  },

  // BUSCA DE REDE GAMA
  NetworkTitle: 'Busca de Rede',
};
