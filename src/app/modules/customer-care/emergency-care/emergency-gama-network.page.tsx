import * as React from 'react';
import { PageView } from '@app/core/analytics';
import { ListMap } from '@app/modules/common';

import { Scroll } from '@atomic';
import { NavigationPageProps } from '@app/core/navigation';
import { GeoCoordinates } from '@app/model';
import { EmergencyCareGamaFilterQuery, CarePointNetworkFragment, CarePointFragment } from '@app/data/graphql';
import { ListMapItem } from '@app/modules/customer-care/emergency-network/emergency-network-state.page';
import { EmergencyNetworkStrings } from '@app/modules/customer-care/emergency-network/emergency-network.strings';


export interface EmergencyGamaNetworkPageProps {
    networks: EmergencyCareGamaFilterQuery;
    location?: GeoCoordinates;
}

@PageView('EmergencyGamaNetwork')
export class EmergencyGamaNetworkPage extends React.Component<NavigationPageProps<EmergencyGamaNetworkPageProps>> {
  static options = { name: 'EmergencyGamaNetworkPage' };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const emergencyNetworkPoints = this.props.navigation.networks;
    const userLocation = this.props.navigation.location;
    // tslint:disable-next-line: max-line-length
    const items: ListMapItem[] = emergencyNetworkPoints.EmergencyCareGamaFilter && emergencyNetworkPoints.EmergencyCareGamaFilter.map(this.mapToListItem);

    return (
        <Scroll>
            <ListMap
                loading={false}
                userLocation={userLocation}
                items={items}
                title={EmergencyNetworkStrings.Title}
                notFoundTitle={EmergencyNetworkStrings.NotFound.Title}
                notFoundMessage={EmergencyNetworkStrings.NotFound.Message}
            />
        </Scroll>
      );
  }


  private mapToListItem = (res: any): ListMapItem => {

    //res.distance = (Math.round((res.distance / 1000) * 100) / 100) * 1000;

    if (res.distance !== 0) {
      res.distance = (Math.round((res.distance / 1000) * 100) / 100) * 1000;
    } else {
      res.distance = null;
    }

    return {
      title: res.carePoint.name,
      key: res.practitionerId,
      distance: res.distance,
      carePoint: res.carePoint,
      isFavorite: res.isFavorite,
    };
  }

}