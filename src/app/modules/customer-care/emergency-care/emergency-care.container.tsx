import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { Subscribe } from 'unstated';
import { QueryContainer } from '@app/core/graphql';
import {
  EmergencyCareQuery,
  EmergencyCareQueryVariables,
} from '@app/data/graphql';
import { GeoCoordinates } from '@app/model';
import {
  LocationError,
  LocationProvider,
  LocationState,
} from '@app/services/providers';
import { EmergencyCareProvider } from '@app/services/providers/emergency-care.provider';

export interface EmergencyCareRes {
  loading: boolean;
  locationError?: LocationError;
  userLocation?: GeoCoordinates;
  queryRes: QueryResult<EmergencyCareQuery, EmergencyCareQueryVariables>;
}

export interface EmergencyCareContainerProps {
  children: (res: EmergencyCareRes) => React.ReactNode;
  codigoGrupoLivreto: string;
}

const MaxDistance = 400; // Kilometers

export class EmergencyCareContainer extends React.PureComponent<EmergencyCareContainerProps> {
  private locationProvider: LocationProvider;
  private emergencyCareProvider: EmergencyCareProvider;
  private data;

  componentDidMount() {
    this.locationProvider.handlePlatforms().then(() => {
      this.emergencyCareProvider.fetch(this.data);
    });
  }

  render() {
    return (
      <Subscribe to={[LocationProvider, EmergencyCareProvider]}>
        {(
          locationProvider: LocationProvider,
          emergencyCareProvider: EmergencyCareProvider,
        ) => {
          this.locationProvider = locationProvider;
          this.emergencyCareProvider = emergencyCareProvider;
          const state: LocationState = locationProvider.state;
          const location = state.location;

          if (!location) {
            return this.props.children({
              loading: false,
              userLocation: null,
              locationError: state.error,
              queryRes: null,
            });
          }

          this.data = {
            latitude: location?.latitude ?? -25.421432110057328,
            longitude: location?.longitude ?? -49.2602354745277,
            maxDistance: MaxDistance,
            codigoGrupoLivreto: this.props.codigoGrupoLivreto,
          };

          return this.props.children({
            loading: this.emergencyCareProvider.state.loading,
            userLocation: location,
            locationError: state.error,
            queryRes: this.emergencyCareProvider.state.data,
          });

          //TODO GRAPHQL
          // return (
          //   <QueryContainer document="emergency-care" variables={{ data }}>
          //     {(
          //       res: QueryResult<
          //         EmergencyCareQuery,
          //         EmergencyCareQueryVariables
          //       >,
          //     ) => {
          //       return this.props.children({
          //         loading: res.loading,
          //         userLocation: location,
          //         locationError: state.error,
          //         queryRes: res,
          //       });
          //     }}
          //   </QueryContainer>
          // );
        }}
      </Subscribe>
    );
  }
}
