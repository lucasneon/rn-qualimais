import * as React from 'react';
import { Platform } from 'react-native';
import {
  ActionSheet,
  BodyWhite,
  NeutralButton,
  SecondaryButton,
  VBox,
  VSeparator,
} from '@atomic';
import { CustomerCareStrings } from './customer-care.strings';

export type AppointmentSheetAction = 'consulta' | 'exame';

export interface AppointmentSheetProps {
  visible: boolean;
  onActionTaken: () => void;
  onDismiss: (action?: AppointmentSheetAction) => void;
}

const Strings = CustomerCareStrings.Appointment;

export class AppointmentSheet extends React.PureComponent<AppointmentSheetProps> {
  private action?: AppointmentSheetAction;

  // tslint:disable:max-line-length
  render() {
    return (
      <ActionSheet
        visible={this.props.visible}
        onCancel={this.handleCancel}
        onDismiss={this.handleDismiss}>
        <VBox>
          <BodyWhite>Escolha o tipo de serviço</BodyWhite>
          <VSeparator />
          <SecondaryButton
            expanded={true}
            text="Consulta"
            onTap={this.handleConsulta}
          />
          <VSeparator />
          <SecondaryButton
            expanded={true}
            text="Exame"
            onTap={this.handleExame}
          />
          <VSeparator />
          <NeutralButton
            expanded={true}
            onTap={this.handleCancel}
            text="Voltar"
          />
          <VSeparator />
        </VBox>
      </ActionSheet>
    );
  }

  private handleCancel = () => {
    this.action = null;
    this.props.onActionTaken();
    this.handleActionAndroid();
  };

  private handleConsulta = () => {
    this.action = 'consulta';
    this.props.onActionTaken();
    this.handleActionAndroid();
  };

  private handleExame = () => {
    this.action = 'exame';
    this.props.onActionTaken();
    this.handleActionAndroid();
  };

  private handleDismiss = () => {
    this.props.onDismiss(this.action);
  };

  // Doing this on Android because Modal onDissmiss property is iOS only
  private handleActionAndroid() {
    if (Platform.OS === 'android') {
      this.props.onDismiss(this.action);
    }
  }
}
