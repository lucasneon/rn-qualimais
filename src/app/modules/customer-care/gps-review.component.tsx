import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { Container } from 'typedi';
import { CardActionSheet } from '@app/components/mol.card-action-sheet';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { ReviewCardContent, ReviewCardContentShimmer } from '@app/components/obj.review';
import { GraphQLProvider, GraphQLProviderToken, QueryContainer } from '@app/core/graphql';
import { FlashMessageService } from '@app/core/navigation';
import {
  RateGpsInput, RateGpsMutation, RateGpsMutationVariables, ResponsibleGuideFragment, ResponsibleGuideQuery
} from '@app/data/graphql';
import { AppError } from '@app/model';
import { ErrorMapper } from '@app/utils';
import { LoadingState } from '@atomic';
import { CustomerCareStrings } from './customer-care.strings';

export interface GpsReviewProps {
  visible: boolean;
  onFinish: () => void;
}

interface GpsReviewState {
  loadingReview: boolean;
}

const Strings = CustomerCareStrings.GpsReview;

export class GpsReview extends React.PureComponent<GpsReviewProps, GpsReviewState> {

  private readonly graphqlProvider: GraphQLProvider = Container.get(GraphQLProviderToken);

  private gps: ResponsibleGuideFragment;

  constructor(props) {
    super(props);
    this.state = { loadingReview: null };
  }

  render() {
    return (
      <CardActionSheet visible={this.props.visible} onCancel={this.props.onFinish}>
        <QueryContainer document='responsible-guide'>
          {(res: QueryResult<ResponsibleGuideQuery>) => {
            this.gps = res.data && res.data.ResponsibleGuide;

            return (
              <LoadingState loading={res.loading} error={!!res.error} data={!!this.gps}>
                <LoadingState.Shimmer>
                  <ReviewCardContentShimmer />
                </LoadingState.Shimmer>
                <LoadingState.ErrorPlaceholder>
                  <PlaceholderSelector small={true} error={ErrorMapper.map(res.error)} onTap={res.refetch} />
                </LoadingState.ErrorPlaceholder>
                {this.gps && (
                  <ReviewCardContent
                    title={Strings.Title}
                    description={Strings.Description}
                    loading={this.state.loadingReview}
                    onCancel={this.props.onFinish}
                    onSend={this.handleSendReview}
                  />
                )}
              </LoadingState>
            );
          }}
        </QueryContainer>
      </CardActionSheet>
    );
  }

  private handleSendReview = (evaluatedValue: number, comment?: string) => {
    const input: RateGpsInput = {
      gpsCode: this.gps.code,
      rating: evaluatedValue,
      observation: comment,
    };

    this.setState({ loadingReview: true });
    this.graphqlProvider.mutate<RateGpsMutationVariables>(
      'rate-gps',
      { data: input },
      (res: RateGpsMutation) => this.handleReviewSuccess(res.RateGps),
      (error: ApolloError) => this.handleReviewError(ErrorMapper.map(error)),
    );
  }

  private handleReviewSuccess = (message: string) => {
    this.setState({ loadingReview: false });
    FlashMessageService.showSuccess(message);
    this.props.onFinish();
  }

  private handleReviewError = (error: AppError) => {
    this.setState({ loadingReview: false });
    FlashMessageService.showError(error);
  }
}
