export const CustomerCareStrings = {
  Title: 'Atendimento',
  PreSchedule: 'Pré-agendar consulta',
  AskTeam: 'Chat',
  Success: 'Consulta pré-agendada com sucesso! Aguarde o contato da equipe para consolidar o agendamento.',
  SearchHealthService: 'Buscar serviço de saúde',
  Appointment: {
    Question: 'Que tipo de consulta você precisa?',
    Specialty: 'Outra especialidade',
    Gynecology: 'Seu Médico Ginecologista',
  },
  GpsReview: {
    Title: 'Avaliar Guia',
    Description: 'Por favor, faça a avaliação do atendimento que você acabou de receber.',
  },
};
