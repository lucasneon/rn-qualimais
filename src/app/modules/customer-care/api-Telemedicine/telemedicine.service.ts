import {
  TELEMEDICINE_BASE_API,
  TELEMEDICINE_PASSWORD,
  TELEMEDICINE_USERNAME,
} from '@app/config';
import { LocalDataSource } from '@app/data';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services';
import axios from 'axios';
import moment from 'moment';
import Container from 'typedi';

const tokenBaseUrl: string = Container.get(TELEMEDICINE_BASE_API);
const username: string = Container.get(TELEMEDICINE_USERNAME);
const password: string = Container.get(TELEMEDICINE_PASSWORD);

const beneficiaryProvider: BeneficiaryProvider = Container.get(
  BeneficiaryProviderToken,
);

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

const getToken = async () => {
  const data = {
    credentials: {
      username: username,
      password: password,
    },
  };

  const res = await axios({
    method: 'post',
    url: `${tokenBaseUrl}/api/auth/v1/signin/email/token`,
    data: data,
    headers: {
      'Content-Type': `application/json`,
    },
  }).catch(err => {
    console.error(err);
    return null;
  });

  console.warn('Token Telemedicina >', res.data.token);
  return res.data.token;
};

export const fetchInvitePatient = async () => {
  const token = await getToken();
  console.warn(token);
  if (!token) {
    return;
  }

  await Promise.all([localDataSource.set<string>('telemedicine_token', token)]);
  const cpf = await localDataSource.get('cpf');

  const fullName = beneficiaryProvider.state.active.name ?? '';
  const listName = fullName.split(' ');
  let firstName = '';
  let lastName = '';
  if (listName) {
    firstName = listName[0];
    lastName = listName[listName.length - 1] ?? '';
  }

  const data = {
    patient_invite_view_model: {
      email: beneficiaryProvider.state.active.email ?? 'lucas@gmail.com',
      phone: beneficiaryProvider.state.active.phone ?? '11979678888',
      first_name: firstName,
      last_name: lastName,
      date_birth:
        moment(beneficiaryProvider.state.active.birthDate).format(
          'YYYY-MM-DD',
        ) ?? '1995-03-24',
      gender: beneficiaryProvider.state.active.gender ?? 'Male',
      generate_link: true,
    },
    cpf: cpf,
  };

  console.warn('Data >', data);

  return axios({
    method: 'post',
    url: `${tokenBaseUrl}/api/auth/v1/invites/imports/patient/custom`,
    data,
    headers: {
      'Content-Type': `application/json`,
      Authorization: token,
    },
  })
    .then(response => {
      console.warn('Response Telemedicina >', response.data);
      const data = response.data;
      return data;
    })
    .catch(err => {
      console.error('Error telemedicina', err);
      return null;
    });
};
