export interface Voucher {
    numero: string;
    codigoEstipulante: string;
    codigoBeneficiario: string;
    contratoPrestador: string;
    nomePrestador: string;
    status: string;
    motivoStatus: string;
    dataEmissao: string;
    dataUtilizacao: string;
    dataCancelamento: string;
    nomeTipoServico: string;
    tipoVoucher: string;
    codigoProcedimento: string;
    nomeProcedimento: string;
    especialidadeCBOS: string;
    nomeEspecialidadeCBOS: string;
    valorProcedimento: string;
    dataInicioValidade: string;
    dataFimValidade: string;
    dataSolicMedico: string;
    nomeProfisSolic: string;
    especialidadeProfisSolic: string;
    nomePrestadorSolic: string;
}