import axios from 'axios';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services';
import Container from 'typedi';
import { VOUCHER_TOKEN } from '@app/config';

const beneficiaryProvider: BeneficiaryProvider = Container.get(
  BeneficiaryProviderToken,
);

const urlToken: string = Container.get(VOUCHER_TOKEN);

export const getToken = async () => {
  const params = {
    grant_type: 'password',
    username: 'QualimaisMobile',
    password: 'L1v3t02chS3rv1',
  };

  const data = Object.keys(params)
    .map(key => `${key}=${encodeURIComponent(params[key])}`)
    .join('&');

  return await axios({
    method: 'post',
    url: urlToken,
    data: data,
    headers: {
      'Content-Type': `application/x-www-form-urlencoded`,
      Authorization: 'Basic bW9iaWxlOm1vYmlsZQ==',
    },
  })
    .then(response => response.data.access_token)
    .catch(err => {
      console.log(' Failure ' + err);
    });
};

export const fetchVouchers = () => {
  const idHeathCareCard =
    beneficiaryProvider.state.active.idHealthCareCard ?? '';
  getToken().then(accessToken => {
    console.warn('Token > ' + accessToken);
    return axios
      .get(
        `https://webservices.crc.com.br/gamasaudewsapphml/api/voucher?codigoSistema=2&codigoOperadora=39&codigoEstipulante=1&cpf=0001015062005&tipoServico=?codigoSistema=2&codigoOperadora=39&codigoEstipulante=1&cpf=${idHeathCareCard}&tipoServico=`,
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
            Accept: 'application/json',
          },
        },
      )
      .then(response => {
        return response.data;
      })
      .then(response => {
        return response;
      });
  });
};
