import React from 'react';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { PageView } from '@app/core/analytics';
import { DividerGray, Root } from '@atomic';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { TicketCell } from '@app/components/org.voucher/voucher-cell.component';
import { TicketStrings } from '@app/components/org.voucher/voucher.strings';
import { VBox, VSeparator, H1, DD } from '@atomic';
import { StyleSheet, View } from 'react-native';

export interface VoucherDetailsPageProps {
  componentId?: string;
  nomeProcedimento: string;
  status: string;
  item: any;
}

interface VoucherDetailsPageState {}

@PageView('VoucherDetails')
export class VoucherDetailsPage extends React.Component<
  NavigationPageProps<VoucherDetailsPageProps>,
  VoucherDetailsPageState
> {
  static options = {
    name: 'VoucherDetailsPage',
    topBar: { backButton: { visible: true } },
  };

  constructor(props) {
    super(props);
    console.log('propsNavigation ' + this.props.navigation.item);
    Navigation.events().bindComponent(this);
    this.state = {
      isLoading: true,
    };
  }

  componentDidMount() {}

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  render() {
    var item = this.props.navigation.item;
    return (
      <Root>
        <CollapsibleHeader title="Vouchers">
          {this.ticket(item)}
          <VBox style={{}}>
            <VSeparator />
            <H1>Status</H1>
            <DD>{item.status ?? 'SEM INFORMAÇÃO'}</DD>
            <VSeparator />
            <DividerGray />
            <H1>Tipo de Voucher</H1>
            <DD>{item.tipoVoucher ?? 'SEM INFORMAÇÃO'}</DD>
            <VSeparator />
            <DividerGray />
            <H1>Tipo de Serviço</H1>
            <DD>{item.nomeTipoServico ?? 'SEM INFORMAÇÃO'}</DD>
            <VSeparator />
            <DividerGray />
            <H1>Procedimento</H1>
            <DD>{item.nomeProcedimento ?? 'SEM INFORMAÇÃO'}</DD>
            <VSeparator />
            <DividerGray />
            <H1>Especialidade</H1>
            <DD>{item.nomeEspecialidadeCBOS ?? 'SEM INFORMAÇÃO'}</DD>
            <VSeparator />
            <DividerGray />
            <H1>Início de Vigência</H1>
            <DD>{item.dataInicioValidade ?? 'SEM INFORMAÇÃO'}</DD>
            <VSeparator />
            <DividerGray />
            <H1>Final de Vigência</H1>
            <DD>{item.dataFimValidade ?? 'SEM INFORMAÇÃO'}</DD>
            <VSeparator />
            <DividerGray />
            <H1>Data de Solicitação do Pedido</H1>
            <DD>{item.dataEmissao ?? 'SEM INFORMAÇÃO'}</DD>
            {this.removeAccents(item.status) !== 'INDISPONIVEL' ? (
              <View>
                <VSeparator />
                <DividerGray />
                <H1>Profissional Solicitante</H1>
                <DD>{item.nomeProfisSolic ?? 'SEM INFORMAÇÃO'}</DD>
                <VSeparator />
                <DividerGray />
                <H1>Especialidade do Profissional Solicitante</H1>
                <DD>{item.especialidadeProfisSolic ?? 'SEM INFORMAÇÃO'}</DD>
              </View>
            ) : (
              <View />
            )}
            <VSeparator />
            <VSeparator />
            <VSeparator />
          </VBox>
        </CollapsibleHeader>
      </Root>
    );
  }

  private ticket = item => {
    return (
      <TicketCell
        nomeProcedimento={item.nomeProcedimento}
        id={item.id}
        voucher={item}
        nomeTipoServico={item.nomeTipoServico.toUpperCase()}
        status={this.removeAccents(item.status).toUpperCase()}
        tag={TicketStrings.Tag.toUpperCase()}
        numero={item.numero}
        dataFimValidade={item.dataFimValidade}
        dataInicioValidade={item.dataInicioValidade}
        motivoStatus={item.motivoStatus}
        hideCheck={true}
      />
    );
  };

  private removeAccents(originalText: string) {
    console.log(originalText);
    var result = originalText.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    console.log(result);
    return result;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  lineContainerNormal: {
    flexDirection: 'column',
    borderBottomColor: '#00619a',
    borderBottomWidth: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
  },

  lineContainerPress: {
    flexDirection: 'column',
    borderBottomColor: '#9e9e9e',
    borderBottomWidth: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
  },
});
