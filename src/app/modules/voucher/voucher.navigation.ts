import { Navigation } from '@app/core/navigation';
import { VoucherPage,VoucherDetailsPage } from '../voucher/index';
export const VoucherNavigation = {
  register: () => {
    Navigation.register(VoucherPage);
    Navigation.register(VoucherDetailsPage);
  },
};