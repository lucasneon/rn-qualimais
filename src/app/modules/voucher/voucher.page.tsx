import React from 'react';
import { GuardProps, Navigation } from '@app/core/navigation';
import { GuardParams } from '../authentication';
import { PageView } from '@app/core/analytics';
import {
  VoucherDetailsPage,
  VoucherDetailsPageProps,
} from './voucher-details.page';
import { LinkButton } from '@atomic';
import { TicketCell } from '@app/components/org.voucher/voucher-cell.component';
import { TicketStrings } from '@app/components/org.voucher/voucher.strings';
import { FlatList, StyleSheet, ActivityIndicator, View } from 'react-native';
import { DT } from '@atomic';
import { CollapsibleHeader as Collapsible } from '@atomic';
import { LargeHeader } from '@app/components/atm.header';
import VoucherApi, { fetchVouchers, getToken } from './voucher.api';
import axios from 'axios';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services';
import Container from 'typedi';
import { VOUCHER_URL } from '@app/config';

interface VoucherPageProps extends GuardProps<GuardParams> {
  componentId: string;
}

interface VoucherPageState {}

const voucherUrl: string = Container.get(VOUCHER_URL);

@PageView('Voucher')
export class VoucherPage extends React.Component<
  VoucherPageProps,
  VoucherPageState
> {
  static options = {
    name: 'VoucherPage',
    topBar: { backButton: { visible: true } },
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      isLoading: false,
      firstTime: true,
      isPress: true,
      isPress2: false,
      isPress3: false,
      dataSource: [],
      responseV: [],
    };
  }

  async componentDidMount() {
    this.fetchVouchers();
  }

  private beneficiaryProvider: BeneficiaryProvider = Container.get(
    BeneficiaryProviderToken,
  );

  async fetchVouchers() {
    this.setState({
      isLoading: true,
    });
    const idHeathCareCard =
      this.beneficiaryProvider.state.active.idHealthCareCard ?? '';
    getToken().then(accessToken => {
      console.warn('Token > ' + accessToken);
      return axios
        .get(
          `${voucherUrl}/voucher?codigoSistema=2&codigoOperadora=39&codigoEstipulante=1&cpf=${idHeathCareCard}&tipoServico=`,
          {
            headers: {
              Authorization: `Bearer ${accessToken}`,
              Accept: 'application/json',
            },
          },
        )
        .then(response => {
          console.warn(response);
          return response.data;
        })
        .then(response => {
          this.setState({
            isLoading: false,
            dataSource: response,
            responseV: response,
          });
        })
        .catch(() => {
          this.setState({
            isLoading: false,
            dataSource: [],
            responseV: [],
          });
        });
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <ActivityIndicator
          color="#171a88"
          size="large"
          style={styles.container}
          animating
        />
      );
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Collapsible.LargeHeader>
            <LargeHeader title={'Vouchers'} />
            <View style={styles.topTabBar}>
              {this.button('Disponíveis', this.showDisponiveis)}
              {this.button2('Indisponíveis', this.showIndisponiveis)}
              {this.button3('Utilizados', this.showUtilizados)}
            </View>
          </Collapsible.LargeHeader>
          {this.isEmpty() ? (
            <View style={styles.containerEmpty}>
              <DT>Nenhum voucher disponível</DT>
            </View>
          ) : (
            this.list()
          )}
        </View>
      );
    }
  }

  private isEmpty = () => {
    const list = this.state.dataSource;
    return list.length === 0 || list === [] || list === null;
  };

  private list = () => {
    return (
      <FlatList
        data={
          this.state.firstTime ? this.showDisponiveis() : this.state.dataSource
        }
        renderItem={this.ticket}
        keyExtractor={item => item}
      />
    );
  };

  private progress = () => {
    return (
      <ActivityIndicator
        color="#009688"
        size="large"
        style={styles.container}
        animating
      />
    );
  };

  private ticket = ({ item }) => {
    return (
      <TicketCell
        nomeProcedimento={item.nomeProcedimento}
        id={item.id}
        voucher={item}
        nomeTipoServico={item.nomeTipoServico.toUpperCase()}
        status={this.removeAccents(item.status).toUpperCase()}
        tag={TicketStrings.Tag.toUpperCase()}
        numero={item.numero}
        dataFimValidade={item.dataFimValidade}
        dataInicioValidade={item.dataInicioValidade}
        motivoStatus={item.motivoStatus}
        onTap={() => this.onClickVoucherDetailsTap(item)}
        hideCheck={true}
      />
    );
  };

  private removeAccents(originalText: string) {
    var result = originalText.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    return result;
  }

  private showIndisponiveis = () => {
    let indisponives = [];
    this.setState({
      isLoading: true,
      firstTime: false,
    });
    this.state.responseV.forEach(e => {
      if (this.removeAccents(e.status) === 'INDISPONIVEL') {
        console.log(e);
        indisponives.push(e);
      }
    });
    this.setState({
      isLoading: false,
      dataSource: indisponives,
    });
  };

  private showDisponiveis = () => {
    let list = [];
    this.setState({
      dataSource: [],
    });

    console.log('chamou disponiveis');
    this.setState({
      isLoading: true,
      firstTime: false,
    });
    this.state.responseV.forEach(e => {
      if (this.removeAccents(e.status) === 'DISPONIVEL') {
        console.log(e);
        list.push(e);
      }
    });
    this.setState({
      isLoading: false,
      dataSource: list,
    });
  };

  private showUtilizados = () => {
    let list = [];
    console.log('click utilizados');
    this.setState({
      isLoading: true,
      firstTime: false,
    });
    this.state.responseV.forEach(e => {
      if (this.removeAccents(e.status) === 'UTILIZADO') {
        console.log(e);
        list.push(e);
      }
    });
    this.setState({
      isLoading: false,
      dataSource: list,
    });
  };

  private button = (title, func) => {
    return (
      <View
        style={
          this.state.isPress
            ? styles.lineContainerNormal
            : styles.lineContainerPress
        }>
        <LinkButton
          style={styles.linkButton}
          text={title}
          onTap={() => {
            this.setState({
              isPress: true,
              isPress2: false,
              isPress3: false,
            });
            func();
          }}
        />
      </View>
    );
  };

  private button2 = (title, func) => {
    return (
      <View
        style={
          this.state.isPress2
            ? styles.lineContainerNormal
            : styles.lineContainerPress
        }>
        <LinkButton
          style={styles.linkButton}
          text={title}
          onTap={() => {
            this.setState({
              isPress: false,
              isPress2: true,
              isPress3: false,
            });
            func();
          }}
        />
      </View>
    );
  };

  private button3 = (title, func) => {
    return (
      <View
        style={
          this.state.isPress3
            ? styles.lineContainerNormal
            : styles.lineContainerPress
        }>
        <LinkButton
          style={styles.linkButton}
          text={title}
          onTap={() => {
            this.setState({
              isPress: false,
              isPress2: false,
              isPress3: true,
            });
            func();
          }}
        />
      </View>
    );
  };

  private onClickVoucherDetailsTap = voucher => {
    Navigation.showModal<VoucherDetailsPageProps>(VoucherDetailsPage, {
      nomeProcedimento: voucher.nomeProcedimento,
      status: voucher.status,
      item: voucher,
    });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  topTabBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignContent: 'space-around',
  },
  containerEmpty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  lineContainerNormal: {
    flexDirection: 'column',
    borderBottomColor: '#00619a',
    borderBottomWidth: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
  },
  linkButton: {
    color: '#171a88',
  },

  lineContainerPress: {
    flexDirection: 'column',
  },
});
