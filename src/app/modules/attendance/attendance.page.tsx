import * as React from 'react';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps, FlashMessageService } from '@app/core/navigation';
import { Scroll } from '@atomic';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import Container from 'typedi';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { ApolloError } from 'apollo-client';
import { ErrorMapper } from '@app/utils';
import { StatusAttendanceQuery } from '@app/data/graphql';
import { AttendanceStrings } from './attendance.strings';
import { ListStatusAttendance, ListStatusAttendanceItem } from './list-attendance.component';

export interface StatusAttendanceNavigationProps {
}

interface StatusAttendanceState {
  loading: boolean;  
  items: ListStatusAttendanceItem[];
}

type StatusAttendanceProps = NavigationPageProps<StatusAttendanceNavigationProps>;

@PageView('StatusAttendance')
export class StatusAttendancePage extends React.Component<StatusAttendanceProps, StatusAttendanceState> {
  static options = {
    name: 'Status de senha',
  };

  private readonly graphQLProvider: GraphQLProvider = Container.get(GraphQLProviderToken);
  
  constructor(props) {
    super(props);
    this.state = { loading: false, items: null }
    Navigation.events().bindComponent(this);
  }

  componentDidAppear() {
  }

  componentDidDisappear() {
  }

  componentDidMount() {
    this.actionAttendance();
  }

  render() {
    
    return (
      <CollapsibleHeader title={AttendanceStrings.Title}>
        <Scroll>
          <ListStatusAttendance
            loading={this.state.loading}
            items={this.state.items}
            title={AttendanceStrings.Title}
            notFoundTitle={AttendanceStrings.NotFound.Title}
            notFoundMessage={AttendanceStrings.NotFound.Message}
            onItemTap={null}
          />
        </Scroll>
      </CollapsibleHeader>
    );
  }

  private mapToAttendanceList = (res: any): ListStatusAttendanceItem => {
    return {
      title: res.attendance,
      key: res.attendance,
      attendance: res.attendance,
      status: res.status,
      dateSolicitation: res.dateSolicitation,
      dateAuthorization: res.dateAuthorization,
      dateValidity: res.dateValidity,
      practitionerName: res.practitionerName,
      typeSolicitation: res.typeSolicitation      
    };
  }

  private actionAttendance() {
    this.setState({ loading: true, });
    this.graphQLProvider.query(
      'status-attendance',
      null,
      this.handleAttendanceSuccess,
      this.handleError,
    );
  }
  
  private handleAttendanceSuccess = (res: StatusAttendanceQuery) => {    
    const items: ListStatusAttendanceItem[] = res && res.StatusAttendance.map(this.mapToAttendanceList);
    this.setState({ loading: false, items: items });
  }

  private handleError = (error: ApolloError) => {
    this.setState({ loading: false });    
    FlashMessageService.showError(ErrorMapper.map(error));
  }  
}
