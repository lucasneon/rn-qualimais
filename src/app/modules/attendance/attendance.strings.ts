export const AttendanceStrings = {
  Title: 'Status de senha',
  NotFound: {
    Title: 'Ops!',
    Message: 'Nenhuma senha encontrada.',
  },
};
