import * as React from 'react';
import { ListInfoCellShimmer } from '@app/components/mol.cell';
import { Placeholder, PlaceholderSelector } from '@app/components/mol.placeholder';
import { AppError } from '@app/model';
import { Asset, LoadingState, Root, VBox, LinkButtonProps } from '@atomic';
import { NewsList } from '@app/components/org.news-list';
import { AttendanceItem } from '@app/components/org.attendance-list';
import moment from 'moment';

export interface ListStatusAttendanceItem {
  title: string,
  key: string,
  attendance?: string;
  status?: string;
  dateSolicitation?: Date;
  dateAuthorization?: Date;
  dateValidity?: Date;
  practitionerName?: string;
  typeSolicitation?: string;
}

export interface ListStatusAttendanceProps {
  loading: boolean;
  items?: ListStatusAttendanceItem[];
  error?: AppError;
  refetch?: () => void;
  title: string;
  notFoundTitle: string;
  notFoundMessage: string;
  onItemTap?: (url: string) => void;
}

interface ListStatusAttendanceState {
}

export class ListStatusAttendance extends React.PureComponent<ListStatusAttendanceProps, ListStatusAttendanceState> {

    constructor(props) {
      super(props);
    }

  render() {
    const { error, items } = this.props;

    return (
      <Root>
        <VBox noGutter={true}>
          <LoadingState loading={this.props.loading} error={!!error} data={!!items}>
            <LoadingState.Shimmer>
              {<ListInfoCellShimmer count={5} />}
            </LoadingState.Shimmer>

            <LoadingState.ErrorPlaceholder>
              {error && (
                <PlaceholderSelector error={error} onTap={this.props.refetch} />
              )}
            </LoadingState.ErrorPlaceholder>

            {items && items.length === 0 ? (
              <Placeholder
                title={this.props.notFoundTitle}
                message={this.props.notFoundMessage}
                image={Asset.Icon.Placeholder.Failure}
              />
            ) :  items && items.length > 0 && (
              <NewsList items={items.map(this.mapToAttendanceItem)} />
            )}
          </LoadingState>
        </VBox>
      </Root>
    );
  }

  private mapToAttendanceItem = (listStatusAttendanceItem: ListStatusAttendanceItem): AttendanceItem => {

    return {
      itemKey: listStatusAttendanceItem.attendance,
      title: `Senha: ${listStatusAttendanceItem.attendance}`,
      descriptions: [
          `Prestador: ${listStatusAttendanceItem.practitionerName}`, 
          `Status: ${listStatusAttendanceItem.status}`,
          `Tipo: ${listStatusAttendanceItem.typeSolicitation}`,
          `Data autorização: ${listStatusAttendanceItem.dateAuthorization === null ? '-' : moment(listStatusAttendanceItem.dateAuthorization.toString()).format('DD MMM YYYY')}`,
          `Data solicitação: ${listStatusAttendanceItem.dateSolicitation === null ? '-' : moment(listStatusAttendanceItem.dateSolicitation.toString()).format('DD MMM YYYY')}`,
          `Data validade: ${listStatusAttendanceItem.dateValidity === null ? '-' : moment(listStatusAttendanceItem.dateValidity.toString()).format('DD MMM YYYY')}`
      ],
    };
  }
}
