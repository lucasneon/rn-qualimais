import * as React from 'react';
import { Placeholder } from '@app/components/mol.placeholder';
import { CarePlanStatus } from '@app/model';
import { Asset, PrimaryButton } from '@atomic';
import { HomeStrings } from './home.strings';

export interface CarePlanProps {
  status: CarePlanStatus;
  onTap?: () => void;
}

const StatusMap: { [type in CarePlanStatus]: any } = {
  [CarePlanStatus.NeedsAnswers]: Asset.Onboard.OnboardRecord,
  [CarePlanStatus.NeedsApproval]: Asset.Onboard.OnboardClock,
  [CarePlanStatus.NeedsAnswersPrincipal]: Asset.Onboard.OnboardRecord,
};

export const CarePlanPlaceholders: React.SFC<CarePlanProps> = props => {
  const needToAnswerQuestionnaire: boolean =
    props.status === CarePlanStatus.NeedsAnswers;

  const needAnswerFromPrincipal: boolean =
    props.status === CarePlanStatus.NeedsAnswersPrincipal;

  let [title, message]: [string, string] = needToAnswerQuestionnaire
    ? [
        HomeStrings.CarePlanPlaceholders.NeedsAnswers.Title,
        HomeStrings.CarePlanPlaceholders.NeedsAnswers.Message,
      ]
    : [
        HomeStrings.CarePlanPlaceholders.NeedsApproval.Title,
        HomeStrings.CarePlanPlaceholders.NeedsApproval.Message,
      ];

  if (needAnswerFromPrincipal) {
    title = HomeStrings.CarePlanPlaceholders.NeedsAnswersPrincipal.Title;
    message = HomeStrings.CarePlanPlaceholders.NeedsAnswersPrincipal.Message;
  }

  return (
    <Placeholder
      title={title}
      message={message}
      image={StatusMap[props.status]}>
      {needToAnswerQuestionnaire && (
        <PrimaryButton
          text={HomeStrings.CarePlanPlaceholders.NeedsAnswers.Action}
          onTap={props.onTap}
          expanded={true}
        />
      )}
    </Placeholder>
  );
};
