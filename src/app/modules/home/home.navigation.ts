import { Navigation } from '@app/core/navigation';
import { LoginGuard } from '@app/modules/authentication';
import { HomePage } from '@app/modules/home';

export const HomeNavigation = {
  register: () => {
    Navigation.register(HomePage, true, LoginGuard('home'));
  },
};
