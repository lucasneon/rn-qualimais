import LottieView from 'lottie-react-native';
import React from 'react';
import { Animated, Easing, StyleSheet, Text } from 'react-native';
import Q from '@assets/animations/heartprogress.json';
import { HBox, Shimmer, VSeparator } from '@atomic';

interface QProgressProps {
  progress: number;
  loading?: boolean;
}

interface QProgressState {
  progress?: Animated.AnimatedValue;
}

export class QProgress extends React.Component<QProgressProps, QProgressState> {
  static defaultProps = { loading: false, progress: null };

  constructor(props) {
    super(props);
    this.state = { progress: new Animated.Value(0) };
  }

  componentDidMount() {
    console.warn(this.getFakeScaleProgress());
    this.startAnimation();
  }

  startAnimation() {
    Animated.timing(this.state.progress, {
      toValue: this.getFakeScaleProgress(),
      duration: 1500,
      useNativeDriver: true,
      easing: Easing.ease,
    }).start();
  }

  render() {
    return (
      <>
        <VSeparator />
        <HBox>
          <HBox.Item hAlign="flex-end">
            <LottieView
              style={{}} // don't remove. there is a bug in this component
              // imageAssetsFolder="images"
              source={Q}
              autoPlay={false}
              progress={this.getFakeScaleProgress()}
            />
          </HBox.Item>
          <HBox.Item hAlign="flex-start">
            {this.props.loading ? (
              <>
                <Shimmer height={24} width="30%" />
                <Shimmer height={16} width="90%" />
                <Shimmer height={16} width="50%" />
              </>
            ) : (
              <>
                <Text style={styles.title}>{this.props.progress} %</Text>
                <Text style={styles.subtitle}>dos cuidados com a saúde</Text>
                <Text style={styles.subtitle}>cumpridos</Text>
              </>
            )}
          </HBox.Item>
        </HBox>
      </>
    );
  }

  private getFakeScaleProgress(): number {
    const progress = this.props.progress / 100;
    return progress > 0 && progress < 0.3
      ? 0.3
      : progress < 0.83 && progress > 0.73
      ? 0.7
      : progress;
  }
}

const styles = StyleSheet.create({
  subtitle: {
    color: '#f58c78',
    fontSize: 18,
    lineHeight: 20,
    fontWeight: 'bold',
  },
  title: {
    color: '#f58c78',
    fontSize: 28,
    fontWeight: 'bold',
  },
});
