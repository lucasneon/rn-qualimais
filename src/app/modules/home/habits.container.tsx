import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { QueryContainer } from '@app/core/graphql';
import { HabitFragment, HabitsInput, HabitsQuery } from '@app/data/graphql';

export interface HabitsContainerChildrenParams {
  activeHabits?: HabitFragment[];
  loading?: boolean;
  error?: any;
  onHabitUpdate?: () => void;
}

export interface HabitsContainerProps {
  fetch: boolean;
  children: (params: HabitsContainerChildrenParams) => React.ReactNode;
}

export class HabitsContainer extends React.PureComponent<HabitsContainerProps> {

  render() {
    const inputData: HabitsInput = { onlyActive: true };
    return this.props.fetch ? (
      <QueryContainer document='habits' variables={{ data: inputData }}>
        {(result: QueryResult<HabitsQuery>) => {
          if (result.loading) {
            return this.props.children({ loading: true });
          }

          if (result.error) {
            return this.props.children({ error: result.error });
          }

          return this.props.children(this.getChildParams(result));
        }}
      </QueryContainer>
    ) : this.props.children({ loading: true });
  }

  private getChildParams(result: QueryResult<HabitsQuery>): HabitsContainerChildrenParams {
    const activeHabits = result && result.data && result.data.Habits;

    return { activeHabits, onHabitUpdate: result.refetch };
  }
}
