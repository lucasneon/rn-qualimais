export const HomeStrings = {
  Title: 'Cuidados',
  NextEvents: 'Próximos eventos',
  Onboard: {
    Title: 'Hábitos',
    Description:
      'Durante suas consultas seu médico poderá sugerir alguns hábitos, ' +
      'essas informações poderão ser visualizadas na área de cuidados.',
    ConcludedHabitsTitle: 'Hábitos concluidos',
    ConcludedHabitsDescription: 'Um hábito é considerado concluído quando...',
    Continue: '(toque em qualquer lugar vazio para continuar)',
    Close: '(toque em qualquer lugar vazio para sair da tela)',
  },
  CarePlanPlaceholders: {
    NeedsAnswers: {
      Title: 'Questionário pendente!',
      Message:
        'Para utilizar o aplicativo, é necessário preencher o seu questionário de saúde!',
      Action: 'Responder questionário',
    },
    NeedsAnswersPrincipal: {
      Title: 'Questionário pendente!',
      Message:
        'É necessário aguardar até que seu titular responda o Questionário de Saúde para que você possa utilizar o aplicativo.',
      Action: 'Responder questionário',
    },
    NeedsApproval: {
      Title: 'Aguardando análise',
      Message: 'Suas respostas estão sendo analisadas',
    },
  },
};
