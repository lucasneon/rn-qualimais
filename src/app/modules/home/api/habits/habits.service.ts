import { fetchAvailableHabits } from './available-habits.service';
import { fetchCarePlanHabits } from './care-plan-habits.service';
import { CarePlanHabitModel, HabitModel } from './habit.model';

export const fetchHabits = async (input: any): Promise<any> => {
  let error;
  const availableHabits = await fetchAvailableHabits().catch(err => {
    console.error(err);
    error = err;
    return null;
  });
  const carePlanHabits = await fetchCarePlanHabits().catch(err => {
    console.error(err);
    error = err;
    return null;
  });

  if (error) {
    return { data: null, error: error };
  }

  let merged: HabitModel[] = findAndMerge(availableHabits, carePlanHabits);
  if (input.onlyActive) {
    merged = merged.filter(it => !!it.carePlanItem);
  } else {
    merged = sortInactivesFirst(merged);
  }
  return { data: merged, error: false };
};

const findAndMerge = (
  availableHabits: HabitModel[],
  carePlanHabits: CarePlanHabitModel[],
): HabitModel[] => {
  availableHabits.forEach(habit => {
    const foundCarePlanHabit: CarePlanHabitModel = carePlanHabits.find(
      it => it.habitCode === habit.id,
    );

    if (foundCarePlanHabit) {
      habit.carePlanItem = foundCarePlanHabit;
    }
  });

  return availableHabits;
};

const sortInactivesFirst = (habits: HabitModel[]): HabitModel[] => {
  return habits.sort(
    (first, second) => +!!first.carePlanItem - +!!second.carePlanItem,
  );
};
