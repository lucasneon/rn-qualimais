import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { HabitModel, HabitTypeModel } from './habit.model';
import { HabitsMapper } from './habits.mapper';

export const fetchAvailableHabits = async (): Promise<HabitModel[]> => {
  const habitTypes: HabitTypeModel[] = await fetchHabitTypes();
  return flatAvailableHabits(habitTypes);
};

const flatAvailableHabits = async (
  habitTypes: HabitTypeModel[],
): Promise<HabitModel[]> => {
  const habits: HabitModel[][] = await Promise.all(
    habitTypes.map(it => fetchFlatHabits(it)),
  );
  const reducer = (
    acc: HabitModel[],
    currentHabits: HabitModel[],
  ): HabitModel[] => {
    return currentHabits && currentHabits.length > 0
      ? acc.concat(...currentHabits)
      : acc;
  };
  return habits.reduce(reducer, []);
};

const fetchHabitTypes = async (): Promise<HabitTypeModel[]> => {
  const res = await habitTypesRequest();
  return res.data.tipoHabitos.map(HabitsMapper.mapType);
};

const fetchFlatHabits = async (
  habitType: HabitTypeModel,
): Promise<HabitModel[]> => {
  const res = await habitRequest(habitType.id);

  if (!res.data.habitos || res.data.habitos.length === 0) {
    return [];
  }

  return res.data.habitos.map(habitRes =>
    HabitsMapper.mapAvailableHabit(habitType, habitRes),
  );
};

export const habitTypesRequest = async () => {
  let res;
  try {
    res = await axiosGet({
      url: '/fusion-gestao-cuidado/api/tipo-habito',
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    console.error(e);
    return { data: null, error: e };
  }

  return { data: res.data, error: false };
};

export const habitRequest = async (habitTypeCode: string) => {
  let res;
  try {
    res = await axiosGet({
      url: `/fusion-gestao-cuidado/api/habito/${habitTypeCode}`,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    console.error(e);
    return { data: null, error: e };
  }

  return { data: res.data, error: false };
};
