import { getCodesPath } from './../care-plan-progress/care-plan-progress.service';
import { LocalDataSource } from '@app/data';
import { DateUtils } from '@app/utils';
import Container from 'typedi';
import {
  CarePlanEventsParams,
  CarePlanItemResponse,
} from '../care-plan/care-plan.entity';
import { CarePlanMapper } from '../care-plan/care-plan.mapper';

import { CarePlanScheduleModel } from '../care-plan/care-plan.model';
import { CarePlanHabitModel } from './habit.model';
import moment from 'moment';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchCarePlanHabits = async (): Promise<CarePlanHabitModel[]> => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  // const [habits, carePlanSummary]: [
  //   CarePlanHabitModel[],
  //   CarePlanProgressModel,
  // ] = await Promise.all([
  //   fetchHabits(beneficiaryCode),
  //   fetchCarePlanProgress(),
  // ]);

  const habits = await fetchHabits(beneficiaryCode);
  const hasActiveHabits: boolean = habits && habits.length > 0;

  if (hasActiveHabits) {
    const filterTodayOrFuture = removePastSchedules(habits);
    return groupSameDayAndFilterMostRecents(filterTodayOrFuture);
  }

  return [];
};

const fetchHabits = async (
  beneficiaryCode: string,
): Promise<CarePlanHabitModel[]> => {
  const params: CarePlanEventsParams =
    CarePlanMapper.mapItemParams(beneficiaryCode);
  params.eventyType = 'HABITO';

  const res = await itemsRequest(params);
  const items: CarePlanItemResponse[] =
    res.data && res.data.planoCuidadoItemAgendamento;

  return items && items.length > 0 ? items.map(CarePlanMapper.mapHabit) : [];
};

const removePastSchedules = (
  habits: CarePlanHabitModel[],
): CarePlanHabitModel[] => {
  const pastMidnight: Date = DateUtils.getPastMidnight();

  return habits.filter(habit => {
    habit.nextSchedules = habit.nextSchedules.filter(
      it => it.limitDate >= pastMidnight,
    );
    return habit.nextSchedules.length > 0;
  });
};

const groupSameDayAndFilterMostRecents = (
  habitItems: CarePlanHabitModel[],
): CarePlanHabitModel[] => {
  const habits: CarePlanHabitModel[] = [];
  let lastItem: CarePlanHabitModel;

  const isNextScheduleSameDay = (
    nextSchedule: CarePlanScheduleModel,
    previousSchedule: CarePlanScheduleModel,
  ) => moment(nextSchedule.limitDate).isSame(previousSchedule.limitDate, 'day');

  habitItems.forEach(item => {
    const next = item.nextSchedules.length > 0 ? item.nextSchedules[0] : null;

    if (!lastItem || lastItem.itemCode !== item.itemCode) {
      lastItem = item;
      habits.push(item);
    } else if (next && isNextScheduleSameDay(next, lastItem.nextSchedules[0])) {
      lastItem.nextSchedules.push(item.nextSchedules[0]);
    }
  });

  return habits;
};

export const itemsRequest = async (params: CarePlanEventsParams) => {
  const filters: string = `${params.from}/${params.until}/${params.eventyType}/${params.generatorType}`;
  const path: string = `${params.beneficiaryCode}/${filters}`;
  let res;
  try {
    res = await axiosGet({
      url: `/fusion-gestao-cuidado/api/plano-cuidado/agendamentos/${getCodesPath()}/${path}`,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    console.error(e);
    return { data: null, error: e };
  }

  return { data: res.data, error: false };
};
