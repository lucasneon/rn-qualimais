import { GeneralTypeModel, SexModel } from '@app/utils/prontlife/common.model';
import {
  CarePlanItemGeneratorModel,
  CarePlanScheduleModel,
} from '../care-plan/care-plan.model';

export interface HabitsInputModel {
  onlyActive: boolean;
}

export interface HabitDetailsInputModel {
  habitCode: string;
  carePlanItemCode?: string;
}

export interface HabitDeactivateInputModel {
  itemCode: string;
}

export interface HabitActivateInputModel {
  carePlanCode: string;
  habitCode: string;
  habitTypeCode: string;
}

export interface HabitFailInputModel {
  scheduleCode: string;
  itemCode: string;
  carePlanCode: string;
  failedAt?: Date;
  reason?: string;
}

export enum HabitAreaModel {
  Eating = 'Eating',
  PhysicalActivity = 'PhysicalActivity',
  WellBeing = 'WellBeing',
  Alcoholism = 'Alcoholism',
  Smoking = 'Smoking',
  Unknown = 'Unknown',
}

export interface HabitTypeModel {
  id: string;
  area: HabitAreaModel;
  display?: string;
}

export interface HabitModel {
  id: string;
  type: HabitTypeModel;
  description: string;
  frequency: GeneralTypeModel;
  warning?: string;
  observation?: string;
  minimumAge?: number;
  maximumAge?: number;
  sex: SexModel;
  carePlanItem?: CarePlanHabitModel;
}

export interface CarePlanHabitModel {
  itemCode: string;
  habitCode: string;
  frequency?: GeneralTypeModel;
  generator?: CarePlanItemGeneratorModel;
  schedules?: CarePlanScheduleModel[];
  nextSchedules?: CarePlanScheduleModel[];
}

export interface GenerateDatesInputModel {
  start: Date;
  end: Date;
  frequencyCode: string;
}
