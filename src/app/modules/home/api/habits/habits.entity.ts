import { GeneralTypeRemote } from "@app/model/api/common.entity";

˝
export interface AvailableHabitsTypeResponse {
  tipoHabitos: HabitTypeResponse[];
  mensagens: string[];
}

export interface HabitTypeResponse {
  codigo: number;
  descricao: string;
}

export interface AvailableHabitsRootResponse {
  habitos: AvailableHabitResponse[];
  mansagens: string[];
}

export interface AvailableHabitResponse {
  codigo: number;
  descricao: string;
  codigoTipoHabito: number;
  frequencia: number;
  periodicidade: FrequencyResponse;
  sexo: GeneralTypeRemote;
  idadeCriticidade: GeneralTypeRemote;
  idadeMinima: number;
  idadeMaxima: number;
  aviso: string;
  observacao?: string;
}

export interface FrequencyResponse {
  codigo: number;
  descricao: string;
  peso: number;
  sequencia: number;
  dias?: number;
  horas?: number;
}
