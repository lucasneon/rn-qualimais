import { GamaSexMap } from '@app/model/api/common.mapper';
import { SexModel } from '@app/utils/prontlife/common.model';
import { HabitAreaModel, HabitModel, HabitTypeModel } from './habit.model';
import { AvailableHabitResponse, HabitTypeResponse } from './habits.entity';

export const HabitsMapper = {
  mapType(typeResponse: HabitTypeResponse): HabitTypeModel {
    return {
      id: typeResponse.codigo.toString(),
      area: HabitAreaMap[typeResponse.codigo] || HabitAreaModel.Unknown,
      display: typeResponse.descricao,
    };
  },

  mapAvailableHabit(
    habitType: HabitTypeModel,
    habitRes: AvailableHabitResponse,
  ): HabitModel {
    const frequencyRes = habitRes.periodicidade;
    return {
      id: habitRes.codigo.toString(),
      type: habitType,
      description: habitRes.descricao,
      frequency: frequencyRes && {
        id: frequencyRes.codigo.toString(),
        description: frequencyRes.descricao,
      },
      warning: habitRes.aviso,
      observation: habitRes.observacao,
      minimumAge: habitRes.idadeMaxima,
      maximumAge: habitRes.idadeMaxima,
      sex: GamaSexMap[habitRes.sexo && habitRes.sexo.chave] || SexModel.Both,
    };
  },
};

const HabitAreaMap: { [id: string]: HabitAreaModel } = {
  15: HabitAreaModel.Eating,
  33: HabitAreaModel.PhysicalActivity,
  31: HabitAreaModel.WellBeing,
  12: HabitAreaModel.Alcoholism,
  11: HabitAreaModel.Smoking,
};
