import { MeasureModel } from '@domain/common';

export interface MedicationScheduleInputModel {
  hour: Date;
}

export interface MedicationInputModel {
  startDate: Date;
  endDate?: Date;
  notify?: boolean;
  orientation?: string;
  dailySchedule: MedicationScheduleInputModel[];
  medicine: MedicineModel;
}

export interface MedicationDeactivateInputModel {
  medicationId: string;
}

export interface ListMedicationInputModel {
  activeOnly?: boolean;
}

export interface MedicationDetailsInputModel {
  medicationId: string;
}

export interface MedicationModel extends MedicationInputModel {
  id: string;
  frequency?: string;
  active: boolean;
}

export interface MedicineModel {
  name: string;
  type?: string;
  commonName?: string;
  aspect?: string;
  dosage?: MeasureModel;
}

export interface MedicineScheduleModel extends MedicationScheduleInputModel {
  id: string;
}
