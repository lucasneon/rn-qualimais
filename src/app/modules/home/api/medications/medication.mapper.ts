import { MedicationModel } from '@domain/medication';
import { MedicationResponse } from './medication.entity';

export const MedicationMapper = {
  mapResponse(response: MedicationResponse): MedicationModel {
    return {
      id: response.codigo && response.codigo.toString(),
      startDate: response.dataInicio && new Date(response.dataInicio),
      endDate: response.dataFim && new Date(response.dataFim),
      active: response.ativo === 'S' ? true : false,
      notify: response.notificacao === 'S' ? true : false,
      orientation: response.orientacao,
      dailySchedule: response.horarios.map(hourRes => ({
        id: hourRes.codigo,
        hour: new Date(hourRes.horario),
      })),
      medicine: {
        name: response.nome,
        aspect: response.aparencia,
        type: response.tipo,
      },
    };
  },
};
