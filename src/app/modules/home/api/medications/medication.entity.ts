import { BaseCarePlanRequest } from '@app/model/api/common.entity';

export interface MedicationRequest extends BaseCarePlanRequest {
  sistemaOrigem: string;
  codigoUsuario: string;
  intervalo: { chave: null };
  horarios: MedicationScheduleRequest[];
  nome: string;
  ativo: 'S' | 'N';
  dosagem?: string;
  aparencia?: string;
  dataInicio: string;
  dataFim?: string;
  orientacao?: string;
  notificacao: 'S' | 'N';
  tipo?: string;
}

export interface MedicationDeactivationRequest {
  sistemaOrigem: string;
  ativo: 'N';
}

export interface MedicationScheduleRequest {
  horario: string;
}

export interface MedicationResponse extends MedicationRequest {
  codigo: number;
  dataAlteracao: string;
  codigoUsuario: string;
  intervalo: any;
  horarios: MedicationScheduleResponse[];
}

export interface MedicationScheduleResponse extends MedicationScheduleRequest {
  codigo: number;
  horario: string;
  dataAlteracao: string;
  codigoUsuario: string;
}
