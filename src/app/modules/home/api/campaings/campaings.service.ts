import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { CampaignsMapper } from './campaings.mapper';
import Container from 'typedi';
import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import { LocalDataSource } from '@app/data';
import { CampaignResponse } from './campaings.entity';

const opCode: string = Container.get(GAMA_OP_CODE);
const simulatingCode: string = Container.get(GAMA_STIMULATING_CODE);

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchCampaings = async () => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const res = await campaignsRequest(beneficiaryCode);
  const campaigns: CampaignResponse[] =
    res.data && res.data.beneficiario && res.data.beneficiario.campanhas;
  if (!campaigns) {
    const errorMoreInfo =
      'Could not find campaings on data source return: ' + res.data.toString();
    console.error(errorMoreInfo);
    return null;
    // throw new CustomError('app.global.error.internal', 500, errorMoreInfo);
  }

  return campaigns.map(CampaignsMapper.mapCampaign);
};

export const campaignsRequest = async (beneficiaryCode: string) => {
  let res;
  try {
    res = await axiosGet({
      url: `/fusion-gestao-cuidado/api/campanhas/${opCode}/${simulatingCode}/${beneficiaryCode}`,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    console.error(e);
    return { data: null, error: e };
  }

  return { data: res.data, error: false };
};
