import {
  GeneralTypeRemote,
  MessageResponse,
} from '@app/model/api/common.entity';

export interface CampaignResponse {
  codigo: number;
  codigoBeneficiario?: number;
  nome: string;
  inicioVigencia?: string;
  fimVigencia?: string;
  questionarios: CampaignQuestionnaireResponse[];
  statusRespostaQuestionario: string;
}

export interface CampaignQuestionnaireResponse {
  codigo: number;
  nome: string;
  guiaQ: boolean;
  podeEnviarEmail: boolean;
  podeResponder: boolean;
  gabaritos: CampaignAnswerResponse[];
  tipoQuestionario?: string;
}

export interface BeneficiaryResponse {
  codigoBeneficiario: number;
  codigoOperadora: number;
  codigoPlano: number;
  codigoEstipulante: number;
  codigoSubEstipulante: number;
  nome: string;
  idade: string;
  sexo: GeneralTypeRemote;
  grauParentesco: GeneralTypeRemote;
  carteirinha: string;
  genero: GeneralTypeRemote;
  estado?: string;
  municipio?: string;
  email: string;
  campanhas: CampaignResponse[];
  dependentes?: BeneficiaryResponse[];
}

export interface CampaignsRootResponse {
  beneficiario: BeneficiaryResponse;
  mensagens: MessageResponse[];
}

export interface CampaignAnswerResponse {
  codigo: number;
  dataAtualizacao: string;
  statusRespostaQuestionario: string;
}
export enum CampaignAnswerResponseStatus {
  RESPONDIDO,
  PARCIALMENTE_RESPONDIDO,
  NAO_RESPONDIDO,
}
