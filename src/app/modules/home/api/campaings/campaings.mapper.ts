import { CampaignModel, QuestionnaireSummaryModel } from './index';
import {
  CampaignAnswerResponse,
  CampaignQuestionnaireResponse,
  CampaignResponse,
} from './campaings.entity';

export const CampaignsMapper = {
  mapCampaign(response: CampaignResponse): CampaignModel {
    return {
      code: response.codigo.toString(),
      name: response.nome,
      start: response.inicioVigencia && new Date(response.inicioVigencia),
      end: response.fimVigencia && new Date(response.fimVigencia),
      questionnaires:
        response.questionarios &&
        response.questionarios.map(mapQuestionnaireStatus),
    };
  },
};

function mapQuestionnaireStatus(
  response: CampaignQuestionnaireResponse,
): QuestionnaireSummaryModel {
  return {
    code: response.codigo.toString(),
    name: response.nome,
    required: response.tipoQuestionario === 'PRINCIPAL',
    ...mapAnswers(response),
  };
}

function mapAnswers(response: CampaignQuestionnaireResponse): {
  responseCode?: string;
  isCompletelyAnswered: boolean;
} {
  const answers: CampaignAnswerResponse[] = response.gabaritos;
  const lastAnswer: CampaignAnswerResponse =
    answers && answers.length > 0 && answers[0];

  return {
    responseCode: lastAnswer
      ? lastAnswer.codigo && lastAnswer.codigo.toString()
      : null,
    isCompletelyAnswered:
      lastAnswer.statusRespostaQuestionario === 'RESPONDIDO',
  };
}
