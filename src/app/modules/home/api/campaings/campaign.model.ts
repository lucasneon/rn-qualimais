import { QuestionnaireInputModel } from '../questionnaire.model';

export interface CampaignQuestionnairesInputModel {
  campaignCode: string;
  questionnaires: QuestionnaireInputModel[];
}

export interface CampaignModel {
  code: string;
  name: string;
  start?: Date;
  end?: Date;
  questionnaires: QuestionnaireSummaryModel[];
}

export interface QuestionnaireSummaryModel {
  code: string;
  name: string;
  responseCode?: string;
  isCompletelyAnswered?: boolean;
  required?: boolean;
}
