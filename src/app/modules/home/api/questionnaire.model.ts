import { QuestionnaireSummaryModel } from './campaings/campaign.model';

// Input

export interface QuestionnaireInputModel {
  questionnaireCode: string;
  responseCode?: string;
}

export interface AnswerQuestionnaireInputModel {
  campaignCode: string;
  questionnaireCode: string;
  responseCode?: string;
  questionGroups: QuestionGroupInputModel[];
  weight?: number;
  height?: number;
}

export interface QuestionGroupInputModel {
  code: string;
  questions: QuestionInputModel[];
}

export interface QuestionInputModel {
  code: string;
  chosenAnswers?: string[];
  typedAnswer?: string;
  type: QuestionTypeModel;
}

// Response

export interface QuestionnaireModel extends QuestionnaireSummaryModel {
  initialText?: string;
  partialText?: string;
  finalText?: string;
  responseCode?: string;
  lastAnsweredAt?: Date;
  questionGroups: QuestionGroupModel[];
  imcRequired: boolean;
  weight?: number;
  height?: number;
}

export interface QuestionGroupModel {
  code: string;
  name: string;
  totalAnswered?: number | string;
  minimumToAnswer?: number | string;
  questions: QuestionModel[];
}

export interface QuestionModel {
  code: string;
  description: string;
  descritive?: string[];
  minValue?: number;
  maxValue?: number;
  required: boolean;
  type: QuestionTypeModel;
  alternatives: AlternativeModel[];
  chosenAnswers?: AnswerModel[];
  conditionToShow?: QuestionnaireConditionModel[];
}

export enum QuestionTypeModel {
  Date = 'Date',
  MultipleChoices = 'MultipleChoices',
  Number = 'Number',
  OneChoice = 'OneChoice',
  Textual = 'Textual',
}

export interface AlternativeModel {
  code: string;
  display?: string;
  conditionToShow?: QuestionnaireConditionModel[];
}

export interface QuestionnaireConditionModel {
  questionCode: string;
  answerCode: string;
}

export interface AnswerModel {
  code?: string;
  display?: string;
}

export interface AnswerQuestionnaireModel {
  responseCode: string;
}
