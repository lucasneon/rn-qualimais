import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import { LocalDataSource } from '@app/data';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { GamaDateFormatter } from '@app/model/api/common.mapper';
import Container from 'typedi';
import {
  CarePlanProgressParams,
  CarePlanProgressResponse,
} from '../care-plan/care-plan.entity';
import { CarePlanProgressModel } from '../care-plan/care-plan.model';

const opCode: string = Container.get(GAMA_OP_CODE);
const simulatingCode: string = Container.get(GAMA_STIMULATING_CODE);

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchCarePlanProgress =
  async (): Promise<CarePlanProgressModel> => {
    const beneficiaryCode: string = await localDataSource.get<string>(
      'beneficiaryCode',
    );
    const params: CarePlanProgressParams = {
      beneficiaryCode,
      segment: 'MENSAL',
      period: GamaDateFormatter.format(new Date(), {
        considerOnlyDay: true,
        includeHours: false,
      }),
    };

    const res = await progressRequest(params);
    const carePlanRes: CarePlanProgressResponse =
      res.data && res.data.planoCuidado;

    return (
      carePlanRes && {
        code: carePlanRes.codigo && carePlanRes.codigo.toString(),
        progress: carePlanRes.percentualRealizado,
      }
    );
  };

export const progressRequest = async (params: CarePlanProgressParams) => {
  const path = `${getCodesPath()}/${params.beneficiaryCode}/${params.segment}/${
    params.period
  }`;
  let res;
  try {
    res = await axiosGet({
      url: `/fusion-gestao-cuidado/api/plano-cuidado/beneficiario/${path}`,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    console.error(e);
    return { data: null, error: e };
  }

  return { data: res.data, error: false };
};

export function getCodesPath() {
  return `${opCode}/${simulatingCode}`;
}
