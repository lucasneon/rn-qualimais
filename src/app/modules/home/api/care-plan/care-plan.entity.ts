import { GeneralTypeRemote } from '@app/model/api/common.entity';

export type CarePlanEventTypeParam = 'CONSULTA' | 'EXAME' | 'HABITO' | 'AMBOS';
export type CarePlanEventGeneratorParam =
  | 'GERADO_LINHA_CUIDADO'
  | 'GERADO_PLANO_CUIDADO'
  | 'AMBOS';

export interface CarePlanProgressParams {
  beneficiaryCode: string;
  segment: 'ANUAL' | 'MENSAL';
  period: string;
}

export interface CarePlanEventsParams {
  beneficiaryCode: string;
  from: string;
  until: string;
  eventyType: CarePlanEventTypeParam;
  generatorType: CarePlanEventGeneratorParam;
}

export interface FulflillEventRequest {
  codigoAgendamento: string;
  codigoItem: string;
  dataRealizada?: string;
  motivo?: string;
}

export interface DeleteItemRequest {
  codigoItem: string;
}

export interface GenerateDatesParams {
  dataInicial: string;
  dataFinal: string;
  codigoPeriodicidade: string;
  diasSemana: string;
}

export interface AddItemRequest {
  codigoHabito: string;
  codigoPeriodicidade: string;
  codigoOperadora: string;
  codigoEstipulante: string;
  codigoBeneficiario: string;
  codigoEspecialidade: null;
  notificacao: boolean;
  origem: string;
  agendamentos: AddItemScheduleRequest[];
  quantidadePlanejada: number;
}

export interface AddItemScheduleRequest {
  dataPlanejamento: string;
  origem: string;
  situacao: string;
}

export interface CarePlanProgressRootResponse {
  planoCuidado: CarePlanProgressResponse;
  mensagens: any[];
}

export interface CarePlanProgressResponse {
  codigo?: number;
  percentualRealizado?: number;
  itens: CarePlanPeriodResponse[];
}

export interface CarePlanPeriodResponse {
  exibicao: string;
  dataInicial: string;
  dataFinal: string;
  habito: ProcedureSummaryResponse;
  exame: ProcedureSummaryResponse;
  consulta: ProcedureSummaryResponse;
}

export interface ProcedureSummaryResponse {
  quantidadeRealizado: number;
  quantidadePlanejada: number;
  quantidadeNaoRealizada: number;
  quantidadeRealizadoPosterior: number;
  quantidadePlanejadaPosterior: number;
  quantidadeNaoRealizadaPosterior: number;
}

export interface CarePlanEventsRootResponse {
  planoCuidadoItemAgendamento: CarePlanItemResponse[];
  mensagens?: any;
}

export interface CarePlanScheduleResponse {
  codigo: number;
  codigoAgendamento: number;
  dataPlanejada?: string;
  dataPlanejamento?: string;
  dataPlanejadaInicial: string;
  dataPlanejadaFinal: string;
  situacao: string;
  situacaoInteger: number;
  origem: string;
  dataRealizada?: any;
  dataAgendamento?: any;
  notificacao?: any;
  motivo?: any;
  dataPlanejamentoFormatada: string;
}

export interface CarePlanItemBaseResponse {
  codigoPlanoCuidado: number;
  periodicidade: GeneralTypeRemote;
  habito?: CarePlanHabitResponse;
  procedimento?: CarePlanProcedureResponse;
  especialidade?: GeneralTypeRemote;
  tipoAgendamento: string;
  aviso?: string;
}

type CarePlanItemAndScheduleResponse = CarePlanScheduleResponse &
  CarePlanItemBaseResponse;

export interface CarePlanItemResponse extends CarePlanItemAndScheduleResponse {
  codigoItem: number;
}

export interface CarePlanProcedureResponse {
  codigo: number;
  codigoProcedimentoTUSS: number;
  descricao: string;
  tipoProcedimento: string;
}

export interface CarePlanHabitResponse {
  codigo: number;
  descricao: string;
  tipoHabito: GeneralTypeRemote;
}

export interface CarePlanItemSearchRootResponse {
  beneficiarioItemDTO: CarePlanItemSearchResponse;
  planoCuidadoItemDTO?: CarePlanItemSearchResponse;
  mensagens?: any;
}

export interface CarePlanItemSearchResponse extends CarePlanItemBaseResponse {
  codigo: number;
  quantidadePlanejada: number;
  texto?: any;
  origem: string;
  codigoPeriodicidade?: any;
  codigoHabito?: any;
  codigoProcedimento?: any;
  codigoEspecialidade?: any;
  agendamentos: CarePlanScheduleResponse[];
  codigoOperadora?: any;
  codigoEstipulante?: any;
  codigoBeneficiario?: any;
}

export interface GenerateDatesResponse {
  datasDisponiveis: string[];
  mensagens: any[];
}
