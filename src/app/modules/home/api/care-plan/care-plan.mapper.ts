import moment from 'moment';
import {
  CarePlanItemDetailModel,
  CarePlanItemGeneratorModel,
  CarePlanItemModel,
  CarePlanProcedureModel,
  CarePlanScheduleModel,
  CarePlanScheduleStatusModel,
  ProcedureTypeModel,
} from './care-plan.model';
import {
  CarePlanEventsParams,
  CarePlanEventTypeParam,
  CarePlanItemBaseResponse,
  CarePlanItemResponse,
  CarePlanItemSearchResponse,
  CarePlanProcedureResponse,
  CarePlanScheduleResponse,
  GenerateDatesParams,
} from './care-plan.entity';
import {
  GamaDateFormatter,
  GeneralTypeMapper,
} from '@app/model/api/common.mapper';
import {
  CarePlanHabitModel,
  GenerateDatesInputModel,
} from '../habits/habit.model';

const Diary = '2';
const MinDate = new Date(2018, 1, 1);
const MaxDate = new Date(2021, 12, 31);

export const CarePlanMapper = {
  mapItemParams(beneficiaryCode: string): CarePlanEventsParams {
    return {
      beneficiaryCode,
      from: GamaDateFormatter.format(MinDate, { includeHours: false }),
      until: GamaDateFormatter.format(MaxDate, { includeHours: false }),
      eventyType: 'AMBOS',
      generatorType: 'AMBOS',
    };
  },

  mapItem(itemRes: CarePlanItemResponse): CarePlanItemModel {
    return {
      ...mapItemBase(itemRes),
      itemCode: itemRes.codigoItem && itemRes.codigoItem.toString(),
      schedule: {
        ...mapScheduleBase(itemRes),
        limitDate:
          itemRes.dataPlanejadaFinal && new Date(itemRes.dataPlanejadaFinal),
      },
    };
  },

  mapHabit(itemRes: CarePlanItemResponse): CarePlanHabitModel {
    return {
      ...mapItemBase(itemRes),
      itemCode: itemRes.codigoItem && itemRes.codigoItem.toString(),
      habitCode: itemRes.habito && itemRes.habito.codigo.toString(),
      nextSchedules: [
        {
          ...mapScheduleBase(itemRes),
          limitDate: itemRes.dataPlanejada && new Date(itemRes.dataPlanejada),
        },
      ],
    };
  },

  mapItemDetails(itemRes: CarePlanItemSearchResponse): CarePlanItemDetailModel {
    const schedules: CarePlanScheduleResponse[] = itemRes.agendamentos;

    return {
      ...mapItemBase(itemRes),
      itemCode: itemRes.codigo && itemRes.codigo.toString(),
      schedule: null,
      schedules:
        schedules &&
        schedules.map<CarePlanScheduleModel>(scheduleRes => ({
          ...mapScheduleBase(scheduleRes),
          limitDate:
            scheduleRes.dataPlanejadaFinal &&
            new Date(scheduleRes.dataPlanejadaFinal),
        })),
    };
  },

  mapHabitDetails(itemRes: CarePlanItemSearchResponse): CarePlanHabitModel {
    const schedules: CarePlanScheduleResponse[] = itemRes.agendamentos;

    return {
      ...mapItemBase(itemRes),
      itemCode: itemRes.codigo && itemRes.codigo.toString(),
      habitCode:
        itemRes.habito &&
        itemRes.habito.codigo &&
        itemRes.habito.codigo.toString(),
      schedules:
        schedules &&
        schedules.map<CarePlanScheduleModel>(scheduleRes => ({
          ...mapScheduleBase(scheduleRes),
          limitDate:
            scheduleRes.dataPlanejamento &&
            new Date(scheduleRes.dataPlanejamento),
        })),
    };
  },

  mapGenerateDatesParams(input: GenerateDatesInputModel): GenerateDatesParams {
    const startDayOfWeek = weekDayMap[moment(input.start).isoWeekday()];

    return {
      codigoPeriodicidade: input.frequencyCode,
      dataInicial: GamaDateFormatter.format(input.start, {
        includeHours: true,
        dayHourSeparator: ' ',
      }),
      dataFinal: GamaDateFormatter.format(input.end, {
        considerOnlyDay: true,
        includeHours: false,
      }),
      diasSemana:
        input.frequencyCode === Diary
          ? Object.values(weekDayMap).join(',')
          : startDayOfWeek,
    };
  },
};

function mapItemBase(itemRes: CarePlanItemBaseResponse): CarePlanItemModel {
  return {
    frequency:
      itemRes.periodicidade && GeneralTypeMapper.map(itemRes.periodicidade),
    generator: generatorMap[itemRes.tipoAgendamento],
    specialty:
      itemRes.especialidade && GeneralTypeMapper.map(itemRes.especialidade),
    procedure: itemRes.procedimento && mapProcedure(itemRes.procedimento),
    itemCode: null,
    schedule: null,
    warning: itemRes.aviso,
  };
}

function mapScheduleBase(
  scheduleRes: CarePlanScheduleResponse,
): CarePlanScheduleModel {
  const status: CarePlanScheduleStatusModel = statusMap[scheduleRes.situacao];
  const doneOrFailDate: Date =
    scheduleRes.dataRealizada && new Date(scheduleRes.dataRealizada);

  return {
    id:
      (scheduleRes.codigo && scheduleRes.codigo.toString()) ||
      (scheduleRes.codigoAgendamento &&
        scheduleRes.codigoAgendamento.toString()),
    status,
    startDate:
      scheduleRes.dataPlanejadaInicial &&
      new Date(scheduleRes.dataPlanejadaInicial),
    scheduledFor:
      scheduleRes.dataAgendamento && new Date(scheduleRes.dataAgendamento),
    doneAt:
      status !== CarePlanScheduleStatusModel.Failed ? doneOrFailDate : null,
    failedAt:
      status === CarePlanScheduleStatusModel.Failed ? doneOrFailDate : null,
  };
}

function mapProcedure(
  procedureRes: CarePlanProcedureResponse,
): CarePlanProcedureModel {
  return {
    id: procedureRes.codigoProcedimentoTUSS
      ? procedureRes.codigoProcedimentoTUSS.toString()
      : procedureRes.codigo && procedureRes.codigo.toString(),
    description: procedureRes.descricao,
    type: procedureTypeMap[procedureRes.tipoProcedimento],
  };
}

const procedureTypeMap: {
  [param in CarePlanEventTypeParam]: ProcedureTypeModel;
} = {
  CONSULTA: ProcedureTypeModel.Appointment,
  EXAME: ProcedureTypeModel.Exam,
  HABITO: undefined,
  AMBOS: undefined,
};

const statusMap: { [status: string]: CarePlanScheduleStatusModel } = {
  PLANEJADO: CarePlanScheduleStatusModel.Planned,
  AGENDADO: CarePlanScheduleStatusModel.Scheduled,
  REALIZADO: CarePlanScheduleStatusModel.Done,
  NAO_REALIZADO: CarePlanScheduleStatusModel.Failed,
};

const generatorMap: { [generator: string]: CarePlanItemGeneratorModel } = {
  GERADO_LINHA_CUIDADO: CarePlanItemGeneratorModel.LinhaCuidado,
  GERADO_PLANO_CUIDADO: CarePlanItemGeneratorModel.PlanoCuidado,
};

const weekDayMap: { [day: number]: string } = {
  1: 'SEGUNDA',
  2: 'TERCA',
  3: 'QUARTA',
  4: 'QUINTA',
  5: 'SEXTA',
  6: 'SABADO',
  7: 'DOMINGO',
};
