import { GeneralTypeModel } from '@app/utils/prontlife/common.model';

// Input
export enum CarePlanScheduleStatusModel {
  Planned = 'Planned',
  Scheduled = 'Scheduled',
  Done = 'Done',
  Failed = 'Failed',
}

export enum ProcedureTypeModel {
  Appointment = 'Appointment',
  Exam = 'Exam',
}

export enum CarePlanItemGeneratorModel {
  LinhaCuidado = 'LinhaCuidado',
  PlanoCuidado = 'PlanoCuidado',
}

export interface CarePlanItemsInputModel {
  types?: ProcedureTypeModel[];
  generator?: CarePlanItemGeneratorModel[];
}

export interface FulfillEventInputModel {
  id: string;
  itemCode: string;
  fulfilledAt?: Date;
}

// Output
export interface CarePlanProgressModel {
  code?: string;
  progress?: number;
}

export interface CarePlanItemModel {
  itemCode: string;
  frequency?: GeneralTypeModel;
  generator?: CarePlanItemGeneratorModel;
  procedure?: CarePlanProcedureModel;
  specialty?: GeneralTypeModel;
  schedule: CarePlanScheduleModel;
  warning?: string;
}

export interface CarePlanScheduleModel {
  id: string;
  status?: CarePlanScheduleStatusModel;
  startDate?: string;
  limitDate?: string;
  scheduledFor?: string;
  doneAt?: string;
  failedAt?: string;
}

export interface CarePlanProcedureModel {
  id: string;
  description: string;
  type?: ProcedureTypeModel;
}

export interface CarePlanItemDetailModel extends CarePlanItemModel {
  schedule: null;
  schedules: CarePlanScheduleModel[];
}
