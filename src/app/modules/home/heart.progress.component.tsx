import LottieView from 'lottie-react-native';
import * as React from 'react';
import { Animated, Easing } from 'react-native';
import Q from '@assets/animations/heartprogress.json';
import { Body, HBox, HDisplay, Shimmer, VSeparator } from'@atomic';

interface QProgressProps {
  progress: number;
  loading?: boolean;
}

interface QProgressState {
  progress?: Animated.AnimatedValue;
}

export class QProgress extends React.Component<QProgressProps, QProgressState> {
  static defaultProps = { loading: false, progress: 0 };

  constructor(props) {
    super(props);
    this.state = { progress: new Animated.Value(0) };
  }

  componentDidMount() {
    Animated.timing(this.state.progress, {
      toValue: this.getFakeScaleProgress(),
      duration: 1500,
      easing: Easing.ease,
    }).start();
  }

  render() {
    return (
      <>
        <VSeparator />
        <HBox>
          <HBox.Item hAlign="flex-end">
            <LottieView
              style={{}} // don't remove. there is a bug in this component
              imageAssetsFolder="images"
              source={Q}
              progress={this.getFakeScaleProgress()}
            />
          </HBox.Item>
          <HBox.Item hAlign="flex-start">
            {this.props.loading ? (
              <>
                <Shimmer height={24} width="30%" />
                <Shimmer height={16} width="90%" />
                <Shimmer height={16} width="50%" />
              </>
            ) : (
              <>
                <HDisplay>{this.props.progress} %</HDisplay>
                <Body>dos cuidados com a saúde </Body>
                <Body>cumpridos</Body>
              </>
            )}
          </HBox.Item>
        </HBox>
      </>
    );
  }

  private getFakeScaleProgress(): number {
    const progress = Math.round(this.props.progress / 10) / 10;

    return progress > 0 && progress < 0.1
      ? 0.1
      : progress < 1 && progress > 0.9
      ? 0.9
      : progress;
  }
}
