import * as React from 'react';
import { Container } from 'typedi';
import { Subscribe } from 'unstated';
import { BeneficiaryCell } from '@app/components/mol.cell';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import {
  CarePlanEvents,
  CarePlanEventsContainer,
  CarePlanEventsContainerChildren,
} from '@app/components/org.care-plan-events';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { Habits } from '@app/components/org.habits/habits.component';
import { Medications } from '@app/components/org.medications/medications.component';
import { PageView } from '@app/core/analytics';
import {
  FlashMessageService,
  GuardProps,
  Navigation,
} from '@app/core/navigation';
import {
  CarePlanCodes,
  CarePlanItemFragment,
  HabitFragment,
  MedicationListItemFragment,
  ProcedureType,
} from '@app/data/graphql';
import { CarePlanStatus, CarePlanSummary } from '@app/model';
import { AccountSwitchPage, AccountPage } from '@app/modules/account';
import { GuardParams } from '@app/modules/authentication';
import {
  AppointmentDetailPage,
  AppointmentDetailPageProps,
  ExamDetailPage,
  ExamDetailPageProps,
  ExamReviewData,
} from '@app/modules/common/events';
import {
  HabitDetailPage,
  HabitDetailsPageProps,
  HabitsPage,
} from '@app/modules/habit';
import { OnboardService, OnboardServiceKey } from '@app/modules/onboard';
import {
  CarePlanProvider,
  CarePlanProviderState,
  HabitsProvider,
  MedicationsProvider,
  BeneficiaryProvider,
  BeneficiaryProviderToken,
} from '@app/services/providers';
import { H2, VSeparator, Asset } from '@atomic';
import { ExamReview } from '../common/events/exam-review.component';
import {
  MedicationDetailsPage,
  MedicationDetailsPageProps,
} from '../habit/medication';
import { MedicationAddPage } from '../habit/medication/medication-add.page';
import { QProgress } from './atm.q-progress.component';
import { CarePlanPlaceholders } from './care-plan-placeholders.component';
import { HomeStrings } from './home.strings';
import { CustomerCarePage } from '@app/modules/customer-care';
import { GeneralStrings } from '@app/modules/common';
import moment from 'moment';
import { LocalDataSource } from '@app/data';

interface HomePageProps extends GuardProps<GuardParams> {
  componentId: string;
}
interface HomePageState {
  reviewData: ExamReviewData;
  showReview: boolean;
  shouldShowAnswerQuestionnaire: boolean;
}
@PageView('Home')
export class HomePage extends React.Component<HomePageProps, HomePageState> {
  static options = {
    name: 'HomePage',
    topBar: { backButton: { visible: false } },
  };

  private beneficiaryProvider: BeneficiaryProvider = Container.get(
    BeneficiaryProviderToken,
  );

  private benefHasCarePlan: boolean =
    this.beneficiaryProvider.state.active.hasCarePlan;

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      reviewData: null,
      showReview: false,
      shouldShowAnswerQuestionnaire: false,
    };
  }

  async componentDidAppear() {
    // if (this.benefHasCarePlan) {
    Container.get<OnboardService>(OnboardServiceKey).check(
      this.props.componentId,
    );
    // }
  }

  async shouldShowAnswerQuestionnaire(carePlan, age: any) {
    const isPrincipalBeneficiary: boolean =
      await this.beneficiaryProvider.isPrincipalBeneficiary();
    if (!isPrincipalBeneficiary && age < 18) {
      console.warn('isShould', true);
      return true;
    }
    console.warn('isShould', false);
    console.warn('carePlanStatus', carePlan.status);
    return false;
  }

  render() {
    return (
      <>
        <Subscribe to={[CarePlanProvider]}>
          {(carePlanProvider: CarePlanProvider) => {
            const res: CarePlanProviderState = carePlanProvider.state;
            const carePlan: CarePlanSummary = res.carePlan;
            // const age = this.beneficiaryProvider.getBeneficiaryAge();

            return (
              <CollapsibleHeader
                title={HomeStrings.Title}
                dock={this.dock()}
                refresh={carePlanProvider.rehydrate}
                loading={res.loading}>
                {res.error ? (
                  <PlaceholderSelector
                    error={res.error}
                    onTap={carePlanProvider.rehydrate}
                  />
                ) : carePlan && carePlan.status ? (
                  <CarePlanPlaceholders
                    onTap={this.handlePlaceholderTap}
                    status={carePlan.status}
                  />
                ) : (
                  <>
                    {console.warn(carePlan && carePlan.progress)}
                    <QProgress
                      progress={carePlan && carePlan.progress}
                      loading={res.loading}
                    />

                    <VSeparator />
                    <Subscribe to={[HabitsProvider]}>
                      {(habitsProvider: HabitsProvider) => (
                        <Habits
                          loading={res.loading || habitsProvider.state.loading}
                          error={habitsProvider.state.error}
                          activeHabits={habitsProvider.state.activeHabits}
                          onHabitTap={this.handleHabitTap}
                          onActivateNewHabitsTap={
                            this.handleActivateNewHabitTap
                          }
                        />
                      )}
                    </Subscribe>
                    <Subscribe to={[MedicationsProvider]}>
                      {(medicationsProvider: MedicationsProvider) => (
                        <Medications
                          loading={
                            res.loading || medicationsProvider.state.loading
                          }
                          error={medicationsProvider.state.error}
                          activeMedications={
                            medicationsProvider.state.activeMedications
                          }
                          onMedicationTap={this.handleMedicationTap}
                          onActivateNewMedicationsTap={
                            this.handleActivateNewMedicationTap
                          }
                        />
                      )}
                    </Subscribe>
                    <VSeparator />
                    {/* Retirado a pedido do cliente */
                    /* <CarePlanEventsContainer
                      fetch={!res.loading}
                      updateProgress={carePlanProvider.updateProgress}>
                      {(eventsRes: CarePlanEventsContainerChildren) => (
                        <CarePlanEvents
                          loading={eventsRes.loading}
                          error={eventsRes.error}
                          events={eventsRes.events}
                          onEventTap={this.handleEventTap(
                            eventsRes.onEventUpdate,
                            carePlanProvider.rehydrate,
                          )}
                        />
                      )}
                    </CarePlanEventsContainer> */}
                  </>
                )}
              </CollapsibleHeader>
            );
          }}
        </Subscribe>
        <ExamReview
          procedureName={
            this.state.reviewData && this.state.reviewData.procedureName
          }
          procedureCode={
            this.state.reviewData && this.state.reviewData.procedureCode
          }
          onSuccess={this.state.reviewData && this.handleReviewSuccess}
          onCancel={this.handleReviewCancel}
          doneAt={
            this.state.reviewData && new Date(this.state.reviewData.doneAt)
          }
          visible={this.state.showReview}
        />
      </>
    );
  }

  private handleChangeBeneficiary = () => {
    Navigation.push(this.props.componentId, AccountSwitchPage);
  };

  private handlePlaceholderTap = () => {
    Navigation.setCurrentTab(this.props.componentId, 1);
    Navigation.popToRoot('HealthPage');
  };

  private handleActivateNewHabitTap = () => {
    Navigation.push(this.props.componentId, HabitsPage);
  };

  private handleHabitTap = (habit: HabitFragment) => {
    Navigation.push<HabitDetailsPageProps>(
      this.props.componentId,
      HabitDetailPage,
      {
        carePlanItemCode: habit.carePlanItem.itemCode,
        habitCode: habit.id,
        habitName: habit.type.display,
        area: habit.type.area,
      },
    );
  };

  private handleEventTap = (
    onEventUpdate: () => void,
    reloadPage: () => void,
  ) => {
    return (event: CarePlanItemFragment) => {
      const procedure = event.procedure;
      const schedule = event.schedule;

      if (procedure.type === ProcedureType.Appointment) {
        const input: CarePlanCodes = {
          scheduleCode: schedule.id,
          itemCode: event.itemCode,
        };

        Navigation.push<AppointmentDetailPageProps>(
          this.props.componentId,
          AppointmentDetailPage,
          {
            appointmentDetailsInput: input,
            onAppointmentScheduled: () => onEventUpdate(),
            onAppointmentCancel: () => reloadPage(),
          },
        );
      } else if (procedure.type === ProcedureType.Exam) {
        Navigation.push<ExamDetailPageProps>(
          this.props.componentId,
          ExamDetailPage,
          {
            carePlanScheduleCode: schedule.id,
            carePlanItemCode: event.itemCode,
            examSpecialtyCode: event.specialty.id,
            procedureCode: event.procedure.id,
            onFufillSuccess: onEventUpdate,
            onReview: this.handleReview,
          },
        );
      }
    };
  };

  private handleActivateNewMedicationTap = () => {
    Navigation.showModal(MedicationAddPage);
  };

  private handleMedicationTap = (
    medicationItem: MedicationListItemFragment,
  ) => {
    Navigation.push<MedicationDetailsPageProps>(
      this.props.componentId,
      MedicationDetailsPage,
      {
        medicationId: medicationItem.id,
        medicationName: medicationItem.medicine.name,
      },
    );
  };

  private handleReview = (data: ExamReviewData) => {
    if (data) {
      this.setState({ reviewData: data, showReview: true });
    }
  };

  private handleReviewSuccess = (message: string) => {
    this.setState({ showReview: false });
    FlashMessageService.showSuccess(message);
  };

  private handleReviewCancel = () => {
    this.setState({ showReview: false });
  };

  private dock = () => {
    console.log();
    const activeBeneficiary = this.props.guard.params.active;

    return (
      <BeneficiaryCell
        onTap={this.handleChangeBeneficiary}
        name={activeBeneficiary && activeBeneficiary.name}
      />
    );
  };

  private setBottomTabsWithoutCarePlan = () => {
    Navigation.setBottomTabs([
      {
        component: CustomerCarePage,
        title: GeneralStrings.BottomTabs.CustomerCarePage,
        icon: Asset.Icon.TabBar.CustomerCare,
      },
      {
        component: AccountPage,
        title: GeneralStrings.BottomTabs.AccountPage,
        icon: Asset.Icon.TabBar.Account,
      },
    ]);
  };
}
