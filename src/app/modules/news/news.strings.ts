export const NewsStrings = {
  Title: 'Notícias',
  NotFound: {
    Title: 'Ops!',
    Message: 'Nenhuma notícia encontrada.',
  },
};
