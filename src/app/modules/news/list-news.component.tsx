import * as React from 'react';
import { ListInfoCellShimmer } from '@app/components/mol.cell';
import { Placeholder, PlaceholderSelector } from '@app/components/mol.placeholder';
import { AppError } from '@app/model';
import { Asset, LoadingState, Root, VBox } from '@atomic';
import { NewsList, NewsListItem } from '@app/components/org.news-list';

export interface ListNewsItem {
  title: string;
  key: string;
  description: string;
  url?: string;
  image?: string;
  date?: string;
}

export interface ListNewsProps {
  loading: boolean;
  items?: ListNewsItem[];
  error?: AppError;
  refetch?: () => void;
  title: string;
  notFoundTitle: string;
  notFoundMessage: string;
  onItemTap?: (url: string) => void;
}

interface ListNewsState {
}

export class ListNews extends React.PureComponent<ListNewsProps, ListNewsState> {

    constructor(props) {
      super(props);
    }

  render() {
    const { error, items } = this.props;

    return (
      <Root>
        <VBox noGutter={true}>
          <LoadingState loading={this.props.loading} error={!!error} data={!!items}>
            <LoadingState.Shimmer>
              {<ListInfoCellShimmer count={5} />}
            </LoadingState.Shimmer>

            <LoadingState.ErrorPlaceholder>
              {error && (
                <PlaceholderSelector error={error} onTap={this.props.refetch} />
              )}
            </LoadingState.ErrorPlaceholder>

            {items && items.length === 0 ? (
              <Placeholder
                title={this.props.notFoundTitle}
                message={this.props.notFoundMessage}
                image={Asset.Icon.Placeholder.Failure}
              />
            ) :  items && items.length > 0 && (
              <NewsList items={items.map(this.mapToNewsItem)} />
            )}
          </LoadingState>
        </VBox>
      </Root>
    );
  }

  private mapToNewsItem = (listNewsItem: ListNewsItem, index: number): NewsListItem => {
    return {
      itemKey: listNewsItem.key,
      title: listNewsItem.title,
      descriptions: [listNewsItem.description],
      url: listNewsItem.url,
      image: listNewsItem.image,
      date: listNewsItem.date,
      onTap: this.props.onItemTap && (() => this.props.onItemTap(listNewsItem.url)),
    };
  }
}
