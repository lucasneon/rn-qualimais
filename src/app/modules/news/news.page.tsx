import * as React from 'react';
import { PageView } from '@app/core/analytics';
import { ExternalNavigator, Navigation, NavigationPageProps, FlashMessageService } from '@app/core/navigation';
import { Scroll } from '@atomic';
import { NewsStrings } from '@app/modules/news/news.strings';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { ListNews, ListNewsItem } from '@app/modules/news/list-news.component';
import Container from 'typedi';
import { NewsQuery } from '@app/data/graphql';
import { ApolloError } from 'apollo-client';
import { ErrorMapper } from '@app/utils';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';

export interface NewsNavigationProps {
}

export interface NewsState {
  loading: boolean;
  items: ListNewsItem[];
}

type NewsProps = NavigationPageProps<NewsNavigationProps>;

@PageView('News')
export class NewsPage extends React.Component<NewsProps, NewsState> {
  static options = {
    name: 'NewsPage',
  };

  private readonly externalNavigator: ExternalNavigator = Container.get(ExternalNavigator);
  private readonly graphQLProvider: GraphQLProvider = Container.get(GraphQLProviderToken);
  
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      items: null,
    };
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    this.queryNews();
  }

  queryNews() {
    this.setState({ loading: true, });
    this.graphQLProvider.query(
      'news',
      null,
      this.handleQuerySuccess,
      this.handleQueryError,
    );
  }

  private handleQuerySuccess = (res: NewsQuery) => {
    const items: ListNewsItem[] = res && res.News.content.map(this.mapToNewsList);
    this.setState({ loading: false, items: items });
  }

  private handleQueryError = (error: ApolloError) => {
    this.setState({ loading: false });    
    FlashMessageService.showError(ErrorMapper.map(error));
  }

  private mapToNewsList = (res: any): ListNewsItem => {
    return {
      key: res.id,
      title: res.title,
      description: res.description,
      image: res.image,
      url: res.url,
      date: res.publicationDate,
    };
  }

  private handleItemTap = async (url: string) => {
    if (url) {
      await this.externalNavigator.openWebsite(url);
    }
  }

  render() {
    return (
      <CollapsibleHeader title={NewsStrings.Title}>
        <Scroll>
          <ListNews
            loading={this.state.loading}
            items={this.state.items}
            title={NewsStrings.Title}
            notFoundTitle={NewsStrings.NotFound.Title}
            notFoundMessage={NewsStrings.NotFound.Message}
            onItemTap={this.handleItemTap}
          />
        </Scroll>
      </CollapsibleHeader>
    );
  }

  // private handleFilterTap = () => {
  //   console.warn('filter tap');
  // }

  // private handleSearchTap = () => {
  //   console.warn('search tap');
  // }
}
