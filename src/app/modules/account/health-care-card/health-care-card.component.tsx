import * as React from 'react';
import { Animated } from 'react-native';
import { HealthCareCardFragment } from '@app/data/graphql';
import { Shimmer } from '@atomic';
import { Swiper } from '@atomic/obj.swiper/swiper.component';
import { HealthCareCardBack } from './health-care-card-back.component';
import { HealthCareCardFront } from './health-care-card-front.component';
import { HealthCardWrapperStyled, HealthCareCardHeight, HealthCareCardWidth } from './health-care-card.style';
import Container from 'typedi';
import { ActiveNetworkSearch } from '@app/config';

interface HealthCareCardProps {
  data: HealthCareCardFragment;
}

interface HealthCareCardState {
  isAnimating: boolean;
}

const IncreaseAngle = true;
const DecreaseAngle = false;

export const HealthCareCardShimmer: React.SFC = () => (
  <HealthCardWrapperStyled>
    <Shimmer rounded={true} height={HealthCareCardHeight} width={HealthCareCardWidth} />
  </HealthCardWrapperStyled>
);
export class HealthCareCard extends React.Component<HealthCareCardProps, HealthCareCardState> {

  private activeNetworkSearch = Container.get(ActiveNetworkSearch);

  private animatedValue;
  private value;
  private frontInterpolate;
  private backInterpolate;
  private frontOpacity;
  private backOpacity;
  private isPetro: boolean;

  constructor(props) {
    super(props);

    this.state = {
      isAnimating: false,
    };

    this.animatedValue = new Animated.Value(0);
    this.value = 0;
    this.animatedValue.addListener(({ value }) => {
      this.value = value;
    });
    this.frontInterpolate = this.animatedValue.interpolate({
      inputRange: [-170, 350],
      outputRange: ['-170deg', '350deg'],
    });
    this.backInterpolate = this.animatedValue.interpolate({
      inputRange: [-170, 350],
      outputRange: ['0deg', '530deg'],
    });
    this.frontOpacity = this.animatedValue.interpolate({
      inputRange: [-90, -89, 89, 90, 269, 270],
      outputRange: [0, 1, 1, 0, 0, 1],
    });
    this.backOpacity = this.animatedValue.interpolate({
      inputRange: [-90, -89, 89, 90, 269, 270],
      outputRange: [1, 0, 0, 1, 1, 0],
    });
  }

  render() {

    this.isPetro = Boolean(this.activeNetworkSearch);

    const frontAnimatedStyle = {
      transform: [
        { rotateY: this.frontInterpolate },
      ],
    };
    const backAnimatedStyle = {
      transform: [
        { rotateY: this.backInterpolate },
      ],
    };

    return (
      <HealthCardWrapperStyled>
        <Swiper onSwipeRight={this.flipRight} onSwipeLeft={this.flipLeft} >
          <HealthCareCardBack data={this.props.data} style={[backAnimatedStyle, { opacity: this.backOpacity }]} isPetro={this.isPetro} />
          <HealthCareCardFront data={this.props.data} style={[frontAnimatedStyle, { opacity: this.frontOpacity}]} isPetro={this.isPetro} />
        </Swiper>
      </HealthCardWrapperStyled>
    );
  }

  flipCard = () => {
    if (this.value >= 90) {
      this.setState({ isAnimating: true }, () => Animated.timing(this.animatedValue, {
        toValue: 0,
      }).start(() => this.setState({ isAnimating: false })));
    } else {
      this.setState({ isAnimating: true }, () => Animated.timing(this.animatedValue, {
        toValue: 180,
      }).start(() => this.setState({ isAnimating: false })));
    }
  }

  private flipRight = () => {
    this.animateCard(IncreaseAngle);
  }

  private flipLeft = () => {
    this.animateCard(DecreaseAngle);
  }

  private animateCard = (incrementAngle?: boolean) => {
    if (!this.state.isAnimating) {
      this.setState({ isAnimating: true }, () => Animated.timing(this.animatedValue, {
        toValue: this.value + (incrementAngle ? 180 : -180),
      }).start(this.fixValue));
    }
  }

  private fixValue = () => {
    this.setState({ isAnimating: false });
    if (this.value % 360 === 0) {
      this.animatedValue.setValue(0);
      return;
    }
    if (this.value % 180 === 0) {
      this.animatedValue.setValue(180);
    }
  }

}
