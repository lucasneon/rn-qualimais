import * as React from 'react';
import { Alert, Animated, Clipboard, TouchableOpacity } from 'react-native';
import { HealthCareCardFragment } from '@app/data/graphql';
import { Asset, VBox, VSeparator } from '@atomic';
import {
  HealthCareCardFrontLogo, HealthCareCardGradientStyled, HealthCareCardTypography
} from './health-care-card.style';
// import { CopyToClipboardImage, HealthCareCardTextWrapper } from './health-care-card-front.style';
import { HealthCareCardStrings } from './health-care-card.strings';

interface HealthCareCardFrontProps {
  data: HealthCareCardFragment;
  style: any;
  isPetro?: boolean;
}

interface CardBackItem {
  label: string;
  text: string | number;
  capitalizeFirstLetter?: boolean;
}

const CardBackItem = (props: CardBackItem) => {
  const capitalizeFirstLetter = text =>
    text.slice(0, 1).toUpperCase() + text.slice(1, text.length).toLowerCase();

  return (
    <>
      <HealthCareCardTypography.LabelFrontCard>{props.label}</HealthCareCardTypography.LabelFrontCard>
      <HealthCareCardTypography.BodyBold>
        {props.capitalizeFirstLetter ? capitalizeFirstLetter(props.text) : props.text}
      </HealthCareCardTypography.BodyBold>
      <VSeparator />
    </>
  );
};

// workaround as text-transform is not working on android
// https://github.com/facebook/react-native/issues/21966
// const capitalizeWords = text => {
//   let result = '';
//   let wordCharIndex = 0;
//   for (let i = 0; i < text.length; i++) {
//     let c = text.charAt(i);
//     if (/\s/.test(c)) {
//       wordCharIndex = 0;
//     } else {
//       if (wordCharIndex === 0) {
//         c = c.toUpperCase();
//       } else {
//         c = c.toLowerCase();
//       }
//       wordCharIndex++;
//     }
//     result += c;
//   }
//   return result;
// };

export const HealthCareCardFront: React.SFC<HealthCareCardFrontProps> = props => {

  const handleCopyToClipboard = () => {
      Clipboard.setString(props.data.idHealthCareCard);
      Alert.alert(HealthCareCardStrings.Copy.Title, HealthCareCardStrings.Copy.Warning);
  };

  return (
    <Animated.View style={props.style} >
      <HealthCareCardGradientStyled >

        <VBox noGutter={true} vAlign='center' hAlign='center' vGrow={true}>
          <HealthCareCardFrontLogo />
        </VBox>

        <VBox>
          <HealthCareCardTypography.BodyBold>
            {props.data.name}
          </HealthCareCardTypography.BodyBold>
          <VSeparator />

          <CardBackItem 
            label={HealthCareCardStrings.BackLabels.HealthCard}
            text={props.data.idHealthCareCard}
          />

          <CardBackItem 
            label={HealthCareCardStrings.BackLabels.Cns}
            text={props.data.planData.cns}
          />

          <CardBackItem 
            label={HealthCareCardStrings.BackLabels.StartTerm}
            text={props.data.planData.startValidity}
          />

          {/* <HealthCareCardTypography.BodyBold>
            {props.data.name}
          </HealthCareCardTypography.BodyBold>
          <VSeparator />

          <HealthCareCardTextWrapper>
            <HealthCareCardTypography.BodyWithImage>
              {HealthCareCardStrings.BackLabels.HealthCard}: {props.data.idHealthCareCard}
            </HealthCareCardTypography.BodyWithImage>
            <TouchableOpacity onPress={handleCopyToClipboard}>
              <CopyToClipboardImage source={Asset.Icon.Custom.Copy} />
            </TouchableOpacity>
          </HealthCareCardTextWrapper>
          
          <VSeparator />

          <HealthCareCardTypography.Body>{HealthCareCardStrings.BackLabels.Cns}: {props.data.planData.cns}</HealthCareCardTypography.Body>
          <VSeparator />
          <HealthCareCardTypography.Body>{HealthCareCardStrings.BackLabels.StartTerm}: {props.data.planData.startValidity}</HealthCareCardTypography.Body>
          <VSeparator /> */}
        </VBox>

        <VSeparator />

        <VSeparator />
      </HealthCareCardGradientStyled>
    </Animated.View >
  );
};
