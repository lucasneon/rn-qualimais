import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { LargeHeader } from '@app/components/atm.header';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { PageView } from '@app/core/analytics';
import { QueryContainer } from '@app/core/graphql';
import { NavigationPageProps } from '@app/core/navigation';
import { HealthCareCardFragment, HealthCareCardQuery, PlanDataFragment } from '@app/data/graphql';
import { ModalPage } from '@app/modules/common/modal.page';
import { ErrorMapper } from '@app/utils';
import { HBox, LinkButton, LoadingState, Root, VBox } from '@atomic';
import { HealthCareCard, HealthCareCardShimmer } from './health-care-card.component';
import { HealthCareCardStrings } from './health-care-card.strings';
import { HealthCardPageView } from './health-care-card.style';
import moment from 'moment';

@PageView('HealthCareCard')
export class HealthCareCardPage extends ModalPage<NavigationPageProps<undefined>> {
  static options = { name: 'HealthCareCardPage' };

  private card;

  render() {
    return (
      <Root>
        <LargeHeader title={HealthCareCardStrings.Title} />
        <QueryContainer document='health-care-card' >
          {(res: QueryResult<HealthCareCardQuery>) => {
            const cardData = res.data && res.data.HealthCareCard;
            const handlePlaceholderTap = () => res.refetch();
            return (
              //console.warn(error);
              <VBox noGutter={true}>
                <HealthCardPageView >
                  <LoadingState data={!!cardData} error={!!res.error} loading={res.loading}>
                    <LoadingState.Shimmer>
                      <HealthCareCardShimmer />
                    </LoadingState.Shimmer>
                    <LoadingState.ErrorPlaceholder>
                      <PlaceholderSelector error={ErrorMapper.map(res.error)} onTap={handlePlaceholderTap} />
                    </LoadingState.ErrorPlaceholder>
                    <HealthCareCard
                      ref={ref => this.card = ref}
                      data={cardData && this.getDataWithPlaceholders(cardData)}
                    />
                  </LoadingState>
                </HealthCardPageView>
                {cardData &&
                  <HBox>
                    <HBox.Item vAlign='flex-start' hAlign='center'>
                      <LinkButton
                        onTap={this.handleTap}
                        text={HealthCareCardStrings.FlipButton.toggle}
                        removeDebounce={true}
                      />
                    </HBox.Item>
                  </HBox>
                }
              </VBox>
            );
          }}
        </QueryContainer>
      </Root>
    );
  }

  private handleTap = () => {
    if (this.card) {
      this.card.flipCard();
    }
  }

  private getDataWithPlaceholders = (data: HealthCareCardFragment): HealthCareCardFragment => {
    const newData: HealthCareCardFragment = {} as HealthCareCardFragment;
    for (const field of Object.keys(data)) {
      newData[field] = data[field] || HealthCareCardStrings.FieldPlaceholder;
    }

    const newPlanData: PlanDataFragment = {} as PlanDataFragment;

    for (const field of Object.keys(data.planData)) {
      newPlanData[field] = data.planData[field] || HealthCareCardStrings.FieldPlaceholder;
    }

    if (newPlanData.startValidity != null) {
      newPlanData.startValidity = moment(newPlanData.startValidity.toString()).format('DD/MM/YYYY');
    }
    // console.warn(newPlanData.startValidity);
    return { ...newData, planData: newPlanData };
  }
}
