import * as React from 'react';
import { Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';
import { Asset, BodyWhite, BodyWhite_, Border, Color, FontFamily, FontSize, Platform, Spacing, BodyWhiteBold_ } from '@atomic';
import { BrandColor } from '@atomic/obj.constants/constants.gama';

export const HealthCareCardBaseWidth = 238;
export const HealthCareCardBaseHeight = 295;

const Ratio = HealthCareCardBaseWidth / HealthCareCardBaseHeight;

const TopBarHeight = 148;
const BottomSpaceHeight = 10;

const ViewHeight = Dimensions.get('window').height - TopBarHeight - BottomSpaceHeight;

export const HealthCareCardHeight = Math.round(0.9 * ViewHeight);

export const HealthCareCardWidth = Math.round(HealthCareCardHeight * Ratio);

const HealthCareCardGradient = styled(LinearGradient).attrs({
  start: { x: 1, y: 0 },
  end: { x: 0, y: 1 },
})`
  overflow: ${Platform.OS === 'ios' ? 'hidden' : 'visible'};
  border-radius: ${Border.RadiusLarge};
  width: ${HealthCareCardWidth};
  height: ${HealthCareCardHeight};
  margin: ${Spacing.Large}px;
`;

export const HealthCareCardGradientStyled = props => (
  <HealthCareCardGradient {...props} colors={[Color.GradientCart, Color.CallToAction]}>
    {props.children}
  </HealthCareCardGradient>
);

export const HealthCareCardTextStyled = styled.Text`
  font-size: ${FontSize.Small};
  color: ${Color.White};
  font-family: ${FontFamily.Primary.Regular};
`;

export const HealthCareCardLabelTextStyled = styled.Text`
  font-size: ${FontSize.XSmall};
  color: ${Color.GrayLight};
  font-family: ${FontFamily.Primary.Regular};
`;

export const HealthCareCardFrontLogo = styled.Image.attrs({
  source: Asset.Image.QLogoWhite,
})``;

export const HealthCareCardBackLogo = styled.Image.attrs({
  source: Asset.Image.LogoWhite,
})``;

export const HealthCareCardBackAns = styled.Image.attrs({
  source: Asset.Image.ANS,
})``;

export const HealthCardWrapperStyled = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const HealthCardPageView = styled.View`
  height: ${ViewHeight};
  justify-content: center;
  align-items: center;
`;

export const HealthCareCardTypography = {
  Label: styled.Text`
    font-family: ${FontFamily.Primary.Light};
    font-size: ${FontSize.XSmall};
    line-height: 12;
    color: ${Color.White};
  `,
  LabelFrontCard: styled.Text`
    font-family: ${FontFamily.Primary.Light};
    font-size: ${FontSize.Small};
    font-weight: bold;
    line-height: 14;
    color: ${Color.White};
  `,
  Title: styled.Text`
    font-family: ${FontFamily.Secondary.Bold};
    font-size: ${FontSize.Small};
    color: ${Color.White};
    line-height: 12;
  `,
  BodyBold: styled(BodyWhite)`
    font-weight: bold;
    color: ${Color.GrayDark};
    ${props => props.capitalize ? 'text-transform: capitalize;' : ''};
  `,
  BodyWithImage: styled(BodyWhite)`
    align-self: stretch;
    align-items: center;
    padding-right: ${Spacing.XSmall};
  `,

  BodyWithCard: BodyWhite_
  ,

  BodyWithCardB: BodyWhiteBold_
  ,

  Body: BodyWhite
  ,
};
