import styled from 'styled-components/native';
import { Color } from '@atomic';

export const HealthCareCardTextWrapper = styled.View`
  flex-direction: row;
  align-self: stretch;
  align-items: center;
  align-content: center;
`;

export const CopyToClipboardImage = styled.Image`
  tint-color: ${Color.White};
`;
