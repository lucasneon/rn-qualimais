import { StyleSheet } from 'react-native';

export const HealthCareCardBackStyles = StyleSheet.create({
  flipCardBack: {
    position: 'absolute',
    top: 0,
  },
});
