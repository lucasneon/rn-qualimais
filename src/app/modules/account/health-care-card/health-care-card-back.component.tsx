import * as React from 'react';
import { Animated, Text, View } from 'react-native';
import { HealthCareCardFragment } from '@app/data/graphql';
import { VBox, VSeparator, VSeparatorCard } from '@atomic';
import { HealthCareCardBackStyles } from './health-care-card-back.style';
import { HealthCareCardStrings } from './health-care-card.strings';
import {
  HealthCareCardBackLogo, HealthCareCardGradientStyled, HealthCareCardTypography, HealthCareCardBackAns
} from './health-care-card.style';

export interface HealthCareCardBackProps {
  data: HealthCareCardFragment;
  style: any;
  isPetro?: boolean;
}
interface CardBackItem {
  label: string;
  text: string | number;
  capitalizeFirstLetter?: boolean;
}

const CardBackItem = (props: CardBackItem) => {
  const capitalizeFirstLetter = text =>
    text.slice(0, 1).toUpperCase() + text.slice(1, text.length).toLowerCase();

  return (
    <>
      <HealthCareCardTypography.LabelFrontCard>{props.label}</HealthCareCardTypography.LabelFrontCard>
      <HealthCareCardTypography.BodyBold>
        {props.capitalizeFirstLetter ? capitalizeFirstLetter(props.text) : props.text}
      </HealthCareCardTypography.BodyBold>

      {/* <HealthCareCardTypography.Label>{props.label2}</HealthCareCardTypography.Label>
      <HealthCareCardTypography.BodyBold>
        {props.capitalizeFirstLetter ? capitalizeFirstLetter(props.text) : props.text2}
      </HealthCareCardTypography.BodyBold> */}

      <VSeparatorCard />
    </>
  );
};

export const HealthCareCardBack: React.SFC<HealthCareCardBackProps> = props => {
  const { planData } = props.data;
  return (
    <Animated.View style={[HealthCareCardBackStyles.flipCardBack, props.style]}>
      <HealthCareCardGradientStyled >
        
        <VBox grow={true} vAlign='center' hAlign='center'>
          <HealthCareCardBackLogo />
        </VBox>

        <VBox >
          <CardBackItem 
            label={HealthCareCardStrings.BackLabels.Plano}
            text={planData.namePlan}
          />

          <CardBackItem 
            label={HealthCareCardStrings.BackLabels.Rede}
            text={planData.nameNetwork}
          />

          <CardBackItem
            label={HealthCareCardStrings.BackLabels.Compreensiveness}
            text={planData.comprehensiveness}
            capitalizeFirstLetter={true}
          />
          <CardBackItem
            label={HealthCareCardStrings.BackLabels.Accommodation}
            text={planData.accommodation}
            capitalizeFirstLetter={true}
          />

          <CardBackItem 
            label={HealthCareCardStrings.BackLabels.Segmentation}
            text={planData.segmentation}
          />

          <CardBackItem 
            label={HealthCareCardStrings.BackLabels.Contractor}
            text={planData.contractor}
          />
          
          <CardBackItem 
            label={HealthCareCardStrings.BackLabels.TypeContractor} 
            text={planData.type} 
          />
          
          <HealthCareCardTypography.BodyWithCardB>Central de Atendimento Gama Saúde:</HealthCareCardTypography.BodyWithCardB>
          <HealthCareCardTypography.BodyWithCard>4004 0178 (capitais e regiões metropolitanas)</HealthCareCardTypography.BodyWithCard>
          <HealthCareCardTypography.BodyWithCard>0800 722 0178 (demais regiões)</HealthCareCardTypography.BodyWithCard>
          <HealthCareCardTypography.BodyWithCard>0800 775 4058 (Ouvidoria) </HealthCareCardTypography.BodyWithCard>

          <View style={{alignItems: 'flex-end', marginTop: -15}}>
            <HealthCareCardBackAns />
          </View>


        </VBox>
         
        {/* Retirado o VBOX */}

        {/* Fim do vbox retirado */}
        <VSeparator />

      </HealthCareCardGradientStyled>
    </Animated.View >
  );
};
