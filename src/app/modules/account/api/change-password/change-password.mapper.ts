import { GAMA_OP_CODE, GAMA_SYSTEM_CODE } from '@app/config';
import Container from 'typedi';
import {
  ChangePasswordInputModel,
  ChangePasswordRequest,
} from './change-password.model';

const opCode: string = Container.get(GAMA_OP_CODE);
const systemCode: string = Container.get(GAMA_SYSTEM_CODE);
const codes = { codigoOperadora: opCode, codigoSistema: systemCode };

export function mapChangePassword(
  input: ChangePasswordInputModel,
  beneficiaryCode: string,
): ChangePasswordRequest {
  console.warn('inputs', {
    ...codes,
    codigoBeneficiario: beneficiaryCode,
    senhaAntiga: input.currentPassword,
    novaSenha: input.newPassword,
  });
  return {
    ...codes,
    codigoBeneficiario: beneficiaryCode,
    senhaAntiga: input.currentPassword,
    novaSenha: input.newPassword,
  };
}
