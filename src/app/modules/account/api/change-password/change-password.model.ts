import { BaseUserRequest } from '@app/model/api/common.entity';

export interface ChangePasswordInputModel {
  currentPassword: string;
  newPassword: string;
}

export interface ChangePasswordRequest extends BaseUserRequest {
  codigoBeneficiario: string;
  novaSenha: string;
  senhaAntiga: string;
}
