import { LocalDataSource } from '@app/data';
import {
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import Container from 'typedi';
import { mapChangePassword } from './change-password.mapper';
import { ChangePasswordInputModel } from './change-password.model';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const changePassword = async (input: ChangePasswordInputModel) => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  return changePasswordRequest(mapChangePassword(input, beneficiaryCode));
};

function changePasswordRequest(req) {
  let res;
  try {
    res = axiosPost({
      url: '/senha/alterar-senha',
      data: req,
      baseUrl: BaseUrl.GAMA_AUTH_URL,
    });
  } catch (err) {
    return err;
  }
  return res;
}
