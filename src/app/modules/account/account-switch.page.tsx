import * as React from 'react';
import { Container } from 'typedi';
import { Subscribe } from 'unstated';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { ProvidersConfig } from '@app/config';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { Beneficiary } from '@app/model';
import { BeneficiaryProvider } from '@app/services/providers';
import { BeneficiariesList } from './beneficiaries-list.component';

@PageView('AccountSwitch')
export class AccountSwitchPage extends React.Component<NavigationPageProps<{}>> {
  static options = { name: 'AccountSwitchPage' };

  private readonly providersConfig: ProvidersConfig = Container.get(ProvidersConfig);

  private beneficiaryProvider: BeneficiaryProvider;

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  render() {
    return (
      <CollapsibleHeader title='Grupo familiar'>
        <Subscribe to={[BeneficiaryProvider]}>
          {(beneficiaryProvider: BeneficiaryProvider) => {
            this.beneficiaryProvider = beneficiaryProvider;
            return (
              <BeneficiariesList
                onValueChange={this.handleValueChange}
                beneficiaries={this.beneficiaryProvider.state.beneficiaries}
                initial={this.beneficiaryProvider.state.active}
              />
            );
          }}
        </Subscribe>
      </CollapsibleHeader>
    );
  }

  private handleValueChange = (beneficiary: Beneficiary) => {
    this.beneficiaryProvider.setActive(beneficiary, () => {
      this.providersConfig.handleContextChange();
    });
  }
}
