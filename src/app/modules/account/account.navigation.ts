import { Navigation } from '@app/core/navigation';
import { HealthCareCardPage } from '@app/modules/account/health-care-card';
import { LoginGuard } from '@app/modules/authentication';
import { AccountSwitchPage } from './account-switch.page';
import { AccountPage } from './account.page';
import { ChangeBeneficiaryInfoPage } from './change-beneficiary-info';
import { ChangePasswordPage } from './change-password';
import { NewsPage } from '@app/modules/news/news.page';
import { StatusAttendancePage } from '../attendance/attendance.page';
import { PublicationsPage } from '@app/modules/magazines/publications.page';
import { MagazinesDetailsPage } from '@app/modules/magazines';
import { PDFPage } from '@app/modules/pdf';

export const AccountNavigation = {
  register: () => {
    Navigation.register(AccountSwitchPage, true, LoginGuard());
    Navigation.register(ChangeBeneficiaryInfoPage);
    Navigation.register(ChangePasswordPage);
    Navigation.register(AccountPage, true, LoginGuard());
    Navigation.register(HealthCareCardPage);
    Navigation.register(NewsPage);
    Navigation.register(StatusAttendancePage);
    Navigation.register(PublicationsPage);
    Navigation.register(MagazinesDetailsPage);
    Navigation.register(PDFPage);
  },
};
