export const AccountStrings = {
  ChangeData: 'Alterar meus dados',
  ChangePassword: 'Alterar minha senha',
  HealthCareCard: 'Carteirinha Virtual',
  Logout: 'Sair',
  AlertTitle: 'Sair da conta',
  AlertMessage: 'Tem certeza que deseja sair?',
  Cancel: 'Cancelar',
  Confirm: 'Sim',
  RegisterBiometry: 'Cadastrar reconhecimento facial',
  SeeHealthCareCard: 'Ver carteirinha virtual',
  ViewData: 'Visualizar meus dados',
};
