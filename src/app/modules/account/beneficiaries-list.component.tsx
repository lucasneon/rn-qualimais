import * as React from 'react';
import { RadioListItem } from '@app/components/mol.radio-list';
import { Age, Beneficiary } from '@app/model';
import { GeneralStrings } from '@app/modules/common';
import { DateUtils } from '@app/utils';
import AvatarImage from '@assets/images/samples/avatar.png';
import { Body, Cell, Form, H3 } from '@atomic';

export interface BeneficiariesListProps {
  beneficiaries: Beneficiary[];
  onValueChange: (beneficiary: Beneficiary) => void;
  initial: Beneficiary;
}

const ListItem: React.SFC<{ beneficiary: Beneficiary}> = props => {
  const age: Age = DateUtils.Calculate.getAge(props.beneficiary.birthDate);

  return (
    <Cell leftGutter={false}>
      <H3>{props.beneficiary.name}</H3>
      <Body>{GeneralStrings.Age(age)}</Body>
    </Cell>
  );
};

export const BeneficiariesList: React.SFC<BeneficiariesListProps> = props => {
  const onValueChange = (idHealthCareCard: string) => {
    props.onValueChange(props.beneficiaries.find(ben => ben.idHealthCareCard === idHealthCareCard));
  };

  return props.beneficiaries && (
    <Form.Field name='beneficiary' value={props.initial.idHealthCareCard} onValueChange={onValueChange}>
      {props.beneficiaries.map(beneficiary => (
        <RadioListItem
          key={beneficiary.idHealthCareCard}
          value={beneficiary.idHealthCareCard}
          source={AvatarImage}
          // disable={true}
        >
          <ListItem beneficiary={beneficiary} />
        </RadioListItem>
      ))}
    </Form.Field >
  );
};
