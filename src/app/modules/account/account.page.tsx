import * as React from 'react';
import { NativeModules } from 'react-native';
import { Container } from 'typedi';
import { Subscribe } from 'unstated';
import { AlertBuilder } from '@app/components/atm.alert-builder';
import { ListCell } from '@app/components/mol.cell';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import {
  FlashMessageService,
  GuardProps,
  Navigation,
} from '@app/core/navigation';
import {
  UnregisterPushInput,
  UnregisterPushMutationVariables,
} from '@app/data/graphql';
import { GuardParams } from '@app/modules/authentication';
import { BeneficiaryProvider } from '@app/services/providers';
import { Asset, PrimaryButton, Root, VBox, VSeparator } from '@atomic';
import { AccountStrings } from './account.strings';
import { ChangeBeneficiaryInfoPage } from './change-beneficiary-info';
import { ChangePasswordPage } from './change-password';
import { HealthCareCardPage } from './health-care-card';
import { EditBenefData } from '@app/config/tokens.config';
import { NewsPage } from '@app/modules/news/news.page';
import { StatusAttendancePage } from '../attendance/attendance.page';
import { PublicationsPage } from '@app/modules/magazines/publications.page';
import { unregisterPushChat } from '@app/domain/push-notification/api/push-register.service';

const { PushNotificationBridge } = NativeModules as any;
import messaging from '@react-native-firebase/messaging';
import { H1, H3 } from '@atomic/atm.typography';
interface AccountPageProps extends GuardProps<GuardParams> {
  componentId: string;
}

@PageView('Account')
export class AccountPage extends React.Component<AccountPageProps, undefined> {
  static options = {
    name: 'AccountPage',
    topBar: {
      backButton: {
        visible: false,
      },
      rightButtons: [
        // {
        //   id: 'EmergencyButton',
        //   component: { name: 'EmergencyButton' },
        // },
      ],
    },
  };

  private beneficiaryProvider: BeneficiaryProvider;
  private graph: GraphQLProvider = Container.get(GraphQLProviderToken);

  private editBenefData: boolean = Container.get(EditBenefData);

  render() {
    const activeBeneficiary = this.props.guard.params.active;

    return (
      <CollapsibleHeader title={activeBeneficiary && activeBeneficiary.name}>
        <Root>
          <ListCell
            text={AccountStrings.ChangePassword}
            leftThumb={Asset.Icon.Custom.Lock}
            rightThumb={Asset.Icon.Atomic.ChevronRight}
            onTap={this.handleChangePassword}
          />
          {/* <ListCell
            text={this.editBenefData ? AccountStrings.ChangeData : AccountStrings.ViewData}
            leftThumb={Asset.Icon.Custom.Summary}
            rightThumb={Asset.Icon.Atomic.ChevronRight}
            onTap={this.handleChangeBeneficiaryInfo}
          /> */}
          {/* <ListCell
            text={AccountStrings.SeeHealthCareCard}
            leftThumb={Asset.Icon.Custom.HealthCareCard}
            rightThumb={Asset.Icon.Atomic.ChevronRight}
            onTap={this.handleHealthCareCardTap}
          />
          <ListCell
            text={'Notícias'}
            leftThumb={Asset.Icon.Custom.HealthCareCard}
            rightThumb={Asset.Icon.Atomic.ChevronRight}
            onTap={this.handleNewsTap}
          />
          <ListCell
            text={'Status de senha'}
            leftThumb={Asset.Icon.Custom.HealthCareCard}
            rightThumb={Asset.Icon.Atomic.ChevronRight}
            onTap={this.handleAttendanceTap} /> 
          <ListCell
            text={'Revistas e Manuais'}
            leftThumb={Asset.Icon.Custom.HealthCareCard}
            rightThumb={Asset.Icon.Atomic.ChevronRight}
            onTap={this.handleMagazinesTap} 
          /> */}
          <VSeparator />
          <VBox>
            <Subscribe to={[BeneficiaryProvider]}>
              {(beneficiaryProvider: BeneficiaryProvider) => {
                this.beneficiaryProvider = beneficiaryProvider;

                return (
                  <PrimaryButton
                    negative={true}
                    expanded={true}
                    text={AccountStrings.Logout}
                    onTap={this.handleLogout}
                  />
                );
              }}
            </Subscribe>
          </VBox>
          <VSeparator />
          <VSeparator />
          <VSeparator />
          <VBox>
            <H1>{'CONTATO'}</H1>
            <H3>{'E-mail: faleconoscoqualimais@qualicorp.net.br'}</H3>
            <H3>
              {'Hórario Expediente do Chat: segunda à sexta das 07:00-19:00.'}
            </H3>
          </VBox>
        </Root>
      </CollapsibleHeader>
    );
  }

  private handleChangeBeneficiaryInfo = () => {
    Navigation.push(this.props.componentId, ChangeBeneficiaryInfoPage, {
      onSuccess: (message: string) => FlashMessageService.showSuccess(message),
    });
  };

  private handleChangePassword = () => {
    Navigation.push(this.props.componentId, ChangePasswordPage, {
      onSuccess: (message: string) => FlashMessageService.showSuccess(message),
    });
  };

  private handleHealthCareCardTap = () => {
    Navigation.showModal(HealthCareCardPage);
  };

  private handleNewsTap = () => {
    Navigation.push(this.props.componentId, NewsPage);
  };

  private handleAttendanceTap = () => {
    Navigation.push(this.props.componentId, StatusAttendancePage);
  };

  private handleMagazinesTap = () => {
    Navigation.push(this.props.componentId, PublicationsPage);
  };

  private handleLogout = () => {
    new AlertBuilder()
      .withTitle(AccountStrings.AlertTitle)
      .withMessage(AccountStrings.AlertMessage)
      .withButton({ text: AccountStrings.Cancel, style: 'cancel' })
      .withButton({
        text: AccountStrings.Confirm,
        onPress: this.handleLogoutConfirmation,
      })
      .show();
  };

  private handleLogoutConfirmation = async () => {
    this.beneficiaryProvider.logout();
    this.unregisterPush();
    Navigation.setCurrentTab(this.props.componentId, 0);
  };

  private unregisterPush = async () => {
    //const token = await PushNotificationBridge?.getRegistrationDeviceToken();
    await messaging().registerDeviceForRemoteMessages();
    const token = await messaging().getToken();
    const input: UnregisterPushInput = { deviceToken: token };
    unregisterPushChat(input);
    // this.graph.mutate<UnregisterPush
    // MutationVariables>(
    //   'unregister-push',
    //   { data: input },
    //   null,
    //   error =>
    //     console.warn('Eror unregistering user for push notification:', error),
    // );
  };
}
