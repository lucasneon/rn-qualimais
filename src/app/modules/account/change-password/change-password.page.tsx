import * as React from 'react';
import { BackHandler } from 'react-native';
import { Container } from 'typedi';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services/providers';
import { Color, ErrorBody, VBox } from '@atomic';
import { ChangePasswordForm } from './change-password-form.component';
import { ChangePasswordStrings } from './change-password.strings';

export const Fields = {
  newPassword: 'newPassword',
  newPasswordConfirmation: 'newPasswordConfirmation',
  currentPassword: 'currentPassword',
};

export interface ChangePasswordNavigationProps {
  mustChangePassword: boolean;
  onSuccess: (message: string) => any;
}

type ChangePasswordProps = NavigationPageProps<ChangePasswordNavigationProps>;

@PageView('ChangePassword')
export class ChangePasswordPage extends React.Component<ChangePasswordProps> {
  static options = { name: 'ChangePasswordPage' };

  private beneficiaryProvider: BeneficiaryProvider = Container.get(BeneficiaryProviderToken);

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  componentDidAppear() {
    if (this.props.navigation.mustChangePassword) {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackTap);
    }
  }

  componentDidDisappear() {
    if (this.props.navigation.mustChangePassword) {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackTap);
      this.beneficiaryProvider.isLoginActive = false;
    }
  }

  render() {
    if (this.props.navigation.mustChangePassword) {
      Navigation.mergeOptions(this.props.componentId, { topBar: { leftButtons: [] } });
    } else {
      Navigation.mergeOptions(this.props.componentId, {topBar: {leftButtonColor: {color: Color.Primary}}});
    }
    return (
      <CollapsibleHeader title={ChangePasswordStrings.Title}>
      {this.props.navigation.mustChangePassword &&
        <VBox>
          <ErrorBody>{ChangePasswordStrings.MustChangePassword}</ErrorBody>
        </VBox>
      }
        <ChangePasswordForm onSuccess={this.onSuccess}/>
      </CollapsibleHeader>
    );
  }

  private handleBackTap = () => {
    this.beneficiaryProvider.logout();
    BackHandler.exitApp();
  }

  private onSuccess = async (data: string) => {
    this.props.navigation.onSuccess(data);
    if (this.props.navigation.mustChangePassword) {
      await Navigation.dismissAllModals();
    } else {
      await Navigation.pop(this.props.componentId);
    }
  }
}
