// tslint:disable:max-line-length
export const ChangePasswordStrings = {
  Title: 'Alterar Senha',
  Labels: {
    CurrentPassword: 'Senha atual',
    NewPassword: 'Nova senha',
    NewPasswordConfirmation: 'Confirme a senha',
  },
  Validations: {
    Password: 'No mínimo 4 caracteres',
    ConfirmPassword: 'Deve ser igual a nova senha',
  },
  SubmitButtonText: 'Salvar',
  MustChangePassword: 'Necesssária mudança de senha no primeiro acesso',
};
// tslint:enable:max-line-length
