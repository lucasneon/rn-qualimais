import * as React from 'react';
import { Subscribe } from 'unstated';
import { GraphQLProvider } from '@app/core/graphql/';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { ChangePasswordMutationVariables } from '@app/data/graphql';
import { ErrorMapper } from '@app/utils';
import { Form, TextField, Validators, VBox, VSeparator } from '@atomic';
import { PrimaryButton } from '@atomic/atm.button/button-types.component';
import { ChangePasswordStrings } from './change-password.strings';
import { changePassword } from '../api/change-password/change-password.service';

export const Fields = {
  newPassword: 'newPassword',
  newPasswordConfirmation: 'newPasswordConfirmation',
  currentPassword: 'currentPassword',
};

export interface ChangePasswordFormProps {
  onSuccess: (message: string) => any;
}

export interface ChangePasswordFormState {
  password: string;
}

export class ChangePasswordForm extends React.Component<
  ChangePasswordFormProps,
  ChangePasswordFormState
> {
  private graph: GraphQLProvider;

  constructor(props) {
    super(props);
    this.state = { password: '' };
  }

  render() {
    return (
      <Subscribe to={[GraphQLProvider]}>
        {(graph: GraphQLProvider) => {
          this.graph = graph;
          return (
            <Form onSubmit={this.handleSubmit}>
              <Form.Scroll>
                <VBox>
                  <VSeparator />
                  <Form.Field
                    label={ChangePasswordStrings.Labels.CurrentPassword}
                    validators={[
                      Validators.Required(),
                      Validators.MinLength(
                        4,
                        ChangePasswordStrings.Validations.Password,
                      ),
                    ]}
                    name={Fields.currentPassword}>
                    <TextField secureTextEntry={true} />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangePasswordStrings.Labels.NewPassword}
                    validators={[
                      Validators.Required(),
                      Validators.MinLength(
                        4,
                        ChangePasswordStrings.Validations.Password,
                      ),
                    ]}
                    name={Fields.newPassword}
                    onValueChange={this.handlePasswordInput}>
                    <TextField secureTextEntry={true} />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangePasswordStrings.Labels.NewPasswordConfirmation}
                    validators={[
                      Validators.Required(),
                      Validators.MinLength(
                        4,
                        ChangePasswordStrings.Validations.Password,
                      ),
                      Validators.EqualsValue(
                        this.state.password,
                        ChangePasswordStrings.Validations.ConfirmPassword,
                      ),
                    ]}
                    name={Fields.newPasswordConfirmation}>
                    <TextField secureTextEntry={true} />
                  </Form.Field>
                  <VSeparator />
                  <Form.Submit>
                    <PrimaryButton
                      text={ChangePasswordStrings.SubmitButtonText}
                      expanded={true}
                      loading={graph.state.loading}
                      submit={true}
                    />
                  </Form.Submit>
                  <VSeparator />
                </VBox>
              </Form.Scroll>
            </Form>
          );
        }}
      </Subscribe>
    );
  }

  private handleSubmit = (formData?: any) => {
    if (Object.keys(formData.error).length === 0) {
      this.changePasswordMutation(
        formData.data.currentPassword,
        formData.data.newPassword,
      );
    }
  };

  private handlePasswordInput = (input: string) => {
    this.setState({ password: input });
  };

  private changePasswordMutation = (
    currentPassword: string,
    newPassword: string,
  ) => {
    changePassword({
      currentPassword,
      newPassword,
    })
      .then(res => {
        if (res.error) {
          this.handleMutationError(res.error);
        } else {
          this.handleMutationSuccess({ ChangePassword: res.data });
        }
      })
      .catch(err => this.handleMutationError(err));
    // this.graph.mutate<ChangePasswordMutationVariables>(
    //   'change-password',
    //   {
    //     data: {
    //       currentPassword,
    //       newPassword,
    //     },
    //   },
    //   this.handleMutationSuccess,
    //   this.handleMutationError,
    // );
  };

  private handleMutationSuccess = data => {
    this.props.onSuccess(data.ChangePassword);
  };

  private handleMutationError = error => {
    FlashMessageService.showError(ErrorMapper.map(error));
  };
}
