import * as React from 'react';
import { View } from 'react-native-animatable';
import { Subscribe } from 'unstated';
import { PhoneField } from '@app/components/atm.phone-field';
import { PickerStateField } from '@app/components/atm.picker-state';
import { BeneficiaryInfoInput } from '@app/data/graphql';
import { cleanText, DateUtils } from '@app/utils';
import {
  FieldShimmer,
  Form,
  FormSubmitData,
  TextField,
  Validators,
  VBox,
  VSeparator,
} from '@atomic';
import { PrimaryButton } from '@atomic/atm.button/button-types.component';
import { ChangeBeneficiaryInfoStrings } from './change-beneficiary-info.strings';
import { BeneficiaryProvider } from '@app/services';
import { Beneficiary } from '@app/model';
import { EditBenefData } from '@app/config/tokens.config';
import { Container } from 'typedi';
import { BeneficiaryInfoModel } from '@app/model/api/beneficiary-info.model';
import { execUpdateBeneficiaryInfo } from '@app/modules/authentication/api/login/beneficiary.service';

const Fields = {
  name: 'name',
  idHealthCareCard: 'idHealthCareCard',
  cpf: 'cpf',
  birthdate: 'birthDate',
  email: 'email',
  phone: 'phone',
  state: 'state',
  city: 'city',
  district: 'district',
  cep: 'cep',
  street: 'street',
  addressNumber: 'addressNumber',
  complement: 'complement',
};

export interface ChangeBeneficiaryInfoFormProps {
  beneficiaryInfo: BeneficiaryInfoModel;
  onMutationSuccess: (beneficiary: BeneficiaryInfoModel) => void;
  onMutationError: (error: any) => void;
}

export const FormShimmer: React.SFC = () => (
  <View>
    {new Array(6).fill(null).map((_, index) => (
      <FieldShimmer key={index} />
    ))}
  </View>
);

export class ChangeBeneficiaryInfoForm extends React.Component<ChangeBeneficiaryInfoFormProps> {
  private beneficiaryProvider: BeneficiaryProvider;

  private editBenefData: boolean = Container.get(EditBenefData);

  constructor(props) {
    super(props);
    this.state = { loading: false };
  }

  render() {
    const { address } = this.props.beneficiaryInfo;
    // tslint:disable:max-line-length
    return (
      <Subscribe to={[BeneficiaryProvider]}>
        {(beneficiary: BeneficiaryProvider) => {
          this.beneficiaryProvider = beneficiary;
          const loading = beneficiary.state.loading;
          return (
            <Form onSubmit={this.handleSubmit}>
              <Form.Scroll>
                <VBox>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.Email}
                    validators={[
                      Validators.Required(),
                      Validators.IsEmail(
                        ChangeBeneficiaryInfoStrings.Validation.Email,
                      ),
                    ]}
                    name={Fields.email}
                    value={this.props.beneficiaryInfo.email}>
                    <TextField
                      autoCapitalize={'none'}
                      editable={this.editBenefData}
                      autoCorrect={false}
                      type={TextField.Type.Email}
                    />
                  </Form.Field>
                  <VSeparator />
                  <PhoneField
                    required={true}
                    label={ChangeBeneficiaryInfoStrings.Labels.Phone}
                    name={Fields.phone}
                    initialValue={this.props.beneficiaryInfo.phone || ''}
                    editable={this.editBenefData}
                  />
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.Cep}
                    validators={[
                      Validators.Required(),
                      Validators.ZipCode(
                        ChangeBeneficiaryInfoStrings.Validation.ZipCode,
                      ),
                    ]}
                    name={Fields.cep}
                    value={address?.cep || ''}>
                    <TextField
                      type={TextField.Type.ZipCode}
                      editable={this.editBenefData}
                    />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.Street}
                    validators={[Validators.Required()]}
                    name={Fields.street}
                    value={address?.street || ''}>
                    <TextField
                      autoCorrect={false}
                      editable={this.editBenefData}
                    />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.AddressNumber}
                    validators={[Validators.Required()]}
                    name={Fields.addressNumber}
                    value={
                      address?.addressNumber
                        ? address.addressNumber.toString()
                        : ''
                    }>
                    <TextField
                      type={TextField.Type.Numeric}
                      editable={this.editBenefData}
                    />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={
                      this.editBenefData
                        ? ChangeBeneficiaryInfoStrings.Labels.Complement.Edit
                        : ChangeBeneficiaryInfoStrings.Labels.Complement.View
                    }
                    name={Fields.complement}
                    value={address?.complement}>
                    <TextField
                      autoCorrect={false}
                      editable={this.editBenefData}
                    />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.District}
                    validators={[Validators.Required()]}
                    name={Fields.district}
                    value={address?.district}>
                    <TextField
                      autoCorrect={false}
                      editable={this.editBenefData}
                    />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.City}
                    validators={[Validators.Required()]}
                    name={Fields.city}
                    value={address?.city}>
                    <TextField
                      autoCorrect={false}
                      editable={this.editBenefData}
                    />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.State}
                    validators={[Validators.Required()]}
                    name={Fields.state}
                    value={address?.state}>
                    {this.editBenefData ? (
                      <PickerStateField />
                    ) : (
                      <TextField editable={this.editBenefData} />
                    )}
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.Name}
                    name={Fields.name}
                    value={this.props.beneficiaryInfo.name}>
                    <TextField editable={false} />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.Cpf}
                    name={Fields.cpf}
                    validators={[
                      Validators.Cpf(
                        ChangeBeneficiaryInfoStrings.Validation.Cpf,
                      ),
                    ]}
                    value={this.props.beneficiaryInfo.cpf}>
                    <TextField type={TextField.Type.CPF} editable={false} />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.IdHealthCareCard}
                    name={Fields.idHealthCareCard}
                    value={this.props.beneficiaryInfo.idHealthCareCard}>
                    <TextField editable={false} />
                  </Form.Field>
                  <VSeparator />
                  <Form.Field
                    label={ChangeBeneficiaryInfoStrings.Labels.BirthDate}
                    name={Fields.birthdate}
                    validators={[]}
                    value={DateUtils.Format.presentation(
                      this.props.beneficiaryInfo.birthDate,
                    )}>
                    <TextField type={TextField.Type.Date} editable={false} />
                  </Form.Field>
                  <VSeparator />
                  {this.editBenefData && (
                    <Form.Submit>
                      <PrimaryButton
                        text={ChangeBeneficiaryInfoStrings.SubmitButtonText}
                        expanded={true}
                        loading={loading}
                        submit={true}
                      />
                    </Form.Submit>
                  )}
                  <VSeparator />
                </VBox>
              </Form.Scroll>
            </Form>
          );
        }}
      </Subscribe>
    );
  }

  private handleSubmit = (formData: FormSubmitData) => {
    if (Object.keys(formData.error).length !== 0) {
      return;
    }

    const data: BeneficiaryInfoInput = {
      email: formData.data.email,
      phone: cleanText(formData.data.phone),
      address: {
        state: formData.data.state,
        city: formData.data.city,
        district: formData.data.district,
        cep: cleanText(formData.data.cep),
        street: formData.data.street,
        addressNumber: formData.data.addressNumber,
        complement: formData.data.complement,
      },
    };
    this.changeBeneficiaryInfoMutation(data);
  };

  private handleMutationSuccess = async (data: BeneficiaryInfoInput) => {
    const activeBeneficiary: Beneficiary =
      this.beneficiaryProvider.state.active;
    activeBeneficiary.city = data.address.city;
    activeBeneficiary.state = data.address.state;
    await this.beneficiaryProvider.setActive(activeBeneficiary);
  };

  private changeBeneficiaryInfoMutation = (data: BeneficiaryInfoInput) => {
    execUpdateBeneficiaryInfo(data)
      .then(async res => {
        await this.handleMutationSuccess(res);
        this.props.onMutationSuccess(res);
      })
      .catch(err => {
        this.props.onMutationError(err);
      });
    // this.graph.mutate<ChangeBeneficiaryInfoMutationVariables>(
    //   'change-beneficiary-info',
    //   { data },
    //   (this.handleMutationSuccess(data), this.props.onMutationSuccess),
    //   this.props.onMutationError,
    // );
  };
}
