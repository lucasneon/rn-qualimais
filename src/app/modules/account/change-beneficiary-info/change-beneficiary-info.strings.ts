export const ChangeBeneficiaryInfoStrings = {
  Labels: {
    Name: 'Nome',
    Cpf: 'CPF',
    BirthDate: 'Data de nascimento',
    IdHealthCareCard: 'Carteirinha',
    Email: 'E-mail',
    Phone: 'Telefone',
    State: 'Estado',
    City: 'Cidade',
    District: 'Bairro',
    Cep: 'CEP',
    Street: 'Rua',
    AddressNumber: 'Número',
    Complement: {
      Edit: 'Complemento (opcional)',
      View: 'Complemento',
    },
  },
  Validation: {
    Email: 'Deve ser um email válido',
    ZipCode: 'Deve ser um CEP válido',
    Cpf: 'Deve ser um CPF válido',
  },
  SubmitButtonText: 'Salvar',
  Title: {
    ChangeData: 'Alterar Dados',
    ViewData: 'Visualizar Dados',
  },
  Success: 'Dados alterados com sucesso',
};
