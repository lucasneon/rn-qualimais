import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { QueryContainer } from '@app/core/graphql';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { BeneficiaryInfoQuery } from '@app/data/graphql';
import { ErrorMapper } from '@app/utils';
import { Color, LoadingState, Root } from '@atomic';
import { ChangeBeneficiaryInfoForm, FormShimmer } from './change-beneficiary-info-form.component';
import { ChangeBeneficiaryInfoStrings } from './change-beneficiary-info.strings';
import { EditBenefData } from '@app/config/tokens.config';
import { Container } from 'typedi';
import { BeneficiaryProvider } from '@app/services';
import { Subscribe } from 'unstated';
import { BeneficiaryInfoModel } from '@app/model/api/beneficiary-info.model';

export interface ChangeBeneficiaryInfoNavigationProps {
  onSuccess: (message: string) => any;
}

type ChangeBeneficiaryInfoProps = NavigationPageProps<ChangeBeneficiaryInfoNavigationProps>;

@PageView('ChangeBeneficiaryInfo')
export class ChangeBeneficiaryInfoPage extends React.Component<ChangeBeneficiaryInfoProps> {
  static options = { name: 'ChangeBeneficiaryInfoPage' };

  private editBenefData: boolean = Container.get(EditBenefData);

  private beneficiaryProvider: BeneficiaryProvider;

  componentDidMount() {
    Navigation.mergeOptions(this.props.componentId, { topBar: { leftButtonColor: { color: Color.Primary } } });
    this.beneficiaryProvider.getBeneficiaryinfo();
  }
  // tslint:disable:max-line-length
  render() {
    return (
      <CollapsibleHeader
        title={
          this.editBenefData
            ? ChangeBeneficiaryInfoStrings.Title.ChangeData
            : ChangeBeneficiaryInfoStrings.Title.ViewData
        }>
        <Root>
          <Subscribe to={[BeneficiaryProvider]}>
            {(beneficiaryProvider: BeneficiaryProvider) => {
              this.beneficiaryProvider = beneficiaryProvider;
              const beneficiaryInfo = beneficiaryProvider.state.beneficiaryInfo;
              const error = beneficiaryProvider.state.error;
              const loading = beneficiaryProvider.state.loading;
              const handlePlaceholderTap = () =>
                beneficiaryProvider.rehydrate();
              return (
                <LoadingState
                  data={!!beneficiaryInfo}
                  error={!!error}
                  loading={loading}>
                  <LoadingState.Shimmer>
                    <FormShimmer />
                  </LoadingState.Shimmer>
                  <LoadingState.ErrorPlaceholder>
                    <PlaceholderSelector
                      error={ErrorMapper.map(error)}
                      onTap={handlePlaceholderTap}
                    />
                  </LoadingState.ErrorPlaceholder>
                  <ChangeBeneficiaryInfoForm
                    onMutationSuccess={this.handleMutationSuccess}
                    onMutationError={this.handleMutationError}
                    beneficiaryInfo={beneficiaryInfo}
                  />
                </LoadingState>
              );
            }}
          </Subscribe>
          {/* TODO alterar para rest */}
          {/* <QueryContainer document='beneficiary-info'>
            {(res: QueryResult<BeneficiaryInfoQuery>) => {
              const handlePlaceholderTap = () => res.refetch();
              const beneficiaryInfo = res.data && res.data.BeneficiaryInfo;
              return (
                <LoadingState data={!!beneficiaryInfo} error={!!res.error} loading={res.loading}>
                  <LoadingState.Shimmer>
                    <FormShimmer />
                  </LoadingState.Shimmer>
                  <LoadingState.ErrorPlaceholder>
                    <PlaceholderSelector error={ErrorMapper.map(res.error)} onTap={handlePlaceholderTap} />
                  </LoadingState.ErrorPlaceholder>
                  <ChangeBeneficiaryInfoForm
                    onMutationSuccess={this.handleMutationSuccess}
                    onMutationError={this.handleMutationError}
                    beneficiaryInfo={beneficiaryInfo}
                  />
                </LoadingState>
              );
            }}
          </QueryContainer> */}
        </Root>
      </CollapsibleHeader>
    );
  }

  private handleMutationSuccess = (beneficiary: BeneficiaryInfoModel) => {
    this.beneficiaryProvider.setState({ beneficiaryInfo: beneficiary });
    this.props.navigation.onSuccess(ChangeBeneficiaryInfoStrings.Success);
    Navigation.pop(this.props.componentId);
  }

  private handleMutationError = (error: any) => {
    FlashMessageService.showError(ErrorMapper.map(error));
  }
}
