import { PageView } from "@app/core/analytics";
import { BackHandler, ScrollView } from "react-native";
import * as React from 'react';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { AcceptanceTermStrings } from "./acceptance-term.string";
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { BeneficiaryProvider, BeneficiaryProviderToken } from "@app/services";
import { Container } from 'typedi';
import { AcceptanceTermForm } from './acceptance-term-form.componet';
import HTML from 'react-native-render-html';
import { VBox } from "@atomic";

export interface AcceptanceTermNavigateProps {
    mustChangePassword: boolean;
    html?: string;
    onSuccess: (message: string) => any;
}

export interface AcceptanceTermPageState {
    html: string;
}

type AcceptanceTermProps = NavigationPageProps<AcceptanceTermNavigateProps>;


@PageView('AcceptanceTermPage')
export class AcceptanceTermPage extends React.Component<AcceptanceTermProps, AcceptanceTermPageState> {
    static options = { name: 'AcceptanceTermPage' };
    private beneficiaryProvider: BeneficiaryProvider = Container.get(BeneficiaryProviderToken);

    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }

    render() {
        Navigation.mergeOptions(this.props.componentId, { topBar: { leftButtons: [] } });
        return (
            <>
                <ScrollView style={{ flex: 1 }}>
                    <CollapsibleHeader title={AcceptanceTermStrings.Title} >
                        <VBox>
                            <HTML html={this.props.navigation.html} />
                        </VBox>
                        <AcceptanceTermForm onSuccess={this.onSuccess}
                            mustChangePassword={this.props.navigation.mustChangePassword} />
                    </CollapsibleHeader>
                </ScrollView>
            </>);
    }

    componentDidAppear() {
        if (this.props.navigation.mustChangePassword) {
            BackHandler.addEventListener('hardwareBackPress', this.handleBackTap);
        }
    }

    componentDidDisappear() {
        if (this.props.navigation.mustChangePassword) {
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackTap);
            this.beneficiaryProvider.isLoginActive = false;
        }
    }

    private handleBackTap = () => {
        this.beneficiaryProvider.logout();
        BackHandler.exitApp();
    }

    // TODO testar função
    private onSuccess = (data: string) => {
        this.props.navigation.onSuccess(data);
        if (this.props.navigation.mustChangePassword) {
            Navigation.dismissAllModals();
        } else {
            Navigation.pop(this.props.componentId);
        }
    }
}
