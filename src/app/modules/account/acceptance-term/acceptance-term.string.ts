export const AcceptanceTermStrings = {
    Title:'Termo de Consentimento',
    Labels: {
        ButtonAceit:'Aceitar',
        ButtonSair: 'Sair'
    }
}