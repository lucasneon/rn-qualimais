import * as React from 'react';
import {
  Form,
  VBox,
  VSeparator,
  PrimaryButton,
  SecondaryButton,
} from '@atomic';
import { NativeModules } from 'react-native';
import { AcceptanceTermStrings } from './acceptance-term.string';
import { Navigation, FlashMessageService } from '@app/core/navigation';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services';
import { Container } from 'typedi';
import { HomePage } from '@app/modules/home';
import {
  UnregisterPushInput,
  UnregisterPushMutationVariables,
  AcceptTermOfUseMutationVariables,
} from '@app/data/graphql';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import {
  ChangePasswordNavigationProps,
  ChangePasswordPage,
} from '../change-password';
import { ErrorMapper } from '@app/utils';
import { LoginPage } from '@app/modules/authentication';
import { unregisterPushChat } from '@app/domain/push-notification/api/push-register.service';
import { acceptTermOfUseInput } from '@app/modules/authentication/api/login/accept-term-of-use.service';

const { PushNotificationBridge } = NativeModules as any;

export interface AcceptanceTermFormProps {
  onSuccess: (message: string) => any;
  mustChangePassword?: boolean;
}

export interface AcceptanceTermFormState {
  password: string;
}

export class AcceptanceTermForm extends React.Component<
  AcceptanceTermFormProps,
  AcceptanceTermFormState
> {
  private graph: GraphQLProvider = Container.get(GraphQLProviderToken);
  private beneficiaryProvider: BeneficiaryProvider = Container.get(
    BeneficiaryProviderToken,
  );
  private activeBenef = this.beneficiaryProvider.state.active;

  constructor(props) {
    super(props);
    this.state = { password: '' };
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <VBox>
          <VSeparator />
          <PrimaryButton
            text={AcceptanceTermStrings.Labels.ButtonAceit}
            expanded={true}
            submit={true}
            onTap={this.handleConfirmation}
          />
          <VSeparator />
          <SecondaryButton
            text={AcceptanceTermStrings.Labels.ButtonSair}
            expanded={true}
            submit={true}
            onTap={this.handleLogoutConfirmation}
          />
        </VBox>
      </Form>
    );
  }

  private handleSubmit = (formData?: any) => {};

  private changePasswordMutation = async (
    consentTerm: string,
    use: string,
    codeBenefiary: string,
  ) => {
    // TODO precisa testar
    acceptTermOfUseInput({
      use,
      codeBenefiary,
      consentTerm,
    })
      .then(() => this.handleMutationSuccess({}))
      .catch(res => this.handleMutationError(res.error));
    // await this.graph.mutate<AcceptTermOfUseMutationVariables>(
    //   'accept-term-of-use',
    //   {
    //     data: {
    //       use,
    //       codeBenefiary,
    //       consentTerm,
    //     },
    //   },
    //   this.handleMutationSuccess,
    //   this.handleMutationError,
    // );
  };

  private handleConfirmation = () => {
    const consentTerm = 'S';
    this.changePasswordMutation(
      consentTerm,
      this.activeBenef.name,
      this.activeBenef.code,
    );
  };

  private handleLogoutConfirmation = () => {
    this.unregisterPush();
    this.beneficiaryProvider.logout();
    Navigation.popToRoot(HomePage.options.name);
    Navigation.showModal(LoginPage);
  };

  private unregisterPush = async () => {
    const token = await PushNotificationBridge.getRegistrationDeviceToken();
    const input: UnregisterPushInput = { deviceToken: token };
    // TODO alterar para REST
    unregisterPushChat(input)
      .then(res => console.warn('unregistred success', res))
      .catch(error => console.warn('unregistred error', error));
    // this.graph.mutate<UnregisterPushMutationVariables>(
    //   'unregister-push',
    //   { data: input },
    //   null,
    //   error =>
    //     console.warn('Eror unregistering user for push notification:', error),
    // );
  };

  private handleFirstPasswordChange = async () => {
    await Navigation.showModal<ChangePasswordNavigationProps>(
      ChangePasswordPage,
      {
        mustChangePassword: true,
        onSuccess: (message: string) =>
          FlashMessageService.showSuccess(message),
      },
    );
  };
  private handleMutationSuccess = async data => {
    console.warn('aceito termos');
    if (this.props.mustChangePassword) {
      this.handleFirstPasswordChange();
    } else {
      await Navigation.dismissAllModals();
    }
  };

  private handleMutationError = error => {
    console.error('Error accepting terms of use:', error);
    FlashMessageService.showError(ErrorMapper.map(error));
  };
}
