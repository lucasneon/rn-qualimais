// tslint:disable:max-line-length
export const strings = {
  pleaseWait: 'Please wait...',
  updateAlert: {
    title: 'Aviso',
    message: 'Uma nova versão está disponível para download',
  },
};
// tslint:enable:max-line-length
