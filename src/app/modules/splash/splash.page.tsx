import * as React from 'react';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { Root } from '@atomic';
import {
  Button,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import SplashScreenImage from '@assets/images/login/bg-splash-screen.png';
import { LoginPage } from '../authentication';
import LogoQualiMais from '@assets/images/logo/logo-quali-mais.png';
import { Alert, Linking, Platform } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import DeviceInfo from 'react-native-device-info';

export class SplashPage extends React.Component<NavigationPageProps<{}>> {
  private versaoOk: boolean = true;

  static options = {
    name: 'SplashPage',
    topBar: {
      rightButtons: [],
      leftButtons: null,
      hardwareBackButton: {
        dismissModalOnPress: false,
      },
    },
    modalPresentationStyle: 'fullScreen',
  };

  onLogin = () => {
    if (!this.versaoOk) {
      this.fetchVersionFirebase();
      return;
    }
    Navigation.showModal(LoginPage);
  };

  componentDidMount() {
    this.fetchVersionFirebase();
  }

  render() {
    return (
      <Root>
        <ImageBackground
          source={SplashScreenImage}
          style={{ flex: 1 }}
          resizeMode="cover">
          <Image
            source={LogoQualiMais}
            width={264}
            height={80}
            style={styles.logo}
          />
          <TouchableOpacity onPress={this.onLogin} style={styles.button}>
            <Text style={styles.btnText}>Entrar</Text>
          </TouchableOpacity>
        </ImageBackground>
      </Root>
    );
  }

  private fetchVersionFirebase = async () => {
    const isIos = Platform.OS === 'ios';
    var deviceOs = isIos ? 'ios' : 'android';

    firestore()
      .collection('versoes')
      .doc(deviceOs)
      .get()
      .then(snap => {
        let data = snap.data();

        let msgImpeditiva = data.mensagem_impeditiva;
        let msgAlerta = data.mensagem_alerta;
        let versaoAlerta = data.versao_alerta;
        let versaoImpeditiva = data.versao_impeditiva;
        let linkLoja = data.link_loja;

        var buildNumber = DeviceInfo.getBuildNumber();

        if (buildNumber < versaoImpeditiva) {
          Alert.alert(
            'Alerta',
            msgImpeditiva,
            [
              {
                text: 'Atualizar agora',
                onPress: () => this.onClickLoja(linkLoja),
              },
            ],
            { cancelable: false },
          );

          this.versaoOk = false;
        } else if (buildNumber < versaoAlerta) {
          Alert.alert('Alerta', msgAlerta, [
            {
              text: 'Ir para loja',
              onPress: () => this.onClickLoja(linkLoja),
            },
            {
              text: 'Cancelar',
            },
          ]);

          this.versaoOk = true;
        }
      });

    this.versaoOk = true;
  };

  private onClickLoja = async (linkLoja: string) => {
    Linking.openURL(linkLoja);
  };
}

const styles = StyleSheet.create({
  logo: {
    marginTop: 88,
    alignSelf: 'center',
  },
  button: {
    marginTop: 'auto',
    marginBottom: 60,
    alignSelf: 'center',
    maxWidth: 170,
    borderColor: '#fff',
    borderWidth: 2,
    borderRadius: 22,
    paddingLeft: 50,
    paddingRight: 50,
    paddingBottom: 10,
    paddingTop: 10,
    backgroundColor: 'rgba(28, 21, 14, 0.6)',
  },
  btnText: {
    color: '#ffff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
