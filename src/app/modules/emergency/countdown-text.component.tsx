import { ButtonTextStyled } from '@atomic/atm.button/button.component.style';
import * as React from 'react';

interface CountdownTextProps {
  duration?: number; // seconds
}

interface CountdownTextState {
  intervalId: any; // type Timer is not declared
  currentCount: number;
}

export class CountdownText extends React.PureComponent<CountdownTextProps, CountdownTextState> {

  constructor(props) {
    super(props);
    this.state = {
      currentCount: props.duration,
      intervalId: null,
    };
  }

  componentDidMount() {
    const intervalId = setInterval(this.timer, 1000);
    this.setState({ intervalId });
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  timer = () => {
    const newCount = this.state.currentCount - 1;
    if (newCount >= 0) {
      this.setState({ currentCount: newCount });
    } else {
      clearInterval(this.state.intervalId);
    }
  }

  stopTimer = () => {
    clearInterval(this.state.intervalId);
    this.setState({ currentCount: 0 });
  }

  render() {
    return (
      <ButtonTextStyled>
        {this.state.currentCount === 0 ? `Ligar` : `Ligando em ${this.state.currentCount}s (toque para ligar)`}
      </ButtonTextStyled>
    );
  }

}
