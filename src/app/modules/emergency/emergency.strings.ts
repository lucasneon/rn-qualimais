export const EmergencyStrings = {
  Title: 'Contato de Emergência',
  HeaderMessage: 'Você será direcionado(a) para uma chamada telefônica com nossa central em instantes:',
  HealthInfo: {
    BloodType: 'Sangue',
    Diabetes: 'Diabetes',
    Hypertension: 'Hipertensão',
    Allergies: 'Alergias',
    Procedures: 'Cirurgias',
    FamilyHistory: 'Histórico familiar',
  },
  PhoneNumber: '03001181002',
};
