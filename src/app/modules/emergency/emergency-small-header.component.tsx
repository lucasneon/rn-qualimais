import { TqSafeAreaView } from '@atomic';
import * as React from 'react';
import {
  EmergencySmallHeaderStyled,
  EmergencySmallHeaderTitleStyled
} from './emergency-small-header.component.style';

interface EmergencySmallHeaderProps {
  title: string;
}

export const EmergencySmallHeader: React.SFC<EmergencySmallHeaderProps> = (props: EmergencySmallHeaderProps) => {

  return (
    <EmergencySmallHeaderStyled>
      <TqSafeAreaView />
      <EmergencySmallHeaderTitleStyled >{props.title}</EmergencySmallHeaderTitleStyled>
    </EmergencySmallHeaderStyled>
  );
};
