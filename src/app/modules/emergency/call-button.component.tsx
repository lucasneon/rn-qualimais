import {
  ButtonContentStyled, ButtonStyled, ButtonThumbStyled,
  ButtonTouchableOpacityStyled
} from '@atomic/atm.button/button.component.style';
import * as React from 'react';
import { Animated } from 'react-native';
import { AnimatedButtonViewStyle, CallButtonWrapperStyled, TimedCallWidth } from './call-button.component.style';
import { CountdownText } from './countdown-text.component';

interface CallButtonProps {
  image: string;
  onTap?: () => void;
}

interface CallButtonState {
  alreadyCalled: boolean;
}

export class CallButton extends React.PureComponent<CallButtonProps, CallButtonState> {

  state = {
    alreadyCalled: false,
  };

  private countdownText;

  private animatedProgressValue;
  private widthInterpolated;

  constructor(props) {
    super(props);
    this.animatedProgressValue = new Animated.Value(0);

    this.widthInterpolated = this.animatedProgressValue.interpolate(
      {
        inputRange: [0, 100], outputRange: [0, 100],
      });
  }

  componentDidMount() {
    Animated.timing(this.animatedProgressValue, {
      toValue: TimedCallWidth,
      duration: 5000,
    }).start(this.handleAnimationEnd);
  }

  componentWillUnmount() {
    this.animatedProgressValue.stopAnimation();
  }

  handlePress = () => {
    this.setState({
      alreadyCalled: true,
    });
    this.animatedProgressValue.setValue(TimedCallWidth);
    this.countdownText.stopTimer();
    if (this.props.onTap) {
      this.props.onTap();
    }
  }

  render() {

    return (
      <ButtonStyled >
        <ButtonTouchableOpacityStyled
          onPress={this.handlePress}
          {...this.props}
        >
          <CallButtonWrapperStyled >
            <Animated.View
              style={[AnimatedButtonViewStyle, { width: (this.widthInterpolated), }]}
            />
            <ButtonContentStyled {...this.props}>
              {this.props.image &&
                <ButtonThumbStyled
                  source={this.props.image}
                />
              }
              <CountdownText ref={ref => this.countdownText = ref} duration={5} />
            </ButtonContentStyled>
          </CallButtonWrapperStyled>
        </ButtonTouchableOpacityStyled>
      </ButtonStyled >
    );
  }

  private handleAnimationEnd = ({ finished }) => {
    // will be false if stopAnimation() was called
    if (finished && !this.state.alreadyCalled && this.props.onTap) {
      this.handlePress();
    }
  }
}
