import styled from 'styled-components/native';
import { Color, FontSize } from '@atomic/obj.constants';
import { LargeHeaderStyled } from '@app/components/atm.header/large-header.component.style';
import { Body, H1Large } from '@atomic';

export const EmergencyLargeHeaderStyled = styled(LargeHeaderStyled)`
  height: auto;
  display: flex;
  flex: 1;
  background-color: ${Color.Accessory};
`;

export const EmergencyLargeHeaderBodyStyled = styled(Body)`
  color: ${Color.White};
  font-size: ${FontSize.Medium};
`;

export const EmergencyLargeHeaderTitleStyled = styled(H1Large)`
  color: ${Color.White};
`;
