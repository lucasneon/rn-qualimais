import * as React from 'react';
import { LargeHeaderBottomStyled } from '@app/components/atm.header/large-header.component.style';
import { Asset, TqSafeAreaView, VBox, VSeparator } from '@atomic';
import { CallButton } from './call-button.component';
import {
  EmergencyLargeHeaderBodyStyled, EmergencyLargeHeaderStyled, EmergencyLargeHeaderTitleStyled
} from './emergency-large-header.component.style';

interface EmergencyLargeHeaderProps {
  title: string;
  message: string;
  onExecution: () => void;
}

export const EmergencyLargeHeader: React.SFC<EmergencyLargeHeaderProps> = (props: EmergencyLargeHeaderProps) => {

  return (
    <EmergencyLargeHeaderStyled >
      <TqSafeAreaView />
      <VBox>
        <VSeparator />
        <VSeparator />
        <EmergencyLargeHeaderTitleStyled >{props.title}</EmergencyLargeHeaderTitleStyled>
        <VSeparator />
        <EmergencyLargeHeaderBodyStyled>
          {props.message}
        </EmergencyLargeHeaderBodyStyled>
        <VSeparator />
        <CallButton
          image={Asset.Icon.Custom.Phone}
          onTap={props.onExecution}
        />
        <VSeparator />
      </VBox>
      <LargeHeaderBottomStyled />
    </EmergencyLargeHeaderStyled>
  );
};
