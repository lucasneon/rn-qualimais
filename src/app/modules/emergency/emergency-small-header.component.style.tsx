import styled from 'styled-components/native';
import { SmallHeaderStyled, SmallHeaderTitleStyled } from '@app/components/atm.header/small-header.component.style';
import { Color } from '@atomic/obj.constants';

export const EmergencySmallHeaderStyled = styled(SmallHeaderStyled)`
  background-color: ${Color.Accessory};
`;

export const EmergencySmallHeaderTitleStyled = styled(SmallHeaderTitleStyled)`
  color: ${Color.White};
`;
