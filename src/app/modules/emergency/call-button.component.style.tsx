import { Dimensions } from 'react-native';
import styled from 'styled-components/native';
import { ButtonWrapperStyled } from '@atomic/atm.button/button.component.style';
import { Color, Spacing } from '@atomic/obj.constants';

export const TimedCallWidth = Dimensions.get('window').width - 2 * Spacing.Large;

export const CallButtonWrapperStyled = styled(ButtonWrapperStyled).attrs({
  outlined: true,
})`
  background-color: transparent;
  border-color: ${Color.White};
  overflow: hidden;
  width: ${TimedCallWidth};
`;

export const AnimatedButtonViewStyle = {
  position: 'absolute',
  left: 0,
  backgroundColor: '#000000',
  opacity: 0.3,
  height: '100%',
};
