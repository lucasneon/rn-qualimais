import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { Animated } from 'react-native';
import { Container } from 'typedi';
import { PageView } from '@app/core/analytics';
import { GraphQLProvider, GraphQLProviderToken, QueryContainer } from '@app/core/graphql';
import { ExternalNavigator, GuardProps, Navigation } from '@app/core/navigation';
import {
  AlertEmergencyMutationVariables, AllergiesQuery, FamilyHistoryQuery, ProceduresQuery
} from '@app/data/graphql';
import { GuardParams } from '@app/modules/authentication';
import { ModalPage } from '@app/modules/common';
import { mapFamilyHistory, mapProcedures, TagSection, TextSection } from '@app/modules/common/health-medical-record';
import { VitalSigns } from '@app/modules/common/vital-signs';
import { Asset, CollapsibleHeader, Color, Scroll, VBox, VSeparator } from '@atomic';
import { EmergencyLargeHeader } from './emergency-large-header.component';
import { EmergencySmallHeader } from './emergency-small-header.component';
import { EmergencyStrings } from './emergency.strings';

const AnimatedComponent = Animated.createAnimatedComponent(Scroll);

export interface EmergencyPageProps extends GuardProps<GuardParams> {
  componentId: string;
  navigation?: any;
}

@PageView('Emergency')
export class EmergencyPage extends ModalPage<EmergencyPageProps> {
  static options = { name: 'EmergencyPage', topBar: { rightButtons: [] } };

  private externalNavigator: ExternalNavigator = Container.get(ExternalNavigator);
  private graphqlProvider: GraphQLProvider = Container.get(GraphQLProviderToken);

  componentDidMount() {
    Navigation.mergeOptions(this.props.componentId, {
      topBar: {
        leftButtons: [
          {
            id: 'close',
            icon: Asset.Icon.NavBar.Close,
            color: Color.White,
          },
        ],
      },
    });
  }

  render() {
    return (
      <CollapsibleHeader topBar={true}>
        <CollapsibleHeader.LargeHeader>
          <EmergencyLargeHeader
            title={EmergencyStrings.Title}
            message={EmergencyStrings.HeaderMessage}
            onExecution={this.handleEmergencyExecution}
          />
        </CollapsibleHeader.LargeHeader>
        <CollapsibleHeader.SmallHeader>
          <EmergencySmallHeader title={EmergencyStrings.Title} />
        </CollapsibleHeader.SmallHeader>
        <AnimatedComponent>
          {// TODO Integrar esses dados
            /* <VSeparator />
          <HBox hAlign='center'>
            <VerticalDefinitionListCell
              topic={EmergencyStrings.HealthInfo.BloodType}
              definition='A+'
            />
            <VerticalDividerGray />
            <VerticalDefinitionListCell
              topic={EmergencyStrings.HealthInfo.Diabetes}
              definition='Não'
            />
            <VerticalDividerGray />
            <VerticalDefinitionListCell
              topic={EmergencyStrings.HealthInfo.Hypertension}
              definition='Não'
            />
          </HBox>
          <VSeparator /> */}
          <VitalSigns disabled={true} />
          <VBox>
            <VSeparator />

            <QueryContainer document='allergies' >
              {(result: QueryResult<AllergiesQuery>) => (
                <TagSection
                  title={EmergencyStrings.HealthInfo.Allergies}
                  tags={result.data && result.data.Allergies ? result.data.Allergies.map(al => al.display) : null}
                  loading={result.loading}
                  error={result.error}
                  readOnly={true}
                />
              )}
            </QueryContainer>

            <QueryContainer document='procedures' >
              {(result: QueryResult<ProceduresQuery>) => (
                <TextSection
                  title={EmergencyStrings.HealthInfo.Procedures}
                  tags={result.data && mapProcedures(result.data)}
                  loading={result.loading}
                  error={result.error}
                />
              )}
            </QueryContainer>

            <QueryContainer document='family-history'  >
              {(result: QueryResult<FamilyHistoryQuery>) => (
                <TextSection
                  title={EmergencyStrings.HealthInfo.FamilyHistory}
                  tags={result.data && mapFamilyHistory(result.data)}
                  loading={result.loading}
                  error={result.error}
                />
              )}
            </QueryContainer>

            <VSeparator />
          </VBox>
        </AnimatedComponent>
      </CollapsibleHeader>
    );
  }

  private handleEmergencyExecution = () => {
    this.externalNavigator.makeCall(EmergencyStrings.PhoneNumber, true);
    this.graphqlProvider.mutate<AlertEmergencyMutationVariables>(
      'alert-emergency',
      { sentAt: new Date().toISOString() },
      () => { console.warn('Request de emergência sucesso!'); },
      error => { console.warn('Request de emergência fail: ' + error); },
    );
  }
}
