import { Navigation } from '@app/core/navigation';
import { EmergencyPage } from './emergency.page';

export const EmergencyNavigation = {
  register: () => {
    Navigation.register(EmergencyPage);
  },
};
