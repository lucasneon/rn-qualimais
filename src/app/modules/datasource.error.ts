export interface DataSourceErrorInfoModel {
  endpoint?: string;
  errorCode?: number;
  serializedData: string;
}
