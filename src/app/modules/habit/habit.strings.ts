export const HabitStrings = {
  Habit: 'Hábito',
  Title: 'Hábitos',
  Sections: {
    Evolution: 'Evolução',
    Frequency: 'Frequência:',
    Configurations: 'Configurações',
    Time: 'Horários',
    Observations: 'Observações',
    Warnings: 'Avisos',
  },
  AvailableHabits: {
    Title: 'Hábitos disponíveis',
    Placeholder: {
      Title: 'Nenhum hábito encontrado',
      Message: 'Não há hábitos disponíveis no momento',
    },
    Active: 'ativo',
    Inactive: 'inativo',
  },
  FulfillDialog: {
    Title: 'Cumprir hábito',
    Message: (date: string) => `Marcar hábito como cumprido em ${date}?`,
    Success: 'Hábito cumprido com sucesso!',
  },
  Deactivate: {
    Title: 'Desativar hábito',
    Message: 'Deseja mesmo desativar o hábito e removê-lo de suas metas?',
    Confirm: 'Sim',
  },
  Activate: {
    Title: 'Ativar hábito',
    Message: 'Ao ativar o hábito, ele aparecerá nas suas metas. Deseja mesmo ativar?',
    Confirm: 'Sim',
  },
};
