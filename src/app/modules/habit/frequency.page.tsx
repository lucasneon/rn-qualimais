import * as React from 'react';
import { PlaceholderUnderConstruction } from '@app/components/mol.placeholder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { NavigationPageProps } from '@app/core/navigation';
import { HabitTypeFragment } from '@app/data/graphql';
import { Root } from '@atomic';

export interface FrequencyPageProps {
  habit: HabitTypeFragment;
}

@PageView('Frequency')
export class FrequencyPage extends React.Component<NavigationPageProps<FrequencyPageProps>, undefined> {
  static options = { name: 'FrequencyPage' };

  render() {
    const habit = this.props.navigation.habit;

    return (
      <CollapsibleHeader title={habit.display}>
        <Root>
          <PlaceholderUnderConstruction />
        </Root>
      </CollapsibleHeader>
    );
  }
}
