import { Navigation } from '@app/core/navigation';
import { FrequencyPage } from './frequency.page';
import { HabitDetailPage } from './habit-detail.page';
import { HabitsPage } from './habits.page';
import { MedicationAddPage } from './medication/medication-add.page';
import { MedicationDetailsPage } from './medication/medication-detail.page';
import { MedicationListPage } from './medication/medication-list.page';

export const HabitNavigation = {
  register: () => {
    Navigation.register(HabitsPage);
    Navigation.register(HabitDetailPage);
    Navigation.register(MedicationListPage);
    Navigation.register(MedicationDetailsPage);
    Navigation.register(MedicationAddPage);
    Navigation.register(FrequencyPage);
  },
};
