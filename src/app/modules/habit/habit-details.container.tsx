import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { QueryContainer } from '@app/core/graphql';
import {
  HabitDetailsInput,
  HabitDetailsQuery,
  HabitFragment,
} from '@app/data/graphql';
import { Subscribe } from 'unstated';
import { HabitsProvider } from '@app/services';

export interface HabitDetailsContainerChildrenParams {
  habitDetails?: HabitFragment;
  loading?: boolean;
  error?: any;
  refetch?: () => void;
}

export interface HabitsContainerProps {
  habitDetailsInput: HabitDetailsInput;
  children: (params: HabitDetailsContainerChildrenParams) => React.ReactNode;
}

export class HabitDetailsContainer extends React.PureComponent<HabitsContainerProps> {
  private habitsProvider: HabitsProvider;

  componentDidMount() {
    this.habitsProvider.fetchDetails(this.props.habitDetailsInput);
  }
  render() {
    //const inputData: HabitDetailsInput = this.props.habitDetailsInput;
    return (
      <Subscribe to={[HabitsProvider]}>
        {(habitsProvider: HabitsProvider) => {
          this.habitsProvider = habitsProvider;
          const error = this.habitsProvider.state.error;
          const loading = this.habitsProvider.state.loading;
          return this.props.children({
            loading: loading,
            error: error,
            refetch: null,
            habitDetails: this.habitsProvider.state.habitsDetail,
          });
        }}
      </Subscribe>
      // <QueryContainer document='habit-details' variables={{ data: inputData }} >
      //   {(result: QueryResult<HabitDetailsQuery>) => {
      //     return this.props.children(this.getChildParams(result));
      //   }}
      // </QueryContainer>
    );
  }

  private getChildParams(
    result: QueryResult<HabitDetailsQuery>,
  ): HabitDetailsContainerChildrenParams {
    const { loading, error, refetch } = result;
    const habitDetails = result && result.data && result.data.HabitDetails;
    return { loading, error, habitDetails, refetch };
  }
}
