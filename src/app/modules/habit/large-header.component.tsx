import * as React from 'react';
import { BigCheck } from '@app/components/atm.big-check/big-check.component';
import { LargeHeaderBottomStyled } from '@app/components/atm.header/large-header.component.style';
import { Watermark, WatermarkType } from '@app/components/atm.watermark';
import { GradientCell } from '@app/components/mol.cell/gradient-cell.component.style';
import { H1LargeWhite, HBox, VBox, VSeparator } from '@atomic';
import { LargeHeaderTopStyled } from './large-header.component.style';

interface LargeHeaderProps {
  onCheckTap: () => void;
  check: boolean;
  title: string;
  watermarkType: WatermarkType;
  type?: 'primary' | 'secondary';
}

export const LargeHeader: React.SFC<LargeHeaderProps> = (props: LargeHeaderProps) => (
  <GradientCell type={props.type}>
    <LargeHeaderTopStyled />
    <Watermark header={true} type={props.watermarkType} big={true} light={true} vAlign='flex-start' />
    <VSeparator />
    <VBox>
      <HBox>
        <HBox.Item wrap={true} vAlign='center'>
          <BigCheck type='secondary' progress={props.check ? 1 : 0} onValueChange={props.onCheckTap} />
        </HBox.Item>
        <HBox.Separator />
        <HBox.Item>
          <H1LargeWhite>{props.title}</H1LargeWhite>
        </HBox.Item>
      </HBox>
    </VBox>
    <VSeparator />
    <LargeHeaderBottomStyled />
  </GradientCell>

);
