import {
  CarePlanItemBaseResponse,
  CarePlanScheduleResponse,
} from '@app/modules/home/api/care-plan/care-plan.entity';

export interface CarePlanItemSearchResponse extends CarePlanItemBaseResponse {
  codigo: number;
  quantidadePlanejada: number;
  texto?: any;
  origem: string;
  codigoPeriodicidade?: any;
  codigoHabito?: any;
  codigoProcedimento?: any;
  codigoEspecialidade?: any;
  agendamentos: CarePlanScheduleResponse[];
  codigoOperadora?: any;
  codigoEstipulante?: any;
  codigoBeneficiario?: any;
}