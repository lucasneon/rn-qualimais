import { LocalDataSource } from '@app/data';
import { FulfillEventInput, HabitActivateInput } from '@app/data/graphql';
import { GamaDateFormatter } from '@app/model/api/common.mapper';
import {
  axiosGet,
  axiosPut,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import {
  AddItemRequest,
  AddItemScheduleRequest,
  CarePlanItemSearchResponse,
  DeleteItemRequest,
  FulflillEventRequest,
} from '@app/modules/home/api/care-plan/care-plan.entity';
import { CarePlanMapper } from '@app/modules/home/api/care-plan/care-plan.mapper';
import {
  CarePlanScheduleModel,
  FulfillEventInputModel,
} from '@app/modules/home/api/care-plan/care-plan.model';
import {
  CarePlanHabitModel,
  GenerateDatesInputModel,
  HabitDeactivateInputModel,
  HabitModel,
} from '@app/modules/home/api/habits/habit.model';
import { fetchHabits } from '@app/modules/home/api/habits/habits.service';
import moment from 'moment';
import Container from 'typedi';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchDetailsHabits = async (carePlanItemCode: string) => {
  const res = await itemDetailsRequest(carePlanItemCode);
  if (res.error) {
    throw new Error(res);
  }
  const item: CarePlanItemSearchResponse =
    res.data && res.data.beneficiarioItemDTO;
  if (!item || !item.habito) {
    throw new Error(`Care plan item ${carePlanItemCode} is not a habit.`);
  }

  return CarePlanMapper.mapHabitDetails(item);
};

const itemDetailsRequest = async (itemCode: string) => {
  let res;
  try {
    res = await axiosGet({
      url: `/fusion-gestao-cuidado/api/plano-cuidado/item/${itemCode}`,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (err) {
    return { data: null, error: err };
  }
  return res;
};

export const deleteItem = async (input: HabitDeactivateInputModel) => {
  // const req: DeleteItemRequest = { codigoPlanoCuidadoItem: input.itemCode };
  const req: DeleteItemRequest = { codigoItem: input.itemCode };
  try {
    await deleteItemRequest(req);
  } catch (error) {
    throw new Error(error);
  }
  return;
};

const deleteItemRequest = async (req: DeleteItemRequest) => {
  let res;
  try {
    res = await axiosPut({
      url: '/fusion-gestao-cuidado/api/plano-cuidado/item/excluir',
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
      data: req,
    });
  } catch (error) {
    return { data: null, error: error };
  }
  return res;
};

export const activeHabits = async (input: HabitActivateInput) => {
  const habitsOfType: HabitModel[] = await fetchHabits({
    id: input.habitTypeCode,
    area: null,
  });
  if (!habitsOfType || !habitsOfType.length) {
    throw new Error(`Habits of type ${input.habitTypeCode} not found.`);
  }
  const habit: HabitModel = habitsOfType.find(it => it.id === input.habitCode);
  if (!habit) {
    throw new Error(`Habit ${input.habitCode} not found.`);
  }
  const generateDatesInput: GenerateDatesInputModel = {
    start: moment().add(1, 'day').hour(10).minute(0).toDate(),
    end: moment().add(60, 'day').hour(10).minute(0).toDate(),
    frequencyCode: habit.frequency.id,
  };
  const dates: Date[] = await generateDates(generateDatesInput);
  const res = await addItem(input, dates, habit.frequency.id);
  return res;
  // return this.localizationService.__('app.care-plan.habit-activate.success');
};

async function generateDates(input: GenerateDatesInputModel): Promise<Date[]> {
  const res = await this.generateDatesRequest(
    CarePlanMapper.mapGenerateDatesParams(input),
  );
  if (!res.data.datasDisponiveis || !res.data.datasDisponiveis.length) {
    throw new Error(res);
  }
  return res.data.datasDisponiveis.map(date => new Date(date));
}

async function addItem(
  input: HabitActivateInput,
  dates: Date[],
  frequencyCode: string,
) {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const mapDates = (date: Date): AddItemScheduleRequest => ({
    dataPlanejamento: GamaDateFormatter.format(date),
    origem: this.interceptor.systemOrigin,
    situacao: 'PLANEJADO',
  });
  const request: AddItemRequest = {
    codigoHabito: input.habitCode,
    origem: this.interceptor.systemOrigin,
    codigoPeriodicidade: frequencyCode,
    agendamentos: dates.map(mapDates),
    quantidadePlanejada: dates.length,
    codigoOperadora: this.interceptor.opCode,
    codigoEstipulante: this.interceptor.stimulantingCode,
    codigoBeneficiario: beneficiaryCode,
    codigoEspecialidade: null,
    notificacao: false,
  };
  const res = await this.addItemRequest(request);
  if (!res.data.planoCuidadoItemDTO) {
    throw new Error(res);
  }
  return res;
}

export const execFulFillMutation = async (input: FulfillEventInput) => {
  try {
    await fulfill(input);
  } catch (error) {
    throw new Error(error);
  }
  const carePlanItem: CarePlanHabitModel = await fetchDetailsHabits(
    input.itemCode,
  );
  const schedule: CarePlanScheduleModel = carePlanItem.schedules.find(
    it => it.id === input.id,
  );

  return schedule;
};

const fulfill = async (input: FulfillEventInput) => {
  const request: FulflillEventRequest = {
    codigoAgendamento: input.id,
    codigoItem: input.itemCode,
    dataRealizada:
      input.fulfilledAt && GamaDateFormatter.format(input.fulfilledAt),
  };
  try {
    await fufillEventRequest(request);
  } catch (error) {
    throw new Error(error);
  }
  return;
};

const fufillEventRequest = async (req: FulflillEventRequest) => {
  let res;
  try {
    res = await axiosPut({
      url: '/fusion-gestao-cuidado/api/plano-cuidado/agendamento/realizar',
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
      data: req,
    });
  } catch (error) {
    return { data: null, error: error };
  }
  console.warn('Result mark calendar >', res);
  return res;
};
