export const MedicationStrings = {
  Title: 'Medicamentos',
  Sections: {
    Evolution: 'Evolução',
    Frequency: 'Frequência',
    Configurations: 'Configurações',
    Time: 'Horários',
    Instructions: 'Instruções',
    Warnings: 'Avisos',
    Start: 'Início',
    End: 'Término',
  },
  Available: {
    Title: 'Medicamentos disponíveis',
    Placeholder: {
      Title: 'Nenhum medicamento encontrado',
      Message: 'Não há medicamentos disponíveis no momento',
    },
    Active: 'ativo',
    Inactive: 'inativo',
  },
  FulfillDialog: {
    Title: 'Cumprir medicamento',
    Message: (date: string) => `Marcar medicamento como tomado em ${date}?`,
    Success: 'Medicamento marcado como tomado com sucesso!',
  },
  Deactivate: {
    Title: 'Desativar medicamento',
    Message: 'Deseja mesmo desativar o medicamento e removê-lo de suas metas?',
    Confirm: 'Sim',
  },
  AddButton: 'Adicionar novo',
  AddPage: {
    Title: 'Novo medicamento',
    Labels: {
      Name: 'medicamento',
      Start: 'início',
      Times: 'horários',
      End: 'término',
      Notification: 'receber notificação',
      Instructions: 'Instruções',
    },
    HasEnd: 'Termina em uma data',
    AddTime: 'Adicionar',
    SubmitButton: 'Adicionar medicamento',
    Notify: 'receber notificação',
  },
  Activate: {
    Title: 'Ativar medicamento',
    Message: 'Ao ativar o medicamento, ele aparecerá nas suas metas. Deseja mesmo ativar?',
    Confirm: 'Sim',
  },
  Continuous: 'uso contínuo',
};
