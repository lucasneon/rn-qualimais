import { MedicationDetailsInput } from '@app/data/graphql';
import { fetchMedicationDetails } from '@app/modules/health/api/medication/medication-details.service';

export interface MedicationDetailsContainerChildrenParams {
  medicationDetails?: any;
  loading?: boolean;
  error?: any;
  refetch?: () => void;
}

export interface MedicationDetailsContainerProps {
  medicationId: string;
  children: (
    params: MedicationDetailsContainerChildrenParams,
  ) => React.ReactNode;
}

export class MedicationDetailsContainer extends React.PureComponent<MedicationDetailsContainerProps> {
  async render() {
    const inputData: MedicationDetailsInput = {
      medicationId: this.props.medicationId,
    };

    const res = await fetchMedicationDetails(inputData);
    if (res?.error) {
      // return getChildParams();
    }
    return this.props.children(this.getChildParams(res));

    // return (
    //   <QueryContainer
    //     document="medication-details"
    //     variables={{ data: inputData }}>
    //     {(result: QueryResult<MedicationDetailsQuery>) => {
    //       return this.props.children(this.getChildParams(result));
    //     }}
    //   </QueryContainer>
    // );
  }

  private getChildParams(
    result: any,
  ): MedicationDetailsContainerChildrenParams {
    console.warn('result >', result);
    const { loading, error } = result;
    const medicationDetails = result.data;
    // const medicationDetails =
    //   result && result.data && result.data.MedicationDetails;
    return { loading, error, medicationDetails };
  }
}
