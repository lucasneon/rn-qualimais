import { ApolloError } from 'apollo-client';
import moment from 'moment';
import * as React from 'react';
import { Subscribe } from 'unstated';
import { Badge as ReadOnlyBadge } from '@app/components/atm.badge';
import { SwitchCell } from '@app/components/mol.cell/switch-cell.component';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import {
  FlashMessageService,
  Navigation,
  NavigationPageProps,
} from '@app/core/navigation';
import { MedicationInput, MedicationScheduleInput } from '@app/data/graphql';
import { MedicationsProvider } from '@app/services/providers';
import { DateUtils, ErrorMapper } from '@app/utils';
import {
  Asset,
  Badge,
  Body,
  Cell,
  CheckboxField,
  Form,
  FormSubmitData,
  HBox,
  InputLabel,
  PrimaryButton,
  Root,
  TextField,
  Validators,
  VBox,
  VSeparator,
} from '@atomic';
import { LinkButtonTextStyled } from '@atomic/atm.button/link-button.component.style';
import { PickerWrapper } from '@atomic/atm.picker/picker-wrapper.component';
import { MedicationStrings } from './medication.strings';

const Fields = {
  name: 'name',
  instructions: 'instructions',
  dailySchedule: 'dailySchedule',
};

interface MedicationAddPageState {
  showEndDatePicker: boolean;
  selectedTimes: MedicationScheduleInput[];
}

const Strings = MedicationStrings.AddPage;

@PageView('MedicationAdd')
export class MedicationAddPage extends React.Component<
  NavigationPageProps<undefined>,
  MedicationAddPageState
> {
  static options = { name: 'MedicationAddPage' };

  private medicationsProvider: MedicationsProvider;

  private startDate: Date;
  private endDate: Date;
  private notify: boolean;

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      showEndDatePicker: false,
      selectedTimes: [],
    };
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  render() {
    return (
      <Root>
        <CollapsibleHeader title={Strings.Title}>
          <VBox>
            <Subscribe to={[MedicationsProvider]}>
              {(medicationsProvider: MedicationsProvider) => {
                this.medicationsProvider = medicationsProvider;
                return (
                  <Form onSubmit={this.handleSubmit}>
                    <Form.Scroll>
                      <VSeparator />
                      <Form.Field
                        label={Strings.Labels.Name}
                        validators={[Validators.Required()]}
                        name={Fields.name}
                        value="">
                        <TextField
                          autoCapitalize="none"
                          autoCorrect={false}
                          type={TextField.Type.Normal}
                        />
                      </Form.Field>
                      <VSeparator />
                      <PickerWrapper
                        mode="date"
                        minDateTime={new Date(moment().format())}>
                        {date => {
                          this.startDate = date;
                          return (
                            <>
                              <Cell
                                leftGutter={false}
                                rightGutter={false}
                                leftThumb={Asset.Icon.Custom.Calendar}>
                                <HBox>
                                  <HBox.Item>
                                    <Body>{Strings.Labels.Start}</Body>
                                  </HBox.Item>
                                  <HBox.Item vAlign="center" hAlign="flex-end">
                                    <LinkButtonTextStyled>
                                      {DateUtils.Format.presentation(date)}
                                    </LinkButtonTextStyled>
                                  </HBox.Item>
                                </HBox>
                              </Cell>
                            </>
                          );
                        }}
                      </PickerWrapper>
                      <VSeparator />
                      <InputLabel>{Strings.Labels.Times}</InputLabel>
                      <VSeparator />
                      <HBox wrap={true}>
                        {this.state.selectedTimes.map(schedule => (
                          <Badge
                            type="neutral"
                            key={schedule.hour}
                            id={schedule.hour}
                            text={schedule.hour}
                            onDismiss={this.handleTimeDismiss}
                          />
                        ))}
                        <PickerWrapper
                          onValueChange={this.handleTimeSelected}
                          mode="time">
                          {_ => (
                            <ReadOnlyBadge
                              outlined={true}
                              type="primary"
                              text={Strings.AddTime}
                            />
                          )}
                        </PickerWrapper>
                      </HBox>
                      <VSeparator />
                      <SwitchCell
                        text={Strings.HasEnd}
                        onChange={this.handleCheckTap}
                      />
                      <VSeparator />
                      {this.state.showEndDatePicker && (
                        <PickerWrapper mode="date">
                          {date => {
                            this.endDate = date;
                            return (
                              <>
                                <Cell
                                  leftGutter={false}
                                  rightGutter={false}
                                  leftThumb={Asset.Icon.Custom.Calendar}>
                                  <HBox>
                                    <HBox.Item>
                                      <Body>{Strings.Labels.End}</Body>
                                    </HBox.Item>
                                    <HBox.Item hAlign="flex-end">
                                      <LinkButtonTextStyled>
                                        {DateUtils.Format.presentation(date)}
                                      </LinkButtonTextStyled>
                                    </HBox.Item>
                                  </HBox>
                                </Cell>
                                <VSeparator />
                              </>
                            );
                          }}
                        </PickerWrapper>
                      )}
                      <Form.Field
                        label={Strings.Labels.Instructions}
                        name={Fields.instructions}>
                        <TextField
                          multiline={true}
                          autoCapitalize="none"
                          type={TextField.Type.Normal}
                        />
                      </Form.Field>
                      <VSeparator />
                      <SwitchCell
                        text={Strings.Notify}
                        onChange={this.handleSwitchChange}
                      />
                      <VSeparator />
                      <Form.Submit>
                        <PrimaryButton
                          text={Strings.SubmitButton}
                          expanded={true}
                          loading={this.medicationsProvider.state.loading}
                          submit={true}
                        />
                      </Form.Submit>
                      <VSeparator />
                    </Form.Scroll>
                  </Form>
                );
              }}
            </Subscribe>
          </VBox>
        </CollapsibleHeader>
      </Root>
    );
  }

  private handleSubmit = (formData: FormSubmitData) => {
    if (Object.keys(formData.error).length !== 0) {
      return;
    }

    const data: MedicationInput = {
      dailySchedule: this.state.selectedTimes.map(schedule => ({
        hour: moment(schedule.hour, 'HH:mm').format('HH:mm:ss.sssZ'),
      })),
      startDate: DateUtils.Format.datasource(this.startDate),
      endDate:
        (this.state.showEndDatePicker &&
          this.endDate &&
          DateUtils.Format.datasource(this.endDate)) ||
        null,
      orientation: formData.data.instructions,
      medicine: { name: formData.data.name },
      notify: this.notify,
    };

    this.medicationsProvider.create(
      data,
      null,
      () => Navigation.dismissModal(this.props.componentId),
      (error: ApolloError) =>
        FlashMessageService.showError(ErrorMapper.map(error)),
    );
  };

  private handleSwitchChange = (active: boolean) => {
    this.notify = active;
  };

  private handleCheckTap = (checked: boolean) => {
    this.setState({ showEndDatePicker: checked });
  };

  private handleTimeDismiss = (time: string) => {
    this.setState({
      selectedTimes: this.state.selectedTimes.filter(
        schedule => schedule.hour !== time,
      ),
    });
  };

  private handleTimeSelected = (time: Date) => {
    const hour = moment(time.toUTCString()).format('HH:mm');
    if (!this.state.selectedTimes.find(schedule => schedule.hour === hour)) {
      this.setState({ selectedTimes: [...this.state.selectedTimes, { hour }] });
    }
  };
}
