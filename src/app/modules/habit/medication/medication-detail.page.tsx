import moment from 'moment';
import * as React from 'react';
import { Animated } from 'react-native';
// import { DateObject } from 'react-native-calendars';
import { LargeHeaderHeight } from '@app/components/atm.header/large-header.component.style';
import { WatermarkType } from '@app/components/atm.watermark';
import { ProgressCalendarShimmer } from '@app/components/mol.calendar/progress-calendar-day.component';
// import { ProgressCalendar } from '@app/components/mol.calendar/progress-calendar.component';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { DateUtils } from '@app/utils';
import {
  Asset,
  Body,
  Cell,
  CollapsibleHeader,
  Color,
  FontSize,
  H2,
  H3,
  HBox,
  LoadingState,
  Scroll,
  Shimmer,
  VBox,
  VSeparator,
} from '@atomic';
import { LargeHeader } from '../large-header.component';
import { MedicationStrings } from './medication.strings';
import { MedicationDetailsProvider } from '@app/services/providers/medication-details.provider';
import { Subscribe } from 'unstated';

const AnimatedComponent = Animated.createAnimatedComponent(Scroll);

export interface MedicationDetailsPageProps {
  medicationId: string;
  medicationName: string;
  onFulfillSuccess?: () => void;
}

interface MedicationDetailPageState {
  fulfillLoading?: boolean;
  currentSelected: string;
  activationLoading?: boolean;
}

@PageView('MedicationDetails')
export class MedicationDetailsPage extends React.Component<
  NavigationPageProps<MedicationDetailsPageProps>,
  MedicationDetailPageState
> {
  static options = {
    name: 'MedicationDetailPage',
    topBar: { backButton: { color: Color.White } },
  };

  private medicationsDetailProvider: MedicationDetailsProvider;

  // private readonly graphQLProvider: GraphQLProvider = Container.get(GraphQLProviderToken);
  // private readonly medicationsProvider: MedicationsProvider = Container.get(MedicationsProviderToken);

  private belowHeader = false;

  constructor(props) {
    super(props);
    this.state = {
      fulfillLoading: false,
      currentSelected: this.calendarLibFormat(new Date()),
      activationLoading: null,
    };
  }

  componentDidMount() {
    this.medicationsDetailProvider.rehydrate(
      this.props.navigation.medicationId,
    );
  }

  render() {
    return (
      <Subscribe to={[MedicationDetailsProvider]}>
        {(medicationsDetailsProvider: MedicationDetailsProvider) => {
          this.medicationsDetailProvider = medicationsDetailsProvider;
          const state = medicationsDetailsProvider.state;
          const loading = state.loading;
          const error = state.error;
          const medicationsDetails = state.medicationDetails;

          return (
            <CollapsibleHeader onScroll={this.handleScroll} topBar={true}>
              <CollapsibleHeader.LargeHeader>
                <LargeHeader
                  title={this.props.navigation.medicationName}
                  // onCheckTap={this.handleMedicationCheckTap}
                  onCheckTap={null}
                  watermarkType={WatermarkType.Medication}
                  check={false}
                />
              </CollapsibleHeader.LargeHeader>
              <AnimatedComponent>
                <LoadingState
                  loading={loading}
                  data={!loading && !!medicationsDetails}
                  error={!!error}>
                  <LoadingState.Shimmer>
                    <VBox>
                      <ProgressCalendarShimmer />
                      <VSeparator />
                      <Shimmer height={FontSize.Medium} width="90%" />
                      <VSeparator />
                      <Shimmer height={FontSize.Medium} width="80%" />
                      <VSeparator />
                      <Shimmer height={FontSize.Medium} width="85%" />
                    </VBox>
                  </LoadingState.Shimmer>
                  <LoadingState.ErrorPlaceholder>
                    <PlaceholderSelector error={error} />
                  </LoadingState.ErrorPlaceholder>
                  {medicationsDetails && (
                    <>
                      {/* <H2>{MedicationStrings.Sections.Evolution}</H2>
                    <VBox>
                      <ProgressCalendar
                        loading={this.state.fulfillLoading}
                        markedDates={{}}
                        // markedDates={this.mapMedicationToMarkings(medicationDetails)}
                        onTap={this.handleDayPress}
                        selected={this.state.currentSelected}
                      />
                    </VBox> */}
                      <H2>{MedicationStrings.Sections.Configurations}</H2>
                      <Cell>
                        <HBox>
                          <HBox.Item>
                            <H3>{MedicationStrings.Sections.Start}</H3>
                          </HBox.Item>
                          <HBox.Item hAlign="flex-end">
                            <Body>
                              {medicationsDetails.startDate &&
                                DateUtils.Format.presentation(
                                  medicationsDetails.startDate,
                                )}
                            </Body>
                          </HBox.Item>
                        </HBox>
                      </Cell>
                      <Cell>
                        <HBox>
                          <HBox.Item>
                            <H3>{MedicationStrings.Sections.End}</H3>
                          </HBox.Item>
                          <HBox.Item hAlign="flex-end">
                            <Body>
                              {(medicationsDetails.endDate &&
                                DateUtils.Format.presentation(
                                  medicationsDetails.endDate,
                                )) ||
                                MedicationStrings.Continuous}
                            </Body>
                          </HBox.Item>
                        </HBox>
                      </Cell>
                      {medicationsDetails.frequency && (
                        <Cell>
                          <HBox>
                            <HBox.Item>
                              <H3>{MedicationStrings.Sections.Frequency}</H3>
                            </HBox.Item>
                            <HBox.Item hAlign="flex-end">
                              <Body>{medicationsDetails.frequency}</Body>
                            </HBox.Item>
                          </HBox>
                        </Cell>
                      )}
                      <VSeparator />
                      {medicationsDetails.orientation && (
                        <VBox>
                          <H2>{MedicationStrings.Sections.Instructions}</H2>
                          <Body>{medicationsDetails.orientation}</Body>
                          <VSeparator />
                        </VBox>
                      )}
                      {/* {medicationDetails.active && (
                      <VBox>
                        <SecondaryButton
                          loading={this.state.activationLoading}
                          negative={true}
                          expanded={true}
                          onTap={this.handleMedicationRemove}
                          text={MedicationStrings.Deactivate.Title}
                        />
                        <VSeparator />
                      </VBox>
                    )} */}
                    </>
                  )}
                </LoadingState>
              </AnimatedComponent>
            </CollapsibleHeader>
          );
        }}
      </Subscribe>
    );
  }

  ////////////////////////// Comentas /////////////////////////////////

  // private handleMedicationRemove = () => {
  //   const deactivateMedication = () => {
  //     this.setState({ activationLoading: true });

  //     this.graphQLProvider.mutate<DeactivateMedicationMutationVariables >(
  //       'deactivate-medication',
  //       {
  //         data: {
  //           carePlanCode: this.campaign.carePlanCode,
  //           itemCode: this.details.carePlanItem.itemCode
  //         }
  //       },
  //       this.handleActivationSuccess,
  //       this.handleActivationError,
  //     );
  //   };

  //   new AlertBuilder().withTitle(MedicationStrings.Deactivate.Title)
  //     .withMessage(MedicationStrings.Deactivate.Message)
  //     .withButton({ text: GeneralStrings.Cancel })
  //     .withButton({ text: MedicationStrings.Deactivate.Confirm, onPress: deactivateMedication })
  //     .show();
  // }

  // private handleActivateMedication = () => {
  //   const activateMedication = () => {
  // this.setState({ activationLoading: true });

  //   const input: MedicationActivateInput = {
  //     carePlanCode: this.medicationsProvider.carePlanCode,
  //     medicationCode: this.details.id,
  //     medicationTypeCode: this.details.type.id,
  //   };

  //   this.graphQLProvider.mutate<ActivateMedicationMutationVariables>(
  //     'activate-medication',
  //     { data: input },
  //     this.handleActivationSuccess,
  //     this.handleActivationError,
  //   );
  // };

  //   new AlertBuilder().withTitle(MedicationStrings.Activate.Title)
  //     .withMessage(MedicationStrings.Activate.Message)
  //     .withButton({ text: GeneralStrings.Cancel })
  //     .withButton({ text: MedicationStrings.Activate.Confirm, onPress: activateMedication })
  //     .show();
  // }

  // private handleActivationSuccess = () => {
  //   this.setState({ activationLoading: false });
  //   this.medicationsProvider.rehydrate();
  //   Navigation.pop(this.props.componentId);
  // }

  // private handleActivationError = (error: ApolloError) => {
  //   this.setState({ activationLoading: false });
  //   FlashMessageService.showError(ErrorMapper.map(error));
  // }

  private handleScroll = (event: any) => {
    const scrollPosition = event.nativeEvent.contentOffset.y;

    if (scrollPosition > LargeHeaderHeight && !this.belowHeader) {
      this.belowHeader = true;
      this.changeBackButtonColor(Color.Primary);
    } else if (scrollPosition < LargeHeaderHeight && this.belowHeader) {
      this.changeBackButtonColor(Color.White);
      this.belowHeader = false;
    }
  };

  private changeBackButtonColor = (newColor: string) => {
    Navigation.mergeOptions(this.props.componentId, {
      topBar: {
        leftButtonColor: newColor,
        backButton: {
          icon: Asset.Icon.NavBar.Back,
          showTitle: false,
          color: newColor,
          visible: true,
        },
      },
    });
  };

  // private mapDates = (schedules: CarePlanScheduleFragment[]) => {
  //   if (schedules) {
  //     schedules.forEach(schedule => this.dateToSchedules[this.calendarLibFormat(schedule.limitDate)] = schedule);
  //   }
  // }

  // private handleDayPress = (date: DateObject) => {
  //   // this.currentSchedule = this.dateToSchedules[date.dateString];

  //   this.setState({ currentSelected: date.dateString, });

  //   // this.handleMedicationCheckTap();
  // }

  // private handleMedicationCheckTap = () => {
  //   if (this.currentSchedule && !this.currentSchedule.doneAt) {
  //     new AlertBuilder().withTitle(MedicationStrings.FulfillDialog.Title)
  //       .withMessage(MedicationStrings.FulfillDialog
  //         .Message(DateUtils.Format.presentationFormat(this.currentSchedule.limitDate)))
  //       .withButton({ text: GeneralStrings.Cancel })
  //       .withButton({ text: GeneralStrings.Yes, onPress: this.handleFulfillDialogConfirmation })
  //       .show();
  //   }
  // }

  // private handleFulfillDialogConfirmation = () => {
  //   const input: FulfillEventInput = {
  //     id: this.currentSchedule.id,
  //     carePlanCode: this.details.carePlanItem.carePlanCode,
  //     itemCode: this.details.carePlanItem.itemCode,
  //     fulfilledAt: DateUtils.Format.datasourceFormat(new Date(), true),
  //   };

  //   this.setState({ fulfillLoading: true });

  //   this.graphQLProvider.mutate<FulfillMedicationMutationVariables>(
  //     'fulfill-medication',
  //     { data: input },
  //     this.handleFulfillSuccess,
  //     this.handleFulfillError,
  //   );
  // }

  // private handleFulfillSuccess = (res: FulfillMedicationMutation) => {
  //   const updatedSchedule = res.FulfillMedication;
  //   const updatedScheduleIndex = this.schedules.findIndex(schedule => schedule.id === updatedSchedule.id);

  //   this.schedules[updatedScheduleIndex] = updatedSchedule;

  //   this.setState({ fulfillLoading: false, });

  //   if (this.props.navigation.onFulfillSuccess) {
  //     this.props.navigation.onFulfillSuccess();
  //   }

  //   FlashMessageService.showSuccess(MedicationStrings.FulfillDialog.Success);
  // }

  // private handleFulfillError = (error: ApolloError) => {
  //   this.setState({ fulfillLoading: false });
  //   FlashMessageService.showError(error);
  // }

  private calendarLibFormat(date: string | Date): string {
    return moment(date).format('YYYY-MM-DD');
  }

  // TODO Calculate progress based on multiple times in a day
  // private getProgress = (dateString: string) => {
  //   return Number(!!this.dateToSchedules[dateString] && !!this.dateToSchedules[dateString].doneAt);
  // }

  // private setPrivateVariables = (medicationDetails: MedicationFragment) => {
  //   this.details = medicationDetails;

  //   if (!this.schedules) {
  //     this.schedules = this.details && this.details.carePlanItem && _.cloneDeep(this.details.carePlanItem.schedules);
  //   }

  //   this.mapDates(this.schedules);

  //   this.currentSchedule = this.dateToSchedules[this.state.currentSelected];
  // }

  // private mapMedicationToMarkings = (medication: MedicationFragment) => {
  //   const markings = {};
  //   if (medication && medication.dailySchedule) {
  //     medication.dailySchedule.forEach(schedule => {
  //       const dateString = this.calendarLibFormat(medication.startDate);
  //       markings[dateString] = { progress: this.getProgress(dateString) };
  //     });
  //   }
  //   return markings;
  // }
}
