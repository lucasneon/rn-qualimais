import moment from 'moment';
import * as React from 'react';
import { FlatList, ListRenderItemInfo } from 'react-native';
import { Subscribe } from 'unstated';
import { WatermarkType } from '@app/components/atm.watermark';
import { ButtonFooter } from '@app/components/mol.button-footer';
import {
  HabitCell,
  HabitCellProps,
  HabitCellShimmer,
} from '@app/components/mol.cell';
import {
  Placeholder,
  PlaceholderSelector,
} from '@app/components/mol.placeholder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { MedicationsStrings } from '@app/components/org.medications/medications.strings';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import {
  MedicationFragment,
  MedicationListItemFragment,
} from '@app/data/graphql';
import { AppError } from '@app/model';
import { CarePlanPlaceholders } from '@app/modules/home/care-plan-placeholders.component';
import { CarePlanProvider, MedicationsProvider } from '@app/services/providers';
import { Asset, LoadingState, VSeparator } from '@atomic';
import { MedicationAddPage } from './medication-add.page';
import {
  MedicationDetailsPage,
  MedicationDetailsPageProps,
} from './medication-detail.page';
import { MedicationStrings } from './medication.strings';

const EmptyPlaceholder = () => (
  <Placeholder
    image={Asset.Icon.Placeholder.Failure}
    title={MedicationStrings.Available.Placeholder.Title}
    message={MedicationStrings.Available.Placeholder.Message}
  />
);

@PageView('MedicationList')
export class MedicationListPage extends React.Component<
  NavigationPageProps<undefined>,
  undefined
> {
  static options = { name: 'MedicationListPage' };

  private medicationProvider: MedicationsProvider;

  componentDidMount() {
    const state = this.medicationProvider.state;

    this.medicationProvider.rehydrate();

    // if (!state.allMedications && !state.loading) {
    //   this.medicationProvider.rehydrate();
    // }
  }

  render() {
    return (
      <Subscribe to={[CarePlanProvider, MedicationsProvider]}>
        {(
          carePlanProvider: CarePlanProvider,
          medicationsProvider: MedicationsProvider,
        ) => {
          this.medicationProvider = medicationsProvider;
          console.warn(carePlanProvider.state.carePlan);

          const carePlan = carePlanProvider.state.carePlan;
          const hasCarePlan: boolean = carePlan && !!carePlan.code;

          // const loading: boolean =
          //   carePlanProvider.state.loading || medicationsProvider.state.loading;

          const loading: boolean = carePlanProvider.state.loading;

          const error: AppError =
            carePlanProvider.state.error || medicationsProvider.state.error;
          const medications: MedicationListItemFragment[] =
            medicationsProvider.state.allMedications;
          console.warn(medicationsProvider.state.loading);

          const onTapError: () => void = hasCarePlan
            ? medicationsProvider.rehydrate
            : carePlanProvider.rehydrate;

          return (
            <>
              <CollapsibleHeader title={MedicationStrings.Title}>
                <LoadingState
                  loading={loading}
                  error={!!error}
                  data={medications && hasCarePlan}>
                  <LoadingState.Shimmer>
                    <HabitCellShimmer count={5} />
                  </LoadingState.Shimmer>
                  <LoadingState.ErrorPlaceholder>
                    <PlaceholderSelector error={error} onTap={onTapError} />
                  </LoadingState.ErrorPlaceholder>
                  {hasCarePlan && medications && (
                    <FlatList
                      ListEmptyComponent={EmptyPlaceholder}
                      data={medications}
                      renderItem={this.renderItem}
                      keyExtractor={this.keyExtractor}
                      ListFooterComponent={<VSeparator />}
                    />
                  )}
                </LoadingState>
                {carePlan && carePlan.status && (
                  <CarePlanPlaceholders
                    status={carePlan.status}
                    onTap={this.handleCarePlanPlaceholderTap}
                  />
                )}
              </CollapsibleHeader>
              {hasCarePlan && (
                <ButtonFooter
                  primary={{
                    text: MedicationStrings.AddButton,
                    onTap: this.handleAddTap,
                  }}
                />
              )}
            </>
          );
        }}
      </Subscribe>
    );
  }

  private renderItem = (
    itemInfo: ListRenderItemInfo<MedicationFragment>,
  ): React.ReactElement<HabitCellProps> => {
    const item: MedicationFragment = itemInfo.item;

    return (
      <HabitCell
        title={item.medicine.name}
        id={item.id}
        type={WatermarkType.Medication}
        tag={MedicationsStrings.Tag}
        inactive={!item.active}
        status={
          item.active
            ? MedicationStrings.Available.Active
            : MedicationStrings.Available.Inactive
        }
        onTap={this.handleItemTap(item)}
        times={item.dailySchedule.map(schedule =>
          moment(schedule.hour, 'HH:mm:ss.sssZ').format('HH:mm'),
        )}
        hideCheck={true}
      />
    );
  };

  private keyExtractor = (item: MedicationListItemFragment) => item.id;

  private handleItemTap = (item: MedicationListItemFragment) => {
    return () => {
      Navigation.push<MedicationDetailsPageProps>(
        this.props.componentId,
        MedicationDetailsPage,
        {
          medicationId: item.id,
          medicationName: item.medicine.name,
        },
      );
    };
  };

  private handleCarePlanPlaceholderTap = () => {
    Navigation.popToRoot(this.props.componentId);
  };

  private handleAddTap = () => {
    Navigation.showModal(MedicationAddPage);
  };
}
