import styled from 'styled-components/native';
import { Platform } from '@atomic';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? Platform.getStatusBarHeight() : 0;

export const LargeHeaderTopStyled = styled.View`
  margin-top: ${STATUSBAR_HEIGHT + 4};
  height: 36;
`;
