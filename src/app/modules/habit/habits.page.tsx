import * as React from 'react';
import { FlatList, ListRenderItemInfo } from 'react-native';
import { Subscribe } from 'unstated';
import { WatermarkType } from '@app/components/atm.watermark';
import { HabitCell, HabitCellProps, HabitCellShimmer } from '@app/components/mol.cell';
import { Placeholder, PlaceholderSelector } from '@app/components/mol.placeholder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { HabitArea, HabitFragment } from '@app/data/graphql';
import { HabitsProvider } from '@app/services/providers';
import { Asset, LoadingState, VSeparator } from '@atomic';
import { HabitDetailPage, HabitDetailsPageProps } from './habit-detail.page';
import { HabitStrings } from './habit.strings';

export const HabitsWatermarkMap: { [key in HabitArea]?: WatermarkType } = {
  [HabitArea.Alcoholism]: WatermarkType.Drinking,
  [HabitArea.Eating]: WatermarkType.Food,
  [HabitArea.PhysicalActivity]: WatermarkType.Exercise,
  [HabitArea.Smoking]: WatermarkType.Smoking,
  [HabitArea.Unknown]: WatermarkType.Consultation,
  [HabitArea.WellBeing]: WatermarkType.Body,
};

const EmptyPlaceholder = () => (
  <Placeholder
    image={Asset.Icon.Placeholder.Failure}
    title={HabitStrings.AvailableHabits.Placeholder.Title}
    message={HabitStrings.AvailableHabits.Placeholder.Message}
  />
);

@PageView('Habits')
export class HabitsPage extends React.Component<NavigationPageProps<undefined>, undefined> {
  static options = { name: 'HabitsPage' };

  private habitsProvider: HabitsProvider;

  render() {
    return (
      <CollapsibleHeader title={HabitStrings.AvailableHabits.Title}>
        <Subscribe to={[HabitsProvider]}>
          {(habitsProvider: HabitsProvider) => {
            this.habitsProvider = habitsProvider;
            const state = habitsProvider.state;

            return (
              <LoadingState loading={state.loading} data={!!state.allHabits} error={!!state.error}>
                <LoadingState.Shimmer>
                  <HabitCellShimmer count={5} />
                </LoadingState.Shimmer>
                <LoadingState.ErrorPlaceholder>
                  <PlaceholderSelector error={state.error} />
                </LoadingState.ErrorPlaceholder>
                {state.allHabits &&
                  <FlatList
                    ListEmptyComponent={EmptyPlaceholder}
                    data={state.allHabits}
                    renderItem={this.renderItem}
                    keyExtractor={this.keyExtractor}
                    ListFooterComponent={<VSeparator />}
                  />
                }
              </LoadingState>
            );
          }}
        </Subscribe>
      </CollapsibleHeader>
    );
  }

  private renderItem = (itemInfo: ListRenderItemInfo<HabitFragment>): React.ReactElement<HabitCellProps> => {
    const item: HabitFragment = itemInfo.item;

    return (
      <HabitCell
        title={item.description}
        id={item.id}
        type={HabitsWatermarkMap[item.type.area]}
        tag={item.frequency.description}
        inactive={!item.carePlanItem}
        status={item.carePlanItem ? HabitStrings.AvailableHabits.Active : HabitStrings.AvailableHabits.Inactive}
        onTap={this.handleItemTap(item)}
        disabled={!!item.carePlanItem}
      />
    );
  }

  private keyExtractor = (item: HabitFragment) => item.id;

  private handleItemTap = (item: HabitFragment) => {
    return () => {
      Navigation.push<HabitDetailsPageProps>(
        this.props.componentId,
        HabitDetailPage,
        {
          habitCode: item.id,
          habitName: item.type.display,
          onFulfillSuccess: this.habitsProvider.rehydrate,
          area: item.type.area,
        },
      );
    };
  }
}
