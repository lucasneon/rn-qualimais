import { ApolloError } from 'apollo-client';
import _ from 'lodash';
import moment from 'moment';
import * as React from 'react';
import { Animated, StyleSheet, Text, View } from 'react-native';
import { DateObject } from 'react-native-calendars';
import { Container } from 'typedi';
import { AlertBuilder } from '@app/components/atm.alert-builder';
import { LargeHeaderHeight } from '@app/components/atm.header/large-header.component.style';
import { ProgressCalendarShimmer } from '@app/components/mol.calendar/progress-calendar-day.component';
import {
  CustomCalendarMarking,
  ProgressCalendar,
} from '@app/components/mol.calendar/progress-calendar.component';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { PageView } from '@app/core/analytics';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import {
  CarePlanScheduleFragment,
  FulfillEventInput,
  FulfillHabitMutationVariables,
  HabitActivateInput,
  HabitArea,
  HabitDetailsInput,
  HabitFragment,
} from '@app/data/graphql';
import { HabitsProvider, HabitsProviderToken } from '@app/services/providers';
import { DateUtils, ErrorMapper } from '@app/utils';
import {
  Asset,
  Body,
  Cell,
  CollapsibleHeader,
  Color,
  FontSize,
  H2,
  HBox,
  LoadingState,
  Scroll,
  SecondaryButton,
  Shimmer,
  VBox,
  VSeparator,
} from '@atomic';
import { GeneralStrings } from '../common';
import {
  HabitDetailsContainer,
  HabitDetailsContainerChildrenParams,
} from './habit-details.container';
import { HabitStrings } from './habit.strings';
import { HabitsWatermarkMap } from './habits.page';
import { LargeHeader } from './large-header.component';
import {
  activeHabits,
  deleteItem,
  execFulFillMutation,
} from './api/habits/habits-detail.service';
import { CarePlanScheduleModel } from '../home/api/care-plan/care-plan.model';

const AnimatedComponent = Animated.createAnimatedComponent(Scroll);

export interface HabitDetailsPageProps {
  habitCode: string;
  carePlanItemCode?: string;
  habitName: string;
  onFulfillSuccess?: () => void;
  area?: HabitArea;
}

interface HabitDetailPageState {
  fulfillLoading?: boolean;
  currentSelected: string;
  activationLoading?: boolean;
}

@PageView('HabitDetail')
export class HabitDetailPage extends React.Component<
  NavigationPageProps<HabitDetailsPageProps>,
  HabitDetailPageState
> {
  static options = {
    name: 'HabitDetailPage',
    topBar: { backButton: { color: Color.White } },
  };

  private readonly graphQLProvider: GraphQLProvider =
    Container.get(GraphQLProviderToken);
  private readonly habitsProvider: HabitsProvider =
    Container.get(HabitsProviderToken);

  private details: HabitFragment;
  private belowHeader = false;
  private dateToSchedules: { [key: string]: CarePlanScheduleFragment } = {};
  private currentSchedule: CarePlanScheduleFragment;
  private schedules: CarePlanScheduleFragment[];

  constructor(props) {
    super(props);
    this.state = {
      fulfillLoading: false,
      currentSelected: this.calendarLibFormat(new Date()),
      activationLoading: null,
    };
  }

  render() {
    const habitDetailsInput: HabitDetailsInput = {
      habitCode: this.props.navigation.habitCode,
      carePlanItemCode: this.props.navigation.carePlanItemCode,
    };

    return (
      <HabitDetailsContainer habitDetailsInput={habitDetailsInput}>
        {(res: HabitDetailsContainerChildrenParams) => {
          const { loading, error, habitDetails } = res;

          this.setData(habitDetails);

          const habitChecked =
            !!this.currentSchedule && !!this.currentSchedule.doneAt;
          const area =
            this.props.navigation.area ||
            (habitDetails.type && habitDetails.type.area);

          return (
            <CollapsibleHeader onScroll={this.handleScroll} topBar={true}>
              <CollapsibleHeader.LargeHeader>
                <LargeHeader
                  title={this.props.navigation.habitName}
                  onCheckTap={this.handleHabitCheckTap}
                  check={habitChecked}
                  watermarkType={
                    HabitsWatermarkMap[area] || HabitsWatermarkMap.Unknown
                  }
                  type="secondary"
                />
              </CollapsibleHeader.LargeHeader>
              <AnimatedComponent>
                <LoadingState
                  loading={loading}
                  data={!loading && !!this.details}
                  error={!!error}>
                  <LoadingState.Shimmer>
                    <VBox>
                      <ProgressCalendarShimmer />
                      <VSeparator />
                      <Shimmer height={FontSize.Medium} width="90%" />
                      <VSeparator />
                      <Shimmer height={FontSize.Medium} width="80%" />
                      <VSeparator />
                      <Shimmer height={FontSize.Medium} width="85%" />
                    </VBox>
                  </LoadingState.Shimmer>
                  <LoadingState.ErrorPlaceholder>
                    <PlaceholderSelector error={error} />
                  </LoadingState.ErrorPlaceholder>
                  {habitDetails && (
                    <>
                      <H2>{HabitStrings.Sections.Evolution}</H2>
                      <VBox>
                        <ProgressCalendar
                          loading={this.state.fulfillLoading}
                          markedDates={this.mapDateToMarkings()}
                          onTap={this.handleDayPress}
                          selected={this.state.currentSelected}
                        />
                      </VBox>
                      <VBox>
                        <VSeparator />
                      </VBox>
                      <VBox>
                        {habitDetails.warning && (
                          <>
                            <VBox>
                              <View style={styles.subTitle}>
                                <Body>{this.details.description}</Body>
                                <Body>{this.details.warning}</Body>
                              </View>
                              <VSeparator />
                            </VBox>
                          </>
                        )}
                        {habitDetails.observation && (
                          <>
                            <VBox>
                              <H2>{HabitStrings.Sections.Observations}</H2>
                              <Body>{this.details.observation}</Body>
                            </VBox>
                            <VSeparator />
                          </>
                        )}
                        <H2>{HabitStrings.Sections.Configurations}</H2>
                      </VBox>
                      <Cell>
                        <HBox>
                          <View style={styles.frequencyBox}>
                            <Text
                              style={{
                                fontSize: 16,
                                color: '#404040',
                                fontFamily: 'Helvetica',
                              }}>
                              {HabitStrings.Sections.Frequency}
                            </Text>
                            <Text>{this.details.frequency.description}</Text>
                          </View>
                        </HBox>
                      </Cell>
                      <VSeparator />
                      <VBox>
                        {habitDetails.carePlanItem ? (
                          <SecondaryButton
                            loading={this.state.activationLoading}
                            negative={true}
                            expanded={true}
                            onTap={this.handleHabitRemove}
                            text={HabitStrings.Deactivate.Title}
                          />
                        ) : (
                          <SecondaryButton
                            loading={this.state.activationLoading}
                            expanded={true}
                            onTap={this.handleActivateHabit}
                            text={HabitStrings.Activate.Title}
                          />
                        )}
                        <VSeparator />
                      </VBox>
                    </>
                  )}
                </LoadingState>
              </AnimatedComponent>
            </CollapsibleHeader>
          );
        }}
      </HabitDetailsContainer>
    );
  }

  private handleHabitRemove = async () => {
    const deactivateHabit = () => {
      this.setState({ activationLoading: true });
      deleteItem({ itemCode: this.details.carePlanItem.itemCode })
        .then(() => this.handleActivationSuccess())
        .catch(error => this.handleActivationError(error));
      // this.graphQLProvider.mutate<DeactivateHabitMutationVariables>(
      //   'deactivate-habit',
      //   { data: { itemCode: this.details.carePlanItem.itemCode } },
      //   this.handleActivationSuccess,
      //   this.handleActivationError,
      // );
    };

    new AlertBuilder()
      .withTitle(HabitStrings.Deactivate.Title)
      .withMessage(HabitStrings.Deactivate.Message)
      .withButton({ text: GeneralStrings.Cancel })
      .withButton({
        text: HabitStrings.Deactivate.Confirm,
        onPress: deactivateHabit,
      })
      .show();
  };

  private handleActivateHabit = () => {
    const activateHabit = () => {
      this.setState({ activationLoading: true });

      const input: HabitActivateInput = {
        habitCode: this.details.id,
        habitTypeCode: this.details.type.id,
      };
      activeHabits(input)
        .then(() => this.handleActivationSuccess())
        .catch(error => this.handleActivationError(error));
      // this.graphQLProvider.mutate<ActivateHabitMutationVariables>(
      //   'activate-habit',
      //   { data: input },
      //   this.handleActivationSuccess,
      //   this.handleActivationError,
      // );
    };

    new AlertBuilder()
      .withTitle(HabitStrings.Activate.Title)
      .withMessage(HabitStrings.Activate.Message)
      .withButton({ text: GeneralStrings.Cancel })
      .withButton({
        text: HabitStrings.Activate.Confirm,
        onPress: activateHabit,
      })
      .show();
  };

  private handleActivationSuccess = () => {
    this.setState({ activationLoading: false });
    this.habitsProvider.rehydrate();
    Navigation.pop(this.props.componentId);
  };

  private handleActivationError = (error: ApolloError) => {
    this.setState({ activationLoading: false });
    FlashMessageService.showError(ErrorMapper.map(error));
  };

  private handleScroll = (event: any) => {
    const scrollPosition = event.nativeEvent.contentOffset.y;

    if (scrollPosition > LargeHeaderHeight && !this.belowHeader) {
      this.belowHeader = true;
      this.changeBackButtonColor(Color.Primary);
    } else if (scrollPosition < LargeHeaderHeight && this.belowHeader) {
      this.changeBackButtonColor(Color.White);
      this.belowHeader = false;
    }
  };

  private changeBackButtonColor = (newColor: string) => {
    Navigation.mergeOptions(this.props.componentId, {
      topBar: {
        leftButtonColor: newColor,
        backButton: {
          icon: Asset.Icon.NavBar.Back,
          showTitle: false,
          color: newColor,
          visible: true,
        },
      },
    });
  };

  private mapDates = (schedules: CarePlanScheduleFragment[]) => {
    if (schedules) {
      schedules.forEach(
        schedule =>
          (this.dateToSchedules[this.calendarLibFormat(schedule.limitDate)] =
            schedule),
      );
    }
  };

  private handleDayPress = (date: DateObject) => {
    this.currentSchedule = this.dateToSchedules[date.dateString];

    this.setState({ currentSelected: date.dateString });

    this.handleHabitCheckTap();
  };

  private handleHabitCheckTap = () => {
    if (this.currentSchedule && !this.currentSchedule.doneAt) {
      new AlertBuilder()
        .withTitle(HabitStrings.FulfillDialog.Title)
        .withMessage(
          HabitStrings.FulfillDialog.Message(
            DateUtils.Format.presentation(this.currentSchedule.limitDate),
          ),
        )
        .withButton({ text: GeneralStrings.Cancel })
        .withButton({
          text: GeneralStrings.Yes,
          onPress: this.handleFulfillDialogConfirmation,
        })
        .show();
    }
  };

  private handleFulfillDialogConfirmation = () => {
    const input: FulfillEventInput = {
      id: this.currentSchedule.id,
      itemCode: this.details.carePlanItem.itemCode,
      fulfilledAt: DateUtils.Format.datasource(new Date(), true),
    };

    this.setState({ fulfillLoading: true });
    execFulFillMutation(input)
      .then(this.handleFulfillSuccess)
      .catch(error => this.handleFulfillError(error));
    // this.graphQLProvider.mutate<FulfillHabitMutationVariables>(
    //   'fulfill-habit',
    //   { data: input },
    //   this.handleFulfillSuccess,
    //   this.handleFulfillError,
    // );
  };

  private handleFulfillSuccess = (res: CarePlanScheduleModel) => {
    console.warn('Marked >', res);
    const updatedSchedule = res;
    const updatedScheduleIndex = this.schedules.findIndex(
      schedule => schedule.id === updatedSchedule.id,
    );

    this.schedules[updatedScheduleIndex] = updatedSchedule;

    this.setState({ fulfillLoading: false });

    if (this.props.navigation.onFulfillSuccess) {
      this.props.navigation.onFulfillSuccess();
    }

    FlashMessageService.showSuccess(HabitStrings.FulfillDialog.Success);
  };

  private handleFulfillError = (error: ApolloError) => {
    this.setState({ fulfillLoading: false });
    FlashMessageService.showError(error);
  };

  private calendarLibFormat(date: string | Date): string {
    return moment(date).format('YYYY-MM-DD');
  }

  // TODO Calculate progress based on multiple times in a day
  private getProgress = (dateString: string) => {
    const done = this.dateToSchedules[dateString].status === 'Done';
    return Number(!!this.dateToSchedules[dateString] && done);
  };

  private setData = (habitDetails: HabitFragment) => {
    this.details = habitDetails;

    if (!this.schedules) {
      this.schedules =
        this.details &&
        this.details.carePlanItem &&
        _.cloneDeep(this.details.carePlanItem.schedules);
    }

    this.mapDates(this.schedules);

    this.currentSchedule = this.dateToSchedules[this.state.currentSelected];
  };

  private mapDateToMarkings = () => {
    const markings: CustomCalendarMarking = {};
    if (this.schedules) {
      this.schedules.forEach(schedule => {
        const dateString = this.calendarLibFormat(schedule.limitDate);
        markings[dateString] = {
          progress: this.getProgress(dateString),
          alert: !!schedule.failedAt,
        };
      });
    }
    return markings;
  };
}

const styles = StyleSheet.create({
  frequencyBox: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 10,
    paddingTop: 10,
    alignContent: 'space-between',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#eeeeee',
    borderRadius: 10,
    width: '100%',
  },

  subTitle: {},
});
