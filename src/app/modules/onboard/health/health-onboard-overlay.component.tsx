import * as React from 'react';
import { NavigationPageProps } from '@app/core/navigation';
import { Asset } from '@atomic';
import { OnboardOverlay } from '../onboard-overlay.component';
import { HealthOnboardStrings } from './health-onboard-overlay.strings';

export class HealthOnboardOverlay extends React.PureComponent<NavigationPageProps<{}>> {
  static options = { name: 'HealthOnboardOverlay' };

  private readonly data = [
    {
      title: HealthOnboardStrings.MedicalRecord.Title,
      description: HealthOnboardStrings.MedicalRecord.Description,
      image: Asset.Onboard.OnboardRecord,
    },
    {
      title: HealthOnboardStrings.Medicine.Title,
      description: HealthOnboardStrings.Medicine.Description,
      image: Asset.Onboard.OnboardMedicine,
    },
    {
      title: HealthOnboardStrings.PastEvents.Title,
      description: HealthOnboardStrings.PastEvents.Description,
      image: Asset.Onboard.OnboardEvents,
    },
  ];

  render() {
    return <OnboardOverlay data={this.data} {...this.props} />;
  }
}
