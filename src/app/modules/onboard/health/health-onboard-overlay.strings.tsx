export const HealthOnboardStrings = {
  MedicalRecord: {
    Title: 'Ficha médica',
    Description: 'Veja as suas informações de maneira rápida e fácil.',
  },
  Medicine: {
    Title: 'Medicamentos',
    Description: 'Controle com facilidade os horários e a frequência no uso de medicamentos.',
  },
  PastEvents: {
    Title: 'Histórico',
    Description: 'Consulte atividades realizadas como exames e consultas, além de seus diagnósticos.',
  },
};
