export const CustomerCareOnboardStrings = {
  Schedule: {
    Title: 'Agende consultas',
    Description:
      'Toda a conveniência para agendar consultas em apenas um clique.',
  },
  Ask: {
    Title: 'Guia',
    Description:
      'Dúvidas clínicas ou administrativas devem ser tratadas primeiro com sua Guia. Disponível pelo chat.',
  },
  Search: {
    Title: 'Rede',
    Description: 'Encontre hospitais, laboratórios e médicos do seu plano.',
  },
  Voucher: {
    Title: 'Voucher',
    Description: 'Utilize seus vouchers para cuidar ainda mais de sua saúde.',
  },
  Telemedicina: {
    Title: 'Telemedicina',
    Description:
      'Sempre que precisar, agende uma consulta via telemedicina, disponível 24h por dia, 7 dias por semana.',
  },
};
