import * as React from 'react';
import { NavigationPageProps } from '@app/core/navigation';
import { Asset } from '@atomic';
import { OnboardOverlay } from '../onboard-overlay.component';
import { CustomerCareOnboardStrings } from './customer-care-overlay.strings';
import IconVoucherBlue from '@assets/icons/atendimento/icon-voucher-blue.png';
import IconTelemedicineBlue from '@assets/icons/atendimento/icon-telemedicine-blue.png';
export class CustomerCareOnboardOverlay extends React.PureComponent<
  NavigationPageProps<{}>
> {
  static options = { name: 'CustomerCareOnboardOverlay' };

  private readonly data = [
    {
      title: CustomerCareOnboardStrings.Schedule.Title,
      description: CustomerCareOnboardStrings.Schedule.Description,
      image: Asset.Onboard.OnboardCalendar,
    },
    {
      title: CustomerCareOnboardStrings.Ask.Title,
      description: CustomerCareOnboardStrings.Ask.Description,
      image: Asset.Onboard.OnboardChat,
    },
    {
      title: CustomerCareOnboardStrings.Search.Title,
      description: CustomerCareOnboardStrings.Search.Description,
      image: Asset.Onboard.OnboardSearch,
    },
    {
      title: CustomerCareOnboardStrings.Voucher.Title,
      description: CustomerCareOnboardStrings.Voucher.Description,
      image: IconVoucherBlue,
    },
    {
      title: CustomerCareOnboardStrings.Telemedicina.Title,
      description: CustomerCareOnboardStrings.Telemedicina.Description,
      image: IconTelemedicineBlue,
    },
  ];

  render() {
    return <OnboardOverlay data={this.data} {...this.props} />;
  }
}
