import { Navigation } from '@app/core/navigation';
import { CustomerCareOnboardOverlay } from './customer-care/customer-care-overlay.component';
import { HealthOnboardOverlay } from './health/health-onboard-overlay.component';
import { HomeOnboardOverlay } from './home/home-onboard-overlay.component';

export const OnboardNavigation = {
  register: () => {
    Navigation.register(HomeOnboardOverlay, false);
    Navigation.register(CustomerCareOnboardOverlay, false);
    Navigation.register(HealthOnboardOverlay, false);
  },
};
