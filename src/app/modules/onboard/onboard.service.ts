import { Container, Service } from 'typedi';
import { Navigation, NavigationPage } from '@app/core/navigation';
import { LocalDataSource } from '@app/data/local';
import { CustomerCareOnboardOverlay } from './customer-care/customer-care-overlay.component';
import { HealthOnboardOverlay } from './health/health-onboard-overlay.component';
import { HomeOnboardOverlay } from './home/home-onboard-overlay.component';

const PageOverlay: { [key: string]: NavigationPage } = {
  HomePage: HomeOnboardOverlay,
  HealthPage: HealthOnboardOverlay,
  CustomerCarePage: CustomerCareOnboardOverlay,
};

export const OnboardServiceKey = 'OnboardServiceKey';

@Service(OnboardServiceKey)
export class OnboardService {
  private readonly localDataSource: LocalDataSource = Container.get(LocalDataSource);

  async check(pageName: string) {
    if (PageOverlay[pageName]) {
      const alreadyAppeared = await this.localDataSource.get(pageName);
      if (!alreadyAppeared) {
        Navigation.showOverlay(PageOverlay[pageName]);
        this.localDataSource.set(pageName, 'true');
      }
    }
  }
}
