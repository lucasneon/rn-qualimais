export const HomeOnboardStrings = {
  HabitControl: {
    Title: 'Seus hábitos',
    Description: 'Na área de cuidados vc gerencia seus hábitos para uma vida mais saudável.',
  },
  HabitAdd: {
    Title: 'Ativando hábitos',
    Description: 'Basta clicar em Ativar hábitos e selecionar na lista de hábitos disponíveis.',
  },
  HabitCompleted: {
    Title: 'Hábitos completos',
    Description: 'Ao clicar em um hábito, você poderá ver e atualizar seu progresso',
  },
};
