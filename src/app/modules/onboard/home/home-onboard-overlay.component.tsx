import * as React from 'react';
import { NavigationPageProps } from '@app/core/navigation';
import { Asset } from '@atomic';
import { OnboardOverlay } from '../onboard-overlay.component';
import { HomeOnboardStrings } from './home-onboard-overlay.strings';
import { ActivateHabit } from '@app/config/tokens.config';
import Container from 'typedi';

export class HomeOnboardOverlay extends React.PureComponent<NavigationPageProps<{}>> {
  static options = { name: 'HomeOnboardOverlay' };

  private activateHabit: boolean = Container.get(ActivateHabit);

  private readonly data = [
    {
      title: HomeOnboardStrings.HabitControl.Title,
      description: HomeOnboardStrings.HabitControl.Description,
      image: Asset.Onboard.OnboardClock,
    },
    {
      title: HomeOnboardStrings.HabitAdd.Title,
      description: HomeOnboardStrings.HabitAdd.Description,
      image: Asset.Onboard.OnboardAdd,
    },
    {
      title: HomeOnboardStrings.HabitCompleted.Title,
      description: HomeOnboardStrings.HabitCompleted.Description,
      image: Asset.Onboard.OnboardCheck,
    },
  ];

  render() {
    if (!this.activateHabit) {
      this.data.splice(1, 1);
    }
    return <OnboardOverlay data={this.data} {...this.props} />;
  }
}
