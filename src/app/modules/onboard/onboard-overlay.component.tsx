import * as React from 'react';
import { CardActionSheet } from '@app/components/mol.card-action-sheet/card-action-sheet.component';
import { OnboardCarousel, OnboardCarouselItem } from '@app/components/mol.onboard-carousel/onboard-carousel.component';
import { Navigation } from '@app/core/navigation';

interface OnboardProps {
  componentId?: string;
  data: OnboardCarouselItem[];
}

export class OnboardOverlay extends React.PureComponent<OnboardProps> {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (!this.props.data) {
      throw new Error('Onboard overlays should have a "data" prop');
    }
  }

  render() {
    return (
      <CardActionSheet visible={true} onCancel={this.handleDismiss} onDismiss={this.handleDismiss}>
        <OnboardCarousel onFinish={this.handleDismiss} data={this.props.data} />
      </CardActionSheet >
    );
  }

  private handleDismiss = async () => {
    await Navigation.dismissOverlay(this.props.componentId);
  }
}
