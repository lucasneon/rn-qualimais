import { DataSourceErrorInfoModel } from './datasource.error';

export class BaseError extends Error {
  original: string;
  moreInfo?: DataSourceErrorInfoModel | string;

  constructor(type: ErrorType, message?: string) {
    super(message);

    this.name = ErrorType[type];
    this.stack = (new Error() as any).stack;
    this.original = message;
    this.message = message;
  }
}

export class CustomError extends BaseError {
  constructor(message: string, public code: number, moreInfo?: string) {
    super(ErrorType.CustomError, message);
    this.moreInfo = moreInfo;
  }
}

export enum ErrorType {
  AuthenticationError,
  BadRequestError,
  CustomError,
  DataSourceError,
  DataValidationError,
  ExistingDataError,
  GraphQLError,
  JwtValidationError,
  NotFoundError,
  ParameterRequiredError,
  ParameterParseJsonError,
  RepositoryError,
  SchemaValidationError,
  EmailError,
  TypeError,
  ValidationError,
}
