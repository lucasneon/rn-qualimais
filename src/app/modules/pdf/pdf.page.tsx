import * as React from 'react';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { Dimensions, StyleSheet, Alert, PermissionsAndroid, Platform } from 'react-native';
import { GuardProps } from '@app/core/navigation';
import { GuardParams } from '@app/modules/authentication';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { OnboardService, OnboardServiceKey } from '@app/modules/onboard';
import { Container } from 'typedi';

// import Pdf from 'react-native-pdf';
// import RNFetchBlob from 'react-native-fetch-blob';
import { PrimaryButton, VBox, VSeparator } from '@atomic';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { AppError } from '@app/model';

const WIN_WIDTH = Dimensions.get('window').width;

interface PDFPageProps extends GuardProps<GuardParams> {
    url: string;
    title: string;
}

interface PDFPageState {
    page: number;
    scale: number;
    numberOfPages: number;
    horizontal: boolean;
    width: number;
}

@PageView('PDF')
export class PDFPage extends React.Component<NavigationPageProps<PDFPageProps>, PDFPageState> {
    static options = { name: 'PDFPage' };

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            scale: 1,
            numberOfPages: 0,
            horizontal: false,
            width: WIN_WIDTH
        };
        Navigation.events().bindComponent(this);
    }
    componentDidAppear() {
        Container.get<OnboardService>(OnboardServiceKey).check('PDFPage');
    }

    handleDownload(url: string) {
        // if (Platform.OS === 'ios') {
        //     RNFetchBlob.ios.openDocument(url);
        //     // IOSDownloadTask: true,
        //     // path: destinationDir + "/boleto_" + Math.floor(date.getTime() + date.getSeconds() / 2) + ext,
        // } else {
        //     let dirs = RNFetchBlob.fs.dirs;
        //     let date = new Date();
        //     let ext = ".pdf";

        //     let options = {
        //         fileCache: true,
        //         addAndroidDownloads: {
        //             useDownloadManager: true,
        //             notification: true,
        //             path: dirs.DownloadDir + "/boleto_" + Math.floor(date.getTime() + date.getSeconds() / 2) + ext,
        //             description: 'Boleto'
        //         }
        //     }

        //     // console.warn('#############picture dir: ' + options.path);
        //     RNFetchBlob.config(options).fetch('GET', url).then((res) => {
        //         Alert.alert("Success Downloaded: " + res.path());
        //     }).catch((err) => {
        //             console.warn('### err: ' + err);
        //     });
        // }
    }

    async requestCameraPermission() {
        // console.warn('###############################foi');
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: 'Cool Photo App Camera Permission',
                    message:
                        'Cool Photo App needs access to your camera ' +
                        'so you can take awesome pictures.',
                },
            );
            console.warn('GRANTED: ' + granted);
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // console.warn('You can use the camera');
                this.handleDownload(this.props.navigation.title);
            } else {
                // console.warn('Camera permission denied');
            }
        } catch (err) {
            // console.warn(err);
        }
    }

    render() {
        let source = { uri: this.props.navigation.url, cache: false };
        // const source = require('./test.pdf');
        const erro: AppError = {
            title: 'Arquivos não encontrados',
            message: 'Não há arquivos disponíveis para download',
        };

        return (
            <CollapsibleHeader title={this.props.navigation.title}>
                <VBox>
                    <PrimaryButton
                        negative={true}
                        expanded={true}
                        text={'Download pdf'}
                        onTap={() => this.requestCameraPermission()}
                    />
                </VBox>
                <VSeparator />
                {/* {this.props.navigation.url ?
                    <Pdf
                        source={source}
                        style={styles.pdf}
                        page={1}
                        scale={1}
                        horizontal={this.state.horizontal}
                        onLoadComplete={(numberOfPages, tableContents) => {
                            // console.warn(`total page count: ${numberOfPages}`);
                            // console.warn(tableContents);
                        }}
                    />
                    : <PlaceholderSelector error={erro} />} */}
            </CollapsibleHeader>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    pdf: {
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    }
})