import { Navigation } from '@app/core/navigation';
import { LoginGuard } from '@app/modules/authentication';
import { HealthOrientationPage } from './health-orientations.page';
import { HealthPage } from './health.page';
import { HealthMedicalRecordPage } from './medical-record';
import { AllergyPage } from './medical-record/allergy';
import { VaccinePage } from './medical-record/vaccine/vaccine.page';
import { PastEventsPage } from './past-events.page';
import { CampaignsPage } from './questionnaire/campaigns.page';
import { QuestionGroupPage } from './questionnaire/question-group.page';
import { InsertVitalSignPage, VitalSignChartPage } from './vital-signs';

export const HealthNavigation = {
  register: () => {
    Navigation.register(HealthPage, true, LoginGuard());
    Navigation.register(HealthMedicalRecordPage, true, LoginGuard());
    Navigation.register(HealthOrientationPage);
    Navigation.register(CampaignsPage);
    Navigation.register(QuestionGroupPage);
    Navigation.register(PastEventsPage);
    Navigation.register(VitalSignChartPage);
    Navigation.register(InsertVitalSignPage);
    Navigation.register(AllergyPage);
    Navigation.register(VaccinePage);
  },
};
