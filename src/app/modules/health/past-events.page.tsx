import * as React from 'react';
import { Container } from 'typedi';
import { Subscribe } from 'unstated';
import { CarePlanEvents } from '@app/components/org.care-plan-events';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { NavigationPageProps } from '@app/core/navigation';
import { HealthStrings } from './health.strings';
import {
  PastEventsProvider,
  PastEventsProviderKey,
} from './past-events.provider';

@PageView('PastEvents')
export class PastEventsPage extends React.Component<
  NavigationPageProps<undefined>
> {
  static options = { name: 'PastEventsPage' };

  private provider: PastEventsProvider = Container.get(PastEventsProviderKey);

  constructor(props) {
    super(props);
    this.provider.active = true;
    this.provider.rehydrate();
  }

  componentWillUnmount() {
    this.provider.active = false;
  }

  render() {
    return (
      <CollapsibleHeader title={HealthStrings.PastEvents.Title}>
        <Subscribe to={[this.provider]}>
          {(provider: PastEventsProvider) => {
            return (
              <CarePlanEvents
                loading={provider.state.loading}
                error={provider.state.error}
                events={provider.state.events}
              />
            );
          }}
        </Subscribe>
      </CollapsibleHeader>
    );
  }
}
