import * as React from 'react';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { QuestionnaireFeedbackFragment } from '@app/data/graphql';
import { ErrorMapper } from '@app/utils';
import { LoadingState } from '@atomic';
import { HealthMedicalRecordStrings } from './medical-record/health-medical-record.strings';
import {
  QuestionnaireFeedback,
  QuestionnaireFeedbackShimmer,
} from './questionnaire-feedback.component';
import { HealthOrientationProvider } from '@app/services/providers/health-orientation.provider';
import { Subscribe } from 'unstated';

@PageView('HealthOrientation')
export class HealthOrientationPage extends React.Component<any, any> {
  static options = { name: 'HealthOrientationPage' };
  private healthOrientationProvider: HealthOrientationProvider;

  private loading: boolean = true;

  componentDidMount() {
    this.healthOrientationProvider.fetchOrientations();
  }

  render() {
    return (
      <CollapsibleHeader
        title={HealthMedicalRecordStrings.CheckHealthOrientations}>
        <Subscribe to={[HealthOrientationProvider]}>
          {(healthOrientationProvider: HealthOrientationProvider) => {
            this.healthOrientationProvider = healthOrientationProvider;
            const questionnairesFeedback =
              healthOrientationProvider.state.orientations;
            const error = healthOrientationProvider.state.error;
            const loading = healthOrientationProvider.state.loading;
            const handlePlaceholderTap = () =>
              healthOrientationProvider.refetch();
            return (
              <LoadingState
                data={!!questionnairesFeedback}
                error={!!error}
                loading={loading}>
                <LoadingState.Shimmer>
                  {new Array(5).fill(null).map((_, index) => (
                    <QuestionnaireFeedbackShimmer key={index} />
                  ))}
                </LoadingState.Shimmer>
                <LoadingState.ErrorPlaceholder>
                  <PlaceholderSelector
                    error={ErrorMapper.map(error)}
                    onTap={handlePlaceholderTap}
                  />
                </LoadingState.ErrorPlaceholder>
                {questionnairesFeedback &&
                  questionnairesFeedback.map(
                    (feedback: QuestionnaireFeedbackFragment) => (
                      <QuestionnaireFeedback
                        key={feedback.questionnaireName}
                        feedbackData={feedback}
                      />
                    ),
                  )}
              </LoadingState>
            );
          }}
        </Subscribe>
        {/* <QueryContainer document="questionnaires-feedback">
          {(res: QueryResult<QuestionnairesFeedbackQuery>) => {
            const handlePlaceholderTap = () => res.refetch();
            const questionnairesFeedback =
              res.data && res.data.QuestionnairesFeedback;
            return (
              <LoadingState
                data={!!questionnairesFeedback}
                error={!!res.error}
                loading={res.loading}>
                <LoadingState.Shimmer>
                  {new Array(5).fill(null).map((_, index) => (
                    <QuestionnaireFeedbackShimmer key={index} />
                  ))}
                </LoadingState.Shimmer>
                <LoadingState.ErrorPlaceholder>
                  <PlaceholderSelector
                    error={ErrorMapper.map(res.error)}
                    onTap={handlePlaceholderTap}
                  />
                </LoadingState.ErrorPlaceholder>
                {questionnairesFeedback &&
                  questionnairesFeedback.map(
                    (feedback: QuestionnaireFeedbackFragment) => (
                      <QuestionnaireFeedback
                        key={feedback.questionnaireName}
                        feedbackData={feedback}
                      />
                    ),
                  )}
              </LoadingState>
            );
          }}
        </QueryContainer> */}
      </CollapsibleHeader>
    );
  }
}
