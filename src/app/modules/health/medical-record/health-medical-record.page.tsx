import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { Subscribe } from 'unstated';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { QueryContainer } from '@app/core/graphql';
import { GuardProps, Navigation } from '@app/core/navigation';
import {
  ConditionQuery,
  FamilyHistoryQuery,
  ProceduresQuery,
} from '@app/data/graphql';
import { Age, Beneficiary } from '@app/model';
import { GuardParams } from '@app/modules/authentication';
import { GeneralStrings } from '@app/modules/common';
import {
  mapCondition,
  mapConditionFragment,
  mapFamilyHistory,
  mapFamilyHistoryFragment,
  mapProcedures,
  mapProceduresFragment,
  mapVaccines,
  TagSection,
  TextSection,
} from '@app/modules/common/health-medical-record';
import { HealthOrientationPage } from '@app/modules/health';
import {
  AllergyProvider,
  VaccineProvider,
  ProcedureProvider,
} from '@app/services/providers';
import { DateUtils } from '@app/utils';
import { Body, H3, PrimaryButton, Root, VBox, VSeparator } from '@atomic';
import { AllergyPage } from './allergy';
import { HealthMedicalRecordStrings } from './health-medical-record.strings';
import { VaccinePage } from './vaccine/vaccine.page';
import { ConditionProvider, ConditionProvier } from '@app/services/providers/condition.provider';
import { FamilyHistoryProvider } from '@app/services/providers/family-history.provider';

interface HealthMedicalRecordProps extends GuardProps<GuardParams> {
  componentId: string;
}

@PageView('HealthMedicalRecord')
export class HealthMedicalRecordPage extends React.Component<HealthMedicalRecordProps> {
  static options = { name: 'HealthMedicalRecordPage' };
  private allergyProvider: AllergyProvider;
  private vaccineProvider: VaccineProvider;
  private procedureProvider: ProcedureProvider;
  private conditionProvider: ConditionProvier;
  private familyHistoryProvider: FamilyHistoryProvider;

  componentDidMount() {
    this.allergyProvider.fetch();
    this.vaccineProvider.fetch();
    this.procedureProvider.fetch();
    this.conditionProvider.fetch();
    this.familyHistoryProvider.fetch();
  }

  render() {
    const activeBeneficiary: Beneficiary = this.props.guard.params.active;
    const age: Age =
      activeBeneficiary &&
      DateUtils.Calculate.getAge(activeBeneficiary.birthDate);

    return (
      <CollapsibleHeader title={HealthMedicalRecordStrings.Title}>
        <Root>
          <VBox>
            {activeBeneficiary && <H3>{activeBeneficiary.name}</H3>}
            {activeBeneficiary && <Body>{GeneralStrings.Age(age)}</Body>}
            <VSeparator />

            <Subscribe to={[AllergyProvider]}>
              {(allergyProvider: AllergyProvider) => {
                this.allergyProvider = allergyProvider;
                const error = allergyProvider.state.error;
                const loading = allergyProvider.state.loading;
                const data = this.allergyProvider.state.data;
                return (
                  <TagSection
                    title={HealthMedicalRecordStrings.Allergies}
                    tags={
                      data &&
                      this.allergyProvider.state.data.map(al => al.display)
                    }
                    loading={loading}
                    error={error}
                    onButtonTap={this.handleAddAllergyTap}
                  />
                );
              }}
            </Subscribe>

            <Subscribe to={[VaccineProvider]}>
              {(vaccineProvider: VaccineProvider) => {
                this.vaccineProvider = vaccineProvider;
                const error = vaccineProvider.state.error;
                const loading = vaccineProvider.state.loading;
                const data = this.vaccineProvider.state.data;
                return (
                  <TagSection
                    title={HealthMedicalRecordStrings.Vaccines}
                    tags={data && mapVaccines(data)}
                    loading={loading}
                    error={error}
                    onButtonTap={this.handleAddVaccineTap}
                  />
                );
              }}
            </Subscribe>
            <Subscribe to={[ProcedureProvider]}>
              {(procedureProvider: ProcedureProvider) => {
                this.procedureProvider = procedureProvider;
                const error = procedureProvider.state.error;
                const loading = procedureProvider.state.loading;
                const data = procedureProvider.state.data;
                return (
                  <TextSection
                    title={HealthMedicalRecordStrings.Procedures}
                    tags={data && mapProceduresFragment(data)}
                    loading={loading}
                    error={error}
                  />
                );
              }}
            </Subscribe>
            {/* <QueryContainer document="procedures">
              {(result: QueryResult<ProceduresQuery>) => (
                <TextSection
                  title={HealthMedicalRecordStrings.Procedures}
                  tags={result.data && mapProcedures(result.data)}
                  loading={result.loading}
                  error={result.error}
                />
              )}
            </QueryContainer> */}

            <Subscribe to={[ConditionProvider]}>
              {(conditionProvider: ConditionProvider) => {
                this.conditionProvider = conditionProvider;
                const error = conditionProvider.state.error;
                const loading = conditionProvider.state.loading;
                const data = conditionProvider.state.data;
                return (
                  <TextSection
                    title={HealthMedicalRecordStrings.Condition}
                    tags={data && mapConditionFragment(data)}
                    loading={loading}
                    error={error}
                  />
                );
              }}
            </Subscribe>

            {/* <QueryContainer document="condition">
              {(result: QueryResult<ConditionQuery>) => (
                <TextSection
                  title={HealthMedicalRecordStrings.Condition}
                  tags={result.data && mapCondition(result.data)}
                  loading={result.loading}
                  error={result.error}
                />
              )}
            </QueryContainer> */}

            <Subscribe to={[FamilyHistoryProvider]}>
              {(familyHistoryProvider: FamilyHistoryProvider) => {
                this.familyHistoryProvider = familyHistoryProvider;
                const error = familyHistoryProvider.state.error;
                const loading = familyHistoryProvider.state.loading;
                const data = familyHistoryProvider.state.data;
                return (
                  <TextSection
                    title={HealthMedicalRecordStrings.FamilyHistory}
                    tags={data && mapFamilyHistoryFragment(data)}
                    loading={loading}
                    error={error}
                  />
                );
              }}
            </Subscribe>
            {/* <QueryContainer document="family-history">
              {(result: QueryResult<FamilyHistoryQuery>) => (
                <TextSection
                  title={HealthMedicalRecordStrings.FamilyHistory}
                  tags={result.data && mapFamilyHistory(result.data)}
                  loading={result.loading}
                  error={result.error}
                />
              )}
            </QueryContainer> */}
            <PrimaryButton
              expanded={true}
              text={HealthMedicalRecordStrings.CheckHealthOrientations}
              onTap={this.handleHealthOrientationTap}
            />
            <VSeparator />
          </VBox>
        </Root>
      </CollapsibleHeader>
    );
  }

  private handleHealthOrientationTap = () => {
    Navigation.push(this.props.componentId, HealthOrientationPage);
  };

  private handleAddAllergyTap = () => {
    Navigation.showModal(AllergyPage);
  };

  private handleAddVaccineTap = () => {
    Navigation.showModal(VaccinePage);
  };
}
