export const HealthMedicalRecordStrings = {
  Title: 'Ficha médica',
  AddVaccine: 'Adicionar vacina',
  Vaccines: 'Vacinas',
  Allergies: 'Alergias',
  Procedures: 'Cirurgias',
  Condition: 'Doenças',
  FamilyHistory: 'Histórico familiar',
  CheckHealthOrientations: 'Orientações de saúde',
};
