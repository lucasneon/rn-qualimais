import { ApolloError } from 'apollo-client';
import moment from 'moment';
import * as React from 'react';
import { Subscribe } from 'unstated';
import { CustomAlerts } from '@app/components/obj.custom-alert';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { Navigation } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { VaccineFragment } from '@app/data/graphql';
import { ModalPage } from '@app/modules/common';
import { VaccineProvider } from '@app/services/providers/vaccine.provider';
import { DateUtils, ErrorMapper } from '@app/utils';
import {
  DateTimePickerField,
  Form,
  FormSubmitData,
  PrimaryButton,
  Root,
  TextField,
  TextFieldType,
  Validators,
  VBox,
  VSeparator,
} from '@atomic';
import { VaccinePageStrings } from './vaccine.strings';

const Fields = {
  vaccine: 'vaccine',
  date: 'date',
};

@PageView('Vaccine')
export class VaccinePage extends ModalPage<undefined, undefined> {
  static options = { name: 'VaccinePage' };
  private vaccineProvider: VaccineProvider;

  render() {
    return (
      <CollapsibleHeader title={VaccinePageStrings.Title}>
        <Root>
          <Form onSubmit={this.handleSubmit}>
            <Form.Scroll keyboardAware={false}>
              <VBox>
                <VSeparator />
                <Form.Field
                  label={VaccinePageStrings.Label}
                  validators={[Validators.Required()]}
                  name={Fields.vaccine}>
                  <TextField type={TextFieldType.Normal} />
                </Form.Field>
                <VSeparator />
                <Form.Field
                  label={VaccinePageStrings.Date}
                  validators={[
                    Validators.Required(),
                    Validators.MaxDateTime(DateUtils.Create.getCurrent(0, 1)),
                  ]}
                  name={Fields.date}
                  value={new Date()}>
                  <DateTimePickerField
                    maxDateTime={DateUtils.Create.getCurrent(0, 1)}
                    mode={'date'}
                  />
                </Form.Field>
                <VSeparator />
                <Subscribe to={[VaccineProvider]}>
                  {(vaccineProvider: VaccineProvider) => {
                    this.vaccineProvider = vaccineProvider;
                    return (
                      <Form.Submit>
                        <PrimaryButton
                          text={VaccinePageStrings.Send}
                          expanded={true}
                          loading={this.vaccineProvider.state.loading}
                          submit={true}
                        />
                      </Form.Submit>
                    );
                  }}
                </Subscribe>
                <VSeparator />
              </VBox>
            </Form.Scroll>
          </Form>
        </Root>
      </CollapsibleHeader>
    );
  }

  handleSubmit = (formData: FormSubmitData) => {
    if (Object.keys(formData.error).length !== 0) {
      return;
    }

    const date = moment(formData.data.date).format('YYYY-MM-DD').toString();

    const data: VaccineFragment = {
      display: formData.data.vaccine,
      takenAt: date,
    };

    this.onSubmit(data);
  };

  private onSubmit = (data: VaccineFragment) => {
    CustomAlerts.additionConfirmation(() =>
      this.vaccineProvider.insert(
        { data },
        this.handleSuccess,
        this.handleError,
      ),
    );
  };

  private handleSuccess = async () => {
    await Navigation.dismissModal(this.props.componentId);
  };

  private handleError = (error: ApolloError) => {
    FlashMessageService.showError(ErrorMapper.map(error));
  };
}
