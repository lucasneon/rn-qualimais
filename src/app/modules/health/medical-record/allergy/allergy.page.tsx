import { ApolloError } from 'apollo-client';
import moment from 'moment';
import * as React from 'react';
import { Subscribe } from 'unstated';
import { CustomAlerts } from '@app/components/obj.custom-alert';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { Navigation } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { AllergyFragment } from '@app/data/graphql';
import { ModalPage } from '@app/modules/common';
import { AllergyProvider } from '@app/services/providers/allergy.provider';
import { DateUtils, ErrorMapper } from '@app/utils';
import {
  DateTimePickerField, Form, FormSubmitData, PrimaryButton, Root, TextField, TextFieldType, Validators, VBox, VSeparator
} from '@atomic';
import { AllergyPageStrings } from './allergy.strings';
import { AllergyModel } from '../../api/allergies/allergies.model';

enum Fields {
  allergy = 'allergy',
  date = 'date',
}

@PageView('Allergy')
export class AllergyPage extends ModalPage<undefined, undefined> {
  static options = { name: 'AllergyPage' };
  private allergyProvider: AllergyProvider;

  render() {
    return (
      <CollapsibleHeader title={AllergyPageStrings.Title}>
        <Root>
          <Form onSubmit={this.handleSubmit}>
            <Form.Scroll keyboardAware={false}>
              <VBox>
                <VSeparator />
                <Form.Field
                  label={AllergyPageStrings.Label}
                  validators={[Validators.Required()]}
                  name={Fields.allergy}
                >
                  <TextField type={TextFieldType.Normal} />
                </Form.Field>
                <VSeparator />
                <Form.Field
                  label={AllergyPageStrings.Date}
                  validators={[Validators.Required(), Validators.MaxDateTime(DateUtils.Create.getCurrent(0, 1))]}
                  name={Fields.date}
                  value={new Date()}
                >
                  <DateTimePickerField maxDateTime={DateUtils.Create.getCurrent(0, 1)} mode={'date'} />
                </Form.Field>
                <VSeparator />
                <Subscribe to={[AllergyProvider]}>
                  {(allergyProvider: AllergyProvider) => {
                    this.allergyProvider = allergyProvider;
                    return (
                      <Form.Submit>
                        <PrimaryButton
                          text={AllergyPageStrings.Send}
                          expanded={true}
                          loading={this.allergyProvider.state.loading}
                          submit={true}
                        />
                      </Form.Submit>
                    );
                  }}
                </Subscribe>
                <VSeparator />
              </VBox>
            </Form.Scroll>
          </Form>
        </Root>
      </CollapsibleHeader>
    );
  }

  handleSubmit = (formData: FormSubmitData) => {
    if (Object.keys(formData.error).length !== 0) {
      return;
    }
    const date = moment(formData.data.date).format('YYYY-MM-DD').toString();

    const data: AllergyModel = {
      display: formData.data.allergy,
      assertedDate: date,
    };

    this.onSubmit(data);
  }

  private onSubmit = (data: AllergyModel) => {
    CustomAlerts.additionConfirmation(() => {
      this.allergyProvider.insert(
        { data },
        this.handleSuccess,
        this.handleError,
      );
    });
  };

  private handleSuccess = async () => {
    await Navigation.dismissModal(this.props.componentId);
  }

  private handleError = (error: ApolloError) => {
    FlashMessageService.showError(ErrorMapper.map(error));
  }
}
