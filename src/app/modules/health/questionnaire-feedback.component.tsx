import * as React from 'react';
import { QuestionnaireFeedbackFragment, QuestionnaireFeedbackItemFragment } from '@app/data/graphql';
import { Body, FontSize, H2, Shimmer, VBox, VSeparator } from '@atomic';
import { HealthStrings } from './health.strings';

export interface QuestionnaireFeedbackProps {
  feedbackData: QuestionnaireFeedbackFragment;
}

export const QuestionnaireFeedbackTitleShimmer = () => (
  <VBox noGutter={true} vAlign='flex-end' hAlign='center'>
    <VSeparator />
    <Shimmer rounded={true} width='70%' height={FontSize.Large} />
  </VBox>
);

export const QuestionnaireFeedbackShimmer: React.SFC = () => (
  <VBox>
    <QuestionnaireFeedbackTitleShimmer />
    <VSeparator />
    <Shimmer width='95%' height={FontSize.Small} />
    <Shimmer width='90%' height={FontSize.Small} />
    <Shimmer width='97%' height={FontSize.Small} />
  </VBox>
);

export class QuestionnaireFeedback extends React.PureComponent<QuestionnaireFeedbackProps> {

  render() {
    const feedbackData = this.props.feedbackData;
    const items = feedbackData.items;

    return (
      <VBox >
        <H2>{feedbackData.questionnaireName}</H2>
        {items && items.length > 0 ? items.map((item: QuestionnaireFeedbackItemFragment) =>
          <React.Fragment key={item.description}>
            <Body>{item.description}</Body>
            <VSeparator />
          </React.Fragment>,
        ) : (
          <Body>{HealthStrings.Questionnaire.NoFeedbackAvailable}</Body>
        )}
      </VBox>
    );
  }
}
