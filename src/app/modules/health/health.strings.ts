export const HealthStrings = {
  Questionnaires: 'Questionários',
  MedicalRecord: 'Ficha médica',
  Medication: 'Medicamentos',
  PastEvents: {
    Cell: 'Eventos anteriores',
    Title: 'Histórico',
  },
  Questionnaire: {
    NoFeedbackAvailable: 'Não há orientações relativas a esse questionário',
  },
};
