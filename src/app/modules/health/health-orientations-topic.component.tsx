import * as React from 'react';
import { QuestionnaireFeedbackItemFragment, QuestionnaireFeedbackTopicReferenceEnum } from '@app/data/graphql';
import { Asset, Body, Cell, H3, Shimmer, VBox, VSeparator } from '@atomic';

export interface HealthOrientationsTopicProps {
  item: QuestionnaireFeedbackItemFragment;
}

const TopicIcons: { readonly [key in QuestionnaireFeedbackTopicReferenceEnum]: string } = {
  [QuestionnaireFeedbackTopicReferenceEnum.GeneralHealth]: Asset.Icon.Custom.HealthIcon,
  [QuestionnaireFeedbackTopicReferenceEnum.EmotionalHealth]: Asset.Icon.Custom.Emotions,
  [QuestionnaireFeedbackTopicReferenceEnum.Exercise]: Asset.Icon.Custom.Workout,
  [QuestionnaireFeedbackTopicReferenceEnum.Habits]: Asset.Icon.Custom.Habit,
  [QuestionnaireFeedbackTopicReferenceEnum.Nutrition]: Asset.Icon.Custom.Nutrition,
  [QuestionnaireFeedbackTopicReferenceEnum.SelfCare]: Asset.Icon.Custom.SelfCare,
  [QuestionnaireFeedbackTopicReferenceEnum.Smoking]: Asset.Icon.Custom.SmokingIcon,
  [QuestionnaireFeedbackTopicReferenceEnum.Weight]: Asset.Icon.Custom.Weight,
};

export const HealthOrientationsTopicShimmer = () => (
  <>
    <Cell>
      <Shimmer height={16} width={160} />
    </Cell>
    <VSeparator />
    {new Array(8).fill(null).map((_, i) => <Shimmer key={i} />)}
    <Shimmer width={250} />
    <VSeparator />
  </>
);

export class HealthOrientationsTopic extends React.PureComponent<HealthOrientationsTopicProps> {

  render() {
    const item = this.props.item;

    return (
      <React.Fragment key={item.topic.reference}>
        <Cell leftThumb={TopicIcons[item.topic.reference] || Asset.Icon.Custom.HealthIcon}>
          <H3>{item.topic.title}</H3>
        </Cell>
        <VBox>
          <VSeparator />
          <Body>{item.description}</Body>
        </VBox>
        <VSeparator />
      </React.Fragment>
    );
  }
}
