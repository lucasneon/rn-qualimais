// private conditionRequest(beneficiaryCode: string): Promise<AxiosResponse<MedicalRecordResponse[]>> {
//     const path = this.interceptor.getCodesPath() + `/${beneficiaryCode}`;
//     return this.interceptor.carePlanEnvBuilderWithInterceptors()
//       .get(`/fusion-gps/api/beneficiario/dados-clinicos/doencas/${path}`)
//       .build()
//       .execute<MedicalRecordResponse[]>();
//   }

import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import { LocalDataSource } from '@app/data';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import Container from 'typedi';

const opCode: string = Container.get(GAMA_OP_CODE);
const simulatingCode: string = Container.get(GAMA_STIMULATING_CODE);
const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const conditionRequest = async () => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const res = await fetchCondition(beneficiaryCode);
  if (!res.data) {
    return [];
  }
  return res.data
    .filter(it => it.ativo)
    .map(conditionRes => ({
      display: conditionRes.descricao,
      notes: conditionRes.observacao,
      assertedDate: conditionRes.data && new Date(conditionRes.data),
    }));
};

const fetchCondition = (beneficiaryCode: string) => {
  const path = `${opCode}/${simulatingCode}/${beneficiaryCode}`;
  return axiosGet({
    url: `/beneficiario/dados-clinicos/doencas/${path}`,
    baseUrl: BaseUrl.GAMA_FUSION_GPS,
  });
};
