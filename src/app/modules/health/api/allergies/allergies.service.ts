import {
  GAMA_OP_CODE,
  GAMA_STIMULATING_CODE,
  GAMA_SYSTEM_ORIGIN,
} from '@app/config';
import { LocalDataSource } from '@app/data';
import { InputParams } from '@app/model/api/input.params.model';
import {
  axiosGet,
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import moment from 'moment';
import Container from 'typedi';
import { AllergyModel, MedicalRecordRequest } from './allergies.model';

const opCode: string = Container.get(GAMA_OP_CODE);
const stimulatingCode: string = Container.get(GAMA_STIMULATING_CODE);
const systemOrigin: string = Container.get(GAMA_SYSTEM_ORIGIN);

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const allergiesRequest = async () => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const res = await fetchAllergies(beneficiaryCode);

  if (!res?.data || res?.error) {
    return { data: null, error: res.error };
  }

  const data = res.data.filter(it => it.ativo === 'S').map(mapAllergy);

  return { data, error: null };
};

const fetchAllergies = async (beneficiaryCode: string) => {
  let res;
  const path = `${opCode}/${stimulatingCode}/${beneficiaryCode}`;
  try {
    res = await axiosGet({
      url: `/beneficiario/dados-clinicos/alergias/${path}`,
      baseUrl: BaseUrl.GAMA_FUSION_GPS,
    });
  } catch (e) {
    console.error(e);
    return { data: null, error: e };
  }
  return {
    data: res.data,
    error: false,
  };
};

const mapAllergy = (data: any) => {
  return {
    __typename: 'Allergy',
    display: data.descricao,
    assertedDate: moment(data.data).format('YYYY-MM-DD'),
  };
};

export const addAllergy = async (input: {
  data: InputParams<AllergyModel>;
}): Promise<any> => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  // const id_pweb: string = await localDataSource.get<string>('id_pweb');
  let assertedDate = moment(input.data.assertedDate).toISOString();
  const request: MedicalRecordRequest = {
    codigoOperadora: opCode,
    codigoEstipulante: stimulatingCode,
    codigoBeneficiario: beneficiaryCode,
    ativo: 'S',
    data: assertedDate,
    descricao: input.data.display,
    sistemaOrigem: systemOrigin,
    // codigoUsuario: id_pweb || null,
  };
  const res = await postAllergyRequest(request);
  if (res.error) {
    throw new Error(res.error);
  }
  return { data: mapAllergy(res.data), error: false };
};

const postAllergyRequest = async (
  medicalRecordRequest: MedicalRecordRequest,
) => {
  let res;
  try {
    res = await axiosPost({
      url: '/fusion-gps/api/beneficiario/dados-clinicos/alergias',
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
      data: medicalRecordRequest,
    });
  } catch (e) {
    console.warn('error,', e);
    return { data: null, error: e };
  }
  return res;
};
