import { BaseCarePlanRequest } from '@app/model/api/common.entity';

export interface MedicalRecordRequest extends BaseCarePlanRequest {
  ativo: string;
  data: string | Date;
  descricao: string;
  sistemaOrigem: string;
  observacao?: string;
  codigoUsuario?: string | unknown;
}

export interface AllergyModel {
  display: string;
  assertedDate: Date | string;
}
