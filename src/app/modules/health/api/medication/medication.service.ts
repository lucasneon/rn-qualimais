import {
  axiosGet,
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import {
  GAMA_OP_CODE,
  GAMA_STIMULATING_CODE,
  GAMA_SYSTEM_ORIGIN,
} from '@app/config';
import Container from 'typedi';
import { LocalDataSource } from '@app/data';
import {
  ListMedicationInputModel,
  MedicationModel,
} from '@app/modules/home/api/medications/medication.model';
import {
  MedicationRequest,
  MedicationResponse,
} from '@app/modules/home/api/medications/medication.entity';
import { MedicationInput } from '@app/data/graphql';
import {
  DateFormatOptions,
  GamaDateFormatter,
} from '@app/model/api/common.mapper';
import moment from 'moment';

const opCode: string = Container.get(GAMA_OP_CODE);
const stimulantingCode: string = Container.get(GAMA_STIMULATING_CODE);
const systemOrigin: string = Container.get(GAMA_SYSTEM_ORIGIN);
const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchMedications = async (input: ListMedicationInputModel) => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const medications = await medicationsRequest(beneficiaryCode);
  if (medications?.error) {
    return { data: null, error: medications.error };
  }
  const mostRecentFirst = (medA: any, medB: any) =>
    new Date(medB.dataAlteracao).getTime() -
    new Date(medA.dataAlteracao).getTime();

  medications.data.sort(mostRecentFirst);

  medications.data.forEach(
    medication => (medication.frequency = getFrequency(medication)),
  );
  const medicationsFilter = input.activeOnly
    ? medications.data.filter(it => it.active)
    : medications;

  return medicationsFilter.data && medicationsFilter.data.length > 0
    ? { data: medicationsFilter.data.map(mapResponse), error: null }
    : { data: [], error: null };
};

const medicationsRequest = async (beneficiaryCode: string) => {
  const path = `${opCode}/${stimulantingCode}/${beneficiaryCode}`;

  let res;
  try {
    res = await axiosGet({
      baseUrl: BaseUrl.GAMA_FUSION_GPS,
      url: `/beneficiario/dados-clinicos/medicamentos/${path}`,
    });
  } catch (e) {
    return { data: null, error: e };
  }

  console.warn(res);

  return res;
};

function getFrequency(medication: MedicationModel) {
  const freq: number =
    medication.dailySchedule && medication.dailySchedule.length;
  return freq > 0
    ? this.frequencyDescription.replace('{times}', freq.toString())
    : null;
}

function mapResponse(response: MedicationResponse): MedicationModel {
  return {
    id: response.codigo && response.codigo.toString(),
    startDate: response.dataInicio && new Date(response.dataInicio),
    endDate: response.dataFim && new Date(response.dataFim),
    active: response.ativo === 'S' ? true : false,
    notify: response.notificacao === 'S' ? true : false,
    orientation: response.orientacao,
    dailySchedule: response.horarios.map(hourRes => ({
      id: hourRes.codigo,
      hour: new Date(hourRes.horario),
    })),
    medicine: {
      name: response.nome,
      aspect: response.aparencia,
      type: response.tipo,
    },
  };
}

export const fetchCreateMedication = async (input: MedicationInput) => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const id_pweb: string = await localDataSource.get<string>('id_pweb');
  const dateOptions: DateFormatOptions = {
    includeHours: true,
    considerOnlyDay: true,
    dayHourSeparator: 'T',
  };
  const req: MedicationRequest = {
    codigoOperadora: opCode,
    codigoEstipulante: stimulantingCode,
    codigoBeneficiario: beneficiaryCode,
    sistemaOrigem: systemOrigin,
    // codigoUsuario: id_pweb,
    nome: input.medicine.name,
    dataInicio: GamaDateFormatter.format(input.startDate, dateOptions),
    dataFim:
      input.endDate && GamaDateFormatter.format(input.endDate, dateOptions),
    aparencia: input.medicine.aspect,
    orientacao: input.orientation,
    ativo: 'S',
    notificacao: input.notify ? 'S' : 'N',
    intervalo: { chave: null },
    horarios: input.dailySchedule.map(sche => ({
      horario: GamaDateFormatter.format(
        moment().format('YYYY-MM-DD') + 'T' + sche.hour,
      ),
    })),
  };
  let res = await createteMedicationRequest(req);
  if (res.error) {
    throw new Error(res.error);
  }
  return res;
};

const createteMedicationRequest = async (data: MedicationRequest) => {
  let res;
  try {
    res = await axiosPost({
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
      url: '/fusion-gps/api/beneficiario/dados-clinicos/medicamentos',
      data,
    });
  } catch (e) {
    return { data: null, error: e };
  }
  return res;
};
