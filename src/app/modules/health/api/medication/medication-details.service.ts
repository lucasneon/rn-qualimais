import { MedicationModel } from '@app/modules/home/api/medications/medication.model';
import { fetchMedications } from './medication.service';

export const fetchMedicationDetails = async (input: any): Promise<any> => {
  const res = await fetchMedications(input);
  if (res?.error) {
    return { data: null, error: res.error, loading: false };
  }
  const listMedications: MedicationModel[] = res.data;
  const details: MedicationModel = listMedications.find(
    it => it.id === input.medicationId,
  );

  if (!details) {
    console.error('Detalhes do medicamento não encontrados');
    return {
      data: null,
      error: 'Detalhes do medicamento não encontrados',
      loading: false,
    };
  }
  return { data: details, error: false, loading: false };
};
