import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import { LocalDataSource } from '@app/data';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import Container from 'typedi';

const opCode: string = Container.get(GAMA_OP_CODE);
const simulatingCode: string = Container.get(GAMA_STIMULATING_CODE);
const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const proceduresRequest = async () => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const res = await fetchProcedures(beneficiaryCode);
  if (res.error) {
    return { data: null, error: res.error };
  }
  if (!res.data) {
    return { data: [], error: null };
  }
  const procedures =
    res.data
      ?.filter(it => it.ativo)
      .map(procedureRes => ({
        id: procedureRes.codigo && procedureRes.codigo.toString(),
        display: procedureRes.descricao,
        detail: procedureRes.observacao,
        registeredAt: procedureRes.data && new Date(procedureRes.data),
      })) || [];
  return { data: procedures, error: false };
};

const fetchProcedures = async (beneficiaryCode: string) => {
  const path = `${opCode}/${simulatingCode}/${beneficiaryCode}`;
  let res;
  try {
    res = await axiosGet({
      baseUrl: BaseUrl.GAMA_FUSION_GPS,
      url: `/beneficiario/dados-clinicos/cirurgias/${path}`,
    });
  } catch (e) {
    res = { data: null, error: e };
  }
  return res;
};
