import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import { LocalDataSource } from '@app/data';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import Container from 'typedi';

const opCode: string = Container.get(GAMA_OP_CODE);
const simulatingCode: string = Container.get(GAMA_STIMULATING_CODE);
const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export interface FamilyHistoryModel {
  id: string;
  display: string;
  relationship?: any;
  registerDate?: Date;
}

export const execFamilyHistoryRequest = async () => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const relationships: { data: { [id: string]: string }; error: any } =
    await relationshipRequest();
  const familyHistory: { data: FamilyHistoryModel[]; error: any } =
    await familyHistoryRequest(beneficiaryCode);

  if (relationships.error || familyHistory.error) {
    return { data: null, error: true };
  }
  const setRelationshipText = (familyCondition: FamilyHistoryModel) =>
    (familyCondition.relationship.description =
      relationships.data[familyCondition.relationship.id]);

  familyHistory.data.forEach(setRelationshipText);
  return { data: familyHistory.data, error: null };
};

export const familyHistoryRequest = async (beneficiaryCode: string) => {
  const res = await fetchFamilyHistory(beneficiaryCode);
  if (res.error) {
    return { data: [], error: true };
  }
  if (!res?.data?.length) {
    return { data: [], error: null };
  }
  let response = res.data
    .filter(it => it.ativo)
    .map(procedureRes => ({
      id: procedureRes.codigo && procedureRes.codigo.toString(),
      display: procedureRes.descricao,
      relationship: {
        id: procedureRes.codigoGrauParentesco.toString(),
        description: null,
      },
      registerDate: procedureRes.data && new Date(procedureRes.data),
    }));
  return { data: response, error: null };
};

export const relationshipRequest = async (): Promise<{
  data: { [id: string]: string };
  error: any;
}> => {
  let res = await fetchRelationship();
  if (res?.error) {
    return { data: null, error: res.error };
  }
  if (!res?.data?.length) {
    return { data: null, error: null };
  }
  const relationshipMap: { [id: string]: string } = {};
  res.data.forEach(
    relationshipRes =>
      (relationshipMap[relationshipRes.value.toString()] =
        relationshipRes.text),
  );

  return { data: relationshipMap, error: null };
};

const fetchFamilyHistory = async (beneficiaryCode: string) => {
  const path = `${opCode}/${simulatingCode}/${beneficiaryCode}`;
  let res;
  try {
    res = await axiosGet({
      url: `/beneficiario/dados-clinicos/saude-familiar/${path}`,
      baseUrl: BaseUrl.GAMA_FUSION_GPS,
    });
  } catch (e) {
    res = { data: null, error: e };
  }
  return { data: res.data || [], error: null };
};

const fetchRelationship = async () => {
  let res;
  try {
    res = await axiosGet({
      url: `/grau-parentesco/ativos/${opCode}`,
      baseUrl: BaseUrl.GAMA_FUSION_GPS,
    });
  } catch (e) {
    res = { data: null, error: e };
  }
  return { data: res.data || [], error: null };
};
