import { fetchItens } from '@app/components/org.care-plan-events/api/care-plan-events.service';
import { CarePlanItemModel } from '@app/modules/home/api/care-plan/care-plan.model';

export const fetchPastEvents = async (
  input: any,
): Promise<CarePlanItemModel[]> => {
  const res = await fetchItens(input);
  if (res?.error) {
    return null;
    // return { data: null, error: res.error };
  }

  const events: CarePlanItemModel[] = res.data;

  const filterDone = (event: CarePlanItemModel) => !!event.schedule.doneAt;
  const filterExpired = (event: CarePlanItemModel) =>
    new Date() < event.schedule.limitDate;

  return events.filter(filterDone).filter(filterExpired);

  // return {
  //   data: events.filter(filterDone).filter(filterExpired),
  //   error: false,
  // };
};
