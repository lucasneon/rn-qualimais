import { LocalDataSource } from '@app/data';
import {
  axiosGet,
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import {
  GAMA_OP_CODE,
  GAMA_STIMULATING_CODE,
  GAMA_SYSTEM_ORIGIN,
} from '@app/config';
import Container from 'typedi';
import { MedicalRecordRequest } from '../allergies/allergies.model';
import { InputParams } from '@app/model/api/input.params.model';
import { MedicalRecordResponse, VaccineModel } from './vaccines.model';
import moment from 'moment';

const opCode: string = Container.get(GAMA_OP_CODE);
const stimulatingCode: string = Container.get(GAMA_STIMULATING_CODE);
const systemOrigin: string = Container.get(GAMA_SYSTEM_ORIGIN);

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const vaccinesRequest = async () => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const result = await fetchVaccines(beneficiaryCode);
  if (!result?.data || result?.error) {
    return { data: null, error: result.error };
  }
  return result.data.filter(it => it.ativo === 'S').map(mapVaccine);
};

export const fetchVaccines = async (beneficiaryCode: string) => {
  let res;
  const path = `${opCode}/${stimulatingCode}/${beneficiaryCode}`;
  try {
    res = await axiosGet({
      url: `/beneficiario/dados-clinicos/vacinas/${path}`,
      baseUrl: BaseUrl.GAMA_FUSION_GPS,
    });
  } catch (e) {
    res = { data: null, error: e };
  }
  return res;
};

export const addVaccine = async (input: {
  data: InputParams<VaccineModel>;
}): Promise<any> => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  // const id_pweb: string = await localDataSource.get<string>('id_pweb');
  let takenAt = moment(input.data.takenAt).toISOString();
  const request: MedicalRecordRequest = {
    codigoOperadora: opCode,
    codigoEstipulante: stimulatingCode,
    codigoBeneficiario: beneficiaryCode,
    ativo: 'S',
    data: takenAt,
    descricao: input.data.display,
    sistemaOrigem: systemOrigin,
    // codigoUsuario: id_pweb,
  };
  const res = await postVaccineRequest(request);
  if (res.error) {
    throw new Error(res.error);
  }
  return { data: mapVaccine(res.data), error: null };
};

export const postVaccineRequest = async (
  vaccineRequest: MedicalRecordRequest,
) => {
  let res;
  try {
    res = await axiosPost({
      url: '/fusion-gps/api/beneficiario/dados-clinicos/vacinas',
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
      data: vaccineRequest,
    });
  } catch (e) {
    console.warn('error,', e);
    return { data: null, error: e };
  }
  return res;
};

function mapVaccine(data: MedicalRecordResponse): VaccineModel {
  return {
    __typename: 'Vaccines',
    display: data.descricao,
    takenAt: new Date(data.data),
  };
}
