import { MedicalRecordRequest } from '../allergies/allergies.model';

export interface MedicalRecordResponse extends MedicalRecordRequest {
  codigo: number;
  dataAlteracao: string;
}

export interface VaccineModel {
  display: string;
  takenAt: Date;
  __typename: string;
}
