import {
  DateFormatOptions,
  GamaDateFormatter,
} from '@app/model/api/common.mapper';
import { InputParams } from '@app/model/api/input.params.model';
import {
  AlternativeModel,
  AnswerQuestionnaireInputModel,
  QuestionGroupInputModel,
  QuestionGroupModel,
  QuestionInputModel,
  QuestionModel,
  QuestionnaireConditionModel,
  QuestionnaireModel,
  QuestionTypeModel,
} from '@app/modules/home/api/questionnaire.model';
import {
  AnswerQuestionnaireRequest,
  AnswerResponse,
  AnswersResponse,
  QuestionGroupRemote,
  QuestionGroupResponse,
  QuestionnaireParams,
  QuestionnaireResponse,
  QuestionRemote,
  QuestionResponse,
  QuestionRuleResponse,
  VisualizationRuleResponse,
} from './campaign-questionnaires.entity';
import {
  AnswerModel,
  UserQuestionnaireInputModel,
} from './campaign-questionnaires.model';

export const mapParams = (
  input: UserQuestionnaireInputModel,
): QuestionnaireParams => {
  return {
    beneficiaryCode: input.beneficiaryCode,
    campaignCode: input.campaignCode,
    questionnaireCode: input.questionnaireCode,
  };
};

export const mapQuestionarie = (
  response: QuestionnaireResponse,
): QuestionnaireModel => {
  return {
    code: response.codigo.toString(),
    name: response.descricao,
    isCompletelyAnswered: null,
    required:
      response.tipoQuestionario && response.tipoQuestionario === 'PRINCIPAL',
    responseCode: null,
    lastAnsweredAt: null,
    initialText: response.textoInicial,
    partialText: response.textoParcial,
    finalText: response.textoFinal,
    questionGroups: response.grupoQuestoes.map(mapQuestionGroup),
    imcRequired: response.perguntarIMC,
  };
};

function mapQuestionGroup(response: QuestionGroupResponse): QuestionGroupModel {
  return {
    code: response.codigo.toString(),
    name: response.descricao,
    questions: mapQuestions(response.questoes, response.regraVisualizacoes),
  };
}

function mapQuestionAnswersGroup(questionGroupRes: QuestionGroupRemote) {
  return {
    code:
      questionGroupRes.codigoGrupoQuestao &&
      questionGroupRes.codigoGrupoQuestao.toString(),
    name: null,
    questions:
      questionGroupRes.questoes &&
      questionGroupRes.questoes.map(mapAnwersQuestions),
  };
}

const mapAnwersQuestions = (questionRes: QuestionRemote): QuestionModel => {
  const answerRes = questionRes.resposta;
  let chosenAnswers: AnswerModel[];
  if (answerRes.resposta) {
    chosenAnswers = [{ display: answerRes.resposta }];
  } else if (answerRes.codigoRespostas) {
    chosenAnswers = answerRes.codigoRespostas.map(it => ({
      code: it.toString(),
    }));
  }
  return {
    code: questionRes.codigoQuestao.toString(),
    chosenAnswers,
  } as QuestionModel;
};

function mapQuestions(
  questionsRes: QuestionResponse[],
  rules: VisualizationRuleResponse[],
): QuestionModel[] {
  const questions: QuestionModel[] = questionsRes.map(res => ({
    code: res.codigo.toString(),
    description: res.descricao,
    descritive: res.descritivos && res.descritivos.map(it => it.texto),
    minValue: res.barraDeslizante && res.barraDeslizante.minimo,
    maxValue: res.barraDeslizante && res.barraDeslizante.maximo,
    required: res.obrigatorio,
    type:
      res.tipoResposta &&
      (questionTypeMap[res.tipoResposta.chave] || QuestionTypeModel.Textual),
    alternatives: res.respostas ? res.respostas.map(mapAlternatives) : [],
    conditionToShow: [],
  }));

  mapConditionsToShow(questions, rules);

  return questions;
}

const questionTypeMap: { [key in string]: QuestionTypeModel } = {
  CHECKBOX: QuestionTypeModel.MultipleChoices,
  RADIO_BTN: QuestionTypeModel.OneChoice,
  TEXTO_NUM: QuestionTypeModel.Number,
  TEXTO_TXT: QuestionTypeModel.Textual,
  TEXTO_DATE: QuestionTypeModel.Date,
  COMBO_BOX: QuestionTypeModel.OneChoice,
  SLIDINGBAR: QuestionTypeModel.Number,
  BARRA_DESL: QuestionTypeModel.Number,
};

function mapConditionsToShow(
  questions: QuestionModel[],
  rules: VisualizationRuleResponse[],
) {
  while (rules.length > 0) {
    const rule: VisualizationRuleResponse = rules.pop();
    const questionRule: QuestionRuleResponse = rule.regraQuestao;
    const linkedQuestionCode: string =
      questionRule.regraRespostasDTO.codigoQuestaoVinculada.toString();
    const linkedAlternative: number =
      questionRule.regraRespostasDTO.codigoRespostaVinculada;
    const conditionToShow: QuestionnaireConditionModel = {
      questionCode: questionRule.codigoQuestao.toString(),
      answerCode: questionRule.codigoResposta.toString(),
    };

    const question: QuestionModel = questions.find(
      it => it.code === linkedQuestionCode,
    );

    if (linkedAlternative) {
      const linkedAlternativeCode: string = linkedAlternative.toString();
      const answer: AlternativeModel = question.alternatives.find(
        it => it.code === linkedAlternativeCode,
      );
      answer.conditionToShow.push(conditionToShow);
    } else if (question) {
      question.conditionToShow.push(conditionToShow);
    }
  }
}

function mapAlternatives(response: AnswerResponse): AlternativeModel {
  return {
    code: response.codigo.toString(),
    display: response.descricao,
    conditionToShow: [],
  };
}

export const includeAnswersProgress = (questionGroup: QuestionGroupModel) => {
  const questions: QuestionModel[] = questionGroup.questions;
  questionGroup.totalAnswered = questions.filter(it => {
    return it.chosenAnswers && it.chosenAnswers.length > 0;
  }).length;
  questionGroup.minimumToAnswer = questions.filter(
    it => it.required && !it.conditionToShow.length,
  ).length;
};

export const questionnaireAnswerMap = (response: AnswersResponse) => {
  return {
    responseCode: response.codigoGabarito && response.codigoGabarito.toString(),
    weight: response.imc && response.imc.peso && +response.imc.peso,
    height: response.imc && response.imc.altura && +response.imc.altura,
    lastAnsweredAt:
      response.dataAtualizacao && new Date(response.dataAtualizacao),
    questionGroups:
      response.gruposQuestoes &&
      response.gruposQuestoes.map(mapQuestionAnswersGroup),
  };
};

export const mergeQuestionsAnwers = (
  questionnaire: QuestionnaireModel,
  answers: Partial<QuestionnaireModel>,
): QuestionnaireModel => {
  if (answers && answers.questionGroups) {
    questionnaire.responseCode = answers.responseCode;
    questionnaire.lastAnsweredAt = answers.lastAnsweredAt;
    questionnaire.height = answers.height;
    questionnaire.weight = answers.weight;

    answers.questionGroups.forEach(answerGroup => {
      const questionGroup = questionnaire.questionGroups.find(
        it => it.code === answerGroup.code,
      );
      if (questionGroup) {
        insertAnswers(questionGroup, answerGroup);
      }
    });
  }

  return questionnaire;
};

function insertAnswers(
  questionGroup: QuestionGroupModel,
  answerGroup: QuestionGroupModel,
) {
  answerGroup.questions.forEach(answer => {
    const question = questionGroup.questions.find(
      it => it.code === answer.code,
    );

    if (question) {
      question.chosenAnswers = answer.chosenAnswers;
    }
  });
}

export const mapRequest = (
  input: InputParams<AnswerQuestionnaireInputModel>,
): AnswerQuestionnaireRequest => {
  console.warn('input:::', input);
  const questionnaireInput: AnswerQuestionnaireInputModel = input;
  const dateOptions: DateFormatOptions = {
    dayHourSeparator: ' ',
    includeHours: true,
    considerOnlyDay: true,
  };

  const mapTypedAnswer = (question: QuestionInputModel): string => {
    if (question.type === QuestionTypeModel.Date) {
      return GamaDateFormatter.format(question.typedAnswer, dateOptions);
    } else {
      return question.typedAnswer;
    }
  };

  const questionTypeMapKeys: string[] = Object.keys(questionTypeMap);

  const mapQuestionRequest = (
    question: QuestionInputModel,
  ): QuestionRemote => ({
    codigoQuestao: question.code,
    resposta: {
      codigoRespostas: question.chosenAnswers,
      resposta: mapTypedAnswer(question),
    },
    tipoResposta: {
      chave: questionTypeMapKeys.find(
        it => questionTypeMap[it] === question.type,
      ),
    },
  });

  const mapQuestionGroupRequest = (
    questionGroup: QuestionGroupInputModel,
  ): QuestionGroupRemote => ({
    codigoGrupoQuestao: questionGroup.code,
    questoes: questionGroup.questions.map(mapQuestionRequest),
  });

  const weight =
    questionnaireInput.weight && questionnaireInput.weight.toString();
  const height =
    questionnaireInput.height && questionnaireInput.height.toString();

  return {
    codigoOperadora: null,
    codigoEstipulante: null,
    codigoBeneficiario: null,
    codigoCampanha: questionnaireInput.campaignCode,
    codigoQuestionario: questionnaireInput.questionnaireCode,
    grupoQuestoes: questionnaireInput.questionGroups.map(
      mapQuestionGroupRequest,
    ),
    imc: { peso: weight, altura: height },
  };
};
