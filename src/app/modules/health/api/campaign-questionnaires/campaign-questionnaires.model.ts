export interface UserQuestionnaireInputModel {
  beneficiaryCode: string;
  campaignCode: string;
  questionnaireCode: string;
  responseCode?: string;
}

export interface QuestionnaireModel extends QuestionnaireSummaryModel {
  initialText?: string;
  partialText?: string;
  finalText?: string;
  responseCode?: string;
  lastAnsweredAt?: Date;
  questionGroups: QuestionGroupModel[];
  imcRequired: boolean;
  weight?: number;
  height?: number;
}

export interface QuestionnaireSummaryModel {
  code: string;
  name: string;
  responseCode?: string;
  isCompletelyAnswered?: boolean;
  required?: boolean;
}

export interface QuestionGroupModel {
  code: string;
  name: string;
  totalAnswered?: number;
  minimumToAnswer?: number;
  questions: QuestionModel[];
}

export interface QuestionModel {
  code: string;
  description: string;
  descritive?: string[];
  minValue?: number;
  maxValue?: number;
  required: boolean;
  type: QuestionTypeModel;
  alternatives: AlternativeModel[];
  chosenAnswers?: AnswerModel[];
  conditionToShow?: QuestionnaireConditionModel[];
}

export interface QuestionnaireConditionModel {
  questionCode: string;
  answerCode: string;
}

export enum QuestionTypeModel {
  Date = 'Date',
  MultipleChoices = 'MultipleChoices',
  Number = 'Number',
  OneChoice = 'OneChoice',
  Textual = 'Textual',
}

export interface AlternativeModel {
  code: string;
  display?: string;
  conditionToShow?: QuestionnaireConditionModel[];
}

export interface AnswerModel {
  code?: string;
  display?: string;
}

export interface AnswerQuestionnaireModel {
  responseCode: string;
}
