import {
  BaseCarePlanRequest,
  GeneralTypeRemote,
} from '@app/model/api/common.entity';

export interface QuestionnaireParams {
  beneficiaryCode: string;
  campaignCode: string;
  questionnaireCode: string;
  responseCode?: string;
}

export interface QuestionnaireResponse {
  codigo: number;
  descricao: string;
  paginacao: number;
  tipoQuestionario?: string;
  textoInicial: string;
  textoFinal: string;
  textoParcial: string;
  perguntarIMC: boolean;
  guiaQ: boolean;
  podeResponder: boolean;
  podeEnviarEmail: boolean;
  grupoQuestoes: QuestionGroupResponse[];
}

export interface QuestionGroupResponse {
  codigo: number;
  descricao: string;
  questoes: QuestionResponse[];
  regraVisualizacoes: VisualizationRuleResponse[];
}

export interface QuestionGroupResponse {
  codigo: number;
  descricao: string;
  questoes: QuestionResponse[];
  regraVisualizacoes: VisualizationRuleResponse[];
}

export interface QuestionResponse {
  codigo: number;
  descricao: string;
  descritivos?: Descritive[];
  observacao?: string;
  tipoResposta: AnswerTypeResponse;
  barraDeslizante: AnswerLimitsResponse;
  obrigatorio: boolean;
  respostas: AnswerResponse[];
}

export interface VisualizationRuleResponse {
  codigoRegraVisualizacao: number;
  regraQuestao: QuestionRuleResponse;
}

export interface Descritive {
  sexo: GeneralTypeRemote;
  texto: string;
}

export interface AnswerResponse {
  codigo: number;
  descricao: string;
  idadeMinima?: number;
  idadeMaxima?: number;
  sexo: AnswerTypeResponse;
}

export interface AnswerTypeResponse {
  codigo?: number;
  chave: string;
  descricao?: string;
}

export interface AnswerLimitsResponse {
  minimo?: number;
  maximo?: number;
  intervalo?: number;
}

export interface QuestionRuleResponse {
  codigoQuestao: number;
  codigoResposta: number;
  regraRespostasDTO: AnswerRuleResponse;
}

export interface AnswerRuleResponse {
  codigoQuestaoVinculada: number;
  codigoRespostaVinculada?: number;
}

export interface AnswersResponse extends AnswerQuestionnaireRequest {
  origemResposta: string;
  usuario: number;
  dataAtualizacao: string;
  codigoGabarito: number;
  gruposQuestoes: QuestionGroupRemote[];
}

export interface AnswerQuestionnaireRequest extends BaseCarePlanRequest {
  codigoCampanha: string;
  codigoQuestionario: string;
  grupoQuestoes: QuestionGroupRemote[];
  imc?: ImcRequest;
}

export interface QuestionGroupRemote {
  codigoGrupoQuestao: string;
  questoes: QuestionRemote[];
}

export interface QuestionRemote {
  codigoQuestao: string;
  resposta: AnswerRequest;
  tipoResposta: QuestionTypeRequest;
}

export interface ImcRequest {
  altura: string;
  peso: string;
}

export interface AnswerRequest {
  codigoRespostas: string[];
  resposta?: string;
}

export interface QuestionTypeRequest {
  chave: string;
}
