import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import Container from 'typedi';
import { LocalDataSource } from '@app/data';
import {
  axiosGet,
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import {
  AnswerQuestionnaireModel,
  UserQuestionnaireInputModel,
} from './campaign-questionnaires.model';
import {
  includeAnswersProgress,
  mapParams,
  mapQuestionarie,
  mapRequest,
  mergeQuestionsAnwers,
  questionnaireAnswerMap,
} from './questionnaire.mapper';
import {
  AnswerQuestionnaireInputModel,
  QuestionnaireModel,
} from '@app/modules/home/api/questionnaire.model';
import {
  AnswerQuestionnaireRequest,
  QuestionnaireParams,
} from './campaign-questionnaires.entity';
import { CampaignQuestionnairesInput } from '@app/data/graphql';
import { InputParams } from '@app/model/api/input.params.model';
import { CustomError } from '@app/modules/base-error';

const opCode: string = Container.get(GAMA_OP_CODE);
const simulatingCode: string = Container.get(GAMA_STIMULATING_CODE);
const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const execGetQuestionnaires = async (
  input: CampaignQuestionnairesInput,
): Promise<any> => {
  // TODO questionaries
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );

  const res = await Promise.all(
    input.questionnaires.map(questionnaire =>
      fetchQuestionnaire({
        beneficiaryCode: beneficiaryCode,
        campaignCode: input.campaignCode,
        questionnaireCode: questionnaire.questionnaireCode,
        responseCode: questionnaire.responseCode,
      }),
    ),
  );
  console.warn('fetchQuestionnaireRes', res);
  return res;
};

export const fetchQuestionnaire = async (
  input: UserQuestionnaireInputModel,
) => {
  const res = await questionnaireRequest(mapParams(input));
  console.warn('Retorno Questionarios', res.data.questionario);
  let answers;
  if (input.responseCode) {
    answers = await fecthAnswers(input);
    console.warn('resultado answers', answers);
  }
  let questionnaire: QuestionnaireModel;
  if (res.error || !res.data.questionario || !res.data.questionario.codigo) {
    return { data: null, error: 'Not found' };
  }
  let answeredQuestionnaire;
  try {
    questionnaire = mapQuestionarie(res.data.questionario);
    answeredQuestionnaire = mergeQuestionsAnwers(questionnaire, answers?.data);
    console.warn('answeredQuestionnaire@', answeredQuestionnaire);
    answeredQuestionnaire.questionGroups.forEach(includeAnswersProgress);
  } catch (error) {
    return {
      data: null,
      error: 'Error mapping questionnaires: ' + error.toString(),
    };
  }
  console.warn('answeredQuestionnaire', answeredQuestionnaire);
  return answeredQuestionnaire;
};

const questionnaireRequest = async (params: QuestionnaireParams) => {
  const path = `${params.beneficiaryCode}/${params.campaignCode}/${params.questionnaireCode}`;
  let res;
  try {
    res = await axiosGet({
      url: `/fusion-gestao-cuidado/api/questionario/${opCode}/${simulatingCode}/${path}`,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    return { data: null, error: e };
  }
  return res;
};

export const fecthAnswers = async (input: any) => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const res = await anwserRequest({ ...input, beneficiaryCode });
  if (res.error) {
    return { data: null, error: res.error };
  }
  let answers;
  try {
    answers = questionnaireAnswerMap(res.data.gabarito);
  } catch (e) {
    return { data: null, error: e };
  }
  return { data: answers, error: null };
};

const anwserRequest = async (input: QuestionnaireParams) => {
  const path = `${input.beneficiaryCode}/${input.campaignCode}/${input.questionnaireCode}/${input.responseCode}`;
  let res;
  try {
    res = await axiosGet({
      url: `/fusion-gestao-cuidado/api/campanha/questionario/${opCode}/${simulatingCode}/${path}`,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    }).catch(e => {
      return { data: null, error: e };
    });
  } catch (e) {
    return { data: null, error: e };
  }
  return res;
};

export const answerQuestionnaire = async (
  params: InputParams<any | AnswerQuestionnaireInputModel>,
): Promise<AnswerQuestionnaireModel> => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const request: AnswerQuestionnaireRequest = {
    ...mapRequest(params),
    codigoOperadora: opCode,
    codigoEstipulante: simulatingCode,
    codigoBeneficiario: beneficiaryCode,
  };
  const res = await answersRequest(request);
  const responseCode: number =
    res.data && res.data.gabarito && res.data.gabarito.codigoGabarito;

  if (!responseCode) {
    throw new CustomError(
      'app.global.error.internal',
      500,
      'Could not get responseCode.',
    );
  }

  return { responseCode: responseCode.toString() };
};

const answersRequest = async (data: AnswerQuestionnaireRequest) => {
  let res;
  try {
    res = await axiosPost({
      data,
      url: '/fusion-gestao-cuidado/api/campanha/questionario',
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    return { data: null, error: e };
  }
  return res;
};
