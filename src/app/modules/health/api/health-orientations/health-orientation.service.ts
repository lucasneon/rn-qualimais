import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import { LocalDataSource } from '@app/data';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { CampaignModel } from '@app/modules/home/api/campaings';
import { fetchCampaings } from '@app/modules/home/api/campaings/campaings.service';
import Container from 'typedi';
import { QuestionnaireParams } from '../campaign-questionnaires/campaign-questionnaires.entity';
import { UserQuestionnaireInputModel } from '../campaign-questionnaires/campaign-questionnaires.model';
import {
  AnsweredQuestionnaireModel,
  QuestionnaireFeedbackModel,
} from './health-orientation.model';
import {
  getAnsweredQuestionnaires,
  mapParamsQuestionnaire,
  mapQuestionnaireFeedback,
} from './health.orientatios.mapper';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);
const opCode: string = Container.get(GAMA_OP_CODE);
const stimulantingCode: string = Container.get(GAMA_STIMULATING_CODE);

export const fetchQuestionaryFeedback = async () => {
  const campaigns: CampaignModel[] = await fetchCampaings();
  const answeredQuestionnaires: AnsweredQuestionnaireModel[] =
    getAnsweredQuestionnaires(campaigns);

  if (!answeredQuestionnaires || answeredQuestionnaires.length === 0) {
    return [];
  }

  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  return Promise.all(
    answeredQuestionnaires.map(async questionnaire => {
      const input: UserQuestionnaireInputModel = {
        beneficiaryCode,
        campaignCode: questionnaire.campaignCode,
        questionnaireCode: questionnaire.code,
      };

      const feedback: QuestionnaireFeedbackModel = await fetchFeedback(input);
      return (
        feedback || {
          questionnaireName: questionnaire.name,
          responseCode: questionnaire.responseCode,
          items: [],
        }
      );
    }),
  );
};

const fetchFeedback = async (
  input: UserQuestionnaireInputModel,
): Promise<QuestionnaireFeedbackModel> => {
  const res = await feedbackRequest(mapParamsQuestionnaire(input));
  if (res.error) {
    throw new Error(res.error);
  }
  return (
    res.data &&
    res.data.questionario &&
    mapQuestionnaireFeedback(res.data.questionario)
  );
};

const feedbackRequest = async (params: QuestionnaireParams) => {
  const path = `${params.beneficiaryCode}/${params.campaignCode}/${params.questionnaireCode}`;
  let codesPath = `${opCode}/${stimulantingCode}`;
  let res;
  try {
    res = await axiosGet({
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
      url: `/fusion-gestao-cuidado/api/devolutiva/${codesPath}/${path}`,
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};
