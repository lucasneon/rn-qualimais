import { QuestionnaireSummaryModel } from '../campaign-questionnaires/campaign-questionnaires.model';

export interface AnsweredQuestionnaireModel extends QuestionnaireSummaryModel {
  campaignCode: string;
}

export interface QuestionnaireFeedbackModel {
  questionnaireName?: string;
  responseCode?: string;
  lastAnsweredAt?: Date;
  items: QuestionnaireFeedbackItemModel[];
}

export interface QuestionnaireFeedbackItemModel {
  topic?: QuestionnaireFeedbackTopicModel;
  description: string;
}

export interface QuestionnaireFeedbackTopicModel {
  title?: string;
  id?: string;
  reference?: TopicReferenceModel;
}

export interface QuestionnaireFeedbackResponse {
  nome: string;
  resultadoDevolutivaItems: QuestionnaireFeedbackResponseItem[];
}

export interface QuestionnaireFeedbackResponseItem {
  id: number;
  descricao: string;
  imagemId?: any;
  imagem?: any;
}


export enum TopicReferenceModel {
  GeneralHealth = 'GeneralHealth',
  Weight = 'Weight',
  SelfCare = 'SelfCare',
  EmotionalHealth = 'EmotionalHealth',
  Nutrition = 'Nutrition',
  Habits = 'Habits',
  Smoking = 'Smoking',
  Exercise = 'Exercise',
}
