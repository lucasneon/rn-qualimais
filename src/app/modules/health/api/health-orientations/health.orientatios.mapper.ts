import { CampaignModel } from '@app/modules/home/api/campaings';
import { QuestionnaireParams } from '../campaign-questionnaires/campaign-questionnaires.entity';
import { UserQuestionnaireInputModel } from '../campaign-questionnaires/campaign-questionnaires.model';
import { AnsweredQuestionnaireModel, QuestionnaireFeedbackItemModel, QuestionnaireFeedbackModel, QuestionnaireFeedbackResponse, QuestionnaireFeedbackResponseItem } from './health-orientation.model';

export const getAnsweredQuestionnaires = (
  campaigns: CampaignModel[],
): AnsweredQuestionnaireModel[] => {
  const filterAnsweredQuestionnaires = (
    campaign: CampaignModel,
  ): AnsweredQuestionnaireModel[] => {
    return campaign.questionnaires
      .filter(it => it.isCompletelyAnswered)
      .map(it => ({ ...it, campaignCode: campaign.code }));
  };

  return campaigns.reduce<AnsweredQuestionnaireModel[]>(
    (_, campaign: CampaignModel) => filterAnsweredQuestionnaires(campaign),
    [],
  );
};

export const mapParamsQuestionnaire = (
  input: UserQuestionnaireInputModel,
): QuestionnaireParams => {
  return {
    beneficiaryCode: input.beneficiaryCode,
    campaignCode: input.campaignCode,
    questionnaireCode: input.questionnaireCode,
  };
};

export const mapQuestionnaireFeedback = (
  response: QuestionnaireFeedbackResponse,
): QuestionnaireFeedbackModel => {
  const mapItem = (
    itemRes: QuestionnaireFeedbackResponseItem,
  ): QuestionnaireFeedbackItemModel => ({
    topic: { id: itemRes.id && itemRes.id.toString() },
    description: itemRes.descricao,
  });

  return {
    questionnaireName: response.nome,
    items: response.resultadoDevolutivaItems.map(mapItem),
  };
};
