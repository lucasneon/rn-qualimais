import { mapVitalSignsRequest, VitalSignsMap } from './vital-sign.mapper';
import {
  axiosGet,
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { VitalSignInput, VitalSignType } from '@app/data/graphql';
import Container from 'typedi';
import { PRONTLIFE_ACCESS_TOKEN } from '@app/config';
import { LocalDataSource } from '@app/data';
import { VitalSignsRequestParams } from './vital-sign.entity';
import { VitalSignsMapper } from '@app/modules/common/vital-signs';
import { requestProntlifeHeaders } from '@app/utils/prontilife.utils';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services';
import { InputParams } from '@app/model/api/input.params.model';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);
const prontlifeToken: string = Container.get(PRONTLIFE_ACCESS_TOKEN);
const Category = 'vital-signs';
const beneficiaryProvider: BeneficiaryProvider = Container.get(
  BeneficiaryProviderToken,
);
export type RecentVitalSignsParams = InputParams<{
  types: VitalSignType[];
}>;

export const execLastMesure = async (input: VitalSignType[]) => {
  const fetchLastMeasure = async (vitalSign: VitalSignType) => {
    const measures = await fetchVitalSign(vitalSign, 1);
    return measures.length > 0
      ? measures[0]
      : { type: VitalSignsMap[vitalSign], measuredAt: null, measureUnit: '' };
  };

  return Promise.all(input.map(fetchLastMeasure));
};

export const fetchVitalSign = async (
  vitalSign: VitalSignType,
  count?: number,
) => {
  // const cpf = '23687244008'; //beneficiaryProvider.state.active.idHealthCareCard;
  const cpf: string = await localDataSource.get<string>('cpf');
  const params: VitalSignsRequestParams = {
    'subject:Patient.identifier': `/person/CPF|${cpf}`,
    category: Category,
    code: VitalSignsMap[vitalSign],
    _count: count,
  };

  const res = await vitalSignsRequest(params);
  if (res.error || res.data.total === 0) {
    return [];
  }
  return VitalSignsMapper.mapResponse(res.data);
};

const vitalSignsRequest = async (params: VitalSignsRequestParams) => {
  let res;
  try {
    res = await axiosGet({
      params,
      url: '/fhir/Observation',
      baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
      headers: requestProntlifeHeaders(prontlifeToken),
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};

export const insertVitalSigns = async (vitalSign: VitalSignInput[]) => {
  // const appointmentId = '95099';
  const appointmentId: string = await localDataSource.get<string>(
    'appointmentId',
  );
  const data = mapVitalSignsRequest(vitalSign, appointmentId);
  const res = await postVitalSignRequest(data);
  if (res.error) {
    throw new Error(res.error);
  }
  return res;
};
const postVitalSignRequest = async data => {
  let res;
  try {
    res = await axiosPost(
      {
        baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
        url: '/fhir/Observation',
        data,
      },
      requestProntlifeHeaders(prontlifeToken),
    );
  } catch (error) {
    return { data: null, error };
  }
  return res;
};
