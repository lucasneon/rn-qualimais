import { VitalSignInput } from '@app/data/graphql';
import moment from 'moment';
import {
  VitalSignMeasureModel,
  VitalSignRequest,
  VitalSignsComponentResponse,
  VitalSignsResponse,
  VitalSignTypeModel,
} from './vital-sign.model';

export const VitalSignsMap: { [vs in VitalSignTypeModel]: string } = {
  [VitalSignTypeModel.Imc]: 'imc',
  [VitalSignTypeModel.imc]: 'imc',
  [VitalSignTypeModel.BloodPressure]: 'pressao_arterial',
  [VitalSignTypeModel.Weight]: 'peso',
  [VitalSignTypeModel.Height]: 'altura',
  [VitalSignTypeModel.Glycemia]: 'hgt',
  [VitalSignTypeModel.DiastolicPressure]: 'pressao_diastolica',
  [VitalSignTypeModel.MeanPressure]: 'pressao_media',
  [VitalSignTypeModel.SystolicPressure]: 'pressao_sistolica',
};

export const mapVitalSignsRequest = (
  input: VitalSignInput[],
  prontlifeId: string,
): VitalSignRequest => {
  const mapSubMeasures = (
    measure: VitalSignMeasureModel,
  ): VitalSignsComponentResponse[] => {
    return (
      measure.subMeasures &&
      measure.subMeasures.map(sub => ({
        code: { text: VitalSignsMap[sub.type] },
        valueQuantity: { value: sub.measure.amount, unit: sub.measure.unit },
      }))
    );
  };

  const mapMeasure = (measure: VitalSignMeasureModel): VitalSignsResponse => {
    return {
      resourceType: 'Observation',
      id: null,
      status: 'final',
      category: [
        {
          coding: [
            {
              system: 'http://hl7.org/fhir/observation-category',
              code: 'vital-signs',
            },
          ],
        },
      ],
      code: { text: VitalSignsMap[measure.type] },
      subject: { reference: 'Patient/' + prontlifeId },
      effectiveDateTime: moment(measure.measuredAt).format(),
      valueQuantity: measure.measure && {
        value: measure.measure.amount,
        unit: measure.measure.unit,
      },
      component: mapSubMeasures(measure),
    };
  };

  return {
    resourceType: 'Bundle',
    type: 'searchset',
    total: input.length,
    entry: input.map(it => ({ resource: mapMeasure(it) })),
  };
};
