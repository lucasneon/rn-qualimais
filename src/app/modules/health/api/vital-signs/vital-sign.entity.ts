export interface VitalSignsRequestParams {
  'subject:Patient.identifier': string;
  category: string;
  _count?: number;
  code: string;
}
