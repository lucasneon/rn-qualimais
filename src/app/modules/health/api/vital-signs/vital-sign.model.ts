import { MeasureModel } from '@app/utils/prontlife/common.model';
import {
  CodeRemote,
  EntryResponse,
  MeasureRemote,
  ReferenceRemote,
  ResourceResponse,
  TypeRemote,
} from '@app/utils/prontlife/prontlife.entity';

export interface VitalSignMeasureModel extends SingleVitalSignModel {
  measureUnit: string;
  subMeasures?: SingleVitalSignModel[];
}

export interface SingleVitalSignModel {
  type: VitalSignTypeModel;
  measuredAt?: Date;
  measure?: MeasureModel;
}

export interface VitalSignRequest {
  resourceType: string;
  type: string;
  total: number;
  entry: Array<Partial<EntryResponse<VitalSignsResponse>>>;
}

export interface VitalSignsResponse extends ResourceResponse {
  status: string;
  category: CodeRemote[];
  code: TypeRemote;
  subject: ReferenceRemote;
  effectiveDateTime: string;
  performer?: ReferenceRemote[];
  valueQuantity?: MeasureRemote;
  component?: VitalSignsComponentResponse[];
}

export interface VitalSignsComponentResponse {
  code: TypeRemote;
  valueQuantity?: MeasureRemote;
}

export enum VitalSignTypeModel {
  Imc = 'imc', //'Imc',
  imc = 'imc', //'Imc',
  BloodPressure = 'BloodPressure',
  Weight = 'peso', //'Weight',
  peso = 'peso', //'Weight',
  Height = 'altura', //'Height',
  altura = 'altura', //'Height',
  Glycemia = 'Glycemia',
  DiastolicPressure = 'DiastolicPressure',
  SystolicPressure = 'SystolicPressure',
  MeanPressure = 'MeanPressure',
}
