import { ApolloError } from 'apollo-client';
import { Container as TypeDIContainer, Service } from 'typedi';
import { Container } from 'unstated';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import {
  CarePlanItemFragment,
  PastEventsQuery,
  PastEventsQueryVariables,
} from '@app/data/graphql';
import { AppError } from '@app/model';
import { ErrorMapper } from '@app/utils';
import { fetchPastEvents } from './api/past-events/past-events.service';
import { CarePlanItemModel } from '../home/api/care-plan/care-plan.model';

export interface PastEventsProviderState {
  loading?: boolean;
  error?: AppError;
  events?: any;
}

export const PastEventsProviderKey = 'PastEventsProviderKey';

@Service(PastEventsProviderKey)
export class PastEventsProvider extends Container<PastEventsProviderState> {
  active: boolean;

  private readonly graphQLProvider: GraphQLProvider =
    TypeDIContainer.get(GraphQLProviderToken);

  constructor() {
    super();
    this.state = { loading: true, error: null, events: null };
  }

  async rehydrate() {
    if (!this.active) {
      return;
    }

    this.setState({ loading: true, error: null, events: null });

    const res: CarePlanItemModel[] = await fetchPastEvents({ data: {} });
    if (!res) {
      console.error('Past events: ', res);
      return this.setState({
        loading: false,
        error: ErrorMapper.map(res),
      });
    }

    const pastEvents = { PastEvents: res };

    this.handleSuccess(pastEvents);

    // TODO GRAPHQL
    // this.graphQLProvider.query<PastEventsQueryVariables>(
    //   'past-events',
    //   { data: {} },
    //   (res: PastEventsQuery) => this.handleSuccess(res),
    //   (error: ApolloError) =>
    //     this.setState({ loading: false, error: ErrorMapper.map(error) }),
    // );
  }
  private handleSuccess(res: any) {
    return this.setState({ loading: false, events: res && res.PastEvents });
  }
}
