import * as React from 'react';
import { Subscribe } from 'unstated';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { CampaignFragment } from '@app/data/graphql';
import { CarePlanProvider } from '@app/services/providers';
import { Campaign, CampaignsShimmer } from './campaign.component';
import { CampaignsStrings } from './campaigs.strings';
import { Navigation } from 'react-native-navigation';

@PageView('Campaigns')
export class CampaignsPage extends React.Component {
  static options = { name: 'CampaignsPage' };

  private campaignProvider: CarePlanProvider;

  render() {
    return (
      <CollapsibleHeader title={CampaignsStrings.Title}>
        <Subscribe to={[CarePlanProvider]}>
          {(campaignProvider: CarePlanProvider) => {
            this.campaignProvider = campaignProvider;
            const state = this.campaignProvider.state;
            const campaigns = state.campaigns;
            console.warn('campaings Care Plan', campaigns);

            return (
              <>
                {this.campaignProvider.state.error && (
                  <PlaceholderSelector
                    error={this.campaignProvider.state.error}
                  />
                )}
                {this.campaignProvider.state.loading && <CampaignsShimmer />}
                {campaigns &&
                  campaigns.length > 0 &&
                  campaigns.map(this.mapCampaigns)}
              </>
            );
          }}
        </Subscribe>
      </CollapsibleHeader>
    );
  }
  private mapCampaigns = (campaign: CampaignFragment) => (
    <Campaign
      key={campaign.code}
      campaignData={campaign}
      onAnswerSuccess={this.handleAnswerSuccess}
    />
  );

  private handleAnswerSuccess = () => {
    this.campaignProvider.rehydrate();
    FlashMessageService.showSuccess(CampaignsStrings.AnswerSuccess);
  };
}
