import * as React from 'react';
import { QuestionFragment, QuestionnaireConditionFragment } from '@app/data/graphql';
import { CheckboxField, Form, Validators } from '@atomic';

interface CheckboxQuestionProps {
  currentQuestion: QuestionFragment;
  onValueChange: (value: any) => void;
  areConditionsSatisfied: (condition: QuestionnaireConditionFragment[]) => boolean;
}

export class CheckboxQuestion extends React.Component<CheckboxQuestionProps> {

  componentDidMount() {
    if (this.props.currentQuestion.chosenAnswers) {
      this.handleSelectionChange(this.props.currentQuestion.chosenAnswers.map(ans => ans.code));
    }
  }

  render() {
    const { currentQuestion } = this.props;
    const validators = [];

    if (currentQuestion.required) {
      validators.push(Validators.Required('Campo obrigatório'));
    }

    const answers = currentQuestion.chosenAnswers;

    return (
      <Form.Field
        validators={validators}
        name='check'
        onValueChange={this.handleSelectionChange}
        value={answers && answers.length && answers.map(ans => ans.code) || []}
      >
        {currentQuestion.alternatives.map(answer => (
          this.props.areConditionsSatisfied(answer.conditionToShow) &&
          <CheckboxField key={answer.code} id={answer.code}>{answer.display}</CheckboxField>
        ))}
      </Form.Field>
    );
  }

  private handleSelectionChange = (values: any) => {
    this.props.onValueChange(values);
  }

}
