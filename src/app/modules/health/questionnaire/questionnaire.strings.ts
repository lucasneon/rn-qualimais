export const QuestionnaireStrings = {
  Finish: 'Finalizar',
  Previous: 'Anterior',
  Next: 'Próximo',
  Exit: {
    Title: 'Progresso do questionário',
    Message: 'Deseja salvar o progresso atual do questionário?',
    Save: 'Salvar e sair',
    Exit: 'Sair sem salvar',
  },
};
