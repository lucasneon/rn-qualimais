import { ApolloError } from 'apollo-client';
import _ from 'lodash';
import * as React from 'react';
import { Container } from 'typedi';
import { Subscribe } from 'unstated';
import {
  QuestionGroupCell,
  QuestionnaireCellShimmer,
} from '@app/components/mol.cell';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { Navigation } from '@app/core/navigation';
import {
  AnswerQuestionnaireMutation,
  CampaignFragment,
  QuestionGroupFragment,
  QuestionnaireFragment,
} from '@app/data/graphql';
import { CampaignQuestionnaireProvider } from '@app/services/providers/campaign-questionnaire.provider';
import { FontSize, H2, Shimmer, VBox, VSeparator } from '@atomic';
import { ImcQuestions } from './imc-questions.service';
import {
  QuestionGroupPage,
  QuestionGroupPageProps,
} from './question-group.page';

export interface CampaignProps {
  campaignData: CampaignFragment;
  onAnswerSuccess: () => void;
}

export const CampaignTitleShimmer = () => (
  <VBox noGutter={true} vAlign="flex-end" hAlign="center">
    <VSeparator />
    <Shimmer rounded={true} width="50%" height={FontSize.Large} />
  </VBox>
);

export const QuestionnairesShimmer = () => (
  <>
    <QuestionnaireCellShimmer />
    <QuestionnaireCellShimmer />
    <QuestionnaireCellShimmer />
  </>
);

export const CampaignsShimmer: React.SFC = () => (
  <>
    <CampaignTitleShimmer />
    <QuestionnairesShimmer />
    <VSeparator />
    <CampaignTitleShimmer />
    <QuestionnairesShimmer />
  </>
);

export class Campaign extends React.PureComponent<CampaignProps> {
  private campaignQuestionnaireProvider = new CampaignQuestionnaireProvider();
  private imcQuestionsService: ImcQuestions;

  constructor(props: CampaignProps) {
    super(props);
    this.imcQuestionsService = Container.get(ImcQuestions);
  }

  componentDidMount() {
    console.warn('Campaing Data', this.props.campaignData);
    this.campaignQuestionnaireProvider.fetchQuestionnaires(
      this.props.campaignData,
    );
  }

  render() {
    const campaignData = this.props.campaignData;

    return (
      <Subscribe to={[this.campaignQuestionnaireProvider]}>
        {() => {
          const state = this.campaignQuestionnaireProvider.state;
          console.warn('questionnaires', state);
          const questionnaires = state.questionnaires;
          return (
            <VBox noGutter={true}>
              <H2>{campaignData.name}</H2>
              {state.loading && <QuestionnairesShimmer />}
              {state.error && <PlaceholderSelector error={state.error} />}
              {questionnaires && this.renderQuestionGroupCells(questionnaires)}
              <VSeparator />
            </VBox>
          );
        }}
      </Subscribe>
    );
  }

  private renderQuestionGroupCells = (
    questionnaires: QuestionnaireFragment[],
  ) => _.flatMap(questionnaires, this.mapQuestionnaireToCells);

  private mapQuestionnaireToCells = (questionnaire: QuestionnaireFragment) => {
    let tempQuestionnaire = questionnaire;
    if (questionnaire.imcRequired) {
      tempQuestionnaire = _.cloneDeep(questionnaire);

      const imcQuestions = this.imcQuestionsService.getAsArray(
        questionnaire.weight,
        questionnaire.height,
      );

      const firstQuestionGroup = questionnaire.questionGroups[0];

      tempQuestionnaire.questionGroups[0].questions = imcQuestions.concat(
        firstQuestionGroup.questions,
      );
    }

    return tempQuestionnaire.questionGroups.map(
      (questionGroup: QuestionGroupFragment) => {
        const handleQuestionGroupTap = () =>
          this.handleQuestionGroupTap(questionGroup, questionnaire.code);
        return (
          <QuestionGroupCell
            text={questionGroup.name}
            key={`${this.props.campaignData.code}-${questionnaire.code}-${questionGroup.code}`}
            item={questionGroup}
            minimum={questionGroup.minimumToAnswer}
            answered={questionGroup.totalAnswered}
            onTap={handleQuestionGroupTap}
          />
        );
      },
    );
  };

  private handleQuestionGroupTap = (
    questionGroup: QuestionGroupFragment,
    questionnaireCode: string,
  ) => {
    Navigation.showModal<QuestionGroupPageProps>(QuestionGroupPage, {
      campaignCode: this.props.campaignData.code,
      questionnaireCode,
      questionGroup,
      onAnswered: this.handleAnsweredQuestionGroup,
    });
  };

  private handleAnsweredQuestionGroup = (
    campaignCode: string,
    questionnaireCode: string,
    questionGroup: QuestionGroupFragment,
    successFn: (resp: AnswerQuestionnaireMutation) => void,
    errorFn: (error: ApolloError) => void,
  ) => {
    this.campaignQuestionnaireProvider.answerQuestionGroup(
      campaignCode,
      questionnaireCode,
      questionGroup,
      resp => {
        this.props.onAnswerSuccess();
        successFn(resp);
      },
      errorFn,
    );
  };
}
