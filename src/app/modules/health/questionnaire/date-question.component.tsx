import * as React from 'react';
import { QuestionFragment } from '@app/data/graphql';
import { DateTimePickerField, Form, Validators } from '@atomic';

interface DateQuestionProps {
  currentQuestion: QuestionFragment;
  onValueChange: (value: any) => void;
}

export class DateQuestion extends React.Component<DateQuestionProps> {

  componentDidMount() {
    this.handleDateChange(this.getDateValue());
  }

  render() {
    const { currentQuestion } = this.props;
    const validators = [];
    if (currentQuestion.required) {
      validators.push(Validators.Required('Campo obrigatório'));
    }

    return (
      <Form.Field
        validators={validators}
        onValueChange={this.handleDateChange}
        value={this.getDateValue()}
        name='date'
      >
        <DateTimePickerField mode='date' />
      </Form.Field >
    );
  }

  private handleDateChange = (date: Date) => {
    this.props.onValueChange(date);
  }

  private getDateValue = () => {
    const answers = this.props.currentQuestion.chosenAnswers;

    return answers && answers.length && answers[0].display && new Date(answers[0].display) || new Date();
  }
}
