import * as React from 'react';
import { AnswerFragment, QuestionFragment } from '@app/data/graphql';
import { Form, TextField, Validators } from '@atomic';

interface NumericQuestionProps {
  currentQuestion: QuestionFragment;
  onValueChange: (value: any) => void;
}

interface NumericValidatorProps {
  required: boolean;
  minValue: number;
  maxValue: number;
  name: string;
  children: any;
  value: string;
}

const NumericValidator: React.SFC<NumericValidatorProps> = (props: NumericValidatorProps) => {

  const { required, minValue, maxValue } = props;

  const validators = [];
  if (required) {
    validators.push(Validators.Required('Campo obrigatório'));
  }

  if (minValue) {
    validators.push(Validators.MinValue(minValue, 'Deve ser no mínimo ' + minValue));
  }

  if (maxValue) {
    validators.push(Validators.MaxValue(maxValue, 'Deve ser no máximo ' + maxValue));
  }

  return (
    <Form.Field
      validators={validators}
      name={props.name}
      value={props.value}
    >
      {props.children}
    </Form.Field>
  );
};

export class NumberQuestion extends React.Component<NumericQuestionProps> {
  private answers: AnswerFragment[] = this.props.currentQuestion.chosenAnswers;

  componentDidMount() {
    if (this.answers && this.answers[0]) {
      this.handleNumberChange(this.answers[0].display);
    }
  }

  render() {
    const { currentQuestion } = this.props;

    return (
      <NumericValidator
        required={currentQuestion.required}
        minValue={currentQuestion.minValue}
        maxValue={currentQuestion.maxValue}
        name='numeric'
        value={this.answers && this.answers[0] && this.answers[0].display || undefined}
      >
        <TextField
          onChangeText={this.handleNumberChange}
          keyboardType={'numeric'}
          type={TextField.Type.NumericDecimal}
          autoFocus={true}
        />
      </NumericValidator>
    );
  }

  private handleNumberChange = (num: string) => {
    this.props.onValueChange(num);
  }
}
