import { Service } from 'typedi';
import { QuestionFragment, QuestionType } from '@app/data/graphql';

export const ImcQuestionsCodes = {
  weight: 'weight',
  height: 'height',
};

@Service()
export class ImcQuestions {
  private weightQuestion: Partial<QuestionFragment>;
  private heightQuestion: Partial<QuestionFragment>;

  constructor() {
    this.weightQuestion = this.getWeight();
    this.heightQuestion = this.getHeight();
  }

  getAsArray(weight?: number, height?: number): [QuestionFragment, QuestionFragment] {
    return [this.getWeight(weight), this.getHeight(height)] as [QuestionFragment, QuestionFragment];
  }

  private getWeight = (value?: number) => {
    if (this.weightQuestion) {
      (this.weightQuestion.chosenAnswers = this.getAnswer(value));
    }
    return this.weightQuestion || (this.weightQuestion = {
      type: QuestionType.Number,
      description: 'Peso (kg)',
      required: true,
      minValue: 0,
      maxValue: 999,
      code: ImcQuestionsCodes.weight,
    });
  }

  private getHeight = (value?: number) => {
    if (this.heightQuestion) {
      this.heightQuestion.chosenAnswers = this.getAnswer(value);
    }
    return this.heightQuestion || (this.heightQuestion = {
      type: QuestionType.Number,
      description: 'Altura (cm)',
      minValue: 30,
      maxValue: 300,
      required: true,
      code: ImcQuestionsCodes.height,
    });
  }

  private getAnswer = (value: number) => [{ display: value ? value.toString() : null, code: null, }];
}
