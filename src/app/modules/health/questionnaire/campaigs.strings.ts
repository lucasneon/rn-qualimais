export const CampaignsStrings = {
  Title: 'Questionários',
  AnswerSuccess: 'Respondido com sucesso!',
  AnswerError: 'Erro ao responder',
};
