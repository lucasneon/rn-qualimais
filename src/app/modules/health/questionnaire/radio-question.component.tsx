import * as React from 'react';
import { View } from 'react-native';
import { AnswerFragment, QuestionFragment, QuestionnaireConditionFragment } from '@app/data/graphql';
import { Form, RadioField, Validators } from '@atomic';

export interface RadioQuestionProps {
  currentQuestion: QuestionFragment;
  onValueChange: (value: any) => void;
  areConditionsSatisfied: (condition: QuestionnaireConditionFragment[]) => boolean;
}

export class RadioQuestion extends React.Component<RadioQuestionProps> {
  private answers: AnswerFragment[] = this.props.currentQuestion.chosenAnswers;

  render() {
    const { currentQuestion } = this.props;
    const validators = [];

    if (currentQuestion.required) {
      validators.push(Validators.Required('Campo obrigatório'));
    }

    return (
      <View>
        <Form.Field
          name='radio'
          validators={validators}
          onValueChange={this.handleSelectionChange}
          value={this.answers && this.answers.length && this.answers[0].code}
        >
          {currentQuestion.alternatives.map(answer => {
            return (
              this.props.areConditionsSatisfied(answer.conditionToShow) &&
              <RadioField key={answer.code} id={answer.code}>
                {answer.display}
              </RadioField>
            );
          })}
        </Form.Field>
      </View >
    );
  }

  private handleSelectionChange = (value: string) => {
    this.props.onValueChange(value);
  }
}
