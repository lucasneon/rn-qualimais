import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { BackHandler } from 'react-native';
import { AlertBuilder } from '@app/components/atm.alert-builder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import {
  AnswerFragment, AnswerQuestionnaireMutation, QuestionFragment, QuestionGroupFragment, QuestionnaireConditionFragment,
  QuestionType
} from '@app/data/graphql';
import { GeneralStrings } from '@app/modules/common';
import { ErrorMapper } from '@app/utils';
import { Form, FormSubmitData, HBox, PrimaryButton, VBox, VSeparator } from '@atomic';
import { QuestionComponent } from './question.component';
import { QuestionnaireStrings } from './questionnaire.strings';

export interface QuestionGroupPageProps {
  campaignCode: string;
  questionnaireCode: string;
  questionGroup: QuestionGroupFragment;
  onAnswered: (
    campaignCode: string,
    questionnaireCode: string,
    questionGroup: QuestionGroupFragment,
    successFn: (resp: AnswerQuestionnaireMutation) => void,
    errorFn: (error: ApolloError) => void,
  ) => void;
}

export type Props = NavigationPageProps<QuestionGroupPageProps>;

interface QuestionGroupPageState {
  currentQuestion: QuestionFragment;
  nextQuestion: QuestionFragment;
  currentIndex: number;
  nextIndex: number;
  loading: boolean;
}

@PageView('QuestionGroup')
export class QuestionGroupPage extends React.Component<Props, QuestionGroupPageState> {
  static options = { name: 'QuestionGroupPage' };

  private questionGroup: QuestionGroupFragment = this.props.navigation.questionGroup;
  private tempCurrentQuestion: QuestionFragment;
  private questions: QuestionFragment[] = this.questionGroup.questions.map(question => ({ ...question }));

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      currentIndex: 0,
      currentQuestion: this.questions[0],
      nextQuestion: this.questions[1],
      nextIndex: 1,
    };

    Navigation.events().bindComponent(this);
  }

  componentDidAppear() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentDidDisappear() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  navigationButtonPressed(_buttonId: any) {
    this.showSaveProgressAlert();
  }

  render() {
    const { currentIndex } = this.state;
    this.tempCurrentQuestion = { ...this.state.currentQuestion };
    return (
      <CollapsibleHeader keyboardShouldPersistTaps='handled' title={this.questionGroup.name}>
        <Form onSubmit={this.handleSubmit}>
          <VBox vGrow={true}>
            <QuestionComponent
              areConditionsSatisfied={this.areConditionsSatisfied}
              currentQuestion={this.tempCurrentQuestion}
              onValueChange={this.handleValueChange}
              key={this.tempCurrentQuestion.code}
            />
            <HBox>
              <HBox.Item>
                <PrimaryButton
                  negative={true}
                  text={QuestionnaireStrings.Previous}
                  onTap={this.renderPreviousQuestion}
                  expanded={true}
                  disabled={!currentIndex || this.state.loading}
                />
              </HBox.Item>
              <HBox.Separator />
              <HBox.Item>
                <Form.Submit>
                  <PrimaryButton
                    loading={this.state.loading}
                    expanded={true}
                    text={this.state.nextQuestion ? QuestionnaireStrings.Next : QuestionnaireStrings.Finish}
                    onTap={this.handleFinish}
                  />
                </Form.Submit>
              </HBox.Item>
            </HBox>
            <VSeparator />
          </VBox>
        </Form>
      </CollapsibleHeader>
    );
  }

  private handleSubmit = (formsSubmitData: FormSubmitData) => {
    if (Object.keys(formsSubmitData.error).length) {
      return;
    }

    if (this.state.nextQuestion) {
      this.setState({ currentQuestion: this.state.nextQuestion, currentIndex: this.state.nextIndex, loading: true },
        this.updateCurrentValueAndNextQuestion);
    } else {
      this.handleFinish();
    }
  }

  private handleValueChange = (value: any) => {
    if (!this.tempCurrentQuestion.chosenAnswers) {
      this.tempCurrentQuestion.chosenAnswers = [];
    }
    switch (this.tempCurrentQuestion.type) {
      case QuestionType.MultipleChoices:
        this.tempCurrentQuestion.chosenAnswers = value.map(val => ({ code: val, display: '' }));
        break;
      case QuestionType.Date:
        this.tempCurrentQuestion.chosenAnswers[0] = { code: '', display: value.toISOString() };
        break;
      case QuestionType.OneChoice:
        this.tempCurrentQuestion.chosenAnswers[0] = { code: value, display: '' };
        break;
      default:
        this.tempCurrentQuestion.chosenAnswers[0] = { code: '', display: value };
    }

    this.updateCurrentValueAndNextQuestion();
  }

  private handleBackPress = (): boolean => {
    if (this.state.currentIndex > 0) {
      this.renderPreviousQuestion();
    } else {
      this.showSaveProgressAlert();
    }

    return true;
  }

  private showSaveProgressAlert() {
    const Strings = QuestionnaireStrings.Exit;

    new AlertBuilder().withTitle(Strings.Title)
      .withMessage(Strings.Message)
      .withButton({ text: GeneralStrings.Cancel, style: 'cancel' })
      .withButton({ text: Strings.Exit, style: 'destructive', onPress: this.handleExitWithoutSaving })
      .withButton({ text: Strings.Save, onPress: this.handleFinish })
      .show();
  }

  private handleExitWithoutSaving = async () => {
    await Navigation.dismissModal(this.props.componentId);
  }

  private handleFinish = () => {
    this.questions[this.state.currentIndex] = { ...this.tempCurrentQuestion };

    const questionGroup: QuestionGroupFragment = {
      ...this.questionGroup,
      questions: this.questions
        .filter((question: QuestionFragment) => this.areConditionsSatisfied(question.conditionToShow)),
    };

    this.setState({ loading: true }, () =>
      this.props.navigation.onAnswered(this.props.navigation.campaignCode, this.props.navigation.questionnaireCode,
        questionGroup, this.handleAnswerSuccess, this.handleAnswerError));
  }

  private renderPreviousQuestion = () => {
    let newIndex = this.state.currentIndex - 1;
    while (newIndex >= 0 && !this.areConditionsSatisfied(this.questions[newIndex].conditionToShow)) {
      newIndex -= 1;
    }
    this.setState({ currentIndex: newIndex, currentQuestion: this.questions[newIndex], loading: true },
      this.updateCurrentValueAndNextQuestion);
  }

  private areConditionsSatisfied = (alternativeConditions: QuestionnaireConditionFragment[]): boolean => {
    const hasConditions: boolean = alternativeConditions && alternativeConditions.length > 0;
    return !hasConditions || alternativeConditions.some(this.findCondition);
  }

  private findCondition = (condition: QuestionnaireConditionFragment) => {
    const answeredQuestion = this.questions
      .find((question: QuestionFragment) => question.code === condition.questionCode);
    return !!answeredQuestion && answeredQuestion.chosenAnswers && answeredQuestion.chosenAnswers
      .some((chosenAnswers: AnswerFragment) => chosenAnswers.code.toString() === condition.answerCode);
  }

  private updateCurrentValueAndNextQuestion = () => {
    this.questions[this.state.currentIndex] = { ...this.tempCurrentQuestion };

    let newIndex = this.state.currentIndex + 1;

    const totalQuestions = this.questions.length;

    while (newIndex < totalQuestions && !this.areConditionsSatisfied(this.questions[newIndex].conditionToShow)) {
      newIndex += 1;
    }

    this.setState({
      nextIndex: newIndex,
      currentQuestion: this.tempCurrentQuestion,
      nextQuestion: this.questions[newIndex],
      loading: false,
    });
  }

  private handleAnswerSuccess = async () => {
    await Navigation.dismissModal(this.props.componentId);
  }

  private handleAnswerError = (error: ApolloError) => {
    FlashMessageService.showError(ErrorMapper.map(error));
    this.setState({ loading: false });
  }
}
