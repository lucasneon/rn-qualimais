import * as React from 'react';
import { Card } from '@app/components/atm.card';
import { QuestionFragment, QuestionnaireConditionFragment, QuestionType } from '@app/data/graphql';
import { Body, H3, VSeparator } from '@atomic';
import { CheckboxQuestion } from './checkbox-question.component';
import { DateQuestion } from './date-question.component';
import { NumberQuestion } from './number-question.component';
import { QuestionView } from './question.component.style';
import { RadioQuestion } from './radio-question.component';
import { TextualQuestion } from './textual-question.component';

export interface QuestionProps {
  currentQuestion: QuestionFragment;
  onValueChange: (value: any) => void;
  areConditionsSatisfied: (conditions: QuestionnaireConditionFragment[]) => boolean;
}

const QuestionTypeComponents: { [key in QuestionType]?: React.ComponentClass } = {
  OneChoice: RadioQuestion,
  MultipleChoices: CheckboxQuestion,
  Textual: TextualQuestion,
  Number: NumberQuestion,
  Date: DateQuestion,
};

export const QuestionComponent: React.SFC<QuestionProps> = (props: QuestionProps) => {
  const QuestionTypeComponent = QuestionTypeComponents[props.currentQuestion.type];
  const question = props.currentQuestion;
  const descritive = question.descritive;

  return (
    <QuestionView>
      <VSeparator />
      <Card>
        <H3>{question.description}{question.required && '*'}</H3>
        {descritive && descritive.map(text => <Body key={text}>{text}</Body>)}
        < VSeparator />
        <QuestionTypeComponent {...props} />
      </Card>
      <VSeparator />
    </QuestionView>
  );
};
