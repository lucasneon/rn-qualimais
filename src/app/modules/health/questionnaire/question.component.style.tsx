import { View } from 'react-native';
import styled from 'styled-components/native';
import { Spacing } from '@atomic';

export const QuestionView = styled(View)`
  padding-horizontal: ${Spacing.Large};
  margin-horizontal: ${-Spacing.Large};
`;
