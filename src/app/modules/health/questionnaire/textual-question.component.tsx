import * as React from 'react';
import { QuestionFragment } from '@app/data/graphql';
import { Form, TextField, Validators } from '@atomic';

interface TextualQuestionProps {
  currentQuestion: QuestionFragment;
  onValueChange: (value: any) => void;
}

interface TextualValidatorProps {
  required: boolean;
  name: string;
  children: any;
  value: string;
}

const TextualValidator: React.SFC<TextualValidatorProps> = (props: TextualValidatorProps) => {

  const { required } = props;

  const validators = [];
  if (required) {
    validators.push(Validators.Required('Campo obrigatório'));
  }

  return (
    <Form.Field
      validators={validators}
      name={props.name}
      value={props.value}
    >
      {props.children}
    </Form.Field>
  );
};

export class TextualQuestion extends React.Component<TextualQuestionProps> {

  render() {
    const { currentQuestion } = this.props;

    const answers = currentQuestion.chosenAnswers;

    return (
      <TextualValidator
        required={currentQuestion.required}
        name='textual'
        value={answers && answers[0] && answers[0].display || undefined}
      >
        <TextField
          onChangeText={this.handleTextChange}
          keyboardType={'default'}
          autoFocus={true}
        />
      </TextualValidator>
    );
  }

  private handleTextChange = (text: string) => {
    this.props.onValueChange(text);
  }
}
