export const HealthOrientationsStrings = {
  Title: 'Orientações de saúde',
  NotAnswered: {
    Title: 'Questionário não respondido.',
    Description: 'Não obtivemos respostas do Questionário de Saúde. Clique no botão abaixo para atualizá-lo.',
  },
};
