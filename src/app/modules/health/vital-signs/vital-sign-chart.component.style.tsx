import styled from 'styled-components/native';
import PureChart from '@app/components/org.chart';
import { Color } from '@atomic';

export const LineChartStyled = styled(PureChart).attrs({
  selectedColor: Color.Success,
  type: 'line',
  height: 300,
})``;
