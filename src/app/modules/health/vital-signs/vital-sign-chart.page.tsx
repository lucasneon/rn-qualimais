import * as React from 'react';
import { QueryResult } from 'react-apollo';
import {
  Placeholder,
  PlaceholderSelector,
} from '@app/components/mol.placeholder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
// import { QueryContainer } from '@app/core/graphql';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import {
  VitalSignQuery,
  VitalSignQueryVariables,
  VitalSignType,
} from '@app/data/graphql';
import { VitalSignsLabelMap } from '@app/modules/common/vital-signs';
import { ErrorMapper } from '@app/utils';
import {
  Asset,
  LoadingState,
  PrimaryButton,
  Root,
  Shimmer,
  VBox,
  VSeparator,
} from '@atomic';
import {
  InsertVitalSignPage,
  InsertVitalSignPageProps,
} from './insert-vital-sign';
import { VitalSignChart } from './vital-sign-chart.component';
import { VitalSignsStrings } from './vital-signs.strings';
import { Subscribe } from 'unstated';
import { VitalSignProvider } from '@app/services';

export interface VitalSignChartPageProps {
  vitalSignType: VitalSignType;
}

const LastMeasuresCount = 25;

@PageView('VitalSignChart')
export class VitalSignChartPage extends React.Component<
  NavigationPageProps<VitalSignChartPageProps>
> {
  static options = { name: 'VitalSignChartPage' };
  private vitalSignProvider: VitalSignProvider;
  private handleRefetch: () => void;
  private queryVar: VitalSignQueryVariables;
  private loading: boolean = true;
  componentDidMount() {
    const type = this.props.navigation.vitalSignType;
    this.queryVar = {
      vitalSign: type,
      count: LastMeasuresCount,
    };
    this.vitalSignProvider.fetch(this.queryVar.vitalSign, this.queryVar.count);
  }

  render() {
    const type = this.props.navigation.vitalSignType;


    return (
      <Subscribe to={[VitalSignProvider]}>
        {(vitalSignProvider: VitalSignProvider) => {
          this.vitalSignProvider = vitalSignProvider;
          const res = vitalSignProvider.state.data;
          const error = vitalSignProvider.state.error;
          this.loading = vitalSignProvider.state.loading;
          const vitalSigns = res;
          return (
            <Root>
              <CollapsibleHeader title={this.getTitle(res)}>
                <Root>
                  <LoadingState
                    data={vitalSigns && vitalSigns.length > 0}
                    error={!!error}
                    loading={this.loading}>
                    <LoadingState.Shimmer>
                      <VBox>
                        <Shimmer height={300} />
                      </VBox>
                    </LoadingState.Shimmer>
                    <LoadingState.ErrorPlaceholder>
                      <PlaceholderSelector
                        error={ErrorMapper.map(error)}
                        onTap={this.handleRefetch}
                      />
                    </LoadingState.ErrorPlaceholder>
                    <LoadingState.EmptyState>
                      <VBox>
                        <Placeholder
                          title={VitalSignsStrings.NoRegister.Title}
                          message={VitalSignsStrings.NoRegister.Message}
                          image={Asset.Onboard.OnboardRecord}
                        />
                        <PrimaryButton
                          text={VitalSignsStrings.Add}
                          onTap={this.handleButtonTap}
                          expanded={true}
                        />
                      </VBox>
                    </LoadingState.EmptyState>
                    <VitalSignChart
                      vitalSigns={vitalSigns}
                      type={type}
                      goToEnd={true}
                    />
                    <VSeparator />
                    <VBox>
                      <PrimaryButton
                        text={VitalSignsStrings.Add}
                        expanded={true}
                        onTap={this.handleButtonTap}
                      />
                    </VBox>
                  </LoadingState>
                </Root>
              </CollapsibleHeader>
            </Root>
          );
        }}
      </Subscribe>

      // <QueryContainer document="vital-sign" variables={queryVar}>
      //   {(res: QueryResult<VitalSignQuery>) => {
      // this.handleRefetch = () => res.refetch(queryVar);
      //     const vitalSigns = res.data && res.data.VitalSign;

      //     return (
      //       <Root>
      //         <CollapsibleHeader title={this.getTitle(res)}>
      //           <Root>
      //             <LoadingState
      //               data={vitalSigns && vitalSigns.length > 0}
      //               error={!!res.error}
      //               loading={res.loading}>
      //               <LoadingState.Shimmer>
      //                 <VBox>
      //                   <Shimmer height={300} />
      //                 </VBox>
      //               </LoadingState.Shimmer>
      //               <LoadingState.ErrorPlaceholder>
      //                 <PlaceholderSelector
      //                   error={ErrorMapper.map(res.error)}
      //                   onTap={this.handleRefetch}
      //                 />
      //               </LoadingState.ErrorPlaceholder>
      //               <LoadingState.EmptyState>
      //                 <VBox>
      //                   <Placeholder
      //                     title={VitalSignsStrings.NoRegister.Title}
      //                     message={VitalSignsStrings.NoRegister.Message}
      //                     image={Asset.Onboard.OnboardRecord}
      //                   />
      //                   <PrimaryButton
      //                     text={VitalSignsStrings.Add}
      //                     onTap={this.handleButtonTap}
      //                     expanded={true}
      //                   />
      //                 </VBox>
      //               </LoadingState.EmptyState>
      //               <VitalSignChart
      //                 vitalSigns={vitalSigns}
      //                 type={type}
      //                 goToEnd={true}
      //               />
      //               <VSeparator />
      //               <VBox>
      //                 <PrimaryButton
      //                   text={VitalSignsStrings.Add}
      //                   expanded={true}
      //                   onTap={this.handleButtonTap}
      //                 />
      //               </VBox>
      //             </LoadingState>
      //           </Root>
      //         </CollapsibleHeader>
      //       </Root>
      //     );
      //   }}
      // </QueryContainer>
    );
  }

  private getTitle(res: QueryResult<VitalSignQuery> | any): string {
    let measureUnit: string;

    try {
      measureUnit = ' (' + res[0].measureUnit + ')';
    } catch (e) {
      measureUnit = '';
    }

    return (
      VitalSignsLabelMap[this.props.navigation.vitalSignType] + measureUnit
    );
  }

  private onSuccess = () => {
    FlashMessageService.showSuccess(VitalSignsStrings.Success);
    this.handleRefetch();
  };

  private handleButtonTap = () => {
    const passProps: InsertVitalSignPageProps = {
      type: this.props.navigation.vitalSignType,
      onSuccess: this.onSuccess,
    };
    Navigation.showModal<InsertVitalSignPageProps>(
      InsertVitalSignPage,
      passProps,
    );
  };
}
