import moment from 'moment';
import * as React from 'react';
import { VitalSignFragment, VitalSignType } from '@app/data/graphql';
import { Color, Root } from '@atomic';
import { LineChart } from './line-chart.component';

export interface VitalSignChartProps {
  vitalSigns: VitalSignFragment[];
  type: VitalSignType;
  goToEnd?: boolean;
}

export const VitalSignChart: React.SFC<VitalSignChartProps> = (
  props: VitalSignChartProps,
) => {
  const mapBloodPressure = () => {
    return props.vitalSigns
      .map(bloodPressure => {
        const systolic = bloodPressure.subMeasures?.find(
          it => it.type === VitalSignType.SystolicPressure,
        );
        const diastolic = bloodPressure.subMeasures?.find(
          it => it.type === VitalSignType.DiastolicPressure,
        );
        return {
          ys: systolic?.measure.amount || 0,
          yd: diastolic?.measure.amount || 0,
          x: moment(bloodPressure.measuredAt).format('DD-MMM-YY'),
        };
      })
      .reduce(
        (acc, cur) => {
          // Push to systolic accumulator
          acc[0].data.push({ x: cur.x, y: cur.ys });
          // Push to diastolic accumulator
          acc[1].data.push({ x: cur.x, y: cur.yd });
          return acc;
        },
        [
          { seriesName: 'systolic', data: [], color: Color.Primary },
          { seriesName: 'diastolic', data: [], color: Color.Secondary },
        ],
      );
  };

  const mapNormalVitalSigns = () => {
    const data = props.vitalSigns.map(vitalSign => {
      console.warn(vitalSign);
      return {
        y: vitalSign.measure?.amount || 0,
        x: moment(vitalSign.mesasuredAt).format('DD-MMM-YY'),
      };
    });
    return [{ seriesName: 'signs', data, color: Color.Primary }];
  };

  return (
    <Root>
      <LineChart
        data={
          props.type === VitalSignType.BloodPressure
            ? mapBloodPressure()
            : mapNormalVitalSigns()
        }
        goToEnd={props.goToEnd}
      />
    </Root>
  );
};
