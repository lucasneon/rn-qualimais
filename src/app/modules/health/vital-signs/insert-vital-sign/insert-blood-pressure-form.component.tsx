import * as React from 'react';
import { VitalSignInput, VitalSignType } from '@app/data/graphql';
import {
  DateTimePickerField, Form, FormSubmitData, PrimaryButton, TextField, TextFieldType, Validators, VBox, VSeparator
} from '@atomic';
import { InsertVitalSignStrings } from './insert.strings';

const Fields = {
  diastolic: 'diastolic',
  systolic: 'systolic',
  date: 'date',
};

const MAX_DIASTOLIC = 150;
const MAX_SYSTOLIC = 210;
const MIN_DIASTOLIC = 40;
const MIN_SYSTOLIC = 60;

export interface InsertBloodPressureFormProps {
  loading: boolean;
  maxDateTime: Date;
  onSubmit: (data: VitalSignInput[]) => void;
}

interface State {
  maxDiastolic: number;
  minSystolic: number;
}

export class InsertBloodPressureForm extends React.PureComponent<InsertBloodPressureFormProps, State> {
  constructor(props) {
    super(props);
    this.state = { maxDiastolic: MAX_DIASTOLIC, minSystolic: MIN_SYSTOLIC };
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Scroll>
          <VBox>
            <VSeparator />
            <Form.Field
              label={InsertVitalSignStrings.BloodPressure.Labels.Systolic}
              validators={[
                Validators.Required(),
                Validators.HasNumber(InsertVitalSignStrings.Validations.Number),
                Validators.MinValue(this.state.minSystolic, InsertVitalSignStrings.Validations.SystolicMin),
                Validators.MaxValue(MAX_SYSTOLIC, InsertVitalSignStrings.Validations.SystolicMax),
              ]}
              name={Fields.systolic}
              onValueChange={this.handleSystolicChange}
            >
              <TextField type={TextFieldType.Numeric} />
            </Form.Field>
            <VSeparator />
            <Form.Field
              label={InsertVitalSignStrings.BloodPressure.Labels.Diastolic}
              validators={[
                Validators.Required(),
                Validators.HasNumber(InsertVitalSignStrings.Validations.Number),
                Validators.MinValue(MIN_DIASTOLIC, InsertVitalSignStrings.Validations.DiastolicMin),
                Validators.MaxValue(this.state.maxDiastolic, InsertVitalSignStrings.Validations.DiastolicMax),
              ]}
              name={Fields.diastolic}
              onValueChange={this.handleDiastolicChange}
            >
              <TextField type={TextFieldType.Numeric} />
            </Form.Field>
            <VSeparator />
            <Form.Field
              label={InsertVitalSignStrings.Date}
              validators={[Validators.Required(), Validators.MaxDateTime(this.props.maxDateTime)]}
              name={Fields.date}
              value={new Date()}
            >
              <DateTimePickerField maxDateTime={this.props.maxDateTime} mode={'datetime'}/>
            </Form.Field>
            <VSeparator />
            <Form.Submit>
              <PrimaryButton
                text={InsertVitalSignStrings.BloodPressure.SubmitButtonText}
                expanded={true}
                loading={this.props.loading}
                submit={true}
              />
            </Form.Submit>
            <VSeparator />
          </VBox>
        </Form.Scroll>
      </Form>
    );
  }

  private handleDiastolicChange = (value: string) => {
    this.setState({ minSystolic: Math.max(MIN_SYSTOLIC, Number(value)) });
  }

  private handleSystolicChange = (value: string) => {
    this.setState({ maxDiastolic: Math.min(MAX_DIASTOLIC, Number(value)) });
  }

  private handleSubmit = (formData: FormSubmitData) => {
    if (Object.keys(formData.error).length !== 0) { // exit function if there is an error
      return;
    }

    const date = formData.data.date.toISOString();
    const data: VitalSignInput[] = [
      {
        type: VitalSignType.BloodPressure,
        measuredAt: date,
        subMeasures: [
          {
            type: VitalSignType.DiastolicPressure,
            measuredAt: date,
            measure: {
              amount: Number(formData.data.diastolic),
              unit: InsertVitalSignStrings.BloodPressure.Unit,
            },
          },
          {
            type: VitalSignType.SystolicPressure,
            measuredAt: date,
            measure: {
              amount: Number(formData.data.systolic),
              unit: InsertVitalSignStrings.BloodPressure.Unit,
            },
          },
        ],
      },
    ];

    this.props.onSubmit(data);
  }
}
