import * as React from 'react';
import { VitalSignInput, VitalSignType } from '@app/data/graphql';
import {
  DateTimePickerField, Form, FormSubmitData, PrimaryButton, TextField, TextFieldType, Validators, VBox, VSeparator
} from '@atomic';
import { InsertVitalSignStrings } from './insert.strings';

const Fields = {
  glycemia: 'glycemia',
  date: 'date',
};

export interface InsertGlycemiaFormProps {
  loading: boolean;
  maxDateTime: Date;
  onSubmit: (data: VitalSignInput[]) => void;
}

export class InsertGlycemiaForm extends React.Component<InsertGlycemiaFormProps> {

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Scroll keyboardAware={false}>
          <VBox>
            <VSeparator />
            <Form.Field
              label={InsertVitalSignStrings.Glycemia.Labels.Glycemia}
              validators={[Validators.Required(), Validators.HasNumber(InsertVitalSignStrings.Validations.Number)]}
              name={Fields.glycemia}
            >
              <TextField type={TextFieldType.Numeric} />
            </Form.Field>
            <VSeparator />
            <Form.Field
              label={InsertVitalSignStrings.Date}
              validators={[Validators.Required(), Validators.MaxDateTime(this.props.maxDateTime)]}
              name={Fields.date}
              value={new Date()}
            >
              <DateTimePickerField maxDateTime={this.props.maxDateTime} mode={'datetime'}/>
            </Form.Field>
            <VSeparator />
            <Form.Submit>
              <PrimaryButton
                text={InsertVitalSignStrings.Glycemia.SubmitButtonText}
                expanded={true}
                loading={this.props.loading}
                submit={true}
              />
            </Form.Submit>
            <VSeparator />
          </VBox>
        </Form.Scroll>
      </Form>
    );
  }

  private handleSubmit = (formData: FormSubmitData) => {
    if (Object.keys(formData.error).length !== 0) {
      return;
    }

    const date = formData.data.date.toISOString();
    const data: VitalSignInput[] = [
      {
        type: VitalSignType.Glycemia,
        measuredAt: date,
        measure: {
          amount: Number(formData.data.glycemia),
          unit: InsertVitalSignStrings.Glycemia.Unit,
        },
      },
    ];
    this.props.onSubmit(data);
  }
}
