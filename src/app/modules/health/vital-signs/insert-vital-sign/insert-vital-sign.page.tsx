import * as React from 'react';
import { Subscribe } from 'unstated';
import { CustomAlerts } from '@app/components/obj.custom-alert';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { GraphQLProvider } from '@app/core/graphql/';
import { Navigation } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import {
  InsertVitalSignMutationVariables,
  VitalSignInput,
  VitalSignType,
} from '@app/data/graphql';
import { ModalPage } from '@app/modules/common/modal.page';
import { VitalSignProvider } from '@app/services/providers';
import { DateUtils, ErrorMapper } from '@app/utils';
import { Root } from '@atomic';
import { InsertBloodPressureForm } from './insert-blood-pressure-form.component';
import { InsertGlycemiaForm } from './insert-glycemia-form.component';
import { InsertIMCForm } from './insert-imc-form.component';
import { InsertVitalSignStrings } from './insert.strings';

export interface FormData {
  title: string;
  form: JSX.Element;
}

export interface InsertVitalSignPageProps {
  type: VitalSignType;
  onSuccess: () => void;
}

@PageView('InsertVitalSign')
export class InsertVitalSignPage extends ModalPage<InsertVitalSignPageProps> {
  static options = { name: 'InsertVitalSignPage' };
  private graph: GraphQLProvider;
  private vitalSignProvider: VitalSignProvider;

  render() {
    return (
      <Root>
        <Subscribe to={[GraphQLProvider, VitalSignProvider]}>
          {(graph: GraphQLProvider, vitalSignProvider: VitalSignProvider) => {
            this.graph = graph;
            this.vitalSignProvider = vitalSignProvider;
            const formData: FormData = this.decideFormData(
              this.props.navigation.type,
            );
            return (
              <CollapsibleHeader title={formData.title}>
                {formData.form}
              </CollapsibleHeader>
            );
          }}
        </Subscribe>
      </Root>
    );
  }

  private decideFormData(type: VitalSignType): FormData {
    const props = {
      onSubmit: this.onSubmit,
      loading: this.graph.state.loading,
      maxDateTime: DateUtils.Create.getCurrent(0, 1),
    };

    switch (type) {
      case VitalSignType.Glycemia:
        return {
          title: InsertVitalSignStrings.Glycemia.Title,
          form: <InsertGlycemiaForm {...props} />,
        };

      case VitalSignType.BloodPressure:
        return {
          title: InsertVitalSignStrings.BloodPressure.Title,
          form: <InsertBloodPressureForm {...props} />,
        };

      default:
        const recentVitalSigns = this.vitalSignProvider.state.recentVitalSigns;
        const height =
          recentVitalSigns &&
          recentVitalSigns.find(sign => sign.type === VitalSignType.Height);
        const heightMeasure = height && height.measure && height.measure.amount;
        return {
          title: InsertVitalSignStrings.IMC.Title,
          form: <InsertIMCForm height={heightMeasure} {...props} />,
        };
    }
  }

  private onSubmit = (data: VitalSignInput[]) => {
    CustomAlerts.additionConfirmation(() => this.insertVitalSignMutation(data));
  };

  private insertVitalSignMutation = (data: VitalSignInput[]) => {
    this.vitalSignProvider
      .insertVitalSign(data)
      .then(async res => {
        if (res.error) {
          this.handleMutationError(res.error);
        }
        this.handleMutationSuccess();
      })
      .catch(error => {
        this.handleMutationError(error);
      });

    // this.graph.mutate<InsertVitalSignMutationVariables>(
    //   'insert-vital-sign',
    //   { data },
    //   this.handleMutationSuccess,
    //   this.handleMutationError,
    // );
  };

  private handleMutationSuccess = async () => {
    await this.vitalSignProvider.rehydrate();
    this.props.navigation.onSuccess();
    await Navigation.dismissModal(this.props.componentId);
  };

  private handleMutationError = (error: any) => {
    FlashMessageService.showError(ErrorMapper.map(error));
  };
}
