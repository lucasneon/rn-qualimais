export const InsertVitalSignStrings = {
  Date: 'Quando',
  Error: 'Falha na adição',
  Validations: {
    Number: 'Precisa ser um número válido',
    DiastolicMin: 'Valor abaixo do plausível',
    DiastolicMax: 'Deve ser menor do que a pressão máxima',
    SystolicMin: 'Deve ser maior do que a pressão mínima',
    SystolicMax: 'Valor acima do plausível',
  },
  IMC: {
    Title: 'Novo IMC',
    Labels: {
      Weight: 'Peso (kg)',
      Height: 'Altura (cm)',
      IMC: 'IMC (kg/m²)',
    },
    Units: {
      Weight: 'kg',
      Height: 'cm',
      IMC: 'kg/m²',
    },
    SubmitButtonText: 'Adicionar medidas',
  },
  BloodPressure: {
    Title: 'Nova pressão arterial',
    Labels: {
      Diastolic: 'Pressão mínima (mm[Hg])',
      Systolic: 'Pressão máxima (mm[Hg])',
    },
    Unit: 'mm[Hg]',
    SubmitButtonText: 'Adicionar pressão',
  },
  Glycemia: {
    Title: 'Nova glicemia',
    Labels: {
      Glycemia: 'Glicemia (mg/dl)',
    },
    Unit: 'mg/dl',
    SubmitButtonText: 'Adicionar glicemia',
  },
};
