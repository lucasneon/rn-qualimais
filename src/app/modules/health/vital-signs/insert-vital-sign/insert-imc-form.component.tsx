import * as React from 'react';
import { VitalSignInput, VitalSignType } from '@app/data/graphql';
import {
  DateTimePickerField, Form, FormSubmitData, PrimaryButton, TextField, TextFieldType, Validators, VBox, VSeparator
} from '@atomic';
import { InsertVitalSignStrings } from './insert.strings';

const Fields = {
  weight: 'weight',
  height: 'height',
  imc: 'imc',
  date: 'date',
};

export interface InsertIMCFormProps {
  loading: boolean;
  maxDateTime: Date;
  height: number;
  onSubmit: (data: VitalSignInput[]) => void;
}

export interface InsertIMCFormState {
  height: number;
  weight: number;
}

export class InsertIMCForm extends React.Component<InsertIMCFormProps, InsertIMCFormState> {

  constructor(props) {
    super(props);
    this.state = { height: this.props.height, weight: null };
  }

  render() {
    const imc = this.calculateIMC();
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Scroll>
          <VBox>
            <VSeparator />
            <Form.Field
              label={InsertVitalSignStrings.IMC.Labels.Weight}
              validators={[Validators.Required(), Validators.HasNumber(InsertVitalSignStrings.Validations.Number)]}
              name={Fields.weight}
              onValueChange={this.handleWeightChange}
            >
              <TextField type={TextFieldType.NumericDecimal} />
            </Form.Field>
            <VSeparator />
            <Form.Field
              label={InsertVitalSignStrings.IMC.Labels.Height}
              validators={[Validators.Required(), Validators.HasNumber(InsertVitalSignStrings.Validations.Number)]}
              name={Fields.height}
              onValueChange={this.handleHeightChange}
              value={String(this.state.height)}
            >
              <TextField type={TextFieldType.Numeric} />
            </Form.Field>
            <VSeparator />
            <Form.Field
              key={imc}
              label={InsertVitalSignStrings.IMC.Labels.IMC}
              validators={[Validators.Required(), Validators.HasNumber(InsertVitalSignStrings.Validations.Number)]}
              name={Fields.imc}
              value={imc}
            >
              <TextField editable={false} type={TextFieldType.NumericDecimal} />
            </Form.Field>
            <VSeparator />
            <Form.Field
              label={InsertVitalSignStrings.Date}
              validators={[Validators.Required(), Validators.MaxDateTime(this.props.maxDateTime)]}
              name={Fields.date}
              value={new Date()}
            >
              <DateTimePickerField maxDateTime={this.props.maxDateTime} mode={'date'} />
            </Form.Field>
            <VSeparator />
            <Form.Submit>
              <PrimaryButton
                text={InsertVitalSignStrings.IMC.SubmitButtonText}
                expanded={true}
                loading={this.props.loading}
                submit={true}
              />
            </Form.Submit>
            <VSeparator />
          </VBox>
        </Form.Scroll>
      </Form>
    );
  }

  private calculateIMC(): string {
    const heightInMeters = this.state.height / 100;   // converts to meters
    const imc = this.state.weight / Math.pow(heightInMeters, 2); // IMC equation
    return imc.toFixed(2);
  }

  private handleHeightChange = (height: string) => {
    this.setState({ height: +height });
  }

  private handleWeightChange = (weight: string) => {
    this.setState({ weight: +weight });
  }

  private handleSubmit = (formData: FormSubmitData) => {
    if (Object.keys(formData.error).length !== 0) {
      return;
    }
    const date = formData.data.date.toISOString();
    const data: VitalSignInput[] = [
      {
        type: VitalSignType.Height,
        measuredAt: date,
        measure: {
          amount: +formData.data.height,
          unit: InsertVitalSignStrings.IMC.Units.Height,
        },
      },
      {
        type: VitalSignType.Weight,
        measuredAt: date,
        measure: {
          amount: +formData.data.weight,
          unit: InsertVitalSignStrings.IMC.Units.Weight,
        },
      },
      {
        type: VitalSignType.Imc,
        measuredAt: date,
        measure: {
          amount: +formData.data.imc,
          unit: InsertVitalSignStrings.IMC.Units.IMC,
        },
      },
    ];
    this.props.onSubmit(data);
  }
}
