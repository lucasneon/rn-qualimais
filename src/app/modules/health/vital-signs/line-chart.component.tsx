import * as React from 'react';
import { Root } from '@atomic';
import { LineChartStyled } from './vital-sign-chart.component.style';

interface Point {
  x: number;
  y: number;
}

export interface LineChartData {
  seriesName: string;
  data: Point[];
  color?: string;
}

export interface LineChartProps {
  data: LineChartData[];
  goToEnd?: boolean;
}

export const LineChart: React.SFC<LineChartProps> = (props: LineChartProps) => {
  const reverse = a => [...a].map(a.pop, a);

  const data = props.data.map(chartData => ({ ...chartData, data: reverse(chartData.data), }));

  const selectedIndex = data[0].data.length - 1;

  return (
    <Root>
      <LineChartStyled
        data={data}
        selectedIndex={selectedIndex}
        goToEnd={props.goToEnd}
      />
    </Root>
  );
};
