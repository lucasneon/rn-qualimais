export const VitalSignsStrings = {
  Add: 'Adicionar nova medida',
  Success: 'Dados inseridos com sucesso.',
  NoRegister: {
    Message: 'Você ainda não registrou medidas.',
    Title: 'Sem medidas',
    Caption: 'sem medidas',
  },
};
