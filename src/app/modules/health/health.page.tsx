import * as React from 'react';
import { Container } from 'typedi';
import { Subscribe } from 'unstated';
import { BeneficiaryCell } from '@app/components/mol.cell';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { GuardProps, Navigation } from '@app/core/navigation';
import { VitalSignType } from '@app/data/graphql';
import { AccountSwitchPage } from '@app/modules/account/account-switch.page';
import { GuardParams } from '@app/modules/authentication';
import { VitalSigns } from '@app/modules/common/vital-signs';
import { MedicationListPage } from '@app/modules/habit/medication';
import { HealthMedicalRecordPage } from '@app/modules/health/medical-record';
import { OnboardService, OnboardServiceKey } from '@app/modules/onboard';
import {
  CarePlanProvider,
  CarePlanProviderToken,
  BeneficiaryProvider,
  BeneficiaryProviderToken,
} from '@app/services/providers';
import { Asset, Cell, H3, Root, VSeparator } from '@atomic';
import { HealthStrings } from './health.strings';
import { PastEventsPage } from './past-events.page';
import { CampaignsPage } from './questionnaire/campaigns.page';
import { VitalSignChartPage, VitalSignChartPageProps } from './vital-signs';
import { Popup } from 'react-native-map-link';
import { Alert } from 'react-native';
import { Color } from '@atomic/obj.constants';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { H3Gray } from '@atomic/atm.typography';
import { CarePlanStatus } from '@app/model';

interface HealthPageProps extends GuardProps<GuardParams> {
  componentId: string;
}

interface HealthPageState {
  animation: string;
  shouldAnswerQuestionnaire: boolean;
}

@PageView('Health')
export class HealthPage extends React.Component<
  HealthPageProps,
  HealthPageState
> {
  static options = {
    name: 'HealthPage',
    topBar: { backButton: { visible: false } },
  };

  private beneficiaryProvider: BeneficiaryProvider = Container.get(
    BeneficiaryProviderToken,
  );
  private benefHasCarePlan: boolean =
    this.beneficiaryProvider.state.active.hasCarePlan;

  private campaignProvider: CarePlanProvider = Container.get(
    CarePlanProviderToken,
  );

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      animation: null,
      shouldAnswerQuestionnaire: false,
    };
  }

  componentDidAppear() {
    if (this.benefHasCarePlan) {
      Container.get<OnboardService>(OnboardServiceKey).check(
        this.props.componentId,
      );
    }
    this.setState({
      shouldAnswerQuestionnaire: this.requiredAnswerQuestionnaire(),
    });
  }

  requiredAnswerQuestionnaire() {
    return (
      this.campaignProvider.state.didAnswerRequiredQuestionnaires === false
    );
  }

  showAlert() {
    Alert.alert(
      'Aviso',
      this.campaignProvider.state.needAnswerPrincipal
        ? 'O titular deve responder o questionário de saúde para liberar essa função!'
        : 'Você deve responder o questionário de saúde para liberar essa função!',
    );
  }

  render() {
    const activeBeneficiary = this.props.guard.params.active;

    return (
      <CollapsibleHeader
        title="Saúde"
        dock={
          <BeneficiaryCell
            onTap={this.handleChangeBeneficiary}
            name={activeBeneficiary && activeBeneficiary.name}
          />
        }>
        <Root>
          <VSeparator />
          <VitalSigns onVitalSignTap={this.handleVitalSignTap} />
          <VSeparator />
          <Subscribe to={[CarePlanProvider]}>
            {(campaignProvider: CarePlanProvider) => (
              <Cell
                leftThumb={Asset.Icon.Custom.Survey}
                rightThumb={Asset.Icon.Atomic.ChevronRight}
                tintColor={
                  campaignProvider.state.needAnswerPrincipal ? Color.Gray : null
                }
                onTap={
                  campaignProvider.state.needAnswerPrincipal
                    ? () => this.showAlert()
                    : this.handleHealthQuestionnaireTap
                }
                showBadge={this.state.shouldAnswerQuestionnaire}>
                <H3>{HealthStrings.Questionnaires}</H3>
                {/* {this.state.shouldAnswerQuestionnaire ? (
                  <H3Gray>{HealthStrings.MedicalRecord}</H3Gray>
                ) : (
                  <H3>{HealthStrings.Questionnaires}</H3>
                )} */}
              </Cell>
            )}
          </Subscribe>
          <Cell
            leftThumb={Asset.Icon.Custom.MedicalRecord}
            rightThumb={Asset.Icon.Atomic.ChevronRight}
            tintColor={this.state.shouldAnswerQuestionnaire ? Color.Gray : null}
            onTap={
              this.state.shouldAnswerQuestionnaire
                ? () => this.showAlert()
                : this.handleMedicalRecordTap
            }>
            {this.state.shouldAnswerQuestionnaire ? (
              <H3Gray>{HealthStrings.MedicalRecord}</H3Gray>
            ) : (
              <H3>{HealthStrings.MedicalRecord}</H3>
            )}
          </Cell>
          <Cell
            leftThumb={Asset.Icon.Custom.Medication}
            rightThumb={Asset.Icon.Atomic.ChevronRight}
            tintColor={this.state.shouldAnswerQuestionnaire ? Color.Gray : null}
            onTap={
              this.state.shouldAnswerQuestionnaire
                ? () => this.showAlert()
                : this.handleMedicationTap
            }>
            {this.state.shouldAnswerQuestionnaire ? (
              <H3Gray>{HealthStrings.Medication}</H3Gray>
            ) : (
              <H3>{HealthStrings.Medication}</H3>
            )}
          </Cell>
          <Cell
            leftThumb={Asset.Icon.Custom.Calendar}
            rightThumb={Asset.Icon.Atomic.ChevronRight}
            tintColor={this.state.shouldAnswerQuestionnaire ? Color.Gray : null}
            onTap={
              this.state.shouldAnswerQuestionnaire
                ? () => this.showAlert()
                : this.handlePastEventsTap
            }>
            {this.state.shouldAnswerQuestionnaire ? (
              <H3Gray>{HealthStrings.PastEvents.Cell}</H3Gray>
            ) : (
              <H3>{HealthStrings.PastEvents.Cell}</H3>
            )}
          </Cell>
        </Root>
      </CollapsibleHeader>
    );
  }

  private handleChangeBeneficiary = () => {
    Navigation.push(this.props.componentId, AccountSwitchPage);
  };

  private handleVitalSignTap = (vitalSignType: VitalSignType) => {
    console.warn(this.props.componentId);
    Navigation.pop(this.props.componentId);
    Navigation.push<VitalSignChartPageProps>(
      this.props.componentId,
      VitalSignChartPage,
      { vitalSignType },
    );
  };

  private handleMedicalRecordTap = () => {
    Navigation.push(this.props.componentId, HealthMedicalRecordPage);
  };

  private handleMedicationTap = () => {
    Navigation.push(this.props.componentId, MedicationListPage);
  };

  private handlePastEventsTap = () => {
    Navigation.push(this.props.componentId, PastEventsPage);
  };

  private handleHealthQuestionnaireTap = () => {
    Navigation.push(this.props.componentId, CampaignsPage);
  };
}
