import { FlatList } from 'react-native';
import React from 'react';
import { View, StyleSheet, SafeAreaView } from 'react-native';
import { HBox } from '@atomic';
import { DD, H2, DT } from '@atomic/atm.typography';
import { LinkButton } from '@atomic/atm.button';
import { Cell } from '@atomic/mol.cell';
import { Asset } from '@atomic/obj.asset';

const Item = ({ price, pharmacy, date, leftThumb }) => (
  <View style={styles.container}>
    <Cell leftThumb={leftThumb}>
      <HBox wrap={false} vAlign="center" hAlign="flex-start">
        <HBox.Item grow={1}>
          <DD>{date}</DD>
        </HBox.Item>
        <HBox.Item grow={1} />
      </HBox>
      <H2>{price}</H2>
      <DT>{pharmacy}</DT>
      <LinkButton
        align="flex-start"
        key={'Ver Detalhes'}
        text={'Ver Detalhes'}
        onTap={() => {}}
      />
    </Cell>
  </View>
);

export function FinancialList(props) {
  const renderItem = ({ item }) => (
    <Item {...item} leftThumb={Asset.Icon.Common.Sifrao} />
  );

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList
        style={{ paddingLeft: 20 }}
        data={props.data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 16,
  },
});
