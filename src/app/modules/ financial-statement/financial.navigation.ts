import { FinancialDetailsPage } from './financial.details.page';
import { FinancialPage } from './financial.page';
import { Navigation } from '@app/core/navigation';
export const FinancialNavigation = {
  register: () => {
    Navigation.register(FinancialPage);
    Navigation.register(FinancialDetailsPage);
  },
};
