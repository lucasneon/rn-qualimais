import { DateUtils } from '@app/utils';
import axios from 'axios';
import pharmacyApi from '../pharmacy/pharmacy.api';

class FinancialApi {
  fetchFinancialData() {
    return pharmacyApi.getToken().then(token => {
      return axios
        .get('https://run.mocky.io/v3/9cfb2008-dc92-4d68-a716-4f40a2c39ea8', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then(response => {
          return response.data;
        })
        .then(response => {
          return this.parseResponse(response);
        });
    });
  }

  // fetchFinancialData() {
  //   return pharmacyApi.getToken().then(token => {
  //     return axios
  //       .get(
  //         'http://restQA.epharma.com.br/api/Movimento/cliente/pbm-online?page=0&from=2021-01-10&to=2021-09-22&fromTime=00:00:01&toTime=23:59:59&p_CPF=76795700860',
  //         {
  //           headers: {
  //             Authorization: `Bearer ${token}`,
  //           },
  //         },
  //       )
  //       .then(response => {
  //         console.log('Response > ' + response.data);
  //         return response.data;
  //       })
  //       .then(response => {
  //         return this.parseResponse(response);
  //       });
  //   });
  // }

  private parseResponse(response) {
    return response.data.map((data: any, key) => {
      return {
        key: key,
        lojaAdress: data.loja.lojaNome,
        lojaUF: data.loja.lojaUF,
        redeNome: data.loja.redeNome,
        valorTotal: this.calculateTotalValue(data.itens),
        dataVenda: DateUtils.Format.presentation(data.venda.dataVenda, false),
        itens: [...data.itens],
      };
    });
  }

  private calculateTotalValue(itens) {
    let result = itens.filter(item => {
      const valor = item.valorPMC;
      return valor;
    });
    let total = 0;
    result.forEach(e => {
      total += e.valorPMC;
    });
    return total.toFixed(2);
  }
}

export default new FinancialApi();
