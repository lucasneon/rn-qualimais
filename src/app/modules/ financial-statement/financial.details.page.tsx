import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import React from 'react';
import { View, StyleSheet, Text, FlatList, ScrollView } from 'react-native';
import { CollapsibleHeader as Collapsible } from '@atomic';
import { LargeHeader } from '@app/components/atm.header';
import { DividerGray, VSeparator } from '@atomic';
import { VBox } from '@atomic/obj.grid';

export interface FinancialDetailsPageProps {
  componentId: string;
  data: any;
}

interface FinancialDetailsPageState {}
@PageView('FinancialDetailsPage')
export class FinancialDetailsPage extends React.Component<
  NavigationPageProps<FinancialDetailsPageProps>,
  FinancialDetailsPageState
> {
  static options = {
    name: 'FinancialDetailsPage',
    topBar: { backButton: { visible: true } },
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {}

  render() {
    const data = this.props.navigation.data;

    return (
      <View style={styles.container}>
        <Collapsible.LargeHeader>
          <LargeHeader title={'Detalhes do extrato'} />
        </Collapsible.LargeHeader>
        <View style={{ flex: 1 }}>
          <ScrollView>
            <VBox>
              <View style={styles.cell}>
                <Text style={styles.label}>Valor da compra</Text>
                <Text style={styles.title}>{`R$ ${data.item.valorTotal}`}</Text>
              </View>
              <View style={styles.cell}>
                <Text style={styles.label}>Dados da rede</Text>
                <Text style={styles.subtitle}>{data.item.redeNome}</Text>
                <Text style={styles.text}>
                  {`${data.item.lojaAdress}\n${data.item.lojaUF}`}
                </Text>
              </View>
              <View style={styles.cell}>
                <Text style={styles.label}>Produtos da Compra</Text>
                <FlatList
                  data={data.item.itens}
                  scrollEnabled={false}
                  renderItem={this.renderItem}
                  keyExtractor={item => item}
                />
              </View>
            </VBox>
          </ScrollView>
        </View>
      </View>
    );
  }

  private renderItem = ({ item }) => {
    return (
      <View>
        <Text style={styles.subtitle}>{item.produto.produtoNome}</Text>
        <View style={styles.productRow}>
          <Text style={styles.text}>Composição</Text>
          <Text style={styles.product}>{item.produto.skuNome}</Text>
        </View>
        <View style={styles.productRow}>
          <Text style={styles.text}>Valor do produto</Text>
          <Text style={styles.product}>{`R$ ${item.valorPMC}`}</Text>
        </View>
        <View style={styles.productRow}>
          <Text style={styles.text}>Fabricante</Text>
          <Text style={styles.product}>{item.produto.industriaNome}</Text>
        </View>
        <VSeparator />
        <DividerGray />
        <VSeparator />
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000',
  },
  subtitle: {
    fontSize: 16,
    lineHeight: 20,
    fontWeight: 'bold',
    color: '#000',
    marginBottom: 10,
  },
  text: {
    fontSize: 14,
    lineHeight: 24,
    color: '#000',
  },
  label: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: 'bold',
    color: '#004071',
    marginBottom: 10,
  },
  product: {
    fontSize: 14,
    lineHeight: 14,
    color: '#004071',
  },
  productRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline',
  },
  cell: {
    marginTop: 10,
    borderColor: '#eeeeee',
    borderRadius: 10,
    borderWidth: 2,
    padding: 20,
  },
});
