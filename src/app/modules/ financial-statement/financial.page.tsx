import { PageView } from '@app/core/analytics';
import { GuardProps, Navigation } from '@app/core/navigation';
import React from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { GuardParams } from '../authentication';
import { CollapsibleHeader as Collapsible } from '@atomic';
import { LinkButton } from '@atomic';
import { LargeHeader } from '@app/components/atm.header';
import { HBox } from '@atomic';
import { DD, DT, H1 } from '@atomic/atm.typography';
import { Cell } from '@atomic/mol.cell';
import { Asset } from '@atomic/obj.asset';
import { FinancialDetailsPage } from './financial.details.page';
import FinancialApi from '@app/modules/ financial-statement/financial.api';

interface FinancialPageProps extends GuardProps<GuardParams> {
  componentId: string;
}

interface FinancialPageState {
  data: any[];
  loading: boolean;
}

@PageView('FinancialPage')
export class FinancialPage extends React.Component<
  FinancialPageProps,
  FinancialPageState
> {
  static options = {
    name: 'FinancialPage',
    topBar: { backButton: { visible: true } },
  };

  state = {
    data: [],
    loading: true,
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    this.fetchFinancialData();
  }

  private fetchFinancialData() {
    this.setState({ loading: true });
    FinancialApi.fetchFinancialData()
      .then(response => {
        this.setState({ data: response, loading: false });
      })
      .catch(() => {
        this.setState({ data: [], loading: false });
      });
  }

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            color="#009688"
            size="large"
            style={styles.container}
            animating
          />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <Collapsible.LargeHeader>
            <LargeHeader title={'Extrato'} />
          </Collapsible.LargeHeader>
          {!this.isEmpty() ? (
            <SafeAreaView style={{ flex: 1 }}>
              <FlatList
                data={this.state.data}
                renderItem={this.renderItem}
                keyExtractor={item => item}
              />
            </SafeAreaView>
          ) : (
            <View style={styles.containerEmpty}>
              <DT>Nenhum registro disponível</DT>
            </View>
          )}
        </View>
      );
    }
  }

  private isEmpty = () => {
    const list = this.state.data;
    return list.length === 0 || list === [] || list === null;
  };

  renderItem = (item: any) => {
    return (
      <View>
        <Cell leftThumb={Asset.Icon.Common.Sifrao} iconLarge={true}>
          <HBox wrap={false} vAlign="center" hAlign="flex-start">
            <HBox.Item grow={1}>
              <DD>{item.item.dataVenda}</DD>
            </HBox.Item>
            <HBox.Item grow={1} />
          </HBox>
          <H1>{'R$ ' + item.item.valorTotal ?? ''}</H1>
          <DT>{item.item.redeNome}</DT>
          <LinkButton
            align="flex-start"
            key={'Ver Detalhes'}
            text={'Ver Detalhes'}
            onTap={() => this.onClickDetails(item)}
          />
        </Cell>
      </View>
    );
  };

  private onClickDetails = item => {
    console.log('click');
    Navigation.push(this.props.componentId, FinancialDetailsPage, {
      data: item,
    });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerEmpty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
