import * as React from 'react';
import { Subscribe } from 'unstated';
import { Navigation, NavigationGuard } from '@app/core/navigation';
import {
  BeneficiaryProvider,
  BeneficiaryProviderState,
} from '@app/services/providers';
import { SplashPage } from '../splash';

export type GuardParams = BeneficiaryProviderState;

// Parameters can be used to filter access by roles. These roles can be passed as parameters in the route.
export function LoginGuard(_parameters?: any) {
  return class Guard extends NavigationGuard<GuardParams> {
    render() {
      return (
        <Subscribe to={[BeneficiaryProvider]}>
          {(beneficiaryProvider: BeneficiaryProvider) => {
            if (!beneficiaryProvider) {
              return null;
            }

            if (
              !beneficiaryProvider.isLogged() &&
              !beneficiaryProvider.isLoginActive
            ) {
              beneficiaryProvider.isLoginActive = true;
              Navigation.showModal(SplashPage);
              return null;
            }

            return (
              beneficiaryProvider.state.beneficiaries &&
              this.props.children(
                beneficiaryProvider.isLogged(),
                beneficiaryProvider.state,
              )
            );
          }}
        </Subscribe>
      );
    }
  };
}
