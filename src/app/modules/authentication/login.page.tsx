import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { PageView } from '@app/core/analytics';
import {
  GraphQLInterface,
  GraphQLProvider,
  GraphQLProviderToken,
} from '@app/core/graphql';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { LoginQueryVariables } from '@app/data/graphql';
import { ErrorMapper } from '@app/utils';
import { Root } from '@atomic';
import {
  ChangePasswordNavigationProps,
  ChangePasswordPage,
} from '../account/change-password';
import { ForgotPasswordPage } from './forgot-password';
import { LoginForm } from './login-form.component';
import { LoginContainer } from './login.container';
import {
  AcceptanceTermPage,
  AcceptanceTermNavigateProps,
} from '../account/acceptance-term';
import Container, { Container as TypeDIContainer } from 'typedi';
import { TermAccpetOfUser } from '@app/config/tokens.config';
import { WebViewTermosPage } from '../common/webview.termos.page';
import { ImageBackground, Linking, View } from 'react-native';
import LoginScreenImage from '@assets/images/login/login-bg-sreen.png';
import { Logo } from '@app/components/atm.logo';
const TermsOfUseLink = 'https://fusionhealthcare.com.br/termos/qualimais';

@PageView('Login')
export class LoginPage extends React.Component<NavigationPageProps<{}>> {
  static options = {
    name: 'LoginPage',
    topBar: {
      rightButtons: [],
      leftButtons: [],
    },
    modalPresentationStyle: 'fullScreen',
  };
  private graph: GraphQLProvider = TypeDIContainer.get(GraphQLProviderToken);

  private termAccpetOfUser = Container.get(TermAccpetOfUser);

  private htmlFile: string;
  private idValue: string;
  private isPetro: boolean;

  componentDidMount() {
    this.fetchQuestionnaires();
  }

  render() {
    this.isPetro = Boolean(this.termAccpetOfUser);
    return (
      <LoginContainer onSuccess={this.handleSuccess} onError={this.handleError}>
        {(
          loginFn: (variables: LoginQueryVariables) => void,
          state: GraphQLInterface,
        ) => {
          return (
            <Root>
              <ImageBackground
                source={LoginScreenImage}
                style={{ flex: 1 }}
                resizeMode="cover">
                <View style={{ paddingTop: 88 }}>
                  <Logo />
                  <LoginForm
                    loading={state.loading}
                    onSubmit={loginFn}
                    termOfConsentiment={this.isPetro}
                    idValue={this.idValue}
                    onIdValueChange={this.handleIdValueChange}
                    onForgotPassword={this.handleForgotPassword}
                    onTermsOfUseClick={this.handleTermsOfUseLinkTap}
                    onTermsOfAcceptClick={this.handleTermsOfAcceptLinkTap}
                  />
                </View>
              </ImageBackground>
            </Root>
          );
        }}
      </LoginContainer>
    );
  }

  private handleSuccess = async (
    mustChangePassword: boolean,
    consentTerm: string,
  ) => {
    if (this.termAccpetOfUser) {
      if (consentTerm === 'N') {
        Navigation.showModal<AcceptanceTermNavigateProps>(AcceptanceTermPage, {
          mustChangePassword: mustChangePassword,
          html: this.htmlFile,
          onSuccess: (message: string) =>
            FlashMessageService.showSuccess(message),
        });
      } else if (mustChangePassword) {
        this.handleFirstPasswordChange();
      } else {
        await Navigation.dismissAllModals();
      }
    } else {
      if (mustChangePassword) {
        this.handleFirstPasswordChange();
      } else {
        await Navigation.dismissAllModals();
      }
    }
  };

  private handleFirstPasswordChange = async () => {
    Navigation.showModal<ChangePasswordNavigationProps>(ChangePasswordPage, {
      mustChangePassword: true,
      onSuccess: (message: string) => FlashMessageService.showSuccess(message),
    });

    await Navigation.dismissModal(this.props.componentId);
  };

  private handleForgotPassword = async () => {
    Navigation.push(this.props.componentId, ForgotPasswordPage, {
      initialUsername: this.idValue,
      onSuccess: (message: string) => FlashMessageService.showSuccess(message),
    });
  };

  private handleError = (apolloError: ApolloError): void => {
    FlashMessageService.showError(ErrorMapper.map(apolloError));
  };

  private handleIdValueChange = value => {
    this.idValue = value;
  };

  private handleTermsOfUseLinkTap = () => {
    Navigation.showModal(WebViewTermosPage, { url: TermsOfUseLink });
  };

  private handleTermsOfAcceptLinkTap = () => {
    Navigation.showModal(WebViewTermosPage, { url: TermsOfUseLink });
    // Navigation.showModal(WebHtmlPage, { url: this.htmlFile });
  };

  async fetchQuestionnaires() {
    //FIXME retorna null, verificar endpoint
    // await this.graph.query<String>(
    //   'term',
    //   null,
    //   this.onQuerySuccess,
    //   this.onQueryError,
    // );
  }
  private onQuerySuccess = (resp: any) => {
    let s = JSON.stringify(resp);
    let o = JSON.parse(s);
    console.warn('term >', o.Term);
    this.htmlFile = o.Term;
  };

  private onQueryError = (error: ApolloError) => {
    console.warn(error);
  };
}
