import {
  GAMA_OP_CODE,
  GAMA_STIMULATING_CODE,
  GAMA_SYSTEM_CODE,
} from '@app/config';
import { AcceptTermOfUseInput } from '@app/data/graphql';
import Container from 'typedi';
import {
  AcceptTermOfUseInputRequest,
  AcceptTermOfUseRequest,
} from './accept-term-of-use.model';
import { axiosPost, BaseUrl } from './http-requests';

const opCode: string = Container.get(GAMA_OP_CODE);
const stimulantingCode: string = Container.get(GAMA_STIMULATING_CODE);
const systemCode: string = Container.get(GAMA_SYSTEM_CODE);
// export class AcceptTermOfUseUsecase implements UseCase<InputParams<AcceptTermOfUseInput>, void> {
//     constructor(private readonly datasource: AuthenticationHttpDataSource) {}

//     exec(acceptTermOfUse: AcceptTermOfUseInput): Promise<void> {
//       return this.datasource.acceptTermOfUse(acceptTermOfUse);
//     }

//     update(acceptTermOfUse: AcceptTermOfUseInput): Promise<void> {
//       return this.datasource.acceptTermOfUseInput(acceptTermOfUse);
//     }
//   }

// fetchAcceptTermOfUse(){
//   let res: { data: any };
//   try {
//     res = await axiosPost('/beneficiario/termo-aceite', data);
//   } catch (e) {
//     console.error(e.message);
//     this.handleError(e);
//   }
// }

export const acceptTermOfUseInput = async (
  acceptTermOfUse: AcceptTermOfUseInput,
) => {
  const req: AcceptTermOfUseInputRequest = {
    codigoBeneficiario: acceptTermOfUse.codeBenefiary,
    codigoOperadora: opCode,
    codigoEstipulante: stimulantingCode,
    codigoSistema: systemCode,
    usuario: acceptTermOfUse.use,
    termoAceito: 'S',
    termoConsentimento: acceptTermOfUse.consentTerm,
  };

  const res = await acceptTermOfUseRequest(req);

  if (res?.data?.codigoErro || !res?.data) {
    throw new Error(res.error);
  }

  return;
};

const acceptTermOfUseRequest = async (req: AcceptTermOfUseRequest) => {
  let res;
  try {
    res = await axiosPost({
      url: '/beneficiario/termo-aceite',
      data: req,
      baseUrl: BaseUrl.GAMA_AUTH_URL,
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};
