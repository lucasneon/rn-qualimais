import { FamilyMemberResponse } from '@app/model/api/family-member.model';
import { axiosPost, BaseUrl } from './http-requests';

export const fetchFamilyGroup = async (codigoBeneficiario: string) => {
  const data = {
    codigoBeneficiario,
    codigoOperadora: '39',
    codigoSistema: '2',
  };
  let res: { data: any };
  try {
    res = await axiosPost({
      url: '/grupo-familiar/lista-grupo-familiar',
      data,
      baseUrl: BaseUrl.GAMA_AUTH_URL,
    });
  } catch (e) {
    console.error(e.message);
    return { data: null, error: e };
  }

  const membersResponse: FamilyMemberResponse[] = res.data;

  const mapMember = (memberRes: FamilyMemberResponse) => {
    return {
      beneficiaryCode:
        memberRes.codigoBeneficiario && memberRes.codigoBeneficiario.toString(),
      name: memberRes.nomeBeneficiario,
      dependant: memberRes.vinculo === 'DEPENDENTE',
    };
  };

  

  const familyMembers = membersResponse && membersResponse.map(mapMember);
  return { data: familyMembers, error: false };
};
