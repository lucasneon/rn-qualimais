import { axiosPost, BaseUrl } from './http-requests';
import {
  AddressModel,
  BeneficiaryInfoMapper,
  BeneficiaryInfoModel,
} from '@app/model/api/beneficiary-info.model';
import { LocalDataSource } from '@app/data';
import Container from 'typedi';
import { InputParams } from '@app/model/api/input.params.model';
import { ChangeBeneficiaryInfoRequest } from '@app/model/api/beneficiary-info.entity';
import { GAMA_NETWORK_CODE, GAMA_OP_CODE, GAMA_SYSTEM_CODE } from '@app/config';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);
const opCode: string = Container.get(GAMA_OP_CODE);
const systemCode: string = Container.get(GAMA_SYSTEM_CODE);
const networkCode: string = Container.get(GAMA_NETWORK_CODE);

const fetchBeneficiaryInfo = async (codigoBeneficiario: string) => {
  const data = {
    codigoBeneficiario,
    codigoOperadora: '39',
    codigoSistema: '2',
  };
  let res: { data: any };
  try {
    res = await axiosPost({
      url: '/beneficiario/busca-dados-cadastrais',
      data,
      baseUrl: BaseUrl.GAMA_AUTH_URL,
    });
  } catch (e) {
    console.error(e.message);
    return { data: null, error: e };
  }

  if (res.data.length > 0) {
    const beneficiaryInfo = BeneficiaryInfoMapper.mapResponse(res.data[0]);
    checkEssentialData(beneficiaryInfo);
    return { data: beneficiaryInfo, error: null };
  }

  return { data: null, error: false };
};

export const fetchAuthBeneficiaryInfo = async (beneficiaryCode: string) => {
  const beneficiaryInfoResult = await fetchBeneficiaryInfo(beneficiaryCode);

  if (beneficiaryInfoResult.error) {
    return { data: null, error: beneficiaryInfoResult.error };
  }

  const beneficiaryInfo: BeneficiaryInfoModel = beneficiaryInfoResult.data;
  const [cpf, idHealthCareCard] = [
    beneficiaryInfo.cpf,
    beneficiaryInfo.idHealthCareCard,
  ];
  const token: string = await localDataSource.get('access_token');

  const authBeneficiaryInfo: BeneficiaryInfoModel = {
    token,
    idHealthCareCard,
    beneficiaryCode,
    cpf,
    name: beneficiaryInfo.name,
    email: beneficiaryInfo.email,
    city: beneficiaryInfo.address.city,
    state: beneficiaryInfo.address.state,
    birthDate: beneficiaryInfo.birthDate,
    dependant: null,
    phone: beneficiaryInfo.phone,
    address: {
      ...beneficiaryInfo.address,
    },
  };

  return { data: authBeneficiaryInfo, error: false };
};

const checkEssentialData = (beneficiaryInfo: BeneficiaryInfoModel) => {
  const requiredData: { [field: string]: string } = {
    name: beneficiaryInfo.name,
    idHealthCareCard: beneficiaryInfo.idHealthCareCard,
    beneficiaryCode: beneficiaryInfo.beneficiaryCode,
    insurerId: beneficiaryInfo.insurerId,
    state: beneficiaryInfo.address.state,
    city: beneficiaryInfo.address.city,
  };

  const checkKey = key => {
    if (!requiredData[key]) {
      console.error(key);
      // throw new CustomError('app.authentication.error.info', 500, `Missing '${key}' value on beneficiaryInfo`);
    }
  };
  Object.keys(requiredData).forEach(checkKey);
};

export const storageBeneficiaryCode = async (beneficiaryCode: string) => {
  try {
    await Promise.all([
      localDataSource.set<string>('beneficiaryCode', beneficiaryCode),
    ]);
  } catch (e) {
    console.error(e);
  }
};

export const storageIdHealthCareCard = async (idHealthCareCard: string) => {
  try {
    await Promise.all([
      localDataSource.set<string>('idHealthCareCard', idHealthCareCard),
    ]);
  } catch (e) {
    console.error(e);
  }
};

export const storageCpf = async (cpf: string) => {
  try {
    await Promise.all([localDataSource.set<string>('cpf', cpf)]);
  } catch (e) {
    console.error(e);
  }
};

export interface UserData {
  idHealthCareCard: string;
  beneficiaryCode: string;
  appointmentId?: string;
  insurerId: string;
  hasCarePlan?: any;
  stipulating?: number;
  operatorCode?: number;
  cpf?: string;
}

export interface BeneficiaryInfoUpdateModel {
  email?: string;
  phone?: string;
  address?: Partial<AddressModel>;
}
const execBeneficiaryinfo = async (beneficiaryCode: string) => {
  const beneficiaryInfo = await fetchBeneficiaryInfo(beneficiaryCode);
  checkEssentialData(beneficiaryInfo.data);
  return beneficiaryInfo;
};

export const execUpdateBeneficiaryInfo = async (
  input: any,
): Promise<BeneficiaryInfoModel> => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const info = await execBeneficiaryinfo(beneficiaryCode);
  joinFieldsToUpdate(info.data, input);

  await changeBeneficiaryInfo(info.data);

  return info.data;
};

const changeBeneficiaryInfo = async (input: any) => {
  const DefaultCns = '0';
  const DefaultSex = 'U';
  const DefaultMaritalStatus = '1';
  if (!input.gender) {
    input.gender = DefaultSex;
  }

  if (!input.maritalStatus) {
    input.maritalStatus = DefaultMaritalStatus;
  }

  if (!input.planData?.cns) {
    input.planData.cns = DefaultCns;
  }
  const serviceCodes = {
    codigoOperadora: opCode,
    codigoSistema: systemCode,
    codigoRede: networkCode,
  };
  try {
    await changeBeneficiaryInfoRequest(
      BeneficiaryInfoMapper.mapRequest(input, serviceCodes),
    );
  } catch (e) {
    console.error(e);
    throw new Error(e);
  }

  return;
};

const changeBeneficiaryInfoRequest = async (
  req: ChangeBeneficiaryInfoRequest,
) => {
  let res;
  try {
    res = await axiosPost({
      url: '/beneficiario/alteracao-cadastral',
      baseUrl: BaseUrl.GAMA_AUTH_URL,
      data: req,
    });
  } catch (error) {
    throw new Error(error);
  }
  return res;
};

function joinFieldsToUpdate(
  info: BeneficiaryInfoModel,
  input: InputParams<BeneficiaryInfoUpdateModel>,
) {
  if (input.phone) {
    info.phone = input.phone;
  }

  if (input.email) {
    info.email = input.email;
  }

  const address = input.address;
  if (address) {
    Object.keys(address).forEach(key => {
      if (address[key]) {
        info.address[key] = address[key];
      }
    });
  }
}
