import { LoginQueryVariables } from '@app/data/graphql';
import { LocalDataSource } from '@app/data';
import axios from 'axios';
import Container from 'typedi';
import {
  GAMA_CLIENT,
  GAMA_CLIENT_PASSWORD,
  GAMA_CLIENT_USRNAME,
  GAMA_OP_CODE,
  GAMA_PASSWORD,
  GAMA_SYSTEM_CODE,
  GAMA_USER,
} from '@app/config';
import { axiosPost, BaseUrl } from './http-requests';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);
const opCode: string = Container.get(GAMA_OP_CODE);
const systemCode: string = Container.get(GAMA_SYSTEM_CODE);

export const requestLogin = async (variables: LoginQueryVariables) => {
  await getToken();
  const data = {
    codigoOperadora: opCode,
    codigoSistema: systemCode,
    senha: variables.data.password,
    usuario: variables.data.user,
  };

  let res: { data: any };
  try {
    res = await axiosPost({
      url: '/beneficiario/autenticar-usuario',
      data: data,
      baseUrl: BaseUrl.GAMA_AUTH_URL,
    });
  } catch (e) {
    console.error(e.response);
    return { data: null, error: e };
  }

  const result = {
    beneficiaryCode: res.data.codigoBeneficiario,
    mustChangePassword: res.data.senhaAlterada !== 'S',
    didAcceptTermOfUse: res.data.termoAceito === 'S',
    consentTerm: res.data.termoConsentimento,
    hasCarePlan: res.data.possuiGestaoCuidado === 'S',
    stipulating: res.data.codigoEstipulante,
  };

  return { data: result, error: false };
};

export function getToken() {
  const username: string = Container.get(GAMA_USER);
  const password: string = Container.get(GAMA_PASSWORD);
  const client: string = Container.get(GAMA_CLIENT);
  const auth_username: string = Container.get(GAMA_CLIENT_PASSWORD);
  const auth_password: string = Container.get(GAMA_CLIENT_USRNAME);

  const params = {
    client,
    username,
    password,
    grant_type: 'password',
  };
  const auth = {
    username: auth_username,
    password: auth_password,
  };
  const data = Object.keys(params)
    .map(key => `${key}=${encodeURIComponent(params[key])}`)
    .join('&');

  return axios({
    method: 'post',
    url: BaseUrl.GAMA_OAUTH_URL,
    data: data,
    // auth,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: 'Basic ZnVzaW9uOmZ1c2lvbg==',
    },
  })
    .then(async response => {
      // console.warn('TOKEN', response.data);
      await Promise.all([
        localDataSource.set<string>('access_token', response.data.access_token),
        localDataSource.set<string>('id_pweb', response.data?.id_pweb || ''),
      ]);
      return response.data.access_token;
    })
    .catch(err => {
      console.error(err);
      console.log(err.response);
      throw new Error(err);
    });
}
