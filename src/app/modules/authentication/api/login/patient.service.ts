import { PRONTLIFE_ACCESS_TOKEN } from '@app/config';
import {
  ProntlifeUtils,
  requestProntlifeHeaders,
} from '@app/utils/prontilife.utils';
import { Container } from 'typedi';
import { UserData } from './beneficiary.service';
import { axiosGet, BaseUrl } from './http-requests';
import { LocalDataSource } from '@app/data';

const prontlifeToken = Container.get(PRONTLIFE_ACCESS_TOKEN);
const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchPatient = async (
  insurerId: string,
  idHealthCareCard: string,
): Promise<string> => {
  const input: Partial<UserData> = {
    insurerId: insurerId || '55879',
    idHealthCareCard,
  };
  // tslint:disable-next-line: max-line-length
  const res = await axiosGet({
    url: '/fhir/Patient',
    params: {
      identifier: ProntlifeUtils.FormatterPatient.insurerParam(input),
    },
    baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
    headers: requestProntlifeHeaders(prontlifeToken),
  });
  if (!res.data || res.data.total === 0) {
    // TODO Ajustar, comentado porque esse estipulante não está cadastrado na prontlife
    //throw new DataSourceError('app.authentication.error.not-found', 500, res);
  }

  try {
    await localDataSource.set<string>(
      'appointmentId',
      res.data.entry[0].resource.id,
    );
    return res.data.entry[0].resource.id;
  } catch (error) {
    // TODO Ajustar, comentado porque esse estipulante não está cadastrado na prontlife
    //throw new RepositoryError(null, 'Error parsing Prontlife Patient data');
  }
};
