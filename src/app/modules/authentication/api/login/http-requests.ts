import { Container } from 'typedi';
import axios from 'axios';
import { LocalDataSource } from '@app/data';
import {
  PRONTLIFE_BASE_URL,
  LIVE_TOUCH_BASE_URL,
  GAMA_FUSION_GPS,
  GAMA_CARE_PLAN_URL,
  GAMA_AUTH_URL,
  GAMA_OAUTH_URL,
} from '@app/config';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const axiosPost = async (
  data: {
    url: string;
    data?: any;
    baseUrl: string;
  },
  headers: any = {},
) => {
  const token = await localDataSource.get('access_token');
  console.log(data);
  console.warn('axiosPost > ', JSON.stringify(data));
  return axios({
    method: 'post',
    url: `${data.baseUrl}${data.url}`,
    data: data.data,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers,
    },
  }).catch(err => {
    console.error('Error axiosPost > ' + data.url, err, data);
    return { data: null, error: err };
  });
};

export const axiosPut = async (data: {
  url: string;
  data?: any;
  baseUrl: string;
}) => {
  const token = await localDataSource.get('access_token');
  console.log(data);
  console.warn('axiosPut > ', JSON.stringify(data));
  return axios({
    method: 'put',
    url: `${data.baseUrl}${data.url}`,
    data: data.data,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  }).catch(err => {
    console.error('Error axiosPut > ' + data.url, err, data);
    return { data: null };
  });
};

export const axiosGet = async (data: {
  url: string;
  params?: any;
  baseUrl: string;
  headers?: any;
}) => {
  const token = await localDataSource.get('access_token');
  console.warn('axiosGet > ', JSON.stringify(data));
  const response = await axios({
    method: 'get',
    url: `${data.baseUrl}${data.url}`,
    params: data.params,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...data.headers,
    },
  }).catch(err => {
    console.error('Error axiosGet > ' + data.url, err, data);
    return { data: null };
  });

  console.warn('axiosGet Response > ', JSON.stringify(response.data));
  return response;
};

const prontLifeUrl: string = Container.get(PRONTLIFE_BASE_URL);
const CHAT_BASE_URL: string = Container.get(LIVE_TOUCH_BASE_URL);
const gamaFusionGps: string = Container.get(GAMA_FUSION_GPS);
const gamaCarePlanUrl: string = Container.get(GAMA_CARE_PLAN_URL);
const gamaAuthUrl: string = Container.get(GAMA_AUTH_URL);
const oauth: string = Container.get(GAMA_OAUTH_URL);
export const BaseUrl = {
  GAMA_OAUTH_URL: oauth,
  GAMA_AUTH_URL: gamaAuthUrl,
  GAMA_CARE_PLAN_URL: gamaCarePlanUrl,
  GAMA_FUSION_GPS: gamaFusionGps,
  PRONTLIFE_BASE_URL: prontLifeUrl,
  LIVE_TOUCH_BASE_URL: CHAT_BASE_URL,
};
