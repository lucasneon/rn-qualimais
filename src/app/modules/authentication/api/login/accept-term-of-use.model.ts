export interface AcceptTermOfUseRequest {
  codigoBeneficiario: string;
  usuario: string;
  codigoOperadora: string;
  codigoSistema: string;
  codigoEstipulante: string;
  termoAceito: 'S' | 'N';
}

export interface AcceptTermOfUseInputRequest {
  codigoBeneficiario: string;
  usuario: string;
  codigoOperadora: string;
  codigoSistema: string;
  codigoEstipulante: string;
  termoAceito: 'S' | 'N';
  termoConsentimento: string;
}
