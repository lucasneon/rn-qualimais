import * as React from 'react';
import { Container } from 'typedi';
import { Subscribe } from 'unstated';
import { ProvidersConfig } from '@app/config';
import { GraphQLInterface, GraphQLProvider } from '@app/core/graphql';
import {
  AuthenticationFragment,
  LoginQuery,
  LoginQueryVariables,
} from '@app/data/graphql';
import { RegisterPushNotificationUseCase } from '@app/domain/push-notification';
import { Beneficiary } from '@app/model';
import {
  BeneficiaryProvider,
  FacialRecognitionProvider,
} from '@app/services/providers';
import { AuthenticationMapper } from './authentication.mapper';
import { FamilyMemberModel } from '@app/model/api/authentication.model';

import {
  fetchAuthBeneficiaryInfo,
  storageBeneficiaryCode,
  storageCpf,
  storageIdHealthCareCard,
} from './api/login/beneficiary.service';
import { requestLogin } from './api/login/login.service';
import { fetchFamilyGroup } from './api/login/family-group.service';
import { fetchPatient } from './api/login/patient.service';

export interface LoginContainerProps {
  onSuccess: (
    isRegisteredInBiometricService: boolean,
    consentTerm: string,
  ) => void;
  onError: (error: any) => void;
  children: (
    loginFn: (variables: LoginQueryVariables) => void,
    state: GraphQLInterface,
  ) => React.ReactNode;
}

export class LoginContainer extends React.Component<LoginContainerProps> {
  private graph: GraphQLProvider;
  private beneficiaryProvider: BeneficiaryProvider;
  private facialRecognitionProvider: FacialRecognitionProvider;
  private providersConfig: ProvidersConfig = Container.get(ProvidersConfig);
  private pushNotificationRegister: RegisterPushNotificationUseCase =
    Container.get(RegisterPushNotificationUseCase);

  componentWillUnmount() {
    this.beneficiaryProvider.isLoginActive = false;
  }
  componentDidMount() {
    this.graph.setState({ loading: false });
  }
  render() {
    return (
      <Subscribe
        to={[GraphQLProvider, BeneficiaryProvider, FacialRecognitionProvider]}>
        {(
          graph: GraphQLProvider,
          beneficiary: BeneficiaryProvider,
          facialRecognition: FacialRecognitionProvider,
        ) => {
          this.beneficiaryProvider = beneficiary;
          this.graph = graph;
          this.facialRecognitionProvider = facialRecognition;

          return this.props.children(this.loginFn, this.graph.state);
        }}
      </Subscribe>
    );
  }

  private handleLoginSuccess = async (loginQuery: LoginQuery) => {
    const data: AuthenticationFragment = loginQuery.Login;

    const activeBeneficiary: Beneficiary = AuthenticationMapper.map(data);
    let allBeneficiaries: Beneficiary[] = [activeBeneficiary];
    const dependants: Beneficiary[] =
      data.dependants && data.dependants.map(AuthenticationMapper.map);

    if (dependants) {
      allBeneficiaries = allBeneficiaries.concat(dependants);
    }

    await Promise.all([
      this.beneficiaryProvider.setBeneficiaries(
        activeBeneficiary,
        allBeneficiaries,
      ),
      this.facialRecognitionProvider.setState({
        isRegistered: data.isRegisteredInBiometricService,
      }),
    ]);

    try {
      this.pushNotificationRegister.execute();
    } catch (err) {}
    this.providersConfig.handleContextChange();

    this.props.onSuccess(data.mustChangePassword, data.consentTerm);
  };

  handleError = (error: any) => {
    this.beneficiaryProvider.logout();

    if (this.props.onError) {
      this.props.onError(error);
    }
  };

  private loginFn = async (variables: LoginQueryVariables) => {
    this.graph.setState({ loading: true });
    await this.fetchLogin(variables);
    // this.graph.query<LoginQueryVariables>(
    //   'login',
    //   variables,
    //   this.handleLoginSuccess,
    //   this.handleError,
    // );
  };

  private fetchLogin = async (variables: LoginQueryVariables) => {
    try {
      const auth = await requestLogin(variables);
      if (auth.error) {
        this.graph.setState({ loading: false });
        return this.handleError(auth.error);
      }
      const beneficiaryCode = auth.data.beneficiaryCode;
      const familyGroup = await fetchFamilyGroup(beneficiaryCode);
      if (familyGroup.error) {
        return this.handleError(familyGroup.error);
      }

      const beneficiary: FamilyMemberModel = familyGroup.data.find(
        member => member.beneficiaryCode === beneficiaryCode,
      );
      await storageBeneficiaryCode(beneficiaryCode);
      const dependants: FamilyMemberModel[] = familyGroup.data.filter(
        dep => dep.beneficiaryCode !== beneficiaryCode,
      );

      const [authBeneficiary, authDependants]: [any, any[]] = await Promise.all(
        [
          fetchAuthBeneficiaryInfo(beneficiaryCode),
          this.authenticateDependants(dependants),
        ],
      );
      if (authBeneficiary.error || authDependants.some(a => a.error)) {
        this.handleError(
          authBeneficiary.error ||
            authDependants?.find(e => e.error != null).error ||
            'Sem dependentes',
        );
      }

      await fetchPatient('1', authBeneficiary.data.idHealthCareCard);

      await storageIdHealthCareCard(authBeneficiary.data.idHealthCareCard);
      await storageCpf(authBeneficiary.data.cpf);
      authBeneficiary.data.dependant = beneficiary.dependant;

      let mapAuthDependents = [];
      if (authDependants) {
        mapAuthDependents = authDependants.map(dep => {
          return {
            ...dep.data,
            dependant: true,
          };
        });
      }

      const response: LoginQuery = {
        Login: {
          ...authBeneficiary.data,
          dependants: mapAuthDependents,
          mustChangePassword: auth.data.mustChangePassword,
          consentTerm: auth.data.consentTerm,
          hasCarePlan: auth.data.hasCarePlan,
          stipulating: auth.data.stipulating,
        },
      };
      console.warn('loginResponse', response);
      this.graph.setState({ loading: false });
      this.handleLoginSuccess(response);
    } catch (e) {
      console.warn(e);
      this.graph.setState({ loading: false });
      this.handleError(e);
    }
  };

  authenticateDependants = async (
    dependants: FamilyMemberModel[],
  ): Promise<any[]> => {
    return dependants.length > 0
      ? Promise.all(
          dependants.map(dep => fetchAuthBeneficiaryInfo(dep.beneficiaryCode)),
        )
      : [];
  };
}
