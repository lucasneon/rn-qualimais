import { Navigation } from '@app/core/navigation';
import { forbidBackPage } from '@app/core/navigation/forbid-back.page.hoc';
import { BiometryConfirmationPage, BiometryPage } from './biometry';
import { ForgotPasswordPage } from './forgot-password';
import { LoginPage } from './login.page';
import { AcceptanceTermPage } from '../account/acceptance-term';
import { SplashPage } from '../splash';

export const AuthenticationNavigation = {
  register: () => {
    Navigation.register(BiometryConfirmationPage);
    Navigation.register(BiometryPage);
    Navigation.register(forbidBackPage(LoginPage));
    Navigation.register(forbidBackPage(SplashPage));
    Navigation.register(ForgotPasswordPage);
    Navigation.register(AcceptanceTermPage);
  },
};
