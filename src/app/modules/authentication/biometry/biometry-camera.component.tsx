import * as React from 'react';
import { AlertButton, Linking, View } from 'react-native';
import { RNCamera, TakePictureOptions, TakePictureResponse } from 'react-native-camera';
import { AlertBuilder } from '@app/components/atm.alert-builder';
import { PlaceholderSelector } from '@app/components/mol.placeholder/placeholder-selector.component';
import { AppError, AppErrorType } from '@app/model';
import { VBox, VSeparator } from '@atomic';
import { PrimaryButton, SecondaryButton } from '@atomic/atm.button/button-types.component';
import { BiometryStrings } from './biometry.strings';

interface BiometryCameraProps {
  enabled: boolean;
  onPictureTaken: (picture: TakePictureResponse) => void;
  onDoLater?: () => void;
}

interface ByometryCameraState {
  processingImage: boolean;
}

const NotAuthorizedPlaceholder = () => {
  const error: AppError = {
    title: BiometryStrings.NotAuthorized.Title,
    message: BiometryStrings.NotAuthorized.Message,
    type: AppErrorType.Camera,
  };

  return <PlaceholderSelector error={error} />;
};

export class BiometryCamera extends React.PureComponent<BiometryCameraProps, ByometryCameraState> {

  private cameraRef: RNCamera;

  constructor(props) {
    super(props);
    this.state = { processingImage: false };
  }

  render() {
    return (
      <>
        {this.props.enabled &&
          <RNCamera
            ref={ref => this.cameraRef = ref}
            notAuthorizedView={NotAuthorizedPlaceholder()}
            captureAudio={false}
            type={RNCamera.Constants.Type.front}
            flashMode={RNCamera.Constants.FlashMode.off}
          >
            <View style={{ height: 300, alignItems: 'stretch', flex: 1 }} />
          </RNCamera>
        }
        <VSeparator />
        <VBox>
          <PrimaryButton
            expanded={true}
            text={BiometryStrings.TakePicture}
            onTap={this.handleTakePictureTap}
            loading={this.state.processingImage}
          />
        </VBox>
        <VSeparator />
        <VBox>
          {this.props.onDoLater &&
            <SecondaryButton
              expanded={true}
              text={BiometryStrings.DoLater}
              onTap={this.props.onDoLater}
            />
          }
        </VBox>
        <VSeparator />
      </>
    );
  }

  private handleTakePictureTap = async () => {
    const authorized = this.cameraRef && this.cameraRef.state && (this.cameraRef.state as any).isAuthorized;

    if (authorized) {
      const options: TakePictureOptions = { quality: 0.8, base64: true, mirrorImage: true, width: 400 };
      this.setState({ processingImage: true });
      const picture: TakePictureResponse = await this.cameraRef.takePictureAsync(options);
      this.setState({ processingImage: false });
      this.props.onPictureTaken(picture);
    } else {
      this.showNotAuthroziedAlert();
    }
  }

  private showNotAuthroziedAlert() {
    const cancelButton: AlertButton = { text: BiometryStrings.NotAuthorized.Cancel, style: 'cancel' };
    const settingsButton: AlertButton = {
      text: BiometryStrings.NotAuthorized.GoToSetting,
      onPress: () => Linking.openSettings,
      style: 'default',
    };

    new AlertBuilder()
      .withTitle(BiometryStrings.NotAuthorized.AlertTitle)
      .withMessage(BiometryStrings.NotAuthorized.AlertMessage)
      .withButton(cancelButton)
      .withButton(settingsButton)
      .show();
  }
}
