import * as React from 'react';
import { BiometryPhotoStyled } from './biometry-photo.componet.style';

export const BiometryPhoto = props => (
  <BiometryPhotoStyled
    style={{ alignSelf: 'stretch', resizeMode: 'cover', width: '100%', height: 300 }}
    {...props}
  />
);
