export const BiometryStrings = {
  Confirmation: {
    Title: 'Veja como ficou',
    PrimaryAction: 'Enviar foto',
    SecondaryAction: 'Tirar outra',
  },
  NotAuthorized: {
    Title: 'Permissão para utilizar a câmera',
    Message: 'Para utilizar o reconhecimento facial, precisamos que você habilite a câmera.',
    AlertTitle: 'Habilite a câmera',
    AlertMessage: 'Nas configurações do aplicativo, procure pelas permissões e habilite a câmera',
    GoToSetting: 'Configurações',
    Cancel: 'Cancelar',
  },
  DoLater: 'Cadastrar Depois',
  Register: {
    Title: 'Reconhecimento facial',
    IntroductionText: 'Você não precisa mais assinar as guias na sua consulta.\n' +
      'Cadastrando sua biometria, você poderá autorizar a consulta sem burocracia.',
  },
  TakePicture: 'Tira foto',
};
