import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { TakePictureResponse } from 'react-native-camera';
import { Subscribe } from 'unstated';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { GraphQLProvider } from '@app/core/graphql';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { RegisterUserImageMutation, RegisterUserImageMutationVariables } from '@app/data/graphql';
import { FacialRecognitionProvider } from '@app/services/providers';
import { ErrorMapper } from '@app/utils';
import { PrimaryButton, Root, SecondaryButton, VBox, VSeparator } from '@atomic';
import { BiometryPhoto } from './biometry-photo.component';
import { BiometryStrings } from './biometry.strings';

export interface BiometryConfirmationPageNavigationProps {
  picture: TakePictureResponse;
  isRightAfterLogin: boolean;
}

interface BiometryConfirmationPageState {
  loading: boolean;
}

type PageProps = NavigationPageProps<BiometryConfirmationPageNavigationProps>;

@PageView('BiometryConfirmation')
export class BiometryConfirmationPage extends React.Component<PageProps, BiometryConfirmationPageState> {
  static options = { name: 'BiometryConfirmationPage' };

  private graphQLProvider: GraphQLProvider;
  private facialRecognitionProvider: FacialRecognitionProvider;

  constructor(props) {
    super(props);
    this.state = { loading: false };
    if (props.navigation.isRightAfterLogin) {
      Navigation.mergeOptions(props.componentId, { topBar: { rightButtons: [], }, });
    }
  }

  render() {
    const picture = this.props.navigation.picture;

    return (
      <Root>
        <Subscribe to={[GraphQLProvider, FacialRecognitionProvider]}>
          {(graphQLProvider: GraphQLProvider, facialRecProvider: FacialRecognitionProvider) => {
            this.graphQLProvider = graphQLProvider;
            this.facialRecognitionProvider = facialRecProvider;
            return (
              <CollapsibleHeader title={BiometryStrings.Confirmation.Title}>
                <Root>
                  <VBox>
                    {picture && <BiometryPhoto source={picture} />}
                    <VSeparator />
                    <PrimaryButton
                      expanded={true}
                      text={BiometryStrings.Confirmation.PrimaryAction}
                      onTap={this.handleSendPicture}
                      loading={this.state.loading}
                    />
                    <VSeparator />
                    <SecondaryButton
                      expanded={true}
                      text={BiometryStrings.Confirmation.SecondaryAction}
                      onTap={this.handleTakeAnotherPicture}
                    />
                  </VBox>
                </Root>
              </CollapsibleHeader>
            );
          }}
        </Subscribe>
      </Root>
    );
  }

  private handleSendPicture = () => {
    const picture = this.props.navigation.picture;
    this.setState({ loading: true });

    if (picture) {
      const variables: RegisterUserImageMutationVariables = { data: { encodedImage: picture.base64 } };
      this.graphQLProvider.mutate('register-user-image', variables, this.onRegisterCompleted, this.handleError);
    }
  }

  private onRegisterCompleted = (data: RegisterUserImageMutation) => {
    const registerRes = data.RegisterUserImage;

    if (registerRes.wasUserRecognized) {
      FlashMessageService.showSuccess(registerRes.statusText);
      this.facialRecognitionProvider.rehydrate();
      this.dismiss();
    } else {
      FlashMessageService.showErrorMessage(registerRes.statusText);
    }

    this.setState({ loading: false });
  }

  private async dismiss() {
    if (this.props.navigation.isRightAfterLogin) {
      await Navigation.dismissAllModals();
    } else {
      await Navigation.popToRoot(this.props.componentId);
    }
  }

  private handleError = (error?: ApolloError) => {
    FlashMessageService.showError(ErrorMapper.map(error));
    this.setState({ loading: false });
  }

  private handleTakeAnotherPicture = () => {
    Navigation.pop(this.props.componentId);
  }

}
