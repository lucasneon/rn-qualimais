import * as React from 'react';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { Body, Root, VBox, VSeparator } from '@atomic';
import { BiometryCamera } from './biometry-camera.component';
import { BiometryConfirmationPage, BiometryConfirmationPageNavigationProps } from './biometry-confirmation.page';
import { BiometryStrings } from './biometry.strings';

export interface BiometryPageProps {
  isRightAfterLogin: boolean;
}

interface BiometryPageState {
  enabled: boolean;
}
@PageView('Biometry')
export class BiometryPage extends React.Component<NavigationPageProps<BiometryPageProps>, BiometryPageState> {
  static options = { name: 'BiometryPage' };

  constructor(props) {
    super(props);
    this.state = { enabled: true };
    Navigation.events().bindComponent(this);
    if (props.navigation.isRightAfterLogin) {
      Navigation.mergeOptions(props.componentId, {
        topBar: {
          rightButtons: [],
        },
      });
    }
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  componentDidAppear() {
    if (!this.state.enabled) {
      this.setState({ enabled: true });
    }
  }

  render() {
    return (
      <CollapsibleHeader title={BiometryStrings.Register.Title}>
        <Root>
          <VBox>
            <Body>{BiometryStrings.Register.IntroductionText}</Body>
          </VBox>
          <VSeparator />
          <BiometryCamera
            enabled={this.state.enabled}
            onPictureTaken={this.handlePicture}
            onDoLater={this.props.navigation.isRightAfterLogin && this.handleDoLater}
          />
        </Root>
      </CollapsibleHeader>
    );
  }

  handlePicture = (picture: any) => {
    this.setState({ enabled: false });
    Navigation.push<BiometryConfirmationPageNavigationProps>(
      this.props.componentId,
      BiometryConfirmationPage,
      { picture, isRightAfterLogin: this.props.navigation.isRightAfterLogin },
    );
  }

  private handleDoLater = async () => {
    await Navigation.dismissAllModals();
  }
}
