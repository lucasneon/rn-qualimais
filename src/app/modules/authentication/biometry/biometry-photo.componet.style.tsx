import styled from 'styled-components/native';

export const BiometryPhotoStyled = styled.Image`
  align-self: stretch;
  resize-mode: cover;
`;
