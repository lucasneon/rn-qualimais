import { AuthenticationFragment } from '@app/data/graphql';
import { Beneficiary } from '@app/model';

export const AuthenticationMapper = {
  map(authentication: AuthenticationFragment): Beneficiary {
    return {
      code: authentication.beneficiaryCode,
      idHealthCareCard: authentication.idHealthCareCard,
      token: authentication.token,
      name: authentication.name,
      email: authentication.email,
      city: authentication.city,
      state: authentication.state,
      dependant: authentication.dependant,
      birthDate: authentication.birthDate && new Date(authentication.birthDate),
      isRegisteredInBiometricService: authentication.isRegisteredInBiometricService,
      hasCarePlan: authentication.hasCarePlan,
    };
  },
};
