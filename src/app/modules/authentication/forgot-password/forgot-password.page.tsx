import * as React from 'react';
import { Subscribe } from 'unstated';
import { Logo } from '@app/components/atm.logo';
import { PageView } from '@app/core/analytics';
import { GraphQLProvider } from '@app/core/graphql/';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { ForgotPasswordMutationVariables } from '@app/data/graphql';
import { ErrorMapper } from '@app/utils';
import {
  Form,
  PrimaryButton,
  Root,
  TextField,
  Validators,
  VBox,
  VSeparator,
} from '@atomic';
import { ForgotPasswordStrings } from './forgot-password.strings';
import { fetchForgotPassword } from './forgot-password.service';
import { View } from 'react-native';

export const Fields = {
  username: 'username',
};

export interface ForgotPasswordNavigationProps {
  initialUsername: string;
  onSuccess: (message: string) => void;
}

interface ForgotPasswordState {
  loading: boolean;
  color?: string;
}

type ForgotPasswordProps = NavigationPageProps<ForgotPasswordNavigationProps>;

@PageView('ForgotPassword')
export class ForgotPasswordPage extends React.Component<
  ForgotPasswordProps,
  ForgotPasswordState
> {
  static options = {
    name: 'ForgotPasswordPage',
    topBar: {
      rightButtons: [],
    },
  };

  

  private graph: GraphQLProvider;

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Subscribe to={[GraphQLProvider]}>
        {(graph: GraphQLProvider) => {
          this.graph = graph;
          return (
            <Root>
              <Form onSubmit={this.handleSubmit}>
                <Form.Scroll>
                  <VBox>
                    <View style={{ marginTop: 60 }}>
                      <Form.Field
                        label={ForgotPasswordStrings.UsernameLabel}
                        value={this.props.navigation.initialUsername}
                        validators={[
                          Validators.Required(),
                          Validators.MinLength(
                            8,
                            ForgotPasswordStrings.Validation,
                          ),
                        ]}
                        name={Fields.username}>
                        <TextField type={TextField.Type.Numeric} />
                      </Form.Field>
                      <VSeparator />
                      <Form.Submit>
                        <PrimaryButton
                          text={ForgotPasswordStrings.SubmitButtonText}
                          expanded={true}
                          loading={graph.state.loading}
                          submit={true}
                        />
                      </Form.Submit>
                      <VSeparator />
                    </View>
                  </VBox>
                </Form.Scroll>
              </Form>
            </Root>
          );
        }}
      </Subscribe>
    );
  }

  private handleSubmit = (formData?: any) => {
    if (Object.keys(formData.error).length === 0) {
      this.forgotMutation(formData.data.username);
    }
  };

  private forgotMutation = async (user: string) => {
    this.setState({ loading: true });
    const result = await fetchForgotPassword(user);
    if (result && result.error) {
      return this.handleMutationError(result.error);
    }
    this.handleMutationSuccess(result.data);

    //TODO GRAPHQL
    // this.graph.mutate<ForgotPasswordMutationVariables>(
    //   'forgot-password',
    //   { user },
    //   this.handleMutationSuccess,
    //   this.handleMutationError,
    // );
  };

  private handleMutationSuccess = data => {
    this.props.navigation.onSuccess(data.ForgotPassword);
    Navigation.pop(this.props.componentId);
  };

  private handleMutationError = error => {
    FlashMessageService.showError(ErrorMapper.map(error));
  };
}
