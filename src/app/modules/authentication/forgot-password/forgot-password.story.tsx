import * as React from 'react';

import { storiesOf } from '@storybook/react-native';
import { ForgotPasswordPage } from './forgot-password.page';

const stories = storiesOf('Screens/Login', module);

const mockProps = {
  navigation: {
    initialUsername: null,
    onSuccess: () => null,
  },
};

stories.add('ForgotPassword', () => (
  <ForgotPasswordPage navigation={mockProps.navigation} />
));
