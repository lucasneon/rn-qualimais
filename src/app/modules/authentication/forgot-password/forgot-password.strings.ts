export const ForgotPasswordStrings = {
  Title: 'Password Recovery',
  UsernameLabel: 'CPF',
  SubmitButtonText: 'Envie-me uma nova senha',
  Validation: 'Insira um CPF válido',
};
