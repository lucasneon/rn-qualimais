import { BaseUserRequest } from '@app/model/api/common.entity';
import { axiosPost, BaseUrl } from '../api/login/http-requests';
import { getToken } from '../api/login/login.service';
import { GAMA_OP_CODE, GAMA_SYSTEM_CODE } from '@app/config';
import Container from 'typedi';

const opCode: string = Container.get(GAMA_OP_CODE);
const systemCode: string = Container.get(GAMA_SYSTEM_CODE);

export const fetchForgotPassword = async (user: string) => {
  await getToken();
  try {
    await axiosPost({
      url: '/senha/esqueci-senha',
      data: mapForgotPassword(user),
      baseUrl: BaseUrl.GAMA_AUTH_URL,
    });
  } catch (e) {
    console.error(e);
    return { data: null, error: e };
  }

  const data = {
    ForgotPassword:
      'Se o beneficiário estiver ativo, foi enviado um e-mail com a nova senha.',
  };

  return { data: data, error: false };
};

export function mapForgotPassword(
  idHealthCareCard: string,
): ForgotPasswordRequest {
  console.warn({
    codigoOperadora: opCode,
    codigoSistema: systemCode,
    usuario: idHealthCareCard,
  });
  return {
    codigoOperadora: opCode,
    codigoSistema: systemCode,
    usuario: idHealthCareCard,
  };
}

export interface ForgotPasswordRequest extends BaseUserRequest {
  usuario: string;
}
