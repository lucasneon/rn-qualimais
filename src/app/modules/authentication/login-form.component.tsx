import * as React from 'react';
import { PrimaryButton, VBox } from '@atomic';
import { LoginStrings } from './login.strings';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import IconCheckbox from '@assets/images/login/checked_blue.png';
import * as Yup from 'yup';

import { PureRoundedCheckbox } from 'react-native-rounded-checkbox';
import { TextField } from 'rn-material-ui-textfield';

export interface LoginPageProps {
  loading: boolean;
  idValue: string;
  termOfConsentiment?: boolean;
  onSubmit: (data?: any) => void;
  onIdValueChange: (value: string) => void;
  onForgotPassword: () => void;
  onTermsOfUseClick: () => void;
  onTermsOfAcceptClick: () => void;
}

interface LoginFormState {
  termsOfUseAccept: boolean;
  login: string;
  password: string;
  errors: any;
}

export class LoginForm extends React.Component<LoginPageProps, LoginFormState> {
  constructor(props) {
    super(props);
    this.state = {
      termsOfUseAccept: true,
      login: '',
      password: '',
      errors: {},
    };
  }

  handleSubmit = async () => {
    this.setState({ errors: {} });
    const schema = Yup.object().shape({
      login: Yup.string()
        // .test('is-cpf', 'CPF inválido', value => validateCPF(value))
        .required('Campo obrigatório'),
      password: Yup.string()
        .min(4, 'No mínimo ${min} caracteres')
        .required('Campo obrigatório'),
    });

    try {
      await schema.validate(this.state, {
        abortEarly: false,
      });
      const payload = {
        data: {
          user: this.state.login,
          password: this.state.password,
        },
      };

      this.props.onSubmit(payload);
    } catch (err) {
      const validationErrors = {};
      if (err instanceof Yup.ValidationError) {
        err.inner.forEach(error => {
          validationErrors[error.path] = error.message;
        });
        this.setState({ errors: validationErrors });
      }
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <VBox>
        <Text style={styles.inputLabel}>Cpf</Text>
        <TextInput
          style={styles.input}
          keyboardType="numeric"
          placeholder="Digite o CPF"
          onChangeText={(event: any) => {
            this.setState({ login: event });
          }}
        />
        {errors.login ? (
          <Text style={styles.errorMessage}>{errors.login}</Text>
        ) : null}

        <Text style={[styles.inputLabel, styles.inputPassword]}>Senha</Text>
        <TextInput
          style={styles.inputPass}
          secureTextEntry={true}
          // secureTextEntry={this.state.password.length === 0 ? false : true}
          placeholder="Digite a senha"
          onChangeText={(event: any) => {
            this.setState({ password: event });
          }}
        />
        {errors.password ? (
          <Text style={styles.errorMessage}>{errors.password}</Text>
        ) : null}

        <TouchableOpacity onPress={this.props.onForgotPassword}>
          <Text style={styles.forgotPassword}>Esqueci Senha</Text>
        </TouchableOpacity>

        <View style={styles.buttonLoginContainer}>
          <PrimaryButton
            text={LoginStrings.SignIn}
            expanded={true}
            loading={this.props.loading}
            disabled={!this.state.termsOfUseAccept}
            onPress={this.state.termsOfUseAccept && this.handleSubmit}
          />
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <PureRoundedCheckbox
            text={''}
            isChecked={this.state.termsOfUseAccept}
            outerStyle={styles.checkboxStyle}
            innerStyle={styles.checkboxStyle}
            onPress={checked => {
              this.setState({ termsOfUseAccept: checked });
            }}>
            {this.state.termsOfUseAccept ? (
              <Image source={IconCheckbox} style={styles.checkboxStyle} />
            ) : (
              ''
            )}
          </PureRoundedCheckbox>
          <TouchableOpacity
            onPress={_ => {
              this.setState({ termsOfUseAccept: !this.state.termsOfUseAccept });
            }}>
            <Text style={{ marginLeft: 20, color: '#ffffff' }}>
              Ao acessar o aplicativo, você concorda com nossos termos de uso.
            </Text>
          </TouchableOpacity>
        </View>

        {this.props.termOfConsentiment ? (
          <TouchableOpacity onPress={this.props.onTermsOfAcceptClick}>
            <Text style={styles.termText}>Ver termos de uso</Text>
          </TouchableOpacity>
        ) : (
          <></>
        )}
      </VBox>
    );
  }
}

const styles = StyleSheet.create({
  inputLabel: {
    paddingLeft: 20,
    fontWeight: 'bold',
    lineHeight: 16,
    marginBottom: 10,
    color: '#ffffff',
    // fontFamily: 'Helvetica',
    fontSize: 16,
  },
  inputPassword: {
    marginTop: 30,
  },
  input: {
    backgroundColor: '#ffffff',
    color: '#757575',
    borderRadius: 10,
    paddingLeft: 20,
    paddingRight: 20,
    height: 50,
    fontFamily: 'Helvetica',
  },
  inputPass: {
    backgroundColor: '#ffffff',
    color: '#757575',
    borderRadius: 10,
    paddingLeft: 20,
    paddingRight: 20,
    fontFamily: 'Helvetica',
    height: 50,
  },
  forgotPassword: {
    fontSize: 16,
    marginTop: 20,
    fontWeight: 'bold',
    lineHeight: 18,
    color: '#ffffff',
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
  buttonLoginContainer: {
    marginTop: 30,
    marginBottom: 20,
  },
  checkboxStyle: {
    height: 20,
    width: 20,
  },
  termText: {
    fontSize: 16,
    marginTop: 20,
    fontWeight: 'bold',
    lineHeight: 18,
    color: '#ffffff',
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
  errorMessage: {
    color: '#e6dada',
    fontSize: 14,
    paddingLeft: 20,
    marginTop: 5,
  },
});
