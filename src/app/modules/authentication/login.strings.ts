export const LoginStrings = {
  Label: {
    Login: 'CPF',
    Password: 'Senha',
  },
  Validation: {
    Login: 'Insira um CPF válido',
    Password: 'No mínimo 4 caracteres',
  },
  ForgotPassword: 'Esqueceu a senha?',
  SignIn: 'Entrar',
  Error: 'Erro ao comunicar com os servidores. Por favor, tente novamente.',
  TermsOfUse: {
    Label: 'Ao acessar o aplicativo, você concorda com os nossos termos de uso.',
    Check: 'Ver termos de uso',
    TermAccept:'Ver termo de consentimento'
  },
};
