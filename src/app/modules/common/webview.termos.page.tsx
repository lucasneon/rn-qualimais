import * as React from 'react';
// import { WebView } from 'react-native-webview';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { Color } from '@atomic';
import {
  View,
  ActivityIndicator,
  StyleSheet,
  SafeAreaView,
} from 'react-native';

import { WebView } from 'react-native-webview';

export interface WebViewTermosPageProps {
  url: string;
}

export class WebViewTermosPage extends React.Component<
  NavigationPageProps<WebViewTermosPageProps>
> {
  static options = {
    name: 'WebviewPage',
    topBar: { background: { color: Color.White }, rightButtons: [] },
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      visible: true,
    };
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  hideSpinner() {
    this.setState({ visible: false });
  }

  componentDidMount() {}

  render() {
    return (
      <>
        <View style={styles.container}>
          <WebView
            source={{ uri: this.props.navigation.url }}
            onLoad={() => this.hideSpinner()}
            startInLoadingState={true}
            javaScriptEnabled={true}
            javaScriptEnabledAndroid={true}
            geolocationEnabled={true}
            mediaPlaybackRequiresUserAction={false}
            allowInlineMediaPlayback={true}
            allowFileAccess={true}
            domStorageEnabled={true}
            allowContentAccess={true}
          />

          {this.state.visible && (
            <ActivityIndicator
              color="#009688"
              size="large"
              style={styles.loading}
            />
          )}
        </View>
      </>
    );
  }

  ActivityIndicatorElement = () => {
    return (
      <ActivityIndicator
        color="#009688"
        size="large"
        style={styles.activityIndicatorStyle}
      />
    );
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    justifyContent: 'center',
    paddingTop: 50,
  },
  sizedBox: {
    paddingTop: 40,
  },
  activityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
  },
});
