import * as React from 'react';
import { Container as TypeDIContainer } from 'typedi';
import { LargeHeader } from '@app/components/atm.header';
import { ListInfoCellShimmer } from '@app/components/mol.cell';
import {
  LocationPlaceholder,
  Placeholder,
  PlaceholderSelector,
} from '@app/components/mol.placeholder';
import {
  SegmentedControl,
  SegmentedControlItemProps,
} from '@app/components/mol.segmented-control';
import {
  CarePointList,
  CarePointListItem,
} from '@app/components/org.care-point-list';
import {
  CarePointMap,
  CarePointMapShimmer,
  CarePointMarkerProps,
} from '@app/components/org.care-point-map';
import { ExternalNavigator, FlashMessageService } from '@app/core/navigation';
import {
  CarePointFragment,
  UpdateCarePointFavoriteMutationVariables,
  UpdateCarePointFavoriteInput,
  UpdateCarePointFavoriteMutation,
} from '@app/data/graphql';
import { Address, AppError, GeoCoordinates } from '@app/model';
import { LocationError } from '@app/services/providers';
import { Asset, LinkButtonProps, LoadingState, Root, VBox } from '@atomic';
import { AddressMapper } from './address.mapper';
import { GeneralStrings } from './common.strings';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { ApolloError } from 'apollo-client';
import { ErrorMapper } from '@app/utils';
import { updateCarePointFavorite } from './api/care-point/care-point.service';

export interface ListMapItem {
  title: string;
  key: string;
  distance?: number;
  carePoint: CarePointFragment;
  rightThumb?: string;
  userLocation?: string;
  isFavorite?: boolean;
  leftThumb?: string;
}

export interface ListMapProps {
  loading: boolean;
  userLocation?: GeoCoordinates;
  locationError?: LocationError;
  items?: ListMapItem[];
  error?: AppError;
  refetch?: () => void;
  title: string;
  notFoundTitle: string;
  notFoundMessage: string;
  onItemTap?: (index: number) => void;
}

interface ListMapState {
  showMap: boolean;
  items?: ListMapItem[];
}

export class ListMap extends React.Component<ListMapProps, ListMapState> {
  private readonly externalNavigator: ExternalNavigator =
    TypeDIContainer.get(ExternalNavigator);
  private locationError: LocationError;

  private readonly favoriteIcon: string = Asset.Icon.Common.StarOn;
  private readonly notFavoriteIcon: string = Asset.Icon.Common.StarOff;

  private readonly graphQLProvider: GraphQLProvider =
    TypeDIContainer.get(GraphQLProviderToken);

  private kilometragemNaoInformada: string = 'Km não informado.';

  indexToUpdate: number;

  constructor(props) {
    super(props);
    this.state = { showMap: false, items: null };
  }

  componentDidMount() {
    this.setState({ items: this.props.items });
  }

  render() {
    const listSegment: SegmentedControlItemProps = {
      text: GeneralStrings.List,
      onTap: this.handleListTap,
    };
    const mapSegment: SegmentedControlItemProps = {
      text: GeneralStrings.Map,
      onTap: this.handleMapTap,
    };

    const { error, items, locationError, userLocation } = this.props;
    this.locationError = locationError;

    return (
      <Root>
        <LargeHeader title={this.props.title} />
        <SegmentedControl left={listSegment} right={mapSegment} />
        {this.state.items && this.state.items.length === 0 ? (
          <Placeholder
            title={this.props.notFoundTitle}
            message={this.props.notFoundMessage}
            // image={Asset.Icon.Placeholder.Failure}
          />
        ) : this.state.items &&
          this.state.items.length > 0 &&
          this.state.showMap ? (
          <CarePointMap
            userCoord={userLocation}
            markers={items.map(this.mapToMarker)}
            centerUser={true}
          />
        ) : (
          this.state.items &&
          this.state.items.length > 0 &&
          !this.state.showMap && (
            <CarePointList
              items={this.state.items.map(this.mapToCarePointItem)}
            />
          )
        )}
      </Root>
    );
  }

  private handleListTap = () => {
    this.setState({ showMap: false });
  };

  private handleMapTap = () => {
    this.setState({ showMap: true });
  };

  private mapToCarePointItem = (
    listMapItem: ListMapItem,
    index: number,
  ): CarePointListItem => {
    const carePointFragment = listMapItem.carePoint;
    const address: Address = AddressMapper.map(carePointFragment.address);
    const coord = {
      latitude: carePointFragment.address.latitude,
      longitude: carePointFragment.address.longitude,
    };
    const contact = carePointFragment && carePointFragment.contact;
    const phone = contact && contact.phoneNumbers && contact.phoneNumbers[0];
    const email = contact && contact.email;
    const website = contact && contact.website;
    let km: any = 0;
    const links: LinkButtonProps[] = [];

    if (phone) {
      links.push({
        text: phone,
        image: Asset.Icon.Custom.Phone,
        onTap: () => this.handlePhoneTap(phone),
      });
    }

    if (email) {
      links.push({
        text: email,
        image: Asset.Icon.Custom.Summary,
        onTap: () => this.handleEmailTap(email),
      });
    }

    if (website) {
      links.push({
        text: website,
        image: Asset.Icon.Custom.Web,
        onTap: () => this.handleWebsiteTap(website),
      });
    }

    if (coord) {
      links.push({
        text: GeneralStrings.TraceRoute,
        image: Asset.Icon.Custom.NavigationIcon,
        onTap: () => this.handleRouteTap(coord),
      });
    }

    // if ( !this.locationError && listMapItem.distance && listMapItem.distance !== 1) {
    //   km = Math.round(listMapItem.distance * 100) / 100 + ' km'
    // } else {
    //   km = this.kilometragemNaoInformada;
    // }
    const descriptions = [];
    if (address.streetAndNumber) {
      descriptions.push(address.streetAndNumber);
    }

    if (address.area) {
      descriptions.push(address.area);
    }

    if (address.zipCode) {
      descriptions.push(address.zipCode);
    }

    return {
      itemKey: listMapItem.key,
      title: listMapItem.title,
      descriptions: descriptions,
      links,
      side: !this.locationError
        ? listMapItem.distance &&
          Math.round(listMapItem.distance * 100) / 100 + ' km'
        : null,
      onTap: this.props.onItemTap && (() => this.props.onItemTap(index)),
      rightThumb: listMapItem.rightThumb,
      leftThumb: null, //listMapItem.isFavorite ? this.favoriteIcon : this.notFavoriteIcon,
      onTapLeftButton: () =>
        this.handleFavoriteTap(
          listMapItem.key,
          listMapItem.carePoint.id,
          listMapItem.isFavorite,
          index,
        ),
    };
  };

  private mapToMarker = (
    listMapItem: ListMapItem,
    index: number,
  ): CarePointMarkerProps => {
    const address = listMapItem.carePoint && listMapItem.carePoint.address;
    return {
      ...this.mapToCarePointItem(listMapItem, index),
      coords: address && {
        latitude: address.latitude,
        longitude: address.longitude,
      },
    };
  };

  private handlePhoneTap = async (phone: string) => {
    await this.externalNavigator.makeCall(phone);
  };

  private handleEmailTap = async (email: string) => {
    await this.externalNavigator.openEmail(email);
  };

  private handleWebsiteTap = async (website: string) => {
    await this.externalNavigator.openWebsite(website);
  };

  private handleRouteTap = async (coordinates: GeoCoordinates) => {
    await this.externalNavigator.openMaps(coordinates);
  };

  private handleFavoriteTap = async (
    practitionerId: string,
    carePointId: string,
    isFavorite: boolean,
    index: number,
  ) => {
    this.indexToUpdate = index;

    const variables: UpdateCarePointFavoriteInput = {
      carePointId: carePointId,
      practionerId: practitionerId,
      flagFavorite: isFavorite ? 'N' : 'S',
    };
    // TODO testar se o método está funcionando
    updateCarePointFavorite(variables)
      .then(res =>
        this.handleUpdateFavoriteSuccess({ UpdateCarePointFavorite: res }),
      )
      .catch(err => this.handleError(err));
    // TODO GRAPHQL
    // this.graphQLProvider.mutate<UpdateCarePointFavoriteMutationVariables>(
    //   'update-care-point-favorite',
    //   { data: variables },
    //   this.handleUpdateFavoriteSuccess,
    //   this.handleError,
    // );
  };

  private handleError = (error: ApolloError) => {
    FlashMessageService.showError(ErrorMapper.map(error));
  };

  private handleUpdateFavoriteSuccess = (
    res: UpdateCarePointFavoriteMutation,
  ) => {
    this.props.items[this.indexToUpdate].isFavorite =
      res.UpdateCarePointFavorite.isFavorite;
    this.setState({ items: this.props.items });
  };
}
