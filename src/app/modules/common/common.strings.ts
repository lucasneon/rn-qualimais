import { Platform } from 'react-native';
import { Age } from '@app/model';

// To keep common strings to be used on many pages/components
export const GeneralStrings = {
  Yes: 'Sim',
  Others: 'Outros',
  Cancel: 'Cancelar',
  PersonalDoctor: 'Médico Gestor',
  ManualBook: 'Guia',
  TraceRoute: 'Ver como chegar',
  List: 'Lista',
  Map: 'Mapa',
  SystemAuth: {
    Need: 'Autentique-se no sistema para acessar o app.',
    Title: 'Requer autenticação',
    Description: 'Para acessar o app, realize a autenticação do sistema.',
    Fail: 'Falha na autenticação.',
    Fallback: Platform.OS === 'ios' ? 'Usar código' : 'Usar código ou padrão',
  },
  ErrorOpenUrl: 'Ocorreu um erro ao abrir o link',
  ErrorLocation: 'O GPS está desabilitado.',
  BottomTabs: {
    HomePage: 'Cuidados',
    HealthPage: 'Saúde',
    CustomerCarePage: 'Atendimento',
    AccountPage: 'Conta',
  },

  Age(age: Age): string {
    return age.years > 2 ? `${age.years} anos` :
      age.years === 1 && age.months > 1 ? `1 ano e ${age.months} meses` :
      age.years === 1 && age.months === 1 ? `1 ano e 1 mês` :
      age.years === 1 && age.months === 0 ? `1 ano` :
      age.years === 0 && age.months > 1 ? `${age.months} meses` :
      age.years === 0 && age.months === 1 ? `1 mês` :
      age.years === 0 && age.months === 0 ? `Recém-nascido` :
      null;
  },
};
