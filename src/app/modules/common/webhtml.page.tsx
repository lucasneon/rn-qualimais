import * as React from 'react';
import { TopAreaView } from '@app/components/obj.top-area';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { Color, VBox } from '@atomic';
import HTML from 'react-native-render-html';

export interface WebHtmlPageProps {
  url: string;
}

export class WebHtmlPage extends React.Component<
  NavigationPageProps<WebHtmlPageProps>
> {
  static options = {
    name: 'WebHtmlPage',
    topBar: { background: { color: Color.White }, rightButtons: [] },
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  render() {
    return (
      <>
        <TopAreaView height={Navigation.TopBarHeight} />
        <VBox>
          <HTML html={this.props.navigation.url} />
        </VBox>
      </>
    );
  }
}
