import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { ListCell, ListCellShimmer } from '@app/components/mol.cell';
import { Placeholder, PlaceholderSelector } from '@app/components/mol.placeholder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { QueryContainer } from '@app/core/graphql';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { GeneralTypeFragment, ScheduleAppointmentMutation, SpecialtiesQuery } from '@app/data/graphql';
import { ErrorMapper } from '@app/utils';
import { Asset, LoadingState, Root, VBox } from '@atomic';
import { AppointmentStrings } from './appointment.strings';
import { SchedulesPage, SchedulesPageProps } from './schedules.page';

export interface SpecialtiesPageProps {
  onScheduleSuccess: (resp: ScheduleAppointmentMutation) => void;
}

@PageView('Specialties')
export class SpecialtiesPage extends React.Component<NavigationPageProps<SpecialtiesPageProps>> {
  static options = { name: 'SpecialtiesPage' };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  render() {
  return (
      <Root>
        <CollapsibleHeader title={AppointmentStrings.SpecialtiesTitle}>
          <VBox>
            <QueryContainer document='specialties'>
              {(res: QueryResult<SpecialtiesQuery>) => {
                const specialties = res.data && res.data.Specialties;
                const handlePlaceholderTap = () => res.refetch();

                return (
                  <LoadingState data={!!specialties} loading={res.loading} error={!!res.error}>
                    <LoadingState.Shimmer>
                      <ListCellShimmer count={10} />
                    </LoadingState.Shimmer>
                    <LoadingState.ErrorPlaceholder>
                      <PlaceholderSelector error={ErrorMapper.map(res.error)} onTap={handlePlaceholderTap} />
                    </LoadingState.ErrorPlaceholder>
                    {specialties && specialties.length === 0 && (
                      <Placeholder
                        title={AppointmentStrings.NoSchedule.Specialty.Title}
                        message={AppointmentStrings.NoSchedule.Specialty.Message}
                        image={Asset.Icon.Placeholder.Failure}
                      />)
                    }
                    {specialties && specialties.map(specialty => (
                      <ListCell
                        key={specialty.id}
                        text={specialty.description}
                        rightThumb={Asset.Icon.Atomic.ChevronRight}
                        onTap={this.handleSpecialtyTap(specialty)}
                      />
                    ))}
                </LoadingState>
                );
              }}
            </QueryContainer>
          </VBox>
        </CollapsibleHeader>
      </Root>
    );
  }

  private handleSpecialtyTap(specialty: GeneralTypeFragment) {
    return () => Navigation.push<SchedulesPageProps>(
      this.props.componentId,
      SchedulesPage,
      { specialtyCode: specialty.id },
    );
  }
}
