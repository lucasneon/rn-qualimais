export const AppointmentStrings = {
  ProfessionalScheduleTitle: 'Agendar consulta',
  SpecialtiesTitle: 'Outra especialidade',
  ScheduleAction: 'Solicitar agendamento',
  To: 'Para',
  Dr: 'Dr(a)',
  CalendarTitle: 'Quando',
  TimeSlotError: 'Erro ao buscar os horários disponíveis',
  Confirmation: {
    Title: 'Deseja agendar a consulta?',
    Message: (date: string, hour: string) => `A consulta será pré-agendada para ${date} às ${hour}. Deseja continuar?`,
    Submit: 'Sim',
    Cancel: 'Não',
  },
  NoSchedule: {
    Specialty: {
      Title: 'Especialistas não encontrados',
      Message: 'Não foram encontrados especialistas com agenda disponível.',
    },
    PersonalDoctor: {
      Title: 'Médico generalista não encontrado',
      Message: 'Não foram encontrados médicos com agenda disponível.',
    },
    MedicoGestor: (titulo: string) => `${titulo} não encontrada`,
  },
  SolicitationConfirmed: {
    Title: 'Consulta solicitada',
    Message: 'Uma solicitação de consulta foi enviada para seu médico. Aguarde a confirmação dele antes de comparecer',
    CellDateText: (day: string, hour: string) => `Solicitado para ${day} · ${hour}`,
    Back: 'Voltar',
    AddToCalendar: 'Adicionar à agenda',
    Tag: 'Consulta',
  },
};
