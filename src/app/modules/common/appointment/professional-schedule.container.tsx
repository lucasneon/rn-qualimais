import moment, { Moment } from 'moment';
import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { Subscribe } from 'unstated';
import { QueryContainer } from '@app/core/graphql';
import {
  ProfessionalScheduleFragment, ProfessionalScheduleInput, ProfessionalScheduleQuery, ProfessionalScheduleQueryVariables
} from '@app/data/graphql';
import { AppError } from '@app/model';
import { BeneficiaryProvider } from '@app/services/providers';
import { ErrorMapper } from '@app/utils';
import { ProfessionalScheduleProvider } from '@app/services/providers/professional-schedule.provider';
import { ProfessionalScheduleModel } from '../api/professional-schedule/professional-schedule.model';

export interface ProfessionalScheduleContainerProps {
  scheduleId: string;
  start: Moment;
  children: (result: ProfessionalScheduleContainerResult) => React.ReactNode;
}

export interface ProfessionalScheduleContainerResult {
  loading?: boolean;
  error?: AppError;
  schedule?: ProfessionalScheduleFragment | ProfessionalScheduleModel;
}

export class ProfessionalScheduleContainer extends React.PureComponent<ProfessionalScheduleContainerProps> {
  private professionalScheduleProvider: ProfessionalScheduleProvider;
  private data;
  componentDidMount() {
    this.professionalScheduleProvider.fetchSchedule(this.data);
  }
  render() {
    return (
      <Subscribe to={[BeneficiaryProvider, ProfessionalScheduleProvider]}>
        {(
          beneficiaryProvider: BeneficiaryProvider,
          professionalScheduleProvider: ProfessionalScheduleProvider,
        ) => {
          this.professionalScheduleProvider = professionalScheduleProvider;
          const start: Moment = this.props.start;
          const end: Moment = moment(start).endOf('month');

          const data: ProfessionalScheduleInput = {
            scheduleId: this.props.scheduleId,
            city: beneficiaryProvider.state.active.city,
            state: beneficiaryProvider.state.active.state,
            from: start.format('YYYY-MM-DD'),
            until: end.format('YYYY-MM-DD'),
          };
          this.data = data;
          const shedule = professionalScheduleProvider.state.schedule;
          const error = professionalScheduleProvider.state.error;
          const loading = professionalScheduleProvider.state.loading;
          return this.props.children({
            loading: loading,
            error: error && ErrorMapper.map(error),
            schedule: loading ? null : shedule,
          });
          // TODO refotarr para RES
          // return (
          //   <QueryContainer document='professional-schedule' variables={{ data }}>
          //     {(res: QueryResult<ProfessionalScheduleQuery, ProfessionalScheduleQueryVariables>) => {
          //       return this.props.children({
          //         loading: res.loading,
          //         error: res.error && ErrorMapper.map(res.error),
          //         schedule: res.loading ? null : res.data && res.data.ProfessionalSchedule,
          //       });
          //     }}
          //   </QueryContainer>
          // );
        }}
      </Subscribe>
    );
  }
}
