import * as React from 'react';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import {
  CarePlanAppointmentFragment,
  CarePlanCodes,
  ScheduleFragment,
} from '@app/data/graphql';
import { ListMap, ListMapItem } from '@app/modules/common';
import { Asset, Scroll } from '@atomic';
import { AppointmentStrings } from './appointment.strings';
import {
  ProfessionalSchedulePage,
  ProfessionalSchedulePageNavigationProps,
} from './professional-schedule.page';
import {
  SchedulesContainer,
  SchedulesContainerRes,
} from './schedules.container';
import { Container } from 'typedi';
import { PersonalDoctor } from '@app/config/tokens.config';
import { ActivityIndicator, Alert, View } from 'react-native';

export interface SchedulesPageProps {
  specialtyCode?: string;
  personalDoctor?: boolean;
  carePlanCodes?: CarePlanCodes;
  minDate?: Date;
  maxDate?: Date;
  onScheduleSuccess?: (appointment: CarePlanAppointmentFragment) => void;
}

@PageView('Schedules')
export class SchedulesPage extends React.PureComponent<
  NavigationPageProps<SchedulesPageProps>
> {
  static options = { name: 'SchedulesPage' };

  private personalDoctor: string = Container.get(PersonalDoctor);

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  render() {
    const props = this.props.navigation;
    return (
      <SchedulesContainer
        specialtyCode={props.specialtyCode}
        personalDoctor={props.personalDoctor}>
        {(res: SchedulesContainerRes) => {
          const data =
            res.queryRes &&
            res.queryRes.data &&
            res.queryRes.data.SpecialtySchedules;
          const specialtyDescription: string =
            data && data.specialty && data.specialty.description;
          const schedules: ScheduleFragment[] = data && data.schedules;

          const items: ListMapItem[] =
            (schedules && schedules.map(this.mapToListItem)) || [];
          const handleItemTap = (index: number) => {
            this.handleItemTap(schedules[index]);
          };
          let placeholderTitle;
          let placeholderMessage;
          if (this.personalDoctor !== '') {
            placeholderTitle = AppointmentStrings.NoSchedule.MedicoGestor(
              this.personalDoctor,
            );
            placeholderMessage =
              AppointmentStrings.NoSchedule.PersonalDoctor.Message;
          } else {
            placeholderTitle = this.props.navigation.personalDoctor
              ? AppointmentStrings.NoSchedule.PersonalDoctor.Title
              : AppointmentStrings.NoSchedule.Specialty.Title;
            placeholderMessage = this.props.navigation.personalDoctor
              ? AppointmentStrings.NoSchedule.PersonalDoctor.Message
              : AppointmentStrings.NoSchedule.Specialty.Message;
          }

          if (res.loading) {
            return (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                }}>
                <ActivityIndicator size="large" color="#171a88" />
              </View>
            );
          }

          return (
            <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
              <Scroll nestedScrollEnabled={true}>
                <ListMap
                  loading={res.loading}
                  userLocation={res.userLocation}
                  items={items}
                  error={res.queryRes && res.queryRes.error}
                  refetch={res.queryRes && res.queryRes.refetch}
                  title={specialtyDescription}
                  notFoundTitle={placeholderTitle}
                  notFoundMessage={placeholderMessage}
                  onItemTap={handleItemTap}
                />
              </Scroll>
            </View>
          );
        }}
      </SchedulesContainer>
    );
  }

  private mapToListItem = (schedule: ScheduleFragment): ListMapItem => {
    return {
      title: schedule.professionalName,
      key: schedule.scheduleId,
      carePoint: schedule.carePoint,
      rightThumb: Asset.Icon.Atomic.ChevronRight,
    };
  };

  private handleItemTap = (schedule: ScheduleFragment) => {
    Navigation.push<ProfessionalSchedulePageNavigationProps>(
      this.props.componentId,
      ProfessionalSchedulePage,
      {
        scheduleId: schedule.scheduleId,
        professionalId: schedule.professionalId,
        professionalName: schedule.professionalName,
        locationId: schedule.carePoint.id,
        carePlanCodes: this.props.navigation.carePlanCodes,
        onScheduleSuccess: this.props.navigation.onScheduleSuccess,
        minDate: this.props.navigation.minDate,
        maxDate: this.props.navigation.maxDate,
      },
    );
  };
}
