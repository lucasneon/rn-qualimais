import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { Subscribe } from 'unstated';
import { QueryContainer } from '@app/core/graphql';
import {
  SchedulesInput,
  SpecialtySchedulesQuery,
  SpecialtySchedulesQueryVariables,
} from '@app/data/graphql';
import { GeoCoordinates } from '@app/model';
import {
  BeneficiaryProvider,
  LocationProvider,
  LocationState,
} from '@app/services/providers';
import { ScheduleProvider } from '@app/services/providers/shchedule.provider';

export interface SchedulesContainerRes {
  loading: boolean;
  userLocation?: GeoCoordinates;
  queryRes?:
    | QueryResult<SpecialtySchedulesQuery, SpecialtySchedulesQueryVariables>
    | any;
}

export interface SchedulesContainerProps {
  specialtyCode?: string;
  personalDoctor?: boolean;
  children: (res: SchedulesContainerRes) => React.ReactNode;
}

export class SchedulesContainer extends React.PureComponent<SchedulesContainerProps> {
  private locationProvider: LocationProvider;
  private scheduleProvider: ScheduleProvider;
  private data: SchedulesInput;

  componentDidMount() {
    this.locationProvider.handlePlatforms().then(async () => {
      await this.scheduleProvider.fetchSchedules(this.data);
    });
  }

  render() {
    return (
      <Subscribe to={[BeneficiaryProvider, LocationProvider, ScheduleProvider]}>
        {(
          beneficiaryProvider: BeneficiaryProvider,
          locationProvider: LocationProvider,
          scheduleProvider: ScheduleProvider,
        ) => {
          this.locationProvider = locationProvider;
          this.scheduleProvider = scheduleProvider;
          const locationState: LocationState = locationProvider.state;
          if (locationProvider.state.fetching) {
            return this.props.children({
              loading: true,
              userLocation: locationState.location,
              queryRes: null,
            });
          }

          const data: SchedulesInput = {
            specialtyCode: '1',
            personalDoctor: this.props.personalDoctor,
            state: beneficiaryProvider.state.active.state,
            city: beneficiaryProvider.state.active.city,
          };

          this.data = data;
          console.warn('Schedules-data', data);
          return this.props.children({
            loading: scheduleProvider.state.loading,
            userLocation: locationState.location,
            queryRes: scheduleProvider.state.data,
          });
          // return (
          //   <QueryContainer document="specialty-schedules" variables={{ data }}>
          //     {(
          //       res: QueryResult<
          //         SpecialtySchedulesQuery,
          //         SpecialtySchedulesQueryVariables
          //       >,
          //     ) => {
          //       return this.props.children({
          //         loading: res.loading,
          //         userLocation: locationState.location,
          //         queryRes: res,
          //       });
          //     }}
          //   </QueryContainer>
          // );
        }}
      </Subscribe>
    );
  }
}
