import moment, { Moment } from 'moment';
import * as React from 'react';
import { Subscribe } from 'unstated';
import { AlertBuilder } from '@app/components/atm.alert-builder';
import { DefinitionListCell } from '@app/components/mol.cell';
import {
  RadioBadges,
  RadioBadgesShimmer,
} from '@app/components/mol.radio-badges';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { GraphQLProvider } from '@app/core/graphql';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import {
  AppointmentInput,
  CarePlanAppointmentFragment,
  CarePlanCodes,
  DaySlotFragment,
  ProfessionalScheduleFragment,
  ScheduleAppointmentMutation,
  ScheduleAppointmentMutationVariables,
} from '@app/data/graphql';
import { DateUtils, ErrorMapper } from '@app/utils';
import {
  Calendar,
  Form,
  H1,
  InputCaptionError,
  PrimaryButton,
  Root,
  VBox,
  VSeparator,
} from '@atomic';
import {
  AppointmentSolicitationPage,
  AppointmentSolicitationPageProps,
} from './appointment-solicitation.page';
import { AppointmentStrings } from './appointment.strings';
import {
  ProfessionalScheduleContainer,
  ProfessionalScheduleContainerResult,
} from './professional-schedule.container';
import { DaySlotModel } from '../api/professional-schedule/professional-schedule.model';
import { execSchedulePost } from '../api/professional-schedule/professional-schedule.service';

export interface ProfessionalSchedulePageNavigationProps {
  scheduleId: string;
  professionalId: string;
  professionalName: string;
  locationId: string;
  carePlanCodes?: CarePlanCodes;
  minDate?: Date;
  maxDate?: Date;
  onScheduleSuccess?: (appointment: CarePlanAppointmentFragment) => void;
}

interface ProfessionalSchedulePageState {
  startDateToFetch?: Moment;
  selectedDay?: Moment;
  selectedHour?: Moment;
  endHour?: Moment;
}

@PageView('ProfessionalSchedule')
export class ProfessionalSchedulePage extends React.Component<
  NavigationPageProps<ProfessionalSchedulePageNavigationProps>,
  ProfessionalSchedulePageState
> {
  static options = { name: 'ProfessionalSchedulePage' };

  private readonly minDate: Moment;
  private readonly maxDate?: Moment;

  private graphQLProvider: GraphQLProvider;

  constructor(
    props: NavigationPageProps<ProfessionalSchedulePageNavigationProps>,
  ) {
    super(props);
    this.minDate = moment(this.getMinDate());
    this.maxDate = props.navigation.maxDate && moment(props.navigation.maxDate);
    this.state = {
      startDateToFetch: this.minDate,
      selectedDay: null,
      selectedHour: null,
      endHour: null,
    };
  }

  render() {
    const { startDateToFetch } = this.state;

    return (
      <CollapsibleHeader title={AppointmentStrings.ProfessionalScheduleTitle}>
        <Root>
          <VSeparator />
          <DefinitionListCell
            term={AppointmentStrings.Dr}
            definition={this.props.navigation.professionalName}
            definitionGrowth={7}
          />
          <VSeparator />
          <VBox vAlign="center" hAlign="center">
            <H1>{AppointmentStrings.CalendarTitle}</H1>
            <VSeparator />
            <Calendar
              minDate={this.minDate}
              maxDate={this.maxDate}
              initialDate={startDateToFetch}
              selectedStartDate={this.state.selectedDay}
              onDateChange={this.handleDateChange}
              onMonthChange={this.handleMonthChange}
            />
            <VSeparator />
          </VBox>
          <VBox vAlign="center" hAlign="flex-start">
            <ProfessionalScheduleContainer
              scheduleId={this.props.navigation.scheduleId}
              start={startDateToFetch}>
              {(res: ProfessionalScheduleContainerResult) => {
                const schedule: ProfessionalScheduleFragment = res.schedule;
                const daySlots: DaySlotModel[] = schedule && schedule.daySlots;
                return (
                  <>
                    {res.loading && !!this.state.selectedDay && (
                      <RadioBadgesShimmer count={12} />
                    )}
                    {daySlots && this.state.selectedDay && (
                      <Form.Field
                        onValueChange={this.handleTagSelection}
                        name="hours">
                        <RadioBadges
                          selectedHour={this.state.selectedHour}
                          daySlot={daySlots.find(
                            it =>
                              moment(it.day).format('YYYY-MM-DD') ===
                              this.state.selectedDay.format('YYYY-MM-DD'),
                          )}
                        />
                      </Form.Field>
                    )}
                    {res.error && (
                      <InputCaptionError>
                        {AppointmentStrings.TimeSlotError}
                      </InputCaptionError>
                    )}
                  </>
                );
              }}
            </ProfessionalScheduleContainer>
            <VSeparator />
            <Subscribe to={[GraphQLProvider]}>
              {(graph: GraphQLProvider) => {
                this.graphQLProvider = graph;
                return (
                  <PrimaryButton
                    text={AppointmentStrings.ScheduleAction}
                    disabled={!this.state.selectedHour}
                    onTap={this.handleButtonTap}
                    // loading={graph.state.loading}
                    expanded={true}
                  />
                );
              }}
            </Subscribe>
            <VSeparator />
          </VBox>
        </Root>
      </CollapsibleHeader>
    );
  }

  private getMinDate(): Date {
    const tomorrow: Date = DateUtils.Create.getCurrent(1);
    const propsMinDate: Date = this.props.navigation.minDate;

    return propsMinDate && propsMinDate > tomorrow ? propsMinDate : tomorrow;
  }

  private readonly handleDateChange = (date: Moment, _) => {
    this.setState({ selectedHour: null });
    this.setState({ selectedDay: date });
  };

  private readonly handleMonthChange = (firstDayOfSelectedMonth: Moment) => {
    const startDate: Moment = this.checkNextStartDate(firstDayOfSelectedMonth);

    if (startDate) {
      this.setState({
        startDateToFetch: startDate,
        selectedDay: startDate,
        selectedHour: null,
      });
    } else {
      this.setState({ selectedHour: null, selectedDay: null });
    }
  };

  private checkNextStartDate(firstDayOfSelectedMonth: Moment): Moment | null {
    const isBeforeMin: boolean = firstDayOfSelectedMonth.isBefore(this.minDate);
    const isBeforeMax: boolean =
      this.maxDate && firstDayOfSelectedMonth.isBefore(this.maxDate);

    if (isBeforeMin) {
      const firstDayOfMin: Moment = moment({
        year: this.minDate.year(),
        month: this.minDate.month(),
      });
      const isSameMonth: boolean = firstDayOfMin.isSame(
        firstDayOfSelectedMonth,
      );

      if (isSameMonth) {
        return this.minDate;
      }
    } else if (!this.maxDate || isBeforeMax) {
      return firstDayOfSelectedMonth;
    }

    return null;
  }

  private handleTagSelection = (dateSelected: {
    start: string;
    end: string;
  }) => {
    this.setState({
      selectedHour: moment(dateSelected.start),
      endHour: moment(dateSelected.end),
    });
  };

  private handleButtonTap = () => {
    const friendlyDate = this.state.selectedDay.format('DD/MM/YYYY (dddd)');
    const friendlyHour = this.state.selectedHour.format('HH:mm');

    const scheduleAppointmentMutationInput: AppointmentInput = {
      start:
        this.state.selectedDay.format('YYYY-MM-DD') +
        'T' +
        this.state.selectedHour.format('HH:mm:ss'),
      end:
        this.state.selectedDay.format('YYYY-MM-DD') +
        'T' +
        this.state.endHour.format('HH:mm:ss'),
      professionalId: this.props.navigation.professionalId,
      locationId: this.props.navigation.locationId,
      carePlan: this.props.navigation.carePlanCodes,
    };

    const onPress = () =>
      this.scheduleAppointmentMutation(scheduleAppointmentMutationInput);

    new AlertBuilder()
      .withTitle(AppointmentStrings.Confirmation.Title)
      .withMessage(
        AppointmentStrings.Confirmation.Message(friendlyDate, friendlyHour),
      )
      .withButton({
        text: AppointmentStrings.Confirmation.Cancel,
        style: 'cancel',
      })
      .withButton({ text: AppointmentStrings.Confirmation.Submit, onPress })
      .show();
  };

  private scheduleAppointmentMutation = async (data: AppointmentInput) => {
    execSchedulePost(data)
      .then(res =>
        this.onMutationSuccess({
          ScheduleAppointment: res,
        } as unknown as ScheduleAppointmentMutation),
      )
      .catch(err => this.onMutationError(err));
    // this.graphQLProvider.mutate<ScheduleAppointmentMutationVariables>(
    //   'schedule-appointment',
    //   { data },
    //   this.onMutationSuccess,
    //   this.onMutationError,
    // );
  };

  private onMutationSuccess = async (resp: ScheduleAppointmentMutation) => {
    if (this.props.navigation.onScheduleSuccess) {
      const appointment: CarePlanAppointmentFragment = resp.ScheduleAppointment;
      this.props.navigation.onScheduleSuccess(appointment);
    }

    await Navigation.dismissModal(this.props.componentId);

    await Navigation.showModal<AppointmentSolicitationPageProps>(
      AppointmentSolicitationPage,
      { appointment: resp.ScheduleAppointment },
    );
  };

  private onMutationError = error => {
    FlashMessageService.showError(ErrorMapper.map(error));
  };
}
