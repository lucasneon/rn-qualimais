import moment from "moment";
import * as React from "react";
import { Image } from "react-native";
import { WatermarkType } from "@app/components/atm.watermark";
import { EventCell, EventCellProps } from "@app/components/mol.cell";
import { CollapsibleHeader } from "@app/components/org.collapsible-header";
import { PageView } from "@app/core/analytics";
import { Navigation, NavigationPageProps } from "@app/core/navigation";
import { CarePlanAppointmentFragment } from "@app/data/graphql";
import { DateUtils } from "@app/utils";
import {
  Asset,
  Body,
  Cell,
  PrimaryButton,
  Root,
  VBox,
  VSeparator,
} from "@atomic";
import { AppointmentStrings } from "./appointment.strings";

export interface AppointmentSolicitationPageProps {
  appointment: CarePlanAppointmentFragment;
}

PageView("AppointmentSolicitation");
export class AppointmentSolicitationPage extends React.Component<
  NavigationPageProps<AppointmentSolicitationPageProps>
> {
  static options = { name: "AppointmentSolicitationPage" };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  render() {
    const cellProps: EventCellProps = this.mapToCellProps(
      this.props.navigation.appointment
    );

    return (
      <Root>
        <CollapsibleHeader
          title={AppointmentStrings.SolicitationConfirmed.Title}
        >
          <VBox vAlign="center" hAlign="center">
            <VSeparator />
            <Image source={Asset.Onboard.OnboardCalendar} />
            <VSeparator />
          </VBox>
          <Cell>
            <Body>{AppointmentStrings.SolicitationConfirmed.Message}</Body>
          </Cell>
          <EventCell disabled={true} {...cellProps} />
          <VSeparator />
        </CollapsibleHeader>
        <VBox>
          <PrimaryButton
            text={AppointmentStrings.SolicitationConfirmed.Back}
            expanded={true}
            onTap={this.handleBackButtonTap}
          />
          <VSeparator />
        </VBox>
      </Root>
    );
  }

  private mapToCellProps(
    appointment: CarePlanAppointmentFragment
  ): EventCellProps {
    let title = appointment.doctor.name;
    if (
      appointment.doctor &&
      appointment.doctor.specialty &&
      appointment.doctor.specialty.description
    ) {
      title = appointment.doctor.specialty.description;
    }

    return {
      title: title,
      type: WatermarkType.Consultation,
      auxiliar:
        appointment.carePlanItem &&
        AppointmentStrings.SolicitationConfirmed.CellDateText(
          DateUtils.Format.presentation(
            appointment.carePlanItem &&
              appointment.carePlanItem.schedule.startDate
          ),
          moment(
            appointment.carePlanItem &&
              appointment.carePlanItem.schedule.startDate
          ).format("HH:mm")
        ),
      tag: AppointmentStrings.SolicitationConfirmed.Tag,
    };
  }

  private handleBackButtonTap = async () => {
    await Navigation.dismissModal(this.props.componentId);
  };
}
