import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { Image, View } from 'react-native';
import { Asset, Badge, Body, ErrorBody, H3, HBox, SecondaryButton, Shimmer, VSeparator } from '@atomic';

export interface SectionProps {
  title: string;
  type?: 'tag' | 'text';
  tags: string[];
  loading?: boolean;
  error?: ApolloError;
  readOnly?: boolean;
  onDismiss?: (id: string) => void;
  onButtonTap?: () => void;
}

const TagsShimmer = () => (
  <HBox>
    <HBox.Item wrap={true}><Shimmer height={30} width={80} rounded={true} /></HBox.Item>
    <HBox.Item wrap={true}><Shimmer height={30} width={80} rounded={true} /></HBox.Item>
    <HBox.Item wrap={true}><Shimmer height={30} width={80} rounded={true} /></HBox.Item>
    <HBox.Separator />
  </HBox>
);

const TextShimmer = () => (
  <>
    <Shimmer height={20} /><Shimmer height={20} width='80%' /><Shimmer height={20} width='70%' />
  </>
);

const Section: React.SFC<SectionProps> = props => (
  <View>
    <HBox wrap={true}>
      <H3>{props.title}</H3>
      {props.error && <Image source={Asset.Icon.Custom.Warning}/>}
    </HBox>
    {props.loading && (props.type && props.type === 'tag' ? <TagsShimmer /> : <TextShimmer />)}
    {props.tags && props.children}
    {props.error && <ErrorBody>Não foi possível carregar os registros.</ErrorBody>}
    <VSeparator />
  </View>
);

export const TagSection = (props: SectionProps) => (
  <Section {...props} type='tag'>
    <HBox wrap={true}>
      {props.tags && props.tags.length === 0 && <Body>Não há registros.</Body>}
      {props.tags && props.tags.map((tag: string) => (
        <Badge key={tag} type='neutral' text={tag} onDismiss={props.onDismiss} />
      ))}
    </HBox>
    {!props.readOnly &&
      <HBox>
        <SecondaryButton short={true} text='Adicionar' onTap={props.onButtonTap} />
      </HBox>
    }
  </Section>
);

export const TextSection = (props: SectionProps) => (
  <Section {...props} type='text'>
    {props.tags && props.tags.length === 0 && <Body>Não há registros.</Body>}
    {props.tags && props.tags.map(tag => <Body key={tag}>• {tag}</Body>)}
  </Section>
);
