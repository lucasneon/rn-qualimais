import moment from 'moment';
import {
  ConditionFragment,
  ConditionQuery,
  FamilyHistoryFragment,
  FamilyHistoryQuery,
  ProcedureFragment,
  ProceduresQuery,
  VaccineFragment,
} from '@app/data/graphql';

const SEPARATOR = ' · ';

export function mapVaccines(data: VaccineFragment[]): string[] {
  // https://momentjs.com/docs/#/displaying/format/
  return data && data.map(v => formatTag(v.display, v.takenAt));
}
/**
 * @deprecated use mapProceduresFragment instead
 */
export function mapProcedures(data: ProceduresQuery): string[] {
  return data.Procedures && data.Procedures.map(p => p.display);
}

export function mapProceduresFragment(data: ProcedureFragment[]): string[] {
  return data && data.map(p => p.display);
}

export function mapCondition(data: ConditionQuery): string[] {
  return data.Condition
    ? data.Condition.map(item => formatTag(item.display, item.assertedDate))
    : null;
}

export function mapConditionFragment(data: ConditionFragment[]): string[] {
  return data
    ? data.map(item => formatTag(item.display, item.assertedDate))
    : null;
}

export function mapFamilyHistory(data: FamilyHistoryQuery): string[] {
  return data.FamilyHistory
    ? data.FamilyHistory.map(
        item =>
          item.display +
          (item.relationship.description
            ? SEPARATOR + item.relationship.description
            : ''),
      )
    : null;
}

export function mapFamilyHistoryFragment(
  data: FamilyHistoryFragment[],
): string[] {
  return data
    ? data.map(
        item =>
          item.display +
          (item.relationship.description
            ? SEPARATOR + item.relationship.description
            : ''),
      )
    : null;
}

function formatTag(display: string, date: string) {
  return display + SEPARATOR + moment(date).format('DD MMM YYYY');
}
