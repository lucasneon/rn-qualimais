import moment from 'moment';
import * as React from 'react';
import { Container } from 'typedi';
import { CardActionSheet } from '@app/components/mol.card-action-sheet';
import { ReviewCardContent } from '@app/components/obj.review';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { FlashMessageService } from '@app/core/navigation';
import { RateDoctorInput, RateDoctorMutation, RateDoctorMutationVariables } from '@app/data/graphql';
import { AppError } from '@app/model';
import { EventsStrings } from './events.strings';

export interface AppointmentReviewProps {
  doctorCPF: string;
  doctorName: string;
  specialtyCode: string;
  appointmentDate: Date;
  onSuccess: (message: string) => void;
}

interface AppointmentReviewState {
  loading: boolean;
  visible: boolean;
}

const Strings = EventsStrings.Appointment.Review;

export class AppointmentReview extends React.PureComponent<AppointmentReviewProps, AppointmentReviewState> {
  private readonly graphQLProvider: GraphQLProvider = Container.get(GraphQLProviderToken);

  constructor(props) {
    super(props);
    this.state = { visible: true, loading: null };
  }

  render() {
    const date: Date = this.props.appointmentDate;

    return (
      <CardActionSheet visible={this.state.visible} onCancel={this.handleCancel}>
        <ReviewCardContent
          onSend={this.handleSendRate}
          title={Strings.Title(this.props.doctorName)}
          description={date && moment(date).format('DD/MM/YYYY - dddd · HH:mm')}
          onCancel={this.handleCancel}
          loading={this.state.loading}
        />
      </CardActionSheet>
    );
  }

  private handleCancel = () => {
    this.setState({ visible: false });
  }

  private handleSendRate = (rate: number, comment?: string) => {
    this.setState({ loading: true });

    const input: RateDoctorInput = {
      doctorCpf: this.props.doctorCPF,
      specialtyCode: this.props.specialtyCode,
      observation: comment,
      rating: rate,
    };

    this.graphQLProvider.mutate<RateDoctorMutationVariables>(
      'rate-doctor',
      { data: input },
      this.handleSuccess,
      this.handleError,
    );
  }

  private handleSuccess = (data: RateDoctorMutation) => {
    this.setState({ loading: false, visible: false });
    this.props.onSuccess(data.RateDoctor);
  }

  private handleError = (error: AppError) => {
    this.setState({ loading: false });
    FlashMessageService.showError(error);
  }
}
