import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { Container } from 'typedi';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import {
  AppointmentDetailsQuery,
  AppointmentDetailsQueryVariables,
  CarePlanAppointmentFragment,
  CarePlanCodes,
} from '@app/data/graphql';
import { AppError, GeoCoordinates } from '@app/model';
import {
  LocationProvider,
  LocationProviderToken,
} from '@app/services/providers';
import { ErrorMapper } from '@app/utils';
import { fetchAppointmentDetails } from './api/appointment-details.service';

interface State {
  loading?: boolean;
  error?: AppError;
  details?: CarePlanAppointmentFragment;
}

export interface AppointmentDetailsContainerChildren extends State {
  location?: GeoCoordinates;
  onAppointmentScheduleSuccess: (
    appointment: CarePlanAppointmentFragment,
  ) => void;
}

export interface AppointmentDetailsProps {
  appointmentDetailsInput: CarePlanCodes;
  children: (res: AppointmentDetailsContainerChildren) => React.ReactNode;
}

export class AppointmentDetailsContainer extends React.Component<
  AppointmentDetailsProps,
  State
> {
  private readonly graphQLProvider: GraphQLProvider =
    Container.get(GraphQLProviderToken);
  private readonly locationProvider: LocationProvider = Container.get(
    LocationProviderToken,
  );

  constructor(props) {
    super(props);
    this.state = { loading: null, error: null, details: null };
  }

  componentDidMount() {
    this.locationProvider.handlePlatforms();
    this.fetchAppointmentDetails();
  }

  render() {
    return this.props.children({
      loading: this.locationProvider.state.fetching || this.state.loading,
      details: this.state.details,
      error: this.state.error,
      location: this.locationProvider.state.location,
      onAppointmentScheduleSuccess: this.handleAppointmentScheduleSuccess,
    });
  }

  private async fetchAppointmentDetails() {
    this.setState({ loading: true });

    const res = await fetchAppointmentDetails({
      data: this.props.appointmentDetailsInput,
    });
    if (res) {
      console.warn('AppointmentDetails >', res);
      this.setState({ loading: false, details: res });
      return;
    }
    this.setState({ loading: false, error: ErrorMapper.map(error) });
    console.warn('AppointmentDetails >', res);

    // this.graphQLProvider.query<AppointmentDetailsQueryVariables>(
    //   'appointment-details',
    //   { data: this.props.appointmentDetailsInput },
    //   (data: AppointmentDetailsQuery) =>
    //     this.setState({ loading: false, details: data.AppointmentDetails }),
    //   (error: ApolloError) =>
    //     this.setState({ loading: false, error: ErrorMapper.map(error) }),
    // );
  }

  private handleAppointmentScheduleSuccess = (
    appointment: CarePlanAppointmentFragment,
  ) => {
    this.setState({ details: appointment });
  };
}
