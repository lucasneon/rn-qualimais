import { ApolloError } from 'apollo-client';
import moment from 'moment';
import * as React from 'react';
import { MutationResult } from 'react-apollo';
import { Container } from 'typedi';
import { AlertBuilder } from '@app/components/atm.alert-builder';
import { ButtonFooter } from '@app/components/mol.button-footer';
import { ListInfoCellProps } from '@app/components/mol.cell';
import { ScheduleDetails } from '@app/components/mol.details/schedule-details.component';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { MutationContainer, MutationVariablesFn } from '@app/core/graphql';
import {
  ExternalNavigator,
  FlashMessageService,
  Navigation,
  NavigationPageProps,
} from '@app/core/navigation';
import {
  CancelScheduledAppointmentMutation,
  CancelScheduledAppointmentMutationVariables,
  CarePlanAppointmentFragment,
  CarePlanCodes,
} from '@app/data/graphql';
import {
  SchedulesPage,
  SchedulesPageProps,
} from '@app/modules/common/appointment';
import { GeneralStrings } from '@app/modules/common/common.strings';
import { HomePage } from '@app/modules/home';
import {
  Asset,
  DividerGray,
  FontSize,
  LinkButtonProps,
  LoadingState,
  Shimmer,
  VBox,
  VSeparator,
} from '@atomic';
import {
  AppointmentDetailsContainer,
  AppointmentDetailsContainerChildren,
} from './appointment-details.container';
import { AppointmentReview } from './appointment-review.component';
import { EventsStrings } from './events.strings';

export interface AppointmentDetailPageProps {
  appointmentDetailsInput: CarePlanCodes;
  review?: boolean;
  onAppointmentScheduled: () => void;
  onAppointmentCancel?: () => void;
}

const AppointmentDetailsShimmer = () => (
  <VBox>
    <Shimmer width="40%" height={FontSize.Medium} />
    <VSeparator />
    <DividerGray />
    <VSeparator />
    <Shimmer width="60%" height={FontSize.Medium} />
    <Shimmer width="40%" height={FontSize.Medium} />
    <Shimmer width="60%" height={FontSize.Medium} />
    <Shimmer width="50%" height={FontSize.Small} />
    <Shimmer width="50%" height={FontSize.Medium} />
    <Shimmer width="40%" height={FontSize.Small} />
    <Shimmer width="40%" height={FontSize.Medium} />
    <Shimmer width="30%" height={FontSize.Small} />
    <VSeparator />
    <DividerGray />
    <VSeparator />
    <Shimmer width="30%" height={FontSize.Medium} />
    <Shimmer width="96%" height={FontSize.Small} />
    <Shimmer width="96%" height={FontSize.Small} />
  </VBox>
);

@PageView('AppointmentDetail')
export class AppointmentDetailPage extends React.Component<
  NavigationPageProps<AppointmentDetailPageProps>
> {
  static options = { name: 'AppointmentDetailPage' };

  private readonly externalNavigator: ExternalNavigator =
    Container.get(ExternalNavigator);

  private details: CarePlanAppointmentFragment;

  render() {
    return (
      <AppointmentDetailsContainer
        appointmentDetailsInput={this.props.navigation.appointmentDetailsInput}>
        {(res: AppointmentDetailsContainerChildren) => {
          this.details = res.details;

          const schedule =
            this.details &&
            this.details.carePlanItem &&
            this.details.carePlanItem.schedule;
          const presentationLimitDate =
            schedule && moment(schedule.limitDate).format('DD MMM');
          const presentationScheduledDate =
            schedule &&
            schedule.scheduledFor &&
            moment(schedule.scheduledFor).format('DD MMM (ddd), HH:mm') + 'h';

          const procedureName =
            this.details &&
            this.details.carePlanItem &&
            this.details.carePlanItem.specialty;
          const doctor = this.details && this.details.doctor;

          return (
            <>
              <CollapsibleHeader
                title={procedureName && procedureName.description}>
                <LoadingState
                  loading={res.loading}
                  error={!!res.error}
                  data={!!this.details}>
                  <LoadingState.Shimmer>
                    <AppointmentDetailsShimmer />
                  </LoadingState.Shimmer>
                  <LoadingState.ErrorPlaceholder>
                    <PlaceholderSelector error={res.error} />
                  </LoadingState.ErrorPlaceholder>
                  <ScheduleDetails
                    /*TODO pegar observação como props do server*/
                    dueDate={presentationLimitDate}
                    appointmentDate={presentationScheduledDate}
                    doctorInfo={
                      doctor && this.details.location && this.getInfoCellProps()
                    }
                    onTap={this.handlePlaceholderTap(
                      res.onAppointmentScheduleSuccess,
                    )}
                  />
                  {this.props.navigation.review && doctor && (
                    <AppointmentReview
                      doctorCPF={doctor.cpf}
                      doctorName={doctor.name}
                      specialtyCode={doctor.specialty && doctor.specialty.id}
                      appointmentDate={
                        schedule && schedule.doneAt && new Date(schedule.doneAt)
                      }
                      onSuccess={this.handleReviewSuccess}
                    />
                  )}
                </LoadingState>
              </CollapsibleHeader>

              {presentationScheduledDate && this.details.appointmentId && (
                <MutationContainer
                  document={'cancel-appointment'}
                  onCompleted={this.handleScheduleCancel}
                  onError={this.handleError}>
                  {(
                    mutateWithVariables: MutationVariablesFn<CancelScheduledAppointmentMutationVariables>,
                    cancelmutationresult: MutationResult<CancelScheduledAppointmentMutation>,
                  ) => {
                    const handleCancelation = () => {
                      mutateWithVariables({
                        data: { appointmentId: this.details.appointmentId },
                      });
                    };
                    const alert = () =>
                      this.showCancelConfirmationAlert(handleCancelation);
                    return (
                      <>
                        {!res.loading && (
                          <ButtonFooter
                            secondary={{
                              text: EventsStrings.Alert.Title,
                              onTap: alert,
                              loading: cancelmutationresult.loading,
                              negative: true,
                            }}
                          />
                        )}
                      </>
                    );
                  }}
                </MutationContainer>
              )}
            </>
          );
        }}
      </AppointmentDetailsContainer>
    );
  }

  // TODO Abstract function mapToCarePointItem from list-map.component
  private getInfoCellProps = (): ListInfoCellProps => {
    const doctor = this.details && this.details.doctor;
    const location = this.details && this.details.location;
    const address = location && location.address;

    const locationContact = location.contact;
    const email = locationContact && locationContact.email;
    const website = locationContact && locationContact.website;
    const phoneNumbers = locationContact && locationContact.phoneNumbers;

    const links: LinkButtonProps[] = [];

    if (phoneNumbers && phoneNumbers.length > 0) {
      links.concat(
        phoneNumbers.map(phone => ({
          image: Asset.Icon.Custom.Phone,
          text: phone,
          onTap: () => this.externalNavigator.makeCall(phone),
        })),
      );
    }

    if (email) {
      links.push({
        text: email,
        image: Asset.Icon.Custom.Summary,
        onTap: () => this.externalNavigator.openEmail(email),
      });
    }

    if (website) {
      links.push({
        text: website,
        image: Asset.Icon.Custom.Web,
        onTap: () => this.externalNavigator.openWebsite(website),
      });
    }

    links.push({
      image: Asset.Icon.Custom.NavigationIcon,
      text: GeneralStrings.TraceRoute,
      onTap: () =>
        this.externalNavigator.openMaps({
          latitude: address.latitude,
          longitude: address.longitude,
        }),
    });

    const addressNumber = address.addressNumber
      ? address.addressNumber + ', '
      : '';
    const complement = address.complement ? address.complement + ',' : '';

    return {
      title: doctor.name,
      descriptions: [
        `${address.street}, ${addressNumber}${complement} •`,
        `${address.district} — ${address.city}, ${address.state} •`,
        address.cep,
      ],
      links,
    };
  };

  private handlePlaceholderTap = (
    onScheduled: (appointment: CarePlanAppointmentFragment) => void,
  ) => {
    return () => {
      const carePlanItem = this.details.carePlanItem;

      const carePlanCodes: CarePlanCodes = {
        itemCode: carePlanItem.itemCode,
        scheduleCode: carePlanItem.schedule.id,
      };

      const onScheduleSuccess = (appointment: CarePlanAppointmentFragment) => {
        onScheduled(appointment);
        this.props.navigation.onAppointmentScheduled();
      };

      Navigation.showModal<SchedulesPageProps>(SchedulesPage, {
        specialtyCode: carePlanItem && carePlanItem.specialty.id,
        carePlanCodes,
        onScheduleSuccess,
        minDate: new Date(
          carePlanItem.schedule && carePlanItem.schedule.startDate,
        ),
        maxDate: new Date(
          carePlanItem.schedule && carePlanItem.schedule.limitDate,
        ),
      });
    };
  };

  private showCancelConfirmationAlert = (cancelFn: () => void) => {
    new AlertBuilder()
      .withTitle(EventsStrings.Alert.Title)
      .withMessage(EventsStrings.Alert.Message)
      .withButton({ text: EventsStrings.Alert.Cancel, style: 'cancel' })
      .withButton({ text: EventsStrings.Alert.Confirm, onPress: cancelFn })
      .show();
  };

  private handleReviewSuccess = (message: string) => {
    FlashMessageService.showSuccess(message);
  };

  private handleScheduleCancel = () => {
    FlashMessageService.showSuccess(EventsStrings.FufillDialog.Cancelation);
    if (this.props.navigation.onAppointmentCancel) {
      this.props.navigation.onAppointmentCancel();
    }
    Navigation.push('HomePage', HomePage);
  };

  private handleError = (error: ApolloError) => {
    FlashMessageService.showError(error);
  };
}
