// tslint:disable:max-line-length

export const EventsStrings = {
  Observations: 'observações',
  Schedule: 'Agendar',
  Reschedule: 'Alterar agendamento',
  FufillDialog: {
    Title: 'Marcar como realizado',
    Message: 'Tem certeza que desja marcar esse exame como realizado?',
    Cancelation: 'Estava ocupado no dia? Você ainda pode remarcar até a data limite',
  },
  Alert: {
    Title: 'Cancelar agendamento',
    Message: 'Tem certeza que deseja cancelar seu agendamento? Você pode reagendar até o tempo limite.',
    Confirm: 'Sim',
    Cancel: 'Não',
  },
  Laboratories: {
    Title: 'Laboratórios',
    ContactIn: 'Entre em contato com um dos laboratórios abaixo para agendar o exame',
    NotFound: {
      Title: 'Sem resultados',
      Description: 'Não foram encontrados laboratórios para este exame.',
    },
  },
  Exam: {
    Title: 'Exame',
    Description: 'Descrição',
    Question: 'Quando você realizou o exame?',
    Review: {
      Title: (examName: string) => `Avalie o atendimento no exame ${examName}`,
    },
  },
  Appointment: {
    Title: 'Consulta',
    Details: {
      Doctor: (doctorName: string) => `Dr. ${doctorName}`,
      Observation: 'observação',
      Warning: (date: string) => `Não se esqueça, de agendar sua consulta até ${date} `,
      NotFound: {
        Title: `Erro de cadastro`,
        Error: `Houve um erro ao recuperar as informações da sua consulta, favor entrar em contato com seu Guia`,
      },
    },
    NoSchedule: {
      Title: 'Consulta não agendada',
      Message: (limitDate?: string) => 'Consulta ainda não foi agendada.' + (limitDate ? ` A data limite para realização é ${limitDate}` : ''),
      Action: 'Agendar consulta',
    },
    Review: {
      Title: (doctorName: string) => `Avalie o atendimento do Dr. ${doctorName}`,
    },
  },
};
