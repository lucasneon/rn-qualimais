import { ApolloError } from 'apollo-client';
import moment from 'moment';
import * as React from 'react';
import { Container } from 'typedi';
import { CardActionSheet } from '@app/components/mol.card-action-sheet';
import { ReviewCardContent } from '@app/components/obj.review';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { FlashMessageService } from '@app/core/navigation';
import { RateExamInput, RateExamMutation, RateExamMutationVariables } from '@app/data/graphql';
import { EventsStrings } from './events.strings';

export interface ExamReviewProps {
  procedureCode: string;
  procedureName: string;
  doneAt: Date;
  visible: boolean;
  onSuccess: (message: string) => void;
  onCancel: () => void;
}

interface ExamReviewState {
  loading: boolean;
}

const Strings = EventsStrings.Exam.Review;

export class ExamReview extends React.PureComponent<ExamReviewProps, ExamReviewState> {
  private readonly graphQLProvider: GraphQLProvider = Container.get(GraphQLProviderToken);

  constructor(props) {
    super(props);
    this.state = { loading: null };
  }

  render() {
    const completionDate: Date = this.props.doneAt;

    return (
      <CardActionSheet visible={this.props.visible} onCancel={this.handleCancel}>
        <ReviewCardContent
          onSend={this.handleSendRate}
          title={Strings.Title(this.props.procedureName)}
          description={completionDate && moment(completionDate).format('DD/MM/YYYY - dddd · HH:mm')}
          onCancel={this.handleCancel}
          loading={this.state.loading}
        />
      </CardActionSheet>
    );
  }

  private handleCancel = () => {
    this.props.onCancel();
  }

  private handleSendRate = (rate: number, comment?: string) => {
    this.setState({ loading: true });

    const input: RateExamInput = {
      procedureCode: this.props.procedureCode,
      observation: comment,
      rating: rate,
    };

    this.graphQLProvider.mutate<RateExamMutationVariables>(
      'rate-exam',
      { data: input },
      this.handleSuccess,
      this.handleError,
    );
  }

  private handleSuccess = (data: RateExamMutation) => {
    this.setState({ loading: false });
    this.props.onSuccess(data.RateExam);
  }

  private handleError = (error: ApolloError) => {
    this.setState({ loading: false });
    FlashMessageService.showError(error);
  }
}
