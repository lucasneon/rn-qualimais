import * as React from 'react';
import { ButtonFooter } from '@app/components/mol.button-footer';
import { PlaceholderSelector } from '@app/components/mol.placeholder';
import { ExamDialog } from '@app/components/obj.dialog/dialog-card-content.component';
import { CarePlanEventsMapper } from '@app/components/org.care-plan-events/care-plan-events.mapper';
import { CarePlanEventsStrings } from '@app/components/org.care-plan-events/care-plan-events.strings';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { FlashMessageService } from '@app/core/navigation/flash-message.service';
import { CarePlanItemFragment, FulfillEventInput } from '@app/data/graphql';
import { AppError, GeoCoordinates } from '@app/model';
import { ErrorMapper } from '@app/utils';
import {
  Body,
  ButtonProps,
  FontSize,
  H3,
  LoadingState,
  Root,
  Shimmer,
  VBox,
  VSeparator,
} from '@atomic';
import { EventsStrings } from './events.strings';
import { LaboratoriesPage, LaboratoriesPageProps } from './laboratories.page';
import Container from 'typedi';
import { ActiveNetworkSearch } from '@app/config';
import {
  ExamDetailsProvider,
  ExamDetailsProviderState,
} from '@app/services/providers/exam-detail.provider';
import { Subscribe } from 'unstated';
import { DefaultCenter, LocationProvider, LocationState } from '@app/services';
import { RightSegment } from '@app/components/mol.segmented-control/segmented-control.style';

export interface ExamDetailPageProps {
  carePlanScheduleCode: string;
  carePlanItemCode: string;
  examSpecialtyCode: string;
  loadingLocaction: boolean;
  procedureCode: string;
  location?: GeoCoordinates;
  onFufillSuccess: () => void;
  onReview: (data: any) => void;
  loadingLocation: boolean;
  userLocation?: GeoCoordinates;
}

export interface ExamReviewData {
  procedureName: string;
  procedureCode: string;
  doneAt?: string;
}

const ExamDetailShimmer = () => (
  <VBox>
    <Shimmer width="60%" height={FontSize.Medium} />
    <VSeparator />
    <Shimmer width="40%" height={FontSize.Medium} />
    <Shimmer width="96%" height={FontSize.Small} />
    <Shimmer width="98%" height={FontSize.Small} />
    <Shimmer width="80%" height={FontSize.Small} />
  </VBox>
);

interface ExamDetailPageState {
  fufillLoad: boolean;
  didFufill: boolean;
  showReview: boolean;
  doneReview: boolean;
}

@PageView('ExamDetail')
export class ExamDetailPage extends React.Component<
  NavigationPageProps<ExamDetailPageProps>,
  ExamDetailPageState
> {
  static options = { name: 'ExamDetailPage' };

  private data: ExamDetailsProviderState;

  private activeSearchNetwork: boolean = Container.get(ActiveNetworkSearch);

  private examDetailsProvider: ExamDetailsProvider;
  private locationProvider: LocationProvider;

  constructor(props) {
    super(props);
    this.state = {
      fufillLoad: null,
      didFufill: null,
      showReview: false,
      doneReview: false,
    };
  }

  componentDidMount() {
    this.locationProvider.handlePlatforms().then(() => {
      this.examDetailsProvider.rehydrate({ ...this.props.navigation });
    });
  }

  render() {
    return this.subscribes();
  }

  private subscribes() {
    return (
      <Subscribe to={[ExamDetailsProvider, LocationProvider]}>
        {(
          examDetailsProvider: ExamDetailsProvider,
          locationProvider: LocationProvider,
        ) => {
          this.locationProvider = locationProvider;
          const locationState: LocationState = locationProvider.state;
          const location: GeoCoordinates =
            locationState.location || DefaultCenter;

          if (locationProvider.state.fetching) {
            return (this.props.navigation.loadingLocaction = true);
          }

          examDetailsProvider.state.errorFn = this.handleFufillError;
          examDetailsProvider.state.successFn = this.handleFufillSuccess;
          this.props.navigation.loadingLocaction = false;
          this.props.navigation.userLocation = location;

          this.examDetailsProvider = examDetailsProvider;
          const state = examDetailsProvider.state;
          this.data = state;
          const carePlanItem: CarePlanItemFragment =
            this.data.details && this.data.details.carePlanItem;
          const schedule = carePlanItem && carePlanItem.schedule;
          const isAlreadyDone: boolean =
            schedule && !!schedule.doneAt === false;
          const procedure =
            this.data.details && this.data.details.carePlanItem.procedure;

          const fufillButtonProps: ButtonProps = {
            loading: this.state.fufillLoad,
            text: EventsStrings.FufillDialog.Title,
            onTap: this.handleFufillTap,
          };
          // const laboratories = examDetailsProvider.state.details?.laboratories;
          // console.warn(laboratories);

          return (
            <Root>
              <CollapsibleHeader title={EventsStrings.Exam.Title}>
                <LoadingState
                  loading={state.loading}
                  error={!!state.error}
                  data={!!this.data}>
                  <LoadingState.Shimmer>
                    <ExamDetailShimmer />
                  </LoadingState.Shimmer>
                  <LoadingState.ErrorPlaceholder>
                    <PlaceholderSelector error={state.error} />
                  </LoadingState.ErrorPlaceholder>
                  {carePlanItem && schedule && (
                    <VBox>
                      <H3>
                        {CarePlanEventsMapper.mapEventDateInfo(
                          schedule,
                          CarePlanEventsStrings.Fulfill.Exam,
                        )}
                      </H3>
                      <VSeparator />
                      <H3>{EventsStrings.Exam.Description}</H3>
                      {carePlanItem.procedure && (
                        <Body>{carePlanItem.procedure.description}</Body>
                      )}
                    </VBox>
                  )}
                </LoadingState>
              </CollapsibleHeader>
              {isAlreadyDone && this.activeSearchNetwork && (
                <ButtonFooter secondary={fufillButtonProps} />
              )}

              {isAlreadyDone && this.activeSearchNetwork === false && (
                <ButtonFooter
                  primary={{
                    text: EventsStrings.Laboratories.Title,
                    onTap: this.handleScheduleTap,
                  }}
                  secondary={fufillButtonProps}
                />
              )}

              <ExamDialog
                description={procedure && procedure.description}
                maxDate={
                  new Date(
                    this.data.details &&
                      this.data.details.carePlanItem.schedule.limitDate,
                  )
                }
                loading={state.fulfillRequestLoading}
                visible={this.state.fufillLoad}
                submit={this.handleFufillConfirm}
                onCancel={this.handleFullfilCancel}
              />
            </Root>
          );
        }}
      </Subscribe>
    );
  }

  private handleScheduleTap = async () => {
    console.log(this.data);
    if (this.data && this.data.details.laboratories) {
      await Navigation.push<LaboratoriesPageProps>(
        this.props.componentId,
        LaboratoriesPage,
        { laboratories: this.data.details.laboratories },
      );
    } else {
      await Navigation.push<LaboratoriesPageProps>(
        this.props.componentId,
        LaboratoriesPage,
        { laboratories: [] },
      );
    }
  };

  private handleFufillTap = () => {
    this.setState({ fufillLoad: true });
  };

  private handleFufillSuccess = async (fulfillDate: string) => {
    this.setState({ fufillLoad: false });
    const examReviewData: ExamReviewData = {
      procedureName:
        this.data.details.carePlanItem &&
        this.data.details.carePlanItem.procedure.description,
      procedureCode:
        this.data.details.carePlanItem &&
        this.data.details.carePlanItem.procedure.id,
      doneAt: fulfillDate,
    };
    this.props.navigation.onFufillSuccess();
    await Navigation.pop(this.props.componentId);
    // if (fulfillDate) {
    //   this.props.navigation.onReview(examReviewData);
    // }
  };

  private handleFufillError = (error: AppError) => {
    this.setState({ fufillLoad: false });
    FlashMessageService.showError(ErrorMapper.map(error));
  };

  private handleFufillConfirm = (date: string) => {
    const input: FulfillEventInput = {
      id: this.data.details.carePlanItem.schedule.id,
      itemCode: this.data.details.carePlanItem.itemCode,
      fulfilledAt: date,
    };
    console.warn(input);
    this.data.fulfillFn(input);
  };

  private handleFullfilCancel = () => {
    this.setState({ fufillLoad: false });
  };
}
