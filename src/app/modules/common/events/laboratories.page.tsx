import * as React from 'react';
import { Subscribe } from 'unstated';
import { PageView } from '@app/core/analytics';
import { NavigationPageProps } from '@app/core/navigation';
import { CarePointNetworkFragment } from '@app/data/graphql';
import { ListMap, ListMapItem } from '@app/modules/common/list-map.component';
import { LocationProvider } from '@app/services/providers';
import { EventsStrings } from './events.strings';

export interface LaboratoriesPageProps {
  laboratories: CarePointNetworkFragment[];
}

@PageView('Laboratories')
export class LaboratoriesPage extends React.PureComponent<NavigationPageProps<LaboratoriesPageProps>> {
  static options = { name: 'LaboratoriesPage' };

  render() {
    return (
      <Subscribe to={[LocationProvider]}>
        {(locationProvider: LocationProvider) => {
          const { laboratories } = this.props.navigation;

          return (
            <ListMap
              loading={locationProvider.state.fetching}
              userLocation={locationProvider.state.location}
              locationError={locationProvider.state.error}
              items={laboratories.map(this.mapToListItem)}
              title={EventsStrings.Laboratories.Title}
              notFoundTitle={EventsStrings.Laboratories.NotFound.Title}
              notFoundMessage={EventsStrings.Laboratories.NotFound.Description}
            />
          );
        }}
      </Subscribe>
    );
  }

  private mapToListItem = (emergencyCare: CarePointNetworkFragment): ListMapItem => {
    return {
      title: emergencyCare.carePoint.name,
      key: emergencyCare.practitionerId,
      distance: (Math.round((emergencyCare.distance / 1000) * 100) / 100),
      carePoint: emergencyCare.carePoint,
    };
  }
}
