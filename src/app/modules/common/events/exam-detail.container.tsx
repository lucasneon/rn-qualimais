import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { QueryResult } from 'react-apollo';
import Container from 'typedi';
import { Subscribe } from 'unstated';
import {
  GraphQLProvider,
  GraphQLProviderToken,
  QueryContainer,
} from '@app/core/graphql';
import {
  ExamDetailsFragment,
  ExamDetailsQuery,
  ExamDetailsQueryVariables,
  FulfillEventInput,
  FulfillExamMutation,
  FulfillExamMutationVariables,
} from '@app/data/graphql';
import { AppError, GeoCoordinates } from '@app/model';
import {
  DefaultCenter,
  LocationError,
  LocationProvider,
} from '@app/services/providers';
import { ErrorMapper } from '@app/utils';
import { ExamDetailsInputModel } from './api/exam.model';

export interface ExamDetailContainerProps {
  carePlanScheduleCode: string;
  carePlanItemCode: string;
  examSpecialtyCode: string;
  procedureCode: string;
  children: (data: ExamDetailContainerChildrenData) => React.ReactNode;
  errorFn: (error: AppError) => void;
  successFn: (date: string) => Promise<void>;
}

export interface ExamDetailContainerChildrenData {
  loading: boolean;
  locationError?: LocationError;
  error?: AppError;
  details?: ExamDetailsFragment;
  fulfillRequestLoading?: boolean;
  fulfillFn?: (data: FulfillEventInput) => void;
}

export class ExamDetailsContainer extends React.PureComponent<ExamDetailContainerProps> {
  state = { fulfillRequestLoading: false };

  private readonly graphQLProvider: GraphQLProvider =
    Container.get(GraphQLProviderToken);
  private locationProvider: LocationProvider;

  componentDidMount() {
    this.locationProvider.handlePlatforms();
  }

  render() {
    return (
      <Subscribe to={[LocationProvider]}>
        {async (locationProvider: LocationProvider) => {
          this.locationProvider = locationProvider;

          const state = locationProvider.state;
          const location: GeoCoordinates = state.location || DefaultCenter;

          const input: ExamDetailsInputModel = {
            carePlanScheduleCode: this.props.carePlanScheduleCode,
            carePlanItemCode: this.props.carePlanItemCode,
            latitude: location.latitude,
            longitude: location.longitude,
            procedureCode: this.props.procedureCode,
          };

          return (
            <QueryContainer document="exam-details" variables={{ data: input }}>
              {(
                res: QueryResult<ExamDetailsQuery, ExamDetailsQueryVariables>,
              ) => {
                const details: ExamDetailsFragment =
                  res.data && res.data.ExamDetails;
                return this.props.children({
                  loading: res.loading,
                  error: res.error,
                  details,
                  fulfillFn: this.handleFufillDialogConfirmation,
                  fulfillRequestLoading: this.state.fulfillRequestLoading,
                });
              }}
            </QueryContainer>
          );
        }}
      </Subscribe>
    );
  }

  private handleFufillDialogConfirmation = (data: FulfillEventInput) => {
    this.setState({ fulfillRequestLoading: true });

    const input: FulfillEventInput = {
      id: data.id,
      itemCode: data.itemCode,
    };

    this.graphQLProvider.mutate<FulfillExamMutationVariables>(
      'fulfill-exam',
      { data: input },
      (date: FulfillExamMutation) => this.handleSucess(date.FulfillExam.doneAt),
      (error: ApolloError) => this.handleError(error),
    );
  };

  private handleSucess = (date: string) => {
    this.setState({ fulfillRequestLoading: false });
    this.props.successFn(date);
  };

  private handleError = (error: ApolloError) => {
    this.setState({ mfulfillRequestLoading: false });
    ErrorMapper.map(error);
    this.props.errorFn(error);
  };
}
