import { GAMA_OP_CODE, GAMA_STIMULATING_CODE } from '@app/config';
import { LocalDataSource } from '@app/data';
import {
  axiosGet,
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { CarePlanMapper } from '@app/modules/home/api/care-plan/care-plan.mapper';
import {
  CarePlanItemDetailModel,
  CarePlanItemModel,
  CarePlanScheduleModel,
  ProcedureTypeModel,
} from '@app/modules/home/api/care-plan/care-plan.model';
import Container from 'typedi';
import {
  CarePointNetworkRequest,
  ProcedureNetworkRequest,
} from './care-point-network.entity';
import { CarePointNetworkMapper } from './care-point-network.mapper';
import {
  CarePointNetworkInputModel,
  CarePointNetworkModel,
  DefaultMaxDistance,
  ProcedureCarePointsInputModel,
} from './care-point-network.model';
import { ExamDetailsInputModel } from './exam.model';

const opCode: string = Container.get(GAMA_OP_CODE);
const stimulatingCode: string = Container.get(GAMA_STIMULATING_CODE);

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchExamDetails = async (
  input: ExamDetailsInputModel,
): Promise<any> => {
  const [laboratories, carePlanItem]: [
    CarePointNetworkModel[],
    CarePlanItemModel,
  ] = await Promise.all([
    fetchProcedureCarePoints(input).catch(() => null),
    fetchCarePlanItemDetails({
      scheduleCode: input.carePlanScheduleCode,
      itemCode: input.carePlanItemCode,
    }),
  ]);

  if (!laboratories && !carePlanItem) {
    return { data: null, error: true };
  }
  carePlanItem.procedure.type = ProcedureTypeModel.Exam;

  return { data: { carePlanItem, laboratories }, error: false };
};

export const fetchProcedureCarePoints = async (
  input: ProcedureCarePointsInputModel,
): Promise<CarePointNetworkModel[]> => {
  if (!input.maxDistance) {
    input.maxDistance = DefaultMaxDistance;
  }

  const res = await carePointNetworkRequest(input);

  return res.data && res.data.map(CarePointNetworkMapper.mapResponse);
};

export const fetchCarePlanItemDetails = async (
  input: CarePlanItemDetailsInputModel,
): Promise<CarePlanItemModel> => {
  const carePlanItem: CarePlanItemDetailModel = await fetchItemDetails(
    input.itemCode,
  );
  const schedules: CarePlanScheduleModel[] =
    carePlanItem.schedules || carePlanItem.schedule;
  const schedule: CarePlanScheduleModel =
    schedules && schedules.find(it => it.id === input.scheduleCode);

  delete carePlanItem.schedules;

  return { ...carePlanItem, schedule };
};

const fetchItemDetails = async (
  itemCode: string,
): Promise<CarePlanItemDetailModel> => {
  const res = await itemDetailsRequest(itemCode);
  return (
    res.data && CarePlanMapper.mapItemDetails(res.data.beneficiarioItemDTO)
  );
};

const itemDetailsRequest = async (itemCode: string) => {
  let res;
  try {
    res = await axiosGet({
      url: `/fusion-gestao-cuidado/api/plano-cuidado/item/${itemCode}`,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    console.error(e);
    return { data: null, error: e };
  }

  return { data: res.data, error: false };
};

const carePointNetworkRequest = async (
  input: ProcedureCarePointsInputModel,
) => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const data: ProcedureNetworkRequest = {
    ...mapInput(input, beneficiaryCode),
    codigoGrupoLivreto: '2',
  };

  let res: { data: any };
  try {
    res = await axiosPost({
      url: '/rede-credenciada/busca-prestador-geolocalizacao',
      data,
      baseUrl: BaseUrl.GAMA_AUTH_URL,
    });
  } catch (e) {
    console.error(e.message);
    return { data: null, error: e };
  }

  if (res.data.length > 0) {
    return { data: res.data, error: null };
  }

  return { data: null, error: false };
};

const mapInput = (
  input: CarePointNetworkInputModel,
  beneficiaryCode: string,
): CarePointNetworkRequest => {
  return {
    codigoOperadora: opCode,
    codigoEstipulante: stimulatingCode,
    codigoBeneficiario: beneficiaryCode,
    latitude: input.latitude.toString().replace('.', ','),
    longitude: input.longitude.toString().replace('.', ','),
    distancia: input.maxDistance.toString(),
  };
};

export interface CarePlanItemDetailsInputModel {
  scheduleCode?: string;
  itemCode?: string;
}
