import { AddressModel } from '@app/model/api/beneficiary-info.model';

export interface CarePointModel {
  id: string;
  name?: string;
  contact?: ContactModel;
  address: AddressModel;
  description?: string;
}

export interface ContactModel {
  phoneNumbers?: string[];
  name?: string;
  email?: string;
  website?: string;
}
