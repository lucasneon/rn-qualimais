import { BaseError, ErrorType } from '@app/modules/base-error';
import {
  CarePointNetworkCityResponse,
  CarePointNetworkDistrictResponse,
  CarePointNetworkResponse,
  CarePointNetworkSpecialityResponse,
  CarePointNetworkStateResponse,
  CarePointNetworkTypeResponse,
  UpdateCarePointFavoriteRequest,
  CarePointNetworkPerPlanResponse,
  CarePointNetworkGamaResponse,
} from './care-point-network.entity';
import {
  CarePointNetworkCityModel,
  CarePointNetworkDistrictModel,
  CarePointNetworkModel,
  CarePointNetworkPerPlanModel,
  CarePointNetworkSpecialityModel,
  CarePointNetworkStateModel,
  CarePointNetworkTypeModel,
  UpdateCarePointFavoriteModel,
} from './care-point-network.model';

export const CarePointNetworkMapper = {
  mapResponse(res: CarePointNetworkResponse): CarePointNetworkModel {
    const [district, cityState] = res.regiao && res.regiao.split(', ');
    const [city, state] = cityState ? cityState.split('-') : ['', ''];
    const [addressType, addressName, addressNumber] =
      res.localizacao && res.localizacao.split(', ');
    const street =
      addressType && addressName ? `${addressType}. ${addressName}` : '';

    return {
      distance: +res.distance,
      practitionerId: res.codigoPrestador,
      isFavorite: res.favorito && res.favorito === 'S',
      carePoint: {
        id: res.codigoPontoAtendimento,
        name: res.prestador,
        contact: {
          phoneNumbers: mapPhones(res),
          website: res.site,
        },
        address: {
          latitude: parseCoordinate(res.latitude),
          longitude: parseCoordinate(res.longitude),
          state,
          city,
          district,
          street,
          addressNumber: addressNumber || '',
        },
      },
    };
  },
  mapGamaResponse(res: CarePointNetworkGamaResponse): CarePointNetworkModel {
    let telefone: string[] = [];
    telefone.push(res.telefone);

    return {
      distance: +res.distancia,
      practitionerId: res.codigoPrestador,
      isFavorite: res.favorito && res.favorito === 'S',
      carePoint: {
        id: res.codigoPontoAtendimento,
        name: res.nomeFantasia,
        contact: {
          phoneNumbers: mapPhonesGama(res.telefone),
          website: res.webSite,
        },
        address: {
          latitude: parseCoordinate(res.latitude),
          longitude: parseCoordinate(res.longitude),
          state: res.estado,
          city: res.cidade,
          district: res.bairro,
          street: res.tipoLogradouro + ' ' + res.logradouro,
          addressNumber: res.numeroLogradouro || '',
        },
      },
    };
  },
  mapCarePointNetworkStates(
    res: CarePointNetworkStateResponse,
  ): CarePointNetworkStateModel {
    return {
      uf: res.sglUf,
      ufName: res.nome,
      total: res.qtd,
    };
  },
  mapCarePointNetworkCities(
    res: CarePointNetworkCityResponse,
  ): CarePointNetworkCityModel {
    return {
      uf: res.siglaUf,
      ufName: res.nomeUF,
      cityCode: res.codigoCidade,
      cityName: res.nomeCidade,
      total: res.quantidade,
    };
  },
  mapCarePointNetworkDistricts(
    res: CarePointNetworkDistrictResponse,
  ): CarePointNetworkDistrictModel {
    return {
      uf: res.siglaUf,
      ufName: res.nomeUF,
      cityCode: res.codigoCidade,
      cityName: res.nomeCidade,
      districtName: res.nomBairro,
      total: res.qtdeBairro,
    };
  },
  mapCarePointNetworkTypes(
    res: CarePointNetworkTypeResponse,
  ): CarePointNetworkTypeModel {
    return {
      uf: res.siglaUf,
      ufName: res.nomeUF,
      cityCode: res.codigoCidade,
      cityName: res.nomeCidade,
      districtName: res.nomBairro,
      typeCode: res.codigoTipoPrestador,
      typeDesc: res.descricaoTipoPrestador,
      bookletCode: res.codigoLivreto,
      bookletDesc: res.descricaoLivreto,
    };
  },
  mapCarePointNetworkSpecialities(
    res: CarePointNetworkSpecialityResponse,
  ): CarePointNetworkSpecialityModel {
    return {
      specialityMainCode: res.codigoEspecialidadePrincipal,
      specialityPublicationCode: res.codigoEspecialidadeDivulgação,
      specialityDesc: res.descricaoEspecialidade,
    };
  },
  mapCarePointFavorite(
    res: UpdateCarePointFavoriteRequest,
  ): UpdateCarePointFavoriteModel {
    return {
      practionerId: res.codigoPrestador,
      carePointId: res.codigoPontoAtendimento,
      isFavorite: res.flagFavorito && res.flagFavorito === 'S',
    };
  },

  mapCarePointNetworkPerPlan(
    res: CarePointNetworkPerPlanResponse,
  ): CarePointNetworkPerPlanModel {
    return {
      comercialName: res.nomeComercial,
      registryNumber: res.numRegistro,
      networkCode: res.rede,
    };
  },
};

function mapPhones(res: CarePointNetworkResponse) {
  const phones: string[] = [];

  if (res.telefone1) {
    phones.push(res.telefone1);
  }

  if (res.telefone2) {
    phones.push(res.telefone2);
  }

  return phones;
}

function mapPhonesGama(phone: string) {
  let phones: string[] = phone.split('/');
  let arrayphones: string[] = [];

  if (phones.length > 0) {
    for (let p of phones) {
      arrayphones.push(p.replace(')', ') '));
    }
  } else {
    arrayphones.push(phone.replace(')', ') '));
  }

  return arrayphones;
}

function parseCoordinate(coord: string): number {
  try {
    if (coord) {
      return +coord.replace(/,/, '.');
    } else {
      return null;
    }
  } catch (e) {
    throw new RepositoryError(null, 'Could not parse latitude and longitude.');
  }
}

export class RepositoryError extends BaseError {
  constructor(message?: string, moreInfo?: string) {
    super(ErrorType.RepositoryError, message);
    this.moreInfo = moreInfo;
  }
}
