import { GamaDateFormatter } from '@app/model/api/common.mapper';
import {
  axiosPut,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { FulflillEventRequest } from '@app/modules/home/api/care-plan/care-plan.entity';
import {
  CarePlanScheduleModel,
  FulfillEventInputModel,
} from '@app/modules/home/api/care-plan/care-plan.model';
import { fetchCarePlanItemDetails } from './exam-detail.service';

export const fetchFulfillExam = async (
  input: FulfillEventInputModel,
): Promise<CarePlanScheduleModel> => {
  await fulfill(input);

  const carePlanItem = await fetchCarePlanItemDetails({
    scheduleCode: input.id,
    itemCode: input.itemCode,
  });

  if (carePlanItem) {
    return carePlanItem.schedule;
  }
  return null;
};

const fulfill = async (input: FulfillEventInputModel): Promise<void> => {
  const request: FulflillEventRequest = {
    codigoAgendamento: input.id,
    codigoItem: input.itemCode,
    dataRealizada:
      input.fulfilledAt && GamaDateFormatter.format(input.fulfilledAt),
  };

  await fufillEventRequest(request);

  return;
};

const fufillEventRequest = async (data: FulflillEventRequest) => {
  let res: { data: any };
  try {
    res = await axiosPut({
      url: '/fusion-gestao-cuidado/api/plano-cuidado/agendamento/realizar',
      data,
      baseUrl: BaseUrl.GAMA_CARE_PLAN_URL,
    });
  } catch (e) {
    console.error(e.message);
    return { data: null, error: e };
  }
  return { data: [], error: false };
};
