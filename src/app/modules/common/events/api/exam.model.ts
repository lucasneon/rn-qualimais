import { CarePlanItemModel } from '@app/modules/home/api/care-plan/care-plan.model';
import {
  CarePointNetworkModel,
  ProcedureCarePointsInputModel,
} from './care-point-network.model';

export interface ExamDetailsInputModel extends ProcedureCarePointsInputModel {
  carePlanScheduleCode: string;
  carePlanItemCode: string;
}

export interface ExamDetailsModel {
  carePlanItem: CarePlanItemModel;
  laboratories?: CarePointNetworkModel[];
}
