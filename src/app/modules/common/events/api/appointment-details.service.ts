import {
  CarePlanItemModel,
  ProcedureTypeModel,
} from '@app/modules/home/api/care-plan/care-plan.model';

import { AppointmentDetailsModel } from '../../api/appointment/appointment.model';
import {
  fetchAppointment,
  fetchAppointmentByCarePlanData,
} from '../../api/appointment/appointment.service';
import { fetchProfessionalAndLocation } from '../../api/professional-schedule/professional-schedule.service';
import { RepositoryError } from './care-point-network.mapper';
import { fetchCarePlanItemDetails } from './exam-detail.service';

export const fetchAppointmentDetails = async (
  input?: any,
): Promise<AppointmentDetailsModel> => {
  const [carePlanItem, details]: [CarePlanItemModel, AppointmentDetailsModel] =
    await Promise.all([
      input.carePlanCodes && fetchCarePlanItemDetails(input.carePlanCodes),
      fetchScheduleDetails(input).catch(() => ({})),
    ]);

  if (carePlanItem) {
    carePlanItem.procedure.type = ProcedureTypeModel.Appointment;
    details.carePlanItem = carePlanItem;
  }

  return details;
};

const fetchScheduleDetails = async (
  input?: any,
): Promise<AppointmentDetailsModel> => {
  if (!input.carePlanCodes && !input.appointmentId) {
    throw new RepositoryError(
      null,
      'Missing "carePlanCodes" or "appointmentId"',
    );
  }

  const appointmentDetails: AppointmentDetailsModel = input.carePlanCodes
    ? await fetchAppointmentByCarePlanData(input.carePlanCodes)
    : await fetchAppointment(input.appointmentId);

  return fetchProfessionalAndLocation(appointmentDetails);
};
