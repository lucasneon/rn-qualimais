import { CarePointModel } from './care-point.model';

export const DefaultMaxDistance = 20; // kilometers

export const DefaultMaxDistanceGama = 100; // kilometers

export interface CarePointNetworkInputModel {
  latitude: number;
  longitude: number;
  maxDistance?: number;
  codigoGrupoLivreto?: string;
}

export interface CarePointNetworkGamaInputModel {
  networkCode: number;
  bookletCode: number;
  specialityDesc: string;
  beneficiaryCode?: number;
  latitude: number;
  longitude: number;
  maxDistance?: number;
  state?: string;
  city?: string;
  district?: string;
}

export interface ProcedureCarePointsInputModel
  extends CarePointNetworkInputModel {
  procedureCode: string;
}

export interface CarePointNetworkModel {
  carePoint: CarePointModel;
  practitionerId: string;
  distance: number;
  isFavorite?: boolean;
}

export interface CarePointNetworkGamaModel {
  codeServicePoint?: string;
  codeProvider: string;
  fantasyName?: string;
  corporateName: string;
  registryCorporate?: string;
  state?: string;
  city?: string;
  district?: string;
  adress?: string;
  cep?: string;
  numberAdress?: string;
  latitude: number;
  longitude: number;
  distancy: number;
  webSite?: any;
  foneNumber?: string;
  isFavorite?: boolean;
}

export interface CarePointNetworkStateInputModel {
  networkCode?: number;
  bookletCode?: number;
  specialityDesc?: string;
}

export interface CarePointNetworkStateModel {
  uf: string;
  ufName: string;
  total?: number;
}

export interface CarePointNetworkCityModel extends CarePointNetworkStateModel {
  cityCode: number;
  cityName: string;
}

export interface CarePointNetworkCityInputModel {
  uf?: string;
  networkCode?: number;
  bookletCode?: number;
  specialityDesc?: string;
}

export interface CarePointNetworkDistrictModel
  extends CarePointNetworkCityModel {
  districtName: string;
}

export interface CarePointNetworkDistrictInputModel
  extends CarePointNetworkCityInputModel {
  cityCode?: number;
  networkCode?: number;
  bookletCode?: number;
  specialityDesc?: string;
  cityDesc?: string;
}

export interface CarePointNetworkTypeModel
  extends CarePointNetworkDistrictModel {
  typeCode: number;
  typeDesc: string;
  typeTotal?: number;
  bookletCode?: number;
  bookletDesc?: string;
}

export interface CarePointNetworkTypeInputModel
  extends CarePointNetworkDistrictInputModel {
  districtName?: string;
  networkCode?: number;
}

export interface CarePointNetworkSpecialityModel {
  specialityMainCode: number;
  specialityPublicationCode: number;
  specialityDesc: string;
  errorCode?: number;
  errorMessage?: string;
}

export interface CarePointNetworkSpecialityInputModel
  extends CarePointNetworkDistrictInputModel {
  districtName: string;
  typeCode: number;
  bookletCode?: number;
  networkCode?: number;
}

// tslint:disable-next-line:max-line-length
export interface CarePointNetworkFilterInputModel
  extends CarePointNetworkSpecialityInputModel,
    CarePointNetworkInputModel {
  specialityPublicationCode: number;
  practitionerName: string;
}

export interface UpdateCarePointFavoriteInputModel {
  practionerId: string;
  carePointId: string;
  flagFavorite: string;
}

export interface UpdateCarePointFavoriteModel {
  practionerId: string;
  carePointId: string;
  isFavorite: boolean;
}

export interface CarePointNetworkPerPlanInputModel {
  planCode: number;
}

export interface CarePointNetworkPerPlanModel {
  comercialName: string;
  registryNumber: string;
  networkCode: number;
}

export interface CarePointNetworkCityModel extends CarePointNetworkStateModel {
  cityCode: number;
  cityName: string;
}

export interface CarePointNetworkGamaFilterInputModel
  extends CarePointNetworkInputModel {
  networkCode: number;
  bookletCode: number;
  specialityDesc: string;
  beneficiaryCode?: string;
  state?: string;
  city?: string;
  district?: string;
}
