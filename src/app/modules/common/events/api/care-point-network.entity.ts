import {
  BaseCarePlanRequest,
  BaseUserRequest,
} from '@app/model/api/common.entity';

export interface CarePointNetworkRequest extends BaseCarePlanRequest {
  latitude: string;
  longitude: string;
  distancia: string;
}

export interface EmergencyCareRequest extends CarePointNetworkRequest {
  codigoGrupoLivreto: string;
}

export interface ProcedureNetworkRequest extends CarePointNetworkRequest {
  codigoProcedimentoPadraoOperadora?: string;
  codigoGrupoLivreto?: string;
}

export interface CarePointNetworkResponse {
  codigoPontoAtendimento: string;
  codigoPrestador: string;
  prestador: string;
  localizacao: string;
  regiao: string;
  latitude: string;
  longitude: string;
  distance: string;
  site?: any;
  telefone1?: string;
  telefone2?: string;
  favorito?: string;
}

export interface CarePointNetworkGamaResponse {
  codigoPontoAtendimento: string;
  codigoPrestador: string;
  nomeFantasia?: string;
  razaoSocial: string;
  cpfCnpj?: string;

  estado?: string;
  cidade?: string;
  bairro?: string;
  tipoLogradouro?: string;
  logradouro?: string;
  numeroLogradouro?: string;
  latitude: string;
  longitude: string;
  distancia: string;
  cep?: string;

  webSite?: string;
  telefone?: string;
  favorito?: string;
}

export interface CarePointNetworkStateRequest extends BaseUserRequest {
  codigoGrupoLivreto?: string;
  descricaoEspec?: string;
}

export interface CarePointNetworkFilterBaseResponse {
  siglaUf: string;
  nomeUF: string;
  codigoCidade: number;
  nomeCidade: string;
}

export interface CarePointNetworkStateResponse {
  sglUf: string;
  nome: string;
  qtd?: number;
}

export interface CarePointNetworkCityRequest
  extends CarePointNetworkStateRequest {
  uf?: string;
  descricaoEspecialidade?: string;
}

export interface CarePointNetworkCityResponse
  extends CarePointNetworkFilterBaseResponse {
  quantidade?: number;
}

export interface CarePointNetworkDistrictRequest
  extends CarePointNetworkCityRequest {
  codigoCidade?: number;
  descricaoCidade?: string;
  descricaoEspecialidade?: string;
}

export interface CarePointNetworkDistrictResponse
  extends CarePointNetworkFilterBaseResponse {
  nomBairro: string;
  qtdeBairro?: number;
}

export interface CarePointNetworkTypeRequest
  extends CarePointNetworkDistrictRequest {
  nomBairro?: string;
  codigoRede?: string;
}

export interface CarePointNetworkTypeResponse
  extends CarePointNetworkFilterBaseResponse {
  nomBairro: string;
  codigoTipoPrestador: number;
  descricaoTipoPrestador: string;
  qtdeTipoPrestador?: number;
  codigoLivreto?: number;
  descricaoLivreto?: string;
}

export interface CarePointNetworkSpecialityRequest
  extends CarePointNetworkDistrictRequest {
  nomBairro: string;
  codigoTipoPrestador: number;
}

export interface CarePointNetworkSpecialityResponse
  extends CarePointNetworkFilterBaseResponse {
  nomBairro: string;
  codigoTipoPrestador: number;
  descricaoTipoPrestador: string;
  codigoEspecialidadePrincipal: number;
  codigoEspecialidadeDivulgação: number;
  descricaoEspecialidade: string;
  codigoErro?: number;
  mensagemErro?: string;
}

export interface EmergencyCareFilterRequest extends BaseUserRequest {
  uf: string;
  codigoCidade: number;
  bairro: string;
  codigoTipoPrestador: number;
  codigoEspecialidadeDivulgacao: number;
  nomePrestador: string;
  latitude: string;
  longitude: string;
  distancia: string;
  codigoBeneficiario: number;
}

export interface UpdateCarePointFavoriteRequest {
  codigoBeneficiario: number | string;
  codigoPrestador: string;
  codigoPontoAtendimento: string;
  flagFavorito: string;
}

export interface CarePointNetworkPerPlanRequest {
  codigoSistema: string;
  codigoOperadora: string;
  codigoPlano: number;
}

export interface CarePointNetworkPerPlanResponse {
  nomeComercial: string;
  numRegistro: string;
  rede: number;
}

export interface EmergencyCareGamaFilterRequest extends BaseUserRequest {
  codigoGrupoLivreto: number;
  latitude: string;
  longitude: string;
  distancia: string;
  descricaoEspecialidade: string;
  codigoBeneficiario?: string;
  uf?: string;
  cidade?: string;
  bairro?: string;
}
