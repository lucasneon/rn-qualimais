import { AddressFragment } from "@app/data/graphql";
import { Address } from "@app/model";

export const AddressMapper = {
  map(addressFrag: AddressFragment): Address {
    const streetName = addressFrag.street || "";
    const streetNumber = addressFrag.addressNumber
      ? ", " + addressFrag.addressNumber
      : "";

    let disctrict = addressFrag.district || "";
    if (disctrict && addressFrag.city) {
      disctrict += " - ";
    }
    let city = addressFrag.city ? addressFrag.city : "";
    if (city && addressFrag.state) {
      city += ", ";
    }

    const state = addressFrag.state ? addressFrag.state : "";

    return {
      streetAndNumber: streetName + streetNumber,
      area: disctrict + city + state,
      zipCode: addressFrag.cep,
    };
  },
};
