import * as React from 'react';
// import { WebView } from 'react-native-webview';
import { Navigation, NavigationPageProps } from '@app/core/navigation';
import { Color } from '@atomic';
import {
  View,
  ActivityIndicator,
  StyleSheet,
  PermissionsAndroid,
  Linking,
} from 'react-native';

import { WebView } from 'react-native-webview';

export interface WebviewPageProps {
  url: string;
}

export class WebViewPage extends React.Component<
  NavigationPageProps<WebviewPageProps>
> {
  static options = {
    name: 'WebviewPage',
    topBar: { background: { color: Color.White }, rightButtons: [] },
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      visible: true,
    };
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }

  hideSpinner() {
    this.setState({ visible: false });
  }

  componentDidMount() {
    this.cameraPermission();
    this.micPermission();
  }

  cameraPermission = async () => {
    let granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: 'Camera Permission',
        message: 'App needs access to your camera ' + 'so others can see you.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the camera');
    } else {
      console.log('Camera permission denied');
    }
  };

  recordPermission = async () => {
    let granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_VIDEO,
      {
        title: 'Record Video Permission',
        message: 'App needs access to your camera ' + 'so others can see you.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the record video');
    } else {
      console.log('record video permission denied');
    }
  };

  micPermission = async () => {
    let granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      {
        title: 'Audio Permission',
        message: 'App needs access to your audio / microphone',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );

    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the Microphone');
    } else {
      console.log('Microphone permission denied');
    }
  };

  render() {
    return (
      <>
        <View style={styles.container}>
          <WebView
            source={{
              uri: this.props.navigation.url,
              headers: {
                'Accept-Language': 'pt-BR',
                'Content-Language': 'pt-BR',
              },
            }}
            onLoad={() => this.hideSpinner()}
            startInLoadingState={true}
            javaScriptEnabled={true}
            useWebKit
            userAgent="Mozilla/5.0 (Linux; An33qdroid 10; Android SDK built for x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.185 Mobile Safari/537.36"
            javaScriptEnabledAndroid={true}
            geolocationEnabled={true}
            mediaPlaybackRequiresUserAction={true}
            allowInlineMediaPlayback={true}
            allowFileAccess={true}
            domStorageEnabled={true}
            onShouldStartLoadWithRequest={this.onShouldStart}
            allowContentAccess={true}
          />

          {this.state.visible && (
            <ActivityIndicator
              color="#009688"
              size="large"
              style={styles.loading}
            />
          )}
        </View>
      </>
    );
  }

  onShouldStart = event => {
    const goToRoom =
      'https://staging.app.sosportal.com.br/sessions/patient/main/HomeCamera';
    if (event !== null && event.url === goToRoom) {
      console.log('Capturou o evento' + event.url);
      this.openUrl(goToRoom);
      return false;
    }
    return true;
  };

  openUrl = (url: string) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  };

  ActivityIndicatorElement = () => {
    return (
      <ActivityIndicator
        color="#009688"
        size="large"
        style={styles.activityIndicatorStyle}
      />
    );
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    paddingTop: 30,
  },
  activityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
  },
});
