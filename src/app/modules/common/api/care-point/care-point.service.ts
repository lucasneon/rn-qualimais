import { LocalDataSource } from '@app/data';
import {
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import Container from 'typedi';
import { UpdateCarePointFavoriteRequest } from '../../events/api/care-point-network.entity';
import { CarePointNetworkMapper } from '../../events/api/care-point-network.mapper';
import { UpdateCarePointFavoriteInputModel } from '../../events/api/care-point-network.model';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const updateCarePointFavorite = async (
  input: UpdateCarePointFavoriteInputModel,
) => {
  const beneficiaryCode: string = await localDataSource.get<string>(
    'beneficiaryCode',
  );
  const req: UpdateCarePointFavoriteRequest = {
    codigoBeneficiario: beneficiaryCode,
    codigoPrestador: input.practionerId,
    codigoPontoAtendimento: input.carePointId,
    flagFavorito: input.flagFavorite,
  };
  const res = await updateCarePointFavoriteRequest(req);
  if (res.error) {
    throw new Error(res.error);
  }
  console.warn('care-point-service: ', res);
  return res.data && CarePointNetworkMapper.mapCarePointFavorite(res.data);
};

const updateCarePointFavoriteRequest = async (
  req: UpdateCarePointFavoriteRequest,
) => {
  let res;
  try {
    res = await axiosPost({
      url: '/prestador-favorito',
      data: req,
      baseUrl: BaseUrl.GAMA_AUTH_URL,
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};
