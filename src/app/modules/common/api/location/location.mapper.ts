import { IdentifierRemote } from '@app/utils/prontlife/prontlife.entity';
import {
  CarePointModel,
  ContactModel,
} from '../../events/api/care-point.model';
import { LocationResponse } from '../schedules/shedules.entity';

const NoData = 'N/A';
const streetIndex = 0;
const numberIndex = 1;
const complementIndex = 2;

export const mapLocation = (locationRes: LocationResponse): CarePointModel => {
  const addressRes = locationRes.address;
  const position = locationRes.position;

  return {
    id: locationRes.id,
    contact: locationRes.telecom && mapContact(locationRes.telecom),
    name: locationRes.name,
    address: addressRes && {
      state: addressRes.state,
      city: addressRes.city,
      district: addressRes.district,
      cep: addressRes.postalCode,
      latitude: position && position.latitude,
      longitude: position && position.longitude,
      ...(addressRes.line && mapMicroAddress(addressRes.line)),
    },
  };
};

function mapContact(telecomRes: IdentifierRemote[]): ContactModel {
  let email: string;
  const phoneNumbers: string[] = [];

  telecomRes.forEach(contactRes => {
    if (contactRes.system === 'email') {
      email = contactRes.value;
    } else if (contactRes.system === 'phone') {
      phoneNumbers.push(contactRes.value);
    }
  });

  return {
    email,
    phoneNumbers: phoneNumbers.length > 0 ? phoneNumbers : undefined,
  };
}

function mapMicroAddress(data: string[]): {
  street: string;
  addressNumber: string;
  complement: string;
} {
  const extractInfo = (index: number): string => {
    return data.length > index ? data[index] : null;
  };
  const checkDataExistance = (dataItem: string): string => {
    return dataItem !== NoData ? dataItem : '';
  };

  const street: string = extractInfo(streetIndex);
  const stringNumber: string = checkDataExistance(extractInfo(numberIndex));
  const complement: string = extractInfo(complementIndex);

  return {
    street: checkDataExistance(street),
    addressNumber: stringNumber && (getOnlyNumbers(stringNumber) || ''),
    complement: checkDataExistance(complement),
  };
}

function getOnlyNumbers(text: string): string {
  return text.replace(/\D/g, '');
}
