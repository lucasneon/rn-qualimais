import { PRONTLIFE_ACCESS_TOKEN } from '@app/config';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { requestProntlifeHeaders } from '@app/utils/prontilife.utils';
import Container from 'typedi';
import { CarePointModel } from '../../events/api/care-point.model';
import { mapLocation } from './location.mapper';

const prontlifeToken: string = Container.get(PRONTLIFE_ACCESS_TOKEN);

export const fetchLocation = async (
  locationId: string,
): Promise<CarePointModel> => {
  const res = await locationRequest(locationId);

  if (res.error || res.data?.resourceType !== 'Location') {
    throw new Error(res.error);
  }

  return mapLocation(res.data);
};

const locationRequest = async (locationId: string) => {
  let res;
  try {
    res = await axiosGet({
      url: '/fhir/Location/' + locationId,
      baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
      headers: requestProntlifeHeaders(prontlifeToken),
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};
