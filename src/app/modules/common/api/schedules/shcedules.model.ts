import { GeneralTypeModel } from '@app/utils/prontlife/common.model';
import { CarePointModel } from '../../events/api/care-point.model';

export interface SchedulesInputModel {
  state: string;
  city: string;
  specialtyCode?: string;
  personalDoctor?: boolean;
}

export interface SpecialtySchedulesModel {
  specialty: GeneralTypeModel;
  schedules: ScheduleModel[];
}

export interface ScheduleModel {
  scheduleId: string;
  professionalId: string;
  professionalCRM?: string;
  professionalName: string;
  carePoint: CarePointModel;
}
