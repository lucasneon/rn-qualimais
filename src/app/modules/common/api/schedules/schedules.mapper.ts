import { UserData } from '@app/modules/authentication/api/login/beneficiary.service';
import { GeneralTypeModel } from '@app/utils/prontlife/common.model';
import {
  CodeRemote,
  CodingRemote,
  IdentifierRemote,
  ReferenceRemote,
} from '@app/utils/prontlife/prontlife.entity';
import {
  CarePointModel,
  ContactModel,
} from '../../events/api/care-point.model';
import { ScheduleModel, SpecialtySchedulesModel } from './shcedules.model';
import {
  FetchSchedulesResponse,
  LocationResponse,
  ProfessionalResponse,
  ScheduleResponse,
  SchedulesResourcesResponse,
} from './shedules.entity';

export const insurerParamMap = (input: Partial<UserData>): string => {
  return `insurer/${input.insurerId || '55879'}/members|${
    input.idHealthCareCard
  }`;
};

export const mapForSingleSpecialty = (
  data: FetchSchedulesResponse,
): SpecialtySchedulesModel => {
  const list: SchedulesResourcesResponse[] = data.entry.map(it => it.resource);
  function filterType<T extends SchedulesResourcesResponse>(
    resourceType: string,
  ): T[] {
    return list.filter<T>((it): it is T => it.resourceType === resourceType);
  }

  const schedulesRes: ScheduleResponse[] =
    filterType<ScheduleResponse>('Schedule');
  const locations: LocationResponse[] =
    filterType<LocationResponse>('Location');
  const professionals: ProfessionalResponse[] =
    filterType<ProfessionalResponse>('Practitioner');
  function mapSchedule(scheduleResponse: ScheduleResponse): ScheduleModel {
    const references = scheduleResponse.actor;
    const professionalId: string = reference(references, 'Practitioner/');
    const locationId: string = reference(references, 'Location/');

    const professionalRes: ProfessionalResponse = professionals.find(
      it => it.id === professionalId,
    );
    const locationRes: LocationResponse = locations.find(
      it => it.id === locationId,
    );
    const infoCRM = professionalRes.identifier.find(it =>
      it.system.includes('CRM'),
    );

    return {
      scheduleId: scheduleResponse.id,
      professionalId,
      professionalName:
        professionalRes.name && professionalRes.name.length > 0
          ? professionalRes.name[0].text
          : null,
      professionalCRM: infoCRM && infoCRM.value,
      carePoint: locationMap(locationRes),
    };
  }

  return {
    specialty: codeValue(schedulesRes[0].specialty, 'specialty/'),
    schedules: schedulesRes.map(mapSchedule),
  };
};

const reference = (references: ReferenceRemote[], prefix: string): string => {
  const ref = references.find(item => item.reference.includes(prefix));
  return ref ? ref.reference.split(prefix)[1] : null;
};

const codeValue = (code: CodeRemote[], system: string): GeneralTypeModel => {
  let coding: CodingRemote;

  // code.forEach(item => {
  //   if (!coding) {
  //     coding = item.coding.find(codingItem => codingItem.system.includes(system));
  //   }
  // });

  return coding ? { id: coding.code, description: coding.display } : null;
};

const locationMap = (locationRes: LocationResponse): CarePointModel => {
  const addressRes = locationRes.address;
  const position = locationRes.position;

  return {
    id: locationRes.id,
    contact: locationRes.telecom && mapContact(locationRes.telecom),
    name: locationRes.name,
    address: addressRes && {
      state: addressRes.state,
      city: addressRes.city,
      district: addressRes.district,
      cep: addressRes.postalCode,
      latitude: position && position.latitude,
      longitude: position && position.longitude,
      ...(addressRes.line && mapMicroAddress(addressRes.line)),
    },
  };
};

function mapContact(telecomRes: IdentifierRemote[]): ContactModel {
  let email: string;
  const phoneNumbers: string[] = [];

  telecomRes.forEach(contactRes => {
    if (contactRes.system === 'email') {
      email = contactRes.value;
    } else if (contactRes.system === 'phone') {
      phoneNumbers.push(contactRes.value);
    }
  });

  return {
    email,
    phoneNumbers: phoneNumbers.length > 0 ? phoneNumbers : undefined,
  };
}

const NoData = 'N/A';
const streetIndex = 0;
const numberIndex = 1;
const complementIndex = 2;

function mapMicroAddress(data: string[]): {
  street: string;
  addressNumber: string;
  complement: string;
} {
  const extractInfo = (index: number): string =>
    data.length > index ? data[index] : null;
  const checkDataExistance = (dataItem: string): string =>
    dataItem !== NoData ? dataItem : '';

  const street: string = extractInfo(streetIndex);
  const stringNumber: string = checkDataExistance(extractInfo(numberIndex));
  const complement: string = extractInfo(complementIndex);

  return {
    street: checkDataExistance(street),
    addressNumber: stringNumber && (getOnlyNumbers(stringNumber) || ''),
    complement: checkDataExistance(complement),
  };
}

function getOnlyNumbers(text: string): string {
  return text.replace(/\D/g, '');
}
