import { PRONTLIFE_ACCESS_TOKEN } from '@app/config';
import { LocalDataSource } from '@app/data';
import { InputParams } from '@app/model/api/input.params.model';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import {
  ProntlifeUtils,
  requestProntlifeHeaders,
} from '@app/utils/prontilife.utils';
import { ProntlifeRequestParams } from '@app/utils/prontlife/prontlife.entity';
import { Container } from 'typedi';
import { mapForSingleSpecialty } from './schedules.mapper';
import { SchedulesInputModel } from './shcedules.model';

const prontlifeToken = Container.get(PRONTLIFE_ACCESS_TOKEN);
const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchSchedulesForSingleSpecialty = async (
  input: InputParams<SchedulesInputModel>,
) => {
  const idHealthCareCard: string = await localDataSource.get<string>(
    'idHealthCareCard',
  );
  // TODO ver especialidade
  const params: ProntlifeRequestParams = {
    'actor:Location.state': input.state,
    'actor:Location.city': input.city,
    'specialty.code': '315047', //input.specialtyCode,
    'Coverage.identifier': ProntlifeUtils.FormatterPatient.insurerParam({
      ...input,
      idHealthCareCard: idHealthCareCard,
      insurerId: input.insurerId || '1',
    }), // verificar para remover
    include: true, // Adds Location and Professional to the Schedules response list
  };
  let res;
  console.warn('Schedules Parans', params);
  try {
    res = await scheduleRequest(params);
  } catch (error) {
    return { data: null, error };
  }

  console.warn('Schedule-result ', res.data);

  if (!res.data || !res.data.entry || res.data.entry.length === 0) {
    return {
      specialty: { id: input.specialtyCode, description: '' },
      schedules: [],
    };
  }

  return mapForSingleSpecialty(res.data);
};

const scheduleRequest = async (params: ProntlifeRequestParams) => {
  let res;
  try {
    res = await axiosGet({
      url: `/fhir/Schedule?Coverage.Identifier=${encodeURIComponent(
        params['Coverage.identifier'],
      )}&actor:Location.city=${encodeURIComponent(
        params['actor:Location.city'],
      )}&actor:Location.state=${encodeURIComponent(
        params['actor:Location.state'],
      )}&include=${encodeURIComponent(
        params.include,
      )}&specialty.code=${encodeURIComponent(
        params['specialty.code'],
      )}&_query=${encodeURIComponent('ProntLife')}`,
      baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
      // params,
      headers: requestProntlifeHeaders(prontlifeToken),
    });
  } catch (e) {
    throw new Error(e);
    // return { data: null, error: e };
  }
  return res;
};
