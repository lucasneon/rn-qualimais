import {
  AddressResponse,
  CodeRemote,
  IdentifierRemote,
  ListResponse,
  NameRemote,
  PositionResponse,
  ReferenceRemote,
  ResourceResponse,
} from '@app/utils/prontlife/prontlife.entity';

export interface LocationResponse extends ResourceResponse {
  status: string;
  name: string;
  telecom: IdentifierRemote[];
  address: AddressResponse;
  position: PositionResponse;
}

export interface ProfessionalResponse extends ResourceResponse {
  identifier: IdentifierRemote[];
  active: boolean;
  name: NameRemote[];
  telecom: IdentifierRemote[];
  gender: string;
}

export type SchedulesResourcesResponse =
  | ScheduleResponse
  | LocationResponse
  | ProfessionalResponse;

export type FetchSchedulesResponse = ListResponse<SchedulesResourcesResponse>;

export interface ScheduleResponse extends ResourceResponse {
  active: boolean;
  specialty: CodeRemote[];
  actor: ReferenceRemote[];
}