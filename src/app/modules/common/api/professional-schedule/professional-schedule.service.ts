import { PRONTLIFE_ACCESS_TOKEN } from '@app/config';
import { LocalDataSource } from '@app/data';
import { CarePlanCodes } from '@app/data/graphql';
import { InputParams } from '@app/model/api/input.params.model';
import {
  axiosGet,
  axiosPost,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import {
  CarePlanItemModel,
  ProcedureTypeModel,
} from '@app/modules/home/api/care-plan/care-plan.model';
import {
  ProntlifeUtils,
  requestProntlifeHeaders,
} from '@app/utils/prontilife.utils';
import { ProntlifeRequestParams } from '@app/utils/prontlife/prontlife.entity';
import Container from 'typedi';
import { CarePointModel } from '../../events/api/care-point.model';
import { fetchCarePlanItemDetails } from '../../events/api/exam-detail.service';
import { AppointmentMapper } from '../appointment/appointment.mapper';
import {
  AppointmentDetailsInputModel,
  AppointmentDetailsModel,
  AppointmentInputModel,
  ProfessionalModel,
} from '../appointment/appointment.model';
import {
  fetchAppointment,
  fetchAppointmentByCarePlanData,
} from '../appointment/appointment.service';
import { fetchLocation } from '../location/location.service';
import { execProfessional } from '../professional/professional.service';
import { mapToProfessionalSchedule } from './professional-schedule.mapper';
import {
  AppointmentRequest,
  ProfessionalScheduleInputModel,
} from './professional-schedule.model';

const localDataSource: LocalDataSource = Container.get(LocalDataSource);

export const fetchBySchedule = async (
  input: InputParams<ProfessionalScheduleInputModel>,
) => {
  const idHealthCareCard: string = await localDataSource.get<string>(
    'idHealthCareCard',
  );
  const reqParams: ProntlifeRequestParams = {
    'actor:Location.state': input.state,
    'actor:Location.city': input.city,
    start: ProntlifeUtils.Formatter.date(input.from),
    end: ProntlifeUtils.Formatter.date(input.until),
    Schedule: input.scheduleId,
    _query: 'ProntLife',
    'Coverage.identifier': ProntlifeUtils.Formatter.insurerParam({
      ...input,
      idHealthCareCard,
      insurerId: input.insurerId || '55879',
    }),
  };

  const res = await slotRequest(reqParams);
  if (res.error) {
    throw new Error(res.error);
  }
  if (!res.data?.total || res.data.total === 0) {
    return { scheduleId: input.scheduleId, daySlots: [] };
  }

  try {
    return mapToProfessionalSchedule(input.from, res.data);
  } catch (error) {
    throw new Error('Error mapping professional schedule');
  }
};

async function slotRequest(params: ProntlifeRequestParams) {
  let res;
  const prontlifeToken: string = Container.get(PRONTLIFE_ACCESS_TOKEN);
  try {
    res = await axiosGet({
      url: '/fhir/Slot',
      baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
      params,
      headers: requestProntlifeHeaders(prontlifeToken),
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
}

export const execSchedulePost = async (input: AppointmentInputModel) => {
  const appointmentId: string = await scheduleAppointment(input);
  const carePlanCodes: CarePlanCodes = input.carePlan;
  const detailsInput: AppointmentDetailsInputModel = carePlanCodes
    ? { carePlanCodes }
    : { appointmentId };
  return execPost(detailsInput);
};

const execPost = async (input?: AppointmentDetailsInputModel) => {
  const [carePlanItem, details]: [
    { data: CarePlanItemModel; error: any },
    AppointmentDetailsModel,
  ] = await Promise.all([
    input.carePlanCodes && fetchCarePlanItemDetails(input.carePlanCodes),
    fetchScheduleDetails(input).catch(() => ({})),
  ]);
  if (carePlanItem?.data) {
    carePlanItem.data.procedure.type = ProcedureTypeModel.Appointment;
    details.carePlanItem = carePlanItem?.data;
  }

  return details;
};

export const scheduleAppointment = async (params: AppointmentInputModel) => {
  const idHealthCareCard: string = await localDataSource.get<string>(
    'idHealthCareCard',
  );
  const data = AppointmentMapper.mapRequest(params, idHealthCareCard);
  const res = await appointmentPost(data);
  if (res.status !== 201) {
    throw new Error('Schedule POST status was not 201');
  }

  if (!res.headers.location) {
    throw new Error('Could not get "appointmentId" from POST response');
  }

  const history = res.headers.location.split('Appointment/')[1];
  const appointmentId = history.split('/')[0];
  return appointmentId;
};

const appointmentPost = async (data: AppointmentRequest) => {
  let res;
  const prontlifeToken: string = Container.get(PRONTLIFE_ACCESS_TOKEN);
  try {
    res = await axiosPost(
      {
        url: '/fhir/Appointment',
        baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
        data,
      },
      {
        ...requestProntlifeHeaders(prontlifeToken),
        'options-email-notification': true,
      },
    );
  } catch (error) {
    return { data: null, error };
  }
  return res;
};

const fetchScheduleDetails = async (
  input?: AppointmentDetailsInputModel,
): Promise<AppointmentDetailsModel> => {
  if (!input.carePlanCodes && !input.appointmentId) {
    throw new Error('Missing "carePlanCodes" or "appointmentId"');
  }

  const appointmentDetails: AppointmentDetailsModel = input.carePlanCodes
    ? await fetchAppointmentByCarePlanData(input.carePlanCodes)
    : await fetchAppointment(input.appointmentId);

  return fetchProfessionalAndLocation(appointmentDetails);
};

export const fetchProfessionalAndLocation = async (
  appointment: AppointmentDetailsModel,
): Promise<AppointmentDetailsModel> => {
  const [professional, location]: [ProfessionalModel, CarePointModel] =
    await Promise.all([
      execProfessional(appointment.doctor.id),
      fetchLocation(appointment.location.id),
    ]);
  professional.specialty = {
    ...professional.specialty,
    description: appointment.doctor.specialty
      ? appointment.doctor.specialty.description
      : null,
  };
  appointment.doctor = professional;
  appointment.location = location;

  return appointment;
};
