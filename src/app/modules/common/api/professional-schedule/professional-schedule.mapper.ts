import { ProfessionalScheduleFragment } from '@app/data/graphql';
import { ProntlifeUtils } from '@app/utils/prontilife.utils';
import {
  EntryResponse,
  ListResponse,
} from '@app/utils/prontlife/prontlife.entity';
import moment from 'moment';
import {
  DaySlotModel,
  ProfessionalScheduleModel,
  SlotResponse,
  SlotsByDay,
} from './professional-schedule.model';

export const mapToProfessionalSchedule = (
  from: Date,
  res: ListResponse<SlotResponse>,
): ProfessionalScheduleFragment => {
  const slotsByDay: SlotsByDay = daysUntilEndOfMonthAsKeys(from);

  const pushSlots = (entryRes: EntryResponse<SlotResponse>): void => {
    const start = entryRes.resource.start;
    const startDay = ProntlifeUtils.Formatter.date(start);

    if (slotsByDay[startDay]) {
      slotsByDay[startDay].push({
        start: new Date(start),
        end: new Date(entryRes.resource.end),
      });
    }
  };
  res.entry.forEach(pushSlots);

  const mapToModel = (day: string) => ({
    day: new Date(day),
    slots: slotsByDay[day],
  });
  const daySlots: DaySlotModel[] = Object.keys(slotsByDay).map(mapToModel);

  return {
    scheduleId: ProntlifeUtils.Extractor.reference(
      [res.entry[0].resource.schedule],
      'Schedule/',
    ),
    daySlots,
  };
};

function daysUntilEndOfMonthAsKeys(from: Date): SlotsByDay {
  const slotsByDay = {};
  let day = moment.utc(from);
  const month: number = day.month();

  while (day.month() === month) {
    slotsByDay[day.format('YYYY-MM-DD')] = [];
    day = day.add(1, 'day');
  }

  return slotsByDay;
}
