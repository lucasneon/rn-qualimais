import {
  CodingRemote,
  IdentifierRemote,
  ReferenceRemote,
  ResourceResponse,
  TypeRemote,
} from '@app/utils/prontlife/prontlife.entity';

export interface SlotResponse extends ResourceResponse {
  schedule: ReferenceRemote;
  start: string;
  end: string;
}

export interface ProfessionalScheduleModel {
  scheduleId: string;
  daySlots: DaySlotModel[];
}

export interface SlotModel {
  start: Date;
  end?: Date;
}

export interface DaySlotModel {
  day: Date;
  slots: SlotModel[];
}

export interface SlotsByDay {
  [day: string]: SlotModel[];
}

export interface ProfessionalScheduleModel {
  scheduleId: string;
  daySlots: DaySlotModel[];
}

export interface ProfessionalScheduleInputModel {
  scheduleId: string;
  from: Date;
  until: Date;
  state: string;
  city: string;
}

export interface ActorRemote {
  reference?: string;
  identifier?: IdentifierRemote;
}

interface BaseAppointmentRemote {
  resourceType: string;
  status?: string;
  start: string;
  end: string;
  participant: ParticipantRemote[]
}

export interface ParticipantRemote {
  type?: TypeRemote[];
  actor: ActorRemote;
  required: string;
  status: string;
}

export interface AppointmentRequest extends BaseAppointmentRemote {
  identifiers?: IdentifierRemote[];
  appointmentType: AppointmentTypeRequest;
}

export interface AppointmentTypeRequest {
  coding: CodingRemote;
}
