import { GeneralTypeModel } from '@app/utils/prontlife/common.model';
import {
  CodeRemote,
  ReferenceRemote,
  ResourceResponse,
} from '@app/utils/prontlife/prontlife.entity';

export interface ProfessionalModel {
  id?: string;
  name: string;
  email?: string;
  cpf?: string;
  specialty: GeneralTypeModel;
  crm?: string;
  bioText?: string;
  imageUrl?: string;
}

export interface ProfessionalRoleResponse extends ResourceResponse {
  active: boolean;
  practitioner: ReferenceRemote;
  specialty: CodeRemote[];
  location: ReferenceRemote[];
}
