import { PRONTLIFE_ACCESS_TOKEN } from '@app/config';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import {
  ProntlifeUtils,
  requestProntlifeHeaders,
} from '@app/utils/prontilife.utils';
import { GeneralTypeModel } from '@app/utils/prontlife/common.model';
import Container from 'typedi';
import { ProfessionalModel } from '../appointment/appointment.model';

const prontlifeToken: string = Container.get(PRONTLIFE_ACCESS_TOKEN);

export const execProfessional = async (
  professionalId: string,
): Promise<ProfessionalModel> => {
  const [professional, specialty]: [ProfessionalModel, GeneralTypeModel] =
    await Promise.all([
      fetchProfessional(professionalId),
      fetchProfessionalRole(professionalId),
    ]);

  professional.specialty = specialty;

  return professional;
};

const fetchProfessional = async (
  professionalId: string,
): Promise<ProfessionalModel> => {
  const res = await professionalRequest(professionalId);
  if (res.error) {
    throw new Error(res.error);
  }
  const professional = res.data;

  if (professional?.resourceType !== 'Practitioner') {
    throw new Error(res.error);
  }

  return {
    id: professional.id,
    name: professional.name.find(item => item.use === 'official').text,
    email: ProntlifeUtils.Extractor.identifiertValue(
      professional.telecom,
      'email',
    ),
    cpf: ProntlifeUtils.Extractor.identifiertValue(
      professional.identifier,
      'CPF',
    ),
    specialty: null,
    crm: ProntlifeUtils.Extractor.identifiertValue(
      professional.identifier,
      'CRM',
    ),
    bioText: undefined, // don't know how this returns yet
    imageUrl: undefined, // don't know how this returns yet
  };
};

const fetchProfessionalRole = async (
  professionalId,
): Promise<GeneralTypeModel> => {
  const res = await professionalRoleRequest(professionalId);

  if (res.error || res.data?.resourceType !== 'PractitionerRole') {
    throw new Error(res.error);
  }

  return ProntlifeUtils.Extractor.codeValue(); //(res.data.specialty, 'specialty');
};

const professionalRequest = async (professionalId: string) => {
  let res;
  try {
    res = await axiosGet({
      url: '/fhir/Practitioner/' + professionalId,
      baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
      headers: requestProntlifeHeaders(prontlifeToken),
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};

const professionalRoleRequest = async (professionalId: string) => {
  let res;
  try {
    res = await axiosGet({
      url: '/fhir/PractitionerRole/' + professionalId,
      baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
      headers: requestProntlifeHeaders(prontlifeToken),
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};
