import { PRONTLIFE_ACCESS_TOKEN } from '@app/config';
import {
  axiosGet,
  BaseUrl,
} from '@app/modules/authentication/api/login/http-requests';
import { requestProntlifeHeaders } from '@app/utils/prontilife.utils';
import { ProntlifeRequestParams } from '@app/utils/prontlife/prontlife.entity';
import Container from 'typedi';
import { AppointmentMapper } from './appointment.mapper';
import {
  AppointmentDetailsModel,
  CarePlanCodesModel,
} from './appointment.model';

const prontlifeToken: string = Container.get(PRONTLIFE_ACCESS_TOKEN);

export const fetchAppointmentByCarePlanData = async (
  input: CarePlanCodesModel,
): Promise<AppointmentDetailsModel> => {
  const { itemCode, scheduleCode } = input;
  const identifier: string = `fusion/item/${itemCode}/evento|${scheduleCode}`;
  const res = await listRequest({ identifier });

  if (!res.data || res.data.total === 0) {
    return null;
  }

  return AppointmentMapper.mapResponse(res.data.entry[0].resource);
};

const listRequest = async (params: ProntlifeRequestParams) => {
  let res;
  try {
    res = await axiosGet({
      url: '/fhir/Appointment/',
      baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
      params,
      headers: requestProntlifeHeaders(prontlifeToken),
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};

export const fetchAppointment = async (
  appointmentId: string,
): Promise<AppointmentDetailsModel> => {
  const res = await appointmentRequest(appointmentId);

  try {
    return AppointmentMapper.mapResponse(res.data);
  } catch (e) {
    throw new Error('Could not map schedule POST: ' + e);
  }
};

const appointmentRequest = async (appointmentId: string) => {
  let res;
  try {
    res = await axiosGet({
      url: '/fhir/Appointment/' + appointmentId,
      baseUrl: BaseUrl.PRONTLIFE_BASE_URL,
      headers: requestProntlifeHeaders(prontlifeToken),
    });
  } catch (error) {
    return { data: null, error };
  }
  return res;
};
