import { IdentifierRemote } from '@app/utils/prontlife/prontlife.entity';
import {
  AppointmentRequest,
  ParticipantRemote,
} from '../professional-schedule/professional-schedule.model';
import {
  AppointmentDetailsModel,
  AppointmentInputModel,
  AppointmentResponse,
  AppointmentRoleStatusModel,
  CarePlanCodesModel,
} from './appointment.model';

export const AppointmentRoleStatusMap: {
  [key in AppointmentRoleStatusModel]: string;
} = {
  [AppointmentRoleStatusModel.Accept]: 'accepted',
  [AppointmentRoleStatusModel.Decline]: 'declined',
  [AppointmentRoleStatusModel.NeedsAction]: 'needs-action',
};
const AppointmentType = '6';
const insurerId = '';
// const idHealthCareCard = '';

export const AppointmentMapper = {
  mapRequest(
    input: AppointmentInputModel,
    idHealthCareCard: string,
  ): AppointmentRequest {
    return {
      resourceType: 'Appointment',
      identifiers: input.carePlan && [mapFusionIdentifier(input.carePlan)],
      start: input.start,
      end: input.end,
      appointmentType: { coding: { code: AppointmentType } },
      participant: [
        {
          actor: { reference: 'Practitioner/' + input.professionalId },
          required: 'required',
          status:
            AppointmentRoleStatusMap[AppointmentRoleStatusModel.NeedsAction],
        },
        {
          actor: { reference: 'Location/' + input.locationId },
          required: 'required',
          status:
            AppointmentRoleStatusMap[AppointmentRoleStatusModel.NeedsAction],
        },
        {
          actor: {
            identifier: {
              type: { text: 'Coverage' },
              system: `insurer/${insurerId || '55879'}/members/`,
              value: idHealthCareCard,
            },
          },
          required: 'required',
          status: AppointmentRoleStatusMap[AppointmentRoleStatusModel.Accept],
        },
      ],
    };
  },
  mapResponse(res: AppointmentResponse): AppointmentDetailsModel {
    const findActorData = (actorText: string): string => {
      const part: ParticipantRemote = res.participant.find(it =>
        it.type.some(t => t.text.includes(actorText)),
      );
      return part.actor.reference.split('/')[1];
    };

    const professionalId = findActorData('Practitioner');
    const locationId = findActorData('Location');

    let especialidade = null;
    if (
      res.specialty &&
      res.specialty[0] &&
      res.specialty[0].coding &&
      res.specialty[0].coding[0] &&
      res.specialty[0].coding[0].display
    ) {
      especialidade = res.specialty[0].coding[0].display;
    }

    return {
      appointmentId: res.id,
      doctor: {
        id: professionalId,
        name: null,
        specialty: {
          id: null,
          description: especialidade,
        },
      },
      location: { id: locationId, address: null },
      carePlanItem: null,
    };
  },
};

function mapFusionIdentifier(
  carePlanCodes: CarePlanCodesModel,
): IdentifierRemote {
  return {
    type: { text: 'Fusion' },
    system: `fusion/plano-cuidado/${carePlanCodes.carePlanCode}/item/${carePlanCodes.itemCode}/evento/`,
    value: carePlanCodes.scheduleCode,
  };
}
