import { CarePlanItemModel } from '@app/modules/home/api/care-plan/care-plan.model';
import { GeneralTypeModel } from '@app/utils/prontlife/common.model';
import { CodingRemote } from '@app/utils/prontlife/prontlife.entity';
import { CarePointModel } from '../../events/api/care-point.model';
import { ParticipantRemote } from '../professional-schedule/professional-schedule.model';

export interface AppointmentInputModel {
  start: string;
  end: string;
  professionalId: string;
  locationId: string;
  carePlan?: CarePlanCodesModel | CarePlanItemModel;
}

interface BaseAppointmentRemote {
  resourceType: string;
  status?: string;
  start: string;
  end: string;
  participant: ParticipantRemote[];
}

export interface CarePlanCodesModel {
  carePlanCode?: string;
  itemCode?: string;
  scheduleCode?: string;
}

export enum AppointmentRoleStatusModel {
  Accept = 'Accept',
  Decline = 'Decline',
  NeedsAction = 'NeedsAction',
}

export interface AppointmentDetailsInputModel {
  carePlanCodes?: CarePlanCodesModel;
  appointmentId?: string;
}

export interface AppointmentDetailsModel {
  //[x: string]: { data: CarePlanItemModel; error: any; };
  details?: { data: CarePlanItemModel; error: any };
  carePlanItem?: CarePlanItemModel;
  appointmentId?: string;
  doctor?: ProfessionalModel;
  location?: CarePointModel;
}

export interface ProfessionalModel {
  id?: string;
  name: string;
  email?: string;
  cpf?: string;
  specialty: GeneralTypeModel;
  crm?: string;
  bioText?: string;
  imageUrl?: string;
}

export interface AppointmentTypeResponse {
  coding: CodingRemote[];
}

export interface AppointmentResponse extends BaseAppointmentRemote {
  id: string;
  serviceType: AppointmentTypeResponse[];
  appointmentType: AppointmentTypeResponse;
  specialty: any;
}
