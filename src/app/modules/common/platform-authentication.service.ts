import TouchID, { AuthenticateConfig } from 'react-native-touch-id';
import { Container, Service } from 'typedi';
import { ShouldCheckTouchIdKey } from '@app/config/tokens.config';
import { BeneficiaryProvider, BeneficiaryProviderToken } from '@app/services/providers';
import { Color } from '@atomic';
import { GeneralStrings } from './common.strings';

@Service()
export class PlatformAuthenticationService {
  private readonly shouldCheck: boolean = Container.get(ShouldCheckTouchIdKey);
  private readonly beneficiaryProvider: BeneficiaryProvider = Container.get(BeneficiaryProviderToken);
  private authPromise: Promise<boolean>;

  async checkAuthentication(): Promise<boolean> {
    if (!this.shouldCheck) {
      return true;
    }

    const shouldCheckForSystemAuth: boolean = !!(await this.beneficiaryProvider.getActiveBeneficiaryToken());

    if (!shouldCheckForSystemAuth) {
      return true;
    }

    if (!this.authPromise) {
      this.authPromise = this.askForSystemAuth();
    }

    return this.authPromise;
  }

  private async askForSystemAuth(): Promise<boolean> {
    const options: AuthenticateConfig = {
      passcodeFallback: true,
      imageErrorColor: Color.Alert,
      unifiedErrors: true,
      fallbackLabel: GeneralStrings.SystemAuth.Fallback,
      imageColor: Color.Black,
      title: GeneralStrings.SystemAuth.Title,
      sensorDescription: GeneralStrings.SystemAuth.Description,
      sensorErrorDescription: GeneralStrings.SystemAuth.Fail,
      cancelText: GeneralStrings.Cancel,
    };

    return TouchID.authenticate(GeneralStrings.SystemAuth.Need, options);
  }
}
