import * as React from 'react';
import { Subscribe } from 'unstated';
import { MeasureList } from '@app/components/mol.measure-list';
import {
  VitalSignFragment,
  VitalSignType,
} from '@app/data/graphql/graphql-schema';
import { VitalSignProvider } from '@app/services/providers';
import { LoadingState, Scroll, Shimmer, ShimmerSeparator, VBox } from '@atomic';
import { VitalSignsMapper } from './vital-signs.mapper';

export interface VitalSignsProps {
  onVitalSignTap?: (vitalSign: VitalSignType) => void;
  disabled?: boolean;
}

export const VitalSignShimmer = () => (
  <VBox>
    <Scroll contentContainerStyle={{ flexDirection: 'row' }}>
      <Shimmer rounded={true} height={130} width={150} />
      <ShimmerSeparator />
      <Shimmer rounded={true} height={130} width={150} />
      <ShimmerSeparator />
      <Shimmer rounded={true} height={130} width={150} />
    </Scroll>
  </VBox>
);

export class VitalSigns extends React.Component<VitalSignsProps> {
  mapToCellProps = (vitalSign: VitalSignFragment) => {
    const cell = VitalSignsMapper.mapToCellProps(vitalSign);
    if (cell && this.props.onVitalSignTap) {
      cell.onTap = () => this.props.onVitalSignTap(vitalSign.type);
    }

    return cell;
  };

  render() {
    return (
      <Subscribe to={[VitalSignProvider]}>
        {(vitalSignProvider: VitalSignProvider) => {
          const data = vitalSignProvider.state.recentVitalSigns;
          const placeholderData = Object.keys(VitalSignType)
            .map(type => ({ type } as VitalSignFragment))
            .map(this.mapToCellProps)
            .filter(it => it.label);
          const cellProps =
            data && data.map(this.mapToCellProps).filter(it => !!it);
          return (
            <LoadingState
              loading={vitalSignProvider.state.loading}
              data={!!data}
              error={!!vitalSignProvider.state.error}>
              <LoadingState.Shimmer>
                <VitalSignShimmer />
              </LoadingState.Shimmer>
              <MeasureList>
                {(cellProps || placeholderData).map(it => (
                  <MeasureList.Item
                    disabled={this.props.disabled}
                    key={it.label}
                    {...it}
                  />
                ))}
              </MeasureList>
            </LoadingState>
          );
        }}
      </Subscribe>
    );
  }
}
