import moment from 'moment';
import { WatermarkType } from '@app/components/atm.watermark';
import { MeasureCellProps } from '@app/components/mol.measure-list';
import { VitalSignFragment, VitalSignType } from '@app/data/graphql';
import { VitalSignsStrings } from '@app/modules/health/vital-signs/vital-signs.strings';
import { VitalSignsMap } from '@app/modules/health/api/vital-signs/vital-sign.mapper';
import { ListResponse, MeasureRemote } from '@app/utils/prontlife/prontlife.entity';
import {
  SingleVitalSignModel,
  VitalSignMeasureModel,
  VitalSignsComponentResponse,
  VitalSignsResponse,
  VitalSignTypeModel,
} from '@app/modules/health/api/vital-signs/vital-sign.model';
import { MeasureModel } from '@app/utils/prontlife/common.model';

export const VitalSignsLabelMap: { [key in VitalSignType]?: string } = {
  [VitalSignType.Imc]: 'IMC',
  [VitalSignType.Weight]: 'Peso',
  [VitalSignType.Height]: 'Altura',
  [VitalSignType.BloodPressure]: 'Pressão arterial',
  [VitalSignType.pressao_arterial]: 'Pressão arterial',
  [VitalSignType.Glycemia]: 'Glicemia',
  [VitalSignType.htg]: 'Glicemia',
  [VitalSignType.hgt]: 'Glicemia',
};

const VitalSignsWatermarkMap: { [key in VitalSignType]?: WatermarkType } = {
  [VitalSignType.Imc]: WatermarkType.Weight,
  [VitalSignType.Weight]: WatermarkType.Weight,
  [VitalSignType.Height]: WatermarkType.Body,
  [VitalSignType.BloodPressure]: WatermarkType.Health,
  [VitalSignType.Glycemia]: WatermarkType.Consultation,
};

export const VitalSignsMapper = {
  mapToCellProps(vitalSign: VitalSignFragment): MeasureCellProps {
    const type: VitalSignType = vitalSign.type;
    let [value, unit]: [string, string] = [null, null];
    let cellProps: MeasureCellProps;
    try {
      [value, unit] = mapValueUnit(vitalSign);
      cellProps = {
        label: VitalSignsLabelMap[type],
        value: value || '——',
        unit,
        type: VitalSignsWatermarkMap[type],
        caption: vitalSign.measuredAt
          ? moment(vitalSign.measuredAt).fromNow()
          : VitalSignsStrings.NoRegister.Caption,
      };
    } catch (e) {
      cellProps = null;
    }
    return cellProps;
  },

  mapResponse(data: ListResponse<VitalSignsResponse>): VitalSignMeasureModel[] {
    const mapSubMeasures = (
      componentRes: VitalSignsComponentResponse[],
      measuredAt: Date,
    ): SingleVitalSignModel[] => {
      return componentRes.map(component => ({
        type: getVitalSignType(component.code.text),
        measure: mapMeasure(component.valueQuantity),
        measuredAt,
      }));
    };

    const mapMeasure = (measureRes: MeasureRemote): MeasureModel => {
      return {
        amount: measureRes.value,
        unit: measureRes.unit,
      };
    };

    const getVitalSignType = (value: string): VitalSignTypeModel => {
      const key: string = Object.keys(VitalSignsMap).find(
        it => VitalSignsMap[it] === value,
      );
      return VitalSignTypeModel[key];
    };

    const getMeasureUnit = (vitalSignRes: VitalSignsResponse): string => {
      return vitalSignRes.valueQuantity
        ? vitalSignRes.valueQuantity.unit
        : vitalSignRes.component[0].valueQuantity.unit;
    };

    return data.entry.map(e => {
      const resource = e.resource;
      const measuredAt = new Date(resource.effectiveDateTime);

      return {
        measureUnit: getMeasureUnit(resource),
        type: getVitalSignType(resource.code.text),
        measuredAt,
        measure: resource.valueQuantity
          ? mapMeasure(resource.valueQuantity)
          : null,
        subMeasures: resource.component
          ? mapSubMeasures(resource.component, measuredAt)
          : null,
      };
    });
  },
};

function mapValueUnit(vitalSign: VitalSignFragment): [string, string] {
  const isPressure = vitalSign.type === VitalSignType.BloodPressure;

  let value: string;
  let unit: string;

  if (isPressure && vitalSign.subMeasures) {
    const diastolic = vitalSign.subMeasures.find(
      it => it.type === VitalSignType.DiastolicPressure,
    );
    const systolic = vitalSign.subMeasures.find(
      it => it.type === VitalSignType.SystolicPressure,
    );

    value = systolic.measure.amount + '/' + diastolic.measure.amount;
    unit = systolic.measure.unit;
  } else {
    value = vitalSign.measure ? vitalSign.measure.amount.toString() : null;
    unit = vitalSign.measure ? vitalSign.measure.unit : null;
  }

  return [value, unit];
}
