import { Navigation } from '@app/core/navigation';
import {
  AppointmentSolicitationPage,
  ProfessionalSchedulePage,
  SchedulesPage,
  SpecialtiesPage,
} from './appointment';
import {
  AppointmentDetailPage,
  ExamDetailPage,
  LaboratoriesPage,
} from './events';
import { WebViewPage } from './webview.page';
import { WebHtmlPage } from './webhtml.page';
import { WebViewTermosPage } from './webview.termos.page';

export const CommonNavigation = {
  register() {
    Navigation.register(AppointmentDetailPage);
    Navigation.register(ExamDetailPage);

    Navigation.register(ProfessionalSchedulePage);
    Navigation.register(SchedulesPage);
    Navigation.register(AppointmentSolicitationPage);
    Navigation.register(SpecialtiesPage);

    Navigation.register(LaboratoriesPage);

    Navigation.register(WebViewPage, false);
    Navigation.register(WebViewTermosPage, false);
    Navigation.register(WebHtmlPage, false);
  },
};
