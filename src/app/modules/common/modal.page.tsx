import * as React from 'react';
import { Navigation, NavigationPageProps } from '@app/core/navigation';

export class ModalPage<T, S = undefined> extends React.Component<NavigationPageProps<T>, S> {
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  async navigationButtonPressed(_buttonId: string) {
    await Navigation.dismissModal(this.props.componentId);
  }
}
