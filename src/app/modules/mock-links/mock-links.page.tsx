import * as React from 'react';
import { Subscribe } from 'unstated';
import { Navigation } from '@app/core/navigation';
import { AccountSwitchPage } from '@app/modules/account/account-switch.page';
import { BeneficiaryProvider } from '@app/services/providers';
import { PrimaryButton, Root, SecondaryButton, VBox, VSeparator } from '@atomic';

const buttons = [ AccountSwitchPage ];

export class MockLinksPage extends React.Component<any, any> {
  static options = {name: 'MockLinksPage', topBar: {backButton: {visible: false}}};

  render() {
    return (
      <Root>
        <VSeparator />
        <VBox>
          {buttons.map((page, index) => {
            const navigate = () => Navigation.push(this.props.componentId, page);
            return (
              <VBox key={index} noGutter={true} >
                <PrimaryButton
                  expanded={true}
                  onTap={navigate}
                  text={page.name}
                />
                <VSeparator />
              </VBox>
            );
            })
          }
        </VBox>
        <VSeparator />
        <Subscribe to={[BeneficiaryProvider]}>
          {(beneficiaryProvider: BeneficiaryProvider) => {
            return (
              <SecondaryButton
                negative={true}
                expanded={true}
                text='Logout'
                // tslint:disable-next-line:jsx-no-lambda
                onTap={() => beneficiaryProvider.logout()}
              />);
          }}
        </Subscribe>
      </Root>
    );
  }

}
