
import { Navigation } from '@app/core/navigation';
import { MockLinksPage } from './mock-links.page';

export const MockNavigation = {
  register: () => {
    Navigation.register( MockLinksPage );
  },
};
