import { PageView } from '@app/core/analytics';
import { GuardProps, Navigation } from '@app/core/navigation';
import React from 'react';
import { ActivityIndicator, Alert, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { GuardParams } from '../../authentication';
import { CollapsibleHeader as Collapsible } from '@atomic';
import { LargeHeader } from '@app/components/atm.header';
import { VBox } from '@atomic/obj.grid';
import { PharmacyFilterSelectItemPage } from './pharmacy-filter-select-item.page';
import pharmacyApi from '../pharmacy.api';
import { ButtonFooter } from '@app/components/mol.button-footer';
import { PrimaryButton } from '@atomic/atm.button';


interface PharmacyFilterPageProps extends GuardProps<GuardParams> {
    componentId: string;
    navigation: any;
}

interface PharmacyFilterPageState {
    estados: any[],
    cidades: any[],
    bairros: any[],
    bairro: any,
    estado: any,
    cidade: any,
    loading: boolean,
}

@PageView('PharmacyFilterPage')
export class PharmacyFilterPage extends React.Component<
PharmacyFilterPageProps,
PharmacyFilterPageState
> {
    static options = {
        name: 'PharmacyFilterPage',
        topBar: { backButton: { visible: true } },
    };

    state = {
        estados: [],
        cidades: [],
        bairros: [],
        loading: false,
        cidade: null,
        estado: null,
        bairro: null
    }

    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        this.setState({ loading: true });
        pharmacyApi.fetchEstados().then(estados => {
            this.setState({ loading: false })
            this.setState({ estados: estados });
        }).catch(err => {
            this.setState({ loading: false })
        })
    }


    tapSelectEstado = () => {
        Navigation.push(this.props.componentId, PharmacyFilterSelectItemPage, {
            title: 'Selecione o Estado',
            items: this.state.estados,
            onClose: (estado) => {
                this.setState({ estado: estado, cidades: [], loading: true });
                pharmacyApi.fetchCidades(estado).then(cidades => {
                    this.setState({ cidades, loading: false });
                }).catch(err => this.setState({ loading: false }))
            }
        })
    }


    tapSelectCidade = () => {
        if (!this.state.estado) {
            Alert.alert('', 'Selecione o Estado');
            return;
        }
        Navigation.push(this.props.componentId, PharmacyFilterSelectItemPage, {
            title: 'Selecione a Cidade',
            items: this.state.cidades,
            onClose: (cidade) => {
                this.setState({ cidade, loading: true });
                pharmacyApi.fetchBairros(this.state.estado, cidade).then(bairros => {
                    this.setState({ bairros, loading: false });
                }).catch(err => this.setState({ loading: false }))
            }
        })
    }

    tapBairros = () => {
        if (!this.state.estado) {
            Alert.alert('', 'Selecione o Estado');
            return;
        }

        if (!this.state.cidade) {
            Alert.alert('', 'Selecione uma Cidade');
            return;
        }

        Navigation.push(this.props.componentId, PharmacyFilterSelectItemPage, {
            title: 'Selecione um Bairro',
            items: this.state.bairros,
            onClose: (bairro) => {
                this.setState({ bairro });
            }
        })
    }

    tapFilter = () => {
        const data = {
            estado: this.state.estado.id,
            cidade: this.state.cidade.label,
            bairro: null
        }

        if (this.state.bairro) {
            data.bairro = this.state.bairro.id
        }

        Navigation.pop(this.props.componentId);
        this.props.navigation.applyFilter(data);
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {
                    this.state.loading ? <View style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center',
                        zIndex: 10,
                        elevation: 10
                    }}>
                        <ActivityIndicator
                            size="large" color={'#398eb9'}
                        />
                    </View> : null
                }
                <ScrollView>
                    <Collapsible.LargeHeader>
                        <LargeHeader title={'Filtrar'} />
                    </Collapsible.LargeHeader>
                    <VBox>
                        <Text style={styles.label}>Estado</Text>
                        <TouchableOpacity disabled={this.state.loading} onPress={this.tapSelectEstado} style={styles.selectContainer}>
                            <Text style={styles.selectText}>{this.state.estado ? this.state.estado.label : 'Selecionar'}</Text>
                        </TouchableOpacity>
                    </VBox>
                    <VBox>
                        <Text style={styles.label}>Cidade</Text>
                        <TouchableOpacity disabled={this.state.loading} onPress={this.tapSelectCidade} style={styles.selectContainer}>
                            <Text style={styles.selectText}>{this.state.cidade ? this.state.cidade.label : 'Selecionar'}</Text>
                        </TouchableOpacity>
                    </VBox>
                    <VBox>
                        <Text style={styles.label}>Bairro</Text>
                        <TouchableOpacity disabled={this.state.loading} onPress={this.tapBairros} style={styles.selectContainer}>
                            <Text style={styles.selectText}>{this.state.bairro ? this.state.bairro.label : 'Selecionar'}</Text>
                        </TouchableOpacity>
                    </VBox>
                    <VBox style={{marginTop: 20}}>
                        <PrimaryButton expanded={true} text="Filtrar" disabled={!(this.state.estado && this.state.cidade)}
                            onTap={this.tapFilter} />
                    </VBox>
                </ScrollView>
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    label: {
        fontWeight: 'bold',
        color: '#000000',
        fontSize: 16,
        marginBottom: 11,
        marginTop: 20
    },
    selectContainer: {
        borderWidth: 2,
        borderColor: '#eeeeee',
        padding: 12,
        borderRadius: 10,
    },
    selectText: {
        color: '#757575',
        fontSize: 16,
    }
})