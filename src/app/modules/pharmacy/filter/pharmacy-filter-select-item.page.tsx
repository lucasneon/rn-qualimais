import { PageView } from '@app/core/analytics';
import { GuardProps, Navigation } from '@app/core/navigation';
import React from 'react';
import { Image, SafeAreaView, ScrollView, View } from 'react-native';
import { GuardParams } from '../../authentication';
import { CollapsibleHeader as Collapsible } from '@atomic';
import { LargeHeader } from '@app/components/atm.header';
import { VBox } from '@atomic/obj.grid';
import { ListCell } from '@app/components/mol.cell';
import { Asset } from '@atomic/obj.asset';
import { createFilter } from 'react-native-search-filter';
import IconSearch from '@assets/icons/pharmacy/search.png';
import { TextField } from '@atomic/atm.text-field';

interface PharmacyFilterSelectItemPageProps extends GuardProps<GuardParams> {
  componentId: string;
  navigation: any;
}

interface PharmacyFilterSelectItemPageState {
  term: string;
}

@PageView('PharmacyFilterSelectItemPage')
export class PharmacyFilterSelectItemPage extends React.Component<
  PharmacyFilterSelectItemPageProps,
  PharmacyFilterSelectItemPageState
> {
  static options = {
    name: 'PharmacyFilterSelectItemPage',
    topBar: { backButton: { visible: true } },
  };

  state = {
    term: '',
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  onTapItem = item => {
    Navigation.pop(this.props.componentId);
    this.props.navigation.onClose(item);
  };

  render() {
    const dataFiltered = this.props.navigation.items.filter(
      createFilter(this.state.term, ['id', 'label']),
    );

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Collapsible.LargeHeader>
          <LargeHeader title={this.props.navigation.title} />
        </Collapsible.LargeHeader>
        <View
          style={{
            margin: 12,
            padding: 10,
            borderRadius: 10,
          }}>
          <TextField
            onChangeText={term => {
              this.setState({ term: term });
            }}
            placeholder="Buscar farmácias"
            style={{
              paddingRight: 20,
            }}
          />
          <Image
            source={IconSearch}
            style={{
              position: 'absolute',
              right: 20,
              top: 25,
              width: 20,
              height: 20,
            }}
          />
        </View>
        <ScrollView style={{ backgroundColor: '#ffffff' }}>
          <VBox>
            {dataFiltered.map(item => {
              return (
                <ListCell
                  key={item.id}
                  text={item.label}
                  rightThumb={Asset.Icon.Atomic.ChevronRight}
                  onTap={() => this.onTapItem(item)}
                />
              );
            })}
          </VBox>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
