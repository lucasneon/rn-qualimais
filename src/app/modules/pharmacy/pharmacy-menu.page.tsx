import { PageView } from '@app/core/analytics';
import { GuardProps, Navigation } from '@app/core/navigation';
import React from 'react';
import {
  ActivityIndicator,
  Image,
  ImageBackground,
  Text,
  View,
} from 'react-native';
import { GuardParams } from '../authentication';
import { CollapsibleHeader as Collapsible } from '@atomic';
import { LargeHeader } from '@app/components/atm.header';
import {
  LocationProvider,
  LocationProviderToken,
} from '@app/services/providers';
import Container from 'typedi';
import BgCarteirinha from '@assets/images/bg_carteirinha.png';
import LogoEpharme from '@assets/images/logo_epharme.png';
import { VBox, VSeparator } from '@atomic/obj.grid';
import { ListCell } from '@app/components/mol.cell';
import { Asset } from '@atomic/obj.asset';
import { PharmacyPage } from './pharmacy.page';
import { FinancialPage } from '../ financial-statement';
import pharmacyApi from './pharmacy.api';
import PharmacyIcon from '@assets/icons/pharmacy/pharmacy_icon.png';
import ExtratoIcon from '@assets/icons/pharmacy/extrato_icon.png';
import MedicationIcon from '@assets/icons/pharmacy/medication_icon.png';
import { MedicinePage } from './medicine/mdicines.page';
interface PharmacyMenuPageProps extends GuardProps<GuardParams> {
  componentId: string;
}

interface PharmacyMenuPageState {
  loading: boolean;
  dadosBeneficiario: any;
}

@PageView('PharmacyPageCarteirinha')
export class PharmacyMenuPage extends React.Component<
  PharmacyMenuPageProps,
  PharmacyMenuPageState
> {
  static options = {
    name: 'PharmacyPageCarteirinha',
    topBar: { backButton: { visible: true } },
  };

  private readonly locationProvider: LocationProvider = Container.get(
    LocationProviderToken,
  );

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);

    this.state = {
      dadosBeneficiario: null,
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    pharmacyApi
      .fetchDadosBenenificiario()
      .then(beneficiario => {
        this.setState({
          dadosBeneficiario: beneficiario,
          loading: false,
        });
      })
      .catch(() => {
        this.setState({ loading: false });
      });
  }

  render() {
    const { dadosBeneficiario, loading } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Collapsible.LargeHeader>
          <LargeHeader title={'Farmácia'} />
        </Collapsible.LargeHeader>
        {loading ? (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
            }}>
            <ActivityIndicator size="large" color="#009688" />
          </View>
        ) : (
          <VBox>
            <View>
              <ImageBackground
                resizeMode="stretch"
                source={BgCarteirinha}
                style={{
                  height: 250,
                  width: '100%',
                }}>
                <View style={{ padding: 20, flex: 1 }}>
                  <View>
                    <Image
                      source={LogoEpharme}
                      style={{ width: 123, height: 20 }}
                    />
                  </View>
                  <View style={{ marginTop: 'auto' }}>
                    <Text
                      style={{
                        marginBottom: 7,
                        color: '#00619a',
                        fontSize: 20,
                        fontWeight: 'bold',
                      }}>
                      {dadosBeneficiario ? dadosBeneficiario.idUsuario : ''}
                    </Text>
                    <Text
                      style={{
                        marginBottom: 4,
                        color: '#00619a',
                        fontSize: 12,
                      }}>
                      {dadosBeneficiario ? dadosBeneficiario.nomeCompleto : ''}
                    </Text>
                    <Text
                      style={{
                        marginBottom: 4,
                        color: '#00619a',
                        fontSize: 12,
                      }}>
                      {dadosBeneficiario ? dadosBeneficiario.nomePlano : ''}
                    </Text>
                  </View>
                </View>
              </ImageBackground>
            </View>
            <VBox>
              <VSeparator />
              <ListCell
                text={'Farmácias'}
                rightThumb={Asset.Icon.Atomic.ChevronRight}
                leftThumb={PharmacyIcon}
                onTap={this.handlePharmacy}
              />
              <ListCell
                text={'Medicamentos'}
                rightThumb={Asset.Icon.Atomic.ChevronRight}
                leftThumb={MedicationIcon}
                onTap={this.handleMedicine}
              />
              <ListCell
                text={'Extrato'}
                rightThumb={Asset.Icon.Atomic.ChevronRight}
                leftThumb={ExtratoIcon}
                onTap={this.handleFinancialTap}
              />
            </VBox>
          </VBox>
        )}
      </View>
    );
  }

  private handlePharmacy = () => {
    Navigation.push(this.props.componentId, PharmacyPage);
  };

  private handleFinancialTap = () => {
    Navigation.push(this.props.componentId, FinancialPage);
  };

  private handleMedicine = () => {
    Navigation.push(this.props.componentId, MedicinePage);
  };
}
