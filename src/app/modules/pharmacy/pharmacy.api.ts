import {
  PharmacyTokenUrl,
  PharmacyClientId,
  PharmacyUsername,
  PharmacyPassword,
} from '@app/config';
import axios from 'axios';
import Container from 'typedi';

class PharmacyApi {
  private readonly tokenUrl: string = Container.get(PharmacyTokenUrl);
  private readonly clientId: string = Container.get(PharmacyClientId);
  private readonly username: string = Container.get(PharmacyUsername);
  private readonly password: string = Container.get(PharmacyPassword);

  getToken() {
    const params = {
      grant_type: 'password',
      client_id: this.clientId,
      username: this.username,
      password: this.password,
    };
    const data = Object.keys(params)
      .map(key => `${key}=${encodeURIComponent(params[key])}`)
      .join('&');

    return axios({
      method: 'post',
      url: this.tokenUrl,
      data: data,
      headers: {
        'Content-Type': `application/x-www-form-urlencoded`,
      },
    }).then(response => response.data.access_token);
  }

  fetchByLocation(lat: number, lng: number) {
    return this.getToken().then(token => {
      return axios.get(
        `https://restqa.epharma.com.br/api/RedeCredenciada/GeoPorCliente?cliente=2640&latitude=${lat}&longitude=${lng}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
    });
  }

  fetchByFilter(estado: string, cidade: string, bairro?: string) {
    let query = `plano=2640&uf=${estado}&cidade=${cidade}`;
    if (bairro) {
      query += `&bairro=${bairro}`;
    }
    console.log(query);
    return this.getToken().then(token => {
      return axios.get(
        `https://restQA.epharma.com.br/api/RedeCredenciada?${query}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
    });
  }

  fetchEstados() {
    return this.getToken().then(token => {
      return axios
        .get(
          'https://restQA.epharma.com.br/api/RedeCredenciada/Estados?plano=2640',
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then(response => response.data)
        .then(response => response.estados)
        .then(response =>
          response.map(item => {
            return {
              id: item.codigo,
              label: item.nome,
            };
          }),
        );
    });
  }

  fetchCidades(estado) {
    return this.getToken().then(token => {
      return axios
        .get(
          `https://restQA.epharma.com.br/api/RedeCredenciada/Cidades?plano=2640&uf=${estado.id}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then(response => response.data)
        .then(response => response.cidades)
        .then(response =>
          response.map(item => {
            return {
              id: item.codigo,
              label: item.nome,
            };
          }),
        );
    });
  }

  fetchBairros(estado, cidade) {
    return this.getToken().then(token => {
      return axios
        .get(
          `https://restQA.epharma.com.br/api/RedeCredenciada/Bairros?plano=2640&uf=${estado.id}&cidade=${cidade.label}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then(response => response.data)
        .then(response => response.bairros)
        .then(response =>
          response.map(item => {
            return {
              id: item.nome,
              label: item.nome,
            };
          }),
        );
    });
  }

  fetchDadosBenenificiario() {
    return this.getToken().then(token => {
      return axios
        .get(
          `https://restqa.epharma.com.br/api/Beneficiario/BeneficiariosAtivosPorPeriodo?status=A&plano=65590&identificador=202109000001`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then(response => response.data)
        .then(response => response.data[0]);
    });
  }
}

export default new PharmacyApi();
