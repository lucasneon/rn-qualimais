import { FlatList, TouchableOpacity } from 'react-native';
import React from 'react';
import { SafeAreaView, View } from 'react-native';
import { HBox } from '@atomic';
import { Body, H4 } from '@atomic/atm.typography';
import { LinkButton } from '@atomic/atm.button';
import { Cell } from '@atomic/mol.cell';

const Item = ({ name, descriptions, links, onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <View style={{ paddingTop: 16 }}>
      <Cell>
        <HBox wrap={false} vAlign="center" hAlign="flex-start">
          <HBox.Item grow={3}>
            <H4>{name}</H4>
          </HBox.Item>
          <HBox.Item grow={1} />
        </HBox>
        {descriptions &&
          descriptions.map(desc => desc && <Body key={desc}>{desc}</Body>)}
        {links &&
          links.length > 0 &&
          links.map(link => (
            <LinkButton align="flex-start" key={link.text} {...link} />
          ))}
      </Cell>
    </View>
  </TouchableOpacity>
);

export function PharmacyNearby(props) {
  const renderItem = ({ item }) => (
    <Item {...item} onPress={() => props.onPress(item)} />
  );

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList
        style={{ paddingLeft: 20 }}
        data={props.data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
}
