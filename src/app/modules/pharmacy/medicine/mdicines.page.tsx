import { PageView } from '@app/core/analytics';
import { GuardProps, Navigation } from '@app/core/navigation';
import React from 'react';
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  StyleSheet,
  View,
} from 'react-native';
import { GuardParams } from '../../authentication';
import { CollapsibleHeader as Collapsible } from '@atomic';
import { LargeHeader } from '@app/components/atm.header';
import SearchInput from 'react-native-search-filter';
import { Asset } from '@atomic/obj.asset';

import medicineApi from './api';
import { ListCell } from '@app/components/mol.cell';
import { MedicineDetailPage } from './medicine-detail.page';
import SearchIcon from '@assets/icons/pharmacy/search.png';
import { DT } from '@atomic/atm.typography';

interface MedicinePageProps extends GuardProps<GuardParams> {
  componentId: string;
}

interface MedicinePageState {
  data: any[];
  loading: boolean;
  term: string;
}

@PageView('Pharmacy')
export class MedicinePage extends React.Component<
  MedicinePageProps,
  MedicinePageState
> {
  timeoutSearch = null;

  static options = {
    name: 'MedicinePage',
    topBar: { backButton: { visible: true } },
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      data: [],
      loading: false,
      term: '',
    };
  }

  componentDidMount() {
    this.fetchMedicines();
  }

  private fetchMedicines() {
    this.setState({ loading: true });
    medicineApi
      .fetchMedicines(this.state.term)
      .then(response => {
        this.setState({ data: response, loading: false });
      })
      .catch(() => {
        this.setState({ loading: false });
      });
  }

  onChangeText = term => {
    if (this.timeoutSearch) {
      clearTimeout(this.timeoutSearch);
    }

    this.timeoutSearch = setTimeout(() => {
      this.timeoutSearch = null;
      this.setState({ term });
      this.fetchMedicines();
    }, 500);
  };

  private tapItem = item => {
    Navigation.push(this.props.componentId, MedicineDetailPage, { data: item });
  };

  render() {
    const { data, loading } = this.state;

    const renderItem = ({ item }) => {
      return (
        <ListCell
          text={item.principioAtivo}
          rightThumb={Asset.Icon.Atomic.ChevronRight}
          onTap={() => this.tapItem(item)}
        />
      );
    };

    return (
      <View style={{ flex: 1 }}>
        <Collapsible.LargeHeader>
          <LargeHeader title={'Medicamentos'} />
        </Collapsible.LargeHeader>
        <View>
          <SearchInput
            onChangeText={term => {
              this.onChangeText(term);
            }}
            style={styles.searchInput}
            placeholder="Buscar medicamento"
          />
        </View>
        {loading ? (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color="#009688" />
          </View>
        ) : this.isEmpty() ? (
          <View style={styles.containerEmpty}>
            <DT>Nenhum medicamento disponível</DT>
          </View>
        ) : (
          <View style={{ flex: 1 }}>
            <SafeAreaView style={{ flex: 1 }}>
              <FlatList
                style={{ paddingLeft: 20, flex: 1 }}
                data={data}
                renderItem={renderItem}
                keyExtractor={item => item.id}
              />
            </SafeAreaView>
          </View>
        )}
      </View>
    );
  }

  private isEmpty = () => {
    const list = this.state.data;
    return list.length === 0 || list === [] || list === null;
  };
}

const styles = StyleSheet.create({
  searchInput: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
    borderColor: '#eeeeee',
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
  },
  containerEmpty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
