import axios from 'axios';
import pharmacyApi from '../pharmacy.api';

class MedicineApi {
  fetchMedicines(filter?: string) {
    let query = 'plano=2640';
    if (filter) {
      query += `&filtro=${filter}`;
    }
    return pharmacyApi.getToken().then(token => {
      return axios
        .get(`https://restQA.epharma.com.br/api/ConsultaMedicamento?${query}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then(response => response.data)
        .then(response => response.medicamentos);
    });
  }
}

export default new MedicineApi();
