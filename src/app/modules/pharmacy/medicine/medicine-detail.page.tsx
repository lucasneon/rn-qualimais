import { PageView } from '@app/core/analytics';
import { GuardProps, Navigation, NavigationPageProps } from '@app/core/navigation';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { GuardParams } from '../../authentication';
import { CollapsibleHeader as Collapsible } from '@atomic';
import { LargeHeader } from '@app/components/atm.header';
import { VBox } from '@atomic/obj.grid';
import { Scroll } from '@atomic/obj.scroll';

interface MedicineDetailPageProps extends GuardProps<GuardParams> {
  componentId: string;
  data: any;
}

interface MedicineDetailPageState {}

@PageView('Pharmacy')
export class MedicineDetailPage extends React.Component<
  NavigationPageProps<MedicineDetailPageProps>,
  MedicineDetailPageState
> {
  static options = {
    name: 'MedicineDetailPage',
    topBar: { backButton: { visible: true } },
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  render() {
    var item = this.props.navigation.data;
    return (
      <View style={{ flex: 1 }}>
        <Collapsible.LargeHeader>
          <LargeHeader title={'Tylenol 500mg'} />
        </Collapsible.LargeHeader>
        <Scroll>
          <VBox>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
                marginBottom: 10,
                color: '#00619a',
                marginTop: 30,
              }}>
              Detalhes
            </Text>
            <View style={styles.boxDetail}>
              <View style={styles.lineDetail}>
                <Text style={styles.label}>Apresentação</Text>
                <Text style={styles.description}>{item.apresentacao}</Text>
              </View>
              <View style={styles.lineDetail}>
                <Text style={styles.label}>Laboratório</Text>
                <Text style={styles.description}>{item.laboratorio}</Text>
              </View>
              {/* <View style={styles.lineDetail}>
                <Text style={styles.label}>Código de barras</Text>
                <Text style={styles.description}>7897705201770</Text>
              </View> */}
              <View>
                <Text style={styles.label}>Princípios ativos</Text>
                <Text style={styles.description}>{item.principioAtivo}</Text>
              </View>
            </View>
          </VBox>
          {/* <VBox>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
                marginBottom: 10,
                color: '#00619a',
                marginTop: 30,
              }}>
              Melhores Preços
            </Text>
            <Text style={{ marginBottom: 20, marginTop: 10 }}>
              Ressaltamos que o valor do medicamento indicado é o valor máximo a
              ser pago e que poderá sofrer variações no momento da compra.
            </Text>
            <View style={styles.boxDetail}>
              <View
                style={[
                  styles.lineDetail,
                  { flexDirection: 'row', alignItems: 'center' },
                ]}>
                <Text>NORDISK - NOVO DIA</Text>
                <Text style={{ marginLeft: 'auto', color: '#00619a' }}>
                  R$ 19,90{' '}
                </Text>
              </View>
              <View style={[{ flexDirection: 'row', alignItems: 'center' }]}>
                <Text>NORDISK - NOVO DIA</Text>
                <Text style={{ marginLeft: 'auto', color: '#00619a' }}>
                  R$ 19,90{' '}
                </Text>
              </View>
            </View>
            <Text style={{ marginTop: 20, marginBottom: 34 }}>
              Para escolha da melhor condição de compra, verifique se o programa
              oferece subsídio e/ou desconto em ficha.
            </Text>
          </VBox> */}
        </Scroll>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  boxDetail: {
    padding: 20,
    borderWidth: 2,
    borderColor: '#eeeeee',
    borderRadius: 10,
  },

  lineDetail: {
    borderBottomWidth: 1,
    borderColor: '#eeeeee',
    paddingBottom: 16,
    marginBottom: 16,
  },

  label: {
    color: '#000000',
    fontSize: 14,
    fontWeight: 'bold',
  },

  description: {
    fontSize: 14,
    marginTop: 10,
  },
});
