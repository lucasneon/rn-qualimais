import { Navigation } from '@app/core/navigation';
import { PharmacyDetailPage } from './pharmacy-detail.page';
import { PharmacyFilterSelectItemPage } from './filter/pharmacy-filter-select-item.page';
import { PharmacyFilterPage } from './filter/pharmacy-filter.page';
import { PharmacyMenuPage } from './pharmacy-menu.page';
import { PharmacyPage } from './pharmacy.page';
import { MedicinePage } from './medicine/mdicines.page';
import { MedicineDetailPage } from './medicine/medicine-detail.page';
export const PharmacyNavigation = {
  register: () => {
    Navigation.register(PharmacyPage);
    Navigation.register(PharmacyDetailPage);
    Navigation.register(PharmacyMenuPage);
    Navigation.register(PharmacyFilterPage);
    Navigation.register(PharmacyFilterSelectItemPage);
    Navigation.register(MedicinePage);
    Navigation.register(MedicineDetailPage);
  },
};
