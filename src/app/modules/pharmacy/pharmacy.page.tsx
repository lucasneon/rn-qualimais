import { PageView } from '@app/core/analytics';
import {
  ExternalNavigator,
  GuardProps,
  Navigation,
} from '@app/core/navigation';
import React from 'react';
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { GuardParams } from '../authentication';
import { CollapsibleHeader as Collapsible } from '@atomic';
import { LargeHeader } from '@app/components/atm.header';
import { PharmacyNearby } from './pharmacy-item';
import {
  LocationProvider,
  LocationProviderToken,
} from '@app/services/providers';
import Container from 'typedi';
import { createFilter } from 'react-native-search-filter';
import { Asset } from '@atomic/obj.asset';
import { GeneralStrings } from '../common';

import pharmacyApi from './pharmacy.api';
import { PharmacyDetailPage } from './pharmacy-detail.page';
import { PharmacyFilterPage } from './filter/pharmacy-filter.page';
import { Placeholder } from '@app/components/mol.placeholder';
import { TextField } from '@atomic/atm.text-field';
import IconSearch from '@assets/icons/pharmacy/search.png';

const KEYS_TO_FILTERS = ['name'];

interface PharmacyPageProps extends GuardProps<GuardParams> {
  componentId: string;
}

interface PharmacyPageState {
  data: any[];
  loading: boolean;
  permissionDenied: boolean;
  term: string;
  filter?: {
    estado: string;
    cidade: string;
    bairro?: string;
  };
}

@PageView('Pharmacy')
export class PharmacyPage extends React.Component<
  PharmacyPageProps,
  PharmacyPageState
> {
  static options = {
    name: 'PharmacyPage',
    topBar: { backButton: { visible: true } },
  };

  private readonly externalNavigator: ExternalNavigator =
    Container.get(ExternalNavigator);
  private readonly locationProvider: LocationProvider = Container.get(
    LocationProviderToken,
  );

  state = {
    data: [],
    loading: false,
    term: '',
    filter: null,
    permissionDenied: false,
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    this.fetchByLocation();
  }

  private fetchByLocation() {
    this.locationProvider.handlePlatforms(location => {
      if (location) {
        this.setState({ loading: true });
        const { latitude, longitude } = location.coords;

        pharmacyApi
          .fetchByLocation(latitude, longitude)
          .then(response => {
            return response.data;
          })
          .then(response => {
            return this.parseResponse(response);
          })
          .then(response => {
            this.setState({ data: response, loading: false });
          })
          .finally(() => {
            this.setState({ loading: false });
          });
      } else {
        this.setState({ permissionDenied: true, loading: false });
      }
    });
  }

  onChangeText = term => {
    this.setState({ term });
  };

  tapItem = item => {
    Navigation.push(this.props.componentId, PharmacyDetailPage, {
      data: item,
    });
  };

  tapFilter = () => {
    Navigation.push(this.props.componentId, PharmacyFilterPage, {
      applyFilter: filter => {
        this.setState({ filter, data: [], loading: true });
        pharmacyApi
          .fetchByFilter(filter.estado, filter.cidade, filter.bairro)
          .then(response => {
            return response.data;
          })
          .then(response => {
            return this.parseResponse(response);
          })
          .then(response => {
            this.setState({ data: response, loading: false });
          })
          .finally(() => {
            this.setState({ loading: false });
          });
      },
    });
  };

  private parseResponse(response) {
    return response.farmacias.map((data: any, key) => {
      const links = [];
      if (data.telefone) {
        links.push({
          image: Asset.Icon.Custom.Phone,
          text: data.telefone,
          onTap: () => this.externalNavigator.makeCall(data.telefone),
        });
      }
      let hasLocation = false;
      if (data.latitude && data.longitude) {
        hasLocation = true;
        data.latitude = parseFloat(data.latitude.replace(',', '.'));
        data.longitude = parseFloat(data.longitude.replace(',', '.'));
        links.push({
          image: Asset.Icon.Custom.NavigationIcon,
          text: GeneralStrings.TraceRoute,
          onTap: () =>
            this.externalNavigator.openMaps({
              latitude: data.latitude,
              longitude: data.longitude,
            }),
        });
      }

      return {
        ...data,
        id: key,
        name: data.rede,
        hasLocation,
        descriptions: [
          `${data.endereco}`,
          `${data.cidade} - ${data.uf}`,
          `${data.cep}`,
        ],
        links: links,
      };
    });
  }

  render() {
    const dataFiltered = this.state.data.filter(
      createFilter(this.state.term, KEYS_TO_FILTERS),
    );
    const { filter, permissionDenied, loading, data } = this.state;

    const componentPermissionDenied = (
      <View style={styles.containerPermissionDanied}>
        <Text style={{ fontSize: 13, color: '#F23E3E' }}>
          A permissão de GPS foi negada.
        </Text>
        <Text style={{ fontSize: 13, color: '#F23E3E' }}>
          Por favorm, tente filtrar por região.
        </Text>
        <TouchableOpacity style={{ marginTop: 10 }} onPress={this.tapFilter}>
          <Text style={{ fontSize: 14, color: '#00619a' }}>
            Trocar / Filtrar
          </Text>
        </TouchableOpacity>
      </View>
    );

    const componentLocation = (
      <View
        style={{
          height: 100,
          backgroundColor: '#eeeeee',
          width: '100%',
          paddingLeft: 40,
          justifyContent: 'center',
        }}>
        <Text style={{ fontSize: 16, color: '#7f7f7f' }}>
          Resultados próximos a
        </Text>
        {filter ? (
          <Text style={{ fontSize: 16, color: '#000000', fontWeight: 'bold' }}>
            {filter.barro ? `${filter.bairro} ` : null}
            {filter.cidade} - {filter.estado}
          </Text>
        ) : (
          <Text style={{ fontSize: 16, color: '#000000', fontWeight: 'bold' }}>
            Sua localização atual
          </Text>
        )}

        <TouchableOpacity style={{ marginTop: 10 }} onPress={this.tapFilter}>
          <Text style={{ fontSize: 14, color: '#00619a' }}>
            Trocar / Filtrar
          </Text>
        </TouchableOpacity>
      </View>
    );

    const componentLoading = (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
        }}>
        <ActivityIndicator size="large" color="#009688" />
      </View>
    );

    const componentList = (
      <View style={{ flex: 1 }}>
        <PharmacyNearby
          data={dataFiltered}
          onPress={item => this.tapItem(item)}
        />
      </View>
    );

    const notFoundResults = (
      <Placeholder
        title={'Farmácias não encontradas'}
        image={Asset.Icon.Placeholder.Failure}
      />
    );

    return (
      <View style={{ flex: 1 }}>
        <Collapsible.LargeHeader>
          <LargeHeader title={'Farmácias'} />
        </Collapsible.LargeHeader>
        <View
          style={{
            margin: 12,
            padding: 10,
            borderRadius: 10,
          }}>
          <TextField
            onChangeText={term => {
              this.onChangeText(term);
            }}
            placeholder="Buscar farmácias"
            style={{
              paddingRight: 20,
            }}
          />
          <Image
            source={IconSearch}
            style={{
              position: 'absolute',
              right: 20,
              top: 25,
              width: 20,
              height: 20,
            }}
          />
        </View>
        {this.state.permissionDenied && !filter
          ? componentPermissionDenied
          : null}
        {!permissionDenied || filter ? componentLocation : null}
        {this.state.loading ? componentLoading : componentList}
        {!loading && (!data || data.length == 0) ? notFoundResults : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerPermissionDanied: {
    height: 100,
    backgroundColor: '#eeeeee',
    width: '100%',
    paddingLeft: 40,
    justifyContent: 'center',
  },
});
