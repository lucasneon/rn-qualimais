import { PageView } from '@app/core/analytics';
import {
  ExternalNavigator,
  GuardProps,
  Navigation,
} from '@app/core/navigation';
import React from 'react';
import {
  Dimensions,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { GuardParams } from '../authentication';
import Container from 'typedi';
import { GeneralStrings } from '../common';
import { Body, H1 } from '@atomic/atm.typography';
import { HBox } from '@atomic/obj.grid';
import { LinkButton, PrimaryButton } from '@atomic/atm.button';
import MapView, { Marker } from 'react-native-maps';
import { LargeHeaderHeight } from '@app/components/atm.header/large-header.component.style';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

interface PharmacyDetailPageProps extends GuardProps<GuardParams> {
  componentId: string;
  navigation: any;
}

interface PharmacyDetailPageState {}

@PageView('PharmacyDetail')
export class PharmacyDetailPage extends React.Component<
  PharmacyDetailPageProps,
  PharmacyDetailPageState
> {
  static options = {
    name: 'PharmacyDetailPage',
    topBar: { backButton: { visible: true } },
  };

  private readonly externalNavigator: ExternalNavigator =
    Container.get(ExternalNavigator);

  state = {
    region: {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    },
    markers: [],
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {}

  render() {
    const { data } = this.props.navigation;

    let mapComponent = null;
    let btnMapComponent = null;
    if (data.longitude && data.latitude) {
      const coordinate = {
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
        latitude: data.latitude,
        longitude: data.longitude,
      };
      mapComponent = (
        <MapView style={{ height: 450 }} initialRegion={coordinate}>
          <Marker key={'1'} coordinate={coordinate} />
        </MapView>
      );

      btnMapComponent = (
        <PrimaryButton
          text={GeneralStrings.TraceRoute}
          expanded={true}
          style={{ marginTop: 10, paddingBottom: 5 }}
          onTap={() => this.externalNavigator.openMaps(coordinate)}
        />
      );
    } else {
      mapComponent = (
        <View style={{ marginTop: LargeHeaderHeight }}>
          <Text style={{ fontSize: 16, marginLeft: 20, marginRight: 20 }}>
            Sem coordenadas cadastradas
          </Text>
        </View>
      );
    }

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView>
          {mapComponent}
          <View
            style={{ flex: 1, marginLeft: 20, marginRight: 20, marginTop: 21 }}>
            <HBox wrap={false} vAlign="center" hAlign="flex-start">
              <HBox.Item grow={3}>
                <H1>{data.rede}</H1>
              </HBox.Item>
              <HBox.Item grow={1} />
            </HBox>
            {data.descriptions.map(
              desc => desc && <Body key={desc}>{desc}</Body>,
            )}
            <LinkButton
              align="flex-start"
              text={data.telefone}
              onTap={() => this.externalNavigator.makeCall(data.telefone)}
            />
            {btnMapComponent}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
