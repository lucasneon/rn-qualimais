import * as React from 'react';
import { LoadingState, Scroll, Shimmer, ShimmerSeparator, VBox, VSeparator, H2, Asset } from '@atomic';
import { MagazineList, MagazineCellProps } from '@app/components/mol.magazine-list';
import { Publication, MagazinesMapper } from '@app/modules/magazines/magazines.mapper';
import { ApolloError } from 'apollo-client';
import { PublicationsQueryVariables, PublicationsQuery } from '@app/data/graphql';
import { GraphQLProvider, GraphQLProviderToken } from '@app/core/graphql';
import { Container } from 'typedi/Container';
import { Placeholder } from '@app/components/mol.placeholder';

export interface MagazinesProps {
  onMagazineTap?: (magazine: Publication) => void;
  disabled?: boolean;
  title: string;
  publicationType: string;
  notFoundTitle: string;
  notFoundMessage: string;
}

export interface MagazineState {
  publications: MagazineCellProps[];
  loading: boolean;
  error: ApolloError;
}

export const MagazinesShimmer = () => (
  <VBox>
    <Shimmer rounded={false} height={20} width={200} />
    <Scroll contentContainerStyle={{ flexDirection: 'row' }}>
      <Shimmer rounded={true} height={200} width={150} />
      <ShimmerSeparator />
      <Shimmer rounded={true} height={200} width={150} />
      <ShimmerSeparator />
      <Shimmer rounded={true} height={200} width={150} />
    </Scroll>
  </VBox>
);

export class Magazines extends React.Component<MagazinesProps, MagazineState> {

  private readonly graphQLProvider: GraphQLProvider = Container.get(GraphQLProviderToken);

  constructor(props) {
    super(props);
    this.state = {
      publications: null,
      loading: false,
      error: null,
    };
  }

  componentDidMount() {
    this.queryMagazines();
  }

  private queryMagazines() {
    this.setState({ loading: true });
    const publicationType = this.props.publicationType;

    this.graphQLProvider.query<PublicationsQueryVariables>(
      'publications',
      { data: { publicationType } },
      this.onQueryMagazinesSuccess,
      this.onQueryError,
    );
  }

  private onQueryMagazinesSuccess = (resp: PublicationsQuery) => {
    const publications: MagazineCellProps[] = resp.Publications && resp.Publications.content.map(this.mapToCellProps);
    this.setState({ publications: publications, loading: false });
  }

  private onQueryError = (error: ApolloError) => {
    this.setState({ error, loading: false });
  }

  private mapToCellProps = (magazine: Publication) => {
    const cell = MagazinesMapper.mapToCellProps(magazine);

    if (cell) {
      cell.onTap = () => this.props.onMagazineTap(magazine);
    }

    return cell;
  }

  render() {
    return (
      <LoadingState
      loading={this.state.loading}
      data={!!this.state.publications}
      error={!!this.state.error}
      >
        <LoadingState.Shimmer>
          <MagazinesShimmer />
        </LoadingState.Shimmer>
        <VSeparator />
        <VBox>
          {this.state.publications && this.state.publications.length === 0 ? (
            <Placeholder
              title={this.props.notFoundTitle}
              message={this.props.notFoundMessage}
              image={Asset.Icon.Placeholder.Failure}
            />
          ) : this.state.publications && this.state.publications.length > 0 && (
            <>
              <H2>{this.props.title}</H2>
              <MagazineList>
                {this.state.publications && this.state.publications.map(it =>
                  <MagazineList.Item disabled={this.props.disabled} key={it.label} {...it} />,
                )}
              </MagazineList>
            </>
          )}
        </VBox>
      </LoadingState>
    );
  }
}
