export const PublicationsStrings = {
  Title: 'Revistas e Manuais',
  Magazines: {
    Title: 'Revistas Disponíveis',
    NotFound: {
      Title: 'Ops!',
      Message: 'Nenhuma revista encontrada.',
    },
  },
  Documents: {
    Title: 'Manuais e Documentos Disponíveis',
    NotFound: {
      Title: 'Ops!',
      Message: 'Nenhuma revista encontrada.',
    },
  },
  Details: {
    Title: 'Detalhes da publicação',
    DonwloadFile: 'Download',
    Open: "Abrir",
    MoreInfo: {
      Title: "Mais informações",
      PublicationDate: "Data de publicação",
      Category: "Categoria",
      FileType: "Formato",
    }
  },
};
