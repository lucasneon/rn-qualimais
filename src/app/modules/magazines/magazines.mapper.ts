import { MagazineCellProps } from '@app/components/mol.magazine-list';

export const MagazinesMapper = {
  mapToCellProps(magazine: Publication): MagazineCellProps {
    let cellProps: MagazineCellProps;
    cellProps = {
      label: magazine.name || '-----',
      value: magazine.id.toString(),
      caption: magazine.description  || '-----',
      image: magazine.thumbnail,
      date: magazine.publicationDate  || '-----',
      file: {
        id: magazine.file.id,
        hash: magazine.file.hash,
        url: magazine.file.url,
      }
    };
    return cellProps;
  },
};

export interface Publication {
  id: number;
  name: string;
  description: string;
  publicationType: string;
  publicationDate: string;
  thumbnail: string;
  file: PublicationFile;
}

export interface PublicationFile {
  id: number;
  hash: string;
  url: string;
}
