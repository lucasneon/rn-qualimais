import * as React from 'react';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { NavigationPageProps, ExternalNavigator } from '@app/core/navigation';
import { Body, SecondaryButton, Root, VBox, VSeparator, HBox, H1, H2, DividerGray } from '@atomic';
import { PublicationsStrings } from '@app/modules/magazines/publications.strings';
import { MagazineImage } from '@app/components/mol.magazine-list';
import { Publication } from '@app/modules/magazines/magazines.mapper';
import moment from 'moment';
import { Container } from 'typedi';

export interface MagazinesDetailsPageProps {
  magazine: Publication;
}

@PageView('MagazinesDetails')
export class MagazinesDetailsPage extends React.Component<NavigationPageProps<MagazinesDetailsPageProps>> {
  static options = { name: 'MagazinesDetailsPage' };

  private readonly externalNavigator: ExternalNavigator = Container.get(ExternalNavigator);

  render() {
    return (
      <Root>
        <CollapsibleHeader title={PublicationsStrings.Details.Title}>
          <VBox>
            <HBox>
              <HBox>
                {this.props.navigation.magazine.thumbnail && <MagazineImage image={this.props.navigation.magazine.thumbnail} />}
              </HBox>
              <HBox.Item>
                <VBox>
                  <H1>{this.props.navigation.magazine.name}</H1>
                  <Body align={'justify'}>{this.props.navigation.magazine.description}</Body>
                  <VSeparator />
                  <SecondaryButton text={PublicationsStrings.Details.DonwloadFile} expanded={true} onTap={this.handleButtonTap} />
                </VBox>
              </HBox.Item>
            </HBox>
          </VBox>
          <VBox grow={true}>
            <VSeparator />
            <DividerGray />
            <VSeparator />
            <H2>{PublicationsStrings.Details.MoreInfo.Title}</H2>
            <Body>{`${PublicationsStrings.Details.MoreInfo.PublicationDate}: ${moment(this.props.navigation.magazine.publicationDate).format('DD MMM YYYY')}`}</Body>
            <Body>{`${PublicationsStrings.Details.MoreInfo.Category}: ${this.props.navigation.magazine.publicationType}`}</Body>
          </VBox>
        </CollapsibleHeader>
      </Root>
    );
  }

  private handleButtonTap = async () => {
    const url: string = this.props.navigation.magazine.file.url;
    if (url) {
      await this.externalNavigator.openWebsite(url);
    }
  }
}
