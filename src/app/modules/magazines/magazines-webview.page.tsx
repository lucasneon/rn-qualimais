import * as React from 'react';
import { PageView } from '@app/core/analytics';
import { NavigationPageProps, Navigation } from '@app/core/navigation';
import { WebView } from 'react-native-webview';
import { ActivityIndicator} from 'react-native';
import { TopAreaView } from '@app/components/obj.top-area';

export interface MagazinesDetailsPageProps {
  url: string;
  title: string;
}

const ChatPageLoad: React.SFC<{}> = () => (
    <ActivityIndicator size='large' style={{flex: 1, justifyContent: 'center'}} />
);

@PageView('MagazinesWebView')
export class MagazinesWebViewPage extends React.Component<NavigationPageProps<MagazinesDetailsPageProps>> {
  static options = { name: 'MagazinesWebViewPage' };

  private webview: WebView;

  render() {
    return (
        <>
        <TopAreaView height={Navigation.TopBarHeight} />
        <WebView
          ref={ref => (this.webview = ref)}
          source={{uri: `https://drive.google.com/viewerng/viewer?url=${this.props.navigation.url}`}}
          startInLoadingState={true}
          renderLoading={this.renderLoad}
          javaScriptEnabled={true}
          cacheEnabled={true}
          allowUniversalAccessFromFileURLs={true}
          style={{marginTop: 20, height: 500}}
        />
        </>
    );
  }

  private renderLoad = () => {
    return <ChatPageLoad />;
  }
}
