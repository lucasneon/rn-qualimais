import * as React from 'react';
import { CollapsibleHeader } from '@app/components/org.collapsible-header';
import { PageView } from '@app/core/analytics';
import { GuardProps, Navigation } from '@app/core/navigation';
import { GuardParams } from '@app/modules/authentication';
import { Root, VSeparator } from '@atomic';
import { PublicationsStrings } from '@app/modules/magazines/publications.strings';
import { Magazines } from '@app/modules/magazines/magazines.component';
import { MagazinesDetailsPage } from '@app/modules/magazines';
import { Publication } from '@app/modules/magazines/magazines.mapper';

interface PublicationsPageProps extends GuardProps<GuardParams> {
  componentId: string;
}

@PageView('Publications')
export class PublicationsPage extends React.Component<PublicationsPageProps> {
  static options = { name: 'PublicationsPage', topBar: { backButton: { visible: true } } };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  private handleMagazineTap = (magazine: Publication) => {
    Navigation.push(this.props.componentId, MagazinesDetailsPage, { magazine });
  }

  render() {
    return (
      <CollapsibleHeader
        title={PublicationsStrings.Title}
      >
        <Root>
          <VSeparator />
          <Magazines
            title={PublicationsStrings.Magazines.Title}
            onMagazineTap={this.handleMagazineTap}
            publicationType={'R'}
            notFoundTitle={PublicationsStrings.Magazines.NotFound.Title}
            notFoundMessage={PublicationsStrings.Magazines.NotFound.Message}
          />
          <Magazines
            title={PublicationsStrings.Documents.Title}
            onMagazineTap={this.handleMagazineTap}
            publicationType={'M'}
            notFoundTitle={PublicationsStrings.Documents.NotFound.Title}
            notFoundMessage={PublicationsStrings.Documents.NotFound.Message}
          />
        </Root>
      </CollapsibleHeader>
    );
  }
}
