// IMPORTANT: Do not change import order.
// If you're using any plugin to sort import, remember to disable before saving this file
import 'reflect-metadata';
// Polyfill for Symbol needed on Android and iOS < 9.0
import 'es6-symbol/implement';
import { bootstrapStorybook } from '@app';

bootstrapStorybook();