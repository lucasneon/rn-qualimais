
// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories() {
  require('../app/components/atm.avatar/avatar.component.story');
  require('../app/components/atm.badge/badge.component.story');
  require('../app/components/atm.radio/custom-radio-field.component.story');
  require('../app/components/mol.cell/cell.story');
  require('../app/components/mol.measure-list/measure-list.component.story');
  require('../app/components/mol.radio-badges/radio-badges.story');
  require('../app/components/mol.segmented-control/segmented-control.story');
  require('../app/components/obj.map/map.story');
  require('../app/components/obj.review/review.story');
  require('../app/modules/authentication/forgot-password/forgot-password.story');
  require('../atomic/atm.badge/badge.component.story');
  require('../atomic/atm.button/button.component.story');
  require('../atomic/atm.checkbox/checkbox-field.component.story');
  require('../atomic/atm.divider/divider.component.story');
  require('../atomic/atm.floating-label/floating-label.component.story');
  require('../atomic/atm.picker/picker.component.story');
  require('../atomic/atm.radio/radio-field.component.story');
  require('../atomic/atm.shimmer/shimmer.component.story');
  require('../atomic/atm.text-field/text-field.component.story');
  require('../atomic/atm.typography/typography.component.story');
  require('../atomic/mol.accordion/accordion.component.story');
  require('../atomic/mol.bullet-point-text/bullet-point-text.component.story');
  require('../atomic/mol.calendar/calendar.component.story');
  require('../atomic/mol.carousel/carousel.component.story');
  require('../atomic/mol.cell/cell.story');
  require('../atomic/mol.dl/dl.component.story');
  require('../atomic/mol.image-picker/image-picker.component.story');
  require('../atomic/mol.image-with-button/image-with-button.component.story');
  require('../atomic/mol.modal-image-zoom/modal-image-zoom.story');
  require('../atomic/mol.stepper/stepper.component.story');
  require('../atomic/mol.tile/tile.component.story');
  require('../atomic/obj.action-sheet/action-sheet.component.story');
  require('../atomic/obj.constants/color.story');
  require('../atomic/obj.form/validators/birthdate.story');
  require('../atomic/obj.form/validators/cpf.story');
  require('../atomic/obj.form/validators/email.story');
  require('../atomic/obj.form/validators/equals-value.story');
  require('../atomic/obj.form/validators/exact-length.story');
  require('../atomic/obj.form/validators/has-letter.story');
  require('../atomic/obj.form/validators/has-number.story');
  require('../atomic/obj.form/validators/max-digit-frequency.story');
  require('../atomic/obj.form/validators/max-length.story');
  require('../atomic/obj.form/validators/min-length.story');
  require('../atomic/obj.form/validators/no-sequence.story');
  require('../atomic/obj.form/validators/phone.story');
  require('../atomic/obj.form/validators/required.story');
  require('../atomic/obj.form/validators/zip-code.story');
  require('../atomic/obj.if/if.component.story');
  require('../atomic/obj.loading-state/loading-state.component.story');
  require('../atomic/obj.scroll/scroll.component.story');
  require('../atomic/org.collapsible-header/collapsible-header.component.story');
  
}

const stories = [
  '../app/components/atm.avatar/avatar.component.story',
  '../app/components/atm.badge/badge.component.story',
  '../app/components/atm.radio/custom-radio-field.component.story',
  '../app/components/mol.cell/cell.story',
  '../app/components/mol.measure-list/measure-list.component.story',
  '../app/components/mol.radio-badges/radio-badges.story',
  '../app/components/mol.segmented-control/segmented-control.story',
  '../app/components/obj.map/map.story',
  '../app/components/obj.review/review.story',
  '../app/modules/authentication/forgot-password/forgot-password.story',
  '../atomic/atm.badge/badge.component.story',
  '../atomic/atm.button/button.component.story',
  '../atomic/atm.checkbox/checkbox-field.component.story',
  '../atomic/atm.divider/divider.component.story',
  '../atomic/atm.floating-label/floating-label.component.story',
  '../atomic/atm.picker/picker.component.story',
  '../atomic/atm.radio/radio-field.component.story',
  '../atomic/atm.shimmer/shimmer.component.story',
  '../atomic/atm.text-field/text-field.component.story',
  '../atomic/atm.typography/typography.component.story',
  '../atomic/mol.accordion/accordion.component.story',
  '../atomic/mol.bullet-point-text/bullet-point-text.component.story',
  '../atomic/mol.calendar/calendar.component.story',
  '../atomic/mol.carousel/carousel.component.story',
  '../atomic/mol.cell/cell.story',
  '../atomic/mol.dl/dl.component.story',
  '../atomic/mol.image-picker/image-picker.component.story',
  '../atomic/mol.image-with-button/image-with-button.component.story',
  '../atomic/mol.modal-image-zoom/modal-image-zoom.story',
  '../atomic/mol.stepper/stepper.component.story',
  '../atomic/mol.tile/tile.component.story',
  '../atomic/obj.action-sheet/action-sheet.component.story',
  '../atomic/obj.constants/color.story',
  '../atomic/obj.form/validators/birthdate.story',
  '../atomic/obj.form/validators/cpf.story',
  '../atomic/obj.form/validators/email.story',
  '../atomic/obj.form/validators/equals-value.story',
  '../atomic/obj.form/validators/exact-length.story',
  '../atomic/obj.form/validators/has-letter.story',
  '../atomic/obj.form/validators/has-number.story',
  '../atomic/obj.form/validators/max-digit-frequency.story',
  '../atomic/obj.form/validators/max-length.story',
  '../atomic/obj.form/validators/min-length.story',
  '../atomic/obj.form/validators/no-sequence.story',
  '../atomic/obj.form/validators/phone.story',
  '../atomic/obj.form/validators/required.story',
  '../atomic/obj.form/validators/zip-code.story',
  '../atomic/obj.if/if.component.story',
  '../atomic/obj.loading-state/loading-state.component.story',
  '../atomic/obj.scroll/scroll.component.story',
  '../atomic/org.collapsible-header/collapsible-header.component.story',
  
];

module.exports = {
  loadStories,
  stories,
};
