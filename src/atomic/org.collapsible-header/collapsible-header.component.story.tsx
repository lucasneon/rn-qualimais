import * as React from 'react';

import { storiesOf } from '@storybook/react-native';

import { MockCollapsiblePage } from './mock-collapsible.page';

const stories = storiesOf('Screens', module);

stories.add('Collapsible Page', () => (
  <MockCollapsiblePage/>
));
