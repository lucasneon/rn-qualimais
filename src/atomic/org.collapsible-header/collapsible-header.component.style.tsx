import styled from 'styled-components/native';
import { Animated } from 'react-native';

export const CollapsibleLargeHeaderStyled = styled(Animated.View)`
  opacity: 1;
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
`;

export const CollapsibleSmallHeaderStyled = styled(CollapsibleLargeHeaderStyled)`
  opacity: 0;
`;
