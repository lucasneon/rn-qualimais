import * as React from 'react';
import {Animated, ScrollView, StatusBar, Text, View } from 'react-native';
import { CollapsibleHeader } from './collapsible-header.component';
import { TqSafeAreaView } from '@atomic';

const Content = () => <View style={{height: 100}}><Text>Content</Text></View>;

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);
export class MockCollapsiblePage extends React.Component<any, any> {

  componentDidMount() {
    StatusBar.setBarStyle('default', true);
  }

  render() {
    return (
      <CollapsibleHeader>
        <CollapsibleHeader.LargeHeader>
          <View style={{backgroundColor: 'blue', height: 200, opacity: 0.2}}><Text>Large Header</Text></View>
        </CollapsibleHeader.LargeHeader>
        <CollapsibleHeader.Dock>
          <View style={{backgroundColor: 'yellow', height: 50, opacity: 0.2}}><Text>Docked</Text></View>
        </CollapsibleHeader.Dock>
        <CollapsibleHeader.SmallHeader>
          <View style={{backgroundColor: 'red', height: 50, opacity: 0.2}}>
            <TqSafeAreaView/>
            <Text>Small Header</Text>
          </View>
        </CollapsibleHeader.SmallHeader>
          <AnimatedScrollView style={{backgroundColor: '#ccc'}}>
            <Text>CONTENT START</Text>
            <Content />
            <Content />
            <Content />
            <Content />
            <Content />
            <Content />
            <Content />
          </AnimatedScrollView>
      </CollapsibleHeader>
    );
  }
}
