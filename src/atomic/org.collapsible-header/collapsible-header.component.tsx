import * as React from 'react';
import { Animated, LayoutChangeEvent, SafeAreaView, View } from 'react-native';
import { Platform, Root } from '@atomic';
import { CollapsibleLargeHeaderStyled, CollapsibleSmallHeaderStyled } from './collapsible-header.component.style';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? Platform.getStatusBarHeight() : 0;
const NAVBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 0;

const HeaderPartial: React.SFC<any> = (props: any) => props.children;
const LargeHeader: React.SFC<any> = (props: any) => <HeaderPartial {...props} />;
LargeHeader.displayName = 'LargeHeader';
const SmallHeader: React.SFC<any> = (props: any) => <HeaderPartial {...props} />;
SmallHeader.displayName = 'SmallHeader';
const Dock: React.SFC<any> = (props: any) => <HeaderPartial {...props} />;
Dock.displayName = 'Dock';

interface CollapsibleHeaderState {
  lgHeaderHeight: number;
  smHeaderHeight: number;
  dockHeight: number;
  contentOffset: number;
}

interface CollapsibleHeaderProps {
  // IOS only: Set this to true when adding a translucent Navbar. This component will be behind it.
  topBar?: boolean;
  onScroll?: (event: any) => void;
}

export class CollapsibleHeader extends React.Component<CollapsibleHeaderProps, CollapsibleHeaderState> {

  static LargeHeader = LargeHeader;
  static SmallHeader = SmallHeader;
  static Dock = Dock;

  static defaultProps = {
    topBar: false,
  };

  private scrollAnim: Animated.Value;
  private clampedScroll: Animated.AnimatedDiffClamp;

  constructor(props) {
    super(props);

    this.scrollAnim = new Animated.Value(0);
    this.clampedScroll = this.getClampedScroll(0);
    this.state = {
      lgHeaderHeight: 0,
      smHeaderHeight: 0,
      dockHeight: 0,
      contentOffset: props.topBar ? (NAVBAR_HEIGHT + STATUSBAR_HEIGHT) : STATUSBAR_HEIGHT,
    };
  }

  getClampedScroll(maxHeight: number) {
    return Animated.diffClamp(
      Animated.add(
        this.scrollAnim.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1],
          extrapolateLeft: 'clamp',
        }),
        0,
      ),
      0,
      maxHeight,
    );
  }

  handleLargeHeaderLayout = (e: LayoutChangeEvent) => {
    const lgHeaderHeight = Math.round(e.nativeEvent.layout.height);
    this.clampedScroll = this.getClampedScroll(lgHeaderHeight);
    this.setState({ lgHeaderHeight });
  }
  handleSmallHeaderLayout = (e: LayoutChangeEvent) => {
    this.setState({ smHeaderHeight: Math.round(e.nativeEvent.layout.height) });
  }
  handleDockLayout = (e: LayoutChangeEvent) => {
    this.setState({ dockHeight: Math.round(e.nativeEvent.layout.height) });
  }

  mapChildren() {
    const children = React.Children.map(this.props.children, (child: any) => child)
      .reduce((acc, child) => {
        if (child.type) {
          acc[acc[child.type.displayName] ? child.type.displayName : 'content'].push(child);
        }
        return acc;
      },
        { [LargeHeader.displayName]: [], [SmallHeader.displayName]: [], [Dock.displayName]: [], content: [] });

    children.content[0] = React.cloneElement(children.content[0], {
      contentContainerStyle: {
        paddingTop: (this.state.lgHeaderHeight > 0 ? this.state.lgHeaderHeight : this.state.smHeaderHeight)
          + this.state.dockHeight - this.state.contentOffset,
      },
      scrollEventThrottle: 16,
      onScroll: Animated.event(
        [{ nativeEvent: { contentOffset: { y: this.scrollAnim } } }], {
          useNativeDriver: true,
          listener: this.props.onScroll,
        },
      ),
    });

    return children;
  }

  render() {
    let yTranslation = this.state.lgHeaderHeight - this.state.smHeaderHeight;
    yTranslation = yTranslation <= 0 ? 1 : yTranslation; // yTranslation should not be equal 0 to avoid opacity flicker

    const largeHeaderTranslate = this.clampedScroll.interpolate({
      inputRange: [0, yTranslation],
      outputRange: [0, -(yTranslation)],
      extrapolate: 'clamp',
    });

    const smallHeaderOpacity = this.clampedScroll.interpolate({
      inputRange: [0, yTranslation],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });

    const children = this.mapChildren();
    const translateY = this.state.lgHeaderHeight > 0 ? largeHeaderTranslate : this.state.smHeaderHeight;
    return (
      <Root>
        <SafeAreaView style={{ flex: 1 }}>{children.content}</SafeAreaView>
        <CollapsibleLargeHeaderStyled style={{ transform: [{ translateY }] }}>
          <View onLayout={this.handleLargeHeaderLayout}>{children[LargeHeader.displayName]}</View>
          <View onLayout={this.handleDockLayout}>{children[Dock.displayName]}</View>
        </CollapsibleLargeHeaderStyled>
        <CollapsibleSmallHeaderStyled style={{ opacity: smallHeaderOpacity }}>
          <View
            onLayout={this.handleSmallHeaderLayout}
          >{children[SmallHeader.displayName]}
          </View>
        </CollapsibleSmallHeaderStyled>
      </Root>
    );
  }
}
