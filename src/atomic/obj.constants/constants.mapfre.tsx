export enum BrandColor {
  RedMapfre = '#e30909',
  RedMapfreLight = '#eb1313',
  RedMapfreDark = '#a31d1d',
  LightGreen = '#1BA39C',
  YellowishGreen = '#fa3232',
  Red = '#9c0000',
  WineRed = '#b20d15',
  Warning = '#e90000',
  GradientCell = '#9e0303',
}

// We used class rather than enums since enums don't support computed values
export class Color {
  public static readonly Primary = BrandColor.RedMapfre;
  public static readonly PrimaryDisabled = '#b81d1d'; // 40% opacity
  public static readonly CallToAction = BrandColor.RedMapfreDark;
  public static readonly Alert = BrandColor.WineRed;
  public static readonly Warning = BrandColor.Warning;
  public static readonly Success = BrandColor.LightGreen;
  public static readonly Secondary = BrandColor.RedMapfreLight;
  public static readonly Neutral = 'gray';
  public static readonly Accessory = BrandColor.RedMapfre;
  public static readonly GradientCell = BrandColor.GradientCell;

  public static readonly Facebook = '#983b3b';
  public static readonly Transparent = 'transparent';

  public static readonly Black = 'black';
  public static readonly White = 'white';

  public static readonly GrayXLight = '#F8F8F8';
  public static readonly GrayLight = '#DEDEDE';
  public static readonly Gray = '#757575';
  public static readonly GrayDark = '#525252';
  public static readonly GrayXDark = '#333333';
  public static readonly GrayXDarkTransparent = Color.GrayXDark + 'a0';

  public static readonly StarHighlightedColor = '#ff0000';
}

export const FontFamily = {
  Primary: {
    Regular: 'Rubik-Regular',
    Medium: 'Rubik-Medium',
    Light: 'Rubik-Light',
    Bold: 'Rubik-Bold',
  },
  Secondary: {
    Regular: 'Montserrat-Regular',
    Semibold: 'Montserrat-SemiBold',
    Bold: 'Montserrat-Bold',
  },
};

export enum FontSize {
  XSmall = 12,
  Small = 14,
  Medium = 16,
  Large = 18,
  XLarge = 24,
}

export enum Spacing {
  XSmall = 4,
  Small = 8,
  Medium = 12,
  Large = 16,
  XLarge = 24,
}

export enum IconSize {
  Small = 16,
  Medium = 24,
  Large = 32,
}

export enum FormsConstants {
  FieldHeight = 50,
}

export const AnimationDuration = 300;
export const ScreenUnit = 'px';
export const DebounceDuration = 1000;

export enum Opacity {
  Disabled = 0.4,
  Active = 0.7,
}

export enum AvatarSize {
  Small = 36,
  Big = 52,
}

// tslint:disable-next-line:max-classes-per-file
export class Border {
  public static readonly Color = Color.GrayLight;
  public static readonly Radius = 2;
  public static readonly RadiusLarge = 16;
  public static readonly Width = 1;
}
