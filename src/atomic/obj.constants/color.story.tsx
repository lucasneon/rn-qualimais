import * as React from 'react';

import { HBox, VBox, VSeparator } from '@atomic/obj.grid';
import { Color, Spacing } from '@atomic/obj.constants';
import { Scroll } from '@atomic/obj.scroll';

import { H2 } from '@atomic/atm.typography';
import { storiesOf } from '@storybook/react-native';
import styled from 'styled-components/native';

const ColorSample: any = styled.View`
  height: 60px;
  align-self: stretch;
  justify-content: center;
  align-items: center;
`;

const ColorSampleText: any = styled.Text`
  margin-top: ${Spacing.XSmall};
  align-self: stretch;
  text-align: center;
`;

const stories = storiesOf('Objects', module);

stories.add('Colors', () => (
  <Scroll>
    <VBox>
      <H2>Brand colors</H2>
    </VBox>
    <HBox>
      <HBox.Item>
        <ColorSample backgroundColor={Color.Primary} />
        <ColorSampleText>Primary - {Color.Primary}</ColorSampleText>
      </HBox.Item>
      <HBox.Item>
        <ColorSample backgroundColor={Color.CallToAction} />
        <ColorSampleText>Call to action - {Color.CallToAction}</ColorSampleText>
      </HBox.Item>
    </HBox>
    <HBox>
      <HBox.Item>
        <ColorSample backgroundColor={Color.Warning} />
        <ColorSampleText>Warning - {Color.Warning}</ColorSampleText>
      </HBox.Item>
      <HBox.Item>
        <ColorSample backgroundColor={Color.Success} />
        <ColorSampleText>Success - {Color.Warning}</ColorSampleText>
      </HBox.Item>
    </HBox>
    <HBox>
      <HBox.Item>
        <ColorSample backgroundColor={Color.Alert} />
        <ColorSampleText>Alert - {Color.CallToAction}</ColorSampleText>
      </HBox.Item>
    </HBox>
    <VSeparator />
    <VBox>
      <H2>Gray shades</H2>
    </VBox>
    <HBox>
      <HBox.Item>
        <ColorSample backgroundColor={Color.GrayXLight} />
        <ColorSampleText>GrayXLight - {Color.GrayXLight}</ColorSampleText>
      </HBox.Item>
      <HBox.Item>
        <ColorSample backgroundColor={Color.GrayLight} />
        <ColorSampleText>GrayLight - {Color.GrayLight}</ColorSampleText>
      </HBox.Item>
    </HBox>
    <HBox>
      <HBox.Item>
        <ColorSample backgroundColor={Color.Gray} />
        <ColorSampleText>Gray - {Color.Gray}</ColorSampleText>
      </HBox.Item>
      <HBox.Item>
        <ColorSample backgroundColor={Color.GrayDark} />
        <ColorSampleText>GrayDark - {Color.GrayDark}</ColorSampleText>
      </HBox.Item>
    </HBox>
    <HBox>
      <HBox.Item>
        <ColorSample backgroundColor={Color.GrayXDark} />
        <ColorSampleText>GrayXDark - {Color.GrayXDark}</ColorSampleText>
      </HBox.Item>
    </HBox>
    <VSeparator />
    <VBox>
      <H2>Black and white</H2>
    </VBox>
    <HBox>
      <HBox.Item>
        <ColorSample backgroundColor={Color.Black} />
        <ColorSampleText>Black</ColorSampleText>
      </HBox.Item>
      <HBox.Item>
        <ColorSample backgroundColor={Color.White} />
        <ColorSampleText>White</ColorSampleText>
      </HBox.Item>
    </HBox>
  </Scroll>
));
