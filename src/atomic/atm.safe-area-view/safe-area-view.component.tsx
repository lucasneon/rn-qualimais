import * as React from 'react';
import { Platform as ReactPlatform, View } from 'react-native';
import { Platform } from '@atomic/obj.platform';

// TODO Fir different behavior of status bar height at ios 10 and ios 11
// Check https://github.com/facebook/react-native/issues/17638

export const TqSafeAreaView = () => {
  // Typing is not right: it is a string, e.g. '10.3.1'
  const marginTop = ReactPlatform.select({
    ios: Platform.getStatusBarHeight(),
    android: 20,
  });

  return (
    <View style={{ marginTop }} />
  );
};
