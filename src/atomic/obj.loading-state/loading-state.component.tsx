import * as React from 'react';
import * as Animatable from 'react-native-animatable';

const EmptyState: React.SFC<any> = (props: any) => props.children;
EmptyState.displayName = 'EmptyState';
const ErrorPlaceholder: React.SFC<any> = (props: any) => props.children;
ErrorPlaceholder.displayName = 'ErrorPlaceholder';
const Shimmer: React.SFC<any> = (props: any) => props.children;
Shimmer.displayName = 'Shimmer';

interface LoadingStateProps {
  data: boolean;
  loading: boolean;
  error: boolean;
  onError?: () => any;
}
/* tslint:disable:no-bitwise */
const STATE_DATA = 1 << 2;
const STATE_LOADING = 1 << 1;
const STATE_ERROR = 1;

export class LoadingState extends React.Component<LoadingStateProps, undefined> {

  static readonly EmptyState = EmptyState;
  static readonly ErrorPlaceholder = ErrorPlaceholder;
  static readonly Shimmer = Shimmer;

  private animatable;

  componentDidUpdate(prevProps: LoadingStateProps) {
    if (!prevProps.data && this.props.data || !prevProps.data && !this.props.data) {
      this.animatable.startAnimation();
    }
  }

  mapChildren(data: boolean, loading: boolean, error: boolean) {
    const children = React.Children.map(this.props.children, (child: any) => child)
    .reduce((acc, child) => {
      if (child.type) {
        acc[acc[child.type.displayName] ? child.type.displayName : 'data'].push(child);
      }
      return acc;
    },
    { [EmptyState.displayName]: [], [ErrorPlaceholder.displayName]: [], [Shimmer.displayName]: [], data: []});

    const childrenState = {
      0: children[EmptyState.displayName],
      [STATE_ERROR]: children[ErrorPlaceholder.displayName],
      [STATE_LOADING]: children[Shimmer.displayName],
      [STATE_LOADING | STATE_ERROR]: children[Shimmer.displayName],
      [STATE_DATA]: children.data,
      [STATE_DATA | STATE_ERROR]: children.data,
      [STATE_DATA | STATE_LOADING]: children.data,
      [STATE_DATA | STATE_LOADING | STATE_ERROR]: children.data,
    };

    return childrenState[Number(data) << 2 | Number(loading) << 1 | Number(error)];
  }

  render() {
    const children = this.mapChildren(this.props.data, this.props.loading, this.props.error);
    return (
      <Animatable.View animation='fadeIn' duration={500} ref={ref => this.animatable = ref}>
        {children}
      </Animatable.View>
    );
  }
}

/* tslint:enable:no-bitwise */
