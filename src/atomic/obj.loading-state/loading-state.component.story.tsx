import * as React from 'react';

import { VBox, VSeparator } from '@atomic/obj.grid';

import { H1, H2 } from '@atomic/atm.typography';
import { storiesOf } from '@storybook/react-native';
import { CheckboxField } from '@atomic/atm.checkbox';
import { LoadingState } from '@atomic/obj.loading-state/loading-state.component';
interface LoadingStatePageState {
  data?: boolean;
  loading?: boolean;
  error?: boolean;
}
class LoadingStatePage extends React.Component<any, LoadingStatePageState> {
  constructor(props) {
    super(props);
    this.state = {
      data: false,
      loading: false,
      error: false,
    };
  }
  handleStateChange = (value: string, checked: boolean) => {
    this.setState({[value]: checked});
  }
  render() {
    return (
      <VBox>
        <H2>Loading state simulation</H2>
        <CheckboxField id='error' onValueChange={this.handleStateChange}>Error</CheckboxField>
        <CheckboxField id='loading'  onValueChange={this.handleStateChange}>Loading</CheckboxField>
        <CheckboxField id='data' onValueChange={this.handleStateChange}>Has data</CheckboxField>
        <VSeparator />
        <LoadingState loading={this.state.loading} data={this.state.data} error={this.state.error}>
          <LoadingState.Shimmer><H1>Showing shimmer</H1></LoadingState.Shimmer>
          <LoadingState.ErrorPlaceholder><H1>Error Placeholder</H1></LoadingState.ErrorPlaceholder>
          <LoadingState.EmptyState><H1>Empty state</H1></LoadingState.EmptyState>
          <H1>This is the data</H1>
        </LoadingState>
      </VBox>
    );
  }
}

const stories = storiesOf('Objects', module);

stories.add('Loading state', () => (
  <LoadingStatePage />
));
