import * as React from 'react';
import { ButtonBackgroundStyled, ButtonContentStyled, ButtonStyled } from './image-with-button.component.style';
import { Image } from 'react-native';
import { LinkButton } from '@atomic/atm.button';
import { Asset } from '@atomic/obj.asset';

export interface ImageWithButtonProps {
  image?: any;
  buttonText: string;
  onTap?: () => any;
}

export class ImageWithButton extends React.Component<ImageWithButtonProps, any> {
  constructor(props: ImageWithButtonProps) {
    super(props);
  }

  render() {
    return (
      <ButtonStyled>
        <ButtonBackgroundStyled source={null}>
          <Image source={Asset.Icon.Atomic.Photo}/>
        </ButtonBackgroundStyled>
        <ButtonContentStyled>
          <LinkButton text={this.props.buttonText} />
        </ButtonContentStyled>
      </ButtonStyled>
    );
  }

}
