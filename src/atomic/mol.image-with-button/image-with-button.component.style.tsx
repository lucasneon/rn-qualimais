import styled from 'styled-components/native';
import { Color, ScreenUnit, Spacing } from '@atomic/obj.constants';
import { ImageBackground } from 'react-native';

export const ButtonStyled = styled.TouchableOpacity`
  padding: ${Spacing.XLarge + ScreenUnit};
  align-items: center;
  width: 50%;
`;

export const ButtonBackgroundStyled = styled(ImageBackground)`
  margin-left: ${Spacing.Large};
  margin-right: ${Spacing.Large};
  background-color: ${Color.GrayXLight};
  border-width: 1;
  border-radius: 2;
  border-color: ${Color.Gray};
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 80;
`;

export const ButtonContentStyled = styled.View`
  align-self: center;
`;
