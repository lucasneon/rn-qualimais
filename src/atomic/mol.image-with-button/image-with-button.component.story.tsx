import * as React from 'react';

import { VBox } from '@atomic/obj.grid';

import { H2 } from '@atomic/atm.typography';
import { ImageWithButton } from '@atomic/mol.image-with-button';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Molecules', module);

stories.add('Image with button', () => (
  <VBox>
    <H2>Image with button</H2>
    <ImageWithButton buttonText='Text' />
  </VBox>
));
