import * as React from 'react';
import { Bullet, BulletProps } from '@atomic/atm.bullet';
import { Caption } from '@atomic/atm.typography';
import { Color } from '@atomic/obj.constants';
import { StepperBulletOutlineStyled, StepperOverlayStyled, StepperStyled } from './stepper-step.component.style';

export enum StepProgress {
  Past,
  Current,
  Future,
}

export interface StepperStepProps {
  bulletProps: BulletProps;
  text: string;
  progress: StepProgress;
  mr?: boolean;
}

export class StepperStep extends React.Component<StepperStepProps, any> {
  render() {
    return (
      <StepperStyled {...this.props}>
        <StepperBulletOutlineStyled {...this.props}>
          <Bullet
            bulletNumber={this.props.bulletProps.bulletNumber}
            bulletBackground={Color.GrayXDark}
            isChecked={this.props.progress === StepProgress.Past}
          />
        </StepperBulletOutlineStyled>
        <Caption>{this.props.text}</Caption>
        {this.props.progress === StepProgress.Future ? <StepperOverlayStyled /> : null}
      </StepperStyled>
    );
  }
}
