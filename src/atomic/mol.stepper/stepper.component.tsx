import * as React from 'react';

import { HBox } from '@atomic';
import { StepperDividerDarkStyled, StepperDividerLightStyled } from './stepper-divider.component.style';

import { StepProgress } from './stepper-step.component';
import { StepperStep } from './stepper-step.component';

export interface StepperProps {
  currentStep: number;
  firstStep?: number;
}

const StepperItem = (_props: {label: string}) => {
  return null;
};

export class Stepper extends React.Component<StepperProps, any> {
  static Item = StepperItem;

  render() {
    const firstStep = this.props.firstStep !== undefined ? this.props.firstStep : 1;
    const stepElements = [];

    React.Children.forEach(this.props.children, (child: any, i: number) => {
      const index = i + firstStep;
      const stepKey = 'Stepper_' + i;
      const dividerKey = 'Divider_' + i;

      if (index > firstStep) {
        if (this.props.currentStep < index) {
          stepElements.push(<StepperDividerLightStyled key={dividerKey}/>);
        } else {
          stepElements.push(<StepperDividerDarkStyled key={dividerKey}/>);
        }
      }

      stepElements.push(
        <StepperStep
          key={stepKey}
          bulletProps={{ bulletNumber: index.toString(), bulletBackground: null }}
          text={child.props.label}
          progress={this.getStepProgress(index)}
          mr={index === firstStep}
        />,
      );

    });

    return (
      <HBox hAlign='center' noGutter={true}>
        {stepElements}
      </HBox>
    );
  }

  getStepProgress = (stepIndex: number): StepProgress => {
    if (stepIndex < this.props.currentStep) {
      return StepProgress.Past;
    } else if (stepIndex === this.props.currentStep) {
      return StepProgress.Current;
    } else if (stepIndex > this.props.currentStep) {
      return StepProgress.Future;
    }

    return null;
  }
}
