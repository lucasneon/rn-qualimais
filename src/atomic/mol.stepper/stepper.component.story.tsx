import * as React from 'react';

import { VBox } from '@atomic/obj.grid';

import { H2 } from '@atomic/atm.typography';
import { Stepper } from '@atomic/mol.stepper';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Molecules', module);

stories.add('Stepper', () => (
  <VBox>
    <H2>Stepper</H2>
    <Stepper currentStep={2}>
      <Stepper.Item label='Dados'/>
      <Stepper.Item label='Documento'/>
      <Stepper.Item label='Comprovante'/>
    </Stepper>
  </VBox>
));
