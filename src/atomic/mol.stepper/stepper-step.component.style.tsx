import styled from 'styled-components/native';
import { Border, Color, Spacing } from '@atomic/obj.constants';
import { StepperStepProps } from './stepper-step.component';
import { StepProgress } from './stepper-step.component';

const BulletOutlineWidth: number = 32;

export const StepperStyled = styled.View`
  align-items: center;
  margin-right: ${(props: StepperStepProps) => props.mr ? 8 : 0};
`;

export const StepperOverlayStyled = styled.View`
  background-color: 'rgba(255, 255, 255, 0.6)';
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
`;

export const StepperBulletOutlineStyled = styled.View`
  width: ${BulletOutlineWidth};
  height: ${BulletOutlineWidth};
  border-radius: ${BulletOutlineWidth / 2};
  border-width: ${(props: StepperStepProps) => props.progress === StepProgress.Current ? Border.Width : 0};
  border-style: dashed;
  border-color: ${Color.GrayXDark};
  justify-content: center;
  align-items: center;
  margin-bottom: ${Spacing.Small};
`;
