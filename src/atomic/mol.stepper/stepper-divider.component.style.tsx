import { Color } from '@atomic/obj.constants';
import styled from 'styled-components/native';

export const StepperDividerLightStyled = styled.View`
  margin-top: 12;
  height: 1;
  width: 15%;
  background-color: ${Color.GrayLight};
`;

export const StepperDividerDarkStyled = styled.View`
  margin-top: 12;
  height: 1;
  width: 15%;
  background-color: ${Color.GrayXDark};
`;
