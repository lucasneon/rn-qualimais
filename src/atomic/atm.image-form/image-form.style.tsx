import { Image } from 'react-native';
import styled from 'styled-components/native';

// import { FormsConstants } from '@atomic/obj.constants';

export const ImageFormStyled = styled(Image)`
  resize-mode: contain;
`;
