import * as React from 'react';
import {
  GestureResponderEvent,
  PanResponder,
  PanResponderGestureState,
  PanResponderInstance,
  View
} from 'react-native';

export enum SwipeDirection {
  SWIPE_UP = 'SWIPE_UP',
  SWIPE_DOWN = 'SWIPE_DOWN',
  SWIPE_LEFT = 'SWIPE_LEFT',
  SWIPE_RIGHT = 'SWIPE_RIGHT',
}

const swipeConfig = {
  velocityThreshold: 0.3,
  directionalOffsetThreshold: 80,
};

function isValidSwipe(velocity, velocityThreshold, directionalOffset, directionalOffsetThreshold) {
  return Math.abs(velocity) > velocityThreshold && Math.abs(directionalOffset) < directionalOffsetThreshold;
}
interface SwiperProps {
  onSwipe?: (direction: SwipeDirection, gestureState: PanResponderGestureState) => void;
  onSwipeDown?: (gestureState: PanResponderGestureState) => void;
  onSwipeLeft?: (gestureState: PanResponderGestureState) => void;
  onSwipeRight?: (gestureState: PanResponderGestureState) => void;
  onSwipeUp?: (gestureState: PanResponderGestureState) => void;
}
export class Swiper extends React.Component<SwiperProps, any> {
  private panResponder: PanResponderInstance;

  constructor(props, context) {
    super(props, context);

    this.panResponder = PanResponder.create({ // stop JS beautify collapse
      onStartShouldSetPanResponder: this.handleShouldSetPanResponder,
      onMoveShouldSetPanResponder: this.handleShouldSetPanResponder,
      onPanResponderRelease: this.handlePanResponderEnd,
      onPanResponderTerminate: this.handlePanResponderEnd,
    });
  }

  _gestureIsClick(gestureState: PanResponderGestureState) {
    return Math.abs(gestureState.dx) < 5  && Math.abs(gestureState.dy) < 5;
  }

  _isValidHorizontalSwipe(gestureState: PanResponderGestureState) {
    const {vx, dy} = gestureState;
    const {velocityThreshold, directionalOffsetThreshold} = swipeConfig;
    return isValidSwipe(vx, velocityThreshold, dy, directionalOffsetThreshold);
  }

  _isValidVerticalSwipe(gestureState: PanResponderGestureState) {
    const {vy, dx} = gestureState;
    const {velocityThreshold, directionalOffsetThreshold} = swipeConfig;
    return isValidSwipe(vy, velocityThreshold, dx, directionalOffsetThreshold);
  }

  render() {
    return (<View {...this.props} {...this.panResponder.panHandlers}/>);
  }

  private getSwipeDirection(gestureState: PanResponderGestureState) {
    const {dx, dy} = gestureState;
    if (this._isValidHorizontalSwipe(gestureState)) {
      return (dx > 0)
        ? SwipeDirection.SWIPE_RIGHT
        : SwipeDirection.SWIPE_LEFT;
    } else if (this._isValidVerticalSwipe(gestureState)) {
      return (dy > 0)
        ? SwipeDirection.SWIPE_DOWN
        : SwipeDirection.SWIPE_UP;
    }
    return null;
  }

  private triggerSwipeHandlers(direction: SwipeDirection, gestureState: PanResponderGestureState) {
    const {onSwipe, onSwipeUp, onSwipeDown, onSwipeLeft, onSwipeRight} = this.props;
    if (onSwipe) {
      onSwipe(direction, gestureState);
    }
    switch (direction) {
      case SwipeDirection.SWIPE_LEFT:
        if (onSwipeLeft) {
          onSwipeLeft(gestureState);
        }
        break;
      case SwipeDirection.SWIPE_RIGHT:
        if (onSwipeRight) {
          onSwipeRight(gestureState);
        }
        break;
      case SwipeDirection.SWIPE_UP:
        if (onSwipeUp) {
          onSwipeUp(gestureState);
        }
        break;
      case SwipeDirection.SWIPE_DOWN:
        if (onSwipeDown) {
          onSwipeDown(gestureState);
        }
        break;
    }
  }

  private handlePanResponderEnd = (_evt: GestureResponderEvent, gestureState: PanResponderGestureState) => {
    const swipeDirection = this.getSwipeDirection(gestureState);
    this.triggerSwipeHandlers(swipeDirection, gestureState);
  }

  private handleShouldSetPanResponder = (evt: GestureResponderEvent, gestureState: PanResponderGestureState) => {
    return evt.nativeEvent.touches.length === 1 && !this._gestureIsClick(gestureState);
  }

}
