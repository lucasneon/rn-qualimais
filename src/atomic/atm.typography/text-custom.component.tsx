import * as React from 'react';

import { Text } from 'react-native';
import { TextCustomProps } from './text-custom.component';
import { ReactNode } from 'react';

export interface TextCustomProps extends Readonly<{ children?: ReactNode }> {
  style?: any;
}

export function TextCustomTemplate(transform: (text: string) => string) {
  return (props: TextCustomProps) => {
    const wrapped = React.Children.map(props.children, (child: any) => {
      return (typeof child === 'string') ? transform(child) : child;
    });

    return (<Text style={props.style}>{wrapped}</Text>);
  };
}

export const TextUpperCase = TextCustomTemplate((text: string): string => text.toUpperCase());
export const TextLowerCase = TextCustomTemplate((text: string): string => text.toLowerCase());
