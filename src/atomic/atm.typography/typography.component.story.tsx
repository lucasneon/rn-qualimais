import * as React from 'react';
import {
  Body,
  Caption,
  DD,
  DT,
  H1,
  H2,
  H3,
  H4,
  HDisplay,
  InputDisabled,
  InputLabel,
  InputPlaceholder,
  InputValue
  } from './';
import { storiesOf } from '@storybook/react-native';
import { VBox, VSeparator } from '@atomic/obj.grid';

const stories = storiesOf('Atoms', module);

stories.add('Typography', () => (
  <VBox>
    <HDisplay>This is a heading display</HDisplay>
    <H1>This is the heading 1 style</H1>
    <H2>This is the heading 2 style</H2>
    <H3>This is the heading 3 style</H3>
    <H4>This is the heading 4 style</H4>
    <VSeparator />
    <Body>This is the body paragraph style. Testing component with two lines of text.</Body>
    <Body>This is the second line</Body>
    <VSeparator />
    <Caption>This is the caption style. Testing component with two lines of text.</Caption>
    <VSeparator />
    <InputValue>This is the input value style.</InputValue>
    <VSeparator />
    <InputDisabled>This is the input disabled style.</InputDisabled>
    <VSeparator />
    <InputLabel>This is the input label style.</InputLabel>
    <VSeparator />
    <InputPlaceholder>This is the input placeholder style.</InputPlaceholder>
    <VSeparator />
    <DT>This is the description title style.</DT>
    <VSeparator />
    <DD>This is the description description style.</DD>
  </VBox>
));
