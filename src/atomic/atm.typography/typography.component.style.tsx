import styled from 'styled-components/native';
import { Color, FontFamily, FontSize, Spacing } from '@atomic/obj.constants';
import { TextUpperCase } from './text-custom.component';

export const HDisplay = styled(TextUpperCase)`
  font-family: ${FontFamily.Secondary.Regular};
  color: ${Color.TitleColorText};
  font-size: ${FontSize.XLarge};
  line-height: 36;
`;

export const H1 = styled.Text`
  font-family: ${FontFamily.Secondary.Bold};
  color: ${Color.TitleColorText};
  font-size: ${FontSize.Large};
  margin-bottom: ${Spacing.Small};
  margin-top: ${Spacing.Small};
`;

export const H1Large = styled(H1)`
  font-size: ${FontSize.XLarge};
  margin-bottom: ${Spacing.Medium};
`;

export const H1LargeWhite = styled(H1Large)`
  color: ${Color.White};
`;

export const H2 = styled.Text`
  text-align: center;
  font-family: ${FontFamily.Secondary.Regular};
  color: ${Color.TitleColorText};
  font-size: ${FontSize.Large};
  margin-bottom: ${Spacing.Medium};
  margin-top: ${Spacing.Medium};
`;

export const H3 = styled.Text`
  font-family: ${FontFamily.Primary.Regular};
  line-height: ${Spacing.XLarge};
  font-size: ${FontSize.Medium};
  color: ${Color.TitleColorText};
`;

export const H3Gray = styled.Text`
  font-family: ${FontFamily.Primary.Regular};
  line-height: ${Spacing.XLarge};
  font-size: ${FontSize.Medium};
  color: ${Color.Gray};
`;

export const H4 = styled.Text`
  font-family: ${FontFamily.Secondary.Semibold};
  color: ${Color.TitleColorText};
  font-size: ${FontSize.Small};
  margin-bottom: ${Spacing.XSmall};
`;

interface BodyProps {
  align?: string;
  white?: boolean;
}

export const Body = styled.Text`
  color: ${Color.GrayDark};
  font-size: ${FontSize.Small};
  line-height: 24;
  font-family: ${FontFamily.Primary.Regular};
  text-align: ${(props: BodyProps) => (props.align ? props.align : 'left')};
`;

export const BodyWhite = styled(Body)`
  color: ${Color.White};
`;

export const BodyWhite_ = styled(Body)`
  color: ${Color.White};
  font-size: ${FontSize.XSmall};
  text-align: ${(props: BodyProps) => (props.align ? props.align : 'left')};
  line-height: 17;
`;

export const BodyWhiteBold_ = styled(Body)`
  color: ${Color.White};
  font-size: ${FontSize.XSmall};
  text-align: ${(props: BodyProps) => (props.align ? props.align : 'left')};
  line-height: 17;
  font-weight: bold;
`;

export const ErrorBody = styled(Body)`
  color: ${Color.Alert};
`;

export const Caption = styled.Text`
  color: ${Color.GrayDark};
  font-size: ${FontSize.XSmall};
  align-self: stretch;
  right: -${props => (props.inList ? 11 * Spacing.XLarge : '0')};
  top: ${props => (props.inList ? Spacing.Large : '0')};
`;

export const CaptionLight = styled(Caption)`
  color: ${Color.GrayLight};
`;

interface InputLabelProps {
  hasError?: boolean;
}

export const InputLabel = styled.Text`
  color: ${(props: InputLabelProps) =>
    props.hasError ? Color.Alert : Color.GrayXDark};
  font-size: 16;
  font-weight: bold;
  text-transform: capitalize;
  margin-bottom: ${Spacing.XSmall};
`;

export const InputLabelLight = styled(InputLabel)`
  color: ${Color.GrayLight};
`;

export const InputValue = styled.Text`
  color: ${Color.TitleColorText};
  font-size: ${FontSize.Small};
`;

export const InputDisabled = styled.Text`
  color: ${Color.Gray};
  font-size: ${FontSize.Small};
`;

export const InputCaption = styled.Text`
  color: ${Color.GrayXDark};
  font-size: ${FontSize.XSmall};
  margin-top: ${Spacing.XSmall};
  align-self: stretch;
`;

export const InputCaptionError = styled(InputCaption)`
  color: ${Color.Alert};
`;

export const InputPlaceholder = styled.Text`
  color: ${Color.GrayDark};
  font-size: ${FontSize.Small};
`;

export const DD = styled.Text`
  color: ${Color.GrayXDark};
  font-size: ${FontSize.Small};
`;

export const DT = styled.Text`
  color: ${Color.TitleColorText};
  font-size: ${FontSize.Small};
`;
