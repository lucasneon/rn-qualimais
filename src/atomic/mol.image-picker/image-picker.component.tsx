import * as React from 'react';
import { Image, ImageURISource, View } from 'react-native';
import RNImagePicker from 'react-native-image-picker';
import { ModalImageZoom } from '@atomic/mol.modal-image-zoom';
import { Asset } from '@atomic/obj.asset';
import {
  ImagePickerCaptionContentStyled, ImagePickerClearBtnStyled, ImagePickerContentStyled, ImagePickerPlaceholderStyled,
  ImagePickerWrapperStyled
} from './image-picker.component.style';
import { strings } from './image-picker.strings';

export interface ImagePickerProps {
  caption: string;
  onValueChange?: (newValue: ImageURISource) => void;
}

interface ImagePickerState {
  value: ImageURISource;
  isModalVisible: boolean;
}

export class ImagePicker extends React.Component<ImagePickerProps, ImagePickerState> {

  private options = {
    title: strings.actionSheet.title,
    takePhotoButtonTitle: strings.actionSheet.fromCamera,
    chooseFromLibraryButtonTitle: strings.actionSheet.fromLibrary,
    cancelButtonTitle: strings.actionSheet.cancel,
    permissionDenied: { // Android Only
      title: strings.permissionDeniedAlert.title,
      text: strings.permissionDeniedAlert.text,
    },
  };

  componentWillMount() {
    this.setState({isModalVisible: false});
  }

  render() {
    return this.state.value ? <this.ImagePickerWithContent /> : <this.ImagePickerPlaceholder />;
  }

  private ImagePickerPlaceholder = () => {
    return (
      <View>
        <ImagePickerWrapperStyled onPress={this.onImageTap}>
          <ImagePickerPlaceholderStyled>
            <Image source={Asset.Icon.Atomic.Photo}/>
          </ImagePickerPlaceholderStyled>
        </ImagePickerWrapperStyled>
        <ImagePickerCaptionContentStyled>{this.props.caption}</ImagePickerCaptionContentStyled>
      </View>
    );
  }

  private ImagePickerWithContent = () => {
    return (
      <View>
        <ModalImageZoom
          image={this.state.value}
          isVisible={this.state.isModalVisible}
          onRequestClose={this.closeModal}
        />
        <ImagePickerWrapperStyled onPress={this.openModal}>
          <ImagePickerContentStyled source={this.state.value} />
          <ImagePickerClearBtnStyled onPress={this.onClearImageTap}>
            <Image source={Asset.Icon.Atomic.Clear} />
          </ImagePickerClearBtnStyled>
        </ImagePickerWrapperStyled>
        <ImagePickerCaptionContentStyled>{this.props.caption}</ImagePickerCaptionContentStyled>
      </View>
    );
  }

  private onImageTap = () => {
    RNImagePicker.showImagePicker(this.options, response => {
      if (response.didCancel) {
        console.warn('User cancelled image picker');
      } else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      } else {
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        const image: ImageURISource = { uri: response.uri };

        this.updateImage(image);
      }
    });
  }

  private onClearImageTap = () => {
    this.updateImage(null);
  }

  private openModal = () => {
    this.setState({isModalVisible: true});
  }

  private closeModal = () => {
    this.setState({isModalVisible: false});
  }

  private updateImage = (value: ImageURISource) => {
    this.setState({value});

    if (this.props.onValueChange) {
      this.props.onValueChange(value);
    }
  }

}
