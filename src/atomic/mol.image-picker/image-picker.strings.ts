export const strings = {
  actionSheet: {
    title: 'Selecionar foto',
    fromCamera: 'Camera',
    fromLibrary: 'Galeria',
    cancel: 'Cancelar',
  },
  permissionDeniedAlert: {
    title: 'Permissão negada',
    text: 'To be able to take pictures with your camera and choose images from your library',
  },
};
