import { Border, Color, FontSize, ScreenUnit, Spacing } from '@atomic/obj.constants';

import styled from 'styled-components/native';

export const ImagePickerWrapperStyled = styled.TouchableOpacity`
  padding: ${Spacing.Medium + ScreenUnit};
  align-self: center;
  align-items: center;
`;

export const ImagePickerPlaceholderStyled = styled.View`
  background-color: ${Color.GrayXLight};
  border-width: ${Border.Width};
  border-radius: ${Border.Radius};
  border-color: ${Color.GrayLight};
  justify-content: center;
  align-items: center;
  width: 80;
  height: 104;
`;

export const ImagePickerContentStyled = styled.Image`
  background-color: ${Color.GrayXLight};
  border-width: ${Border.Width};
  border-radius: ${Border.Radius};
  border-color: ${Color.GrayLight};
  justify-content: center;
  align-items: center;
  width: 80;
  height: 104;
`;

export const ImagePickerCaptionContentStyled = styled.Text`
  align-self: center;
  color: ${Color.Primary};
  text-align: center;
  font-size: ${FontSize.Small};
`;

export const ImagePickerClearBtnStyled = styled.TouchableOpacity`
  position: absolute;
  right: 0;
  top: 0;
`;
