import * as React from 'react';

import { VBox } from '@atomic/obj.grid';

import { H2 } from '@atomic/atm.typography';
import { ImagePicker } from './image-picker.component';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Form', module);

stories.add('Image Picker', () => (
  <VBox>
    <H2>Image Picker</H2>
    <ImagePicker
      caption='CPF'
    />
  </VBox>
));
