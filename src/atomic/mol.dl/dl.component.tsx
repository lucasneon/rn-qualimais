import * as React from 'react';
import { DDStyled, DLStyled, DTStyled } from './dl.component.style';

export interface DLProps {
  inline?: boolean;
}

export interface DDProps {
  inline?: boolean;
  isLarge?: boolean;
}

export class DL extends React.PureComponent<DLProps, any> {

  public static DT = DTStyled;
  public static DD = DDStyled;

  public static defaultProps: Partial<DLProps> = {
    inline: false,
  };

  render() {
    return (
      <DLStyled {...this.props}>
        {this.getChildren()}
      </DLStyled>
    );
  }

  private getChildren() {
    const { inline } = this.props;

    return React.Children.map(this.props.children, (child: any) => {
      return React.cloneElement(child, {
        inline,
        ellipsizeMode: 'tail',
        numberOfLines: 1,
      });
    });
  }
}
