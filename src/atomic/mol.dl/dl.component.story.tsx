import * as React from 'react';

import { Caption, H2, H3 } from '@atomic/atm.typography';
import { VBox, VSeparator } from '@atomic/obj.grid';
import { DL } from '@atomic/mol.dl';

import { Scroll } from '@atomic/obj.scroll';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Molecules', module);

stories.add('Definition list', () => (
  <Scroll>
    <VBox>
      <H2>Definition List -- DT/DD</H2>
      <H3>DL Inline</H3>
      <DL inline={true}>
        <DL.DT>DT Inline</DL.DT>
        <DL.DD>DD</DL.DD>
      </DL>
      <DL inline={true}>
        <DL.DT>DT Inline</DL.DT>
        <DL.DD>DD very very very very very very very very very very very very very very very very long</DL.DD>
      </DL>
      <VSeparator />
      <H3>DL Stacked</H3>
      <DL>
        <DL.DT>DT Stacked</DL.DT>
        <DL.DD>DD with
          &nbsp;<Caption>(caption)</Caption>
        </DL.DD>
      </DL>
      <DL>
        <DL.DT>DT Stacked</DL.DT>
        <DL.DD>DD with
          &nbsp;<Caption>(caption)</Caption>
        </DL.DD>
      </DL>
    </VBox>
  </Scroll>
));
