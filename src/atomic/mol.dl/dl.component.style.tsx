import styled from 'styled-components/native';

import { Spacing } from '@atomic/obj.constants';
import { DD, DT } from '@atomic/atm.typography';
import { DDProps, DLProps } from './dl.component';

export const DLStyled = styled.View`
  flex-direction: ${(props: DLProps) => props.inline ? 'row' : 'column' };
  align-self: stretch;
`;

export const DTStyled = styled(DT)`
  margin-top: ${Spacing.XSmall};
  margin-right: ${(props: DLProps) => props.inline ? Spacing.XSmall : '0px' };
`;

export const DDStyled = styled(DD)`
  margin-top: ${Spacing.XSmall};
  margin-bottom: ${Spacing.XSmall};
  ${(props: DDProps) => props.inline ? 'flex: 99' : null };
`;
