import moment, { Moment } from 'moment';
import * as React from 'react';
import {
  DatePickerAndroid,
  DatePickerAndroidOpenOptions, Modal, Platform, TimePickerAndroid,
  TimePickerAndroidOpenOptions, TouchableOpacity, TouchableWithoutFeedback
} from 'react-native';
import { LinkButton } from '@atomic';
import {
  DatePickerBackgroundStyled, PickerIOSHeaderStyled, PickerIOSModalMaskStyled, PickerIOSModalPositionStyled
} from './picker.component.style';
import RNDateTimePicker from '@react-native-community/datetimepicker';




export interface DateTimePickerFieldProps {
  mode: 'date' | 'time' | 'datetime';
  disabled?: boolean;
  maxDateTime?: Date;
  minDateTime?: Date;
  children: (date: Date) => React.ReactNode;
  onBlur?: () => void;
  onValueChange?: (date: Date) => void;
}

export interface DateTimePickerChildProps extends DateTimePickerFieldProps {
  value: Date;
}

interface DateTimePickerFieldState {
  visibleIOS: boolean;
  value: Date;
}

export class PickerWrapper extends React.Component<DateTimePickerFieldProps, DateTimePickerFieldState> {
  private value: Date = new Date();
  private pickedValue: Moment = moment();
  private cancelled: boolean = false;
  constructor(props) {
    super(props);
    this.state = {
      visibleIOS: false,
      value: new Date(),
    };
  }

  render() {
    return (
      <TouchableOpacity onPress={Platform.OS === 'ios' ? this.handleIOSPress : this.handleAndroidPress}>
        {this.props.children(this.state.value)}
        <Modal
          transparent={true}
          visible={this.state.visibleIOS}
          animationType={'fade'}
          onRequestClose={this.close}
        >
          <PickerIOSModalPositionStyled>
            <TouchableWithoutFeedback onPress={this.toggle} style={{ flex: 1 }}>
              <PickerIOSModalMaskStyled />
            </TouchableWithoutFeedback>
            <PickerIOSHeaderStyled>
              <LinkButton text='OK' onTap={this.handleOk} expanded={false} />
            </PickerIOSHeaderStyled>
            <DatePickerBackgroundStyled>
              <RNDateTimePicker
                value={this.value}
                date={this.value}
                maximumDate={this.props.maxDateTime}
                minimumDate={this.props.minDateTime}
                mode={this.props.mode}
                onChange={this.handleDateChange}
                display="spinner"
              />
            </DatePickerBackgroundStyled>
          </PickerIOSModalPositionStyled>
        </Modal>
      </TouchableOpacity>
    );
  }

  private handleIOSPress = () => {
    this.setState({ visibleIOS: true });
  }

  private close = () => {
    this.setState({ visibleIOS: false });

    if (this.props.onBlur) {
      this.props.onBlur();
    }
  }
  private handleAndroidPress = () => {
    this.openAndroidPickers();
  }

  private async openAndroidPickers() {  // Android has separate date and time pickers
    this.value = moment(this.state.value).toDate();  // initializes pickedValue with current one
    this.cancelled = false;

    await this.openDatePicker();   // opens date picker and sets its values on pickedValue
    await this.openTimePicker();   // opens time picker and sets its values on pickedValue

    if (!this.cancelled) {   // if any of the pickers was cancelled value is not set
      this.setState({ value: this.pickedValue.toDate() });
      if (this.props.onValueChange) {
        this.props.onValueChange(this.pickedValue.toDate());
      }
    }
  }

  private async openDatePicker() {
    if (this.props.mode !== 'time') { // doesn't pick the date if you only want to pick the time
      const dateOptions: DatePickerAndroidOpenOptions = {
        date: this.value,
        maxDate: this.props.maxDateTime,
        minDate: this.props.minDateTime,
        mode: 'spinner',
      };

      try {
        const date = await DatePickerAndroid.open(dateOptions);
        if (date.action !== DatePickerAndroid.dismissedAction) {
          this.pickedValue.set({ year: date.year, month: date.month, date: date.day });
        } else {
          this.cancelled = true;
        }
      } catch ({ code, message }) {
        console.warn('Cannot open date picker', message);
      }
    }
  }

  private async openTimePicker() {
    if (!this.cancelled && this.props.mode !== 'date') { // doesn't pick the time if you only want to pick the date
      const timeOptions: TimePickerAndroidOpenOptions = {
        hour: this.state.value.getHours(),
        minute: this.state.value.getMinutes(),
        is24Hour: false,
        mode: 'spinner',
      };

      try {
        const time = await TimePickerAndroid.open(timeOptions);
        if (time.action !== TimePickerAndroid.dismissedAction) {
          this.pickedValue.set({ hour: time.hour, minute: time.minute, second: 0, millisecond: 0 });
        } else {
          this.cancelled = true;
        }
      } catch ({ code, message }) {
        console.warn('Cannot open time picker', message);
      }
    } else { // mimicking IOS (time set to 0 if not picked)
      this.pickedValue.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    }
  }

  private toggle = () => {
    const visibleIOS: boolean = !this.state.visibleIOS;
    this.setState({ visibleIOS });

    if (!visibleIOS && this.props.onBlur) {
      this.props.onBlur();
    }
  }

  private handleOk = () => {
    this.setState({ value: this.value });
    if (this.props.onValueChange) {
      this.props.onValueChange(this.value);
    }
    this.close();
  }

  private handleDateChange = (event, value: Date) => this.value = value;
}
