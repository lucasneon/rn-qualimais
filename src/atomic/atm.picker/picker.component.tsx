import * as React from 'react';
import { Asset } from '@atomic/obj.asset';
import { Else, If } from '@atomic/obj.if';
import { FormFieldContexState, FormFieldContext } from '@atomic/obj.form/form-field.component';
import { LinkButton } from '@atomic/atm.button';
import {
  Modal,
  Picker,
  Platform,
  TouchableWithoutFeedback
} from 'react-native';

import {
  PickerFieldChevronStyled,
  PickerFieldPlaceholderStyled,
  PickerFieldStyled,
  PickerFieldValueStyled,
  PickerIOSHeaderStyled,
  PickerIOSModalMaskStyled,
  PickerIOSModalPositionStyled,
  PickerStyled,
  PickerViewStyled
} from './picker.component.style';
export interface PickerFieldItem {
  value: string | number;
  label: string;
}

export interface PickerFieldProps {
  items: PickerFieldItem[];
  disabled?: boolean;
  onValueChange?: (value: any) => void;
  onBlur?: () => void;
  placeholder?: string;
}

interface PickerFieldState {
  value: string | number;
  visible: boolean;
  label: string;
}

export class PickerField extends React.Component<PickerFieldProps, PickerFieldState> {

  private touched: boolean = false;
  private formFieldConsumer: FormFieldContexState;

  constructor(props: PickerFieldProps) {
    super(props);

    this.state = { visible: false, label: '', value: null };
  }

  componentDidMount() {
    if (this.formFieldConsumer && this.formFieldConsumer.value) {
      const state = this.getSelectedItem(this.formFieldConsumer.value);
      this.setState(state);
    }
  }

  render() {
    const pickerSelection = (
      <PickerStyled selectedValue={this.state.value}  onValueChange={this.handleValueChange}>
      {
          this.props.items.map((item: PickerFieldItem) => {
          return  <Picker.Item key={item.value} value={item.value} label={item.label}/>;
        })
      }
      </PickerStyled>
    );

    return (
      <FormFieldContext.Consumer>
      {(formFieldConsumer: FormFieldContexState) => {
        this.formFieldConsumer = formFieldConsumer;
        return (
          <PickerViewStyled>
            {
              Platform.OS === 'ios' &&
              <PickerFieldStyled
                onPress={this.toggle}
                hasError={this.formFieldConsumer && this.formFieldConsumer.errors.length > 0}
              >
                <If cond={this.state.value !== undefined && this.state.value !== null}>
                  <PickerFieldValueStyled>{this.state.label}</PickerFieldValueStyled>
                <Else />
                  <PickerFieldPlaceholderStyled>{this.props.placeholder}</PickerFieldPlaceholderStyled>
                </If>
                <PickerFieldChevronStyled source={Asset.Icon.Atomic.ChevronDown}/>
              </PickerFieldStyled>
            }
            {
              Platform.OS === 'android' &&
              <PickerFieldStyled
                hasError={this.formFieldConsumer && this.formFieldConsumer.errors.length > 0}
              >
                {pickerSelection}
              </PickerFieldStyled>
            }
            {/* Create a modal only for IOS, since Android has its own Modal built-in*/}
            {
              Platform.OS === 'ios' &&
              <Modal
                transparent={true}
                visible={this.state.visible}
                animationType={'fade'}
                onRequestClose={this.close}
              >
                <PickerIOSModalPositionStyled>
                  <TouchableWithoutFeedback onPress={this.toggle} style={{flex: 1}}>
                    <PickerIOSModalMaskStyled/>
                  </TouchableWithoutFeedback>
                  <PickerIOSHeaderStyled>
                    <LinkButton text='OK' onTap={this.handleOk} expanded={false} />
                  </PickerIOSHeaderStyled>
                  {pickerSelection}
                </PickerIOSModalPositionStyled>
              </Modal>
            }
          </PickerViewStyled>
        );
      }}
      </FormFieldContext.Consumer>
    );
  }

  private getSelectedItem(selectedValue: string | number) {

    const selectedItem: PickerFieldItem[] = this.props.items
                                            .filter((value: PickerFieldItem) => {
                                              return value.value === selectedValue;
                                            });
    if (selectedItem.length) {
      return {label: selectedItem[0].label, value: selectedItem[0].value};
    }

    return {label: '', value: null};
  }

  private handleValueChange = (value: string | number) => {
    this.touched = true;

    if (this.props.onValueChange) {
      this.props.onValueChange(value);
    }

    const state = this.getSelectedItem(value);
    if (this.formFieldConsumer) {
      this.formFieldConsumer.onValueChange(state.value, state.label);
    }
    this.setState(state);

    if (Platform.OS === 'android') {
      this.close();
    }
  }

  private toggle = () => {
    const visible: boolean = !this.state.visible;
    if (this.formFieldConsumer) {
      this.formFieldConsumer.onFocusChange(visible);
    }
    this.setState({ visible });

    if (!visible && this.props.onBlur) {
      this.props.onBlur();
    }
  }

  private close = () => {
    if (this.formFieldConsumer) {
      this.formFieldConsumer.onFocusChange(false);
    }
    this.setState({ visible: false});

    if ( this.props.onBlur) {
      this.props.onBlur();
    }
  }

  private handleOk = () => {
    if (!this.touched) {
      const item: PickerFieldItem = this.props.items[0];
      this.handleValueChange(item.value);
    }
    this.close();
  }
}
