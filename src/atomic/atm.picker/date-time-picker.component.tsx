import { FormFieldContexState, FormFieldContext } from '@atomic/obj.form/form-field.component';
import moment from 'moment';
import * as React from 'react';
import { Platform } from 'react-native';
import { DateTimePickerAndroid } from './date-time-picker-android.component';
import { DateTimePickerIOS } from './date-time-picker-ios.component';

export interface DateTimePickerFieldProps {
  mode: 'date' | 'time' | 'datetime';
  disabled?: boolean;
  maxDateTime?: Date;
  minDateTime?: Date;
  onValueChange?: (value: Date) => void;
  onBlur?: () => void;
}

export interface DateTimePickerChildProps extends DateTimePickerFieldProps {
  label: string;
  value: Date;
  visible?: boolean;
  formFieldConsumer: FormFieldContexState;
}

interface DateTimePickerFieldState {
  value: Date;
  label: string;
}

export class DateTimePickerField extends React.Component<DateTimePickerFieldProps, DateTimePickerFieldState> {

  private formFieldConsumer: FormFieldContexState;

  constructor(props) {
    super(props);

    this.state = { label: '', value: new Date() }; // if no default value is passed, set to now
  }

  componentDidMount() {
    if (this.formFieldConsumer && this.formFieldConsumer.value) {
      const state = this.getSelectedItem(this.formFieldConsumer.value);
      this.setState(state);
    }
  }

  render() {
    return (
      <FormFieldContext.Consumer>
        {(formFieldConsumer: FormFieldContexState) => {
          this.formFieldConsumer = formFieldConsumer;
          const childProps = { // setting props that are different from the parent
            label: this.state.label,
            value: this.state.value,
            formFieldConsumer: this.formFieldConsumer,
            onValueChange: this.handleValueChange,
          };
          return <DateTimePickerAndroid {...childProps} {...this.props} />
        }}
      </FormFieldContext.Consumer>
    );
  }

  private getSelectedItem(selectedValue: Date) {
    let mask = '';

    switch (this.props.mode) {
      case 'datetime':
        mask = 'D MMMM YYYY • H:mm';
        break;
      case 'time':
        mask = 'H:mm';
        break;
      case 'date':
        mask = 'D MMMM YYYY';
        break;
    }

    const label: string = moment(selectedValue).format(mask);

    return { label, value: selectedValue };
  }

  private handleValueChange = (value: Date) => {
    if (this.props.onValueChange) {
      this.props.onValueChange(value);
    }

    const state = this.getSelectedItem(value);

    if (this.formFieldConsumer) {
      this.formFieldConsumer.onValueChange(state.value, state.label);
    }

    this.setState(state);
  }
}
