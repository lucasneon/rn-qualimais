import { Platform } from 'react-native';
import styled, { css } from 'styled-components/native';
import { Border, Color, FontSize, FormsConstants, Spacing } from '@atomic/obj.constants';

export const PickerViewStyled = styled.View`
  flex-direction: column;
  align-self: stretch;
`;

export const PickerIOSModalPositionStyled = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: flex-end;
`;

export const PickerIOSModalMaskStyled = styled.View`
  background-color: ${Color.Black};
  position: absolute;
  opacity: 0.6;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

export const PickerIOSHeaderStyled = styled.View`
  background-color: ${Color.GrayXLight};
  height: 44px;
  align-items: center;
  justify-content: flex-end;
  flex-direction: row;
  padding-right: ${Spacing.Large};
`;

interface StyledPickerFieldProps {
  hasError: boolean;
}

export const PickerFieldStyled = styled.TouchableOpacity`
  border-color: ${(props: StyledPickerFieldProps) => props.hasError ? Color.Alert : Border.Color};
  border-radius: ${Border.RadiusLarge};
  border-width: ${Border.Width};
  align-self: stretch;
  padding-horizontal: ${Spacing.Small};
  min-height: ${FormsConstants.FieldHeight};
  justify-content: center;
`;

export const PickerFieldValueStyled = styled.Text`
  font-size: ${FontSize.Small};
  padding-right: ${Spacing.Large};
`;

export const PickerFieldPlaceholderStyled = styled(PickerFieldValueStyled)`
  color: ${Color.Gray};
`;

export const PickerFieldChevronStyled = styled.Image`
  position: absolute;
  right: ${Spacing.Small};
`;

export const PickerStyled = styled.Picker`
  ${Platform.select({ ios: css` background-color: white; ` })};
`;

export const DatePickerBackgroundStyled = styled.View`
  ${Platform.select({ ios: css` background-color: white; ` })};
`;
