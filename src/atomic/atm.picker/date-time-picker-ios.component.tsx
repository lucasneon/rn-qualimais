import { LinkButton } from '@atomic/atm.button';
import { Asset } from '@atomic/obj.asset';
import * as React from 'react';
import { DatePickerIOS, Modal, TouchableWithoutFeedback } from 'react-native';
import {
  DatePickerBackgroundStyled,
  PickerFieldChevronStyled,
  PickerFieldStyled,
  PickerFieldValueStyled,
  PickerIOSHeaderStyled,
  PickerIOSModalMaskStyled,
  PickerIOSModalPositionStyled,
  PickerViewStyled
} from './picker.component.style';
import { DateTimePickerChildProps } from './date-time-picker.component';

interface DateTimePickerIOSState {
  visible: boolean;
}

export class DateTimePickerIOS extends React.Component<DateTimePickerChildProps, DateTimePickerIOSState> {

  private value: Date = new Date();

  constructor(props) {
    super(props);
    this.state = { visible: false };
  }

  render() {
    return (
      <PickerViewStyled>
        <PickerFieldStyled
          onPress={this.toggle}
          hasError={this.props.formFieldConsumer && this.props.formFieldConsumer.errors.length > 0}
        >
          <PickerFieldValueStyled>{this.props.label}</PickerFieldValueStyled>
          <PickerFieldChevronStyled source={Asset.Icon.Atomic.ChevronDown} />
        </PickerFieldStyled>
        <Modal
          transparent={true}
          visible={this.state.visible}
          animationType={'fade'}
          onRequestClose={this.close}
        >
          <PickerIOSModalPositionStyled>
            <TouchableWithoutFeedback onPress={this.toggle} style={{ flex: 1 }}>
              <PickerIOSModalMaskStyled />
            </TouchableWithoutFeedback>
            <PickerIOSHeaderStyled>
              <LinkButton text='OK' onTap={this.handleOk} expanded={false} />
            </PickerIOSHeaderStyled>
            <DatePickerBackgroundStyled>
              <DatePickerIOS
                date={this.props.value}
                maximumDate={this.props.maxDateTime}
                minimumDate={this.props.minDateTime}
                mode={this.props.mode}
                onDateChange={this.handleDateChange}
              />
            </DatePickerBackgroundStyled>
          </PickerIOSModalPositionStyled>
        </Modal>
      </PickerViewStyled>
    );
  }

  private toggle = () => {
    const visible: boolean = !this.state.visible;
    if (this.props.formFieldConsumer) {
      this.props.formFieldConsumer.onFocusChange(visible);
    }
    this.setState({ visible });

    if (!visible && this.props.onBlur) {
      this.props.onBlur();
    }
  }

  private close = () => {
    if (this.props.formFieldConsumer) {
      this.props.formFieldConsumer.onFocusChange(false);
    }
    this.setState({ visible: false });

    if (this.props.onBlur) {
      this.props.onBlur();
    }
  }

  private handleOk = () => {
    this.props.onValueChange(this.value);
    this.close();
  }

  private handleDateChange = (value: Date) => this.value = value;
}
