import moment from 'moment';
import * as React from 'react';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import { InputLabel } from '@atomic/atm.typography';
import { DateTimePickerChildProps } from './date-time-picker.component';
import { PickerFieldStyled, PickerViewStyled } from './picker.component.style';


interface DateTimePickerState {
  showDate: boolean;
  showTime: boolean;
}
export class DateTimePickerAndroid extends React.Component<DateTimePickerChildProps, DateTimePickerState> {
  private pickedValue: moment.Moment;
  
  constructor(props) {
    super(props);
    this.state = {
      showDate: false,
      showTime: false
    }
  }

  componentDidMount() {
    this.pickedValue = moment(this.props.value);
  }

  render() {
    let dateOptions: any = {};
    if (this.props.mode !== 'time') { // doesn't pick the date if you only want to pick the time
      dateOptions  = {
        value: this.props.value,
        maxDate: this.props.maxDateTime,
        minDate: this.props.minDateTime,
        mode: 'spinner',
      };
    }

    return (
      <PickerViewStyled>
        <PickerFieldStyled
          onPress={this.handleAndroidPicker}  // makes so that when touched calls the picker modal
          hasError={this.props.formFieldConsumer && this.props.formFieldConsumer.errors.length > 0}
        >
            <InputLabel>{this.props.label ? this.props.label : 'Selecione uma data'}</InputLabel> 
          {this.state.showDate && this.props.mode !== 'time' && <RNDateTimePicker mode='date' 
            maximumDate={this.props.maxDateTime} 
            minimumDate={this.props.minDateTime}
            value={this.props.value} 
            onChange={this.onChangeDate.bind(this)} />}
          {this.state.showTime && this.props.mode !== 'date' && <RNDateTimePicker mode='time' 
            value={this.props.value} 
            onChange={this.onChangeTime.bind(this)} />}
        </PickerFieldStyled>
      </PickerViewStyled>
    );
  }

  private onChangeDate(_, date: Date) {
    this.setState({showDate: false});
    this.pickedValue.set({ year: date.getFullYear(), month: date.getMonth(), date: date.getDate() });
    this.props.onValueChange(this.pickedValue.toDate());

    if(this.props.mode !== 'date') {
      this.setState({showTime: true});
    }
  };

  private onChangeTime(_, time: Date) {
    this.setState({showTime: false});
    this.pickedValue.set({ hour: time.getHours(), minute: time.getMinutes(), second: 0, millisecond: 0 });
    this.props.onValueChange(this.pickedValue.toDate());
  }

  private handleAndroidPicker = () => {
    if(this.props.mode !== 'time') {
      this.setState({showDate: true});
    }
  }
}
