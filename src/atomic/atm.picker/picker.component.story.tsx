import moment from 'moment';
import * as React from 'react';
import { AutoFormSample } from '@atomic';
import { LinkButtonTextStyled } from '@atomic/atm.button/link-button.component.style';
import { PickerField } from '@atomic/atm.picker';
import { Body, H2 } from '@atomic/atm.typography';
import { Cell } from '@atomic/mol.cell';
import { Form, Validators } from '@atomic/obj.form';
import { HBox, VBox, VSeparator } from '@atomic/obj.grid';
import { storiesOf } from '@storybook/react-native';
import { DateTimePickerField } from './date-time-picker.component';
import { PickerWrapper } from './picker-wrapper.component';

const stories = storiesOf('Form', module);

stories.add('Picker field', () => (
  <AutoFormSample>
    <VBox>
      <H2>Picker with placeholder</H2>
      <PickerField
        placeholder='Selecione'
        items={[
          { value: 1, label: 'value 1' },
          { value: 2, label: 'value 2' }]}
      />
      <VSeparator />
      <H2>Picker with default value</H2>
      <Form.Field value={1} name='defaultValue' >
        <PickerField
          items={[
            { value: 1, label: 'value 1' },
            { value: 2, label: 'value 2' }]}
        />
      </Form.Field>
      <VSeparator />
      <H2>Picker with validation</H2>
      <Form.Field name='picker3' validators={[Validators.Required('Required')]}>
        <PickerField
          items={[
            { value: 1, label: 'very very very very very very very very very very long text' },
            { value: 2, label: 'short text' }]}
        />
      </Form.Field>
      <VSeparator />
      <H2>Picker with date and time</H2>
      <Form.Field
        name='picker4'
        value={new Date()}
        validators={[
          Validators.Required('Required'),
          Validators.MaxDateTime(moment().add({ d: 1, h: 1 }).set({ s: 0, ms: 0 }).toDate()),
          Validators.MinDateTime(moment().subtract({ d: 1, h: 1 }).set({ s: 0, ms: 0 }).toDate()),
        ]}
      >
        <DateTimePickerField
          maxDateTime={moment().add({ d: 1, h: 1 }).set({ s: 0, ms: 0 }).toDate()}
          minDateTime={moment().subtract({ d: 1, h: 1 }).set({ s: 0, ms: 0 }).toDate()}
          mode={'datetime'}
        />
      </Form.Field>
      <VSeparator />
      <H2>Picker with date</H2>
      <Form.Field
        name='picker5'
        value={new Date()}
        validators={[
          Validators.Required('Required'),
          Validators.MaxDate(moment().add(2, 'd').toDate()),
          Validators.MinDate(moment().subtract(2, 'd').toDate()),
        ]}
      >
        <DateTimePickerField
          maxDateTime={moment().add(2, 'd').set({ s: 0, ms: 0 }).toDate()}
          minDateTime={moment().subtract(2, 'd').set({ s: 0, ms: 0 }).toDate()}
          mode={'date'}
        />
      </Form.Field>
      <VSeparator />
      <H2>Picker with time</H2>
      <Form.Field
        name='picker6'
        value={new Date()}
        validators={[
          Validators.Required('Required'),
          Validators.MaxDateTime(moment().add(2, 'h').set({ s: 0, ms: 0 }).toDate()),
          Validators.MinDateTime(moment().subtract(2, 'h').set({ s: 0, ms: 0 }).toDate()),
        ]}
      >
        <DateTimePickerField
          maxDateTime={moment().add(2, 'h').set({ s: 0, ms: 0 }).toDate()}
          minDateTime={moment().subtract(2, 'h').set({ s: 0, ms: 0 }).toDate()}
          mode={'time'}
        />
      </Form.Field>
      <H2>Picker wrapper</H2>
      <PickerWrapper mode='time'>
        {date => <Cell>
          <HBox>
            <HBox.Item>
              <Body>horário</Body>
            </HBox.Item>
            <HBox.Item>
              <LinkButtonTextStyled>{moment(date).format('HH:mm')}</LinkButtonTextStyled>
            </HBox.Item>
          </HBox>
        </Cell>}
      </PickerWrapper>
    </VBox>
  </AutoFormSample>
));
