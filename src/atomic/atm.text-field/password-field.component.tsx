import * as React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { LoginStrings } from '@app/modules/authentication/login.strings';
import { Form, Validator } from '@atomic';
import { TextField, TextFieldProps } from '@atomic/atm.text-field';
import { Asset } from '@atomic/obj.asset';

interface PasswordFieldState {
  hidePassword: boolean;
}
export interface PasswordFieldProps extends TextFieldProps {
  name?: string;
  validators: Validator[];
}

export class PasswordField extends React.PureComponent<PasswordFieldProps, PasswordFieldState> {
  static defaultProps: TextFieldProps = {
    editable: true,
  };

  constructor(props: PasswordFieldProps) {
    super(props);
    this.state = { hidePassword: true};
  }

  render() {

    return (
      <Form.Field
        label={LoginStrings.Label.Password}
        validators={this.props.validators}
        name={this.props.name}
      >
        <TextField secureTextEntry={this.state.hidePassword}>
          <TouchableOpacity  onPress={this.toggleSecure}>
            <Image
              source={this.state.hidePassword ? Asset.Icon.Custom.EyeClosed : Asset.Icon.Custom.EyeOpen}
            />
          </TouchableOpacity>
        </TextField>
      </Form.Field>
    );
  }

  private toggleSecure = () => {
    this.setState(prevState => ({hidePassword: !prevState.hidePassword}));
  }

}
