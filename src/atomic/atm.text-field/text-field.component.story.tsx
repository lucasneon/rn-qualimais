import * as React from 'react';

import { VBox, VSeparator} from '@atomic/obj.grid';
import { H2, InputLabel } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from './text-field.component';
import { storiesOf } from '@storybook/react-native';
import { Form } from '@atomic';

// import { action } from '@storybook/addon-actions';

const stories = storiesOf('Form', module);

stories.add('TextField', () => (
  <AutoFormSample>
    <VBox>
      <H2>Text Fields</H2>
      <Form.Field name='normal' label='Normal'>
        <TextField type={TextField.Type.Normal}/>
      </Form.Field>
      <VSeparator />
      <Form.Field name='normal' label='With initial value'>
        <TextField type={TextField.Type.Normal} defaultValue='This is an initial value'/>
      </Form.Field>
      <VSeparator />
      <Form.Field name='normal' label='Disabled' >
        <TextField type={TextField.Type.Normal} defaultValue='This is not editable' editable={false}/>
      </Form.Field>
      <VSeparator />
      <InputLabel>Email</InputLabel>
      <Form.Field name='email' >
        <TextField type={TextField.Type.Email}/>
      </Form.Field>
      <VSeparator />
      <InputLabel>CPF</InputLabel>
      <Form.Field name='cpf' >
        <TextField type={TextField.Type.CPF}/>
      </Form.Field>
      <VSeparator />
      <InputLabel>Date</InputLabel>
      <Form.Field name='date' >
        <TextField type={TextField.Type.Date}/>
      </Form.Field>
      <VSeparator />
      <Form.Field name='cnpj' >
        <InputLabel>CNPJ</InputLabel>
        <TextField type={TextField.Type.CNPJ}/>
      </Form.Field>
      <VSeparator />
      <InputLabel>Numeric</InputLabel>
      <Form.Field name='numeric' >
        <TextField type={TextField.Type.Numeric}/>
      </Form.Field>
      <VSeparator />
       <InputLabel>CEP</InputLabel>
      <Form.Field name='CEP' >
        <TextField type={TextField.Type.ZipCode}/>
      </Form.Field>
      <VSeparator />
     <InputLabel>Money</InputLabel>
      <Form.Field name='money' >
        <TextField type={TextField.Type.Money}/>
      </Form.Field>
      <VSeparator />
      <InputLabel>Phone</InputLabel>
      <Form.Field name='phone'>
        <TextField  type={TextField.Type.CellPhone}/>
      </Form.Field>
      <VSeparator />
      <InputLabel>Custom Mask </InputLabel>
      <Form.Field name='custom-mask' >
        <TextField mask='[AAA]-[0000]' placeholder='AAA-9999'/>
      </Form.Field>
      <VSeparator />
      <InputLabel>Password</InputLabel>
      <Form.Field name='password'>
        <TextField
          type={TextField.Type.Normal}
          maxLength={8}
          placeholder='Sua senha pessoal de acesso'
          secureTextEntry={true}
        />
      </Form.Field>
      <VSeparator />
      <InputLabel>Auto Resize field</InputLabel>
      <Form.Field name='extendable'>
        <TextField
          type={TextField.Type.Normal}
          placeholder='Digite um texto grande'
        />
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
