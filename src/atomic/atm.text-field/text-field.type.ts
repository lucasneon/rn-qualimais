// tslint:disable:max-line-length
/*
Mask examples:

International phone numbers: +1 ([000]) [000] [00] [00]
Local phone numbers: 8 ([000]) [000]-[00]-[00]
Visa card numbers: [0000] [0000] [0000] [0000]
Names: [A][-----------------------------------------------------]
Text: [A…]
Dates: [00]{/}[00]{/}[9900]
Masks consist of blocks of symbols, which may include:

[] — a square brackets block for valueable symbols written by user.
Square brackets block may contain any number of special symbols:

0 — mandatory digit. For instance, [000] mask will allow user to enter three numbers: 123.
9 — optional digit . For instance, [00099] mask will allow user to enter from three to five numbers.
А — mandatory letter. [AAA] mask will allow user to enter three letters: abc.
а — optional letter. [АААааа] mask will allow to enter from three to six letters.
_ — mandatory symbol (digit or letter).
- — optional symbol (digit or letter).
… — ellipsis. Allows to enter endless count of symbols. For details and rules see Elliptical masks.

Symbols outside the square brackets will take a place in the output. For instance, +7 ([000]) [000]-[0000] mask will format the input field to the form of +7 (123) 456-7890.

{} — a block for valueable yet fixed symbols, which could not be altered by the user.
Symbols within the square and curly brackets form an extracted value (or «valueable characters»). In other words, [00]-[00] and [00]{-}[00] will form the same output 12-34, but in the first case the value, extracted by the library, will be equal to 1234, and in the second case it will result in 12-34.

Elliptical format examples:

[…] is a wildcard mask, allowing to enter letters and digits. Always returns true in Result.complete.
[00…] is a numeric mask, allowing to enter digits. Requires at least two digits to be complete.
[9…] is a numeric mask, allowing to enter digits. Always returns true in Result.complete.
[_…] is a wildcard mask with a single mandatory character. Allows to enter letters and digits. Requires a single character (digit or letter).
[-…] acts same as […].

*/
// tslint:enable:max-line-length

type KeyboardType =
  'default' |
  'email-address' |
  'numeric' |
  'phone-pad' |
  'ascii-capable' |
  'numbers-and-punctuation' |
  'url' |
  'number-pad' |
  'name-phone-pad' |
  'decimal-pad' |
  'twitter' |
  'web-search' |
  'visible-password';

export interface TextFieldConfig {
  keyboard: KeyboardType;
  mask?: string;
  placeholder?: string;
}

type TextFieldTypes = {
  [A in TextFieldTypeKey]: TextFieldConfig;
};

enum TextFieldTypeKey {
  CellPhone = 'CellPhone',
  CNPJ = 'CNPJ',
  CPF = 'CPF',
  Date = 'Date',
  Email = 'Email',
  Money = 'Money',
  Normal = 'Normal',
  Numeric = 'Numeric',
  NumericDecimal = 'NumericDecimal',
  ZipCode = 'ZipCode',
}
export const TextFieldType: TextFieldTypes = {
  [TextFieldTypeKey.CellPhone]: {
    keyboard: 'numeric',
    placeholder: '(99) 99999-9999',
    mask: '{(}[00]{)} [00009]{-}[0000]',
  },
  [TextFieldTypeKey.CNPJ]: {
    keyboard: 'numeric',
    placeholder: '99.999.999/9999-99',
    mask: '[00]{.}[000]{.}[000]{.}[000]{/}[0000]{-}[00]',
  },
  [TextFieldTypeKey.CPF]: {
    keyboard: 'numeric',
    placeholder: '999.999.999-99',
    mask: '[000].[000].[000]-[00]',
  },
  [TextFieldTypeKey.Date]: {
    keyboard: 'numeric',
    placeholder: 'e.g.: dd/mm/yyyy',
    mask: '[09]/[00]/[0000]',
  },
  [TextFieldTypeKey.Email]: {
    keyboard: 'email-address',
    placeholder: 'eg: name@email.com',
  },
  [TextFieldTypeKey.Money]: {
    keyboard: 'numeric',
    mask: 'R$ [0999999999999]{,}[00]',
    placeholder: 'R$ 0,00',
  },
  [TextFieldTypeKey.Normal]: {
    keyboard: 'default',
  },
  [TextFieldTypeKey.Numeric]: {
    keyboard: 'numeric',
    mask: '[999999999999999]',
  },
  [TextFieldTypeKey.NumericDecimal]: {
    keyboard: 'numeric',
    mask: '[999999999999999].[99]',
  },
  [TextFieldTypeKey.ZipCode]: {
    keyboard: 'numeric',
    mask: '[00000]{-}[000]',
    placeholder: '99999-999',
  },
};
