import { Animated, TextInput, View } from 'react-native';
import TextInputMask from 'react-native-text-input-mask';
import styled from 'styled-components/native';
import { Border, Color, FontSize, FormsConstants, Spacing } from '@atomic/obj.constants';
import { TextFieldProps } from './text-field.component';

type StyledTextFieldProps = TextFieldProps & { hasError: boolean, focus: boolean, height: number };

const borderColor = (props: StyledTextFieldProps) => {
  let color = props.focus ? Color.BlueDark : Border.Color;
  if (props.hasError) {
    color = Color.Alert;
  }
  return color;
};

const isEditable = (props: StyledTextFieldProps) => {
  return props.editable || props.editable === undefined;
};

export const TextFieldStyled = styled(TextInputMask).attrs({
  underlineColorAndroid: 'transparent',
})`
  flex: 1;
  align-self: stretch;
  align-items: center;
  height: ${(props: StyledTextFieldProps) => Math.max(FormsConstants.FieldHeight, props.height)};
  opacity: ${(props: StyledTextFieldProps) => isEditable(props) ? 1 : 0.9 };
  padding-horizontal: ${Spacing.Small};
  font-size: ${FontSize.Small};
`;

export const TextFieldWrapperStyled = styled.View `
  flexDirection: row;
  border-color: ${(props: StyledTextFieldProps) => borderColor(props)};
  border-radius: ${Border.RadiusLarge};
  border-width: ${Border.Width};
  border-style: ${(props: StyledTextFieldProps) => isEditable(props) ? 'solid' : 'dashed'};
  align-self: stretch;
  align-items: center;
  height: ${(props: StyledTextFieldProps) => Math.max(FormsConstants.FieldHeight, props.height)};
  opacity: ${(props: StyledTextFieldProps) => isEditable(props) ? 1 : 0.9 };
  padding-horizontal: ${Spacing.Small};
`;

export const TextFieldBorderStyled = styled(Animated.View)`
  position: absolute;
  bottom: 0;
  height: 2;
  opacity: 0.6;
  background-color: ${(props: StyledTextFieldProps) => props.hasError ? Color.Alert : Color.BlueDark};
`;

export const ChatView = styled(View)`
  flexDirection: row;
  backgroundColor: ${Color.White};
  borderTopColor: gray;
  borderWidth: 0.5;
  alignSelf: stretch;
  alignItems: center;
`;

export const ChatTextInput = styled(TextInput)`
  flex: 1;
  borderColor: ${Color.Primary};
  border-radius: ${Border.RadiusLarge};
  border-width: ${Border.Width};
  height: ${FormsConstants.FieldHeight};
  padding-horizontal: ${Spacing.Small};
  top: 1;
  margin: ${Spacing.XSmall}px;
`;
