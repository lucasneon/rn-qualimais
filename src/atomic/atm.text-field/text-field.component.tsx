import * as _ from 'lodash';
import * as React from 'react';
import { Animated, Easing, TextInputProps, View } from 'react-native';
import { Shimmer, VBox, VSeparator } from '@atomic';
import { AnimationDuration, FontSize, FormsConstants } from '@atomic/obj.constants';
import { FormFieldContexState, FormFieldContext } from '@atomic/obj.form/form-field.component';
import { TextFieldBorderStyled, TextFieldStyled, TextFieldWrapperStyled } from './text-field.component.style';
import { TextFieldConfig, TextFieldType } from './text-field.type';

interface OnContentSizeChangeEvent {
  nativeEvent: { contentSize: { width: number; height: number } };
}

export interface TextFieldProps extends TextInputProps {
  noBorder?: boolean;
  type?: TextFieldConfig;
  mask?: string;
  value?: string;
  placeholder?: string;
  defaultValue?: string;
  onChangeText?: any;
  editable?: boolean;
  secureTextEntry?: boolean;
  onFocusChange?: (hasFocus: boolean) => void;
}

export interface TextFieldState {
  focus: boolean;
  height: number;
  defaultValue: string;
  value: string;
}

const POS_EMPTY = 1;
const POS_FILLED = 0;

export const FieldShimmer: React.SFC = () => (
  <VBox>
    <VSeparator/>
    <Shimmer rounded={true} height={FontSize.Small} width={50} />
    <Shimmer rounded={true} height={FormsConstants.FieldHeight} />
  </VBox>
);

export class TextField extends React.Component<TextFieldProps, TextFieldState> {

  static Type = {...TextFieldType};
  static defaultProps = {
    editable: true,
    type: {
      Normal: {
        keyboard: 'default',
      },
    },
  };

  private positionAnimValue: Animated.Value;
  private formFieldConsumer: FormFieldContexState;

  constructor(props: TextFieldProps) {
    super(props);
    this.state = { height: 0, focus: false, defaultValue: props.defaultValue, value: props.value};
    this.positionAnimValue = new Animated.Value(this.state.focus ? POS_FILLED : POS_EMPTY);
  }

  componentDidMount() {
    if (this.formFieldConsumer && this.formFieldConsumer.value) {
      if (this.props.value) {
        // tslint:disable-next-line
        throw new Error('Use of TextField.value detected. To avoid overriding component behavior, please use  <Form.Field value={any} ..> props');
      }

      this.setState({defaultValue: this.formFieldConsumer.value});
    }
  }

  render() {
    const { type, value, placeholder, mask, onChangeText, onFocusChange, children , ...other } = this.props;
    const position = this.positionAnimValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['4%', '60%'],
    });

    return (
      <FormFieldContext.Consumer>
        {(formFieldConsumer: FormFieldContexState) => {
          this.formFieldConsumer = formFieldConsumer;
          return (
            <View>
              <TextFieldWrapperStyled
                hasError={this.formFieldConsumer && this.formFieldConsumer.errors.length > 0}
                height={this.state.height}
                mask={mask || type.mask}
                focus={this.state.focus}
                placeholder={placeholder || type.placeholder}
                onBlur={this.handleFocus(false)}
                onFocus={this.handleFocus(true)}
                onContentSizeChange={this.handleContentSizeChange}
                {...other}
              >
                <TextFieldStyled
                  defaultValue={this.state.defaultValue}
                  value={this.formFieldConsumer && this.formFieldConsumer.value}
                  height={this.state.height}
                  keyboardType={type.keyboard}
                  mask={mask || type.mask}
                  placeholder={placeholder || type.placeholder}
                  onContentSizeChange={this.handleContentSizeChange}
                  onChangeText={this.handleChangeText}
                  {...other}
                />
                {children}
                <TextFieldBorderStyled
                  hasError={this.formFieldConsumer && this.formFieldConsumer.errors.length > 0}
                  style={{left: position, right: position}}
                />
              </TextFieldWrapperStyled>
            </View>);
        }}
      </FormFieldContext.Consumer>
    );
  }

  private handleChangeText = (formatted: string, raw: string) => {
    if (this.props.onChangeText) {
      this.props.onChangeText(formatted);
    }
    if (this.formFieldConsumer && this.formFieldConsumer.onValueChange) {
      this.formFieldConsumer.onValueChange(formatted, raw);
    }
  }

  private handleContentSizeChange = (event: OnContentSizeChangeEvent) => {
    this.setState({
      height: _.get(event, 'nativeEvent.contentSize.height', 0),
    });
  }

  private handleFocus = (focus: boolean) => () => {
    this.setState({focus}, this.animate);
    if (this.props.onFocusChange) {
      this.props.onFocusChange(focus);
    }

    if (this.formFieldConsumer && this.formFieldConsumer.onFocusChange) {
      this.formFieldConsumer.onFocusChange(focus);
    }
  }

  private animate = () => {
    Animated.timing(this.positionAnimValue, {
      toValue: this.state.focus ? POS_FILLED : POS_EMPTY,
      duration: AnimationDuration,
      easing: Easing.inOut(Easing.cubic),
    }).start();
  }
}
