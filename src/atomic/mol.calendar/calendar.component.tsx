import moment, { Moment } from 'moment';
import * as React from 'react';
import { LayoutChangeEvent } from 'react-native';
import { CalendarGrid } from './calendar-grid.component';
import { CalendarHeader } from './calendar-header.component';
import { Swiper } from '../obj.swiper/swiper.component';
import { CalendarWeekdays } from './calendar-weekdays.component';

const SWIPE_LEFT = 'SWIPE_LEFT';
const SWIPE_RIGHT = 'SWIPE_RIGHT';

export enum DateChangeType {
  START_DATE,
  END_DATE,
}

interface CalendarProps {
  allowRangeSelection?: boolean;
  startFromMonday?: boolean;
  initialDate?: Moment;
  minDate?: Moment;
  maxDate?: Moment;
  disabledDates?;
  minRangeDuration?;
  maxRangeDuration?;
  onDateChange?: (date: Moment, type: DateChangeType) => void;
  onMonthChange?: (date: Moment) => void;
  enableSwipe?: boolean;
  selectedStartDate?: Moment;
  selectedEndDate?: Moment;
}

interface CalendarState {
  currentMonth: number;
  currentYear: number;
  selectedStartDate: Moment;
  selectedEndDate: Moment;
  scaler: number;
}

export class Calendar extends React.Component<CalendarProps, CalendarState> {
  static defaultProps = {
    initialDate: moment(),
    enableSwipe: true,
    onDateChange: () => { console.warn('onDateChange() not provided'); },
  };

  constructor(props) {
    super(props);
    this.state = {
      scaler: 1,
      currentMonth: null,
      currentYear: null,
      selectedStartDate: props.selectedStartDate || null,
      selectedEndDate: props.selectedEndDate || null,
      ...this.updateMonthYear(props.initialDate),
    };
  }

  componentDidUpdate(prevProps, prevState) {
    let doStateUpdate = false;

    let newMonthYear = {};
    if (!moment(prevProps.initialDate).isSame(this.props.initialDate, 'day')) {
      newMonthYear = this.updateMonthYear(this.props.initialDate);
      doStateUpdate = true;
    }

    let selectedDateRanges = {};
    if (this.props.selectedStartDate &&
          !moment(prevState.selectedStartDate).isSame(this.props.selectedStartDate, 'day') ||
        this.props.selectedEndDate &&
          !moment(prevState.selectedEndDate).isSame(this.props.selectedEndDate, 'day')) {
      const { selectedStartDate = null, selectedEndDate = null } = this.props;
      selectedDateRanges = { selectedStartDate, selectedEndDate };
      doStateUpdate = true;
    }

    if (doStateUpdate) {
      this.setState({...newMonthYear, ...selectedDateRanges});
    }
  }

  updateMonthYear = (initialDate = this.props.initialDate) => {
    const monthYear = {
      currentMonth: moment(initialDate).month(),
      currentYear: moment(initialDate).year(),
    };

    return monthYear;
  }

  handleOnPressDay = day => {
    const { currentYear, currentMonth, selectedStartDate, selectedEndDate } = this.state;
    const { allowRangeSelection, onDateChange } = this.props;

    const date = moment({year: currentYear, month: currentMonth, day});
    if (allowRangeSelection &&
      selectedStartDate &&
      date.isSameOrAfter(selectedStartDate) &&
      !selectedEndDate) {
      this.setState({ selectedEndDate: date });
      // propagate to parent date has changed
      onDateChange(date, DateChangeType.END_DATE);
    } else {
      this.setState({ selectedStartDate: date, selectedEndDate: null });
      // propagate to parent date has changed
      onDateChange(date, DateChangeType.START_DATE);
    }
  }

  handleOnPressPrevious = () => {
    let { currentYear } = this.state;
    const { currentMonth } = this.state;
    let previousMonth = currentMonth - 1;
    // if previousMonth is negative it means the current month is January,
    // so we have to go back to previous year and set the current month to December
    if (previousMonth < 0) {
      previousMonth = 11;
      currentYear -= 1;  // decrement year

      // setting month to December
      this.setState({ currentMonth: previousMonth, currentYear });
    } else {
      this.setState({ currentMonth: previousMonth, currentYear });
    }
    if (this.props.onMonthChange) {
      this.props.onMonthChange(moment({year: currentYear, month: previousMonth}));
    }
  }

  handleOnPressNext = () => {
    const { currentMonth } = this.state;
    let { currentYear } = this.state;
    let nextMonth = currentMonth + 1;
    // if nextMonth is greater than 11 it means the current month is December,
    // so we have to go forward to the next year and set the current month to January
    if (nextMonth > 11) {
      nextMonth = 0;
      currentYear += 1;  // increment year
      // setting month to January
      this.setState({ currentMonth: nextMonth, currentYear });
    } else {
      this.setState({ currentMonth: nextMonth, currentYear });
    }
    if (this.props.onMonthChange) {
      this.props.onMonthChange(moment({year: currentYear, month: nextMonth}));
    }
  }

  handleSwipe = (gestureName: string) => {
    if (this.props.enableSwipe) {
      switch (gestureName) {
        case SWIPE_LEFT:
          this.handleOnPressNext();
          break;
        case SWIPE_RIGHT:
          this.handleOnPressPrevious();
          break;
      }
    }
  }

  handleWidth = (e: LayoutChangeEvent) => {
    const width = Math.round(e.nativeEvent.layout.width);
    const scaler = (width / 7) / 50; // 7 days per row;

    if (this.state.scaler !== scaler) {
      this.setState({scaler});
    }
  }

  render() {
    const { currentMonth, currentYear, selectedStartDate, selectedEndDate } = this.state;

    const {
      allowRangeSelection,
      startFromMonday,
      minDate,
      maxDate,
      disabledDates,
      minRangeDuration,
      maxRangeDuration,
    } = this.props;

    const disabledDatesTime = [];

    // Convert input date into timestamp
    if (disabledDates && Array.isArray(disabledDates)) {
      disabledDates.map(date => {
        const thisDate = moment(date);
        thisDate.set({hour: 0, minute: 0, second: 0, millisecond: 0});
        disabledDatesTime.push(thisDate.valueOf());
      });
    }

    let minRangeDurationTime = [];

    if (allowRangeSelection && minRangeDuration) {
      if (Array.isArray(minRangeDuration)) {
        minRangeDuration.map(minDuration => {
          const thisDate = moment(minDuration.date);
          thisDate.set({hour: 0, minute: 0, second: 0, millisecond: 0});
          minRangeDurationTime.push({date: thisDate.valueOf(), minDuration: minDuration.minDuration});
        });
      } else {
        minRangeDurationTime = minRangeDuration;
      }
    }

    let maxRangeDurationTime = [];

    if (allowRangeSelection && maxRangeDuration) {
      if (Array.isArray(maxRangeDuration)) {
        maxRangeDuration.map(maxDuration => {
          const thisDate = moment(maxDuration.date);
          thisDate.set({hour: 0, minute: 0, second: 0, millisecond: 0});
          maxRangeDurationTime.push({date: thisDate.valueOf(), maxDuration: maxDuration.maxDuration});
        });
      } else {
        maxRangeDurationTime = maxRangeDuration;
      }
    }

    return (
      <Swiper onSwipe={this.handleSwipe}>
        <CalendarHeader
          currentMonth={currentMonth}
          currentYear={currentYear}
          onPressPrevious={this.handleOnPressPrevious}
          onPressNext={this.handleOnPressNext}
        />
        <CalendarWeekdays startFromMonday={startFromMonday} scaler={this.state.scaler} />
        <CalendarGrid
          scaler={this.state.scaler}
          month={currentMonth}
          year={currentYear}
          onPressDay={this.handleOnPressDay}
          disabledDates={disabledDatesTime}
          minRangeDuration={minRangeDurationTime}
          maxRangeDuration={maxRangeDurationTime}
          startFromMonday={startFromMonday}
          allowRangeSelection={allowRangeSelection}
          selectedStartDate={selectedStartDate && moment(selectedStartDate)}
          selectedEndDate={selectedEndDate && moment(selectedEndDate)}
          minDate={minDate && moment(minDate)}
          maxDate={maxDate && moment(maxDate)}
        />
      </Swiper>
    );
  }
}
