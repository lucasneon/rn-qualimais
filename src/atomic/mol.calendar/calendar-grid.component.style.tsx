import styled from 'styled-components/native';

export const CalendarGridStyled = styled.View`
  align-self: center;
  justify-content: center;
`;

export const CalendarGridWeekStyled = styled.View`
  flex-direction: row;
`;
