import * as React from 'react';
import { CalendarString } from './calendar.strings';
import { CalendarWeekdaysStyled, CalendarWeekdayTextStyled } from './calendar-weekdays.component.style';
export interface WeekdaysProps {
  startFromMonday?: boolean;
  scaler: number;
}

export const CalendarWeekdays: React.SFC<WeekdaysProps> = (props: WeekdaysProps) => {

  const wd = props.startFromMonday ? CalendarString.Weekdays_Mon : CalendarString.Weekdays; // English Week days Array

  return (
    <CalendarWeekdaysStyled>
    { wd.map((day, key) => (
      <CalendarWeekdayTextStyled key={key} scaler={props.scaler}>
        {day}
      </CalendarWeekdayTextStyled>))}
    </CalendarWeekdaysStyled>
  );
};
