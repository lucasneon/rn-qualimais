import * as React from 'react';
import moment from 'moment';
import { Moment } from 'moment';
import {
  CalendarDayDisabledStyled,
  CalendarDayStyled,
  CalendarDayTextStyled,
  CalendarOutOfRangeDayStyled,
  CalendarSelectableDayStyled,
} from './calendar-day.component.style';
import { CalendarGridProps } from './calendar-grid.component';
import { Text } from 'react-native';

interface CalendarDayProps extends CalendarGridProps {
  day: number;
  scaler: number;
}
// tslint:disable-next-line:cyclomatic-complexity
export const CalendarDay: React.SFC<CalendarDayProps> = (
  props: CalendarDayProps,
) => {
  const {
    day,
    month,
    year,
    onPressDay,
    selectedStartDate,
    selectedEndDate,
    allowRangeSelection,
    minDate,
    maxDate,
    disabledDates,
    minRangeDuration,
    maxRangeDuration,
    scaler,
  } = props;

  const thisDay = moment({ year, month, day });
  const today = moment();

  let dateOutOfRange;
  let dateIsBeforeMin = false;
  let dateIsAfterMax = false;
  let dateIsDisabled = false;
  let dateIsBeforeMinDuration = false;
  let dateIsAfterMaxDuration = false;

  // First let's check if date is out of range
  // Check whether props maxDate / minDate are defined. If not supplied,
  // don't restrict dates.
  if (maxDate) {
    dateIsAfterMax = thisDay.isAfter(maxDate, 'day');
  }
  if (minDate) {
    dateIsBeforeMin = thisDay.isBefore(minDate, 'day');
  }

  if (disabledDates) {
    disabledDates.forEach((date: Moment) => {
      if (date.valueOf() === thisDay.valueOf()) {
        dateIsDisabled = true;
      }
    });
  }

  if (
    allowRangeSelection &&
    minRangeDuration &&
    selectedStartDate &&
    thisDay.valueOf() > selectedStartDate.valueOf()
  ) {
    if (Array.isArray(minRangeDuration)) {
      const index = minRangeDuration.findIndex(
        i => i.date === selectedStartDate.valueOf(),
      );
      if (
        index >= 0 &&
        selectedStartDate.valueOf() +
          minRangeDuration[index].minDuration * 86400000 >
          thisDay.valueOf()
      ) {
        dateIsBeforeMinDuration = true;
      }
    } else if (
      selectedStartDate.valueOf() + minRangeDuration * 86400000 >
      thisDay.valueOf()
    ) {
      dateIsBeforeMinDuration = true;
    }
  }

  if (
    allowRangeSelection &&
    maxRangeDuration &&
    selectedStartDate &&
    thisDay.valueOf() > selectedStartDate.valueOf()
  ) {
    if (Array.isArray(maxRangeDuration)) {
      const index = maxRangeDuration.findIndex(
        i => i.date === selectedStartDate.valueOf(),
      );
      if (
        index >= 0 &&
        selectedStartDate.valueOf() +
          maxRangeDuration[index].maxDuration * 86400000 <
          thisDay.valueOf()
      ) {
        dateIsAfterMaxDuration = true;
      }
    } else if (
      selectedStartDate.valueOf() + maxRangeDuration * 86400000 <
      thisDay.valueOf()
    ) {
      dateIsAfterMaxDuration = true;
    }
  }

  dateOutOfRange =
    dateIsAfterMax ||
    dateIsBeforeMin ||
    dateIsBeforeMinDuration ||
    dateIsAfterMaxDuration;

  const isSelectedStart = selectedStartDate ? true : false;
  const isSelectedEnd = selectedEndDate ? true : false;
  const isThisDayBetween = thisDay.isBetween(
    selectedStartDate,
    selectedEndDate,
    'day',
  );

  if (!dateOutOfRange && !dateIsDisabled) {
    const isToday = thisDay.isSame(today, 'day');
    const isThisDaySameAsSelectedStart = thisDay.isSame(
      selectedStartDate,
      'day',
    );
    const isThisDaySameAsSelectedEnd = thisDay.isSame(selectedEndDate, 'day');

    const handlePressDay = () => {
      onPressDay(day);
    };

    return (
      <CalendarDayStyled>
        <CalendarSelectableDayStyled
          scaler={scaler}
          today={isToday}
          start={isThisDaySameAsSelectedStart}
          between={isThisDayBetween}
          end={isThisDaySameAsSelectedEnd}
          allowRange={allowRangeSelection}
          startSelected={isSelectedStart}
          endSelected={isSelectedEnd}
          onPress={handlePressDay}>
          <CalendarDayTextStyled
            today={isToday}
            start={isThisDaySameAsSelectedStart}
            between={isThisDayBetween}
            end={isThisDaySameAsSelectedEnd}>
            {day}
          </CalendarDayTextStyled>
        </CalendarSelectableDayStyled>
      </CalendarDayStyled>
    );
  } else {
    // dateOutOfRange = true
    return (
      <CalendarDayStyled>
        <CalendarOutOfRangeDayStyled
          outOfRange={dateOutOfRange && isSelectedEnd && isThisDayBetween}
          scaler={scaler}>
          <CalendarDayDisabledStyled>{day}</CalendarDayDisabledStyled>
        </CalendarOutOfRangeDayStyled>
      </CalendarDayStyled>
    );
  }
};

export const CalendarEmptyDay = (props: any) => (
  <CalendarOutOfRangeDayStyled outOfRange={false} scaler={props.scaler} />
);
