import styled from 'styled-components/native';
import { Color, InputDisabled, InputValue, Spacing } from '@atomic';

const TODAY_BG_COLOR = Color.GrayLight;
const SELECTED_BG_COLOR = Color.Success;

export interface CalendarSelectableProps {
  scaler: number;
  today: boolean;
  start: boolean;
  between: boolean;
  end: boolean;
  allowRange: boolean;
  startSelected: boolean;
  endSelected: boolean;
}

export interface CalendarOutOfRangeProps {
  outOfRange: boolean;
  scaler: number;
}

export const CalendarSelectableDayStyled = styled.TouchableOpacity`
  width: ${(props: CalendarSelectableProps) => Math.round(50 * props.scaler)};
  height: ${(props: CalendarSelectableProps) => Math.round(40 * props.scaler)};
  align-self: center;
  justify-content: center;
  background-color: ${(props: CalendarSelectableProps) =>
    props.start || props.end || props.between
      ? SELECTED_BG_COLOR
      : props.today
      ? TODAY_BG_COLOR
      : 'transparent'};
  border-top-left-radius: ${(props: CalendarSelectableProps) =>
    (props.startSelected && props.start) ||
    (!props.start && !props.between && !props.end && props.today)
      ? 40
      : 0};
  border-bottom-left-radius: ${(props: CalendarSelectableProps) =>
    (props.startSelected && props.start) ||
    (!props.start && !props.between && !props.end && props.today)
      ? 40
      : 0};
  border-top-right-radius: ${(props: CalendarSelectableProps) =>
    (props.endSelected && props.end) ||
    (props.startSelected && !props.endSelected) ||
    (!props.start && !props.between && !props.end && props.today)
      ? 40
      : 0};
  border-bottom-right-radius: ${(props: CalendarSelectableProps) =>
    (props.endSelected && props.end) ||
    (props.startSelected && !props.endSelected) ||
    (!props.start && !props.between && !props.end && props.today)
      ? 40
      : 0};
`;

export const CalendarOutOfRangeDayStyled = styled.View`
  width: ${(props: CalendarOutOfRangeProps) => Math.round(50 * props.scaler)};
  height: ${(props: CalendarOutOfRangeProps) => Math.round(40 * props.scaler)};
  align-self: center;
  justify-content: center;
  background-color: ${(props: CalendarOutOfRangeProps) =>
    props.outOfRange ? SELECTED_BG_COLOR : 'transparent'};
`;

export const CalendarDayStyled = styled.View`
  align-items: center;
  justify-content: center;
  padding-top: ${Spacing.Small};
  background-color: transparent;
`;

export const CalendarDayTextStyled = styled(InputValue)`
  text-align: center;
  justify-content: center;
  align-items: center;
  color: ${(props: CalendarSelectableProps) =>
    props.start || props.end || props.between
      ? '#fff'
      : props.today
      ? '#ffff'
      : '#000'};
`;

export const CalendarDayDisabledStyled = styled(InputDisabled)`
  text-align: center;
  color: ${Color.GrayLight};
`;
