import styled from 'styled-components/native';
import { Color, Spacing } from '@atomic/obj.constants';

interface CalendarWeekdayTextProps {
  scaler: number;
}

export const CalendarWeekdaysStyled = styled.View`
  flex-direction: row;
  border-bottom-width: 1;
  border-top-width: 1;
  padding-top: ${Spacing.Small};
  padding-bottom: ${Spacing.Small};
  align-self: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0);
  border-color: rgba(0, 0, 0, 0.2);
`;

export const CalendarWeekdayTextStyled = styled.Text`
  width: ${(props: CalendarWeekdayTextProps) => 50 * props.scaler};
  font-size: ${(props: CalendarWeekdayTextProps) => 12 * props.scaler};
  color: ${Color.Black};
  text-align: center;
`;
