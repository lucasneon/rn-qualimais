import moment, { Moment } from 'moment';
import * as React from 'react';
import 'react-native-get-random-values';
import uuid, { v4 } from 'uuid';
import { CalendarDay, CalendarEmptyDay } from './calendar-day.component';
import { CalendarGridStyled, CalendarGridWeekStyled } from './calendar-grid.component.style';

const getDaysInMonth = (month, year) => {
  const lastDayOfMonth = new Date(year, month + 1, 0);
  return lastDayOfMonth.getDate();
};

export interface CalendarGridProps {
  month: number;
  year: number;
  allowRangeSelection: boolean;
  startFromMonday?: boolean;
  minDate: Moment;
  maxDate: Moment;
  disabledDates;
  minRangeDuration;
  maxRangeDuration;
  onPressDay?: (day: number) => void;
  selectedStartDate: Moment;
  selectedEndDate: Moment;
  scaler: number;
}
export const CalendarGrid: React.SFC<CalendarGridProps> = (props: CalendarGridProps) => {
  const {
    month,
    year,
    onPressDay,
    startFromMonday,
    selectedStartDate,
    selectedEndDate,
    allowRangeSelection,
    minDate,
    maxDate,
    disabledDates,
    minRangeDuration,
    maxRangeDuration,
    scaler,
  } = props;

  // let's get the total of days in this month, we need the year as well, since
  // leap years have different amount of days in February
  const totalDays = getDaysInMonth(month, year);
  // Let's create a date for day one of the current given month and year
  const firstDayOfMonth = moment({ year, month, day: 1 });
  // isoWeekday() gets the ISO day of the week with 1 being Monday and 7 being Sunday.
  // We will need this to know what day of the week to show day 1
  const firstWeekDay = firstDayOfMonth.isoWeekday();
  // fill up an array of days with the amount of days in the current month
  const days = Array.apply(null, {length: totalDays}).map(Number.call, Number);
  const guideArray = [ 0, 1, 2, 3, 4, 5, 6 ];

  // Get the starting index, based upon whether we are using monday or sunday as first day.
  const startIndex = (startFromMonday) ? (firstWeekDay - 1) % 7 : firstWeekDay;

  function generateColumns(i) {
    const column = guideArray.map(index => {
      if (i === 0) { // for first row, let's start showing the days on the correct weekday
        if (index >= startIndex) {
          if (days.length > 0) {
            const day = days.shift() + 1;
            return (
              <CalendarDay
                scaler={scaler}
                key={day}
                day={day}
                month={month}
                year={year}
                onPressDay={onPressDay}
                selectedStartDate={selectedStartDate}
                selectedEndDate={selectedEndDate}
                allowRangeSelection={allowRangeSelection}
                minDate={minDate}
                maxDate={maxDate}
                disabledDates={disabledDates}
                minRangeDuration={minRangeDuration}
                maxRangeDuration={maxRangeDuration}
              />
            );
          } else {
            return null;
          }
        } else {
          return <CalendarEmptyDay key={v4()} scaler={scaler}>Emp</CalendarEmptyDay>;
        }
      } else {
        if (days.length > 0) {
          const day = days.shift() + 1;
          return (
            <CalendarDay
              scaler={scaler}
              key={day}
              day={day}
              month={month}
              year={year}
              onPressDay={onPressDay}
              selectedStartDate={selectedStartDate}
              selectedEndDate={selectedEndDate}
              allowRangeSelection={allowRangeSelection}
              minDate={minDate}
              maxDate={maxDate}
              disabledDates={disabledDates}
              minRangeDuration={minRangeDuration}
              maxRangeDuration={maxRangeDuration}
            />
          );
        } else {
          return null;
        }
      }
    });
    return column;
  }
  return (
    <CalendarGridStyled>
      { guideArray.map(index => (
          <CalendarGridWeekStyled key={index}>
            {generateColumns(index)}
          </CalendarGridWeekStyled>
        ))
      }
    </CalendarGridStyled>
  );
};
