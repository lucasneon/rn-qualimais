import styled from 'styled-components/native';
import { H3 } from '@atomic';
import { Spacing } from '@atomic/obj.constants';

export const CalendarHeaderStyled = styled.View`
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  align-self: stretch;
  background-color: transparent;
  padding-horizontal: ${Spacing.Small};
`;

export const CalendarHeaderTitleStyled = styled(H3)`
  text-align: center;
`;
