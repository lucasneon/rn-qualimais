import * as React from 'react';
import { CalendarString } from './calendar.strings';
import { CalendarHeaderStyled, CalendarHeaderTitleStyled } from './calendar-header.component.style';
import { LinkButton } from '@atomic/atm.button';
import { Asset } from '@atomic';

interface HeaderControlsProps {
  currentMonth: number;
  currentYear: number;
  onPressNext: () => void;
  onPressPrevious: () => void;
}

export const CalendarHeader: React.SFC<HeaderControlsProps> = (props: HeaderControlsProps) => {
  const {
    currentMonth,
    currentYear,
    onPressNext,
    onPressPrevious,
  } = props;

  const month = CalendarString.Months[currentMonth];
  const year = currentYear;

  // style={[styles.monthLabel, textStyle]}>
  return (
    <CalendarHeaderStyled>
      <LinkButton
        text='Anterior'
        onTap={onPressPrevious}
        image={Asset.Icon.Atomic.ChevronLeft}
      />
      <CalendarHeaderTitleStyled>
          {month} {year}
      </CalendarHeaderTitleStyled>
      <LinkButton
        text='Próximo'
        onTap={onPressNext}
        image={Asset.Icon.Atomic.ChevronRight}
        imageOnRight={true}
      />
    </CalendarHeaderStyled>
  );
};
