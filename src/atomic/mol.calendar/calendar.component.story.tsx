import * as React from 'react';
import * as moment from 'moment';
import { Calendar } from './calendar.component';
import { H2, Scroll, VBox } from '@atomic';
import { storiesOf } from '@storybook/react-native';
import { VSeparator } from '@atomic/obj.grid';
const stories = storiesOf('Molecules', module);

stories.add('Calendar', () => (
  <Scroll>
    <VBox>
      <H2>Range selection calendar</H2>
      <Calendar
        allowRangeSelection={true}
        minRangeDuration={3}
        disabledDates={[moment('2018/07/13')]}
      />
    </VBox>
    <VSeparator />
    <VBox>
      <H2>Single selection calendar</H2>
      <Calendar
        minDate={moment().subtract({days: 7})}
        maxDate={moment().add({days: 7})}
        disabledDates={[moment('2018/07/13')]}
      />
    </VBox>
    <VSeparator />
  </Scroll>
));
