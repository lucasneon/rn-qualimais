import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components/native';

export const ScrollStyled = styled.ScrollView`
`;

export const KeyboardAwareScrollStyled: any = ScrollStyled.withComponent(KeyboardAwareScrollView);
