import * as React from 'react';

import { VBox } from '@atomic/obj.grid';

import { H2 } from '@atomic/atm.typography';
import { Scroll } from '@atomic/obj.scroll';
import { storiesOf } from '@storybook/react-native';
import styled from 'styled-components/native';

const ScrollSample: any = styled.View`
  height: 1000;
  background-color: gray;
  align-self: stretch;
`;

const stories = storiesOf('Objects', module);

stories.add('Scroll', () => (
  <Scroll>
    <VBox>
      <H2>Scroll</H2>
      <ScrollSample />
    </VBox>
  </Scroll>
));
