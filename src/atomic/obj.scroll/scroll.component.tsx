import * as React from 'react';
import { findNodeHandle, ScrollView, ScrollViewProps, UIManager } from 'react-native';
import { KeyboardAwareScrollStyled, ScrollStyled } from './scroll.component.style';

export interface ScrollProps extends ScrollViewProps {
  keyboardAware?: boolean;

}
export class Scroll extends React.Component<ScrollProps, any> {
  static defaultProps = {
    keyboardAware: true,
  };
  private scrollView: ScrollView | any; // Aparently KeyboardAwareScrollView methods are not mapped to Typescript.

  constructor(props: ScrollProps) {
    super(props);
  }

  scrollTo(x: number, y: number) {
    if (this.props.keyboardAware) {
      this.scrollView.scrollToPosition(x, y, true);
    } else {
      this.scrollView.scrollTo({x, y, animated: true});
    }
  }

  scrollToComponent(component: React.Component): void {
    if (!component) {
      return;
    }

    UIManager.measureLayout(
      findNodeHandle(component),
      findNodeHandle(this),
      () => {
        console.warn('Error scrolling');
      },
      (_x: number, y: number) => {
        this.scrollTo(0, y - 50);
    });
  }

  render() {

    const { keyboardAware, ...props } = this.props;

    if (keyboardAware) {
      return (
        <KeyboardAwareScrollStyled
          enableResetScrollToCoords={false}
          ref={ref => this.scrollView = ref}
          {...props}
        >
          {this.props.children}
        </KeyboardAwareScrollStyled>
      );
    }

    return (
      <ScrollStyled ref={ref => this.scrollView = ref} {...props}>
        {this.props.children}
      </ScrollStyled>
      );
  }
}
