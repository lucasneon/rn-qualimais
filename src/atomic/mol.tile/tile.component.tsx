import * as React from 'react';
import { DD, H4 } from '@atomic/atm.typography';
import { TileImageBackgroundStyled, TileStyled } from './tile.component.style';
import { VSeparator } from '@atomic/obj.grid';
export interface TileProps {
  brandLogo?: any;
  text: string;
  description?: string;
  onTap?: () => any;
}

export class Tile extends React.Component<TileProps, any> {
  constructor(props: TileProps) {
    super(props);
  }

  render() {
    return (
      <TileStyled>
        <TileImageBackgroundStyled />
        <VSeparator />
        <H4>{this.props.text}</H4>
        <VSeparator />
        <DD>{this.props.description}</DD>
      </TileStyled>
    );
  }
}
