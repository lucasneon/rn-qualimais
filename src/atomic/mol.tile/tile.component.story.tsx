import * as React from 'react';

import { VBox } from '@atomic/obj.grid';

import { Tile } from '@atomic/mol.tile';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Molecules', module);
const action = () => console.log('Button pressed');

stories.add('Tiles and cards', () => (
  <VBox>
    <Tile
      text='This is a clickable tile'
      description='This is a description. Testing multiple lines'
      onTap={action}
    />
  </VBox>
));
