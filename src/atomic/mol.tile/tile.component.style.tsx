import { Color, ScreenUnit, Spacing } from '@atomic/obj.constants';

import styled from 'styled-components/native';

export const TileStyled = styled.TouchableOpacity`
  padding: ${Spacing.Medium + ScreenUnit};
  width: 204;
  height: 294;
  border-width: 1;
  border-radius: 4;
  border-color: ${Color.GrayLight};
`;

export const TileImageBackgroundStyled = styled.View`
  width: 100%;
  height: 90;
  background-color: black;
`;
