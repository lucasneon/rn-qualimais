import * as React from 'react';
import { ImageURISource } from 'react-native';
import { InputLabel } from '@atomic/atm.typography';
import { FormFieldCaption } from './form-field-caption.component';
import { FormContext, FormContextState } from './form.component';
import { ValidationError, Validator } from './validator.model';

export type FieldValuePrimitives = string | number | boolean | ImageURISource;
export type FieldValue = FieldValuePrimitives | FieldValuePrimitives[];

export interface FormFieldContexState {
  value: any;
  other: any;
  onValueChange: (value: any, others: any) => void;
  onFocusChange: (focus: boolean) => void;
  touched: boolean;
  dirty: boolean;
  focus: boolean;
  errors: any[];
}

export const FormFieldContext = React.createContext<FormFieldContexState>(null);

export interface FormFieldProps {
  name: string;
  label?: string;
  value?: any;
  onValueChange?: (value: any, others: any) => void;
  validators?: Validator[];
  validatorPlaceholder?: string;
}

export class FormField extends React.Component<FormFieldProps, FormFieldContexState> {

  private formContext: FormContextState;

  constructor(props) {
    super(props);

    this.state = {
      other: undefined,
      value: props.value,
      errors: [],
      touched: false,
      dirty: false,
      focus: false,
      onValueChange: this.handleValueChange,
      onFocusChange: this.handleFocusChange,
    };
  }

  componentDidMount() {
    return this.formContext && this.formContext.register(this);
  }

  componentWillUnmount() {
    return this.formContext && this.formContext.unregister(this);
  }

  render() {
    return (
      <FormContext.Consumer>
        {(formContext: FormContextState) => {
          this.formContext = formContext;
          return (
            <FormFieldContext.Provider value={this.state}>
              {
                this.props.label &&
                <InputLabel hasError={this.state.errors.length > 0}>
                  {this.props.label}
                </InputLabel>
              }
              {this.props.children}
              {
                this.props.validatorPlaceholder && this.state.errors.length === 0 &&
                <FormFieldCaption validationPlaceholder={this.props.validatorPlaceholder} />
              }
              <FormFieldCaption errors={this.state.errors} />
            </FormFieldContext.Provider>
          );
        }}
      </FormContext.Consumer>
    );
  }

  validate(value: any) {
    const errors: ValidationError[] = [];

    // Pass it to each validator
    if (this.props.validators && this.props.validators.length > 0) {
      for (const validator of this.props.validators) {

        // Add to validation array if errors
        if (!validator.validationFn(value)) {
          errors.push(validator.error);
        }
      }
    }

    return this.setState({ errors });
  }

  private handleValueChange = (value: any, other: any) => {

    let val = value;
    let oth = other;
    // Its a checkbox, must verrify if checked/unchecked from the "other" parameter
    if (Array.isArray(value)) {
      val = this.state.value || [];
      oth = this.state.other || {};
      const checkboxValue = value[0];
      const index = val.indexOf(checkboxValue, 0);

      if (other && index < 0) {
        val.push(checkboxValue);
      }

      if (!other && index > -1) {
        val.splice(index, 1);
      }
      oth[checkboxValue] = other;
    }

    this.validate(val);
    this.setState({ value: val, other: oth, dirty: true });

    if (this.props.onValueChange) {
      this.props.onValueChange(val, oth);
    }
  }

  private handleFocusChange = (focus: boolean) => {
    this.setState({ focus });
    if (!focus) {
      this.validate(this.state.value);
      this.setState({ touched: true });
    }
  }
}
