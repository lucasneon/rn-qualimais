import { FieldValue } from './form-field.component';

export interface Validator {
  error: ValidationError;
  validationFn: ValidationFunction;
}

export type ValidationFunction = (value: FieldValue) => boolean;

export interface ValidationError {
  name: string;
  message: string;
}
