import * as React from 'react';
import { AutoFormSample } from '@atomic/pag.form-sample';
import { Form, Validators } from '@atomic/obj.form';
import { H2 } from '@atomic/atm.typography';
import { storiesOf } from '@storybook/react-native';
import { TextField } from '@atomic/atm.text-field';
import { VBox } from '@atomic/obj.grid';

const stories = storiesOf('Validators', module);

stories.add('Email', () => (
  <AutoFormSample>
    <VBox>
      <H2>Email Validator</H2>
      <Form.Field
        name='email'
        label='Type a valid email'
        validators={[Validators.IsEmail('Not a valid email')]}
        validatorPlaceholder='Checks if the filled textfield is a valid e-mail'
      >
        <TextField type={TextField.Type.Email} />
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
