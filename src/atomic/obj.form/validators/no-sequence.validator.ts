import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';

export function NoSequence(length: number, errorMessage: string): Validator {

  return {
    error: {
      name: 'NoSequence',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string') {
        const hasSequence = getForbiddenSequences(length).some(sequence => value.includes(sequence));

        return !hasSequence;
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    }),
  };
}

function reverseString(str: string) {
  return str.split('').reverse().join('');
}

function getForbiddenSequences(length: number): string[] {
  const digits = '0123456789';
  const forbiddenSequences = [];

  for (let i = 0 ; i + length <= digits.length ; i++) {
      const sequence = digits.substr(i, length);

      forbiddenSequences.push(sequence);
      forbiddenSequences.push(reverseString(sequence));
  }

  return forbiddenSequences;
}
