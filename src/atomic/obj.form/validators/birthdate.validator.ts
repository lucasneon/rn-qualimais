import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';

const BIRTH_DATE_REGEX = /^((0?[1-9]|[12][0-9]|3[01])[/](0?[1-9]|1[012])[/](19|20)[0-9]{2})*$/;

export function Birthdate(errorMessage: string): Validator {
  return {
    error: {
      name: 'Birthdate',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string') {
        return BIRTH_DATE_REGEX.test(value);
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    }),
  };
}
