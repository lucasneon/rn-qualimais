import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';

export function MaxDateTime(max: Date): Validator {
  return {
    error: {
      name: 'MaxDateTime',
      message: 'Acima do máximo',
    },
    validationFn: (value: FieldValue): boolean => {
      if (value && Object.prototype.toString.call(value) === '[object Date]') {
        return value <= max;
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    },
  };
}
