import * as React from 'react';

import { VBox, VSeparator } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { H2, InputLabel } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Validators', module);

stories.add('EqualsValue', () => (
  <ValidatorEqualsValueSample />
));

class ValidatorEqualsValueSample extends React.Component<any, any> {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
    };
  }

  onEmailChange = (newEmail: string) => {
    this.setState({email: newEmail});
  }

  render() {
    return (
      <AutoFormSample>
        <VBox>
          <H2>Equals Value Validator</H2>
          <InputLabel>E-mail</InputLabel>
          <TextField type={TextField.Type.Email} onChangeText={this.onEmailChange} />
          <VSeparator />
          <Form.Field
            name='email_confirmation'
            label='E-mail confirmation'
            validators={[Validators.EqualsValue(this.state.email, 'Not equal')]}
            validatorPlaceholder='Checks if the filled data is equal to an specific value. e.g.: Another field'
          >
            <TextField type={TextField.Type.Email} />
          </Form.Field>
        </VBox>
      </AutoFormSample>
    );
  }
}
