import * as React from 'react';

import { VBox } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { H2 } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Validators', module);

stories.add('Has Letter', () => (
  <AutoFormSample>
    <VBox>
      <H2>HasLetter Validator</H2>
      <Form.Field
        name='password'
        label='Password (at least one letter)'
        validators={[Validators.HasLetter('Must have at least one letter')]}
        validatorPlaceholder='Checks if the filled textfield has at least one letter'
      >
        <TextField
          type={TextField.Type.Normal}
          placeholder='Password must have a letter'
        />
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
