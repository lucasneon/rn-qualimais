import * as React from 'react';

import { VBox } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { H2 } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Validators', module);

const MAX_FREQUENCY = 3;

stories.add('Max Digit Frequency', () => (
  <AutoFormSample>
    <VBox>
      <H2>Max Digit Frequency Validator</H2>
      <Form.Field
        name='password'
        label='Repeated chars'
        validators={[Validators.MaxDigitFrequency(
          MAX_FREQUENCY,
          `Must not contain more or equal than ${MAX_FREQUENCY} repeated numbers`,
        )]}
        validatorPlaceholder='Checks if the filled textfield does not have a count of repeated numbers'
      >
        <TextField
          type={TextField.Type.Normal}
          placeholder='Type in numbers'
        />
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
