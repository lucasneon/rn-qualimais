import * as React from 'react';

import { VBox } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { Body, H2 } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Validators', module);

const SEQUENCE_LENGTH = 3;

stories.add('No Sequence', () => (
  <AutoFormSample>
    <VBox>
      <H2>No Sequence Validator</H2>
      <Form.Field
        name='password'
        label='Dont repeat 3 characters in sequence'
        validators={[Validators.NoSequence(
          SEQUENCE_LENGTH,
          `Must not contain a sequence of ${SEQUENCE_LENGTH} consecutive numbers`,
        )]}
        validatorPlaceholder='Checks if the filled textfield does not have a sequence of numbers of defined length*'
      >
        <TextField
          type={TextField.Type.Numeric}
          placeholder='Type in numbers'
        />
      </Form.Field>
      <Body>* Sequences can be in ascending (123) or descending (321)</Body>
    </VBox>
  </AutoFormSample>
));
