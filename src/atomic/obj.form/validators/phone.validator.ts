import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';
import { strings } from '../validator.string';

// tslint:disable-next-line
const PHONE_REGEX = /^((\+55 )*(\(\d{2}\) ((\d{4}-\d{4,5})|\d{5}-\d{3,4}))|\d{12,13})$/i;

export function IsPhone(errorMessage: string = strings.defaultCaptions.phone): Validator {
  return {
    error: {
      name: 'IsPhone',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue) => {
      if (typeof value === 'string') {
        return PHONE_REGEX.test(value);
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    }),
  };
}
