import CPF from 'cpf-check';
import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';
import { strings } from '../validator.string';

export function Cpf(errorMessage: string = strings.defaultCaptions.cpf): Validator {
  return {
    error: {
      name: 'Cpf',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string') {
        return CPF.validate(value);
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    }),
  };
}
