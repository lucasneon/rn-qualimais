import * as React from 'react';

import { VBox } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { H2, InputLabel } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Validators', module);

stories.add('Phone', () => (
  <AutoFormSample>
    <VBox>
      <H2>Email Validator</H2>
      <InputLabel>TextField</InputLabel>
      <Form.Field
        name='phone'
        label='Type a valid phone number'
        validators={[Validators.IsPhone('Not a valid telephone')]}
        validatorPlaceholder='Checks if the filled textfield is a valid telephone'
      >
        <TextField
          type={TextField.Type.CellPhone}
          placeholder='e.g.: (xx) zyyyy-yyyy'
        />
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
