import * as React from 'react';

import { VBox } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { H2 } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Validators', module);

stories.add('Has Number', () => (
  <AutoFormSample>
    <VBox>
      <H2>HasNumber Validator</H2>
      <Form.Field
        name='password'
        label='Password (at least one number)'
        validators={[Validators.HasNumber('Must have at least one number')]}
        validatorPlaceholder='Checks if the filled textfield has at least one number'
      >
        <TextField
          type={TextField.Type.Normal}
          placeholder='type in your password'
        />
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
