import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';

const HAS_NUMBER_REGEX = /\d/i;

export function HasNumber(errorMessage: string): Validator {
  return {
    error: {
      name: 'HasNumber',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string') {
        return HAS_NUMBER_REGEX.test(value);
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    }),
  };
}
