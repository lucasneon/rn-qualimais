import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';

export function ExactLength(length: number, errorMessage: string): Validator {
  return {
    error: {
      name: 'ExactLength',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string' || Array.isArray(value)) {
        return value.length === length;
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    }),
  };
}
