import * as React from 'react';

import { CheckboxField } from '@atomic/atm.checkbox';
import { VBox, VSeparator } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { H2 } from '@atomic/atm.typography';
import { RadioField } from '@atomic/atm.radio';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { ImagePicker } from '@atomic/mol.image-picker';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

enum RadioOptions {
  first,
  second,
}

const stories = storiesOf('Validators', module);

stories.add('Required', () => (
  <AutoFormSample>
    <VBox>
      <H2>Required Validator</H2>
      <Form.Field
        name='textfield'
        label='Required text'
        validators={[Validators.Required('Required')]}
        validatorPlaceholder='Checks if any text is filled (i.e.: not empty)'
      >
        <TextField type={TextField.Type.Normal} maxLength={10} />
      </Form.Field>
      <VSeparator />
      <Form.Field
        name='radio'
        label='Required radio'
        validators={[Validators.Required('Required radio')]}
        validatorPlaceholder='Checks if any option is selected'
      >
        <RadioField id={RadioOptions.first}>Value 1</RadioField>
        <RadioField id={RadioOptions.second}>Value 2</RadioField>
      </Form.Field>
      <VSeparator />
      <Form.Field
        name='checkbox_group'
        label='Required check'
        validators={[Validators.Required('Required')]}
        validatorPlaceholder='Checks if one or more options are checked/selected'
      >
        <CheckboxField id={1}>Value 1 label</CheckboxField>
        <CheckboxField id={2}>Value 2 label</CheckboxField>
        <CheckboxField id={3}>Value 3 label</CheckboxField>
      </Form.Field>
      <VSeparator />
      <Form.Field
        name='image_picker'
        label='Required image picker'
        validators={[Validators.Required('Required')]}
        validatorPlaceholder='Checks if an image is selected'
      >
        <ImagePicker caption='Document Photo'/>
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
