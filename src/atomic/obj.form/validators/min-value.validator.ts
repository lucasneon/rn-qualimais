import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';

export function MinValue(min: number, errorMessage: string): Validator {
  return {
    error: {
      name: 'MinValue',
      message: errorMessage,
    },
    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string') {
        return parseInt(value, 10) >= min;
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    }),
  };
}
