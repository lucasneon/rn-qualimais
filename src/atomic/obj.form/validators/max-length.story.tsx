import * as React from 'react';

import { CheckboxField } from '@atomic/atm.checkbox';
import { VBox, VSeparator } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { H2 } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Validators', module);

const LENGTH_TO_CHECK = 5;

stories.add('Length: Max', () => (
  <AutoFormSample>
    <VBox>
      <H2>Max Length Validator</H2>
      <Form.Field
        name='normal'
        label='Select up to 5 items'
        validators={[
          Validators.MaxLength(LENGTH_TO_CHECK, `Field must have no more than ${LENGTH_TO_CHECK} chars`),
        ]}
        validatorPlaceholder='Checks if the filled textfield has less than defined length'
      >
        <TextField
          type={TextField.Type.Normal}
          placeholder='Type anything here'
        />
      </Form.Field>
      <VSeparator />
      <Form.Field
        name='checkbox'
        label='Select up to 5 items'
        validators={[
          Validators.MaxLength(LENGTH_TO_CHECK, `Must select a maximum of ${LENGTH_TO_CHECK} options`),
        ]}
        validatorPlaceholder='Checks if a maximum number of options are checked/selected'
      >
        <CheckboxField id={1}>Value 1 label</CheckboxField>
        <CheckboxField id={2}>Value 2 label</CheckboxField>
        <CheckboxField id={3}>Value 3 label</CheckboxField>
        <CheckboxField id={4}>Value 4 label</CheckboxField>
        <CheckboxField id={5}>Value 5 label</CheckboxField>
        <CheckboxField id={6}>Value 6 label</CheckboxField>
        <CheckboxField id={7}>Value 7 label</CheckboxField>
        <CheckboxField id={8}>Value 8 label</CheckboxField>
        <CheckboxField id={9}>Value 9 label</CheckboxField>
      </Form.Field>

    </VBox>
  </AutoFormSample>
));
