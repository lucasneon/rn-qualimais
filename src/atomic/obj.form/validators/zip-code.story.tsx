import * as React from 'react';

import { VBox } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { H2 } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Validators', module);

stories.add('Zip code', () => (
  <AutoFormSample>
    <VBox>
      <H2>Zip code Validator</H2>
      <Form.Field
        name='zip-code'
        label='Type a valid zipcode'
        validators={[Validators.ZipCode('Not a valid zip-code')]}
        validatorPlaceholder='Checks if the filled data is a VALID zip-code'
      >
        <TextField type={TextField.Type.ZipCode}/>
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
