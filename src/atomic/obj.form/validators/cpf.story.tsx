import * as React from 'react';

import { VBox } from '@atomic/obj.grid';
import { Validators } from '@atomic/obj.form';
import { H2 } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { storiesOf } from '@storybook/react-native';
import { Form, TextField } from '@atomic';

const stories = storiesOf('Validators', module);

stories.add('CPF', () => (
  <AutoFormSample>
    <VBox>
      <H2>Cpf Validator</H2>
      <Form.Field
        name='cpf'
        label='Type a valid CPF'
        validators={[Validators.Cpf('Not a valid CPF')]}
        validatorPlaceholder='Checks if the filled data is a VALID cpf'
      >
        <TextField type={TextField.Type.CPF} />
      </Form.Field>

    </VBox>
  </AutoFormSample>
));
