import { FieldValue } from '../form-field.component';
import { ValidationFunction } from '../validator.model';

// Wraps a validation function ignoring empty values

export function ignoreEmpty(validationFn: ValidationFunction): ValidationFunction {
  return (value: FieldValue): boolean => {
    // General null & undefined
    if (value === null || value === undefined) {
      return true;
    }

    // Strings
    if (typeof value === 'string' && value.length === 0) {
      return true;
    }

    // Arrays
    if (Array.isArray(value) && value.length === 0) {
      return true;
    }

    // Empty objects (considering JS quirk: typeof null === 'object')
    if (typeof value === 'object' && value && Object.keys(value).length === 0) {
      return true;
    }

    // If not-empty, return intended validation function
    return validationFn(value);
  };
}
