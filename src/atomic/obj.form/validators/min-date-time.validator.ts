import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';

export function MinDateTime(min: Date): Validator {
  return {
    error: {
      name: 'MinDateTime',
      message: 'Abaixo do mínimo',
    },
    validationFn: (value: FieldValue): boolean => {
      if (value && Object.prototype.toString.call(value) === '[object Date]') {
        return value >= min;
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    },
  };
}
