import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';
import { strings } from '../validator.string';

// tslint:disable-next-line
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function IsEmail(errorMessage: string = strings.defaultCaptions.email): Validator {
  return {
    error: {
      name: 'IsEmail',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string') {
        return EMAIL_REGEX.test(value);
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    }),
  };
}
