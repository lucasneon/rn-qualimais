import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import moment from 'moment';

export function MaxDate(max: Date): Validator {
  return {
    error: {
      name: 'MaxDate',
      message: 'Acima do máximo',
    },
    validationFn: (value: FieldValue): boolean => {
      if (value && Object.prototype.toString.call(value) === '[object Date]') {
        return value <= moment(max).set({h: 0, m: 0, s: 0, ms: 0});
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    },
  };
}
