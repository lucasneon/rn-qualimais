import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';

const HAS_LETTER_REGEX = /[a-z]/i;

export function HasLetter(errorMessage: string): Validator {
  return {
    error: {
      name: 'HasLetter',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string') {
        return HAS_LETTER_REGEX.test(value);
      }

      // Raises error if trying to validate worng type/kind of field
      return false;
    }),
  };
}
