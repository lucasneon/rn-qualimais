import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';

const charsToTest = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

export function MaxDigitFrequency(maxFrequency: number, errorMessage: string): Validator {
  return {
    error: {
      name: 'MaxDigitFrequency',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string') {
        const isOverLimit = charsToTest.some(char => countFrequency(value, char) > maxFrequency);

        return !isOverLimit;
      }

      // Raises error if trying to validate worng type/kind of field
      return false;
    }),
  };
}

function countFrequency(value: string, char: string): number {
  const matches = value.match(new RegExp(char, 'g'));
  return (matches || []).length;
}
