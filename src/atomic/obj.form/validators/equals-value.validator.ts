import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';

export function EqualsValue(toMatch: any, errorMessage: string): Validator {
  return {
    error: {
      name: 'EqualsValue',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => value === toMatch),
  };
}
