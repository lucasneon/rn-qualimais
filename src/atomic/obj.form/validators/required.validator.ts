import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { strings } from '../validator.string';

export function Required(errorMessage: string = strings.defaultCaptions.required): Validator {
  return {
    error: {
      name: 'Required',
      message: errorMessage,
    },

    validationFn: (value: FieldValue) => {
      if (typeof value === 'boolean') {
        return value;
      }
      if (typeof value === 'number' && !Number.isNaN(value)) {
        return true;
      }
      if (typeof value === 'string' || Array.isArray(value)) {
        return value.length > 0;
      }
      // JS quirk: typeof null === 'object'
      if (typeof value === 'object' && value) {
        return true;
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    },
  };
}
