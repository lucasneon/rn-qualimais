import * as React from 'react';

import { VBox } from '@atomic/obj.grid';
import { Form, Validators } from '@atomic/obj.form';
import { H2 } from '@atomic/atm.typography';

import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic/atm.text-field';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Validators', module);

stories.add('Birthdate', () => (
  <AutoFormSample>
    <VBox>
      <H2>Birthdate Validator</H2>
      <Form.Field
        name='birthdate'
        label='Type a valid date'
        validators={[Validators.Birthdate('Not a date')]}
        validatorPlaceholder='Checks if the filled data is a date'
      >
        <TextField type={TextField.Type.Date} />
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
