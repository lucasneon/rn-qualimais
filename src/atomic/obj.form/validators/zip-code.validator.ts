import { FieldValue } from '../form-field.component';
import { Validator } from '../validator.model';
import { ignoreEmpty } from './ignore-empty.validator';
import { strings } from '../validator.string';

const ZIP_CODE_REGEX = /^(\d{5}[-]\d{3})|(\d{8})$/;

export function ZipCode(errorMessage: string = strings.defaultCaptions.zipCode): Validator {
  return {
    error: {
      name: 'ZipCode',
      message: errorMessage,
    },

    validationFn: ignoreEmpty((value: FieldValue): boolean => {
      if (typeof value === 'string') {
        return ZIP_CODE_REGEX.test(value);
      }

      // Raises error if trying to validate wrong type/kind of field
      return false;
    }),
  };
}
