import { InputCaption, InputCaptionError } from '@atomic/atm.typography';

import styled from 'styled-components/native';

export const FormFieldErrorCaptionStyled = styled(InputCaptionError)`
`;

export const FormFieldHelperCaptionStyled = styled(InputCaption) `
`;
