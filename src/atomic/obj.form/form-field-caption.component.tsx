import * as React from 'react';
import { View } from 'react-native';
import { Else, If } from '@atomic/obj.if';
import { FormFieldErrorCaptionStyled, FormFieldHelperCaptionStyled } from './form-field-caption.component.style';
import { ValidationError } from './validator.model';

export interface FormFieldCaptionProps {
  errors?: ValidationError[];
  showAll?: boolean;
  validationPlaceholder?: string;
}

export class FormFieldCaption extends React.PureComponent<FormFieldCaptionProps, any> {

  static defaultProps = {
    errors: [],
  };

  render() {
    const wrap = this.props.errors.map((error: ValidationError, index: number) => (
      <View key={error.name + index}>
        <FormFieldErrorCaptionStyled>
          {error.message}
        </FormFieldErrorCaptionStyled>
      </View>
    ));

    return (
      <If cond={this.props.showAll}>
        <View>{wrap}</View>
      <Else />
        <>
          {this.props.errors && this.props.errors.length > 0 ?
            <FormFieldErrorCaptionStyled key={this.props.errors[0].name}>
              {this.props.errors[0].message}
            </FormFieldErrorCaptionStyled>
          :
            this.props.validationPlaceholder ?
              <FormFieldHelperCaptionStyled>{this.props.validationPlaceholder}</FormFieldHelperCaptionStyled>
              : null
          }
        </>
      </If>
    );
  }
}
