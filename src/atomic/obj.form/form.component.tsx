import * as React from 'react';
import { Scroll as AtomicScroll, ScrollProps as AtomicScrollProps } from '@atomic';
import { FormField, FormFieldProps } from './form-field.component';
import { ValidationError } from './validator.model';

const Field: React.SFC<FormFieldProps> = (props: FormFieldProps) => <FormField {...props} />;
const Submit: React.SFC<any> = (props: any) => (
  <FormContext.Consumer>
    {(form: FormContextState) => {
      return React.Children.map(props.children, (child: any) => {
        return React.cloneElement(child, { onTap: form.submit });
      });
    }}
  </FormContext.Consumer>
);

interface ScrollProps extends AtomicScrollProps { children: any; }
const Scroll: React.SFC<ScrollProps> = (props: ScrollProps) => (
  <FormContext.Consumer>
    {(form: FormContextState) => (
      <AtomicScroll
        ref={ref => form.scroll = ref}
        keyboardShouldPersistTaps='handled'
        {...props}
      >
        {props.children}
      </AtomicScroll>
    )}
  </FormContext.Consumer>
);

export interface FormContextState {
  submit: () => void;
  register: (field: FormField) => void;
  unregister: (field: FormField) => void;
  scroll?: AtomicScroll;
}
export interface FormSubmitData {
  data: { [fieldName: string]: any; };
  other: { [fieldName: string]: any; };
  error: { [fieldName: string]: ValidationError };
}

export const FormContext = React.createContext<FormContextState>(null);

interface FormProps {
  onSubmit: (data: FormSubmitData) => void;
}
export class Form extends React.Component<FormProps, FormContextState> {
  static Field = Field;
  static Submit = Submit;
  static Scroll = Scroll;

  private fields: { [name: string]: FormField } = {};

  constructor(props) {
    super(props);

    this.state = {
      submit: this.handleSubmit,
      register: this.handleRegister,
      unregister: this.handleUnregister,
    };
  }

  render() {
    return (
      <FormContext.Provider value={this.state}>
        {this.props.children}
      </FormContext.Provider>
    );
  }

  private handleRegister = (field: FormField) => {
    this.fields[field.props.name] = field;
  }

  private handleUnregister = (field: FormField) => {
    delete this.fields[field.props.name];
  }

  private handleSubmit = async () => {
    const formData = await this.mapFormData();
    if (Object.keys(formData.error).length > 0 && this.state.scroll) {
      const key = Object.keys(formData.error)[0];
      const component = this.fields[key] as React.Component;
      this.state.scroll.scrollToComponent(component);
    }

    if (this.props.onSubmit) {
      this.props.onSubmit(formData);
    }
  }

  private mapFormData(): Promise<FormSubmitData> {
    const formData: FormSubmitData = { data: {}, other: {}, error: {} };

    return Promise.all(
      // Trigger validation on all the fields
      Object.keys(this.fields)
        .filter((fieldName: string) => this.fields[fieldName].validate)
        .map((fieldName: string) => this.fields[fieldName])
        .map((field: FormField) => field.validate(field.state.value)),
    ).then(() => {
      Object.keys(this.fields)
        .map((fieldName: string) => this.fields[fieldName])
        .map((field: FormField) => {
          formData.data[field.props.name] = field.state.value;
          formData.other[field.props.name] = field.state.other;

          if (field.state.errors) {
            field.state.errors.map((error: ValidationError) => {
              formData.error[field.props.name] = {
                name: error.name,
                message: error.message,
              };
            });
          }
        });

      return formData;
    });
  }
}
