// tslint:disable:max-line-length
export const strings = {
  defaultCaptions: {
    cpf: 'CPF inválido',
    email: 'E-mail inválido',
    phone: 'Telefone inválido',
    required: 'Campo obrigatório',
    zipCode: 'CEP inválido',
  },
};
// tslint:enable:max-line-length
