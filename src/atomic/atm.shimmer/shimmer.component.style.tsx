import styled from 'styled-components/native';
import { Border, Spacing } from '@atomic/obj.constants';
import { ShimmerProps } from './shimmer.component';

export const ShimmerStyled = styled.View`
  width: ${(props: ShimmerProps) => props.width ? props.width : '100%'};
  height: ${(props: ShimmerProps) => props.height ? props.height : Spacing.Medium};
  margin-bottom: ${Spacing.XSmall};
  margin-right: ${Spacing.XSmall};
  border-radius: ${(props: ShimmerProps) => props.rounded ? Border.RadiusLarge : 0};
  overflow: hidden;
`;

export const ShimmerSeparator = styled.View`
  width: 1;
  height: ${Spacing.Small};
`;
