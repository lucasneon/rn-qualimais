import * as React from 'react';
import { storiesOf } from '@storybook/react-native';

import { VBox, VSeparator } from '@atomic/obj.grid';
import { Shimmer, ShimmerIcon, ShimmerSeparator } from '@atomic/atm.shimmer';

const stories = storiesOf('Atoms', module);

stories.add('Shimmer', () => (
  <VBox>
    <VSeparator/>
    <Shimmer width={'80%'}/>
    <ShimmerSeparator />
    <Shimmer width={'20%'} />
    <VSeparator/>

    <ShimmerIcon />
    <ShimmerSeparator />
    <Shimmer width={50} />
    <VSeparator/>
  </VBox>
));
