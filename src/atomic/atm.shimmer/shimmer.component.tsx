import * as React from 'react';
import AnimatedLinearGradient from 'react-native-animated-linear-gradient';
import { Color, IconSize } from '@atomic/obj.constants';
import { ShimmerStyled } from './shimmer.component.style';

export interface ShimmerProps {
  width?: number | string;
  height?: number | string;
  rounded?: boolean;
}

const SHIMMER_PROPS = {
  customColors: [Color.GrayLight, Color.GrayLight, Color.White, Color.GrayLight],
  points: {
    start: {x: 0, y: 0.6},
    end: {x: 1, y: 0.6},
  },
  speed: 500,
};

export const Shimmer: React.SFC<ShimmerProps> = (props: ShimmerProps) => (
  <ShimmerStyled {...props}>
    <AnimatedLinearGradient {...SHIMMER_PROPS} />
  </ShimmerStyled>
);

export const ShimmerIcon: React.SFC = () => (
  <Shimmer width={IconSize.Medium} height={IconSize.Medium} />
);
