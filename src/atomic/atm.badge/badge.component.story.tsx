import * as React from 'react';
import { Badge } from '@atomic/atm.badge';
import { H2 } from '@atomic/atm.typography';
import { HBox, VBox } from '@atomic/obj.grid';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Atoms', module);
const handleDismiss = (id: string) => console.warn(`Badge ${id} dismissed`);
stories.add('Badges', () => (
  <VBox>
    <H2>Badges</H2>
    <HBox wrap={true}>
      <Badge text='Neutral' type='neutral' onDismiss={handleDismiss} id='123'/>
      <Badge text='Primary' type='primary' />
      <Badge text='Success' type='success' />
      <Badge text='Success' type='success' />
      <Badge text='Secondary' type='secondary' />
    </HBox>
  </VBox>
));
