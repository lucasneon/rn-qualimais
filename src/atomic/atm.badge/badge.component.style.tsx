import styled from 'styled-components/native';
import { Border, Color, FontSize, Spacing } from '@atomic/obj.constants';
import { BadgeProps } from './badge.component';

const BADGE_HEIGHT = 32;

const badgeColors = {
  primary: Color.Primary,
  secondary: Color.Secondary,
  success: Color.Success,
  neutral: Color.GrayXLight,
};

export const BadgeStyled = styled.View`
  flex-direction: row;
  padding-horizontal: ${Spacing.Medium};
  border-radius: ${Border.RadiusLarge};
  background-color: ${(props: BadgeProps) => props.type ? badgeColors[props.type] : Color.Primary};
  align-self: flex-start;
  align-items: center;
  justify-content: center;
  margin-bottom: ${Spacing.XSmall};
  margin-right: ${Spacing.XSmall};
  height: 32px;
`;

export const BadgeTextStyled = styled.Text`
  font-size: ${FontSize.Small};
  color: ${(props: BadgeProps) => (props.type && props.type === 'neutral') ? Color.GrayXDark : Color.White};
`;

export const BadgeCloseStyled = styled.Image`
  height: ${BADGE_HEIGHT / 2};
  width: ${BADGE_HEIGHT / 2};
  tint-color: ${(props: BadgeProps) => (props.type && props.type === 'neutral') ? Color.GrayXDark : Color.White};
`;

export const BadgeCloseButtonStyled = styled.TouchableOpacity`
  justify-content: center;
  align-items: flex-end;
  width: ${BADGE_HEIGHT * 0.75};
  height: ${BADGE_HEIGHT};
`;
