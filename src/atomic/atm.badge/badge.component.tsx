import * as React from 'react';
import { Asset } from '@atomic/obj.asset';
import {
  BadgeCloseButtonStyled,
  BadgeCloseStyled,
  BadgeStyled,
  BadgeTextStyled
  } from './badge.component.style';

export interface BadgeProps {
  type: 'primary' | 'secondary' | 'neutral' | 'success';
  text: string;
  id?: string;
  onDismiss?: (id: string) => void;
}

interface BadgeState {
  dismissed: boolean;
}

export class Badge extends React.Component<BadgeProps, BadgeState> {

  constructor(props: BadgeProps) {
    super(props);

    this.state = {
      dismissed: false,
    };
  }

  handleDismiss = () => {
    this.setState({dismissed: true});

    if (this.props.onDismiss) {
      this.props.onDismiss(this.props.id);
    }
  }

  render() {
    if (this.state.dismissed) {
      return null;
    }

    return (
      <BadgeStyled {...this.props}>
        <BadgeTextStyled {...this.props}>{this.props.text}</BadgeTextStyled>
        { this.props.onDismiss &&
          <BadgeCloseButtonStyled onPress={this.handleDismiss}>
            <BadgeCloseStyled  {...this.props} source={Asset.Icon.Atomic.Close} />
          </BadgeCloseButtonStyled>
        }
      </BadgeStyled>
    );
  }

}
