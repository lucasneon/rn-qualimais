import * as React from 'react';
import { Else, ElseIf, ElseIfProps } from './else.component';

export interface IfProps {
  cond: boolean;
}

export class If extends React.Component<IfProps> {

  private visibleChildren: React.ReactNode[] = [];

  render() {
    this.updateVisibleChildren();
    return this.visibleChildren;
  }

  componentWillUnmount() {
    this.visibleChildren = [];
  }

  private updateVisibleChildren() {
    // clearing visible children
    this.visibleChildren = [];

    let isIncluding = this.props.cond;

    React.Children.forEach(this.props.children, child => {
      isIncluding = this.shouldInclude(child, isIncluding);

      if (isIncluding) {
        this.visibleChildren.push(child);
      }
    });
  }

  private shouldInclude(child: React.ReactNode, isIncluding: boolean): boolean {
    if (isIncluding) {
      return !isElementOf(child, ElseIf) && !isElementOf(child, Else);
    }

    if (isElementOf(child, ElseIf) && this.visibleChildren.length === 0) {
      return (child as React.ReactElement<ElseIfProps>).props.cond;
    }

    if (isElementOf(child, Else) && this.visibleChildren.length === 0) {
      return !this.props.cond;
    }

    return isIncluding;
  }
}

function isElementOf(child: React.ReactNode, elementClass: React.ReactNode): boolean {
  return (child as React.ReactElement<any>).type === elementClass;
}
