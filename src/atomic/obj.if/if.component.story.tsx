import * as React from 'react';
import { Body, H2, VBox, VSeparator } from '@atomic';
import { PrimaryButton } from '@atomic/atm.button';
import { storiesOf } from '@storybook/react-native';
import { Else, ElseIf, If } from './';

class IfSample extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = { toggleIf: 0 };
  }

  render() {
    return (
      <VBox>
        <H2>If component</H2>
          <PrimaryButton
            text={`Toggle If: ${this.state.toggleIf}`}
            expanded={true}
            onTap={this.toggleIf}
          />
        <VSeparator />
        <If cond={this.state.toggleIf === 0}>
          <Body>1. If.cond === true</Body>

        <ElseIf cond={this.state.toggleIf === 1} />
          <Body>2. ElseIf.cond === true</Body>

        <ElseIf cond={this.state.toggleIf === 2} />
          <Body>3. ElseIf2.cond === true</Body>

        <Else />
          <Body>4. Else</Body>
        </If>
      </VBox>
    );
  }

  private toggleIf = () => {
    if (this.state.toggleIf === 3) {
      this.setState({toggleIf: 0});
    } else {
      this.setState(prevState => {
        const newToggleIf: number = prevState.toggleIf + 1;
        return { toggleIf: newToggleIf };
      });
    }
  }

}

const stories = storiesOf('Objects', module);

stories.add('If', () => (
  <IfSample />
));
