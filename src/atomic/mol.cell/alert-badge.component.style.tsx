import styled from 'styled-components/native';
import { Color, FontSize, IconSize } from '@atomic/obj.constants';
import { CellThumbSize } from './cell.component.style';

export const AlertBadgeDiameter = IconSize.Small;

export const AlertBadgeStyled = styled.View`
  position: absolute;
  top: ${5 - AlertBadgeDiameter / 2};
  left: ${CellThumbSize - AlertBadgeDiameter / 2 + 1};
  width: ${AlertBadgeDiameter};
  height: ${AlertBadgeDiameter};
  border-radius: ${AlertBadgeDiameter / 2};
  background-color: ${Color.Alert};
`;

export const AlertBadgeTextStyled = styled.Text`
  font-size: ${FontSize.XSmall};
  text-align: center;
  text-align-vertical: center;
  color: ${Color.White};
`;
