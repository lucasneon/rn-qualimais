import * as React from 'react';
import {
  AlertBadgeStyled,
  AlertBadgeTextStyled
} from './alert-badge.component.style';

export interface AlertBadgeProps {
  text: string;
  elementSize?: number;
  relativePosition?: boolean;
}

export const AlertBadge = (props: AlertBadgeProps) => {
  return (
    <AlertBadgeStyled elementSize={props.elementSize} relativePosition={props.relativePosition}>
      <AlertBadgeTextStyled>{props.text}</AlertBadgeTextStyled>
    </AlertBadgeStyled>
  );
};
