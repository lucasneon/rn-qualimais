import { Platform } from 'react-native';
import styled from 'styled-components/native';
import { CellProps } from '@atomic/mol.cell/cell.component';
import { Border, Color, IconSize, Opacity } from '@atomic/obj.constants';
import { GUTTER } from '@atomic/obj.grid';

const cellStyled: any =
  Platform.OS === 'android'
    ? styled.TouchableNativeFeedback
    : styled.TouchableOpacity;

export const CellThumbSize = IconSize.Large;

export const CellStyled = cellStyled.attrs({
  activeOpacity: Opacity.Active,
})`
  background-color: ${Color.White};
`;

export const CellBackgroundStyled = styled.View`
  padding-vertical: ${GUTTER};
  border-bottom-width: ${Border.Width};
  border-bottom-color: ${Border.Color};
`;

export const CellThumbStyled = styled.Image`
  height: ${(props: CellProps) =>
    props.iconLarge ? IconSize.Large : IconSize.Medium};
  ${(props: CellProps) => `tintColor: ${props.tintColor}`}
  width: ${(props: CellProps) =>
    props.iconLarge ? IconSize.Large : IconSize.Medium};
  resize-mode: contain;
  ${(props: CellProps) => (props.isAlert ? `tintColor: ${Color.Alert}` : '')}
`;
