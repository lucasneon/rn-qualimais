import * as React from 'react';
import { Asset } from '@atomic';
import { Body } from '@atomic/atm.typography';
import { Cell, HDisplayCell } from '@atomic/mol.cell';
import { Scroll } from '@atomic/obj.scroll';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Molecules', module);

const action = () => console.log('Button pressed');
stories.add('Cells', () => (
  <Scroll>
    <Cell
      onTap={action}
      leftThumb={Asset.Icon.Atomic.ChevronRight}
      rightThumb={Asset.Icon.Atomic.ChevronRight}
    >
      <Body>Any content can be added inside a cell. Both left and right images may be invisible.</Body>
    </Cell>
    <HDisplayCell>This is a heading display cell.</HDisplayCell>
  </Scroll>
));
