import styled from 'styled-components/native';

import { Color, ScreenUnit, Spacing } from '@atomic/obj.constants';
import { HDisplay } from '@atomic/atm.typography';
import { GUTTER } from '@atomic/obj.grid';

export const HDisplayCell = styled(HDisplay) `
  padding-vertical: ${Spacing.Large + ScreenUnit};
  padding-horizontal: ${GUTTER + ScreenUnit};
  background-color: ${Color.White};
`;
