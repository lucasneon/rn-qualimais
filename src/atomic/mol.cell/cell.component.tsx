import _ from 'lodash';
import * as React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { DebounceDuration } from '@atomic/obj.constants';
import { HBox } from '@atomic/obj.grid';
import { AlertBadge } from './alert-badge.component';
import {
  CellBackgroundStyled,
  CellStyled,
  CellThumbStyled,
} from './cell.component.style';
import { NewsImage } from '@app/components/atm.news-image';

export interface CellProps extends Readonly<{ children?: React.ReactNode }> {
  leftGutter?: boolean;
  rightGutter?: boolean;
  leftThumb?: any;
  rightThumb?: any;
  iconLarge?: boolean;
  isAlert?: boolean;
  tintColor?: any;
  showBadge?: boolean;
  image?: string;
  onTapLeftButton?: () => void;
  onTap?: () => any;
}

export const Cell: React.SFC<CellProps> = (props: CellProps) => (
  <CellStyled
    onPress={
      props.onTap
        ? _.debounce(props.onTap, DebounceDuration, { leading: true })
        : null
    }
    disabled={!props.onTap}>
    <CellBackgroundStyled>
      {props.image && <NewsImage image={props.image} />}
      <HBox>
        {props.leftGutter && <HBox.Separator />}
        {props.leftThumb && (
          <>
            <HBox.Item wrap={true} hAlign={'flex-end'} vAlign={'center'}>
              <TouchableOpacity onPress={props.onTapLeftButton}>
                <CellThumbStyled
                  isAlert={props.isAlert}
                  source={props.leftThumb}
                  iconLarge={props.iconLarge}
                  tintColor={props.tintColor ? props.tintColor : '#f58c78'}
                />
                {props.showBadge && <AlertBadge text="!" />}
              </TouchableOpacity>
            </HBox.Item>
            <HBox.Separator />
          </>
        )}
        <HBox.Item>{props.children}</HBox.Item>
        {props.rightThumb && (
          <>
            <HBox.Separator />
            <HBox.Item wrap={true} hAlign={'flex-end'} vAlign={'center'}>
              <Image source={props.rightThumb} />
            </HBox.Item>
          </>
        )}
        {props.rightGutter && <HBox.Separator />}
      </HBox>
    </CellBackgroundStyled>
  </CellStyled>
);

Cell.defaultProps = {
  leftGutter: true,
  rightGutter: true,
};
