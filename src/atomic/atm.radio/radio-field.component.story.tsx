import * as React from 'react';

import { AutoFormSample, Validators } from '@atomic';
import { VBox } from '@atomic/obj.grid';
import { H2, InputLabel } from '@atomic/atm.typography';
import { RadioField } from '@atomic/atm.radio';

import { storiesOf } from '@storybook/react-native';
import { Form } from '@atomic';

const stories = storiesOf('Form', module);

stories.add('RadioField', () => (
  <AutoFormSample>
    <VBox>
      <H2>Radio Sample</H2>
      <InputLabel>Radio group</InputLabel>
      <Form.Field value={2} name='radio' validators={[Validators.Required('Required')]}>
        <RadioField id={null}>Value null</RadioField>
        <RadioField id={1}>Value 1</RadioField>
        <RadioField id={2}> Value 2</RadioField>
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
