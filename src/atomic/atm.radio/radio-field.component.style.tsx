import { Color, Spacing } from '@atomic/obj.constants';

import { InputValue } from '@atomic/atm.typography';
import { RadioFieldSelectProps } from './radio-field.component';
import styled from 'styled-components/native';

export const RadioFieldStyled = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  margin-bottom: ${Spacing.Small};
`;

export const RadioFieldBulletStyled = styled.View`
  width: ${Spacing.XLarge};
  height: ${Spacing.XLarge};
  margin-right: ${Spacing.Small};
  border-width: 1;
  border-color: ${(props: RadioFieldSelectProps) => props.selected ? Color.Primary : Color.Black};
  border-radius: 12;
  justify-content: center;
  align-items: center;
`;

export const RadioFieldBulletSpacingStyled = styled.View`
  height: ${Spacing.XSmall};
`;

export const RadioFieldBulletSelectedStyled = styled.View`
  width: 14;
  height: 14;
  background-color: ${(props: RadioFieldSelectProps) => props.selected ? Color.Primary : 'transparent'};
  border-radius: 7;
`;

export const RadioFieldTextStyled = styled(InputValue)`
  flex-grow: 1;
  width: 0; /* hack for text wrapping */
`;
