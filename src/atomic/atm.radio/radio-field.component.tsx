import * as React from 'react';
import { FormFieldContexState, FormFieldContext } from '@atomic/obj.form/form-field.component';

import {
  RadioFieldBulletSelectedStyled,
  RadioFieldBulletStyled,
  RadioFieldStyled,
  RadioFieldTextStyled
} from './radio-field.component.style';

export interface RadioFieldSelectProps {
  selected: boolean;
}
const RadioFieldImage = (props: RadioFieldSelectProps) => {
  return (
    <RadioFieldBulletStyled {...props}>
      <RadioFieldBulletSelectedStyled {...props} />
    </RadioFieldBulletStyled>
  );
};

export interface RadioFieldProps {
  id: any;
  onValueChange?: (id: any) => void;
}

export class RadioField extends React.Component<RadioFieldProps, undefined> {

  private formFieldConsumer: FormFieldContexState;

  constructor(props: RadioFieldProps) {
    super(props);
  }

  render() {
    return (
      <FormFieldContext.Consumer>
        {(formFieldConsumer: FormFieldContexState) => {
          this.formFieldConsumer = formFieldConsumer;
          if (!formFieldConsumer) {
            throw new Error('<RadioField /> must be wrapped with a <Form.Field> tag');
          }
          return (
            <RadioFieldStyled onPress={this.handlePress}>
              <RadioFieldImage selected={formFieldConsumer.value === this.props.id}/>
              <RadioFieldTextStyled>{this.props.children}</RadioFieldTextStyled>
            </RadioFieldStyled>
          );
        }}
      </FormFieldContext.Consumer>
    );
  }

  private handlePress = () => {
    if (this.props.onValueChange) {
      this.props.onValueChange(this.props.id);
    }

    if (this.formFieldConsumer) {
      this.formFieldConsumer.onValueChange(this.props.id, null);
    }
  }
}
