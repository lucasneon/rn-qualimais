import _ from 'lodash';
import * as React from 'react';
import { DebounceDuration } from '@atomic/obj.constants';
import {
  ButtonContentStyled,
  ButtonSpinnerStyled,
  ButtonStyled,
  ButtonTextStyled,
  ButtonThumbStyled,
  ButtonTouchableOpacityStyled,
  ButtonWrapperStyled,
} from './button.component.style';
import { HBox } from '@atomic';
import { AlertBadge } from '@atomic/mol.cell/alert-badge.component';
import { ActivityIndicator } from 'react-native';

export interface AtomicButtonProps {
  disabled?: boolean;
  expanded?: boolean;
  image?: any;
  loading?: boolean;
  onTap?: () => any;
  text?: string;
  submit?: boolean;
  short?: boolean;
  square?: boolean;
  withDividerSide?: string;
  badgeNumber?: string;
  style?: any;
}

export interface ButtonColorProps extends AtomicButtonProps {
  outlined?: boolean;
  color:
    | 'primary'
    | 'secondary'
    | 'neutral'
    | 'success'
    | 'alert'
    | 'accessory'
    | 'facebook'
    | 'original'
    | 'transparent';
  originalColor?: boolean;
}

export class Button extends React.PureComponent<ButtonColorProps> {
  static defaultProps = { loading: false, short: false };

  static options = { name: 'Button' };

  render() {
    const spinnerProps = { isVisible: this.props.loading };
    const { outlined, color, withDividerSide } = this.props;

    return (
      <ButtonStyled {...this.props}>
        <ButtonTouchableOpacityStyled
          onPress={
            this.props.onTap
              ? _.debounce(this.props.onTap, DebounceDuration, {
                  leading: true,
                })
              : null
          }
          disabled={this.props.loading || this.props.disabled}
          {...this.props}>
          <ButtonWrapperStyled {...this.props}>
            <ButtonContentStyled {...this.props}>
              {this.props.image && (
                <ButtonThumbStyled
                  source={this.props.image}
                  color={color}
                  outlined={outlined}
                />
              )}
              {this.props.text && (
                <ButtonTextStyled
                  color={color}
                  outlined={outlined}
                  withDividerSide={withDividerSide}>
                  {this.props.text}
                </ButtonTextStyled>
              )}
              {this.props.badgeNumber && (
                <>
                  <HBox.Separator />
                  <AlertBadge
                    text={this.props.badgeNumber}
                    relativePosition={true}
                  />
                </>
              )}
            </ButtonContentStyled>
            {this.props.loading && (
              <ActivityIndicator
                style={{ marginBottom: 15 }}
                size="large"
                color="#171a88"
              />
            )}
          </ButtonWrapperStyled>
        </ButtonTouchableOpacityStyled>
      </ButtonStyled>
    );
  }
}
