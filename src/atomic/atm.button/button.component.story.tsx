import * as React from 'react';
import { LinkButton } from '@atomic/atm.button';
import { Body, H2 } from '@atomic/atm.typography';
import { HBox, VBox, VSeparator } from '@atomic/obj.grid';
import { Scroll } from '@atomic/obj.scroll';
import { storiesOf } from '@storybook/react-native';
import { FacebookButton, NeutralButton, PrimaryButton, SecondaryButton } from './button-types.component';

const action = () => console.log('Button pressed');

const stories = storiesOf('Atoms', module);

stories.add('Buttons', () => (
  <Scroll>
    <VBox>
      <H2>Types</H2>
      <HBox>
        <HBox.Item><Body>Enabled</Body></HBox.Item>
        <HBox.Separator />
        <HBox.Item><Body>Disabled</Body></HBox.Item>
      </HBox>
      <VSeparator />
      <HBox>
        <HBox.Item>
          <PrimaryButton text='Primary' expanded={true} onTap={action} />
        </HBox.Item>
        <HBox.Separator />
        <HBox.Item>
          <PrimaryButton text='Primary' disabled={true} expanded={true} onTap={action} />
        </HBox.Item>
      </HBox>
      <VSeparator />
      <HBox>
        <HBox.Item>
          <PrimaryButton negative={true} text='Primary negative' expanded={true} onTap={action} />
        </HBox.Item>
        <HBox.Separator />
        <HBox.Item>
          <PrimaryButton negative={true} text='Primary negative' disabled={true} expanded={true} onTap={action} />
        </HBox.Item>
      </HBox>
      <VSeparator />
      <HBox>
        <HBox.Item>
          <SecondaryButton expanded={true} text='Secondary' onTap={action} />
        </HBox.Item>
        <HBox.Separator />
        <HBox.Item>
          <SecondaryButton expanded={true} disabled={true} text='Secondary' onTap={action} />
        </HBox.Item>
      </HBox>
      <VSeparator />
      <HBox>
        <HBox.Item>
          <SecondaryButton negative={true} expanded={true} text='Secondary negative' onTap={action} />
        </HBox.Item>
        <HBox.Separator />
        <HBox.Item>
          <SecondaryButton negative={true} expanded={true} disabled={true} text='Secondary megative' onTap={action} />
        </HBox.Item>
      </HBox>
      <VSeparator />
      <HBox>
        <HBox.Item>
          <NeutralButton expanded={true} text='Neutral' onTap={action} />
        </HBox.Item>
        <HBox.Separator />
        <HBox.Item>
          <NeutralButton expanded={true} disabled={true} text='Neutral' onTap={action} />
        </HBox.Item>
      </HBox>
      <VSeparator />
      <LinkButton text='Link button' onTap={action} expanded={true} image={require('@assets/ic_circle.png')} />
    </VBox>
    <VBox>
      <H2>Sizes</H2>
      <PrimaryButton text='Wrap content' />
      <VSeparator />
      <PrimaryButton text='Expanded' expanded={true} />
      <VSeparator />
      <PrimaryButton short={true} text='Short' expanded={false} />
    </VBox>
    <VBox>
      <H2>Others</H2>
      <FacebookButton text='Facebook button' expanded={true} loading={false} />
      <VSeparator />
      <HBox>
        <HBox.Item>
          <PrimaryButton image={require('@assets/ic_circle.png')} text='With image' expanded={true} />
        </HBox.Item>
        <HBox.Separator />
        <HBox.Item>
          <SecondaryButton image={require('@assets/ic_circle.png')} text='With image' expanded={true} />
        </HBox.Item>
      </HBox>
      <VSeparator />
      <PrimaryButton text='With loading' expanded={true} loading={true} />
      <VSeparator />
      <SecondaryButton text='With loading' expanded={true} loading={true} />
      <VSeparator />
    </VBox>
  </Scroll>
));
