export interface ButtonProps {
  disabled?: boolean;
  expanded?: boolean;
  image?: any;
  loading?: boolean;
  onTap?: () => any;
  text?: string;
  submit?: boolean;
  short?: boolean;
}
