import styled from 'styled-components/native';
import { SpinnerStyled } from '@atomic/atm.loader/circle-loader.component';
import { Border, Color, FontFamily, FontSize, Opacity, Spacing } from '@atomic/obj.constants';
import { Platform } from '@atomic/obj.platform';
import { ButtonColorProps } from './button.component';

export const TotalButtonHeight = 48;
export const ButtonHeightWithoutBorder = TotalButtonHeight - 2 * Border.Width;
export const TotalButtonShortHeight = 30;
export const ButtonShortHeightWithoutBorder = TotalButtonShortHeight - 2 * Border.Width;
export const ButtonIconSize = Spacing.Large;

const BorderLeftDividerButton = 16;
const BorderRightDividerButton = 16;

const buttonColors = {
  primary: Color.Primary,
  secondary: Color.Secondary,
  neutral: Color.Neutral,
  accessory: Color.Accessory,
  alert: Color.Alert,
  success: Color.Success,
  facebook: Color.Facebook,
  transparent: Color.Transparent,
  white: Color.White,
};

export const ButtonThumbStyled = styled.Image`
  width: ${ButtonIconSize};
  height: ${ButtonIconSize};
  margin-right: ${Spacing.Small};
  ${(props: ButtonColorProps) => props.originalColor! && `tintColor: ${
      props.outlined ? buttonColors[props.color] : Color.White}; ` };
`;

export const ButtonStyled = styled.View`
  opacity: ${(props: ButtonColorProps) => (props.disabled || props.loading) ? Opacity.Disabled : 1};
  justify-content: center;
  align-items: center;
  align-self: ${(props: ButtonColorProps) => props.expanded ? 'stretch' : 'auto'};
`;

const buttonStyled: any = Platform.OS === 'android' ? styled.TouchableNativeFeedback : styled.TouchableOpacity;

export const ButtonTouchableOpacityStyled = buttonStyled.attrs({
  activeOpacity: Opacity.Active,
})`
  align-self: ${(props: ButtonColorProps) => props.expanded ? 'stretch' : 'auto'};
`;

export const ButtonWrapperStyled = styled.View`
  background-color: ${(props: ButtonColorProps) => !props.outlined && props.color ? buttonColors[props.color] :
    Color.White};
  border-color: ${(props: ButtonColorProps) => props.color && props.color !== 'transparent' ?
  buttonColors[props.color] : Color.White};
  border-width: ${Border.Width};
  border-top-width: ${(props: ButtonColorProps) => props.withDividerSide ? 0 : Border.Width};
  height: ${(props: ButtonColorProps) => props.short ? ButtonShortHeightWithoutBorder : ButtonHeightWithoutBorder};
  align-self: ${(props: ButtonColorProps) => props.expanded ? 'stretch' : 'auto'};
  justify-content: center;
  align-items: center;
  padding-horizontal: ${Spacing.Medium};
  ${(props: ButtonColorProps) => {
    if (props.square) {
      return null;
    } else if (props.withDividerSide === 'left') {
      return `border-bottom-left-radius: ${BorderLeftDividerButton}`;
    } else if (props.withDividerSide === 'right') {
      return `border-bottom-right-radius: ${BorderRightDividerButton}`;
    } else {
      return `border-radius: ${Border.RadiusLarge};`;
    }
  }};
`;

export const ButtonTextStyled = styled.Text`
  color: ${(props: ButtonColorProps) => props.outlined && props.color ? buttonColors[props.color] : Color.White};
  text-align: center;
  font-family: ${(props: ButtonColorProps) => props.withDividerSide ?
    FontFamily.Primary.Medium : FontFamily.Primary.Medium};
  font-size: ${(props: ButtonColorProps) => props.withDividerSide ? FontSize.Small : FontSize.Medium};
`;

export const ButtonContentStyled = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  overflow: ${(props: ButtonColorProps) => props.loading ? 'hidden' : 'visible'};
  opacity: ${(props: ButtonColorProps) => props.loading ? 0 : 1};
`;

export const ChatButtonImage = styled.Image`
  backgroundColor: ${Color.Primary};
  borderRadius: 19;
  width: 38;
  height: 38;
  marginRight: ${Spacing.XSmall};
`;

interface ButtonSpinnerStyledProps {
  outlined?: boolean;
  buttonColor?: any;
}

export const ButtonSpinnerStyled: any = styled(SpinnerStyled).attrs((props: ButtonSpinnerStyledProps) => {
  return {
  size: ButtonIconSize * 1.5 ,
  color: props.outlined ? buttonColors[props.buttonColor] : Color.White,
}; })``;
