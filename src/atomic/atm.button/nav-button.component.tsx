import * as React from 'react';
import { NavButtonStyled, NavButtonThumbStyled } from './nav-button.component.style';

export interface NavButtonProps {
  disabled?: boolean;
  icon?: any;
  onTap?: () => any;
  hasNotification?: boolean;
}

export const NavButton = (props: NavButtonProps) => {
  return (
      <NavButtonStyled
        onPress={props.onTap ? props.onTap : null}
        disabled={props.disabled}
        {...props}
      >
        {props.icon ?
          <NavButtonThumbStyled
            source={props.icon}
            resizeMode='contain'
          /> : null
        }
      </NavButtonStyled>
  );
};
