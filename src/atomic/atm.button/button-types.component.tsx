import * as React from 'react';
import { AtomicButtonProps, Button } from './button.component';

export interface ButtonProps extends AtomicButtonProps {
  negative?: boolean;
}

export const PrimaryButton: React.SFC<ButtonProps> = props =>
  <Button color={props.negative ? 'secondary' : 'primary'} {...props} />;

export const SecondaryButton: React.SFC<ButtonProps> = props =>
  <Button color={props.negative ? 'secondary' : 'success'} outlined={true} {...props} />;

export const NeutralButton: React.SFC<ButtonProps> = props =>
  <Button outlined={true} color='neutral' {...props} />;

export const FacebookButton: React.SFC<ButtonProps> = props =>
  <Button color='facebook' image={require('@assets/ic_facebook/ic_facebook.png')} {...props} />;
