import styled from 'styled-components/native';
import { Color, FontFamily, FontSize, Opacity, Spacing } from '@atomic/obj.constants';
import { ButtonIconSize } from './button.component.style';
import { LinkButtonProps } from './link-button.component';

const LinkButtonColor = Color.Primary;

interface LinkButtonThumbStyledProps {
  imageOnRight?: boolean;
  textStyle?: boolean;
}
export const LinkButtonThumbStyled = styled.Image`
  width: ${ButtonIconSize};
  height: ${ButtonIconSize};
  margin-right: ${(props: LinkButtonThumbStyledProps) => props.imageOnRight ? 0 : Spacing.Small };
  margin-left: ${(props: LinkButtonThumbStyledProps) => props.imageOnRight ? Spacing.Small : 0 };
  tintColor: ${(props: LinkButtonThumbStyledProps) => props.textStyle ? "grey" :  LinkButtonColor};
  resize-mode: contain;
`;

export const LinkButtonStyled = styled.TouchableOpacity.attrs({
  activeOpacity: Opacity.Active,
})`
  align-self: ${(props: LinkButtonProps) => props.expanded ? 'stretch' : 'auto'};
  height: ${Spacing.Large * 2};
  justify-content: ${(props: LinkButtonProps) => props.align ? props.align : 'center'};
  align-items: center;
  opacity: ${(props: LinkButtonProps) => props.disabled ? Opacity.Disabled : 1};
  flex-direction: row;
`;

export const LinkButtonTextStyled = styled.Text`
  color: ${(props: LinkButtonThumbStyledProps) => props.textStyle ? "grey" : LinkButtonColor};
  text-align: center;
  font-family: ${FontFamily.Primary.Medium};
  font-size: ${FontSize.Medium};
`;
