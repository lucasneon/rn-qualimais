import { Color, Spacing } from '@atomic/obj.constants';

import { NavButtonProps } from './nav-button.component';
import styled from 'styled-components/native';

const NavBtnColor = Color.Primary;

export const NavButtonThumbStyled = styled.Image`
  width: ${Spacing.XLarge};
  height: ${Spacing.XLarge};
  tintColor: ${NavBtnColor};
`;

export const NavButtonStyled = styled.TouchableOpacity`
  width: ${Spacing.Large * 2 + Spacing.XLarge};
  height: 44;
  justifyContent: center;
  alignItems: center;
  opacity: ${(props: NavButtonProps) => props.disabled ? 0.5 : 1};
  flexDirection: row;
`;
