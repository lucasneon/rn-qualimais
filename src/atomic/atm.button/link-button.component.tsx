import _ from 'lodash';
import * as React from 'react';
import { DebounceDuration } from '@atomic/obj.constants';
import { LinkButtonStyled, LinkButtonTextStyled, LinkButtonThumbStyled } from './link-button.component.style';

export interface LinkButtonProps {
  disabled?: boolean;
  expanded?: boolean;
  image?: any;
  onTap?: () => any;
  text?: string;
  imageOnRight?: boolean;
  textStyle?: boolean;
  removeDebounce?: boolean;
  align?: string;
}

export const LinkButton = (props: LinkButtonProps) => {
  return (
    <LinkButtonStyled
      onPress={props.onTap ?
        !props.removeDebounce ? _.debounce(props.onTap, DebounceDuration, { leading: true }) : props.onTap
        : null}
      disabled={props.disabled}
      {...props}
    >
      {
        props.image &&
        !props.imageOnRight &&
        <LinkButtonThumbStyled imageOnRight={props.imageOnRight} source={props.image} />
      }
      <LinkButtonTextStyled textStyle={props.textStyle} {...props}>{props.text}</LinkButtonTextStyled>
      {
        props.image &&
        props.imageOnRight &&
        <LinkButtonThumbStyled imageOnRight={props.imageOnRight} source={props.image} />
      }
    </LinkButtonStyled>
  );
};
