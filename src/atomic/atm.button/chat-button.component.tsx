import * as React from 'react';
import { ChatButtonImage } from './button.component.style';
import { TouchableOpacity} from 'react-native';
import { Asset } from '@atomic';

export interface ChatButtonProps {
  onTap: () => void;
  disabled?: boolean;
}

export class ChatButton extends React.PureComponent<ChatButtonProps> {
  render() {
    return (
        <TouchableOpacity onPress={this.props.onTap} disabled={this.props.disabled}>
            <ChatButtonImage source={Asset.Icon.Custom.Send}/>
        </TouchableOpacity>
    );
  }
}
