import * as React from 'react';
import { PrimaryButton } from '@atomic/atm.button';
import { InputLabel } from '@atomic/atm.typography';
import { Form } from '@atomic/obj.form';
import { VBox } from '@atomic/obj.grid';

// Automatic form with submit button & label to show submitted data
export class AutoFormSample extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  handleSubmit = (data: any) => {
    this.setState({submittedData: JSON.stringify(data)});
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Scroll>
          {this.props.children}
          <VBox>
            <InputLabel>{this.state.submittedData}</InputLabel>
            <PrimaryButton
              text='Submit'
              expanded={true}
              submit={true}
            />
          </VBox>
        </Form.Scroll>
      </Form>
    );
  }
}
