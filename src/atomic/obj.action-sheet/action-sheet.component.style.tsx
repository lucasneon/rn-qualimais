import { Dimensions } from 'react-native';
import styled from 'styled-components/native';
import { Color } from '@atomic/obj.constants';
import { ActionSheetProps } from './action-sheet.component';

const window = Dimensions.get('window');

export const ActionSheetModalStyled = styled.View`
  flex: 1;
  justify-content: ${(props: ActionSheetProps) => props.justifyContent ? props.justifyContent : `flex-end`};
`;

export const ActionSheetTouchableStyled = styled.TouchableHighlight.attrs(props => ({
  underlayColor: (props.light ? Color.White : Color.Black),
}))`
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: ${window.height};
  width: ${window.width};
  position: absolute;
  background-color: ${(props: ActionSheetProps) => props.light ? Color.White : Color.Black};
  opacity: ${(props: ActionSheetProps) => props.light ? 0.9 : 0.7};
`;
