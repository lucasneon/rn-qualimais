import * as React from 'react';
import { NeutralButton, PrimaryButton } from '@atomic/atm.button';
import { Root, VBox, VSeparator } from '@atomic/obj.grid';
import { storiesOf } from '@storybook/react-native';
import { ActionSheet } from './action-sheet.component';

const stories = storiesOf('Objects', module);

stories.add('Action sheet', () => (
  <ActionSheetStory />
));

class ActionSheetStory extends React.PureComponent<{}, { visible: boolean }> {
  private action: boolean;

  constructor(props) {
    super(props);
    this.state = { visible: false };
  }

  render() {
    const { visible } = this.state;
    return (
      <Root>
        <VBox vAlign='center'>
          <PrimaryButton expanded={true} text='Open action sheet' onTap={this.handleButtonTap} />
        </VBox>
        <ActionSheet onCancel={this.handleCancel} visible={visible} onDismiss={this.handleDismiss(this.action)}>
          <VBox>
            <PrimaryButton expanded={true} text='Primary action' onTap={this.handleActionSheetAction} />
            <VSeparator />
            <NeutralButton expanded={true} text='Cancel' onTap={this.handleCancelButton} />
            <VSeparator />
          </VBox>
        </ActionSheet>
      </Root>
    );
  }

  private handleButtonTap = () => {
    console.log('Open action sheet button tapped');
    this.setState({ visible: true });
  }

  private handleCancel = () => {
    console.log('Cancelled');
  }

  private handleDismiss = (primaryAction?: boolean) => {
    return () => {
      if (primaryAction) {
        console.log('Dismissed with primary action');
      } else {
        console.log('Dismissed');
      }
    };
  }

  private handleActionSheetAction = () => {
    this.action = true;
    this.setState({ visible: false });
  }

  private handleCancelButton = () => {
    this.action = false;
    this.setState({ visible: false });
  }
}
