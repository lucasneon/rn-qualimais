import * as React from 'react';
import { Modal, View } from 'react-native';
import * as Animatable from 'react-native-animatable';
import KeyboardShift from '@app/utils/keyboard-shift.utils';
import { ActionSheetModalStyled, ActionSheetTouchableStyled } from './action-sheet.component.style';

export interface ActionSheetProps {
  onCancel: () => void;
  children: any;
  visible: boolean;
  light?: boolean;
  justifyContent?: string;
  onDismiss?: () => void;
}

export const ActionSheet: React.SFC<ActionSheetProps> = (props: ActionSheetProps) => {
  const animation = props.visible ? 'slideInUp' : 'slideOutDown';

  return (

    <Modal
      animationType='fade'
      transparent={true}
      visible={props.visible}
      onRequestClose={props.onCancel}
      onDismiss={props.onDismiss}
    >
    <KeyboardShift useButtonOffset={true}>
      <ActionSheetModalStyled {...props} >
        <ActionSheetTouchableStyled {...props} onPress={props.onCancel}><View /></ActionSheetTouchableStyled>
        <Animatable.View animation={animation} duration={300}>
          {props.children}
        </Animatable.View>
      </ActionSheetModalStyled>
      </KeyboardShift>
    </Modal>
  );
};
