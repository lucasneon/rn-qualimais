import * as React from 'react';

import { BulletStyled, BulletTextStyled } from './bullet.component.style';
import { Image } from 'react-native';

export interface BulletProps {
  bulletNumber: string;
  bulletBackground?: any;
  isChecked?: boolean;
}

export class Bullet extends React.Component<BulletProps, any> {
  render() {
    return (
      <BulletStyled {...this.props}>
        {this.props.isChecked ?
          <Image source={require('@assets/ic_check_white/ic_check_white.png')} /> :
          <BulletTextStyled>{this.props.bulletNumber}</BulletTextStyled>}
      </BulletStyled>
    );
  }
}
