import styled from 'styled-components/native';
import { BulletProps } from './bullet.component';
import { Color, FontSize, Spacing } from '@atomic/obj.constants';

export const BulletStyled = styled.View`
  width: ${Spacing.XLarge};
  height: ${Spacing.XLarge};
  border-radius: ${Spacing.XLarge / 2};
  background-color: ${(props: BulletProps) => props.bulletBackground ? props.bulletBackground : Color.Alert};
  justify-content: center;
  align-items: center;
`;

export const BulletTextStyled = styled.Text`
  font-size: ${FontSize.XSmall};
  color: ${Color.White};
`;
