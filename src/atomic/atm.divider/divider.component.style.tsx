import { Color } from '@atomic/obj.constants';
import styled from 'styled-components/native';

export const DividerGray = styled.View`
  width: 100%;
  height: 1;
  background-color: ${Color.GrayLight};
`;

export const VerticalDividerGray = styled.View`
  width: 1;
  height: 100%;
  background-color: ${Color.GrayLight};
`;
