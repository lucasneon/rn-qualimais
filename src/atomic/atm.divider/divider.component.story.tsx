import * as React from 'react';

import { DividerGray } from '@atomic/atm.divider';
import { H2 } from '@atomic/atm.typography';
import { VBox } from '@atomic/obj.grid';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Atoms', module);

stories.add('Divider', () => (
  <VBox noGutter={true}>
    <H2>Divider</H2>
    <DividerGray />
  </VBox>
));
