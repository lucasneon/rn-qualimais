import Check from '@assets/icons/atomic/ic_check.png';
import ChevronDown from '@assets/icons/atomic/ic_chevron_down.png';
import ChevronLeft from '@assets/icons/atomic/ic_chevron_left.png';
import ChevronRight from '@assets/icons/atomic/ic_chevron_right.png';
// Atomic component's icons
import Clear from '@assets/icons/atomic/ic_clear.png';
import Photo from '@assets/icons/atomic/ic_photo.png';
import Plus from '@assets/icons/atomic/ic_plus.png';
// Custom icons
import Calendar from '@assets/icons/custom/ic_calendar.png';
import Card from '@assets/icons/custom/ic_card.png';
import Nutrition from '@assets/icons/custom/ic_eating.png';
import Emotions from '@assets/icons/custom/ic_emotions.png';
import EyeClosed from '@assets/icons/custom/ic_eye_clsd.png';
import EyeOpen from '@assets/icons/custom/ic_eye_opn.png';
import Copy from '@assets/icons/custom/ic_copy.png';
import Habit from '@assets/icons/custom/ic_habit.png';
import HealthIcon from '@assets/icons/custom/ic_health.png';
import HealthCareCard from '@assets/icons/custom/ic_health_card.png';
import Lock from '@assets/icons/custom/ic_lock.png';
import MedicalRecord from '@assets/icons/custom/ic_medical_record.png';
import MedicationIcon from '@assets/icons/custom/ic_medication.png';
import NavigationIcon from '@assets/icons/custom/ic_navigation.png';
import Phone from '@assets/icons/custom/ic_phone.png';
import SelfCare from '@assets/icons/custom/ic_self_care.png';
import SmokingIcon from '@assets/icons/custom/ic_smoke.png';
import Summary from '@assets/icons/custom/ic_summary.png';
import Survey from '@assets/icons/custom/ic_survey.png';
import Warning from '@assets/icons/custom/ic_warning.png';
import Web from '@assets/icons/custom/ic_web.png';
import Weight from '@assets/icons/custom/ic_weight.png';
import Workout from '@assets/icons/custom/ic_workout.png';
// Navbar icons
import Back from '@assets/icons/nav_bar/ic_back.png';
import Close from '@assets/icons/nav_bar/ic_close.png';
import Send from '@assets/icons/custom/ic_chat_send.png';

// Placeholder icons
import Camera from '@assets/icons/placeholder/ic_camera.png';
import NoConnection from '@assets/icons/placeholder/ic_no_connection.png';
import Failure from '@assets/icons/placeholder/ic_not_implemented.png';
import NotImplemented from '@assets/icons/placeholder/ic_not_implemented.png';
// Tabbar icons
import Account from '@assets/icons/tab_bar/ic_account.png';
import Care from '@assets/icons/tab_bar/ic_care.png';
import CustomerCare from '@assets/icons/tab_bar/ic_customer_care.png';
import Health from '@assets/icons/tab_bar/ic_health.png';
import Body from '@assets/images/bkg_body.png';
// Background images
import Consultation from '@assets/images/bkg_consultation.png';
import Drinking from '@assets/images/bkg_drinking.png';
import Exam from '@assets/images/bkg_exam.png';
import Exercise from '@assets/images/bkg_exercise.png';
import Food from '@assets/images/bkg_food.png';
import Footer from '@assets/images/bkg_footer.png';
import HealthBackground from '@assets/images/bkg_health.png';
import LogoLines from '@assets/images/bkg_logo_lines.png';
import Medication from '@assets/images/bkg_medication.png';
import Smoking from '@assets/images/bkg_smoking.png';
import WeightBackground from '@assets/images/bkg_weight.png';
import OnboardAdd from '@assets/images/img_add.png';
import ANS from '@assets/images/img_ans.png';
import OnboardCalendar from '@assets/images/img_calendar.png';
import OnboardChat from '@assets/images/img_chat.png';
import OnboardCheck from '@assets/images/img_check.png';
// Onboard images
import OnboardClock from '@assets/images/img_clock.png';
import OnboardEvents from '@assets/images/img_event.png';
// App images
import Logo from '@assets/images/img_logo.png';
import LogoShadedTransparentBackground from '@assets/images/img_logo_shaded.png';
import OnboardMedicine from '@assets/images/img_medicine.png';
import Pin from '@assets/images/img_pin.png';
import QLogoWhite from '@assets/images/img_q_logo.png';
import LogoWhite from '@assets/images/img_qsaude_logo.png';
import OnboardRecord from '@assets/images/img_record.png';
import OnboardSearch from '@assets/images/img_search.png';
import Star from '@assets/images/img_star.png';
import Avatar from '@assets/images/samples/avatar.png';

export const Asset = {
  Icon: {
    TabBar: { Account, Care, Health, CustomerCare },
    NavBar: { Back, Close },
    Atomic: {
      Clear,
      Check,
      ChevronDown,
      ChevronLeft,
      ChevronRight,
      Close,
      Photo,
      Plus,
    },
    Custom: {
      Calendar,
      Card,
      Copy,
      Habit,
      MedicalRecord,
      Medication: MedicationIcon,
      Send,
      SelfCare,
      Emotions,
      HealthIcon,
      Weight,
      SmokingIcon,
      Workout,
      Nutrition,
      Phone,
      NavigationIcon,
      Warning,
      Web,
      Lock,
      HealthCareCard,
      Summary,
      Survey,
      EyeOpen,
      EyeClosed,
    },
    Placeholder: {
      Camera,
      Failure,
      NoConnection,
      NotImplemented,
    },
  },
  Image: {
    Logo,
    LogoWhite,
    QLogoWhite,
    LogoShadedTransparentBackground,
    ANS,
    Star,
    Pin,
    Avatar,
  },
  Background: {
    Consultation,
    Drinking,
    Exam,
    Exercise,
    Food,
    Footer,
    Medication,
    Smoking,
    LogoLines,
    Health,
    WeightBackground,
    Body,
    HealthBackground,
  },
  Onboard: {
    OnboardAdd,
    OnboardCheck,
    OnboardClock,
    OnboardCalendar,
    OnboardChat,
    OnboardSearch,
    OnboardRecord,
    OnboardMedicine,
    OnboardEvents,
  },
};
