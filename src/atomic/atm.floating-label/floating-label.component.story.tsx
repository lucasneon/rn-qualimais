import * as React from 'react';
import { storiesOf } from '@storybook/react-native';

import { VBox, VSeparator} from '@atomic/obj.grid';
import { FloatingLabel } from '@atomic/atm.floating-label';
import { H2, H3 } from '@atomic/atm.typography';
import { AutoFormSample } from '@atomic/pag.form-sample';
import { TextField } from '@atomic';
import { Form, Validators } from '@atomic/obj.form';

const stories = storiesOf('Form', module);

stories.add('FloatingLabel', () => (
  <AutoFormSample>
    <VBox>
      <H2>TextFields with FloatingLabel</H2>
      <H3>Without Validator</H3>
      <VSeparator />
      <Form.Field name='noValidator'>
        <FloatingLabel label='No Validator' />
        <TextField type={TextField.Type.Normal} />
      </Form.Field>
      <VSeparator />
      <H3>With Validator</H3>
      <VSeparator />
      <Form.Field name='validator' validators={[Validators.Required()]}>
        <FloatingLabel label='With Validator' />
        <TextField type={TextField.Type.Normal} />
      </Form.Field>
      <VSeparator />
      <Form.Field name='initialValue'>
        <FloatingLabel label='With initial value' />
        <TextField type={TextField.Type.Normal} value='This is an initial value'/>
      </Form.Field>
      <VSeparator />
      <Form.Field name='disabled'>
        <FloatingLabel label='Disabled'/>
        <TextField type={TextField.Type.Normal} value='This is not editable' editable={false}/>
      </Form.Field>
      <VSeparator />
      <Form.Field name='CPF' validators={[Validators.Cpf('Invalid CPF')]}>
        <FloatingLabel label='CPF' />
        <TextField type={TextField.Type.CPF} placeholder=' ' />
      </Form.Field>
      <VSeparator />
      <Form.Field name='password'>
        <FloatingLabel label='Password' />
        <TextField type={TextField.Type.Normal} maxLength={8} secureTextEntry={true}/>
      </Form.Field>
    </VBox>
  </AutoFormSample>
));
