import styled from 'styled-components/native';
import { Animated } from 'react-native';

import { Color, Spacing } from '@atomic/obj.constants';

export const FloatingLabelWrapperStyled = styled.View`
  padding-top: ${Spacing.Large};
`;

interface StyledFloatLabeledFieldLabelProps {
  focus: boolean;
  hasError: boolean;
}

export const FloatingLabelStyled = styled(Animated.Text)`
  position: absolute;
  z-index: -1;
  color: ${(props: StyledFloatLabeledFieldLabelProps) => {
    if (props.hasError) {
      return Color.Alert;
    }
    return props.focus ? Color.Primary : Color.GrayDark;
  }};
`;
