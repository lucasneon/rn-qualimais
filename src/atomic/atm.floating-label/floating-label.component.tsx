import * as React from 'react';
import { Animated, Easing } from 'react-native';
import { FontSize, Spacing } from '@atomic/obj.constants';
import { GUTTER } from '@atomic/obj.grid';
import { FloatingLabelStyled, FloatingLabelWrapperStyled } from './floating-label.component.style';
import { FormFieldContexState, FormFieldContext } from '@atomic/obj.form/form-field.component';
export interface FloatingLabelProps {
  label: string;
}
const ANIMATION_DURATION = 200;

const ANIM_VALUES = {
  top: { above: 0, over: Spacing.Large + Spacing.Medium },
  left: { above: 0, over: GUTTER },
  font: { small: FontSize.XSmall, big: FontSize.Medium },
};

export class FloatingLabel extends React.Component<FloatingLabelProps, undefined> {
  private animValue: Animated.Value = new Animated.Value(0);

  render() {
    return (
      <FormFieldContext.Consumer>
      {(formFieldConsumer: FormFieldContexState) => {
        if (!formFieldConsumer) {
          throw new Error('<FloatingLabel /> must be wrapped in a <Form.Field> tag');
        }
        Animated.timing(this.animValue, {
          toValue: formFieldConsumer.focus || formFieldConsumer.value ? 1 : 0,
          duration: ANIMATION_DURATION,
          easing: Easing.inOut(Easing.cubic),
        }).start();

        const top = this.animValue.interpolate({
          inputRange: [0, 1],
          outputRange: [ANIM_VALUES.top.over, ANIM_VALUES.top.above],
        });
        const left = this.animValue.interpolate({
          inputRange: [0, 1],
          outputRange: [ANIM_VALUES.left.over, ANIM_VALUES.left.above],
        });
        const fontSize = this.animValue.interpolate({
          inputRange: [0, 1],
          outputRange: [ANIM_VALUES.font.big, ANIM_VALUES.font.small],
        });
        return (
          <FloatingLabelWrapperStyled>
            <FloatingLabelStyled
              hasError={formFieldConsumer.errors.length > 0}
              focus={formFieldConsumer.focus}
              style={{top, left, fontSize}}
              pointerEvents='none'
            >
              {this.props.label}
            </FloatingLabelStyled>
          </FloatingLabelWrapperStyled>
        );
      }}
      </FormFieldContext.Consumer>
    );
  }

  // --------------------------------------------------
  // Animation from label to placeholder and vice-versa
  // isLabel: if focused or filled;
  // isPlaceholder: not focused and not filled;
}
