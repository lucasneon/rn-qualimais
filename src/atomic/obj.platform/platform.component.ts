import { Dimensions, Platform as ReactPlatform, StatusBar } from 'react-native';

export class Platform {
  static isIphoneX = () => {
    const dimen = Dimensions.get('window');
    return (
      ReactPlatform.OS === 'ios' &&
        (dimen.height === 812 || dimen.width === 812)
    );
  }
  static get OS() {
    return ReactPlatform.OS;
  }

  static getStatusBarHeight = () => {
    return ReactPlatform.select({
        ios: Platform.isIphoneX() ? 44 : 20,
        android: StatusBar.currentHeight,
    });
  }
}
