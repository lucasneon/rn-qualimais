import * as React from 'react';

import { View } from 'react-native';

interface AccordionGroupState {
  [key: string]: boolean;
}

export class AccordionGroup extends React.Component<any, AccordionGroupState> {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        {this.getAccordions()}
      </View>
    );
  }

  componentDidMount() {
    React.Children.map(this.props.children, (child: any, i: number) => {
      const accordionId = 'Accordion_' + i;

      if (!this.state.hasOwnProperty(accordionId)) {
        this.setState({...this.state, [accordionId]: child.props.isExpanded ? true : false});
      }
    });
  }

  private accordionTapHandle = (key: string, isExpanded: boolean) => {
    const accordionItems = {};

    Object.getOwnPropertyNames(this.state).map(accordionId => {
      accordionItems[accordionId] = false;
    });

    accordionItems[key] = isExpanded;
    this.setState(accordionItems);
  }

  private getAccordions() {

    return React.Children.map(this.props.children, (child: any, i: number) => {

      const accordionId = 'Accordion_' + i;

      return React.cloneElement(child, {
        accordionId,
        onHeaderTap: this.accordionTapHandle,
        isExpanded: this.state[accordionId],
      });
    });

  }
}
