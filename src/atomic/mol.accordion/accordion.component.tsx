import * as React from 'react';
import { Animated, Easing, LayoutChangeEvent, View } from 'react-native';
import { Asset } from '@atomic/obj.asset';
import { AnimationDuration } from '@atomic/obj.constants';
import { If } from '@atomic/obj.if';
import {
  AccordionContentStyled, AccordionContentWrapperStyled, AccordionHeaderChevronStyled, AccordionHeaderContentStyled,
  AccordionHeaderStyled
} from './accordion.component.style';

export interface AccordionProps {
  header: React.ComponentClass<any> | React.StatelessComponent<any>;
  accordionId?: string;
  onHeaderTap?: (key: string, isExpanded: boolean) => void;
  isExpanded?: boolean;
  hasChevron?: boolean;
}

export interface AccordionState {
  isExpanded: boolean;
  animation?: Animated.Value;
}

export class Accordion extends React.Component<AccordionProps, AccordionState> {

  private height: number;
  private isAnimating: boolean = false;
  private spinValue;

  constructor(props) {
    super(props);
    this.state = {
      isExpanded: this.props.isExpanded ? true : false,
    };

    this.spinValue = new Animated.Value(this.props.isExpanded ? 1 : 0);
  }

  UNSAFE_componentWillReceiveProps(nextProps: Readonly<AccordionProps>, _nextContext: any): void {
    if (nextProps.isExpanded !== undefined && nextProps.isExpanded !== this.state.isExpanded) {
      this.toggle(nextProps.isExpanded);
    }
  }

  render() {
    const Header = this.props.header;

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });

    return (
      <View>
        <AccordionHeaderStyled onPress={this.onHeaderTap}>
          <AccordionHeaderContentStyled>
            <Header/>
          </AccordionHeaderContentStyled>
          <If cond={this.props.hasChevron}>
            <AccordionHeaderChevronStyled
              style={{transform: [{rotate: spin}] }}
              source={Asset.Icon.Atomic.ChevronDown}
            />
          </If>
        </AccordionHeaderStyled>
        <AccordionContentStyled style={{height: this.state.animation}}>
          <AccordionContentWrapperStyled onLayout={this.handleLayout}>
            {this.props.children}
          </AccordionContentWrapperStyled>
        </AccordionContentStyled>
      </View>
    );
  }

  private toggle(isExpanded: boolean) {
    if (this.isAnimating) {
      return;
    }
    this.isAnimating = true;

    Animated.parallel([
      Animated.timing(
        this.spinValue, {
          toValue: isExpanded ? 1 : 0,
          duration: AnimationDuration,
          easing: Easing.inOut(Easing.quad),
        },
      ),
      Animated.timing(
        this.state.animation, {
          toValue: isExpanded ? this.height : 0,
          duration: AnimationDuration,
          easing: Easing.inOut(Easing.quad),
        },
      ),
    ]).start( () => {
      this.isAnimating = false;
      this.setState({isExpanded});
    });
  }

  private onHeaderTap = () => {
    const shouldExpand: boolean = !this.state.isExpanded;
    if (this.props.onHeaderTap) {
      this.props.onHeaderTap(this.props.accordionId, shouldExpand);
    }
    this.toggle(shouldExpand);
  }

  private handleLayout = (event: LayoutChangeEvent) => {
    if (event.nativeEvent.layout.height > 0) {
      this.height = event.nativeEvent.layout.height;
    }

    if (!this.isAnimating) {
      this.setState({animation: new Animated.Value(this.state.isExpanded ? event.nativeEvent.layout.height : 0)});
    }
  }
}
