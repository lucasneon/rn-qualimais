import * as React from 'react';

import { Body, H2, H3, H4 } from '@atomic/atm.typography';
import { VBox } from '@atomic/obj.grid';

import { Accordion } from '@atomic/mol.accordion';
import { AccordionGroup } from '@atomic/mol.accordion';
import { Scroll } from '@atomic/obj.scroll';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Molecules', module);

stories.add('Accordion', () => {
  const accordionHeader = () => {
    return <H3>Accordion Header</H3>;
  };
  const innerAccordionHeader = () => {
    return <H4>Inner Accordion Header</H4>;
  };
  const accordionContent = 'Lorem ipsum dolor sit amet, consectetur ' +
    'adipiscing elit. Nam pulvinar congue sapien eu consectetur.';

  return (
    <Scroll>
      <VBox noGutter={true}>
        <H2>Accordion Group</H2>
        <AccordionGroup>
          <Accordion hasChevron={true} header={accordionHeader} isExpanded={true}>
            <Body>{accordionContent}</Body>
          </Accordion>
          <Accordion hasChevron={true} header={accordionHeader}>
            <Accordion header={innerAccordionHeader}>
              <Body>{accordionContent}</Body>
            </Accordion>
            <Body>{accordionContent}</Body>
          </Accordion>
          <Accordion hasChevron={true} header={accordionHeader}>
            <Body>{accordionContent}</Body>
          </Accordion>
        </AccordionGroup>
        <H2>Single Accordions</H2>
        <Accordion hasChevron={true} header={accordionHeader}>
          <Body>{accordionContent}</Body>
        </Accordion>
        <Accordion header={accordionHeader}>
          <Accordion header={innerAccordionHeader}>
            <Body>{accordionContent}</Body>
          </Accordion>
          <Body>{accordionContent}</Body>
        </Accordion>
        <Accordion header={accordionHeader}>
          <Body>{accordionContent}</Body>
        </Accordion>
      </VBox>
    </Scroll>
  );
});
