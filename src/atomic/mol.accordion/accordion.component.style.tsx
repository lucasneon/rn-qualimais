import { Animated } from 'react-native';
import styled from 'styled-components/native';

import { Color, Opacity, ScreenUnit, Spacing } from '@atomic/obj.constants';

export const AccordionContentStyled = styled(Animated.View)`
  overflow: hidden;
  backgroundColor: transparent;
`;

export const AccordionHeaderStyled = styled.TouchableOpacity.attrs({
  activeOpacity: Opacity.Active,
})`
  background-color: ${Color.GrayXLight};
  padding: ${Spacing.Large + ScreenUnit};
  align-items: center;
  flex-direction: row;
`;

export const AccordionHeaderContentStyled = styled.View`
  flex-grow: 1;
  flex-shrink: 1;
`;

export const AccordionContentWrapperStyled = styled.View`
  position: absolute;
  left: 0;
  right: 0;
`;

export const AccordionHeaderChevronStyled = styled(Animated.Image)`
  marginLeft: ${Spacing.Small};
`;
