import { Color } from '@atomic/obj.constants';
import styled from 'styled-components/native';

export const BulletPointTextStyled = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const BulletPointCircleTextStyled = styled.View`
  width: 24;
  height: 24;
  border-radius: 12;
  background-color: ${Color.Alert};
  justify-content: center;
  align-items: center;
`;

export const BulletPointTextTextStyled = styled.View`
  flex: 1;
`;
