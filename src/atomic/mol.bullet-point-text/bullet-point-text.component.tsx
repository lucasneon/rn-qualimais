import * as React from 'react';

import { Body, H3 } from '@atomic/atm.typography';
import { HBox } from '@atomic/obj.grid';

import { BulletPointCircleTextStyled } from './';

export interface BulletPointTextProps {
  bulletPointText: string;
  bulletPointContentText: string;
}

export const BulletPointText = (props: BulletPointTextProps) => {
  return (
    <HBox vAlign={'center'}>
      <BulletPointCircleTextStyled>
        <Body>{props.bulletPointText}</Body>
      </BulletPointCircleTextStyled>
      <H3>{props.bulletPointContentText}</H3>
    </HBox>
  );
};
