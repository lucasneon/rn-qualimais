import * as React from 'react';

import { VBox } from '@atomic/obj.grid';

import { BulletPointText } from '@atomic/mol.bullet-point-text';
import { H2 } from '@atomic/atm.typography';
import { Scroll } from '@atomic/obj.scroll';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Molecules', module);

stories.add('Bullet point', () => (
  <Scroll>
    <VBox>
      <H2>Bullet Point Text</H2>
      <BulletPointText bulletPointText='Bullet Point Text' bulletPointContentText='Bullet Point Content' />
    </VBox>
  </Scroll>
));
