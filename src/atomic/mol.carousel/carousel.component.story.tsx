import * as React from 'react';

import { CarouselSample } from '@atomic/mol.carousel/carousel.sample';
import { storiesOf } from '@storybook/react-native';

const stories = storiesOf('Molecules', module);

stories.add('Carousel', () => (
  <CarouselSample />
));
