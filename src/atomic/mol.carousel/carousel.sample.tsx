import * as React from 'react';
import { View } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Body, H2 } from '@atomic/atm.typography';
import { VBox } from '@atomic/obj.grid';
import { Scroll } from '@atomic/obj.scroll';

// Automatic form with submit button & label to show submitted data
export class CarouselSample extends React.Component<any, any> {

  private carouselRef;

  constructor(props: any) {
    super(props);
    this.state = {
      index: 0,
      entries: [{title: 'Title 1'}, {title: 'Title 2'}, {title: 'Title 3'}],
    };
  }

  renderItem = ({ item }) => {
    return (
      <View style={{backgroundColor: '#eee', width: 300, height: 200}}>
        <H2>{item.title}</H2>
      </View>
    );
  }

  render() {
    return (
      <Scroll>
        <VBox>
          <H2>Based on archriss/react-native-snap-carousel</H2>
          <Body
            selectable={true}
          >
            https://github.com/archriss/react-native-snap-carousel/blob/
            master/doc/PROPS_METHODS_AND_GETTERS.md
          </Body>
        </VBox>
        <Carousel
          ref={c => this.carouselRef = c}
          data={this.state.entries}
          renderItem={this.renderItem}
          sliderWidth={380}
          itemWidth={300}
          loop={true}
          loopClonesPerSide={2}
          autoplay={true}
          autoplayDelay={500}
          autoplayInterval={3000}
          // tslint:disable-next-line:jsx-no-lambda
          onSnapToItem={index => this.setState({ index })}
        />
        <Pagination
          containerStyle={{alignSelf: 'center'}}
          dotsLength={this.state.entries.length}
          activeDotIndex={this.state.index}
          dotColor={'rgba(0, 0, 0, 1)'}
          inactiveDotColor={'rgb(0, 0, 0)'}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
          carouselRef={this.carouselRef}
          tappableDots={!!this.carouselRef}
        />

      </Scroll>
    );
  }
}
