import * as React from 'react';

import { AutoFormSample, VBox, VSeparator } from '@atomic';
import { CheckboxField } from '@atomic/atm.checkbox';
import { H2, InputLabel } from '@atomic/atm.typography';

import { storiesOf } from '@storybook/react-native';
import { Form } from '@atomic';

const stories = storiesOf('Form', module);

stories.add('Checkbox', () => (
  <AutoFormSample>
    <VBox>
      <H2>Checkbox Samples</H2>
      <InputLabel>Checkbox Group</InputLabel>
      <Form.Field name='checkboxGroup' label='Checkbox group'>
        <CheckboxField id={1}>Value 1</CheckboxField>
        <CheckboxField id={2}>Value 2</CheckboxField>
        <CheckboxField id={3}>Value 3</CheckboxField>
      </Form.Field>
      <VSeparator />
      <InputLabel>Standalone Checkbox</InputLabel>
      <CheckboxField id={'S'}>With custom value</CheckboxField>
      <CheckboxField id={'checked'} checked={true}>Boolean (value props ommitted)</CheckboxField>
      <CheckboxField id={'disabled'} disabled={true}>Boolean disabled</CheckboxField>
      <CheckboxField id={'checked/disabled'} disabled={true} checked={true}>Boolean disabled</CheckboxField>
    </VBox>
  </AutoFormSample>
));
