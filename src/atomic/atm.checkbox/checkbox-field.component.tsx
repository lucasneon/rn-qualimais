import * as React from 'react';
import { Image } from 'react-native';
import { Asset } from '@atomic/obj.asset';
import { FormFieldContexState, FormFieldContext } from '@atomic/obj.form/form-field.component';
import {
  CheckboxFieldSelectStyled, CheckboxFieldStyled, CheckboxFieldTextStyled
} from './checkbox-field.component.style';

const CheckboxFieldSelect = (props: CheckboxFieldProps) => {
  return (
    <CheckboxFieldSelectStyled {...props}>
      <Image source={Asset.Icon.Atomic.Check} style={{tintColor: 'white'}} />
    </CheckboxFieldSelectStyled>
  );
};

export interface CheckboxFieldProps {
  onValueChange?: (id: any, checked: boolean) => void;
  id: any;
  checked?: boolean;
  disabled?: boolean;
}

interface CheckboxFieldState  {
  checked: boolean;
}

export class CheckboxField extends React.Component<CheckboxFieldProps, CheckboxFieldState> {

  private formFieldConsumer: FormFieldContexState;

  constructor(props: CheckboxFieldProps) {
    super(props);

    this.state = {
      checked: props.checked ? true : false,
    };
  }

  componentDidMount() {
    if (this.props.checked !== undefined && this.formFieldConsumer) {
      throw new Error('Use <FormField value={[value]} /> to set the checked value');
    }

    if (this.formFieldConsumer) {
      if (Array.isArray(this.formFieldConsumer.value) &&
          this.formFieldConsumer.value.indexOf(this.props.id) > -1
      ) {
        this.formFieldConsumer.onValueChange([this.props.id], true);
        this.setState({checked: true});
      }
    }
  }

  onPress = () => {
    const checked: boolean = !this.state.checked;

    this.setState({checked });
    if (this.props.onValueChange) {
      this.props.onValueChange(this.props.id, checked);
    }

    if (this.formFieldConsumer) {
      this.formFieldConsumer.onValueChange([this.props.id], checked);
    }
  }

  render() {
    return (
      <FormFieldContext.Consumer>
      {(formFieldConsumer: FormFieldContexState) => {
        this.formFieldConsumer = formFieldConsumer;
        return (
          <CheckboxFieldStyled onPress={this.onPress} disabled={this.props.disabled}>
            <CheckboxFieldSelect id={this.props.id} checked={this.state.checked} disabled={this.props.disabled} />
            <CheckboxFieldTextStyled id={this.props.id} disabled={this.props.disabled}>
              {this.props.children}
            </CheckboxFieldTextStyled>
          </CheckboxFieldStyled>
        );
      }}
      </FormFieldContext.Consumer>
    );
  }
}
