import styled from 'styled-components/native';

import { Border, Color, Spacing } from '@atomic/obj.constants';
import { InputValue } from '@atomic/atm.typography';
import { CheckboxFieldProps } from './checkbox-field.component';

const CHECKBOX_WIDTH = 20;

const checkboxColor = (props: CheckboxFieldProps) => {
  if (props.checked) {
    return 'transparent';
  } else if (props.disabled) {
    return Color.Gray;
  }
  return Color.BlueDark;
};

const checkboxBgColor = (props: CheckboxFieldProps) => {
  if (props.checked) {
    if (props.disabled) {
      return Color.PrimaryDisabled;
    }
    return Color.BlueDark;
  }
  return 'transparent';
};

export const CheckboxFieldStyled = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  margin-vertical: ${Spacing.XSmall};
`;

export const CheckboxFieldSelectStyled = styled.View`
  width: ${CHECKBOX_WIDTH};
  height: ${CHECKBOX_WIDTH};
  margin-right: ${Spacing.Small};
  border-width: ${Border.Width};
  border-color: ${(props: CheckboxFieldProps) => checkboxColor(props)};
  background-color: ${(props: CheckboxFieldProps) => checkboxBgColor(props)};
  border-radius: ${Border.Radius};
  justify-content: center;
  align-items: center;
`;

export const CheckboxFieldBulletSpacingStyled = styled.View`
  height: ${Spacing.Large};
`;

export const CheckboxFieldTextStyled = styled(InputValue)`
  ${(props: CheckboxFieldProps) => props.disabled ? `color: ${Color.Gray};` : null }
  flex-grow: 1;
  width: 0; /* hack for text wrapping */
`;
