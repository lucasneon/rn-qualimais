import { Color, Spacing } from '@atomic/obj.constants';

import styled from 'styled-components/native';

export const ModalImageZoomOverlayStyled = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${ Color.GrayXDarkTransparent };
  padding: 15%;
`;

export const ModalImageZoomContentStyled = styled.Image`
  height: 100%;
  width: 100%;
  margin-bottom: ${ Spacing.Medium };
  resize-mode: contain;
`;
