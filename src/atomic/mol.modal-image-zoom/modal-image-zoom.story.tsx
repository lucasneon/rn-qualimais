import * as React from 'react';
import { Asset } from '@atomic';
import { PrimaryButton } from '@atomic/atm.button';
import { H2 } from '@atomic/atm.typography';
import { VBox } from '@atomic/obj.grid';
import { storiesOf } from '@storybook/react-native';
import { ModalImageZoom } from './modal-image-zoom.component';

class ImagePickerSample extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = { isModalVisible: false };
  }

  render() {
    return (
      <VBox>
        <H2>Modal Image Zoom</H2>
        <PrimaryButton
          text='Open Modal'
          onTap={this.openModal}
          expanded={true}
        />
        <ModalImageZoom
          image={Asset.Icon.Atomic.Photo}
          isVisible={this.state.isModalVisible}
          onRequestClose={this.closeModal}
        />
      </VBox>
    );
  }

  private openModal = () => {
    this.setState({isModalVisible: true});
  }

  private closeModal = () => {
    this.setState({isModalVisible: false});
  }
}

const stories = storiesOf('Molecules', module);

stories.add('Modal Image Zoom', () => (
  <ImagePickerSample />
));
