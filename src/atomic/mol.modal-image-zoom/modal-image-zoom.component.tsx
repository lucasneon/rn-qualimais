import * as React from 'react';
import { ImageURISource, Modal } from 'react-native';
import { NeutralButton } from '@atomic/atm.button';
import { ModalImageZoomContentStyled, ModalImageZoomOverlayStyled } from './modal-image-zoom.component.style';
import { strings } from './modal-image-zoom.strings';

export interface ModalImageZoomProps {
  image: ImageURISource;
  isVisible: boolean;
  onRequestClose: () => void;
}

export class ModalImageZoom extends React.Component<ModalImageZoomProps, any> {

  render() {
    return (
      <Modal
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={this.props.onRequestClose}
      >
        <ModalImageZoomOverlayStyled>
          <ModalImageZoomContentStyled
            source={this.props.image}
          />
          <NeutralButton
            expanded={true}
            onTap={this.props.onRequestClose}
            text={strings.closeButtonLabel}
          />
        </ModalImageZoomOverlayStyled>
      </Modal>
    );
  }
}
