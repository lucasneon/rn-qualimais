import styled from 'styled-components/native';
import { Color, Spacing } from '@atomic/obj.constants';

export const GUTTER = Spacing.Large;
export const HALF_GUTTER = Spacing.Small;
export const HALF_XGUTTER = Spacing.XSmall;


export interface RootProps {
  bgColor?: boolean;
}

interface BoxProps {
  hAlign?: string;
  vAlign?: string;
}

export interface VBoxProps extends BoxProps {
  vGrow?: boolean;
  noGutter?: boolean;
}

export interface HBoxProps extends BoxProps {
  wrap?: boolean;
}

export interface HBoxItemProps extends HBoxProps {
  grow?: number;
}

export const Root = styled.View`
  background-color: ${(props: RootProps) => props.bgColor ? Color.GrayXLight : Color.White};
  flex: 1;
  align-self: stretch;
`;

export const VBox = styled.View`
  padding-horizontal: ${(props: VBoxProps) => props.noGutter ? '0' : (GUTTER)};
  ${(props: VBoxProps) => props.vGrow ? 'flex-grow: 1' : null};
  justify-content: ${(props: VBoxProps) => props.vAlign ? props.vAlign : 'flex-start'};
  align-items: ${(props: VBoxProps) => props.hAlign ? props.hAlign : 'stretch'};
`;

export const VSeparator = styled.View`
  height: ${GUTTER};
`;

export const VSeparatorCard = styled.View`
  height: ${HALF_GUTTER};
`;

const HBoxStyled = styled.View`
  flex-direction: row;
  align-content: flex-start;
  align-items: ${(props: HBoxProps) => props.wrap ? 'flex-start' : 'stretch'};
  flex-wrap: ${(props: HBoxProps) => props.wrap ? 'wrap' : 'nowrap'};
`;

export const HBox: any = HBoxStyled;
HBox.Item = styled.View`
  ${(props: HBoxItemProps) => !props.wrap ? (props.grow ? `
  flex-basis: 0;
  flex-grow:  ${props.grow}` :
    'flex: 1') : null};
  justify-content: ${(props: HBoxItemProps) => props.vAlign ? props.vAlign : 'flex-start'};
  align-items: ${(props: HBoxItemProps) => props.hAlign ? props.hAlign : 'stretch'};
`;
HBox.Separator = styled.View`
  width: ${GUTTER};
`;
